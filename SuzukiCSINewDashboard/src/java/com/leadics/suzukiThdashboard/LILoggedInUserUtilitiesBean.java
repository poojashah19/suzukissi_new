package com.leadics.suzukiThdashboard;

import com.leadics.suzukithdashboard.service.LISystemuserService;
import com.leadics.suzukithdashboard.service.LIWebsessionService;
import com.leadics.suzukithdashboard.service.LISystemuserviewService;
import com.leadics.suzukithdashboard.to.LILipermissionsRecord;
import com.leadics.suzukithdashboard.to.LIWebsessionRecord;
import com.leadics.suzukithdashboard.to.LISystemuserRecord;
import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.controller.LIWebsessionController;
import javax.servlet.http.*;
import javax.servlet.*;

import org.json.simple.JSONObject;

import com.leadics.utils.DateUtils;
import com.leadics.utils.LogUtils;
import com.leadics.utils.PropertyUtil;
import com.leadics.utils.StringUtils;
import com.leadics.suzukithdashboard.to.LISystemuserviewRecord;

import java.util.*;
import java.util.regex.Pattern;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.*;

public class LILoggedInUserUtilitiesBean {

    private static LogUtils logger
            = new LogUtils(LILoggedInUserUtilitiesBean.class.getName());

    public static void main(String[] args)
            throws Exception {

    }

    public boolean hasMainMenuPermission(HttpServletRequest request, String category, String menuCode, LILipermissionsRecord[] customPermissions) {
        return true;
    }

    public boolean hasCustomPermissionDefined(String roleCode, LILipermissionsRecord[] customPermissions) {
        boolean foundVal = false;
        for (int index = 0; index < customPermissions.length; index++) {
            String perCode = StringUtils.noNull(customPermissions[index].getRolecode());
            roleCode = StringUtils.noNull(roleCode);
            if (perCode.equals(roleCode)) {
                return true;
            }
        }
        return false;
    }

    public String getPermissionCode(String roleCode, String menuCode, String category, LILipermissionsRecord[] customPermissions) {
        for (int index = 0; index < customPermissions.length; index++) {
            if (StringUtils.isSame(customPermissions[index].getRolecode(), roleCode)) {
                if (StringUtils.isSame(customPermissions[index].getFeaturecode(), menuCode)) {
                    return customPermissions[index].getBlock();
                }
            }
        }
        return "";
    }

    public void generatePermissionFile(String roleName, String menuCode, String category) {
        try {
            if (!PropertyUtil.isPropertyEquals("WriteToLISMenuItem", "Y")) {
                return;
            }
            if (isPermissionAlreadyRegisteredInLISMenu(roleName, menuCode, category)) {
                return;
            }
            return;
        } catch (Exception e) {
            logger.error(e);
        }
    }

    public boolean isPermissionAlreadyRegisteredInLISMenu(String roleName, String menuCode, String category) {
        try {
            String miquery = "select count(*) cnt from mfs_menu_item WHERE (MENU_ITEM_CODE = '#MICODE#')AND(CATEGORY = '#CAT#')AND(ROLE_CODE = '#ROLENAME#')";
            miquery = StringUtils.replaceString(miquery, "#MICODE#", menuCode, true);
            miquery = StringUtils.replaceString(miquery, "#CAT#", category, true);
            miquery = StringUtils.replaceString(miquery, "#ROLENAME#", roleName, true);

            LIDAO dao = new LIDAO();
            int cnt = dao.loadCount(miquery);
            return (cnt > 0);
        } catch (Exception e) {
            logger.error(e);
            return true;
        }
    }

    public boolean hasMenuPermission(HttpServletRequest request, String category, String menuCode, LILipermissionsRecord[] customPermissions) {

        LISystemuserviewRecord loggedInUser = getLoggedInUser(request, null);
        String roleName = StringUtils.noNull(loggedInUser.getRolename());

        generatePermissionFile(category, menuCode, "MenuItem");

        if (customPermissions == null) {
            return true;
        }
        if (customPermissions.length < 1) {
            return true;
        }

        String menuCodeKey = category + " " + "MenuItem" + " " + menuCode;

        if (!hasCustomPermissionDefined(roleName, customPermissions)) {
            return true;
        }
        String permCode = StringUtils.noNull(getPermissionCode(roleName, menuCodeKey, category, customPermissions));

        if (permCode.equals("Y")) {
            return false;
        }
        return true;
    }

    public boolean hasPermission(HttpServletRequest request, HttpServletResponse response, String accessCode) {
        return true;
    }

    public boolean isGroupLevelPermissionToBeApplied(HttpServletRequest request) {
        String sessionViewMode = StringUtils.noNull(request.getSession().getAttribute("VIEW_MODE"));
        if (sessionViewMode.equals("MAKER")) {
            return true;
        }

        if (sessionViewMode.equals("CHECKER")) {
            return true;
        }

        return false;
    }

    LIMiscUtilitiesBean utilBean = new LIMiscUtilitiesBean();

    public void recordLogin(HttpServletRequest request)
            throws Exception {
    }

    public void recordAccess(HttpServletRequest request, HttpServletResponse response, String serviceName) {

        /*
         try
         {
         LIWebaccesslogService webAccessLogService = new LIWebaccesslogService();
         LIWebaccesslogRecord webAccessLogRecord = new LIWebaccesslogRecord();
         webAccessLogRecord.setService(serviceName);
         webAccessLogRecord.setSource(utilBean.getIPAddress(request));
         webAccessLogRecord.setSessionid(request.getSession().getId());
         webAccessLogRecord.setUserid(getLoggedInUser(request, response).getUserid());
         webAccessLogRecord.setCreatedby(getLoggedInUser(request, response).getId());
         webAccessLogRecord.setCreatedat(DateUtils.getCurrentDateTime());
         webAccessLogService.insertLIWebaccesslogRecord(webAccessLogRecord);

         if (serviceName.equalsIgnoreCase("Login"))
         {
         LIWebsessionService webSessionService = new LIWebsessionService();
         LIWebsessionRecord webSessionRecord = new LIWebsessionRecord();
         webSessionRecord.setSessionid(request.getSession().getId());
         webSessionRecord.setSource(utilBean.getIPAddress(request));
         webSessionRecord.setLastacc(DateUtils.getCurrentDateTime());
         webSessionRecord.setUserid(getLoggedInUser(request, response).getUserid());
         webSessionRecord.setRstatus(LIConstants.STATUS_ACTIVE);
         webSessionRecord.setCreatedby(getLoggedInUser(request, response).getId());
         webSessionRecord.setCreatedat(DateUtils.getCurrentDateTime());
         webSessionService.insertLIWebsessionRecord(webSessionRecord);
         }
         else if (serviceName.equalsIgnoreCase("Log Off"))
         {
         LIWebsessionService webSessionService = new LIWebsessionService();
         LIWebsessionRecord searchRecord = new LIWebsessionRecord();
         searchRecord.setUserid(getLoggedInUser(request, response).getUserid());
	    		
         LIWebsessionRecord resultRecord = webSessionService.searchFirstLIWebsessionRecord(searchRecord);
         if (resultRecord == null) return;
         webSessionService.deleteLIWebsessionRecord(resultRecord);
         }
         else
         {
         LIWebsessionService webSessionService = new LIWebsessionService();
         LIWebsessionRecord searchRecord = new LIWebsessionRecord();
         searchRecord.setUserid(getLoggedInUser(request, response).getUserid());

         LIWebsessionRecord resultRecord = webSessionService.searchFirstLIWebsessionRecord(searchRecord);
         if (resultRecord == null) return;
         resultRecord.setLastacc(DateUtils.getCurrentDateTime());
         webSessionService.updateLIWebsessionRecord(resultRecord);
         }
         }
         catch(Exception exception)
         {
         utilBean.logUnexpectedError(exception + "");
         logger.error(exception + "");
         }
         */
    }

    public void recordAccessAndSetViewPermissions(HttpServletRequest request, HttpServletResponse response, String viewMode, String serviceName) {
        recordAccess(request, response, serviceName);
        request.getSession().setAttribute("VIEW_MODE", viewMode);
        request.getSession().setAttribute("VIEW_SERVICE_NAME", serviceName);
    }

    public String getViewServiceName(HttpServletRequest request) {
        HttpSession session = request.getSession();
        return StringUtils.noNull(session.getAttribute("VIEW_SERVICE_NAME"));
    }

    public boolean hasAccess(String pageName, LISystemuserviewRecord userRecord)
            throws Exception {
        return true;
    }

    public void processLoginIfNotLoggedIn(HttpServletRequest req, HttpServletResponse res, String UserID)
            throws Exception {
        if (!isUserLoggedIn(req, res)) {
            LISystemuserviewRecord searchRecord = new LISystemuserviewRecord();
            searchRecord.setUserid(UserID);

            LISystemuserviewService searchService = new LISystemuserviewService();
            LISystemuserviewRecord userRecord = searchService.searchFirstLISystemuserviewRecord(searchRecord);
            LISystemuserviewRecord resultRecord = new LISystemuserviewRecord();
            resultRecord.loadContent(userRecord);
            req.getSession(true).setAttribute("LoginUserRecord", resultRecord);
        }
    }

    public boolean isUserLoggedIn(HttpServletRequest req, HttpServletResponse res) {
        HttpSession session = req.getSession(true);
        Object recordObject = session.getAttribute("LoginUserRecord");
        if (recordObject == null) {
            return false;
        }

        LISystemuserviewRecord userRecord = (LISystemuserviewRecord) recordObject;
        String userID = userRecord.getUserid();
        if (userID == null) {
            return false;
        }
        if (userID.trim().length() < 1) {
            return false;
        }
        return true;
    }

    public LISystemuserviewRecord getUserRecord(String recordId)
            throws Exception {
        LISystemuserviewRecord resultRecord = new LISystemuserviewRecord();
        if (!StringUtils.isNullOrEmpty(recordId)) {
            LISystemuserviewService service = new LISystemuserviewService();

            LISystemuserviewRecord searchRecord = new LISystemuserviewRecord();
            searchRecord.setId(recordId);

            LISystemuserviewRecord userViewRecord = service.searchFirstLISystemuserviewRecord(searchRecord);
            if (userViewRecord == null) {
                resultRecord = new LISystemuserviewRecord();
            } else {
                resultRecord.loadContent(userViewRecord);
            }
        }
        return resultRecord;
    }

    public String getLoginInstitutionid(HttpServletRequest req, HttpServletResponse res) {
        LIMiscUtilitiesBean bean = new LIMiscUtilitiesBean();
        LISystemuserviewRecord loggedInUserRecord = getLoggedInUser(req, res);
        if (loggedInUserRecord == null) {
            return "";
        }
        return bean.noNull(loggedInUserRecord.getInstitutionid());
    }

    public String getInstitutionId(HttpServletRequest req, HttpServletResponse res) {
        LIMiscUtilitiesBean bean = new LIMiscUtilitiesBean();
        LISystemuserviewRecord loggedInUserRecord = getLoggedInUser(req, res);
        if (loggedInUserRecord == null) {
            return "";
        }
        return bean.noNull(loggedInUserRecord.getInstitutionid());
    }

    public String getLoggedInUserRecordId(HttpServletRequest req, HttpServletResponse res) {
        LIMiscUtilitiesBean bean = new LIMiscUtilitiesBean();
        LISystemuserviewRecord loggedInUserRecord = getLoggedInUser(req, res);
        if (loggedInUserRecord == null) {
            return "";
        }
        return bean.noNull(loggedInUserRecord.getId());
    }

    public String getLoggedInUserId(HttpServletRequest req, HttpServletResponse res) {
        LIMiscUtilitiesBean bean = new LIMiscUtilitiesBean();
        LISystemuserviewRecord loggedInUserRecord = getLoggedInUser(req, res);
        if (loggedInUserRecord == null) {
            return "";
        }
        return bean.noNull(loggedInUserRecord.getUserid());
    }

    public String getLoggedInUserRole(HttpServletRequest req, HttpServletResponse res) {
        LIMiscUtilitiesBean bean = new LIMiscUtilitiesBean();
        LISystemuserviewRecord loggedInUserRecord = getLoggedInUser(req, res);
        if (loggedInUserRecord == null) {
            return "";
        }
        return bean.noNull(loggedInUserRecord.getRolename());
    }

    public boolean isValidSession(HttpServletRequest req, HttpServletResponse res) {
        LISystemuserviewService service = new LISystemuserviewService();
        return service.isValidSession(
                getLoggedInUser(req, res).getUserid(),
                req.getSession(true).getId()
        );
    }
    
    public boolean isValidSessionBySessionID(HttpServletRequest req, HttpServletResponse res) {
        LIWebsessionRecord objLIWebsessionRecord=new LIWebsessionController().loadFormLIWebsessionRecord(req,res);
        LIWebsessionService service=new LIWebsessionService();
        return service.isValidSession(
                objLIWebsessionRecord.getUserid(),
                objLIWebsessionRecord.getSessionid()
        );
    }

    public LISystemuserviewRecord getLoggedInUser(HttpServletRequest req, HttpServletResponse res) {
        HttpSession session = req.getSession(true);
        Object recordObject = session.getAttribute("LoginUserRecord");
        if (recordObject == null) {
            return null;
        }

        LISystemuserviewRecord userRecord = (LISystemuserviewRecord) recordObject;
        String userID = userRecord.getUserid();
        if (userID == null) {
            return null;
        }
        if (userID.trim().length() < 1) {
            return null;
        }
        return userRecord;
    }

    public boolean isUserProfileSet(LISystemuserviewRecord userRecord2) {
        if (userRecord2 == null) {
            return false;
        }
        if (StringUtils.isNullOrEmpty(userRecord2.getSeqq1())) {
            return false;
        }

        if (StringUtils.isNullOrEmpty(userRecord2.getSeqa1())) {
            return false;
        }

        if (StringUtils.isNullOrEmpty(userRecord2.getEmail())) {
            return false;
        }
        if (StringUtils.isNullOrEmpty(userRecord2.getMobile())) {
            return false;
        }

        if (!PropertyUtil.isPropertyEquals("ACSQLimitTo1", "Y")) {
			//if (StringUtils.isNullOrEmpty(userRecord2.getPrefcomm())) return false;
            //if (StringUtils.isNullOrEmpty(userRecord2.getPreflang())) return false;
            //if (StringUtils.isNullOrEmpty(userRecord2.getCountry())) return false;		

            if (StringUtils.isNullOrEmpty(userRecord2.getSeqq2())) {
                return false;
            }
            if (StringUtils.isNullOrEmpty(userRecord2.getSeqq3())) {
                return false;
            }
            if (StringUtils.isNullOrEmpty(userRecord2.getSeqa2())) {
                return false;
            }
            if (StringUtils.isNullOrEmpty(userRecord2.getSeqa3())) {
                return false;
            }
        }

        return true;
    }

    public boolean isUserPasswordChangeRequired(LISystemuserviewRecord userRecord2)
            throws Exception {
        if (userRecord2 == null) {
            return false;
        }

        LISystemuserService userService = new LISystemuserService();
        LISystemuserRecord dbUserRecord = userService.loadLISystemuserRecord(userRecord2.getId());
        if (dbUserRecord == null) {
            return false;
        }

        if (StringUtils.noNull(dbUserRecord.getInitialpwd()).equals(StringUtils.noNull(dbUserRecord.getUserpassword()))) {
            return true;
        }

        if (isPasswordExpired(dbUserRecord)) {
            return true;
        }
        return false;
    }

    public boolean isPasswordExpired(LISystemuserRecord userRecord) {
        try {
            if (StringUtils.isNullOrEmpty(userRecord.getPwdsetdate())) {
                return true;
            }

            Date pwdSetDate = DateUtils.convertDate(userRecord.getPwdsetdate(), DateUtils.getLongDateFormat());
            Date today = new Date();
            int pwdvalidityPeriod = Integer.parseInt(PropertyUtil.getProperty("pwdvalidity"));

            int diffDays = DateUtils.getDifferenceInDays(pwdSetDate, today);
            if (diffDays > pwdvalidityPeriod) {
                return true;
            }
            return false;
        } catch (Exception exception) {
            logger.error(exception);
            logger.error(StringUtils.getStackTrace(exception));
            return true;
        }
    }

    public void closeSession(String sessionID, String userID, String reason) {
        try {
            String updateStatement = "";
            updateStatement += " UPDATE web_session ";
            updateStatement += " SET CLOSE_TYPE = '#CLOSETYPE#', ";
            updateStatement += " CLOSED_AT = '#CLOSEDAT#', ";
            updateStatement += " RSTATUS = '0' ";
            updateStatement += " WHERE (RSTATUS = '1')AND(SESSIONID = '#SESSIONID#')AND(USER_ID='#USERID#') ";

            updateStatement = StringUtils.replaceString(updateStatement, "#CLOSETYPE#", reason, true);
            updateStatement = StringUtils.replaceString(updateStatement, "#CLOSEDAT#", DateUtils.getCurrentDateTimene(), true);
            updateStatement = StringUtils.replaceString(updateStatement, "#SESSIONID#", sessionID, true);
            updateStatement = StringUtils.replaceString(updateStatement, "#USERID#", userID, true);

            LIDAO dao = new LIDAO();
            dao.executeUpdateQuery(updateStatement);
        } catch (Exception e) {
            logger.error(e);
        }
    }

    public void updateLastAccTS(String sessionID, String userID) {
        try {
            LIWebsessionService serv = new LIWebsessionService();
            LIWebsessionRecord websearchrec = new LIWebsessionRecord();
            websearchrec.setRstatus("1");
            websearchrec.setSessionid(sessionID);
            websearchrec.setUserid(userID);
            websearchrec = serv.searchFirstLIWebsessionRecord(websearchrec);
            if (websearchrec == null) {
                return;
            }

            websearchrec.setLastacc(DateUtils.getCurrentDateTime());
            serv.updateLIWebsessionRecordNonNull(websearchrec);
        } catch (Exception e) {
            logger.error(e);
        }
    }

    public void logOffUser(HttpServletRequest req, HttpServletResponse res) {
        HttpSession session = req.getSession(true);
        session.setAttribute("AuthenticateActionMessage", "");
        session.setAttribute("LoginUserRecord", null);
    }

    public boolean isRequestSecure(HttpServletRequest req) {
        if (PropertyUtil.isPropertyEquals("DisableIPSecurityScanning", "Y")) {
            return true; 
        }

        Enumeration<String> params = req.getParameterNames();
        String value = null;
        while (params.hasMoreElements()) {
            value = req.getParameter(params.nextElement());
            
            if (!stripXSS(value)) {
                return false;
            }
        }
        return true;
    }

    private boolean stripXSS(String value) {
        String cleanValue = null;

        value = StringUtils.noNull(value);
        value = value.toLowerCase();
        if (value.contains("<script>")) {
            return false;
        }
        if (value.contains("</script>")) {
            return false;
        }
        if (value.contains("javascript:")) {
            return false;
        }
        if (value.contains("vbscript:")) {
            return false;
        }

        String regex1 = "<script>(.*?)</script>.*";
        String regex2 = "&lt;script&gt;(.*?)&lt;/script&gt.*";
        String regex3 = "&lt;script&gt.*";
        String regex4 = "/&lt;script&gt.*";
        String regex5 = "src[\r\n]*=[\r\n]*\\\'(.*?)\\\'.*";
        String regex6 = "src[\r\n]*=[\r\n]*\\\"(.*?)\\\".*";
        String regex7 = "</script>.*";
        String regex8 = "<script.*?src=(.*?)";
        String regex9 = "eval\\((.*?)\\).*";
        String regex10 = "expression\\((.*?)\\).*";
        String regex11 = "javascript:.*";
        String regex12 = "vbscript:.*";
        String regex13 = "onload(.*?)=.*";

        if (value != null) {
            cleanValue = Normalizer.normalize(value, Normalizer.Form.NFD);
            cleanValue = cleanValue.replaceAll("\0", "");

            if (Pattern.compile(regex1, Pattern.CASE_INSENSITIVE).matcher(cleanValue).matches()) {
                return false;
            } else if (Pattern.compile(regex2, Pattern.CASE_INSENSITIVE).matcher(cleanValue).matches()) {
                return false;
            } else if (Pattern.compile(regex3, Pattern.CASE_INSENSITIVE).matcher(cleanValue).matches()) {
                return false;
            } else if (Pattern.compile(regex4, Pattern.CASE_INSENSITIVE).matcher(cleanValue).matches()) {
                return false;
            } else if (Pattern.compile(regex5, Pattern.CASE_INSENSITIVE).matcher(cleanValue).matches()) {
                return false;
            } else if (Pattern.compile(regex6, Pattern.CASE_INSENSITIVE).matcher(cleanValue).matches()) {
                return false;
            } else if (Pattern.compile(regex7, Pattern.CASE_INSENSITIVE).matcher(cleanValue).matches()) {
                return false;
            } else if (Pattern.compile(regex8, Pattern.CASE_INSENSITIVE).matcher(cleanValue).matches()) {
                return false;
            } else if (Pattern.compile(regex9, Pattern.CASE_INSENSITIVE).matcher(cleanValue).matches()) {
                return false;
            } else if (Pattern.compile(regex10, Pattern.CASE_INSENSITIVE).matcher(cleanValue).matches()) {
                return false;
            } else if (Pattern.compile(regex11, Pattern.CASE_INSENSITIVE).matcher(cleanValue).matches()) {
                return false;
            } else if (Pattern.compile(regex12, Pattern.CASE_INSENSITIVE).matcher(cleanValue).matches()) {
                return false;
            } else if (Pattern.compile(regex13, Pattern.CASE_INSENSITIVE).matcher(cleanValue).matches()) {
                return false;
            } else {
                return true;
            }
        }
        return true;
    }
}

/*
 public String getLoggedinUserRole(HttpServletRequest request, HttpServletResponse response)
 {
 try
 {
 LISystemuserviewRecord userRecord = getLoggedInUser(request, response);
 if (userRecord == null) return "";

 LISystemuserviewService service = new LISystemuserviewService();
 LISystemuserviewRecord searchrecord = new LISystemuserviewRecord();
 searchrecord.setRstatus("1");
 searchrecord.setId(userRecord.getId());
   	 	
	 	
 LISystemuserviewRecord userInfoViewRecord = service.searchFirstLISystemuserviewRecord(searchrecord);
	 	
 if (userInfoViewRecord == null) return "";
   	 	
   	 	
 return noNull(userInfoViewRecord.getRolename());
 }
 catch(Exception exception)
 {
 utilBean.logUnexpectedError(exception + "");
 logger.error(exception + "");
 return "";
 }
 }

 public boolean isLoggedinUserGlobalAdminByRole(HttpServletRequest request, HttpServletResponse response)
 {
 String userRole = noNull(getLoggedinUserRole(request, response));
 if (userRole.contains("GLOBAL-ADMIN")) return true;
 return false;
 }
    
 public boolean isLoggedinUserMakerByRole(HttpServletRequest request, HttpServletResponse response)
 {
 String userRole = noNull(getLoggedinUserRole(request, response));
 if (userRole.contains("MAKER")) return true;
 return false;
 }
    
 public boolean isLoggedinUserCheckerByRole(HttpServletRequest request, HttpServletResponse response)
 {
 String userRole = noNull(getLoggedinUserRole(request, response));
 if (userRole.contains("CHECKER")) return true;
 return false;
 }
    
 public boolean isLoggedinUserAdminByRole(HttpServletRequest request, HttpServletResponse response)
 {
 String userRole = noNull(getLoggedinUserRole(request, response));
 if (!userRole.contains("GLOBAL-ADMIN"))
 {
 if (userRole.contains("ADMIN"))
 return true;
 }
 return false;
 }                
    
 public boolean isLoggedinUserGlobalAdmin(HttpServletRequest request, HttpServletResponse response)
 {
 String userRole = noNull(getLoggedinUserRole(request, response));
 if (userRole.contains("GLOBAL-ADMIN")) return true;
 return false;
 }
    
 public boolean isLoggedinUserAdmin(HttpServletRequest request, HttpServletResponse response)
 {
 if (!isViewMode(request, response, "ADMIN")) return false;

 String userRole = noNull(getLoggedinUserRole(request, response));
 if (userRole.contains("ADMIN")) return true;
 return false;
 }
    
 public boolean isLoggedinUserChecker(HttpServletRequest request, HttpServletResponse response)
 {
 if (!isViewMode(request, response, "CHECKER")) return false;    	
 String userRole = noNull(getLoggedinUserRole(request, response));
 if (userRole.contains("CHECKER")) return true;
 return false;
 }
    
 public boolean isLoggedinUserMaker(HttpServletRequest request, HttpServletResponse response)
 {
 if (!isViewMode(request, response, "MAKER")) return false;
 String userRole = noNull(getLoggedinUserRole(request, response));
 if (userRole.contains("MAKER")) return true;
 return false;
 }

 public boolean isViewMode(HttpServletRequest request, HttpServletResponse response, String inputViewMode)
 {
 inputViewMode = StringUtils.noNull(inputViewMode);
 String sessionViewMode = StringUtils.noNull(request.getSession().getAttribute("VIEW_MODE"));
 return  inputViewMode.equals(sessionViewMode);
 }
    
 public boolean hasPermission(HttpServletRequest request, HttpServletResponse response, String accessCode)
 {
 try
 {
 LIResourcepermissionsService service = new LIResourcepermissionsService();
	
 String sessionViewMode = StringUtils.noNull(request.getSession().getAttribute("VIEW_MODE"));
	    	
 LIResourcepermissionsRecord searchRecord = new LIResourcepermissionsRecord();
 searchRecord.setResgroup("ADMIN_UITABLE_PERMISSIONS");
 searchRecord.setRstatus("1");
 searchRecord.setResource(accessCode);
 searchRecord.setPermission(sessionViewMode);

 LIResourcepermissionsRecord[] permissionList = service.searchLIResourcepermissionsRecords(searchRecord);
 if (permissionList == null) return false;
	    	
 for (int index = 0; index < permissionList.length; index++)
 {
 String permittedRole = StringUtils.noNull(permissionList[index].getPermission());
 if (permittedRole.equals("ADMIN"))
 {
 if (isLoggedinUserAdmin(request, response))
 {
 return true;
 }
 }
	    		
 if (permittedRole.equals("MAKER"))
 {
 if (isLoggedinUserMaker(request, response))
 {
 return true;
 }
 }
	    		
 if (permittedRole.equals("CHECKER"))
 {
 if (isLoggedinUserChecker(request, response))
 {
 return true;
 }
 }    		    		
 }
 return false;
 }
 catch(Exception exception)
 {
 logger.error(exception + "");
 return false;
 }
 }
 */
