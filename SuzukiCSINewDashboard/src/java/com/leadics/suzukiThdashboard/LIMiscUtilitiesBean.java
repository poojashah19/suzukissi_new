package com.leadics.suzukiThdashboard;

import com.leadics.suzukithdashboard.service.LIConsoleaccesslogService;
import com.leadics.suzukithdashboard.service.LIIntfpermittedipService;
import com.leadics.suzukithdashboard.to.LIIntfpermittedipRecord;
import com.leadics.suzukithdashboard.to.LIConsoleaccesslogRecord;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.common.LIDAO;
import javax.servlet.http.*;
import javax.servlet.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.leadics.utils.*;

import java.util.*; 
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.*;

public class LIMiscUtilitiesBean
{
    int scrollDifference = 50;
    
	private static LogUtils logger =
			new LogUtils(LIMiscUtilitiesBean.class.getName());
	
	public String generateDropdownOptionsAsJSON(String query, String selectedValue)
	{
		try
		{
			LIService service = new LIService();

			StringBuffer resultBuffer = new StringBuffer();
			Map demandCodeMap = service.loadMap(
					query, 
					"ID", 
					"TXT", 
					false);

			JSONArray resultArray = new JSONArray();

			Iterator demandCodeIterator = demandCodeMap.keySet().iterator();
	        int demandCodeIndex = 0;
	        while (demandCodeIterator.hasNext())
	        {
	        	String demandCode = StringUtils.noNull(demandCodeIterator.next().toString());
	        	String demandCodeDesc = StringUtils.getMapValue(demandCodeMap, demandCode);
	        	
	        	JSONObject demandObject = new JSONObject();
	        	demandObject.put("id", demandCode);
	        	demandObject.put("text", demandCodeDesc);
	        	resultArray.add(demandObject);
	        }
	        return resultArray.toJSONString();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return (new String());
		}
	}	
    
	public String getCurrencyDescription(String currency)
	{
		currency  = StringUtils.noNull(currency);
		if (currency.equals("KES")) return "Kenyan Schilling";
		if (currency.equals("USD")) return "US Dollar";
		if (currency.equals("GBP")) return "British Pound";
		if (currency.equals("JPY")) return "Japanese Yen";
		if (currency.equals("AED")) return "United Arab Emirates Dirham";
		if (currency.equals("EUR")) return "Euro";
		return currency;
	}

    public void logUnexpectedError(String message)
    {
    	logger.error("Unexpected Exception:" + message);
    }
    
    public boolean isEqual(String s1, String s2)
    {
        s1 = noNull(s1);
        s2 = noNull(s2);
        return s1.equals(s2);
    }

    public boolean hasMetacharacter(String value)
    {
    	value = noNull(value);
    	if (value.toLowerCase().contains("meta")) return true;
    	if (value.toLowerCase().contains("<")) return true;
    	if (value.toLowerCase().contains(">")) return true;
    	return false;
    }
    
    public boolean isPermittedIP(LIIntfpermittedipRecord record, String ipAddress) 
    {
    	String recordContent = StringUtils.noNull(record.getPermittedip());
    	if (StringUtils.isNullOrEmpty(recordContent)) return false;
    	if (recordContent.equals("*")) return true;
    	if (recordContent.toUpperCase().equals("ALL")) return true;
    	if (ipAddress.contains(recordContent)) return true;
    	return false;
    }
   
    public boolean isIntranetIP(String ipAddress) 
    {
    	ipAddress = StringUtils.noNull(ipAddress).trim();
    	
    	if (ipAddress.startsWith("10.")) return true;
    	if (ipAddress.startsWith("127.")) return true;
    	if (ipAddress.startsWith("192.168.")) return true;
    	if (ipAddress.startsWith("172.16.")) return true;

    	return false;
    }
    
    public boolean hasInterfaceAccess(HttpServletRequest request, String accessCode)
    {
    	return hasInterfaceAccess(request, accessCode, true);
    }

    public boolean hasInterfaceAccess(HttpServletRequest request, String accessCode, boolean allowIntranetSkip)
    {
    	try
    	{
	    	String ipAddress = StringUtils.noNull(getIPAddress(request));

	    	LogUtils.println("Source IP Address:" + ipAddress);
	    	LogUtils.println("Access Code:" + accessCode);
	    	if (StringUtils.isNullOrEmpty(ipAddress)) return false;

	    	String InterfaceAccessControlEnabled = StringUtils.noNull(PropertyUtil.getProperty("InterfaceAccessControlEnabled"));
			if (!InterfaceAccessControlEnabled.equals("Y")) return true;

			if (allowIntranetSkip)
			{
		    	String InterfaceAccessControlIntranetIPEnabled = StringUtils.noNull(PropertyUtil.getProperty("InterfaceAccessControlSkipForIntranetIP"));
				if (InterfaceAccessControlEnabled.equals("Y"))
				{
					if (isIntranetIP(ipAddress)) return true;	
				}
			}

	    	LIIntfpermittedipService intfService = new LIIntfpermittedipService();
	    	LIIntfpermittedipRecord intfSearchRecord = new LIIntfpermittedipRecord();
	    	intfSearchRecord.setIntfcode(accessCode);
	    	LIIntfpermittedipRecord[] records = intfService.searchLIIntfpermittedipRecords(intfSearchRecord);
	    	if (records == null) return false;
	    	
	    	for (int index = 0; index < records.length; index++)
	    	{
	    		if (isPermittedIP(records[index], ipAddress))
	    		{
	    			return true;
	    		}
	    	}
	    	return false;
    	}
    	catch(Exception exception)
    	{
    		logger.error(exception);
    		return false;
    	}
    }

    
    private static final String HEADER_X_FORWARDED_FOR =
            "X-FORWARDED-FOR";

        public static String remoteAddr(HttpServletRequest request) {
            String remoteAddr = request.getRemoteAddr();
            String x;
            if ((x = request.getHeader(HEADER_X_FORWARDED_FOR)) != null) {
                remoteAddr = x;
                int idx = remoteAddr.indexOf(',');
                if (idx > -1) {
                    remoteAddr = remoteAddr.substring(0, idx);
                }
            }
            return remoteAddr;
        }
        
    
        
    public String getIPAddress(HttpServletRequest request)
    {
        String remoteAddr = request.getRemoteAddr();
        String x;
        if ((x = request.getHeader(HEADER_X_FORWARDED_FOR)) != null) {
            remoteAddr = x;
            int idx = remoteAddr.indexOf(',');
            if (idx > -1) {
                remoteAddr = remoteAddr.substring(0, idx);
            }
        }
        return remoteAddr;
    }
    
    public String getJSONString(JSONObject inputObject)
    {
    	if (inputObject == null) return (new JSONObject()).toJSONString();
    	return inputObject.toJSONString();
    }
    
    
    public String getRecordsNotFoundMessage()
    {
        String result = "<TABLE><TR><TD CLASS = \"ERRORMESSAGE\" ALIGN = \"CENTER\">No Records Found</TD></TR></TABLE>";
        return result;
    }

    public String getRequiredFieldMessage()
    {
        String result = "<TABLE><TR><TD CLASS = \"FOOTERMESSAGE\" ALIGN = \"CENTER\">* indicates Required Field</TD></TR></TABLE>";
        return result;
    }

    public int getScrollDifference()
    {
        return scrollDifference;
    }
    
    public String getYNCode(String value)
    {
        return getYNCode(value,"No");
    }
    
    public String getYNCode(String value, String defaultCode)
    {
        if (isNull(value)) return defaultCode;
        if (((value.toUpperCase()).trim()).equals("Y")) return "Yes";
        return "No";
    }
    
    public boolean isValidDouble(String value)
    {
        if (isNull(value)) return false;
        try
        {
            Double.parseDouble(value);
            return true;
        }
        catch(Exception e)
        {
            return false;
        }
    }

    public double getDecimalValue(String value)
    {
        if (isNull(value)) return 0;
        value = getNumber(value);
        try
        {
            double d = (new Double(value)).doubleValue();
            return d;
        }
        catch(Exception e)
        {
            return 0;
        }
    }
    
    public String formatDecimal(String value,boolean includeDollar)
    {
        String Format = "###########0.00";
        String frmvalue = "";
        try
        {
            value = getNumber(value);
            frmvalue = formatDecimal(value,Format);
            if (isNull(frmvalue)) frmvalue = "0.00";
        }
        catch(Exception e)
        {
            frmvalue = "0.00";
        }
        
        if (includeDollar)
        {
            frmvalue = "$" + frmvalue;
        }
        return frmvalue;
    }

    public String formatDecimal(String Value)
    throws Exception
    {
    	if (Value == null) return "";
        if (Value.trim().length() < 1) return "";
        return formatDecimal(Value, "###########0.00");
    }
    
    public String formatInt(String Value)
    throws Exception
    {
    	if (Value == null) return "";
        if (Value.trim().length() < 1) return "";
        return formatDecimal(Value, "###########0");
    }    

    public String formatDecimal(String Value, String Format)
    throws Exception
    {
        if (Value == null) return Value;
        if (Format == null) return Value;
        if (Value.trim().length() < 1) return Value;

        double d = (new Double(Value)).doubleValue();
        DecimalFormat df = new DecimalFormat(Format);
        return df.format(d);
    }

    public int getCurrentPageNumber(String pageNumber)
    {
        if (pageNumber == null) return 1;
        if (pageNumber.trim().length() < 1) return 1;
        try
        {
            return Integer.parseInt(pageNumber);
        }
        catch(Exception e)
        {
            return 1;
        }
    }
    
    public String getTopSearchActionMessage(int count, boolean searchRecordFound)
    {
        String resultMessage = "";
        if (searchRecordFound)
        {
            if (count <= 0) resultMessage = "No Records Found";
            else
            if (count > 0) resultMessage = "See below for Search Results";
        }
        else
        {
            resultMessage = "";
        }
        String result = "<CENTER><P><B><FONT COLOR=\"red\" FACE=\"Arial\">" + resultMessage + "</FONT></B></CENTER>";
        return result;
    }

    public String getBottomSearchActionMessage(int count, boolean searchRecordFound)
    {
        String resultMessage = "";
        if (searchRecordFound)
        {
            if (count <= 0) resultMessage = "";
            else
            if (count == 1) resultMessage = "1 Record Found";            
            else
            if (count > 0) resultMessage = count + " Records Found";
        }
        else
        {
            resultMessage = "";
        }
        String result = "<CENTER><P><B><FONT COLOR=\"red\" FACE=\"Arial\">" + resultMessage + "</FONT></B></CENTER>";
        return result;
    }

    public int getStartRecordIndex(String pageNumber)
    {
        int currpagenumber = getCurrentPageNumber(pageNumber);
        return ((currpagenumber  - 1) * scrollDifference + 1);
    }
    
    public int getEndRecordIndex(String pageNumber)
    {
        int currpagenumber = getCurrentPageNumber(pageNumber);
        return (currpagenumber * scrollDifference);
    }
    
    public String getSystemDate(String OutputFormat)
    {
        return (new DateUtils()).getSystemDate(OutputFormat);
    }

    public String getSystemDateMMDDYYYY()
    {
        String OutputFormat = "MM/dd/yyyy";
        return (new DateUtils()).getSystemDate(OutputFormat);
    }
    
    
    public String getSystemDatetime()
    {
        String OutputFormat = "MM/dd/yyyy hh:mm:ss a, z";
        return (new DateUtils()).getSystemDate(OutputFormat);
    }
    
    public String getReportDatetime()
    {
        String OutputFormat = "dd MMM yyyy hh:mm:ss a";
        return (new DateUtils()).getSystemDate(OutputFormat);
    }    

    public String getSystemDate()
    {
        String OutputFormat = "MM/dd/yyyy";
        return (new DateUtils()).getSystemDate(OutputFormat);
    }
    
    public String getYesterDate(String dateFormat)
    {
    	try
    	{
        DateUtils fd = new DateUtils();
        String formattedDate = fd.getSystemDate(dateFormat);
        return fd.getPreviousDate(formattedDate, dateFormat, dateFormat);
    	}
    	catch(Exception e) { return ""; }
    }   
    
    public String getPreviousDate(String dateValue, String dateFormat)
    {
    	try
    	{
    		DateUtils fd = new DateUtils();
    		return fd.getNextDate(dateValue, dateFormat, dateFormat);
    	}
    	catch(Exception e) { return ""; }
    }        
    
    public String getNumber(String value)
    {
        if (value == null) return value;
        value = replaceString(value,",","",true);
        value = replaceString(value," ","",true);
        return value;
    }

    public String getCheckBoxSelectionIndicator(String Value)
    {
        if (Value == null) return "";
        if (Value.equals("Y")) return "CHECKED";
        else
        return "";
    }

    public String getRadioSelectionIndicator(String ExpectedValue, String Value)
    {
        if (ExpectedValue == null) return "";
        if (Value == null) return "";
        if (ExpectedValue.equals(Value))
        return "CHECKED";
        else
        return "";
    }

    public Vector removeElementFromVector(Vector aVector, String Element)
    {
        if (aVector == null) return aVector;
        if (Element == null) return aVector;

        Vector resultVector = new Vector();
        for (int index = 0; index < aVector.size(); index++)
        {
            String line = aVector.elementAt(index) + "";
            if (!line.equals(Element))
            {
                resultVector.addElement(line);
            }
        }
        return resultVector;
    }

    public String getElementAt(Vector inputVector, int row, int col)
    {
        try
        {
            Vector aVector = (Vector)inputVector.elementAt(row);
            String result = (String)aVector.elementAt(col);
            return result;
        }
        catch(Exception e)
        {
            return null;
        }
    }

	public String makeNull(String value)
	{
	    if (value == null) return null;
	    if (value.trim().length() < 1) return null;
	    return value;
	}

    public boolean isElementInVector(Vector aVector, String element)
    {
        if (aVector==null) return false;
        if (aVector.size() < 1) return false;
        if (element == null) return false;
        for (int index = 0; index < aVector.size(); index++)
        {
            String vectorElement = aVector.elementAt(index) + "";
            if (vectorElement.equals(element))
            {
                return true;
            }
        }
        return false;
    }
    
    public boolean isValidDate(String s_date, String s_Format)
    {
        try
        {
        	if (s_date == null) return false;
            java.sql.Date result = (new DateUtils()).getSQLDateObject(s_date, s_Format);
            if (result == null) return false;
            return true;
        }
        catch(Exception e)
        {
            return false;
        }
    }
    
    public boolean isValidNumber(String sNumber)
    {
        try
        {
            int no = Integer.parseInt(sNumber);
            return true;
        }
        catch(Exception e)
        {
            return false;
        }
    }
    
    

    public void forwardToPage(HttpServletRequest req, HttpServletResponse res, String S_URL)
    {
        try
        {
                res.sendRedirect(S_URL);        
                return;
        }
        catch(Exception e)
        {
            logger.error(e); 
        }
    }

    public static String noNullReplaceCodes(String value, boolean htmlspace)
    {
        value = noNull(value,htmlspace);
        value = replaceString(value, "\n","<BR>",true);
        return value;
    }
    
    public static String noNull(Object value, boolean htmlspace)
    {
        String result = noNull(value);
        
        if (htmlspace)
        {
            if (result.equals(""))
            result = "&nbsp;";
        }
        return result.trim();
    }
    
    public String printIfNotNull(String value, String printValue)
    {
        if (!isNull(value)) return printValue;
        return "";
    }
    
    public static boolean isNull(Object value)
    {
        if (value == null) return true;
        String svalue = noNull(value, false);
        if (svalue.trim().length() > 0)
        {
            return false;
        }
        return true;
    }
    
    public void writeToSession(HttpServletRequest req, HttpServletResponse res, String SessVar, Object value)
    {
        if (!isNull(value))
        {
            HttpSession session = req.getSession(true);
            session.putValue(SessVar, value);
        }
        else
        {
            HttpSession session = req.getSession(true);
            session.removeValue(SessVar);
        }
    }
    
    public String getFromSession(HttpServletRequest req, HttpServletResponse res, String SessVar)
    {
        String resultMessage = "";
        
        HttpSession session = req.getSession(true);
        Object O_Message = session.getValue(SessVar);
        if (O_Message == null) return "";
        
        String Message = O_Message.toString();
        if (Message == null) return "";
        if (Message.trim().length() < 1) return "";
        
        resultMessage = Message;
        return resultMessage;
    }
    
	public static String  replaceString(String Source, String Old, String New, boolean repeat)
	{
		if (Source == null) return "";
		if (Old == null) return Source;
		if (New == null) return Source;
		if (Old.equals(New)) return Source;

		int index = Source.indexOf(Old);
		if (index < 0) return Source;

		String firstPart = Source.substring(0,index);
		String finalPart = Source.substring(index + Old.length());

		if (repeat)
		{
			if ((finalPart != null)&&(finalPart.length() > 0))
				return (firstPart + New + replaceString(finalPart, Old, New, repeat));
		}
		return (firstPart + New + finalPart);
	}
    

    public static String noNullNoDecode(Object value)
    {
        if (value == null) return "";
        String svalue = value + "";
        if (svalue == null) return "";
        return svalue.trim();
    }

    public static String noNull(Object value)
    {
        if (value == null) return "";
        return noNull(value + "");
    }

    public static String noNull(String value)
    {
        if (value == null) return "";
        value = replaceString(value, "\"", "&quot;", true);
        return value.trim();
    }
    
    public static String noNull(String value, String defaultValue)
    {
    	String result = noNull(value);
    	if (result.length() < 1) return defaultValue;
    	return result;
    }    

    public void setActionMessage(HttpServletRequest req, HttpServletResponse res, String MessageCode, String Message)
    {
        HttpSession session = req.getSession(true);
        session.putValue(MessageCode, Message);
    }

    public void clearActionMessage(HttpServletRequest req, HttpServletResponse res, String MessageCode)
    {
        HttpSession session = req.getSession(true);
        session.putValue(MessageCode, "");
    }


    public String printAndClearActionMessage(HttpServletRequest req, HttpServletResponse res)
    {
    	return printAndClearMessage(req, res, "AdminActionMessage");
    }
    
    public String printAndClearErrorMessage(HttpServletRequest req, HttpServletResponse res)
    {
    	return printAndClearMessage(req, res, "AdminActionMessageException");
    }    
    
        
    public String printAndClearMessage(HttpServletRequest req, HttpServletResponse res, String MessageCode)
    {
    	return printAndClearMessage(req, res, MessageCode, "red");
    }
    
    public String printAndClearMessage(HttpServletRequest req, HttpServletResponse res, String MessageCode, String colorCode)
    {
        String resultMessage = "";

        HttpSession session = req.getSession(true);
        Object O_Message = session.getValue(MessageCode);
        if (O_Message == null) return resultMessage;
        
        String Message = O_Message.toString();
        if (Message == null) return resultMessage;
        if (Message.trim().length() < 1) return resultMessage;
        
        resultMessage = Message;
        if (resultMessage.startsWith("E:"))
        {
        	if (StringUtils.isNullOrEmpty(colorCode))
        	{
        		colorCode = "red";
        	}
        	
        	resultMessage = "<font color=\"" + colorCode + "\">" + resultMessage.substring(2) + "</font>";
        }
        else
        if (resultMessage.startsWith("S:"))
        {
        	resultMessage = "<font color=\"blue\"><b>" + resultMessage.substring(2) + "</b></font>";
        }     
        clearActionMessage(req, res, MessageCode);

        return resultMessage;
    }
    
    public boolean hasActionMesageToBePrinted(HttpServletRequest request, HttpServletResponse response)
    {
    	String mesg = getActionMessage(request, response,"AdminActionMessage");
    	return (!StringUtils.isNullOrEmpty(mesg));
    }
    
    public String getActionMesageTitle(HttpServletRequest request, HttpServletResponse response)
    {
    	String mesg = getActionMessage(request, response,"AdminActionMessage");
    	
    	if (StringUtils.isNullOrEmpty(mesg)) return "";
    	if (mesg.startsWith("E:")) return "ERROR";
    	return "RESULT";
    }    

    public String getActionMessage(HttpServletRequest req, HttpServletResponse res, String MessageCode)
    {
        String resultMessage = "";
        
        HttpSession session = req.getSession(true);
        Object O_Message = session.getValue(MessageCode);
        if (O_Message == null) return resultMessage;
        
        String Message = O_Message.toString();
        if (Message == null) return resultMessage;
        if (Message.trim().length() < 1) return resultMessage;
        
        resultMessage = Message;
        return resultMessage;
    }

    public StringBuffer printSelectCodeForDoubles(StringBuffer sb, Vector aVectorList, String selectFieldName, String defaultValue, String eventCode)
    {
            sb.append("<SELECT NAME=\"" + selectFieldName + "\" " + eventCode + " >");
            for (int index = 0; index < aVectorList.size(); index++)
            {
                if (defaultValue == null)
                {
                    sb.append("<OPTION VALUE=\"" + getElementAt(aVectorList, index, 0)  + "\">" + getElementAt(aVectorList, index, 1) + "</OPTION>\n");
                }
                else
                {
                    if (getElementAt(aVectorList, index, 0).equals(defaultValue))
                    {
                        sb.append("<OPTION VALUE=\"" + getElementAt(aVectorList, index, 0) + "\" SELECTED>" + getElementAt(aVectorList, index, 1) + "</OPTION>\n");
                    }
                    else
                    {
                        sb.append("<OPTION VALUE=\"" + getElementAt(aVectorList, index, 0) + "\">" + getElementAt(aVectorList, index, 1) + "</OPTION>\n");
                    }
                }
            }
            sb.append("</SELECT>");
            return sb;
    }

    public StringBuffer printSelectMultipleCodeForDoubles(StringBuffer sb, Vector aVectorList, String selectFieldName, String[] defaultValue, String eventCode)
    {
            sb.append("<SELECT NAME=\"" + selectFieldName + "\" " + eventCode + " >");
            for (int index = 0; index < aVectorList.size(); index++)
            {
                if (defaultValue == null)
                {
                    sb.append("<OPTION VALUE=\"" + getElementAt(aVectorList, index, 0)  + "\">" + getElementAt(aVectorList, index, 1) + "</OPTION>\n");
                }
                else
                {
                    if (isWordExistingInArray(defaultValue, getElementAt(aVectorList, index, 0)))
                    {
                        sb.append("<OPTION VALUE=\"" + getElementAt(aVectorList, index, 0) + "\" SELECTED>" + getElementAt(aVectorList, index, 1) + "</OPTION>\n");
                    }
                    else
                    {
                        sb.append("<OPTION VALUE=\"" + getElementAt(aVectorList, index, 0) + "\">" + getElementAt(aVectorList, index, 1) + "</OPTION>\n");
                    }
                }
            }
            sb.append("</SELECT>");
            return sb;
    }
    
    public boolean isWordExistingInArray(String[] list, String value)
    {
        if (list == null) return false;
        if (value == null) return false;
        for (int index = 0; index < list.length; index++)
        {
            if (list[index] == null) continue;
            if (list[index].equals(value)) return true;
        }
        return false;
    }
    
//    String[] getWordsInLines(String SouceString)    

    public String addSearchCondition(String Condition, String Structure, String NullStructure, String columnName, String value)
    {
        String ResultCondition = "";
        if (!isNull(value))
        {
            Structure = replaceString(Structure, "$COLNAME$", columnName, true);
            Structure = replaceString(Structure, "$COLVAL$", value, true);        
            ResultCondition = Structure;
        }
        else
        {
            NullStructure = replaceString(NullStructure, "$COLNAME$", columnName, true);
            NullStructure = replaceString(NullStructure, "$COLVAL$", value, true);
            ResultCondition = NullStructure;            
        }
        return addSearchCondition(Condition, ResultCondition);
    }
    
    public String addSearchCondition(String Condition, String newCondition)
    {
        if (isNull(Condition))
            return newCondition;
        if (isNull(newCondition))
            return Condition;
        return Condition + "AND" + newCondition;
    }
    
    public String addLeadingZeros(String value)
    {
        if (value == null) value = "";
        for (int index = value.length(); index < 5; index++)
        {
            value = "0" + value;
        }
        return value;
    }
    
    public boolean isAlphaNumeric(int val)
    {
        if ((val >= 'A')&&(val <= 'Z')) return true;
        if ((val >= 'a')&&(val <= 'z')) return true;
        if ((val >= '0')&&(val <= '9')) return true;
        return false;
    }
    
    public String generateRandomString(int maxlength)
    {
        String result = "";
        while(true)
        {
            int randomNumber = (int)(Math.random() * 150);  //Generates a random number between 0-150
            if (!isAlphaNumeric(randomNumber)) continue;
            result = result + (char)randomNumber;
            if (result.length() >= maxlength) break;
        }
        return result;
    }        

    public String getSelectHeader(String selectName, int visibleRows, int pixels, boolean multiple, String eventCode)
    {
        String multiString = "";
        if (multiple)
        multiString = "MULTIPLE";

        String header = "<SELECT NAME=\"$SELECTNAME$\" SIZE = \"$VISIBLEROWS$\" WIDTH=\"$PIXELSIZE$\" STYLE=\"WIDTH:$PIXELSIZE$px\" $MULTIPLEYN$ $EVENTCODE$>";
        
        if (pixels == 0)
        {
        	header = "<SELECT NAME=\"$SELECTNAME$\" SIZE = \"$VISIBLEROWS$\"  $MULTIPLEYN$ $EVENTCODE$>";
        }
        header = replaceString(header,"$SELECTNAME$",selectName,true);
        header = replaceString(header,"$VISIBLEROWS$","" + visibleRows,true);
        header = replaceString(header,"$PIXELSIZE$","" + pixels,true);
        header = replaceString(header,"$MULTIPLEYN$",multiString,true);
        header = replaceString(header,"$EVENTCODE$",eventCode,true);
        return header;
    }
    
    public StringBuffer getSelectListString(StringBuffer sb, Vector aVectorList, String defaultValue, String selectName, int visibleRows, int pixels, boolean multiple, String eventCode)
    {
    	String[] defaultval = new String[1];
    	defaultval[0] = defaultValue;
    	if (defaultValue == null) defaultval = null;
    	return getSelectList(sb, aVectorList, defaultval, selectName, visibleRows, pixels, multiple, eventCode);
    }
    
    public Map getStaticMap(String fieldName)
    throws Exception
    {
    	LIDAO dao = new LIDAO();
    	Map  mapContent = dao.loadMap("select distinct data_value, trim(data_text) data_text  from static_data where ((data_type = '" + fieldName + "')AND(STATUS = '1')) ORDER BY sequence_code,data_text", "data_value", "data_text", false);
    	return mapContent;
    }

    public StringBuffer getStaticSelectList(String fieldName, String dataTypeVector, String inputSelectedValue, int visibleRows, int pixels, String eventCode, boolean emptyRecord)
    throws Exception
    {
    	LIDAO dao = new LIDAO();
    	Vector listContent = dao.loadDoubleColumn("select distinct data_value, trim(data_text) data_text from static_data where ((data_type = '" + dataTypeVector + "')AND(STATUS = '1')) ORDER BY data_text", "data_value", "data_text", emptyRecord);    	
    	String[] selectValue = {""};
    	selectValue[0] = inputSelectedValue;
    	return getSelectList(new StringBuffer(), listContent, selectValue, fieldName, visibleRows, pixels, false, eventCode);
    }

    public StringBuffer getSelectList(StringBuffer sb, Vector aVectorList, String[] defaultValue, String selectName, int visibleRows, int pixels, boolean multiple, String eventCode)
    {
        sb.append(getSelectHeader(selectName, visibleRows, pixels, multiple, eventCode));
        
        for (int index = 0; index < aVectorList.size(); index++)
        {
            String selectedString = "";
            if (defaultValue != null)
            {
                if (isWordExistingInArray(defaultValue, getElementAt(aVectorList, index, 0)))
                {
                    selectedString = "SELECTED";
                }
            }            
            sb.append("<OPTION VALUE=\"" + getElementAt(aVectorList, index, 0) + "\" " + selectedString + ">" + getElementAt(aVectorList, index, 1) + "</OPTION>\n");
        }
        sb.append("</SELECT>");

        return sb;
    }
    
    public String getInitCap(String value)
    {
        if (value == null) return null;
        value = value.toLowerCase().trim();
        char[] chars = value.toCharArray();         
        boolean setNextCharacterToUpper = true;
        for (int index = 0; index < chars.length; index++)
        {
            if (setNextCharacterToUpper) chars[index] = Character.toUpperCase(chars[index]);
            if (Character.isWhitespace(chars[index]))
            setNextCharacterToUpper = true;
            else setNextCharacterToUpper = false;
        }
        String result = new String(chars);
        return result;
    }
    

	public String getAuthenticationActionMessage(HttpServletRequest req, HttpServletResponse res)
    {
        HttpSession session = req.getSession(true);
        Object messageObject = session.getAttribute("AuthenticateActionMessage");
        if (messageObject == null) return "";
        return messageObject.toString();
    }

	
	public boolean checkMandatoryParam(HttpServletRequest req, HttpServletResponse res, String parameter, String fieldTitle)
	throws Exception
    {
		if (StringUtils.isNullOrEmpty(req.getParameter(parameter)))
		{
			setActionMessage(req, res, "AdminActionMessage", "E:" + fieldTitle + " is Mandatory");
			return false;    	
		}
		return true;
    }
	
	public boolean checkNullObject(HttpServletRequest req, HttpServletResponse res, Object obj, String errorMessage)
	throws Exception
    {
		if (obj == null)
		{
			setActionMessage(req, res, "AdminActionMessage", "E:" + errorMessage);
			return false;
		}
		return true;
    }	
	
	
	
    public void clearAuthenticMessage(HttpServletRequest req, HttpServletResponse res)
    {
        HttpSession session = req.getSession(true);
		session.setAttribute("AuthenticateActionMessage", "");
    }
    
	public String getStatusMessage(HttpServletRequest req, HttpServletResponse res)
    {
        HttpSession session = req.getSession(true);
        Object messageObject = session.getAttribute("StatusMessage");
        if (messageObject == null) return "";
        return messageObject.toString();
    }
	
	public void clearStatusMessage(HttpServletRequest req, HttpServletResponse res)
    {
        HttpSession session = req.getSession(true);
		session.setAttribute("StatusMessage", "");
    }
	
	public String getFeeAsCurrency(String fee)
	{
		String regAmount = formatDecimal(fee + "",false);
		return "Rs." + regAmount + "/-";
	}
	
	public String getFormattedDateTime(String datetime)
	{
		try
		{
			if (isNull(datetime)) return "";
			DateUtils fd = new DateUtils();
			return fd.getFormattedDate(datetime, "yyyyMMddHHmms", "dd MMM yyyy HH:mm:ss");
		}
		catch(Exception e)
		{
			return "";
		}
	}

	public String formatReportInputDate(String datetime)
	{
		try
		{
			if (isNull(datetime)) return "";
			DateUtils fd = new DateUtils();
			return fd.getFormattedDate(datetime, "dd/MM/yyyy", "yyyyMMdd");
		}
		catch(Exception e)
		{
			return "";
		}
	}
	
	public String formatIndianDate(String datetime)
	{
		try
		{
			if (isNull(datetime)) return "";
			DateUtils fd = new DateUtils();
			return fd.getFormattedDate(datetime, "yyyyMMdd", "dd/MM/yyyy");
		}
		catch(Exception e)
		{
			return "";
		}
	}		
	
	public String getFormattedDate(String datetime)
	{
		try
		{
			if (isNull(datetime)) return "";
			DateUtils fd = new DateUtils();
			return fd.getFormattedDate(datetime, "yyyyMMddHHmms", "dd-MMM-yyyy");
		}
		catch(Exception e)
		{
			return "";
		}
	}
	
    public String getSystemDateMMDDYYYYHHMISS()
    {
        String OutputFormat = "yyyyMMddHHmms";
        return (new DateUtils()).getSystemDate(OutputFormat);
    }
	
	public String getReportDate(String datetime)
	{
		try
		{
			if (isNull(datetime)) return "";
			DateUtils fd = new DateUtils();
			return fd.getFormattedDate(datetime, "MM/dd/yyyy", "dd-MMM-yyyy");
		}
		catch(Exception e)
		{
			return "";
		}
	}	
	
	public String getSessionValue(HttpSession session, String attribute)
	{
		if (session == null) return "";
		String value = noNull(session.getAttribute(attribute));
		return noNull(value);
	}
	
	public String[] getSessionValueArray(HttpSession session, String attribute)
	{
		if (session == null) return null;
		String value = noNull(session.getAttribute(attribute));
		if (isNull(value)) return null;
		
		String[] result = {""};
		result[0] = value;
		return result;
	}
	
    public Vector getHourList()
    {
    	Vector resultVector = new Vector();
    	for (int h=0;h<=23;h++)
    	{
    		String val = "" + h;
    		if (h<=9)
    		val = "0" + h;

        	Vector aVector = new Vector();
            aVector.addElement(val);
            aVector.addElement(val);
            resultVector.addElement(aVector);
    	}
    	return resultVector;
    }
    public Vector getMinList()
    {
    	Vector resultVector = new Vector();
    	for (int h=0;h<=55;h+=5)
    	{
    		String val = "" + h;
    		if (h<=9)
    		val = "0" + h;

        	Vector aVector = new Vector();
            aVector.addElement(val);
            aVector.addElement(val);
            resultVector.addElement(aVector);
    	}
    	return resultVector;
    }    

   
    
    public boolean isOTPValid(HttpServletRequest request, String sessionValueCode, String inputOTP)
    {
    	try
    	{
    		return true;
    	}
    	catch(Exception e)
    	{
    		logger.error(e);
    		return false;
    	}
    }    
    
    public void appendRequestParameterMapToSession(HttpServletRequest request, HttpServletResponse response)
    {
    	HashMap loadRequestParameterMapFromRequest = loadRequestParameterMap(request, response);
    	
    	LogUtils.println("Request Parameter Map is:" + loadRequestParameterMapFromRequest);
    	HttpSession session = request.getSession(true);
    	
    	HashMap loadRequestParameterMapFromSession = new HashMap();
    	Object value = session.getAttribute("SessionParameterMap");
    	if (value!=null) loadRequestParameterMapFromSession= (HashMap)value;

    	Iterator iterator = loadRequestParameterMapFromRequest.keySet().iterator();
    	while(iterator.hasNext())
    	{
    		String key = iterator.next().toString();
    		String val = StringUtils.noNull(loadRequestParameterMapFromRequest.get(key));
    		loadRequestParameterMapFromSession.put(key, val);
    	}
    	session.setAttribute("SessionParameterMap", loadRequestParameterMapFromSession);
    }
    
    public HashMap loadRequestParameterMap(HttpServletRequest request, HttpServletResponse response)
    {
        HashMap resultMap = new HashMap();
      
    	Map<String, String[]> requestParameterMap = request.getParameterMap();
        for(String parameter : requestParameterMap.keySet()) 
        {
                String[] values = requestParameterMap.get(parameter);
                resultMap.put(parameter, values[0]);
        }
        return resultMap;
    }

    public Map getContentMapForCode(String code)
    {
    	try
    	{
	    	String query = "SELECT DISTINCT CVALUE ID, CVALUE TXT FROM CONTENT_MAP WHERE (CTYPE = '$CCATG$') AND (RSTATUS = '1') ORDER BY CSEQ";

	    	LIDAO dao = new LIDAO();
	    	if (dao.isOracleDatabase())
	    	{
	    		query = "SELECT DISTINCT CVALUE ID, CSEQ, CVALUE TXT FROM CONTENT_MAP WHERE (CTYPE = '$CCATG$') AND (RSTATUS = '1')  ORDER BY CSEQ";
	    	}
	    	query = StringUtils.replaceString(query, "$CCATG$", code, true);

	    	return dao.loadMap(query, "ID", "TXT", false);
    	}
    	catch(Exception e)
    	{
    		logger.error(e);
    		return new HashMap();
    	}
    }
    
    public StringBuffer getSelectListByMap(StringBuffer sb, Map map, String defaultValue, String selectName, int visibleRows, int pixels, boolean multiple, String eventCode)
    {
    	sb.append(getSelectHeader(selectName, visibleRows, pixels, multiple, eventCode));
        defaultValue = StringUtils.noNull(defaultValue);
        Iterator iterator = map.keySet().iterator();
        int counter = 1;

        boolean foundValue = false;
        while(iterator.hasNext())
        {
        	String key = StringUtils.noNull(iterator.next());

        	if (defaultValue.equals(key))
        	{
        		foundValue = true;
        	}
        }
        
        iterator = map.keySet().iterator();
        while(iterator.hasNext())
        {
        	String key = StringUtils.noNull(iterator.next());
        	String value = StringUtils.noNull(map.get(key));
        	
        	String selectedValue = "";
        	
        	if (defaultValue.equals(key)) selectedValue = "SELECTED";
        	
        	if (!foundValue)
        	{
        		if (counter == 1)
        		{
        			selectedValue = "SELECTED";
        		}
        	}

        	String format = "<OPTION VALUE=\"$KEY$\" $SELECTEDKEY$>$VALUE$</OPTION>\n";
        	format = StringUtils.replaceString(format, "$KEY$", key, true);
        	format = StringUtils.replaceString(format, "$VALUE$", value, true);
        	format = StringUtils.replaceString(format, "$SELECTEDKEY$", selectedValue, true);
        	sb.append(format);
        	counter++;
        }
        sb.append("</SELECT>");

        return sb;
    }
    
    public void recordConsoleAccess(HttpServletRequest request, HttpSession session, String channel)
    {
    	try
    	{
            String remoteAddr = getIPAddress(request);
            String page = request.getRequestURI();
            String sessionID = session.getId();

            LILoggedInUserUtilitiesBean loggedBean = new LILoggedInUserUtilitiesBean();
            String userID = StringUtils.noNull(loggedBean.getLoggedInUserId(request, null));

            if (StringUtils.isNullOrEmpty(userID)) return;

            loggedBean.updateLastAccTS(sessionID, userID);

            LIConsoleaccesslogService accessServie = new LIConsoleaccesslogService();
            LIConsoleaccesslogRecord accessRecord = new LIConsoleaccesslogRecord();
            accessRecord.setUserid(userID);
            accessRecord.setChannel(channel);            
            accessRecord.setSource(remoteAddr);
            accessRecord.setPagepath(page);
            accessRecord.setSessionid(sessionID);
            accessRecord.setCreatedat(DateUtils.getCurrentDateTime());
            accessServie.insertLIConsoleaccesslogRecord(accessRecord);
    	}
    	catch(Exception e)
    	{
    		logger.error(e);
    	}
    }

	private static final Charset UTF8_CHARSET = Charset.forName("UTF-8");

	static String  decodeUTF8(byte[] bytes) {
	    return new String(bytes, UTF8_CHARSET);
	}

	static byte[] encodeUTF8(String string) {
	    return string.getBytes(UTF8_CHARSET);
	}
}
