package com.leadics.utils;

import org.apache.commons.codec.binary.Base64;
public class LIAppSecurityUtil
{
	public String encodeToBase64String(String orig)
	{
		byte[] encoded = Base64.encodeBase64(orig.getBytes());
		return new String(encoded);
	}
	
	public String decodeBase64String(String encoded)
	{
		byte[] decoded = Base64.decodeBase64(encoded.getBytes());
		return new String(decoded);
	}
	
	public String encryptStringToServer(String orig)
	{
		String appVersionCode = "a12"; //3 chars
		String custCode = "c1b"; //3 chars
		String encryptionTypeCode = "e1b"; //3 chars
		String appender = "a="; //2 chars

		LogUtils.println("Original String:" + orig);
		String encrypted = encodeToBase64String(orig);
		LogUtils.println("Base64Encoding:" + encrypted);
		String result = "==" + appVersionCode + encryptionTypeCode + encrypted + custCode + appender;
		LogUtils.println("Result        :" + result);
		
		return result;
	}

	public String getAppVersionCode(String encrypted)
	{
		if (!encrypted.startsWith("==")) return "";
		String appVersionCode = encrypted.substring(2,5);
		return appVersionCode;
	}
	
	public String getEncryptionTypeCode(String encrypted)
	{
		if (!encrypted.startsWith("==")) return "";
		String appVersionCode = encrypted.substring(5,8);
		return appVersionCode;
	}	
	
	public String getDecryptedText(String encrypted)
	{
		if (!encrypted.startsWith("==")) return encrypted;
		String messageText = encrypted.substring(8);
		int maxLen = messageText.length();
		messageText = messageText.substring(0, maxLen - 5);
		messageText = decodeBase64String(messageText);
		return messageText;
	}
	
	public String getCustCode(String encrypted)
	{
		if (!encrypted.startsWith("==")) return "";
		String messageText = encrypted.substring(8);
		int maxLen = messageText.length();
		messageText = messageText.substring(maxLen - 5);
		messageText = messageText.substring(0,3);
		return messageText;
	}

	public static void main(String args[]) 
    		throws Exception 
    {
		LIAppSecurityUtil secUtil = new LIAppSecurityUtil();
		String encryptedString = secUtil.encryptStringToServer("This is plain text");
//		LogUtils.println(secUtil.getAppVersionCode(encryptedString));
//		LogUtils.println(secUtil.getEncryptionTypeCode(encryptedString));
//		LogUtils.println(secUtil.getDecryptedText(encryptedString));
//		LogUtils.println(secUtil.getCustCode(encryptedString));
    }
}
