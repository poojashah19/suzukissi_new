package com.leadics.utils;

// Java imports
public interface LIConstants
{
	final public static String STATUS_INACTIVE="0";
	final public static String STATUS_ACTIVE="1";
	final public static String STATUS_BLOCKED="2";
	final public static String STATUS_PENDING_SUBMISSION="3";
	final public static String STATUS_PENDING_APPROVAL="4";
	final public static String STATUS_DENIED_APPROVAL="5";
	final public static String STATUS_BLOCKED_FAILED_LOGIN="6";

	final static public String YES = "1";
	final static public String NO = "0";
	

	final static public String TO_BE_SENT = "0";
	final static public String SENT = "1";	
	final static public String SENT_FAIL = "2";
	final static public String IN_PROGRESS = "3";
	
	
	final static public String STATUS_REPORT_PENDING_GENERATION = "0";
	final static public String STATUS_REPORT_GENERATING = "1";	
	final static public String STATUS_REPORT_GENERATED = "2";
	final static public String STATUS_REPORT_FAILED = "3";	
}
