package com.leadics.utils;
import java.util.*;
import org.slf4j.*;
public class LogUtils
{
    /**
     * Internal logger object
     */
    private Logger logger = null;

    /**
     * Time Logger
     */
    private Logger timeLogger = null;

    /**
     * The DEBUG Level designates fine-grained informational events that
     * are most useful to debug an application
     */
    public static final int DEBUG = 1;

    /**
    The INFO level designates informational messages that highlight the
    progress of the application at coarse-grained level.
    */
    public static final int INFO = 2;

    /**
    The WARN level designates potentially harmful situations
    */
    public static final int WARN = 3;

    /**
    The ERROR level designates error events that might still allow the
    application to continue running
    */
    public static final int ERROR = 4;

    /**
     * The FATAL level designates very severe error events that will
     * presumably lead the application to abort.
     */
    public static final int FATAL = 5;

    /**
     * Constructor method that creates internal logger object
     * and timeLoggerObject
     * @param inputClass Calling Class
     */
    public LogUtils(String inputClassName)
    {
        String timerLogName = null;

        //Create logger
        logger = LoggerFactory.getLogger(inputClassName);

        //Time logger
        timerLogName = inputClassName;

        if (timerLogName.indexOf('.') > -1)
        {
            timerLogName =
                timerLogName.substring(
                timerLogName.lastIndexOf('.') + 1);
        }

        timeLogger =
        		LoggerFactory.getLogger("timer." + timerLogName);
    }

    /**
     * Logs message with specified level
     * @param message Message to be logged
     * @param level Message level
     * @throws IllegalArgumentException On Invalid message input
     */
    public void log(StringBuffer messageBuffer, int level)
        throws IllegalArgumentException
    {
        //Validate message
        if (messageBuffer == null)
        {
            throw new IllegalArgumentException(
                "Message cannot be null" + " in LogUtils.log method");
        }

        log(messageBuffer.toString(), level);
    }

    public static void printlnMust(String message)
    {
    	{
    		System.out.println(message);
    	}
    }
    
    public static void impprintln(String message)
    {
    	println("IMP:" + message);
    }

    public static void println(String message)
    {
    	String DebugLogEnabled = StringUtils.noNull(PropertyUtil.getProperty("DebugLogEnabled"));
    	if (DebugLogEnabled.equals("Y"))
    	{
    		System.out.println(message);
    	}
    	else
    	{
    		if (isToBePrinted(message))
    		{
    			System.out.println(message);	
    		}
    	}
    }

    private static boolean isToBePrinted(String message)
    {
		message = StringUtils.noNull(message);
		if (message.toLowerCase().contains("err")) return true;
		if (message.toLowerCase().contains("exception")) return true;
		if (message.toLowerCase().contains("stack")) return true;
		if (message.toLowerCase().contains("imp")) return true;
		return false;
    }

    public static void println(Object message)
    {
    	if (message == null) return;
    	println(message.toString());
    }    

    public static void println()
    {
    	println("");
    }     

    /**
     * Logs message with specified level
     * @param message Message to be logged
     * @param level Message level
     * @throws IllegalArgumentException On Invalid message input
     */
    public void log(String message, int level)
        throws IllegalArgumentException
    {
        //Validate message
        if (message == null)
        {
            throw new IllegalArgumentException(
                "Message cannot be null" + " in LogUtils.log method");
        }

        //Validate level
        if (level < DEBUG)
        {
            throw new IllegalArgumentException(
                "Invalid log level in" + " LogUtils.log method");
        }

        //Validate level
        if (level > FATAL)
        {
            throw new IllegalArgumentException(
                "Invalid log level in" + " LogUtils.log method");
        }

        try
        {
            if (level == DEBUG)
            {
                debug(message);
            }
            if (level == INFO)
            {
                info(message);
            }
            if (level == WARN)
            {
                warn(message);
            }
            if (level == ERROR)
            {
                error(message);
            }
            if (level == FATAL)
            {
            	error(message);
            }
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }

    /**
     * Logs message with DEBUG Level
     * @param message Message to be logged
     * @param logger Logger
    */
    public void debug(String message)
    {
        if (!logger.isDebugEnabled()) return;
    	logger.debug(message);
    }
    
    public void fatal(String message)
    {
        if (!logger.isErrorEnabled()) return;
    	logger.error(message);
    }
    
    /**
     * Logs message with INFO Level
     * @param message Message to be logged
     * @param logger Logger
     */
    public void info(String message)
    {
        if (!logger.isInfoEnabled()) return;
        logger.info(message);
    }

    /**
     * Logs message with WARN Level
     * @param message Message to be logged
     * @param logger Logger
     */
    public void warn(String message)
    {
        if (!logger.isWarnEnabled()) return;
        logger.warn(message);
    }

    /**
     * Logs message with ERROR Level
     * @param message Message to be logged
     * @param logger Logger
     */
    public void error(String message)
    {
        if (!logger.isErrorEnabled()) return;
    	logger.error(message);
    }
    
    public void error(String errMesg, Exception e)
    {
        if (!logger.isErrorEnabled()) return;
    	logger.error(errMesg, e);
    }
    
    public void error(Exception e)
    {
        if (!logger.isErrorEnabled()) return;
        e.printStackTrace();
    	logger.error("", e);
    }
    
    public void error(Throwable e)
    {
        if (!logger.isErrorEnabled()) return;
        e.printStackTrace();
    	logger.error("", e);
    }

    /**
     * Logs message with Trace Level
     * @param message Message to be logged
     * @param logger Logger
     */
    public void trace(String message)
    {
        if (!logger.isTraceEnabled()) return;
        logger.trace(message);
    }

    /**
     * Logs message with DEBUG Level
     * @param message Message to be logged
     * @param logger Logger
    */
    public void debugTimer(String message)
    {
        if (!timeLogger.isDebugEnabled()) return;
        timeLogger.debug(message);
    }

    /**
     * Logs message with INFO Level
     * @param message Message to be logged
     * @param logger Logger
     */
    public void infoTimer(String message)
    {
        if (!timeLogger.isInfoEnabled()) return;
        timeLogger.info(message);
    }

    /**
     * Logs message with WARN Level
     * @param message Message to be logged
     * @param logger Logger
     */
    public void warnTimer(String message)
    {
        if (!timeLogger.isWarnEnabled()) return;
        timeLogger.warn(message);
    }

    /**
     * Logs message with ERROR Level
     * @param message Message to be logged
     * @param logger Logger
     */
    public void errorTimer(String message)
    {
        if (!timeLogger.isErrorEnabled()) return;
        timeLogger.error(message);
    }

    /**
     * Logs message with Trace Level
     * @param message Message to be logged
     * @param logger Logger
     */
    public void traceTimer(String message)
    {
        if (!timeLogger.isTraceEnabled()) return;
        timeLogger.trace(message);
    }

    /**
    * Creates a string with the following information in tabs.
    * Function Name, UniqueID and current time
    * in format yyyy-MM-dd HH:mm:ss.SSS
    *
    * @param functionName Function Name
    * @param scope Function Name
    * @param uniqueID Unique ID. If Unique ID passed is null,
    * it will be replaced with a space by this method
    * @return         String with the following information in tabs.
    *                 Function Name, UniqueID and current Time in
    * format yyyy-MM-dd HH:mm:ss.SSS
    * @throws IllegalArgumentException for Invalid input
    */
    public String generateFunctionBeginTimerMessage(
        String functionName,
        String scope,
        String uniqueID)
        throws IllegalArgumentException
    {
        if (!timeLogger.isDebugEnabled())
        {
            return "";
        }

        //Validate Function name
        if (functionName == null)
        {
            throw new IllegalArgumentException(
                "Function name cannot be null in"
                    + " LogUtils.generateFunctionBeginTimerMessage");
        }

        //Validate Scope
        if (scope == null)
        {
            throw new IllegalArgumentException(
                "Scope cannot be null in"
                    + " LogUtils.generateFunctionBeginTimerMessage");
        }

        //Validate Unique ID
        if (uniqueID == null)
        {
            uniqueID = " ";
        }

        try
        {
            //Get current time
            String currentTime =
                DateUtils.getCurrentTime("yyyy-MM-dd HH:mm:ss.SSS");

            //Generate Message to log
            String messageToLog =
                functionName + "\t" + scope + "\t" +
                uniqueID + "\t" + currentTime;

            //Return message to log
            return messageToLog;
        }
        catch(Exception exception)
        {
            throw new IllegalArgumentException
                ("Failed to generate begin timer message:" +
                exception);
        }
    }

    /**
    * Creates a string with the following information in tabs.
    * Function Name, UniqueID and current time
    * in format yyyy-MM-dd HH:mm:ss.SSS
    *
    * @param functionName Function Name
    * @param uniqueID Unique ID. If Unique ID passed is null,
    * it will be replaced with a space by this method
    * @return         String with the following information in tabs.
    *                 Function Name, UniqueID and current Time in
    * format yyyy-MM-dd HH:mm:ss.SSS
    * @throws IllegalArgumentException for Invalid input
    */
    public String generateFunctionBeginTimerMessage(
        String functionName,
        String uniqueID)
        throws IllegalArgumentException
    {
        //Call begin timer message with scope
        return generateFunctionBeginTimerMessage(
            functionName,
            "Main",
            uniqueID);
    }

    /**
    * Input is the string generated by
    * generateFunctionBeginTimerMessage method.
    * Parses date-time value and calculates the difference
    * with current time,
    * and returns a final string in the following format.
    * Function Name, UniqueID start
    * Time in format yyyy-MM-dd HH:mm:ss.SSS,
    * end time in format yyyy-MM-dd HH:mm:ss.SSS difference in format
    * HH:mm:ss.SSS
    *
    * @param functionBeginTimerMessage String generated by
    * generateFunctionBeginTimerMessage method
    * @return         String with the following information
    * in tabs.
    *                 Function Name, UniqueID and current Time in
    * Format yyyy-MM-dd HH:mm:ss.SSS,
    *                 end time in format yyyy-MM-dd HH:mm:ss.SSS
    * difference in format HH:mm:ss.SSS
    * @throws IllegalArgumentException for exception
    */
    public String generateFunctionEndTimerMessage
        (String functionBeginTimerMessage)
        throws IllegalArgumentException
    {
        if (!timeLogger.isDebugEnabled())
        {
            return "";
        }

        if (functionBeginTimerMessage == null)
        {
            throw new IllegalArgumentException(
                "Invalid Begin Timer Message value for"
                    + " generateFunctionEndTimerMessage method");
        }

        try
        {
            String startTimeValue =
                StringUtils.getToken(functionBeginTimerMessage, "\t",
                3);

            String endTimeValue =
                DateUtils.getCurrentTime("yyyy-MM-dd HH:mm:ss.SSS");

            Date startTime =
                DateUtils.convertDate(
                    startTimeValue,
                    "yyyy-MM-dd HH:mm:ss.SSS");

            Date endTime =
                DateUtils.convertDate(
                    endTimeValue,
                    "yyyy-MM-dd HH:mm:ss.SSS");

            String timeDifference =
                DateUtils.getTimeDifference(startTime, endTime);

            //Create message to log
            String messageToLog =
                functionBeginTimerMessage
                + "\t"
                + endTimeValue
                + "\t"
                + timeDifference
                + "\t";

            infoTimer(messageToLog);

            return messageToLog;
        }
        catch(Exception exception)
        {
            throw new IllegalArgumentException
                ("Failed to generate end timer message:" + exception);
        }
    }

    /**
     * Determines if DEBUG level is enabled in logger object
     * @return true - If enabled; false - otherwise
     */
    public boolean isDebugEnabled()
    {
        return logger.isDebugEnabled();
    }

    /**
     * Determines if INFO level is enabled in logger object
     * @return true - If enabled; false - otherwise
     */
    public boolean isInfoEnabled()
    {
        return logger.isInfoEnabled();
    }
}
