package com.leadics.utils;

// Java imports
import java.util.ArrayList;
import java.util.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Vector;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.text.ParsePosition;

public class DateUtils
{
	/**
	 * The logger object instance for the class
	 */
	private static LogUtils logger =
		new LogUtils(DateUtils.class.getName());

    /**
     * The constant for the date format yyyyMMdd
     */
    public static final String YYYYMMDD = "yyyyMMdd";

    /**
    * Returns short default date string format i.e.
    * <code>yyyyMMdd</code>
    */
    protected static String getShortDefaultDateFormat()
    {
        return YYYYMMDD;
    }

    /**
    * Converts date object to date-time string
    * @param inputDate Date object to be converted
    * @param outputFormat Output format.
    * Refer to <code>java.text.SimpleDateFormat</code> for date format
    * codes
    * @return          Formatted date-time string
    * @throws IllegalArgumentException for Invalid input
    * @throws Exception for all others
    */
    public static String formatDate(
        java.util.Date inputDate,
        String outputFormat)
        throws IllegalArgumentException, Exception
    {
        //Validate input date value
        if (inputDate == null)
        {
            throw new IllegalArgumentException("Input date cannot "
                    + " be null in DateUtils.formatDate method");
        }

        //Validate output date format
        if (outputFormat == null)
        {
            throw new IllegalArgumentException("Output format cannot"
                    + " be null in DateUtils.formatDate method");
        }

        //Apply formatting
        SimpleDateFormat formatter = new SimpleDateFormat(outputFormat);
        return formatter.format(inputDate);
    }

    /**
    * Returns default date-time string format i.e.
    * <code>yyyyMMdd'T'HH:mm:ss</code>
    * @return Default date-time string i.e. yyyyMMdd'T'HH:mm:ss
    */
    protected static String getDefaultDateFormat()
    {
        //Yantra default date-time string format
        return "yyyyMMdd'T'HH:mm:ss";
    }


	/**
	* Returns default date-time string format i.e.
	* <code>yyyyMMdd'T'HH:mm:ss</code>
	* @return Default date-time string i.e. yyyyMMdd'T'HH:mm:ss
	*/
	protected static String getDefaultDateFormatISO()
	{
		//Yantra default date-time string format
		return "yyyy-MM-dd'T'HH:mm:ss";
	}


    /**
    * Converts date object to date-time string in
    * default date format
    *
    * @param inputDate Date object to be converted
    * @return          Date-time string in default date format
    * @throws IllegalArgumentException for Invalid input
    * @throws Exception for all others
    * @see getDefaultDateFormat
    */
    public static String convertDate(java.util.Date inputDate)
        throws IllegalArgumentException, Exception
    {
        return formatDate(inputDate, getDefaultDateFormat());
    }

    /**
    * Converts date-time string to Date object.
    * Date-time string should be in default date format
    *
    * @param inputDate Date-time string to be converted
    * @return          Equivalent date object to input string
    * @throws IllegalArgumentException for Invalid input
    * @throws Exception for all others
    */
    public static java.util.Date convertDate(String inputDate)
        throws IllegalArgumentException, Exception
    {

        //Validate input date value
        if (inputDate == null)
        {
            throw new IllegalArgumentException("Input date cannot "
                    + " be null in DateUtils.convertDate method");
        }
        if (inputDate.indexOf("T") != -1 && inputDate.indexOf("-") == -1)
        {
            return convertDate(inputDate, getDefaultDateFormat());
        }
        else if(inputDate.indexOf("T") != -1 && inputDate.indexOf("-") != -1)
        {

			return convertDate(inputDate, getDefaultDateFormatISO());
        }
        else
        {

            return convertDate(inputDate, getShortDefaultDateFormat());
        }
    }

    /**
    * Converts date-time string to Date object
    *
    * @param inputDate Date-time string to be converted
    * @param inputDateFormat Format of date-time string.
    * Refer to <code>java.util.SimpleDateFormat</code> for date
    * format codes
    * @return          Equivalent date object to input string
    * @throws IllegalArgumentException for Invalid input
    * @throws Exception for all others
    */
    public static java.util.Date convertDate(
        String inputDate,
        String inputDateFormat)
        throws IllegalArgumentException, Exception
    {
        //Validate Input Date value
        if (inputDate == null)
        {
            throw new IllegalArgumentException(
                "Input date cannot be null"
                    + " in DateUtils.convertDate method");
        }

        //Validate Input Date format
        if (inputDateFormat == null)
        {
            throw new IllegalArgumentException(
                "Input date format cannot"
                    + " be null in DateUtils.convertDate method");
        }

		//Apply formatting
        SimpleDateFormat formatter =
            new SimpleDateFormat(inputDateFormat);

        ParsePosition position = new ParsePosition(0);
        return formatter.parse(inputDate, position);
    }

    /**
    * Returns current date-time string in desired format
    *
    * @param outputFormat Desired output date-time format
    * @return          Current date-time string in desired format
    * @throws IllegalArgumentException for Invalid input
    * @throws Exception for all others
    */
    public static String getCurrentTime(String outputFormat)
        throws IllegalArgumentException, Exception
    {
        //Create current date object
        Date currentDateTime = new Date();

        //Apply formatting
        return formatDate(currentDateTime, outputFormat);
    }
    

    public static String getCurrentDateTimene()
    {
    	try
    	{
    		return getCurrentTime("yyyyMMddHHmmss");
    	}
    	catch(Exception e)
    	{
    		logger.error(e);
    		return "";
    	}
    }
    
    public static String getCurrentDateTime()
    throws Exception
    {
    	return getCurrentTime("yyyyMMddHHmmss");
    }
    
    public static String getCurrentDate()
    throws Exception
    {
    	return getCurrentTime("yyyyMMdd");
    }    
    
    public static String getLongDateFormat()
    {
    	return "yyyyMMddHHmmss";
    }
    
    
    
    
    

    /**
     * Adds leading zeros to given value until max length is reached
     */
    private static String addLeadingZeros(long value, int maxLength)
    {
        String result = Long.toString(value);

        int remaining = maxLength - result.length();
        for (int index = 0; index < remaining; index++)
        {
            result = "0" + result;
        }
        return result;
    }

    /**
    * Returns time difference between two date objects
    *
    * @param startTime Start time
    * @param endTime End time.
    * End time should be greater than Start time.
    * @return        Time difference in HH:mm:ss.SSS format
    * @throws IllegalArgumentException for Invalid input
    * @throws Exception for all others
    */
    public static String getTimeDifference(
        Date startTime,
        Date endTime)
        throws IllegalArgumentException, Exception
    {
        //Validate Start time
        if (startTime == null)
        {
            throw new IllegalArgumentException(
                "Start time cannot be"
                    + " null in DateUtils.getTimeDifference method");
        }

        //Validate End time
        if (endTime == null)
        {
            throw new IllegalArgumentException(
                "End time cannot be "
                    + "null in DateUtils.getTimeDifference method");
        }

        //Check whether start time is less than end time
        if (startTime.after(endTime))
        {
            throw new IllegalArgumentException(
                "End time should be greater than Start time in"
                    + "  DateUtils.getTimeDifference method");
        }

        long longStartTime = startTime.getTime();
        long longEndTime = endTime.getTime();

        //Get total difference in milli seconds
        long difference = longEndTime - longStartTime;

        long temp = difference;

        //Get milli seconds
        long milliseconds = temp % 1000;
        temp = temp / 1000;

        //Get seconds
        long seconds = temp % 60;
        temp = temp / 60;

        //Get Minutes
        long minutes = temp % 60;
        temp = temp / 60;

        //Get Hours
        long hours = temp;

        //Calculate result
        String result =
            addLeadingZeros(hours,2)
            + ":"
            + addLeadingZeros(minutes,2)
            + ":"
            + addLeadingZeros(seconds,2)
            + "."
            + addLeadingZeros(milliseconds,3);

        //Format result and return
        return result;
    }

    /**
    * Returns time difference between two date objects in hours
    *
    * @param startTime Start time
    * @param endTime End time.
    * End time should be greater than Start time.
    * @return        Time difference in hours
    * @throws IllegalArgumentException for Invalid input
    * @throws Exception for all others
    */
    public static long getTimeDifferenceInHours(
        Date startTime,
        Date endTime)
        throws IllegalArgumentException, Exception
    {
        //Validate Start time
        if (startTime == null)
        {
            throw new IllegalArgumentException(
                "Start time cannot be null in "
                    + " DateUtils.getTimeDifferenceInHours method");
        }

        //Validate End time
        if (endTime == null)
        {
            throw new IllegalArgumentException(
                "End time cannot be null in "
                    + "DateUtils.getTimeDifferenceInHours method");
        }

        //Check whether start time is less than end time
        if (startTime.after(endTime))
        {
            throw new IllegalArgumentException(
                "End time should be greater than Start time in"
                    + "  DateUtils.getTimeDifferenceInHours method");
        }

        long differenceInHours =
            (endTime.getTime() - startTime.getTime())/(1000 * 3600);

        //Return number of hours
        return differenceInHours;
    }


    /**
    * Returns difference between two dates in days. This method
    * does not consider timings for calculation.  Result can be
    * a positive or negative or zero value. This method results
    * the difference as an integer.
    *
    * @param startDate Start date
    * @param endDate End date
    * @return        Difference in dates
    * @throws IllegalArgumentException for Invalid input
    * @throws Exception for all others
    */
    public static int getDifferenceInDays(
            String startDate,
            String endDate)
            throws IllegalArgumentException, Exception
    {
    	Date sd = convertDate(startDate, DateUtils.getLongDateFormat());
    	Date ed = convertDate(endDate, DateUtils.getLongDateFormat());
    	return getDifferenceInDays(sd,ed);
    }

    public static int getDifferenceInDays(
        Date startDate,
        Date endDate)
        throws IllegalArgumentException, Exception
    {
        //Difference in days
        int differenceInDays = 0;

        //Validate Start date
        if (startDate == null)
        {
            throw new IllegalArgumentException(
                "Start date cannot be"
                    + " null in DateUtils.getDifferenceInDays method");
        }

        //Validate End date
        if (endDate == null)
        {
            throw new IllegalArgumentException(
                "End date cannot be "
                    + "null in DateUtils.getDifferenceInDays method");
        }

        //Calculate difference in days
        long difference =
            (endDate.getTime() - startDate.getTime())/(1000 * 86400);

        //Set sign
        differenceInDays = (int)difference;

        return differenceInDays;
    }

    /**
    * Adds specified interval to input date.
    * Valid values for Interval are Calendar.YEAR,
    * Calendar.MONTH, Calendar.DATE etc. See
    * Calendar API for more information
    * @param inputDate Input Date
    * @param interval Interval
    * @param amount Amount to add(use negative numbers
    * to subtract
    * @return Date after addition
    * @throws IllegalArgumentException for Invalid input
    * @throws Exception for all others
    */
    public static Date addToDate(
        Date inputDate,
        int interval,
        int amount)
        throws IllegalArgumentException, Exception
    {
        //Difference in days
        int differenceInDays = 0;

        //Validate Input date
        if (inputDate == null)
        {
            throw new IllegalArgumentException(
                "Input date cannot be"
                    + " null in DateUtils.addToDate method");
        }

        //Get instance of calendar
        Calendar calendar = Calendar.getInstance();

        //Set input date to calendar
        calendar.setTime(inputDate);

        //Add amount to interval
        calendar.add(interval, amount);

        //Return result date;
        return calendar.getTime();
    }
    
    /**
    * Parses year from the given date. Returns all dates in that year in YYYYMMDD format.
    * Input Date should in MM/DD/YYYY format.
    * @return List of Dates in YYYYMMDD format
    * @param S_Date   Input Date with Year Field
    * @param S_Format Input Date format per the codes in java.text.SimpleDateFormat
    * @exception Exception
    * @see java.text.SimpleDateFormat
    */
    public Vector getYearlyCalendar(String S_Date, String S_Format)
    throws Exception
    {
        String S_Year = getFormattedDate(S_Date, S_Format, "yyyy");
        int year = Integer.parseInt(S_Year);
        
        GregorianCalendar gc = new GregorianCalendar();
        gc.set(year, gc.JANUARY, 1);
        
        Vector anVectorYearCalendar = new Vector();
        
        while(true)
        {
            int calYear = gc.get(gc.YEAR);
            if (calYear != year) break;
            
            SimpleDateFormat outputYYYYMMDD = new SimpleDateFormat("yyyyMMdd");
            SimpleDateFormat outputDD = new SimpleDateFormat("dd");
            
            String OutputYYYYMMDD = outputYYYYMMDD.format(gc.getTime());
            String OutputDD = outputDD.format(gc.getTime());
            
            Vector anVector = new Vector();
            anVector.addElement(OutputYYYYMMDD);
            anVector.addElement(OutputDD);
            anVectorYearCalendar.addElement(anVector);
            
            gc.add(gc.DAY_OF_MONTH, 1);
        }
        return anVectorYearCalendar;
    }
    
    public boolean isLastDayInMonth(String S_Date, String InputFormat)
    {
        GregorianCalendar gc = new GregorianCalendar();
        SimpleDateFormat inputFormatter = new SimpleDateFormat(InputFormat);
        ParsePosition pos = new ParsePosition(0);
        gc.setTime(inputFormatter.parse(S_Date, pos));
        
        int InitialMonth = gc.get(gc.MONTH);
        gc.add(gc.DAY_OF_MONTH, 1);
        int LaterMonth = gc.get(gc.MONTH);
        
        return (InitialMonth != LaterMonth);
    }
    
    public boolean isSaturday(String S_Date, String InputFormat)
    {
        GregorianCalendar gc = new GregorianCalendar();
        SimpleDateFormat inputFormatter = new SimpleDateFormat(InputFormat);
        ParsePosition pos = new ParsePosition(0);
        gc.setTime(inputFormatter.parse(S_Date, pos));
        if (gc.get(gc.DAY_OF_WEEK) == gc.SATURDAY) return true;
        return false;
    }
    
    public int getFirstDayOfMonth(String S_Date, String InputFormat)
    {
        GregorianCalendar gc = new GregorianCalendar();
        SimpleDateFormat inputFormatter = new SimpleDateFormat(InputFormat);
        ParsePosition pos = new ParsePosition(0);
        gc.setTime(inputFormatter.parse(S_Date, pos));
        return gc.get(gc.DAY_OF_WEEK);
    }
    
    public String getSystemDate(String OutputFormat)
    {
        SimpleDateFormat outputFormatter = new SimpleDateFormat(OutputFormat);
        return outputFormatter.format(new Date());
    }

    public java.sql.Date getSQLDateObject(String S_Date, String InputFormat, java.sql.Date defaultDate)
    {
        try
        {
            return getSQLDateObject(S_Date, InputFormat);
        }
        catch(Exception e)
        {
            return defaultDate;
        }
    }

    public java.sql.Date getSQLDateObject(String S_Date, String InputFormat)
    throws Exception
    {
        if (S_Date == null) return null;
        if (S_Date.trim().length() < 1) return null;
        GregorianCalendar gc = new GregorianCalendar();
        SimpleDateFormat inputFormatter = new SimpleDateFormat(InputFormat);
        ParsePosition pos = new ParsePosition(0);
        gc.setTime(inputFormatter.parse(S_Date, pos));
        
        java.sql.Date sqldate = new java.sql.Date((gc.getTime()).getTime());
        return sqldate; 
    }
    
    public java.sql.Date getCurrentSQLDateObject()
    throws Exception
    {
    	java.sql.Date sqldate = new java.sql.Date(new java.util.Date().getTime());
    	return sqldate;
    }    

    public Date getDateObject(String S_Date, String InputFormat)
    throws Exception
    {
        if (S_Date == null) return null;
        GregorianCalendar gc = new GregorianCalendar();
        SimpleDateFormat inputFormatter = new SimpleDateFormat(InputFormat);
        ParsePosition pos = new ParsePosition(0);
        gc.setTime(inputFormatter.parse(S_Date, pos));
        return gc.getTime();
    }
    
    public static String getFullFormattedDateNE(String S_Date)
    {
    	try
    	{
    		return getFullFormattedDate(S_Date);
    	}
    	catch(Exception e)
    	{
    		logger.error(e);
    		return S_Date;
    	}
    }

    public static String getFullFormattedDate()
    throws Exception
    {
    	try
    	{
    		return getFullFormattedDate(DateUtils.getCurrentDateTime());
    	}
    	catch(Exception e)
    	{
    		return "";
    	}
    }
    
    public static String getFullFormattedDate(String S_Date)
    throws Exception
    {
    	try
    	{
    		if (S_Date == null) return "";
    		if (S_Date.trim().length() < 1) return "";

    		String InputFormat = "yyyyMMddHHmmss";
	    	String OutputFormat = "dd MMM yyyy HH:mm:ss";
	    	return getFormattedDate(S_Date, InputFormat, OutputFormat);
    	}
    	catch(Exception e)
    	{
    		return "";
    	}
    }
    public static String getFormattedDateNE(String S_Date, String InputFormat, String OutputFormat)
    {
    	try
    	{
    		String formattedDate = DateUtils.getFormattedDate(S_Date, InputFormat, OutputFormat);
    		return formattedDate;
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    		return "";
    	}
    }
    
    public static String getFormattedDate(String S_Date, String InputFormat, String OutputFormat)
    throws Exception
    {
        if (S_Date == null) return null;
        if (S_Date.length() < 1) return null;
        
        GregorianCalendar gc = new GregorianCalendar();

        SimpleDateFormat inputFormatter = new SimpleDateFormat(InputFormat);
        ParsePosition pos = new ParsePosition(0);
        gc.setTime(inputFormatter.parse(S_Date, pos));

        SimpleDateFormat outputFormatter = new SimpleDateFormat(OutputFormat);
        return outputFormatter.format(gc.getTime());
    }

    public boolean isFutureDate(String S_Date,  String InputFormat)
    throws Exception
    {
        SimpleDateFormat inputFormatter = new SimpleDateFormat(InputFormat);
        ParsePosition pos = new ParsePosition(0);
        Date idate  = inputFormatter.parse(S_Date, pos);
        Date sysdate = new Date();
        return idate.after(sysdate);
    }

    public static long getDifferenceInSeconds(String startTimeString,  String startTimeFormat, String endTimeString,  String endTimeFormat)
    throws Exception
    {
        SimpleDateFormat startTimeFormatter = new SimpleDateFormat(startTimeFormat);
        SimpleDateFormat endTimeFormatter = new SimpleDateFormat(endTimeFormat);
        Date startTime = startTimeFormatter.parse(startTimeString, new ParsePosition(0));
        Date endTime = endTimeFormatter.parse(endTimeString, new ParsePosition(0));
        long differenceInSeconds = (endTime.getTime() - startTime.getTime())/(1000);
        return differenceInSeconds;
    }
    
    public static int getDifference(String S_Date1,  String Input1Format, String S_Date2,  String Input2Format)
    throws Exception
    {
        SimpleDateFormat input1Formatter = new SimpleDateFormat(Input1Format);
        SimpleDateFormat input2Formatter = new SimpleDateFormat(Input2Format);
        ParsePosition pos1 = new ParsePosition(0);
        ParsePosition pos2 = new ParsePosition(0);

        Date idate1 = input1Formatter.parse(S_Date1, pos1);
        Date idate2 = input2Formatter.parse(S_Date2, pos2);

        GregorianCalendar gc = new GregorianCalendar();

        Date BigDate;

        int sign = 1;

        if (idate1.after(idate2))
        {
            sign = -1;
            gc.setTime(idate2);
            BigDate = idate1;
        }
        else
        {
            sign = 1;
            gc.setTime(idate1);
            BigDate = idate2;
        }

        int difference = 0;

        while(!gc.getTime().equals(BigDate))
        {
            gc.add(Calendar.DAY_OF_MONTH, 1);
            difference++;
        }

        difference = sign * difference;
        return difference;
    }

    public String getPreviousMonth(String S_Date, String InputFormat, String OutputFormat)
    throws Exception
    {
        GregorianCalendar gc = new GregorianCalendar();

        SimpleDateFormat inputFormatter = new SimpleDateFormat(InputFormat);
        ParsePosition pos = new ParsePosition(0);
        gc.setTime(inputFormatter.parse(S_Date, pos));

        gc.add(Calendar.MONTH, -1);

        SimpleDateFormat outputFormatter = new SimpleDateFormat(OutputFormat);
        return outputFormatter.format(gc.getTime());
    }

    public String getNextMonth(String S_Date, String InputFormat, String OutputFormat)
    throws Exception
    {
        GregorianCalendar gc = new GregorianCalendar();

        SimpleDateFormat inputFormatter = new SimpleDateFormat(InputFormat);
        ParsePosition pos = new ParsePosition(0);
        gc.setTime(inputFormatter.parse(S_Date, pos));

        gc.add(Calendar.MONTH, 1);

        SimpleDateFormat outputFormatter = new SimpleDateFormat(OutputFormat);
        return outputFormatter.format(gc.getTime());
    }

    public String getPreviousDate(String S_Date, String InputFormat, String OutputFormat)
    throws Exception
    {
        GregorianCalendar gc = new GregorianCalendar();

        SimpleDateFormat inputFormatter = new SimpleDateFormat(InputFormat);
        ParsePosition pos = new ParsePosition(0);
        gc.setTime(inputFormatter.parse(S_Date, pos));

        gc.add(Calendar.DAY_OF_MONTH, -1);

        SimpleDateFormat outputFormatter = new SimpleDateFormat(OutputFormat);
        return outputFormatter.format(gc.getTime());
    }

    public String getNextDate(String S_Date, String InputFormat, String OutputFormat)
    throws Exception
    {
        GregorianCalendar gc = new GregorianCalendar();

        SimpleDateFormat inputFormatter = new SimpleDateFormat(InputFormat);
        ParsePosition pos = new ParsePosition(0);
        gc.setTime(inputFormatter.parse(S_Date, pos));

        gc.add(Calendar.DAY_OF_MONTH, 1);

        SimpleDateFormat outputFormatter = new SimpleDateFormat(OutputFormat);
        return outputFormatter.format(gc.getTime());
    }

    public String getPreviousYear(String S_Date, String InputFormat, String OutputFormat)
    throws Exception
    {
        GregorianCalendar gc = new GregorianCalendar();

        SimpleDateFormat inputFormatter = new SimpleDateFormat(InputFormat);
        ParsePosition pos = new ParsePosition(0);
        gc.setTime(inputFormatter.parse(S_Date, pos));

        gc.add(Calendar.YEAR, -1);

        SimpleDateFormat outputFormatter = new SimpleDateFormat(OutputFormat);
        return outputFormatter.format(gc.getTime());
    }

    public String getNextYear(String S_Date, String InputFormat, String OutputFormat)
    throws Exception
    {
        GregorianCalendar gc = new GregorianCalendar();

        SimpleDateFormat inputFormatter = new SimpleDateFormat(InputFormat);
        ParsePosition pos = new ParsePosition(0);
        gc.setTime(inputFormatter.parse(S_Date, pos));

        gc.add(Calendar.YEAR, 1);

        SimpleDateFormat outputFormatter = new SimpleDateFormat(OutputFormat);
        return outputFormatter.format(gc.getTime());
    }
    
    public static String getPreviousWeek(String S_Date, String InputFormat, String OutputFormat)
    throws Exception
    {
        GregorianCalendar gc = new GregorianCalendar();

        SimpleDateFormat inputFormatter = new SimpleDateFormat(InputFormat);
        ParsePosition pos = new ParsePosition(0);
        gc.setTime(inputFormatter.parse(S_Date, pos));

        gc.add(Calendar.DAY_OF_MONTH, -7);

        SimpleDateFormat outputFormatter = new SimpleDateFormat(OutputFormat);
        return outputFormatter.format(gc.getTime());
    }
    
    public static String getPreviousDayCount(String S_Date, String InputFormat, String OutputFormat, int dayCount)
    throws Exception
    {
        GregorianCalendar gc = new GregorianCalendar();

        SimpleDateFormat inputFormatter = new SimpleDateFormat(InputFormat);
        ParsePosition pos = new ParsePosition(0);
        gc.setTime(inputFormatter.parse(S_Date, pos));

        gc.add(Calendar.DAY_OF_MONTH, -1 * dayCount);

        SimpleDateFormat outputFormatter = new SimpleDateFormat(OutputFormat);
        return outputFormatter.format(gc.getTime());
    }

    public static String getNextWeek(String S_Date, String InputFormat, String OutputFormat)
    throws Exception
    {
        GregorianCalendar gc = new GregorianCalendar();

        SimpleDateFormat inputFormatter = new SimpleDateFormat(InputFormat);
        ParsePosition pos = new ParsePosition(0);
        gc.setTime(inputFormatter.parse(S_Date, pos));

        gc.add(Calendar.DAY_OF_MONTH, 7);

        SimpleDateFormat outputFormatter = new SimpleDateFormat(OutputFormat);
        return outputFormatter.format(gc.getTime());
    }
    
    public static boolean isValidDateValue(String inputDate, String inputDateFormat)
    {
    	try
    	{
    		java.util.Date resultDate = convertDate(inputDate,inputDateFormat);
    		if (resultDate == null) return false;
        	return true;
    	}
    	catch(Exception exception)
    	{
    		return false;
    	}
    }
    
	public static String getYesterDate()
	throws Exception
	{
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, -1);
		String newDate = DateUtils.formatDate(cal.getTime(), DateUtils.getLongDateFormat());
		return newDate;
	}

	public static String getLastMonthCode()
	throws Exception
	{
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		String newDate = DateUtils.formatDate(cal.getTime(), "yyyyMM");
		return newDate;
	}
	
	public static String getShortYesterDate()
	throws Exception
	{
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, -1);
		String newDate = DateUtils.formatDate(cal.getTime(), DateUtils.getShortDefaultDateFormat());
		return newDate;
	}
	
	public static String getDayBeforeYesterDate()
	throws Exception
	{
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, -2);
		String newDate = DateUtils.formatDate(cal.getTime(), DateUtils.getLongDateFormat());
		return newDate;
	}	    
    

	public static String getFullDateDiffHours(int count)
	throws Exception
	{
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.HOUR_OF_DAY, count);
		String newDate = DateUtils.formatDate(cal.getTime(), DateUtils.getLongDateFormat());
		return newDate;
	}
	
	public static String getNextFullDate(String inputDateVal)
	throws Exception
	{
		Date inputDate = convertDate(inputDateVal, DateUtils.getLongDateFormat());
		Calendar cal = Calendar.getInstance();
		cal.setTime(inputDate);
		cal.add(Calendar.DAY_OF_MONTH, 1);
		return DateUtils.formatDate(cal.getTime(), DateUtils.getLongDateFormat());
	}
	
	public static boolean isFutureDate(String inputDateVal)
	throws Exception
	{
		Date inputDate = convertDate(inputDateVal, DateUtils.getLongDateFormat());
		Date currentDate = new java.util.Date();
		return inputDate.after(currentDate);
	}

	public static boolean isPastDate(String inputDateVal)
	throws Exception
	{
		Date inputDate = convertDate(inputDateVal, DateUtils.getLongDateFormat());
		Date currentDate = new java.util.Date();
		return currentDate.after(inputDate);
	}
	
	public static boolean isValidShortDate(String inputDateVal)
	throws Exception
	{
		try
		{
			if (inputDateVal == null) return false;
			if (inputDateVal.length() != DateUtils.getShortDefaultDateFormat().length()) return false;
			Date inputDate = convertDate(inputDateVal, DateUtils.getShortDefaultDateFormat());
			LogUtils.println(inputDateVal + ":" + inputDate);
			return true;
		}
		catch(Exception e)
		{
			return false;
		}
	}
	
	public static String mapToToday(String inputDateVal)
	throws Exception
	{
		Date inputDate = convertDate(inputDateVal, DateUtils.getLongDateFormat());
		Calendar today = Calendar.getInstance();
		Calendar otherday = Calendar.getInstance();
		otherday.setTime(inputDate);
		otherday.set(Calendar.DAY_OF_MONTH, today.get(Calendar.DAY_OF_MONTH));
		otherday.set(Calendar.YEAR, today.get(Calendar.YEAR));
		otherday.set(Calendar.MONTH, today.get(Calendar.MONTH));
		return formatDate(otherday.getTime(), DateUtils.getLongDateFormat());
	}
	
	
	public static String getWeekBeginning(String startDate)
	{
		try
		{
			Date startDateYYYYMMDD = DateUtils.convertDate(startDate, DateUtils.YYYYMMDD);
			Calendar cal = Calendar.getInstance();
			cal.setTime(startDateYYYYMMDD);
			int weekDeductor = cal.get(Calendar.DAY_OF_WEEK) - 1;
			cal.add(Calendar.DAY_OF_MONTH, (-1 * weekDeductor));
			String weekDate = DateUtils.formatDate(cal.getTime(), DateUtils.YYYYMMDD);
			return weekDate;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return startDate;
			
		}
	}
	
	public static ArrayList generateCalendarStartDates(String startDate, int weekCount)
	{
		ArrayList listOfWeeks = new ArrayList();
		try
		{
			Date startDateYYYYMMDD = DateUtils.convertDate(startDate, DateUtils.YYYYMMDD);
			Calendar cal = Calendar.getInstance();
			cal.setTime(startDateYYYYMMDD);

			for (int index = 0; index < weekCount; index++)
			{
				String currentDate = DateUtils.formatDate(cal.getTime(), DateUtils.YYYYMMDD);
				cal.add(Calendar.DAY_OF_MONTH, 7);
				listOfWeeks.add(currentDate);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return listOfWeeks;
	}

	public static HashMap getWeekIndexMap(String startDate)
	{
		HashMap weekMap = new HashMap();
		try
		{
			Date startDateYYYYMMDD = DateUtils.convertDate(startDate, DateUtils.YYYYMMDD);
			Calendar cal = Calendar.getInstance();
			cal.setTime(startDateYYYYMMDD);

			int weekIndex=0;
			for (int index = 1; index <= 3650; index++)
			{
				String currentDate = DateUtils.formatDate(cal.getTime(), DateUtils.YYYYMMDD);
				cal.add(Calendar.DAY_OF_MONTH, 1);
				
				
				weekMap.put(currentDate, weekIndex + "");
				if (index%7==0)
				{
					weekIndex++;
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return weekMap;
	}	

	public static void main(String[] args)
    throws Exception
    {
		LogUtils.println("Last Month Code:" + DateUtils.getLastMonthCode());
    }
}
