/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.suzukithdashboard.common;

import com.leadics.suzukiThdashboard.LILoggedInUserUtilitiesBean;

import com.leadics.suzukithdashboard.service.LIControllermapService;
import com.leadics.suzukithdashboard.to.LIControllermapRecord;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author varma.sagi
 */
public class LIAction extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LIAction</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LIAction at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        try {
            HttpServletRequest req = request;
            HttpServletResponse res = response;
            String actionType = request.getParameter("hiddenActionType");
            response.setContentType("text/html");

            LILoggedInUserUtilitiesBean utilBean = new LILoggedInUserUtilitiesBean();
            if (!utilBean.isRequestSecure(req)) {
                return;
            }

            String tableCode = actionType.substring(0,
                    actionType.lastIndexOf('-') + 1);
            String actionCode = actionType.substring(actionType
                    .lastIndexOf('-') + 1);


            LIControllermapService service = new LIControllermapService();
            LIControllermapRecord searchRecord = new LIControllermapRecord();
            searchRecord.setCode(tableCode);
            LIControllermapRecord[] records = service
                    .searchLIControllermapRecords(searchRecord);


            Class c = Class.forName(records[0].getClassname());
            Method m = c.getDeclaredMethod("processWebRequest", new Class[]{
                javax.servlet.http.HttpServletRequest.class,
                javax.servlet.http.HttpServletResponse.class,
                java.lang.String.class});

            Object i = c.newInstance();
            Object r = m.invoke(i,
                    new Object[]{request, response, actionCode});
        } catch (Exception exception) {
            out.println("Unauthorized Access");
            out.println("<H1><c>LeadICS</H1>");

        }
    }

    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        try {
            HttpServletRequest req = request;
            HttpServletResponse res = response;
            String actionType = request.getParameter("hiddenActionType");
            response.setContentType("text/html");

            LILoggedInUserUtilitiesBean utilBean = new LILoggedInUserUtilitiesBean();
            if (!utilBean.isRequestSecure(req)) {
                return;
            }

            String tableCode = actionType.substring(0,
                    actionType.lastIndexOf('-') + 1);
            String actionCode = actionType.substring(actionType
                    .lastIndexOf('-') + 1);



            LIControllermapService service = new LIControllermapService();
            LIControllermapRecord searchRecord = new LIControllermapRecord();
            searchRecord.setCode(tableCode);
            LIControllermapRecord[] records = service
                    .searchLIControllermapRecords(searchRecord);


            Class c = Class.forName(records[0].getClassname());
            Method m = c.getDeclaredMethod("processWebRequest", new Class[]{
                javax.servlet.http.HttpServletRequest.class,
                javax.servlet.http.HttpServletResponse.class,
                java.lang.String.class});

            Object i = c.newInstance();
            Object r = m.invoke(i,
                    new Object[]{request, response, actionCode});
        } catch (Exception exception) {
            out.println("Unauthorized Access");
            out.println("<H1><c>LeadICS</H1>");

        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
