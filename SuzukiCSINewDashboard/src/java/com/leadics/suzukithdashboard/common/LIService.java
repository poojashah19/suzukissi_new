package com.leadics.suzukithdashboard.common;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import com.leadics.utils.DateUtils;
import com.leadics.utils.LogUtils;
import com.leadics.utils.PropertyUtil;
import com.leadics.utils.StringUtils;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

public class LIService {

    static LogUtils logger = new LogUtils(LIService.class.getName());
    public final String strWhereCluse = "<<WHERE_CONDITION>>";
    public final String strBestWhereCluse = "<<BEST_WHERE_CONDITION>>";
    public final String strStudyWhereCluse = "<<STUDY_WHERE_CONDITION>>";
    public final String strSamplelimit = "#samplelimit#";
    public final String strDealerCountLimit = "#dealercountlimit#";
       public final String  strModelCountLimit="#modelcountlimit#";
       public final String strFactor="#factor#";
    public Map loadMap(String Query, String Column1, String Column2, boolean insertBlank)
            throws Exception {
        LIDAO dao = new LIDAO();
        return dao.loadMap(Query, Column1, Column2, insertBlank);
    }

    public String getBestStringCSI(HttpServletRequest request) throws Exception {

        String BestString = "Best Overall";
        String strZone = request.getParameter("zone");
        String strCity = request.getParameter("city");

        if (strZone != null && !strZone.equalsIgnoreCase("ALL") && !strZone.equalsIgnoreCase("Study Total")) {
            BestString = "Best in " + strZone;
        } else if (strCity != null && !strCity.equalsIgnoreCase("ALL")) {
            String region = new LIDAO().loadSequenceID("select distinct region from factor_aggregate where city='" + strCity + "' limit 1", "region");
            BestString = "Best in " + region;
        }
        return BestString;
    }
    
  
    public String getBestString(HttpServletRequest request) throws Exception {

        String BestString = "Best Overall";
        String strRegion = request.getParameter("region");
        String strZone = request.getParameter("zone");
        String strCity = request.getParameter("city");
//        if (strRegion != null && !strRegion.equalsIgnoreCase("ALL") && !strRegion.equalsIgnoreCase("Study Total")) {
//            BestString = "Best in " + strRegion;
//        } else
//            
            
            
            if (strZone != null && !strZone.equalsIgnoreCase("ALL") && !strZone.equalsIgnoreCase("Study Total")) {
            BestString = "Best in " + strZone;
        } else if (strCity != null && !strCity.equalsIgnoreCase("ALL")) {
            String region = new LIDAO().loadSequenceID("select distinct region from factor_aggregate where city='" + strCity + "' limit 1", "region");
            BestString = "Best in " + region;
        }
        return BestString;
    }
    
    

    public String getWorstStringCSI(HttpServletRequest request) throws Exception {

        String BestString = "Worst Overall";

        String strZone = request.getParameter("zone");
        String strCity = request.getParameter("city");
        if (strZone != null && !strZone.equalsIgnoreCase("ALL") && !strZone.equalsIgnoreCase("Study Total")) {
            BestString = "Worst in " + strZone;
        } else if (strCity != null && !strCity.equalsIgnoreCase("ALL")) {
            String region = new LIDAO().loadSequenceID("select distinct region from factor_aggregate where city='" + strCity + "' limit 1", "region");
            BestString = "Worst in " + region;
        }
        return BestString;
    }
    
     public String getWorstStringSSI(HttpServletRequest request) throws Exception {

        String BestString = "Worst Overall";

        String strZone = request.getParameter("zone");
        String strCity = request.getParameter("city");
        if (strZone != null && !strZone.equalsIgnoreCase("ALL") && !strZone.equalsIgnoreCase("Study Total")) {
            BestString = "Worst in " + strZone;
        } else if (strCity != null && !strCity.equalsIgnoreCase("ALL")) {
            String region = new LIDAO().loadSequenceID("select distinct region from factor_aggregate where city='" + strCity + "' limit 1", "region");
            BestString = "Worst in " + region;
        }
        return BestString;
    }

    public String getWorstString(HttpServletRequest request) throws Exception {

        String BestString = "Worst Overall";
        String strRegion = request.getParameter("region");
        String strZone = request.getParameter("zone");
        String strCity = request.getParameter("city");
//        if (strRegion != null && !strRegion.equalsIgnoreCase("ALL") && !strRegion.equalsIgnoreCase("Study Total")) {
//            BestString = "Worst in " + strRegion;
//        } else
            if (strZone != null && !strZone.equalsIgnoreCase("ALL") && !strZone.equalsIgnoreCase("Study Total")) {
            BestString = "Worst in " + strZone;
        } else if (strCity != null && !strCity.equalsIgnoreCase("ALL")) {
            String region = new LIDAO().loadSequenceID("select distinct region from factor_aggregate where city='" + strCity + "' limit 1", "region");
            BestString = "Worst in " + region;
        }
        return BestString;
    }

    public String getBestStringDealer(HttpServletRequest request) throws Exception {

        String BestString = "Best Overall";
        String strRegion = request.getParameter("dealerRegion");
        String switcher = request.getParameter("changeloyalSwitch");
        LIDAO dao = new LIDAO();
//        String strZone = dao.loadString("SELECT DISTINCT zone FROM sop_aggregate WHERE region like \"" + strRegion + "\" limit 1");

        if (switcher != null && !switcher.equalsIgnoreCase("Region") && strRegion != null && !strRegion.equalsIgnoreCase("Study Total")) {
            BestString = "Best in " + strRegion;
        }
//        } else if (switcher != null && !switcher.equalsIgnoreCase("Zone") && strZone != null && !strZone.equalsIgnoreCase("Study Total")) {
//            BestString = "Best in " + strZone;
//        }
        return BestString;
    }

    public String getWorstStringDealer(HttpServletRequest request) throws Exception {

        String BestString = "Worst Overall";
        String strRegion = request.getParameter("dealerRegion");
        String switcher = request.getParameter("changeloyalSwitch");
        LIDAO dao = new LIDAO();
//        String strZone = dao.loadString("SELECT DISTINCT zone FROM sop_aggregate WHERE region like \"" + strRegion + "\" limit 1");

        if (switcher != null && !switcher.equalsIgnoreCase("Study Total") && !switcher.equalsIgnoreCase("Region") && strRegion != null && !strRegion.equalsIgnoreCase("Study Total")) {
            BestString = "Worst in " + strRegion;
        } 
       
        return BestString;
    }

    public String buildWherecondition(List filterList, HttpServletRequest request) {
        try {
            String where_condtion = "";
            List<String> objFilterList = filterList;
            for (String temp : objFilterList) {
                if (temp.equalsIgnoreCase("biannual")) {
                    String strBiannual = request.getParameter("biannual");
                    if (strBiannual != null && !strBiannual.equalsIgnoreCase("Study Total") && !strBiannual.equalsIgnoreCase("Study Best") && !strBiannual.equalsIgnoreCase("All")) {
                        where_condtion = where_condtion + " AND `Bi-Annual` =\"" + strBiannual + "\"";
                    }
                } 
                  else if (temp.equalsIgnoreCase("dealergroup")) {
                    String strdealergroup = request.getParameter("dealergroup");
                        System.out.println("dealergroup: ::::" + strdealergroup);
                    if (strdealergroup != null && !strdealergroup.equalsIgnoreCase("Study Total") && !strdealergroup.equalsIgnoreCase("All")) {
                        System.out.println("zone: " + strdealergroup);
                        where_condtion = where_condtion + " AND dealergroup=\"" + strdealergroup + "\" ";
                    }
                } 
                else if (temp.equalsIgnoreCase("biannualmonth")) {
                    String strBiannual = request.getParameter("biannual");
                    if (strBiannual != null && !strBiannual.equalsIgnoreCase("Study Total") && !strBiannual.equalsIgnoreCase("Study Best") && !strBiannual.equalsIgnoreCase("All")) {
                        where_condtion = where_condtion + " AND `BiAnnual` =\"" + strBiannual + "\"";
                    }

                } else if (temp.equalsIgnoreCase("month")) {
                    String strMonth = request.getParameter("month");
                    if (strMonth != null && !strMonth.equalsIgnoreCase("Study Total") && !strMonth.equalsIgnoreCase("All")) {
                        where_condtion = where_condtion + " AND Month=\"" + strMonth + "\"";
                    }

                } else if (temp.equalsIgnoreCase("year")) {
                    String strYear = request.getParameter("year");
                    if (strYear != null && !strYear.equalsIgnoreCase("Study Total") && !strYear.equalsIgnoreCase("All")) {
                        where_condtion = where_condtion + " and year=\""+strYear+"\"";
                    }
//                    else{
//                        where_condtion = where_condtion + " AND Month in (SELECT Month FROM calender_filter WHERE year = '2016' order by date)";
//                    }

                }
                else if (temp.equalsIgnoreCase("yeargreater")) {
                     String strYear = PropertyUtil.getProperty("yearfilter");
                    if (strYear != null && !strYear.equalsIgnoreCase("Study Total") && !strYear.equalsIgnoreCase("All")) {
                        where_condtion = where_condtion + "AND year >= " + strYear;
                    }


                }
                else if (temp.equalsIgnoreCase("tableyear")) {
                    String strYear = request.getParameter("year");
                    if (strYear != null && !strYear.equalsIgnoreCase("Study Total") && !strYear.equalsIgnoreCase("All")) {
                        where_condtion = where_condtion + " AND Report_Month in (SELECT Month FROM calender_filter WHERE year = '" + strYear + "' order by date)";
                    }
//                    else{
//                        where_condtion = where_condtion + " AND Month in (SELECT Month FROM calender_filter WHERE year = '2016' order by date)";
//                    }

                } else if (temp.equalsIgnoreCase("biannualtable")) {
                    String strBiannual = request.getParameter("biannual");
                    if (strBiannual != null && !strBiannual.equalsIgnoreCase("Study Total") && !strBiannual.equalsIgnoreCase("All")) {
                        where_condtion = where_condtion + " AND Report_Month in (SELECT Month FROM calender_filter WHERE BiAnnual = \"" + strBiannual + "\" order by date)";
                    }

                } else if (temp.equalsIgnoreCase("monthtable")) {
                    String strMonth = request.getParameter("month");
                    if (strMonth != null && !strMonth.equalsIgnoreCase("Study Total") && !strMonth.equalsIgnoreCase("All")) {
                        where_condtion = where_condtion + " AND Report_Month=\"" + strMonth + "\"";
                    }

                } else if (temp.equalsIgnoreCase("model")) {
                    String strModel = request.getParameter("model");
                    if (strModel != null && !strModel.equalsIgnoreCase("Study Total") && !strModel.equalsIgnoreCase("All")) {
                        where_condtion = where_condtion + " AND model = '" + strModel + "' ";
                    }
                } else if (temp.equalsIgnoreCase("region")) {
                    String strRegion = request.getParameter("region");
                    if (strRegion != null && !strRegion.equalsIgnoreCase("Study Total") && !strRegion.equalsIgnoreCase("All")) {
                        System.out.println("strRegion: " + strRegion);
                        where_condtion = where_condtion + " AND region=\"" + strRegion + "\" ";
                    }
                } else if (temp.equalsIgnoreCase("bestregion")) {
                   String strRegion = request.getParameter("region");
                    if (strRegion != null && !strRegion.equalsIgnoreCase("Study Total") && !strRegion.equalsIgnoreCase("All")) {
                        System.out.println("strRegion: " + strRegion);
                        where_condtion = where_condtion + " AND region=\"" + strRegion + "\" ";
                    }
                   
                }  else if (temp.equalsIgnoreCase("dealergroupregion")) {
                   String strdealergroup = request.getParameter("dealergroup");
                    if (strdealergroup != null && !strdealergroup.equalsIgnoreCase("Study Total") && !strdealergroup.equalsIgnoreCase("All")) {
                        System.out.println("strRegion: " + strdealergroup);
                         String reg = new LIDAO().loadSequenceID("select distinct region from factor_aggregate where dealergroup='" + strdealergroup + "'", "region");
                     
                        where_condtion = where_condtion + " AND region=\"" + reg + "\" ";
                    }
                }else if (temp.equalsIgnoreCase("zone")) {
                    String strZone = request.getParameter("zone");
                    if (strZone != null && !strZone.equalsIgnoreCase("Study Total") && !strZone.equalsIgnoreCase("All")) {
                        System.out.println("zone: " + strZone);
                        where_condtion = where_condtion + " AND zone=\"" + strZone + "\" ";
                    }
                } 
                
              
                
                
                else if (temp.equalsIgnoreCase("brand")) {
                    String strBrand = request.getParameter("brand");
                    if (strBrand != null && !strBrand.equalsIgnoreCase("Study Total") && !strBrand.equalsIgnoreCase("All")) {
                        where_condtion = where_condtion + " AND model in (Select ModelName from brandmodels where BrandName='" + strBrand + "') ";
                    }
                } else if (temp.equalsIgnoreCase("factor")) {
                    String strFactor = request.getParameter("factor");
                    if (strFactor != null) {
                        where_condtion = where_condtion + " AND FactorName like '" + strFactor + "' ";
                    } 
                } else if (temp.equalsIgnoreCase("attributeFactor")) {
                    String strFactor = request.getParameter("factor");
                    if (strFactor != null) {
                        where_condtion = where_condtion + " AND Factor like '" + strFactor + "' ";
                    } 
                } else if (temp.equalsIgnoreCase("dealer")) {
                    String strDealer = request.getParameter("dealer");
                    if (strDealer != null && !strDealer.equalsIgnoreCase("Study Total") && !strDealer.equalsIgnoreCase("All")) {
                        where_condtion = where_condtion + " AND dealer = '" + strDealer + "' ";
                    }
                } else if (temp.equalsIgnoreCase("benchmarkscope")) {
                    String strBenchmarkscope = request.getParameter("benchmarkscope");
                    if (strBenchmarkscope != null && !strBenchmarkscope.equalsIgnoreCase("Study Total") && !strBenchmarkscope.equalsIgnoreCase("All")) {
                        where_condtion = where_condtion + " AND benchmarkscope = '" + strBenchmarkscope + "' ";
                    }
                } else if (temp.equalsIgnoreCase("benchmarkmeasure")) {
                    String strBenchmarkmeasure = request.getParameter("benchmarkmeasure");
                    if (strBenchmarkmeasure != null && !strBenchmarkmeasure.equalsIgnoreCase("Study Total") && !strBenchmarkmeasure.equalsIgnoreCase("All")) {
                        where_condtion = where_condtion + " AND benchmarkmeasure = '" + strBenchmarkmeasure + "' ";
                    }
                } else if (temp.equalsIgnoreCase("attribute")) {
                    String strAttribute = request.getParameter("attribute");
                    if (strAttribute != null && !strAttribute.equalsIgnoreCase("Study Total") && !strAttribute.equalsIgnoreCase("All")) {
                        where_condtion = where_condtion + " AND attributeName like \"" + strAttribute + "%\" ";
                    }
                }
            }

            System.out.println("where_condtion : " + where_condtion);
            return where_condtion;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
    
    
    
    
    
    
    public String buildCSIWherecondition(List filterList, HttpServletRequest request) {
        try {
            String where_condtion = "";
            List<String> objFilterList = filterList;
            for (String temp : objFilterList) {
                if (temp.equalsIgnoreCase("biannual")) {
                    String strBiannual = request.getParameter("biannual");
                    if (strBiannual != null && !strBiannual.equalsIgnoreCase("Study Total") && !strBiannual.equalsIgnoreCase("Study Best") && !strBiannual.equalsIgnoreCase("All")) {
                        where_condtion = where_condtion + " AND `Bi-Annual` =\"" + strBiannual + "\"";
                    }
                } else if (temp.equalsIgnoreCase("biannualmonth")) {
                    String strBiannual = request.getParameter("biannual");
                    if (strBiannual != null && !strBiannual.equalsIgnoreCase("Study Total") && !strBiannual.equalsIgnoreCase("Study Best") && !strBiannual.equalsIgnoreCase("All")) {
                        where_condtion = where_condtion + " AND `BiAnnual` =\"" + strBiannual + "\"";
                    }

                } else if (temp.equalsIgnoreCase("month")) {
                    String strMonth = request.getParameter("month");
                    if (strMonth != null && !strMonth.equalsIgnoreCase("Study Total") && !strMonth.equalsIgnoreCase("All")) {
                        where_condtion = where_condtion + " AND Month=\"" + strMonth + "\"";
                    }

                } else if (temp.equalsIgnoreCase("year")) {
                    String strYear = request.getParameter("year");
                    if (strYear != null && !strYear.equalsIgnoreCase("Study Total") && !strYear.equalsIgnoreCase("All")) {
                        where_condtion = where_condtion + " AND Month in (SELECT Month FROM calender_filter WHERE year = '" + strYear + "' order by date)";
                    }
//                    else{
//                        where_condtion = where_condtion + " AND Month in (SELECT Month FROM calender_filter WHERE year = '2016' order by date)";
//                    }

                } else if (temp.equalsIgnoreCase("tableyear")) {
                    String strYear = request.getParameter("year");
                    if (strYear != null && !strYear.equalsIgnoreCase("Study Total") && !strYear.equalsIgnoreCase("All")) {
                        where_condtion = where_condtion + " AND Report_Month in (SELECT Month FROM calender_filter WHERE year = '" + strYear + "' order by date)";
                    }
//                    else{
//                        where_condtion = where_condtion + " AND Month in (SELECT Month FROM calender_filter WHERE year = '2016' order by date)";
//                    }

                } else if (temp.equalsIgnoreCase("biannualtable")) {
                    String strBiannual = request.getParameter("biannual");
                    if (strBiannual != null && !strBiannual.equalsIgnoreCase("Study Total") && !strBiannual.equalsIgnoreCase("All")) {
                        where_condtion = where_condtion + " AND Report_Month in (SELECT Month FROM calender_filter WHERE BiAnnual = \"" + strBiannual + "\" order by date)";
                    }

                } else if (temp.equalsIgnoreCase("monthtable")) {
                    String strMonth = request.getParameter("month");
                    if (strMonth != null && !strMonth.equalsIgnoreCase("Study Total") && !strMonth.equalsIgnoreCase("All")) {
                        where_condtion = where_condtion + " AND Report_Month=\"" + strMonth + "\"";
                    }

                } else if (temp.equalsIgnoreCase("model")) {
                    String strModel = request.getParameter("model");
                    if (strModel != null && !strModel.equalsIgnoreCase("Study Total") && !strModel.equalsIgnoreCase("All")) {
                        where_condtion = where_condtion + " AND model = '" + strModel + "' ";
                    }
                } else if (temp.equalsIgnoreCase("region")) {
                    String strRegion = request.getParameter("region");
                    if (strRegion != null && !strRegion.equalsIgnoreCase("Study Total") && !strRegion.equalsIgnoreCase("All")) {
                        System.out.println("strRegion: " + strRegion);
                        where_condtion = where_condtion + " AND region=\"" + strRegion + "\" ";
                    }
                } else if (temp.equalsIgnoreCase("bestregion")) {
                   String strRegion = request.getParameter("region");
                    if (strRegion != null && !strRegion.equalsIgnoreCase("Study Total") && !strRegion.equalsIgnoreCase("All")) {
                        System.out.println("strRegion: " + strRegion);
                        where_condtion = where_condtion + " AND region=\"" + strRegion + "\" ";
                    }
                } else if (temp.equalsIgnoreCase("zone")) {
                    String strZone = request.getParameter("zone");
                    if (strZone != null && !strZone.equalsIgnoreCase("Study Total") && !strZone.equalsIgnoreCase("All")) {
                        System.out.println("zone: " + strZone);
                        where_condtion = where_condtion + " AND zone=\"" + strZone + "\" ";
                    }
                } 
                
                else if (temp.equalsIgnoreCase("dealergroup")) {
                    String strdealergroup = request.getParameter("dealergroup");
                    if (strdealergroup != null && !strdealergroup.equalsIgnoreCase("Study Total") && !strdealergroup.equalsIgnoreCase("All")) {
                        System.out.println("zone: " + strdealergroup);
                        where_condtion = where_condtion + " AND dealergroup=\"" + strdealergroup + "\" ";
                    }
                } 
                
                
                else if (temp.equalsIgnoreCase("brand")) {
                    String strBrand = request.getParameter("brand");
                    if (strBrand != null && !strBrand.equalsIgnoreCase("Study Total") && !strBrand.equalsIgnoreCase("All")) {
                        where_condtion = where_condtion + " AND model in (Select ModelName from brandmodels where BrandName='" + strBrand + "') ";
                    }
                } else if (temp.equalsIgnoreCase("factor")) {
                    String strFactor = request.getParameter("factor");
                    if (strFactor != null) {
                        where_condtion = where_condtion + " AND FactorName like '" + strFactor + "' ";
                    } else {
                        where_condtion = where_condtion + " AND FactorName like 'Service Initiation' ";
                    }
                } else if (temp.equalsIgnoreCase("attributeFactor")) {
                    String strFactor = request.getParameter("factor");
                    if (strFactor != null) {
                        where_condtion = where_condtion + " AND Factor like '" + strFactor + "' ";
                    } else {
                        where_condtion = where_condtion + " AND Factor like 'Service Initiation' ";
                    }
                } else if (temp.equalsIgnoreCase("dealer")) {
                    String strDealer = request.getParameter("dealer");
                    if (strDealer != null && !strDealer.equalsIgnoreCase("Study Total") && !strDealer.equalsIgnoreCase("All")) {
                        where_condtion = where_condtion + " AND dealer = '" + strDealer + "' ";
                    }
                } else if (temp.equalsIgnoreCase("benchmarkscope")) {
                    String strBenchmarkscope = request.getParameter("benchmarkscope");
                    if (strBenchmarkscope != null && !strBenchmarkscope.equalsIgnoreCase("Study Total") && !strBenchmarkscope.equalsIgnoreCase("All")) {
                        where_condtion = where_condtion + " AND benchmarkscope = '" + strBenchmarkscope + "' ";
                    }
                } else if (temp.equalsIgnoreCase("benchmarkmeasure")) {
                    String strBenchmarkmeasure = request.getParameter("benchmarkmeasure");
                    if (strBenchmarkmeasure != null && !strBenchmarkmeasure.equalsIgnoreCase("Study Total") && !strBenchmarkmeasure.equalsIgnoreCase("All")) {
                        where_condtion = where_condtion + " AND benchmarkmeasure = '" + strBenchmarkmeasure + "' ";
                    }
                } else if (temp.equalsIgnoreCase("attribute")) {
                    String strAttribute = request.getParameter("attribute");
                    if (strAttribute != null && !strAttribute.equalsIgnoreCase("Study Total") && !strAttribute.equalsIgnoreCase("All")) {
                        where_condtion = where_condtion + " AND attributeName like \"" + strAttribute + "%\" ";
                    }
                }
            }

            System.out.println("where_condtion : " + where_condtion);
            return where_condtion;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public String buildSSIWherecondition(List filterList, HttpServletRequest request) {
        try {
            String where_condtion = "";
            List<String> objFilterList = filterList;
            for (String temp : objFilterList) {
                if (temp.equalsIgnoreCase("biweekly")) {
                    String strBiweekly = request.getParameter("biweekly");
                    if (strBiweekly != null && !strBiweekly.equalsIgnoreCase("ALL")) {
                        where_condtion = where_condtion + " AND Bi_Weekly = '" + strBiweekly + "'";
                    }
                } else if (temp.equalsIgnoreCase("month")) {
                    String strMonth = request.getParameter("month");
                    if (strMonth != null && strMonth.equalsIgnoreCase("Trailing 3mth")) {
                        where_condtion = where_condtion + " AND month IN (SELECT MONTH FROM (SELECT MONTH,STR_TO_DATE(CONCAT(LEFT(MONTH,3),'-',RIGHT(MONTH,2)), '%b-%y') AS dt FROM ssi_calender_filter ORDER BY dt DESC LIMIT 3) AS a ) ";
                    } else if (strMonth != null && !strMonth.equalsIgnoreCase("ALL")) {
                        where_condtion = where_condtion + " AND month  = '" + strMonth + "' ";
                    }
                } else if (temp.equalsIgnoreCase("quarter")) {
                    String strQuarter = request.getParameter("quarter");

                    if (strQuarter != null && !strQuarter.equalsIgnoreCase("ALL")) {
                        where_condtion = where_condtion + " AND Quarter=\"" + strQuarter + "\"";
                    }

                } else if (temp.equalsIgnoreCase("wave")) {
                    String strWave = request.getParameter("wave");

                    if (strWave != null && !strWave.equalsIgnoreCase("ALL")) {
                        where_condtion = where_condtion + " AND Quarter=\"" + strWave + "\"";
                    }

                } else if (temp.equalsIgnoreCase("year")) {
                    String strYear = request.getParameter("year");
                    if (strYear != null && !strYear.equalsIgnoreCase("ALL")) {
                        where_condtion = where_condtion + " AND quarter in (SELECT quarter FROM ssi_calender_filter WHERE year = '" + strYear + "')";
                    }

                } else if (temp.equalsIgnoreCase("model")) {
                    String strModel = request.getParameter("model");
                    if (strModel != null && !strModel.equalsIgnoreCase("ALL")) {
                        where_condtion = where_condtion + " AND model = '" + strModel + "' ";
                    }
                } else if (temp.equalsIgnoreCase("city")) {
                    String strCity = request.getParameter("city");
                    if (strCity != null && !strCity.equalsIgnoreCase("ALL")) {
                        where_condtion = where_condtion + " AND city = '" + strCity + "' ";
                    }
                } else if (temp.equalsIgnoreCase("bestcity")) {
                    String strCity = request.getParameter("city");
                    if (strCity != null && !strCity.equalsIgnoreCase("ALL")) {
                        String region = new LIDAO().loadSequenceID("select distinct region from ssi_factor_aggregate where city='" + strCity + "' limit 1", "region");
                        where_condtion = where_condtion + " AND region = '" + region + "' ";
                    }
                } else if (temp.equalsIgnoreCase("cityregion")) {
                    String strCity = request.getParameter("city");
                    if (strCity != null && !strCity.equalsIgnoreCase("ALL")) {
                        String region = new LIDAO().loadSequenceID("select distinct region from ssi_factor_aggregate where city='" + strCity + "' limit 1", "region");
                        where_condtion = where_condtion + " AND region = '" + region + "' ";
                    }
                } else if (temp.equalsIgnoreCase("region")) {
                    String strRegion = request.getParameter("region");
                    if (strRegion != null && !strRegion.equalsIgnoreCase("ALL")) {
                        System.out.println("strRegion: " + strRegion);
                        where_condtion = where_condtion + " AND region='" + strRegion + "' ";
                    }
                } else if (temp.equalsIgnoreCase("bestregion")) {
                    String strRegion = request.getParameter("region");
                    if (strRegion != null && !strRegion.equalsIgnoreCase("ALL")) {
                        System.out.println("strRegion: " + strRegion);
                        where_condtion = where_condtion + " AND region='" + strRegion + "' ";
                    }
                } else if (temp.equalsIgnoreCase("state")) {
                    String strState = request.getParameter("state");
                    if (strState != null && !strState.equalsIgnoreCase("ALL")) {
                        System.out.println("state: " + strState);
                        where_condtion = where_condtion + " AND state='" + strState + "' ";
                    }
                } else if (temp.equalsIgnoreCase("beststate")) {
                    String strState = request.getParameter("state");
                    if (strState != null && !strState.equalsIgnoreCase("ALL")) {
                        String region = new LIDAO().loadSequenceID("select distinct region from ssi_factor_aggregate where state='" + strState + "' limit 1", "region");
                        where_condtion = where_condtion + " AND region = '" + region + "' ";
                    }
                } else if (temp.equalsIgnoreCase("brand")) {
                    String strBrand = request.getParameter("brand");
                    if (strBrand != null && !strBrand.equalsIgnoreCase("ALL")) {
                        where_condtion = where_condtion + " AND model in (Select ModelName from brandmodels where BrandName='" + strBrand + "') ";
                    }
                } else if (temp.equalsIgnoreCase("factor")) {
                    String strFactor = request.getParameter("factor");
                    if (strFactor != null) {
                        where_condtion = where_condtion + " AND FactorName like '" + strFactor + "' ";
                    } else {
                        where_condtion = where_condtion + " AND FactorName like 'Sales Initiation' ";
                    }
                } else if (temp.equalsIgnoreCase("attributeFactor")) {
                    String strFactor = request.getParameter("factor");
                    if (strFactor != null) {
                        where_condtion = where_condtion + " AND Factor like '" + strFactor + "' ";
                    } else {
                        where_condtion = where_condtion + " AND Factor like 'Sales Initiation' ";
                    }
                } else if (temp.equalsIgnoreCase("dealer")) {
                    String strDealer = request.getParameter("dealer");
                    if (strDealer != null && !strDealer.equalsIgnoreCase("ALL")) {
                        where_condtion = where_condtion + " AND dealer = '" + strDealer + "' ";
                    }
                } else if (temp.equalsIgnoreCase("benchmarkscope")) {
                    String strBenchmarkscope = request.getParameter("benchmarkscope");
                    if (strBenchmarkscope != null && !strBenchmarkscope.equalsIgnoreCase("ALL")) {
                        where_condtion = where_condtion + " AND benchmarkscope = '" + strBenchmarkscope + "' ";
                    }
                } else if (temp.equalsIgnoreCase("benchmarkmeasure")) {
                    String strBenchmarkmeasure = request.getParameter("benchmarkmeasure");
                    if (strBenchmarkmeasure != null && !strBenchmarkmeasure.equalsIgnoreCase("ALL")) {
                        where_condtion = where_condtion + " AND benchmarkmeasure = '" + strBenchmarkmeasure + "' ";
                    }
                } else if (temp.equalsIgnoreCase("attribute")) {
                    String strAttribute = request.getParameter("attribute");
                    if (strAttribute != null && !strAttribute.equalsIgnoreCase("ALL")) {
                        where_condtion = where_condtion + " AND attributeName like \"" + strAttribute + "%\" ";
                    }
                }
            }

            System.out.println("where_condtion : " + where_condtion);
            return where_condtion;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getTranslateString(String str, String langcode) throws Exception {
        String strTranslated = new LIDAO().loadString("SELECT ContentReganal FROM lang_conversion WHERE ContentEN=\"" + str + "\" AND LangCode='" + langcode + "' LIMIT 1 ");

        System.out.println(strTranslated+"Query:::::::::::::: : " + "SELECT ContentReganal FROM lang_conversion WHERE ContentEN=\"" + str + "\" AND LangCode='" + langcode + "' LIMIT 1");

        if (strTranslated == null) {
            return str;
        } else {
            return strTranslated;
        }

    }
    public String getTranslateStringWithAsterisk(String str, String langcode) throws Exception {
         int len=str.length();

       String strTranslated="";
        if(str.contains("**"))
        {
            String doublestr=str.substring(0,len-2);
        
           strTranslated = new LIDAO().loadString("SELECT ContentReganal FROM lang_conversion WHERE ContentEN=\"" + doublestr + "\" AND LangCode='" + langcode + "' LIMIT 1 ");
             strTranslated +="**";
             }
        else if(str.contains("*"))
        {
           String singleStr=str.substring(0,len-1);

            strTranslated = new LIDAO().loadString("SELECT ContentReganal FROM lang_conversion WHERE ContentEN=\"" + singleStr + "\" AND LangCode='" + langcode + "' LIMIT 1 ");
            strTranslated +="*";
        }
        else
        {
           strTranslated = new LIDAO().loadString("SELECT ContentReganal FROM lang_conversion WHERE ContentEN=\"" + str + "\" AND LangCode='" + langcode + "' LIMIT 1 ");
   
        }




        if (strTranslated==null) {
            return str;
        } else {
            return strTranslated;
        }

    }
    public String getmultilineTranslateString(String str, String langcode) throws Exception {
        String strTranslated = new LIDAO().loadString("SELECT ContentReganal FROM lang_conversion WHERE ContentEN=\"" + str + "\" AND LangCode='" + langcode + "' LIMIT 1 ");

        System.out.println("thaitranslation:::::::::::::: : " + strTranslated);

        if (strTranslated == null) {
            return str;
        } else {
            return strTranslated;
        }

    }

    public String getRegionofDealer(String str) throws Exception {
        String strTranslated = new LIDAO().loadString("select region from factor_aggregate WHERE dealer=\"" + str + "\" LIMIT 1 ");
        return strTranslated;

    }

    public boolean isDuplicateSession(String userId) {
        try {
            String EnableDuplicateSession = StringUtils.noNull(PropertyUtil.getProperty("EnableDuplicateSession"));
            if (EnableDuplicateSession.equals("Y")) {
                return false;
            }

            String query = "";
            int maxTimeDiff = Integer.parseInt(PropertyUtil.getProperty("websessiontimeout"));

            query += "SELECT count(*) CNT FROM WEB_SESSION ";
            query += "WHERE (TIMESTAMPDIFF(MINUTE,LASTACC,NOW()) < '" + maxTimeDiff + "') ";
            query += "AND (UPPER(USER_ID) = UPPER('" + userId + "')) ";
            query += "AND (RSTATUS = '1') ";

            LIDAO dao = new LIDAO();

            if ((dao.isMSSQL()) || (dao.isMSSQL8())) {
                query = "SELECT count(*) CNT FROM WEB_SESSION ";
                query += "WHERE (datediff(MINUTE,LASTACC,GETDATE())  < '" + maxTimeDiff + "') ";
                query += "AND (UPPER(USER_ID) = UPPER('" + userId + "')) ";
                query += "AND (RSTATUS = '1') ";
            }

            if (dao.isOracleDatabase()) {
//				select count(*) cnt  from WEB_SESSION where ((to_number(to_date(sysdate,'YYYYMMDDHH24MISS') - to_date(LASTACC,'YYYYMMDDHH24MISS')) * 24 * 60)<'15') AND User_id='BOKADMIN' ;
                query = "SELECT count(*) CNT FROM WEB_SESSION ";
                query += "WHERE (((SYSDATE - LASTACC)*24*60)  < '" + maxTimeDiff + "') ";
                query += "AND (UPPER(USER_ID) = UPPER('" + userId + "')) ";
                query += "AND (RSTATUS = '1') ";

                query = StringUtils.replaceString(query, "#SYSDATE#", DateUtils.getCurrentDateTime(), true);
            }

            int count = dao.loadCount(query);
            return (count > 0);
        } catch (Exception exception) {
            logger.error(exception);
            logger.error("isDuplicateSession" + getStackTrace(exception));
            return false;
        }
    }

    public boolean isValidSession(String userId, String sessionId) {
        try {
            String query = "";
            int maxTimeDiff = Integer.parseInt(PropertyUtil.getProperty("websessiontimeout"));

            LogUtils.println("DB Check 1:");

            query += "SELECT count(*) CNT FROM web_session  ";
            query += "WHERE (TIMESTAMPDIFF(MINUTE,LASTACC,NOW()) < '" + maxTimeDiff + "') ";
            query += "AND (UPPER(USER_ID) = UPPER('" + userId + "')) ";
            query += "AND (UPPER(SESSIONID) = UPPER('" + sessionId + "')) ";
            query += "AND (RSTATUS = '1') ";

            LIDAO dao = new LIDAO();

            if ((dao.isMSSQL()) || (dao.isMSSQL8())) {
                query = "SELECT count(*) CNT FROM WEB_SESSION ";
                query += "WHERE (datediff(MINUTE,LASTACC,GETDATE()) < '" + maxTimeDiff + "') ";
                query += "AND (UPPER(USER_ID) = UPPER('" + userId + "')) ";
                query += "AND (UPPER(SESSIONID) = UPPER('" + sessionId + "')) ";
                query += "AND (RSTATUS = '1') ";
            }

            if (dao.isOracleDatabase()) {
                query = "SELECT count(*) CNT FROM WEB_SESSION ";
                query += "WHERE (((SYSDATE - LASTACC)*24*60) < '" + maxTimeDiff + "') ";
                query += "AND (UPPER(USER_ID) = UPPER('" + userId + "')) ";
                query += "AND (UPPER(SESSIONID) = UPPER('" + sessionId + "')) ";
                query += "AND (RSTATUS = '1') ";

                query = StringUtils.replaceString(query, "#SYSDATE#", DateUtils.getCurrentDateTime(), true);
            }

            LogUtils.println("Query for Session Check:" + query);
            LogUtils.println("DB Check 2:" + dao.isMSSQL());
            LogUtils.println("DB Check 2:" + dao.isMSSQL8());

            int count = dao.loadCount(query);
            LogUtils.println("Query Output:" + count);
            return (count > 0);
        } catch (Exception exception) {
            logger.error(exception);
            logger.error("isValidSession" + getStackTrace(exception));
            return false;
        }
    }

    public void clearOldWebSessions(String userId) {
        try {
            String updateStatement = "";
            updateStatement += " UPDATE web_session ";
            updateStatement += " SET CLOSE_TYPE = '#CLOSETYPE#', ";
            updateStatement += " CLOSED_AT = '#CLOSEDAT#', ";
            updateStatement += " RSTATUS = '0' ";
            updateStatement += " WHERE (RSTATUS = '1')AND(USER_ID='#USERID#') ";

            updateStatement = StringUtils.replaceString(updateStatement, "#CLOSETYPE#", "AUTO_CLOSE", true);
            updateStatement = StringUtils.replaceString(updateStatement, "#CLOSEDAT#", DateUtils.getCurrentDateTimene(), true);
            updateStatement = StringUtils.replaceString(updateStatement, "#USERID#", userId, true);
            LIDAO dao = new LIDAO();
            dao.executeUpdateQuery(updateStatement);
        } catch (Exception exception) {
            logger.error(exception);
            logger.error("clearOldWebSessions" + getStackTrace(exception));
        }
    }

    public static String formatAmountNoDecimal(String inputAmount) {
        String result = formatAmount(inputAmount);
        result = StringUtils.replaceString(result, ".", "", true);
        return result;
    }

    public static String formatAmountWithZero(String inputAmount) {
        try {
            double amount = Double.parseDouble(inputAmount);
            DecimalFormat df = new DecimalFormat("#,###,###,##0.00");
            return df.format(amount);
        } catch (Exception exception) {
            //logger.error(exception);
            return inputAmount;
        }
    }

    public static String formatAmountWithCommas(String inputAmount) {
        try {
            double amount = Double.parseDouble(inputAmount);
            DecimalFormat df = new DecimalFormat("#,###,###,##0.00");
            return df.format(amount);
        } catch (Exception exception) {
            //logger.error(exception);
            return inputAmount;
        }
    }

    public static String formatAmount(String inputAmount) {
        try {
            double amount = Double.parseDouble(inputAmount);
            DecimalFormat df = new DecimalFormat("##.00");
            return df.format(amount);
        } catch (Exception exception) {
            //logger.error(exception);
            return inputAmount;
        }
    }

    public static String formatAmount(String inputAmount, String format) {
        try {
            double amount = Double.parseDouble(inputAmount);
            DecimalFormat df = new DecimalFormat(format);
            return df.format(amount);
        } catch (Exception exception) {
            //logger.error(exception);
            return inputAmount;
        }
    }

    public static String addDecimalToCurrentAmount(String inputAmount) {
        if (StringUtils.isNull(inputAmount)) {
            inputAmount = "0";
        }
        if (inputAmount.length() < 3) {
            inputAmount = "000" + inputAmount;
        }

        String mainValue = inputAmount.substring(0, inputAmount.length() - 2);
        String decimalValue = inputAmount.substring(inputAmount.length() - 2);
        return mainValue + "." + decimalValue;
    }

    public static String truncateNonDecimalValue(String inputAmount) {
        if (StringUtils.isNull(inputAmount)) {
            inputAmount = "0";
        }
        if (inputAmount.length() < 3) {
            inputAmount = "000" + inputAmount;
        }

        String mainValue = inputAmount.substring(0, inputAmount.length() - 2);
        String decimalValue = inputAmount.substring(inputAmount.length() - 2);
        return mainValue;
    }

    public void createMakerCheckerAuditEntry(String tableName, String recordId, String actionName, String createID, String comments)
            throws Exception {
        try {
            return;
        } catch (Exception exception) {
            logger.error("createMakerCheckerAuditEntry" + getStackTrace(exception));
            throw exception;
        }
    }

    public HashMap callActionEvent(String eventName, String recordID) {
        HashMap inputMap = new HashMap();
        inputMap.put("EventRecordID", recordID);
        inputMap.put("EventName", eventName);
        return inputMap;
    }

    public boolean isValidStatusForDeny(String inputStatus) {
        if (StringUtils.isNull(inputStatus)) {
            return false;
        }
        inputStatus = inputStatus.trim();
        if (inputStatus.equals("4")) {
            return true;
        }
        return false;
    }

    public boolean isValidStatusForPasswordReset(String inputStatus) {
        if (StringUtils.isNull(inputStatus)) {
            return false;
        }
        inputStatus = inputStatus.trim();
        if (inputStatus.equals("1")) {
            return true;
        }
        return false;
    }

    public boolean isValidStatusForPinReset(String inputStatus) {
        if (StringUtils.isNull(inputStatus)) {
            return false;
        }
        inputStatus = inputStatus.trim();
        if (inputStatus.equals("1")) {
            return true;
        }
        return false;
    }

    public boolean isValidStatusForBlock(String inputStatus) {
        if (StringUtils.isNull(inputStatus)) {
            return false;
        }
        inputStatus = inputStatus.trim();
        if (inputStatus.equals("1")) {
            return true;
        }
        return false;
    }

    public boolean isValidStatusForApproval(String inputStatus) {
        if (StringUtils.isNull(inputStatus)) {
            return false;
        }
        inputStatus = inputStatus.trim();
        if (inputStatus.equals("4")) {
            return true;
        }
        return false;
    }

    public boolean isValidStatusForReset(String inputStatus) {
        if (StringUtils.isNull(inputStatus)) {
            return false;
        }
        inputStatus = inputStatus.trim();
        if (inputStatus.equals("0")) {
            return true;
        }
        if (inputStatus.equals("1")) {
            return true;
        }
        if (inputStatus.equals("2")) {
            return true;
        }
        return false;
    }

    public boolean isValidStatusForSubmission(String inputStatus) {
        if (StringUtils.isNull(inputStatus)) {
            return true;
        }
        inputStatus = inputStatus.trim();
        if (inputStatus.equals("-1")) {
            return true;
        }
        if (inputStatus.equals("3")) {
            return true;
        }
        if (inputStatus.equals("5")) {
            return true;
        }
        return false;
    }

    public String getStackTrace(Exception e) {
        return StringUtils.getStackTrace(e);
    }

    public String getErrorMessage(Exception e) {
        logger.error(e);
        String message = e.toString();
        message = message.toLowerCase();

        if (message.indexOf("sqlintegrityconstraintviolation") > -1) {
            return "Duplicate Record";
        }

        if (message.indexOf("duplicate") > -1) {
            return "Duplicate Record";
        }

        if (message.indexOf("duplicate") > -1) {
            return "Duplicate Record";
        }

        return message;
    }

    public boolean isEligibleRecordForSubmit(String masterRecordID) {
        try {
            String query = "select count(*) cnt from audit_record where (master_record_code = '#MASTRECID#')";
            query = StringUtils.replaceString(query, "#MASTRECID#", masterRecordID + "", true);
            LIDAO dao = new LIDAO();
            int cnt = dao.loadCount(query);
            return (cnt > 0);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public String getStartDate(String loginRecordID) {
        try {
            String query = "SELECT min(schedule_date) start_date FROM audit_master where ((rstatus = '1') and (P3_ACTUAL_DATE is null) and (auditor_id = '#AUDITORID#'))";
            query = StringUtils.replaceString(query, "#AUDITORID#", loginRecordID + "", true);
            LIDAO dao = new LIDAO();
            String resultStartDate = dao.loadString(query);
            return resultStartDate;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                return DateUtils.getCurrentTime(DateUtils.YYYYMMDD);
            } catch (Exception e2) {
                return "";
            }
        }
    }

}
