package com.leadics.suzukithdashboard.common;

import java.net.InetAddress;

import org.apache.commons.lang.StringEscapeUtils;

import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.*;
import javax.servlet.http.*;

import org.json.simple.JSONObject;
import org.slf4j.*;

import com.leadics.suzukiThdashboard.LILoggedInUserUtilitiesBean;
import com.leadics.suzukithdashboard.to.LISystemuserviewRecord;
import com.leadics.utils.LISecurityUtil;
import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;

public class LIController {

    static LogUtils logger = new LogUtils(LIController.class.getName());

    public String getCustomerName()
            throws Exception {
        InetAddress ip = InetAddress.getLocalHost();
        String hostName = ip.getHostName();
        String hostAddress = ip.getHostAddress();
        String key = hostName + hostAddress;

        LISecurityUtil su = new LISecurityUtil();
        String encryptedKey = su.encrypt(key);
        return encryptedKey;
    }

    private static final String HEADER_X_FORWARDED_FOR
            = "X-FORWARDED-FOR";

    public static String remoteAddr(HttpServletRequest request) {
        String remoteAddr = request.getRemoteAddr();
        String x;
        if ((x = request.getHeader(HEADER_X_FORWARDED_FOR)) != null) {
            remoteAddr = x;
            int idx = remoteAddr.indexOf(',');
            if (idx > -1) {
                remoteAddr = remoteAddr.substring(0, idx);
            }
        }
        return remoteAddr;
    }

    public String getIPAddress(HttpServletRequest request) {
        String remoteAddr = request.getRemoteAddr();
        String x;
        if ((x = request.getHeader(HEADER_X_FORWARDED_FOR)) != null) {
            remoteAddr = x;
            int idx = remoteAddr.indexOf(',');
            if (idx > -1) {
                remoteAddr = remoteAddr.substring(0, idx);
            }
        }
        return remoteAddr;
    }

    public String getLoggedInID(HttpServletRequest req) {
        try {
            LILoggedInUserUtilitiesBean loginUtils = new LILoggedInUserUtilitiesBean();
            LISystemuserviewRecord userRecord = loginUtils.getLoggedInUser(req, null);
            if (userRecord == null) {
                return "";
            }
            return userRecord.getId();
        } catch (Exception e) {
            logger.error(e);
            return "";
        }
    }

    public String getReqComponent(HttpServletRequest req) {
        String loggedInID = getLoggedInID(req);
        String result = "";
        if (!StringUtils.isNullOrEmpty(loggedInID)) {
            result = "AdminConsole";
        }
        return result;
    }

    public String getErrorMessage(Exception e) {
        try {
            LIService serv = new LIService();
            String result = serv.getErrorMessage(e);
            return result;
        } catch (Exception e2) {
            logger.error(e2);
            return e.toString();
        }
    }

    public String getResponse(boolean success, String message) {
        JSONObject result = new JSONObject();
        result.put("success", true);
        result.put("message", message);
        return result.toJSONString();
    }

    public String getResponse(boolean success, String message, Exception e) {
        if (success) {
            return getResponse(success, message);
        }
        logger.error(StringUtils.getStackTrace(e));
        String errorMessage = getErrorMessage(e);
        return getResponse(success, message + ":" + errorMessage);
    }

    public String getLoggedInUserId(HttpServletRequest req, HttpServletResponse res) {
        LILoggedInUserUtilitiesBean loginUtils = new LILoggedInUserUtilitiesBean();
        LISystemuserviewRecord userRecord = loginUtils.getLoggedInUser(req, res);
        if (userRecord == null) {
            return "";
        }
        return userRecord.getId();
    }

    public String getStringValue(HttpServletRequest req, String paramName) {
        Object value = req.getParameter(paramName);
        if (value == null) {
            value = "";
        }
        return value.toString();
    }

    public String[] getMultiFormFieldValue(HttpServletRequest req, HttpServletResponse res, String FieldID) {
        return req.getParameterValues(FieldID);
    }

    public boolean getCheckboxFormFieldValue(HttpServletRequest req, HttpServletResponse res, String FieldID) {
        StringUtils bean = new StringUtils();
        String value = req.getParameter(FieldID);
        if (bean.isNull(value)) {
            return false;
        }
        if (value.trim().equals("Y")) {
            return true;
        }
        return false;
    }

    public void setActionMessage(HttpServletRequest req, HttpServletResponse res, String message) {
        setActionMessage(req, res, "AdminActionMessage", message);
    }

    public void setActionMessage(HttpServletRequest req, HttpServletResponse res, String id, String message) {
        HttpSession session = req.getSession(true);
        session.setAttribute(id, message);
    }

    public void setActionMessage(HttpServletRequest req, HttpServletResponse res, String id, Exception exception) {
        HttpSession session = req.getSession(true);
        String stackTrace = StringUtils.getStackTrace(exception);
        session.setAttribute(id, stackTrace);
        logger.error(stackTrace);
    }

    public String getMapValue(HashMap inputMap, String key) {
        if (inputMap == null) {
            return "";
        }
        if (StringUtils.isNullOrEmpty(key)) {
            return "";
        }
        if (!inputMap.containsKey(key)) {
            return "";
        }
        return StringUtils.noNull(inputMap.get(key));
    }

    public String getFormFieldValueEncode(HttpServletRequest req, HttpServletResponse res, String FieldID) {
        String result = getFormFieldValue(req, res, FieldID);
        result = StringEscapeUtils.escapeHtml(result);
        return result;
    }

    public String getFormFieldValue(HttpServletRequest req, HttpServletResponse res, String FieldID) {
        if (FieldID.startsWith("rb")) {
            return StringUtils.noNull(req.getParameter(FieldID));
        }
        if (FieldID.startsWith("tf")) {
            return StringUtils.noNull(req.getParameter(FieldID));
        }
        if (FieldID.startsWith("ta")) {
            return StringUtils.noNull(req.getParameter(FieldID));
        }
        if (FieldID.startsWith("hidden")) {
            return StringUtils.noNull(req.getParameter(FieldID));
        }
        if (FieldID.startsWith("select")) {
            return StringUtils.noNull(req.getParameter(FieldID));
        }
        if (FieldID.startsWith("checkbox")) {
            String checkedFlag = StringUtils.noNull(req.getParameter(FieldID));
            if (checkedFlag.equals("Y")) {
                return "Y";
            } else {
                return "N";
            }
        }
        if (FieldID.startsWith("pw")) {
            String s_password = StringUtils.noNull(req.getParameter(FieldID));
            return s_password;
        }
        return StringUtils.noNull(req.getParameter(FieldID));
    }
}
