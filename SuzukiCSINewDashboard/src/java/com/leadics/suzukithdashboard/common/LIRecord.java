
/*
 * MFBranchRecord.java
 *
 * Copyright (c) ModeFinserver Private Limited
 *
 *
 * This software is the confidential and proprietary information of 
 * ModeFinserver Private Limited ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ModeFinserver Private Limited
 *
 * Project Name             : MODE Mobile Banking
 * Module                   : Mobile Banking V4
 * Author                   : Srinivasa Varma M, ModeFinserver Private Limited
 * Date                     : Mar 23, 2013
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.leadics.utils.LogUtils;
import com.leadics.utils.*;

import org.json.simple.*;
import org.json.simple.parser.*;

public class LIRecord {

    static LogUtils logger = new LogUtils(LIRecord.class.getName());

    private boolean noNullFlag = false;
    private String actionSource;

    private String reqComponent;

    public String getReqComponent() {
        return reqComponent;
    }

    public void setReqComponent(String reqComponent) {
        this.reqComponent = reqComponent;
    }

    public String getLoggID() {
        return loggID;
    }

    public void setLoggID(String loggID) {
        this.loggID = loggID;
    }

    private String loggID;

    public String getActionSource() {
        return StringUtils.noNull(actionSource);
    }

    public void setActionSource(String actionSource) {
        this.actionSource = actionSource;
    }

    public void enableNoNull() {
        noNullFlag = true;
    }

    public void disableNoNull() {
        noNullFlag = false;
    }

    public boolean isNoNullEnabled() {
        return noNullFlag;
    }

    private boolean noNullHTMLFlag = false;

    public void enableNoNullHTML() {
        noNullHTMLFlag = true;
    }

    public void disableNoNullHTML() {
        noNullHTMLFlag = false;
    }

    public boolean isNoNullForHTMLEnabled() {
        return noNullHTMLFlag;
    }

}
