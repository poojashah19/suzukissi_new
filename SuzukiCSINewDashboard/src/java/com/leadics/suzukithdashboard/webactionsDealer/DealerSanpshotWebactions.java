/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.suzukithdashboard.webactionsDealer;

import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.service.LIReportquirescsiService;
import com.leadics.suzukithdashboard.service.LIReportquirescsiService;
import com.leadics.suzukithdashboard.to.LIReportquirescsiRecord;
import com.leadics.utils.LogUtils;
import com.leadics.utils.PropertyUtil;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author varma.sagi
 */
public class DealerSanpshotWebactions {

    static LogUtils logger = new LogUtils(DealerSanpshotWebactions.class.getName());

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {

                if (!StringUtils.isNullOrEmpty(actionType)) {
                    if (actionType.equals("getDealerFactorScores")) {
                        getDealerFactorScores(request, response);
                        return;
                    }
                    if (actionType.equals("getDealerSOPTopBottom")) {
                        getDealerSOPTopBottom(request, response);
                        return;
                    }
                    if (actionType.equals("getDealerFactorTrend")) {
                        getDealerFactorTrend(request, response);
                        return;
                    }
                    if (actionType.equals("getDealerAttributeTrend")) {
                        getDealerAttributeTrend(request, response);
                        return;
                    }
                    if (actionType.equals("getDealerScores")) {
                        getDealerScores(request, response);
                        return;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    public void getDealerScores(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDealerScores'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

        filterList.add("state");
        filterList.add("region");
        filterList.add("wave");
        filterList.add("model");
        filterList.add("city");

        String where_condtion = service.buildWherecondition(filterList, request);

        String strDealer = request.getParameter("dealer");
        if (strDealer != null && !strDealer.equalsIgnoreCase("ALL")) {
            strQuery = StringUtils.replaceString(strQuery, "!dealer!", strDealer, true);
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!dealer!", "%", true);
        }

        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.load2Vals(strQuery, "Wave", "wave", "Score", "Score");
        out.println(jsonArray.toJSONString());
        return;
    }

    public void getDealerFactorScores(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDealerFactorScores'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();
        List<String> bestfilterList = new ArrayList<String>();

        filterList.add("state");
        filterList.add("region");
        filterList.add("wave");
        filterList.add("model");
        filterList.add("city");
        filterList.add("year");

        bestfilterList.add("year");
        bestfilterList.add("wave");
        bestfilterList.add("model");
        bestfilterList.add("bestcity");
        bestfilterList.add("beststate");
        bestfilterList.add("bestregion");

        String where_condtion = service.buildWherecondition(filterList, request);
        String best_where_condtion = service.buildWherecondition(bestfilterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strBestWhereCluse, best_where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);
        String strBestString = service.getBestString(request);
        String strDealer = request.getParameter("dealer");
        if (strDealer != null && !strDealer.equalsIgnoreCase("ALL")) {
            strQuery = StringUtils.replaceString(strQuery, "!dealer!", strDealer, true);
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!dealer!", "%", true);
        }

        JSONArray jsonArray = dao.load3Vals(strQuery, "Factor", "Factor", "Score", "score", "BestScore", "maxscore");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();

        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String factor = (String) tmpJson.get("Factor");
            String Score = (String) tmpJson.get("Score");
            String bestscore = (String) tmpJson.get("BestScore");
            addingJson.put("name", factor);
            addingJson.put("bartitle", Score);
            addingJson.put("y", Integer.parseInt(Score));
            addingJson.put("tooltip", factor + "<br>Factor Score: <b>" + Score + "</b>");
            addingJson.put("scatter", strBestString);
            if (bestscore != null && bestscore != "0" && !bestscore.equalsIgnoreCase("null")) {
                addingJson.put("svalue", Integer.parseInt(bestscore));
            } else {

                addingJson.put("svalue", null);
            }
            finalArray.add(addingJson);
        }

        out.println(finalArray.toJSONString());
        return;
    }

    public void getDealerSOPTopBottom(HttpServletRequest request, HttpServletResponse response) throws Exception {

        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDealerSOPTopBottom'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

        filterList.add("state");
        filterList.add("region");
        filterList.add("wave");
        filterList.add("model");
        filterList.add("city");
        filterList.add("dealer");

        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.load3Vals(strQuery, "Attribute", "Attribute", "Score", "score", "TopOrBottom", "top_or_bottom");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        JSONArray Attributes = new JSONArray();
        JSONArray Scores = new JSONArray();
        JSONArray TopORBottoms = new JSONArray();
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String Attribute = (String) tmpJson.get("Attribute");
            String Score = (String) tmpJson.get("Score");
            String toporbootom = (String) tmpJson.get("TopOrBottom");
            addingJson.put("name", Attribute);
            addingJson.put("bartitle", Score + "%");
            addingJson.put("y", Integer.parseInt(Score));
            addingJson.put("tooltip", Attribute + " <br>SOP Score <b>" + Score + "</b>");
            if (toporbootom.equalsIgnoreCase("Top")) {
                addingJson.put("color", "#7E57C2");
            } else if (toporbootom.equalsIgnoreCase("Bottom")) {
                addingJson.put("color", "#757575");
            }
            finalArray.add(addingJson);
        }

        out.println(finalArray.toJSONString());
        return;
    }

    public void getDealerFactorTrend(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDealerFactorTrend'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

        filterList.add("state");
        filterList.add("region");
        filterList.add("year");
        filterList.add("model");
        filterList.add("city");
        filterList.add("factor");

        String where_condtion = service.buildWherecondition(filterList, request);

        String strDealer = request.getParameter("dealer");
        if (strDealer != null && !strDealer.equalsIgnoreCase("ALL")) {
            strQuery = StringUtils.replaceString(strQuery, "!dealer!", strDealer, true);
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!dealer!", "%", true);
        }

        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.load3Vals(strQuery, "period", "period", "factor_score", "factor_score", "bestscore", "bestscore");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();

        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject jsonobj = new JSONObject();
            String Period = (String) tmpJson.get("period");
            String factor_score = (String) tmpJson.get("factor_score");
            String bestscore = (String) tmpJson.get("bestscore");

            if (bestscore == null) {
                bestscore = "0";
            }

            jsonobj.put("name", Period);
            jsonobj.put("bartitle", factor_score);
            jsonobj.put("y", Integer.parseInt(factor_score));
            jsonobj.put("tooltip", Period + "<br>Factor Score : <b>" + factor_score + "</b>");
            jsonobj.put("svalue", Integer.parseInt(bestscore));
            finalArray.add(jsonobj);
        }

        out.println(finalArray.toJSONString());
        return;
    }

    public void getDealerAttributeTrend(HttpServletRequest request, HttpServletResponse response) {
        try {
            PrintWriter out = response.getWriter();
            LIReportquirescsiService queryService = new LIReportquirescsiService();
            String periodSwitch = request.getParameter("period");
            String csiswitch = request.getParameter("factor");
            LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDealerAttributeTrend'");
            if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Year")) {
                if (csiswitch != null && csiswitch.equalsIgnoreCase("")) {
                    queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getAttributeTrendByWaveYear'");
                } else {
                    queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getAttributeTrendByWaveNotYear'");
                }
            } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Bi-Annual Period")) {
                if (csiswitch != null && csiswitch.equalsIgnoreCase("")) {
                    queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getAttributeTrendByWaveBi'");
                } else {
                    queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getAttributeTrendByWaveNotBi'");
                }
            } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Month")) {
                if (csiswitch != null && csiswitch.equalsIgnoreCase("")) {
                    queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getAttributeTrendByWaveMonth'");
                } else {
                    queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getAttributeTrendByWaveNotMonth'");
                }
            }
            String strQuery = queryRecord.getQuery();
            LIDAO dao = new LIDAO();
            LIService service = new LIService();
            List<String> filterList = new ArrayList<String>();
            filterList.add("state");
            filterList.add("region");
            filterList.add("year");
            filterList.add("model");
            filterList.add("city");
            filterList.add("attributefactor");
            if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Year")) {
            } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Bi-Annual")) {
                filterList.add("biannual");
            } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Month")) {
//                        filterList.add("month");
            }
            String where_condtion = service.buildWherecondition(filterList, request);
            String strDealer = request.getParameter("dealer");
            if (strDealer != null && !strDealer.equalsIgnoreCase("ALL")) {
                strQuery = StringUtils.replaceString(strQuery, "!dealer!", strDealer, true);
            } else {
                strQuery = StringUtils.replaceString(strQuery, "!dealer!", "%", true);
            }
            strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
            JSONArray jsonArray = dao.load4Vals(strQuery, "period", "period", "attribute", "attribute", "score", "score", "bestscore", "bestscore");
            System.out.println(jsonArray.toJSONString());
            Iterator i = jsonArray.iterator();
            JSONArray finalArray = new JSONArray();
            LinkedHashMap periods = new LinkedHashMap();
            LinkedHashMap attributes = new LinkedHashMap();
            LinkedHashMap scores = new LinkedHashMap();
            LinkedHashMap bestscores = new LinkedHashMap();

            while (i.hasNext()) {
                JSONObject tmpJson = (JSONObject) i.next();
                String period = (String) tmpJson.get("period");
                String attribute = (String) tmpJson.get("attribute");
                String score = (String) tmpJson.get("score");
                String bestscore = (String) tmpJson.get("bestscore");
                System.out.println(tmpJson.toJSONString());
                periods.put(period, period);
                attributes.put(attribute.trim(), attribute.trim());
                scores.put(period + "-" + attribute.trim(), score);
                bestscores.put(period + "-" + attribute.trim(), bestscore);
            }
            Iterator itimeline = periods.keySet().iterator();

            while (itimeline.hasNext()) {
                JSONObject jsonbject = new JSONObject();
                String timeline = (String) itimeline.next();
                System.out.println("timeline:" + timeline);
                jsonbject.put("name", timeline);
                Iterator iattribute = attributes.keySet().iterator();
                JSONArray tmpJsonArray = new JSONArray();
                while (iattribute.hasNext()) {
                    JSONObject tmpJsonObj = new JSONObject();
                    String attribute = (String) iattribute.next();
                    String score = (String) scores.get(timeline + "-" + attribute);
                    if (score != null) {
                        tmpJsonObj.put("y", Double.parseDouble(score));
                    } else {
                        tmpJsonObj.put("y", Double.parseDouble("0.0"));
                    }
                    tmpJsonObj.put("bartitle", score);
                    tmpJsonObj.put("name", attribute.trim());
                    tmpJsonObj.put("tooltip", "<b>" + attribute + "<br>" + score + "</b>");
                    tmpJsonArray.add(tmpJsonObj);
                }
                System.out.println(tmpJsonArray.toJSONString());
                jsonbject.put("data", tmpJsonArray);
                finalArray.add(jsonbject);
            }

            out.println(finalArray.toJSONString());
            return;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
