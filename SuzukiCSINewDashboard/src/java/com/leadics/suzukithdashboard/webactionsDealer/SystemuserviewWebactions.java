/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    package com.leadics.suzukithdashboard.webactionsDealer;

import com.leadics.suzukithdashboard.controller.LISystemuserController;
import com.leadics.suzukithdashboard.service.LISystemuserService;
import com.leadics.suzukithdashboard.to.LISystemuserRecord;
import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author varma.sagi
 */
public class SystemuserviewWebactions extends LISystemuserController {

    static LogUtils logger = new LogUtils(SystemuserviewWebactions.class.getName());

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {
                super.processWebRequest(request, response, actionType);

                if (!StringUtils.isNullOrEmpty(actionType)) {
                    if (actionType.equals("Login")) {
                        validateLogin(request, response);
                        return;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    public void validateLogin(HttpServletRequest request, HttpServletResponse response) throws Exception {

        PrintWriter out = response.getWriter();
        LISystemuserController objLISystemuserController = new LISystemuserController();
        LISystemuserRecord objLISystemuserRecord = objLISystemuserController.loadJSONFormLISystemuserRecord(request, response);
        LISystemuserService objLISystemuserService = new LISystemuserService();
        objLISystemuserRecord = objLISystemuserService.searchFirstLISystemuserRecord(objLISystemuserRecord);
        if (objLISystemuserRecord != null) {
            out.println(objLISystemuserRecord.getJSONObject().toJSONString());
            return;
        } else {
            out.println("Not valid user");
            return;
        }

    }

}
