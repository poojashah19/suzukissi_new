/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.suzukithdashboard.webactionsDealer;

import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.service.LIReportquirescsiService;
import com.leadics.suzukithdashboard.to.LIReportquirescsiRecord;
import com.leadics.utils.LogUtils;
import com.leadics.utils.PropertyUtil;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author varma.sagi
 */
public class SOPPerformance {

    static LogUtils logger = new LogUtils(PriorityAnalysis.class.getName());

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {

                if (!StringUtils.isNullOrEmpty(actionType)) {
                    if (actionType.equals("getSOPPerformance")) {
                        getSOPPerformance(request, response);
                        return;
                    }
                    if (actionType.equals("getSOPTrend")) {
                        getSOPTrend(request, response);
                        return;
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    public void getSOPTrend(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String periodSwitch = request.getParameter("period");
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getSOPTrendRegion'");
        if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Year")) {
            queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getSOPTrendRegionYear'");
        } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Bi-Annual Period")) {
            queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getSOPTrendRegionBi'");
        } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Month")) {
            queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getSOPTrendRegionMonth'");
        }
        String strQuery = queryRecord.getQuery();
        String strCountQuery = queryRecord.getCountquery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

//        filterList.add("region");
        filterList.add("wave");
        filterList.add("model");
        filterList.add("city");
        filterList.add("attribute");
        if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Year")) {
                 filterList.add("year");
        } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Bi-Annual")) {
            filterList.add("biannual");
        } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Month")) {
            filterList.add("month");
        }
        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strCountQuery = StringUtils.replaceString(strCountQuery, service.strWhereCluse, where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);

        String regionName = "region";
        String strRegion = request.getParameter("region");
        if (strRegion != null && !strRegion.equalsIgnoreCase("ALL")) {
            strCountQuery = StringUtils.replaceString(strCountQuery, "!region!", "region like '" + strRegion + "'", true);
            strQuery = StringUtils.replaceString(strQuery, "!region!", "region like '" + strRegion + "'", true);
            regionName = strRegion;
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!region!", " 1=1 ", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!region!", " 1=1 ", true);
        }

        String strState = request.getParameter("state");
        if (strState != null && !strState.equalsIgnoreCase("ALL")) {
            strQuery = StringUtils.replaceString(strQuery, "!state!", "state like '" + strState + "'", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!state!", "state like '" + strState + "'", true);
            regionName = dao.loadString("SELECT DISTINCT region FROM sop_aggregate WHERE state like '" + strState + "' limit 1");
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!state!", " 1=1 ", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!state!", " 1=1 ", true);
        }

        String strDealer = request.getParameter("dealer");
        String strCompareTo = request.getParameter("compareto");

        if (strDealer != null && !strDealer.equalsIgnoreCase("ALL")) {
            strQuery = StringUtils.replaceString(strQuery, "!dealer!", "dealer like '" + strDealer + "'", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!dealer!", "dealer like '" + strDealer + "'", true);
            regionName = dao.loadString("SELECT DISTINCT region FROM sop_aggregate WHERE dealer like '" + strDealer + "' limit 1");
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!dealer!", " 1=1 ", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!dealer!", " 1=1 ", true);
        }

        String avgString = "Study Average";
        String BestString = "Study Best";
        if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strRegion != null && !strRegion.equalsIgnoreCase("ALL")) {
            avgString = regionName + " Average";
            BestString = regionName + " Best";
            strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", "region like '" + strRegion + "'", true);
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strState != null && !strState.equalsIgnoreCase("ALL")) {
            avgString = regionName + " Average";
            BestString = regionName + " Best";
            strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", " region like (SELECT DISTINCT region FROM sop_aggregate WHERE state like '" + strState + "' limit 1) ", true);
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strDealer != null && !strDealer.equalsIgnoreCase("ALL")) {
            avgString = regionName + " Average";
            BestString = regionName + " Best";
            strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", " region like (SELECT DISTINCT region FROM sop_aggregate WHERE dealer like '" + strDealer + "' limit 1) ", true);
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", " 1=1 ", true);
        }

        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
         System.out.println("DEbug 44"+strQuery);
        JSONArray jsonArray = dao.load4Vals(strQuery, "period", "period", "score", "score", "RegionAvg", "RegionAvg", "RegionBest", "RegionBest");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        System.out.println("DEbug 2"+jsonArray.toJSONString());
        System.out.println("LENGTH"+jsonArray.toString().length());
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject jsonobj = new JSONObject();
            
            System.out.println("Entered");
            String period = (String) tmpJson.get("period");
            String score = (String) tmpJson.get("score");
            String average = (String) tmpJson.get("RegionAvg");
            String best = (String) tmpJson.get("RegionBest");

            jsonobj.put("name", period);
            jsonobj.put("bartitle", score + "%");
            jsonobj.put("y", Integer.parseInt(score));
//            jsonobj.put("name", period + " <br> <b>" + score + "</b>");

            jsonobj.put("name", period);
            jsonobj.put("linename", avgString);
            if (average == null) {
                jsonobj.put("linescore", null);
            } else {
                jsonobj.put("linescore", Integer.parseInt(average));
            }

            jsonobj.put("bestname", BestString);
            if (best == null) {
                jsonobj.put("bestvalue", null);
                jsonobj.put("bestvalueName", null);
            } else {
                jsonobj.put("bestvalue", Integer.parseInt(best));
                jsonobj.put("bestvalueName", best + "%");
            }

            finalArray.add(jsonobj);
        }

        String count = dao.loadString(strCountQuery);
        JSONObject obj = new JSONObject();
        obj.put("samplecount", count);
        obj.put("data", finalArray);
        out.println(obj.toJSONString());
        return;
    }

    public void getSOPPerformance(HttpServletRequest request, HttpServletResponse response) throws Exception {


        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getSOPPerformance'");
        String strQuery = queryRecord.getQuery();
        String strCountQuery = queryRecord.getCountquery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        List<String> filterList = new ArrayList<String>();

//        filterList.add("region");
        filterList.add("wave");
        filterList.add("model");
        filterList.add("city");

        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strCountQuery = StringUtils.replaceString(strCountQuery, service.strWhereCluse, where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        String strDealer = request.getParameter("dealer");
        String strRegion = request.getParameter("region");
        String regionName = "region";
        String strCompareTo = request.getParameter("compareto");

        if (strRegion != null && !strRegion.equalsIgnoreCase("ALL")) {
            strQuery = StringUtils.replaceString(strQuery, "!region!", "region like '" + strRegion + "'", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!region!", "region like '" + strRegion + "'", true);
            regionName = strRegion;
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!region!", " 1=1 ", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!region!", " 1=1 ", true);
        }

        String strState = request.getParameter("state");
        if (strState != null && !strState.equalsIgnoreCase("ALL")) {
            strQuery = StringUtils.replaceString(strQuery, "!state!", "state like '" + strState + "'", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!state!", "state like '" + strState + "'", true);
            regionName = dao.loadString("SELECT DISTINCT region FROM sop_aggregate WHERE state like '" + strState + "' limit 1");
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!state!", " 1=1 ", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!state!", " 1=1 ", true);
        }


        if (strDealer != null && !strDealer.equalsIgnoreCase("ALL")) {
            strQuery = StringUtils.replaceString(strQuery, "!dealer!", "dealer like '" + strDealer + "'", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!dealer!", "dealer like '" + strDealer + "'", true);
            regionName = dao.loadString("SELECT DISTINCT region FROM sop_aggregate WHERE dealer like '" + strDealer + "' limit 1");
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!dealer!", " 1=1 ", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!dealer!", " 1=1 ", true);
        }

        String avgString = "Study Average";
        String BestString = "Study Best";
        if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strRegion != null && !strRegion.equalsIgnoreCase("ALL")) {
            avgString = regionName + " Average";
            BestString = regionName + " Best";
            strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", "region like '" + strRegion + "'", true);
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strState != null && !strState.equalsIgnoreCase("ALL")) {
            avgString = regionName + " Average";
            BestString = regionName + " Best";
            strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", " region like (SELECT DISTINCT region FROM sop_aggregate WHERE state like '" + strState + "' limit 1) ", true);
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strDealer != null && !strDealer.equalsIgnoreCase("ALL")) {
            avgString = regionName + " Average";
            BestString = regionName + " Best";
            strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", " region like (SELECT DISTINCT region FROM sop_aggregate WHERE dealer like '" + strDealer + "' limit 1) ", true);
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", " 1=1 ", true);
        }

        JSONArray jsonArray = dao.load4Vals(strQuery, "attribute", "attribute", "dealerscore", "dealerscore", "regionavg", "regionavg", "regionbest", "regionbest");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject jsonobj = new JSONObject();
            String attribute = (String) tmpJson.get("attribute");
            String dealerscore = (String) tmpJson.get("dealerscore");
            String regionavg = (String) tmpJson.get("regionavg");
            String rgionbest = (String) tmpJson.get("regionbest");
            System.out.println("regionavg:" + regionavg);
            System.out.println("rgionbest:" + rgionbest);


            jsonobj.put("name", attribute);
            jsonobj.put("bartitle", dealerscore + "%");
            jsonobj.put("y", Integer.parseInt(dealerscore));
            jsonobj.put("tooltip", attribute + "<br>" + dealerscore + "%");
            jsonobj.put("scatterbest", BestString);
            jsonobj.put("scatteravg", avgString);
            if (regionavg == null) {
                jsonobj.put("svalue", null);
            } else {
                jsonobj.put("svalue", Integer.parseInt(regionavg));
            }

            if (rgionbest == null) {
                jsonobj.put("bvalue", null);
            } else {
                jsonobj.put("bvalue", Integer.parseInt(rgionbest));
            }

            finalArray.add(jsonobj);
        }
        String count = dao.loadString(strCountQuery);
        JSONObject obj = new JSONObject();
        obj.put("samplecount", count);
        obj.put("data", finalArray);
        out.println(obj.toJSONString());
        return;
    }

}
