/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.suzukithdashboard.webactionsDealer;

import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.service.LIReportquirescsiService;
import com.leadics.suzukithdashboard.to.LIReportquirescsiRecord;
import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author varma.sagi
 */
public class LoyaltyAndAdvocacy {

    String langCode = "EN";
    static LogUtils logger = new LogUtils(LoyaltyAndAdvocacy.class.getName());

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {
                String lang = request.getParameter("langCode");
                if (lang != null) {
                    langCode = lang;
                }
                if (!StringUtils.isNullOrEmpty(actionType)) {
                    if (actionType.equals("getLoyaltyAndAdvocacy")) {
                        getLoyaltyAndAdvocacy(request, response);
                        return;
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    public void getLoyaltyAndAdvocacy(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_dealer WHERE reportname='getLoyaltyAndAdvocacy'");
        String strQuery = queryRecord.getQuery();
        String strCountQuery = queryRecord.getCountquery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();
        
//        filterList.add("zone");
//        filterList.add("region");
//        filterList.add("model");
//        filterList.add("dealer");
        filterList.add("year");

        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        String strCompareTo = request.getParameter("changeloyalSwitch");
        String strDealer = request.getParameter("dealer");
        String strDealergroup = request.getParameter("dealergroup");
//        String zone=dao.loadString("SELECT DISTINCT zone FROM sop_aggregate WHERE dealer like \"" + strDealer + "\" limit 1");
        String region = "";

        if (strDealer != null && !strDealer.equalsIgnoreCase("Study Total") && !strDealer.equalsIgnoreCase("All")) {
//            strCompareTo="dealer";
            region = dao.loadString("SELECT DISTINCT region FROM loyaltyandadvocacy_aggregate WHERE dealer like \"" + strDealer + "\" limit 1");
        } else if (strDealergroup != null && !strDealergroup.equalsIgnoreCase("Study Total") && !strDealergroup.equalsIgnoreCase("All")) {
            //dg region mapping from dg_region table
            region = dao.loadString("SELECT DISTINCT region FROM dg_region WHERE dealergroup like \"" + strDealergroup + "\" limit 1");
        }

//       System.out.println("strCompareTo::::::::::::::::::"+strCompareTo);
//        System.out.println(region);
        if (strCompareTo != null && strCompareTo.equalsIgnoreCase("dealergoup")) {
            strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", "dealergroup like \"" + strDealergroup + "\"", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!regiondealer!", "dealergroup like \"" + strDealergroup + "\"", true);
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("dealer")) {
            strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", "dealer like \"" + strDealer + "\"", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!regiondealer!", "dealer like \"" + strDealer + "\"", true);
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region")) {
             System.out.println("DGregion::::: " +region);
            strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", "region like \"" + region + "\"", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!regiondealer!", "region like \"" + region + "\"", true);
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("Study Total")) {
            strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", " 1=1", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!regiondealer!", " 1=1", true);
        }

        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strCountQuery = StringUtils.replaceString(strCountQuery, service.strWhereCluse, where_condtion, true);

        JSONArray jsonArray = dao.load5Vals(strQuery, "AttributeName", "AttributeName", "DefinitelyWouldNotCount", "DefinitelyWouldNotCount", "DefinitelyWouldCount", "DefinitelyWouldCount", "ProbablyWouldCount", "ProbablyWouldCount", "ProbablyWouldNotCount", "ProbablyWouldNotCount");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        LinkedHashMap attributes = new LinkedHashMap();
        LinkedHashMap nIndexs = new LinkedHashMap();
        LinkedHashMap DefinitelyWoulds = new LinkedHashMap();

        nIndexs.put("Definitely Will Not", "Definitely Will Not");
        nIndexs.put("Probably Will Not", "Probably Will Not");
        nIndexs.put("Probably Will", "Probably Will");
        nIndexs.put("Definitely Will", "Definitely Will");

        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            String Attribute = (String) tmpJson.get("AttributeName");
            String DefinitelyWouldNotCount = (String) tmpJson.get("DefinitelyWouldNotCount");
            String ProbablyWouldNotCount = (String) tmpJson.get("ProbablyWouldNotCount");
            String ProbablyWouldCount = (String) tmpJson.get("ProbablyWouldCount");
            String DefinitelyWouldCount = (String) tmpJson.get("DefinitelyWouldCount");
            Attribute = Attribute.trim();

            attributes.put(Attribute, Attribute);

            DefinitelyWoulds.put(Attribute + "-" + "Definitely Will Not", DefinitelyWouldNotCount);
            DefinitelyWoulds.put(Attribute + "-" + "Probably Will Not", ProbablyWouldNotCount);
            DefinitelyWoulds.put(Attribute + "-" + "Probably Will", ProbablyWouldCount);
            DefinitelyWoulds.put(Attribute + "-" + "Definitely Will", DefinitelyWouldCount);
        }

        Iterator iattributes = attributes.keySet().iterator();
        while (iattributes.hasNext()) {

            String graphflag = "";
            JSONObject jsonobject = new JSONObject();

            String attribute = (String) iattributes.next();
            System.out.println("Attribute:" + attribute);
            if (attribute.equalsIgnoreCase("Recommend Dealer")) {
                graphflag = " ";
                System.out.println("Graphflag in" + graphflag);

            }
            System.out.println("Graphflag" + graphflag);

            String attributetemp = attribute;
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                attributetemp = service.getTranslateString(attribute, langCode);

            }
            System.out.println("Attribute:" + attribute);
            jsonobject.put("chartheading", attributetemp);
            Iterator ivalues = nIndexs.keySet().iterator();
            JSONArray tmpJsonArray = new JSONArray();
            while (ivalues.hasNext()) {

                JSONObject tmpJsonObj = new JSONObject();
                String nIndex = (String) ivalues.next();
                System.out.println("nindex : " + graphflag + nIndex);
                String DefinitelyWouldtemp = "% Definitely Will";
                String DefinitelyWould = (String) DefinitelyWoulds.get(attribute + "-" + nIndex);

                System.out.println("DefinitelyWould : " + DefinitelyWould);
                if (DefinitelyWould == null) {
                    DefinitelyWould = "0";
                }
                String nIndextemp = nIndex;

                JSONObject jsonobj = new JSONObject();
                if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
//                    DefinitelyWouldtemp = "% แน่นอนจะ";
                    nIndextemp = service.getTranslateString(graphflag + nIndex, langCode);
                }
                jsonobj.put("name", nIndextemp);
                //jsonobj.put("axisname", DefinitelyWouldtemp);

                JSONArray tmp = new JSONArray();
                JSONObject tmpObj = new JSONObject();
                tmpObj.put("y", Double.parseDouble(DefinitelyWould));
                tmpObj.put("bartitle", DefinitelyWould + "%");
                tmpObj.put("tooltip", nIndextemp + "<br><b>" + DefinitelyWould + "%</b>");
                tmp.add(tmpObj);
                jsonobj.put("data", tmp);

                tmpJsonArray.add(jsonobj);

            }
            jsonobject.put("chartdata", tmpJsonArray);
            finalArray.add(jsonobject);
        }
        String count = dao.loadString(strCountQuery);
        JSONObject obj = new JSONObject();
        obj.put("samplecount", count);
        obj.put("data", finalArray);
        out.println(obj.toJSONString());
//        out.println(finalArray.toJSONString());
        return;
}
}
