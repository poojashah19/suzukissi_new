/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.suzukithdashboard.webactionsDealer;

import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.service.LIReportquirescsiService;
import com.leadics.suzukithdashboard.to.LIReportquirescsiRecord;
import com.leadics.utils.LogUtils;
import com.leadics.utils.PropertyUtil;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author varma.sagi
 */
public class TrendAnalysisWebactions {

    static LogUtils logger = new LogUtils(TrendAnalysisWebactions.class.getName());

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {

                if (!StringUtils.isNullOrEmpty(actionType)) {
                    if (actionType.equals("getTrendByWave")) {
                        getTrendByWave(request, response);
                        return;
                    }
                    if (actionType.equals("getFactorTrendByWave")) {
                        getFactorTrendByWave(request, response);
                        return;
                    }
                    if (actionType.equals("getAttributeTrendByWave")) {
                        getAttributeTrendByWave(request, response);
                        return;
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    public void getTrendByWave(HttpServletRequest request, HttpServletResponse response) throws Exception {
//        PrintWriter out = response.getWriter();
//        LIReportquirescsiService queryService = new LIReportquirescsiService();
//        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_dealer WHERE reportname='getTrendByWave'");
//        String strQuery = queryRecord.getQuery();
//        LIDAO dao = new LIDAO();
//        LIService service = new LIService();
//
//        List<String> bestfilterList = new ArrayList<String>();
//        List<String> filterList = new ArrayList<String>();
//
//        filterList.add("state");
//        filterList.add("region");
//        filterList.add("year");
//        filterList.add("model");
//        filterList.add("city");
//        filterList.add("dealer");
//
//        bestfilterList.add("wave");
//        bestfilterList.add("model");
//        bestfilterList.add("bestcity");
//        bestfilterList.add("beststate");
//        bestfilterList.add("bestregion");
//
//        String where_condtion = service.buildWherecondition(filterList, request);
//        String best_where_condtion = service.buildWherecondition(bestfilterList, request);
//        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
//        strQuery = StringUtils.replaceString(strQuery, service.strBestWhereCluse, best_where_condtion, true);
//        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
//        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);
//        String strBestString = service.getBestString(request);
//
//        JSONArray jsonArray = dao.load3Vals(strQuery, "period", "period", "csi_score", "csi_score", "bestscore", "bestscore");
//        Iterator i = jsonArray.iterator();
//        JSONArray finalArray = new JSONArray();
//
//        while (i.hasNext()) {
//            JSONObject tmpJson = (JSONObject) i.next();
//            JSONObject jsonobj = new JSONObject();
//            String Period = (String) tmpJson.get("period");
//            String csi_score = (String) tmpJson.get("csi_score");
//            String bestscore = (String) tmpJson.get("bestscore");
//            if (bestscore == null) {
//                bestscore = "0";
//            }
//            jsonobj.put("name", Period);
//            jsonobj.put("bartitle", csi_score);
//            jsonobj.put("y", Integer.parseInt(csi_score));
//            jsonobj.put("tooltip", Period + "<br> Score : <b>" + csi_score + "</b>");
//            if (bestscore != null && bestscore !="0" && !bestscore.equalsIgnoreCase("null")) {
//                jsonobj.put("svalue", Integer.parseInt(bestscore));
//            } else {
//                jsonobj.put("svalue", null);
//            }
//            jsonobj.put("scatter", strBestString);
//            finalArray.add(jsonobj);
//        }
//
//        out.println(finalArray.toJSONString());
//        return;

        String periodSwitch = request.getParameter("period");
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getTrendByWave'");
        if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Year")) {
            queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getTrendByWaveYear'");
        } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Bi-Annual Period")) {
            queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getTrendByWaveBi'");
        } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Month")) {
            queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getTrendByWaveMonth'");
        }
        String strQuery = queryRecord.getQuery();
        String strCountQuery = queryRecord.getCountquery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> bestfilterList = new ArrayList<String>();
        List<String> filterList = new ArrayList<String>();

        filterList.add("state");
        filterList.add("region");
        filterList.add("year");
        filterList.add("model");
        filterList.add("city");
        filterList.add("dealer");

//        bestfilterList.add("wave");
        bestfilterList.add("model");
        bestfilterList.add("dealer");
        bestfilterList.add("beststate");
        bestfilterList.add("bestregion");
        if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Year")) {
//                 filterList.add("year"); 
        } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Bi-Annual")) {
            filterList.add("biannual");
        } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Month")) {
            filterList.add("month");
        }
        String where_condtion = service.buildWherecondition(filterList, request);

        String best_where_condtion = service.buildWherecondition(bestfilterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strCountQuery = StringUtils.replaceString(strCountQuery, service.strWhereCluse, where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strBestWhereCluse, best_where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);
        String strBestString = service.getBestString(request);

        String samplecount = dao.loadString(strCountQuery, "samplecount");

        JSONArray jsonArray = dao.load3Vals(strQuery, "period", "period", "csi_score", "csi_score", "bestscore", "bestscore");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject jsonobj = new JSONObject();
            String Period = (String) tmpJson.get("period");
            String csi_score = (String) tmpJson.get("csi_score");
            String bestscore = (String) tmpJson.get("bestscore");
            if (bestscore == null) {
                bestscore = "0";
            }
            jsonobj.put("name", Period);
            jsonobj.put("bartitle", csi_score);
            jsonobj.put("y", Integer.parseInt(csi_score));
            jsonobj.put("tooltip", Period + "<br>CSI Score : <b>" + csi_score + "</b>");
            if (bestscore != null && bestscore != "0" && !bestscore.equalsIgnoreCase("null")) {
                jsonobj.put("svalue", Integer.parseInt(bestscore));
            } else {
                jsonobj.put("svalue", null);
            }
            jsonobj.put("scatter", strBestString);
            finalArray.add(jsonobj);
        }
        JSONObject obj = new JSONObject();
        obj.put("samplecount", samplecount);
        obj.put("data", finalArray);
        out.println(obj.toJSONString());
        return;
    }

    public void getFactorTrendByWave(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        String periodSwitch = request.getParameter("period");
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_dealer WHERE reportname='getFactorTrendByWave'");
        if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Year")) {
            queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getFactorTrendByWaveYear'");
        } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Bi-Annual Period")) {
            queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getFactorTrendByWaveBi'");
        } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Month")) {
            queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getFactorTrendByWaveMonth'");
        }
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> bestfilterList = new ArrayList<String>();
        List<String> filterList = new ArrayList<String>();

        filterList.add("state");
        filterList.add("region");
        filterList.add("year");
        filterList.add("model");
        filterList.add("city");
        filterList.add("factor");
        filterList.add("dealer");

        bestfilterList.add("wave");
        bestfilterList.add("region");
        bestfilterList.add("year");
        bestfilterList.add("factor");
        bestfilterList.add("model");
        bestfilterList.add("dealer");
        bestfilterList.add("beststate");
        bestfilterList.add("bestregion");
        if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Year")) {
//                 filterList.add("year"); 
        } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Bi-Annual")) {
            filterList.add("biannual");
        } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Month")) {
            filterList.add("month");
        }
        String where_condtion = service.buildWherecondition(filterList, request);
        String best_where_condtion = service.buildWherecondition(bestfilterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strBestWhereCluse, best_where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);
        String strBestString = service.getBestString(request);

        JSONArray jsonArray = dao.load3Vals(strQuery, "period", "period", "factor_score", "factor_score", "bestscore", "bestscore");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();

        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject jsonobj = new JSONObject();
            String Period = (String) tmpJson.get("period");
            String factor_score = (String) tmpJson.get("factor_score");
            String bestscore = (String) tmpJson.get("bestscore");
            if (bestscore == null) {
                bestscore = "0";
            }
            jsonobj.put("name", Period);
            jsonobj.put("bartitle", factor_score);
            jsonobj.put("y", Integer.parseInt(factor_score));
            jsonobj.put("tooltip", Period + "<br>Factor Score : <b>" + factor_score + "</b>");
            if (bestscore != null && bestscore != "0" && !bestscore.equalsIgnoreCase("null")) {
                jsonobj.put("svalue", Integer.parseInt(bestscore));
            } else {
                jsonobj.put("svalue", null);
            }
            jsonobj.put("scatter", strBestString);
            finalArray.add(jsonobj);
        }

        out.println(finalArray.toJSONString());
        return;
    }

    public void getAttributeTrendByWave(HttpServletRequest request, HttpServletResponse response) {
        try {
            PrintWriter out = response.getWriter();
            LIReportquirescsiService queryService = new LIReportquirescsiService();
            String periodSwitch = request.getParameter("period");
            LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_dealer WHERE reportname='getAttributeTrendByWave'");
            if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Year")) {
                queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getAttributeTrendByWaveYear'");
            } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Bi-Annual Period")) {
                queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getAttributeTrendByWaveBi'");
            } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Month")) {
                queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getAttributeTrendByWaveMonth'");
            }
            String strQuery = queryRecord.getQuery();
            LIDAO dao = new LIDAO();
            LIService service = new LIService();
            List<String> filterList = new ArrayList<String>();
            filterList.add("state");
            filterList.add("region");
            filterList.add("year");
            filterList.add("model");
            filterList.add("city");
            filterList.add("attributefactor");
            filterList.add("dealer");
            if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Year")) {
//                 filterList.add("year"); 
            } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Bi-Annual")) {
                filterList.add("biannual");
            } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Month")) {
                filterList.add("month");
            }
            String where_condtion = service.buildWherecondition(filterList, request);
            strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
            JSONArray jsonArray = dao.load4Vals(strQuery, "period", "period", "attribute", "attribute", "score", "score", "bestscore", "bestscore");
            System.out.println(jsonArray.toJSONString());
            Iterator i = jsonArray.iterator();
            JSONArray finalArray = new JSONArray();
            LinkedHashMap periods = new LinkedHashMap();
            LinkedHashMap attributes = new LinkedHashMap();
            LinkedHashMap scores = new LinkedHashMap();
            LinkedHashMap bestscores = new LinkedHashMap();

            while (i.hasNext()) {
                JSONObject tmpJson = (JSONObject) i.next();
                String period = (String) tmpJson.get("period");
                String attribute = (String) tmpJson.get("attribute");
                String score = (String) tmpJson.get("score");
                String bestscore = (String) tmpJson.get("bestscore");
                System.out.println(tmpJson.toJSONString());
                periods.put(period, period);
                attributes.put(attribute.trim(), attribute.trim());
                scores.put(period + "-" + attribute.trim(), score);
                bestscores.put(period + "-" + attribute.trim(), bestscore);
            }
            Iterator itimeline = periods.keySet().iterator();
//          
            while (itimeline.hasNext()) {
                JSONObject jsonbject = new JSONObject();
                String timeline = (String) itimeline.next();
                System.out.println("timeline:" + timeline);
                jsonbject.put("name", timeline);
                Iterator iattribute = attributes.keySet().iterator();
                JSONArray tmpJsonArray = new JSONArray();
                while (iattribute.hasNext()) {
                    JSONObject tmpJsonObj = new JSONObject();
                    String attribute = (String) iattribute.next();
                    String score = (String) scores.get(timeline + "-" + attribute);
                    if (score != null) {
                        tmpJsonObj.put("y", Double.parseDouble(score));
                    } else {
                        tmpJsonObj.put("y", Double.parseDouble("0.0"));
                    }
                    tmpJsonObj.put("bartitle", score);
                    tmpJsonObj.put("name", attribute.trim());
                    tmpJsonObj.put("tooltip", "<b>" + attribute + "<br>" + score + "</b>");
                    tmpJsonArray.add(tmpJsonObj);
                }
                System.out.println(tmpJsonArray.toJSONString());
                jsonbject.put("data", tmpJsonArray);
                finalArray.add(jsonbject);
            }

            out.println(finalArray.toJSONString());
            return;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
