/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.suzukithdashboard.webactionsDealer;

import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.service.LIReportquirescsiService;
import com.leadics.suzukithdashboard.to.LIReportquirescsiRecord;
import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author varma.sagi
 */
public class ModelAnalysisWebactions {

    static LogUtils logger = new LogUtils(ModelAnalysisWebactions.class.getName());

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {

                if (!StringUtils.isNullOrEmpty(actionType)) {
                    if (actionType.equals("getModelScores")) {
                        getModelScores(request, response);
                        return;
                    }
                    if (actionType.equals("getModelFactorScores")) {
                        getModelFactorScores(request, response);
                        return;
                    }
                    if (actionType.equals("getModelFactorTrend")) {
                        getModelFactorTrend(request, response);
                        return;
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    public void getModelFactorTrend(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        String periodSwitch = request.getParameter("period");
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getModelFactorTrend'");
        if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Year")) {
            queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getModelFactorTrendYear'");
        } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Bi-Annual Period")) {
            queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getModelFactorTrendBi'");
        } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Month")) {
            queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getModelFactorTrendMonth'");
        }
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

        filterList.add("state");
        filterList.add("region");
        filterList.add("year");
        filterList.add("model");
        filterList.add("city");
//        filterList.add("wave");
        if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Year")) {
        } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Bi-Annual")) {
            filterList.add("biannual");
        } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Month")) {
//                filterList.add("month"); 
        }
        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);

        String strCity = request.getParameter("city");

        if (strCity != null && !strCity.equalsIgnoreCase("All")) {
            strQuery = strQuery.replaceAll("!city!", strCity);
        } else {
            strQuery = strQuery.replaceAll("!city!", "%");
        }

        String strFactor = request.getParameter("factor");

        if (strFactor != null) {
            strQuery = strQuery.replaceAll("!factor!", strFactor);
        } else {
            strQuery = strQuery.replaceAll("!factor!", "Service Advisor");
        }

        JSONArray jsonArray = dao.load3Vals(strQuery, "period", "period", "factor_score", "factor_score", "bestscore", "bestscore");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject jsonobj = new JSONObject();
            String Period = (String) tmpJson.get("period");
            String factor_score = (String) tmpJson.get("factor_score");
            String bestscore = (String) tmpJson.get("bestscore");
            if (bestscore == null) {
                bestscore = "0";
            }
            jsonobj.put("name", Period);
            jsonobj.put("bartitle", factor_score);
            jsonobj.put("y", Integer.parseInt(factor_score));
            jsonobj.put("tooltip", Period + "<br>Factor Score : <b>" + factor_score + "</b>");
            jsonobj.put("svalue", Integer.parseInt(bestscore));
            finalArray.add(jsonobj);
        }

        out.println(finalArray.toJSONString());
        return;
    }

    public void getModelFactorScores(HttpServletRequest request, HttpServletResponse response) {
        try {
            PrintWriter out = response.getWriter();
            LIReportquirescsiService queryService = new LIReportquirescsiService();
            LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getModelFactorScores'");
            String strQuery = queryRecord.getQuery();
            LIDAO dao = new LIDAO();
            LIService service = new LIService();
            List<String> filterList = new ArrayList<String>();
            filterList.add("state");
            filterList.add("region");
            filterList.add("year");
            filterList.add("model");
            filterList.add("city");
//            filterList.add("wave");

            String where_condtion = service.buildWherecondition(filterList, request);
            strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
            JSONArray jsonArray = dao.load3Vals(strQuery, "model", "model", "factor", "factor", "score", "score");
            System.out.println(jsonArray.toJSONString());
            Iterator i = jsonArray.iterator();
            JSONArray finalArray = new JSONArray();
            LinkedHashMap models = new LinkedHashMap();
            LinkedHashMap factors = new LinkedHashMap();
            LinkedHashMap scores = new LinkedHashMap();

            while (i.hasNext()) {
                JSONObject tmpJson = (JSONObject) i.next();
                String model = (String) tmpJson.get("model");
                String factor = (String) tmpJson.get("factor");
                String score = (String) tmpJson.get("score");
                String bestscore = (String) tmpJson.get("bestscore");
                System.out.println(tmpJson.toJSONString());
                models.put(model, model);
                factors.put(factor.trim(), factor.trim());
                scores.put(model + "-" + factor.trim(), score);
            }

            Iterator iModels = models.keySet().iterator();

            while (iModels.hasNext()) {
                JSONObject jsonbject = new JSONObject();
                String model = (String) iModels.next();
                System.out.println("model:" + model);
                Iterator ifactor = factors.keySet().iterator();
                jsonbject.put("model", model);
                while (ifactor.hasNext()) {
                    JSONObject tmpJsonObj = new JSONObject();
                    String factor = (String) ifactor.next();
                    String score = (String) scores.get(model + "-" + factor);
                    if (score == null) {
                        score = "NA";
                    }
                    jsonbject.put(factor.trim().replaceAll(" ", ""), score);
                }
                finalArray.add(jsonbject);
            }

            out.println(finalArray.toJSONString());
            return;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getModelScores(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getModelScores'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

        filterList.add("state");
        filterList.add("region");
        filterList.add("year");
        filterList.add("model");
        filterList.add("city");
//        filterList.add("wave");

        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.load2Vals(strQuery, "model", "model", "csi_score", "csi_score");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject jsonobj = new JSONObject();
            String model = (String) tmpJson.get("model");
            String csi_score = (String) tmpJson.get("csi_score");

            jsonobj.put("name", model);
            jsonobj.put("bartitle", csi_score);
            jsonobj.put("y", Integer.parseInt(csi_score));
            jsonobj.put("tooltip", model + "<br>CSI Avg : <b>" + csi_score + "</b>");
            finalArray.add(jsonobj);
        }

        out.println(finalArray.toJSONString());
        return;
    }

}
