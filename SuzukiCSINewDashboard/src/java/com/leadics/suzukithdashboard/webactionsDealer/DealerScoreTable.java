/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.suzukithdashboard.webactionsDealer;

import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.service.LICsidealerscoretableviewService;
import com.leadics.suzukithdashboard.service.LIReportquirescsiService;
import com.leadics.suzukithdashboard.service.LISsidealerscoretableviewService;
import com.leadics.suzukithdashboard.to.LICsidealerscoretableviewRecord;
import com.leadics.suzukithdashboard.to.LIReportquirescsiRecord;
import com.leadics.suzukithdashboard.to.LISsidealerscoretableviewRecord;
import com.leadics.utils.LogUtils;
import com.leadics.utils.PropertyUtil;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author varma.sagi
 */
public class DealerScoreTable {

    String langCode = "EN";

    static LogUtils logger = new LogUtils(DealerScoreTable.class.getName());

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {
                String lang = request.getParameter("langCode");
                if (lang != null) {
                    langCode = lang;
                }

                if (!StringUtils.isNullOrEmpty(actionType)) {
                    if (actionType.equals("getDealerScoreTable1")) {
                        getDealerScoreTable(request, response);
                        return;
                    }
                    if (actionType.equals("getDealerScoreTableDG")) {
                        getDealerScoreTableDG(request, response);
                        return;
                    }
                    if (actionType.equals("getTOP3RowsDealerScore1")) {
                        getTOP3RowsDealerScore(request, response);
                        return;
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    public void getDealerScoreTableDG(HttpServletRequest request, HttpServletResponse response) throws Exception {
           PrintWriter out = response.getWriter();
           LIReportquirescsiService queryService = new LIReportquirescsiService();
           LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDealerScoreTableNew'");

           LIReportquirescsiRecord queryRecord1 = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDealerRankTop3Rows2'");
           String strQuery = queryRecord.getQuery();
           LIService service = new LIService();
           List<String> filterList = new ArrayList<String>();
           String loyalSwitch=request.getParameter("loyalSwitch");
           String strdealergroup = request.getParameter("dealergroup");
           
           if(!loyalSwitch.equalsIgnoreCase("Region")){
                filterList.add("dealergroup");
                filterList.add("region");
            }
                filterList.add("year");
    //          filterList.add("model");
    //          filterList.add("month");
    //          filterList.add("regiona");
    //          filterList.add("quarter");
    //          filterList.add("city");
           if (!langCode.equalsIgnoreCase("EN")) {
               System.out.println("Lang Code :" + langCode);
               strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
           }

           //dg region mapping from dg_region
            if(loyalSwitch.equalsIgnoreCase("Region")){
            strQuery = StringUtils.replaceString(strQuery, "!region!", "AND region IN (SELECT region FROM dg_region WHERE dealergroup='"+strdealergroup+"')", true);
            }
            else{
                strQuery = StringUtils.replaceString(strQuery, "!region!", " ", true);
            }
        
           String where_condtion = service.buildWherecondition(filterList, request);
           strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
           strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);
           strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
           strQuery = StringUtils.replaceString(strQuery, service.strFactor, PropertyUtil.getProperty("factor"), true);

           LIDAO dao = new LIDAO();

           JSONArray jsonArray = dao.load6_Vals(strQuery, "rank", "rank", "colour", "colour", "dealerName", "dealerName", "factorName", "factorName", "score", "score", "region", "region");

           JSONArray array = new JSONArray();
           JSONArray finalarray = new JSONArray();

           LinkedHashMap<String, JSONObject> dealersHash = new LinkedHashMap();

           Iterator i = jsonArray.iterator();
           while (i.hasNext()) {

               JSONObject obj = (JSONObject) i.next();
                String dealer;
               if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                   dealer = service.getTranslateStringWithAsterisk((String) obj.get("dealerName"), langCode);

               } else {
                   dealer = (String) obj.get("dealerName");

               }
               String rank = (String) obj.get("rank");
               if(rank.equalsIgnoreCase("999") || rank.equalsIgnoreCase("989"))
               {
                   rank="";
               }
               String region;
                if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                        region = service.getTranslateString((String) obj.get("region"), langCode);

               } else {
                   region = (String) obj.get("region");

               }
               
               String score = (String) obj.get("score");
               String factor = (String) obj.get("factorName");
               String colour = (String) obj.get("colour");
               JSONObject newJsonObj = null;

               // add if item is not object in list
               if (dealersHash.containsKey(dealer)) {
                   newJsonObj = dealersHash.get(dealer);
                   newJsonObj.put(factor, score);
                   newJsonObj.put(factor + "color", colour);

               } else {
                   newJsonObj = new JSONObject();
                   newJsonObj.put(factor, score);
                   newJsonObj.put(factor + "color", colour);
                   newJsonObj.put("region", region);
                   newJsonObj.put("rank", rank);

               }
               dealersHash.put(dealer, newJsonObj);

           }

           List<String> filterList1 = new ArrayList<String>();
           filterList1.add("dealergroup");
           filterList1.add("region");
           filterList1.add("year");
//           filterList1.add("regiona");
//           filterList1.add("quarter");
//           filterList1.add("month");
//           filterList1.add("city");
           String studyRegion = request.getParameter("region");
           String studyDealerGroup = request.getParameter("dealergroup");
         
           String study2 = "";
           /////////////////////////////////for Dealer Group Login//////////////////////////////////////////////////////      
           if (studyDealerGroup != null && !studyDealerGroup.equals("All") && !studyDealerGroup.equals("Study Total")) {
     
               if (studyRegion != null && !studyRegion.equals("All") && !studyRegion.equals("Study Total")) {
                   study2 = studyRegion;

               } else {
                   study2 = studyDealerGroup;
               }
           } ////////////////for HQ and Regional Login/////////////////////////////////////////////// 
           else {
               if (studyRegion != null && !studyRegion.equals("All") && !studyRegion.equals("Study Total")) {
                   study2 = studyRegion;

               }
               else
               {
                   study2 = "Study Total";
               }
           }

           String strQuery1 = queryRecord1.getQuery();
           String where_condtion1 = service.buildWherecondition(filterList1, request);
           strQuery1 = StringUtils.replaceString(strQuery1, service.strWhereCluse, where_condtion1, true);
           strQuery1 = StringUtils.replaceString(strQuery1, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);
           strQuery1 = StringUtils.replaceString(strQuery1, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
           strQuery1 = StringUtils.replaceString(strQuery1, service.strFactor, PropertyUtil.getProperty("factor"), true);
           JSONArray jsonArray_top3 = dao.load6_Vals(strQuery1, "rank", "rank", "colour", "colour", "dealer", "dealer", "factorName", "factorName", "score", "score", "region", "region");
           LinkedHashMap<String, JSONObject> dealersHash_top3 = new LinkedHashMap();
           
           Iterator i_top3 = jsonArray_top3.iterator();

           while (i_top3.hasNext()) {

               JSONObject obj = (JSONObject) i_top3.next();
            
               String dealer= (String) obj.get("dealer");
                if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                   dealer = service.getTranslateStringWithAsterisk((String) obj.get("dealer"), langCode);
                   System.out.println("dealer"+dealer);

               } else {
                   dealer = (String) obj.get("dealer");

               }
               String rank = (String) obj.get("rank");
               String region= (String) obj.get("region");

               
               String score = (String) obj.get("score");
               String factor = (String) obj.get("factorName");
               String colour = (String) obj.get("colour");
               JSONObject newJsonObj = null;

               // add if item is not object in list
               if (dealersHash_top3.containsKey(dealer)) {
                   newJsonObj = dealersHash_top3.get(dealer);
                   newJsonObj.put(factor, score);
                   newJsonObj.put(factor + "color", colour);
                

               } else {
                   newJsonObj = new JSONObject();
                   newJsonObj.put(factor, score);
                   newJsonObj.put(factor + "color", colour);
                   newJsonObj.put("region", region);
                   newJsonObj.put("rank", rank);
                     if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                            newJsonObj.put("dealer", service.getTranslateStringWithAsterisk(study2, langCode));
                     }
                     else
                     {
                           newJsonObj.put("dealer",study2);
                     }
                
                      

               }
               dealersHash_top3.put(dealer, newJsonObj);

           }
           //adding to array for top3
           Set<String> keys_top3 = dealersHash_top3.keySet();
           Iterator<String> itr_top3 = keys_top3.iterator();
           int count=0;
           while (itr_top3.hasNext()) {
               count++;
               String keyofmap = itr_top3.next();
               
               JSONObject obj = dealersHash_top3.get(keyofmap);
                obj.put("counterfreeze", count);
               finalarray.add(obj);
           }
           //adding to array for dealers
           Set<String> keys = dealersHash.keySet();
           Iterator<String> itr = keys.iterator();;
           while (itr.hasNext()) {

               String keyofmap = itr.next();
               JSONObject obj = dealersHash.get(keyofmap);

               obj.put("dealer", keyofmap);

               finalarray.add(obj);
           }
           finalarray.add(array);
           out.println(finalarray.toJSONString());
           return;
        }
    
     public void getDealerScoreTable(HttpServletRequest request, HttpServletResponse response) throws Exception {
           PrintWriter out = response.getWriter();
           LIReportquirescsiService queryService = new LIReportquirescsiService();
           LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDealerScoreTable2'");

           LIReportquirescsiRecord queryRecord1 = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDealerRankTop3Rows2'");
           String strQuery = queryRecord.getQuery();
           LIService service = new LIService();
           List<String> filterList = new ArrayList<String>();

           filterList.add("dealergroup");
           filterList.add("region");
           filterList.add("year");
//           filterList.add("model");
//           filterList.add("month");
//           filterList.add("regiona");
//           filterList.add("quarter");
//           filterList.add("city");
           if (!langCode.equalsIgnoreCase("EN")) {
               System.out.println("Lang Code :" + langCode);
               strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
           }

           String where_condtion = service.buildWherecondition(filterList, request);
           strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
           strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);
           strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
           strQuery = StringUtils.replaceString(strQuery, service.strFactor, PropertyUtil.getProperty("factor"), true);

           LIDAO dao = new LIDAO();

           JSONArray jsonArray = dao.load6_Vals(strQuery, "rank", "rank", "colour", "colour", "dealerName", "dealerName", "factorName", "factorName", "score", "score", "region", "region");

           JSONArray array = new JSONArray();
           JSONArray finalarray = new JSONArray();

           LinkedHashMap<String, JSONObject> dealersHash = new LinkedHashMap();

           Iterator i = jsonArray.iterator();
           while (i.hasNext()) {

               JSONObject obj = (JSONObject) i.next();
                String dealer;
               if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                   dealer = service.getTranslateStringWithAsterisk((String) obj.get("dealerName"), langCode);

               } else {
                   dealer = (String) obj.get("dealerName");

               }
               String rank = (String) obj.get("rank");
               if(rank.equalsIgnoreCase("999") || rank.equalsIgnoreCase("989"))
               {
                   rank="";
               }
               String region;
                if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                        region = service.getTranslateString((String) obj.get("region"), langCode);

               } else {
                   region = (String) obj.get("region");

               }
               
               String score = (String) obj.get("score");
               String factor = (String) obj.get("factorName");
               String colour = (String) obj.get("colour");
               JSONObject newJsonObj = null;

               // add if item is not object in list
               if (dealersHash.containsKey(dealer)) {
                   newJsonObj = dealersHash.get(dealer);
                   newJsonObj.put(factor, score);
                   newJsonObj.put(factor + "color", colour);

               } else {
                   newJsonObj = new JSONObject();
                   newJsonObj.put(factor, score);
                   newJsonObj.put(factor + "color", colour);
                   newJsonObj.put("region", region);
                   newJsonObj.put("rank", rank);

               }
               dealersHash.put(dealer, newJsonObj);

           }

           List<String> filterList1 = new ArrayList<String>();
           filterList1.add("dealergroup");
           filterList1.add("region");
           filterList1.add("year");
//           filterList1.add("regiona");
//           filterList1.add("quarter");
//           filterList1.add("month");
//           filterList1.add("city");
           String studyRegion = request.getParameter("region");
           String studyDealerGroup = request.getParameter("dealergroup");
         
           String study2 = "";
           /////////////////////////////////for Dealer Group Login//////////////////////////////////////////////////////      
           if (studyDealerGroup != null && !studyDealerGroup.equals("All") && !studyDealerGroup.equals("Study Total")) {
     
               if (studyRegion != null && !studyRegion.equals("All") && !studyRegion.equals("Study Total")) {
                   study2 = studyRegion;

               } else {
                   study2 = studyDealerGroup;
               }
           } ////////////////for HQ and Regional Login/////////////////////////////////////////////// 
           else {
               if (studyRegion != null && !studyRegion.equals("All") && !studyRegion.equals("Study Total")) {
                   study2 = studyRegion;

               }
               else
               {
                   study2 = "Study Total";
               }
           }

           String strQuery1 = queryRecord1.getQuery();
           String where_condtion1 = service.buildWherecondition(filterList1, request);
           strQuery1 = StringUtils.replaceString(strQuery1, service.strWhereCluse, where_condtion1, true);
           strQuery1 = StringUtils.replaceString(strQuery1, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);
           strQuery1 = StringUtils.replaceString(strQuery1, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
           strQuery1 = StringUtils.replaceString(strQuery1, service.strFactor, PropertyUtil.getProperty("factor"), true);
           JSONArray jsonArray_top3 = dao.load6_Vals(strQuery1, "rank", "rank", "colour", "colour", "dealer", "dealer", "factorName", "factorName", "score", "score", "region", "region");
           LinkedHashMap<String, JSONObject> dealersHash_top3 = new LinkedHashMap();
           
           Iterator i_top3 = jsonArray_top3.iterator();

           while (i_top3.hasNext()) {

               JSONObject obj = (JSONObject) i_top3.next();
            
               String dealer= (String) obj.get("dealer");
                if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                   dealer = service.getTranslateStringWithAsterisk((String) obj.get("dealer"), langCode);
                   System.out.println("dealer"+dealer);

               } else {
                   dealer = (String) obj.get("dealer");

               }
               String rank = (String) obj.get("rank");
               String region= (String) obj.get("region");

               
               String score = (String) obj.get("score");
               String factor = (String) obj.get("factorName");
               String colour = (String) obj.get("colour");
               JSONObject newJsonObj = null;

               // add if item is not object in list
               if (dealersHash_top3.containsKey(dealer)) {
                   newJsonObj = dealersHash_top3.get(dealer);
                   newJsonObj.put(factor, score);
                   newJsonObj.put(factor + "color", colour);
                

               } else {
                   newJsonObj = new JSONObject();
                   newJsonObj.put(factor, score);
                   newJsonObj.put(factor + "color", colour);
                   newJsonObj.put("region", region);
                   newJsonObj.put("rank", rank);
                     if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                            newJsonObj.put("dealer", service.getTranslateStringWithAsterisk(study2, langCode));
                     }
                     else
                     {
                           newJsonObj.put("dealer",study2);
                     }
                
                      

               }
               dealersHash_top3.put(dealer, newJsonObj);

           }
           //adding to array for top3
           Set<String> keys_top3 = dealersHash_top3.keySet();
           Iterator<String> itr_top3 = keys_top3.iterator();
           int count=0;
           while (itr_top3.hasNext()) {
               count++;
               String keyofmap = itr_top3.next();
               
               JSONObject obj = dealersHash_top3.get(keyofmap);
                obj.put("counterfreeze", count);
               finalarray.add(obj);
           }
           //adding to array for dealers
           Set<String> keys = dealersHash.keySet();
           Iterator<String> itr = keys.iterator();;
           while (itr.hasNext()) {

               String keyofmap = itr.next();
               JSONObject obj = dealersHash.get(keyofmap);

               obj.put("dealer", keyofmap);

               finalarray.add(obj);
           }
           finalarray.add(array);
           out.println(finalarray.toJSONString());
           return;
        }

    public void getTOP3RowsDealerScore(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord1 = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_dealer WHERE reportname='getDealerRankTop3Rows'");
        LIService service = new LIService();

        LIDAO dao = new LIDAO();
        String result = dao.updateQuery("SET @r1=0,  @r2=0,  @r3=0,  @r4=0,  @r5=0,  @r6=0,  @r7=0, @r8=1000, @r9=10000,@rsum=0, @rorder=0");
        LICsidealerscoretableviewService serviceObj = new LICsidealerscoretableviewService();
        JSONArray finalarray = new JSONArray();
        String topregion = request.getParameter("region");

        String dealer = request.getParameter("dealer");
        dealer = dealer.replaceAll("\\*", "");

        System.out.println("topregion:" + topregion);
        String studytotalLable = "Study Total";
        String Zonelable = "All Zones";
        String regionlable = "All Regions";
        String zone = request.getParameter("zone");
        if (dealer != null && !dealer.equalsIgnoreCase("Study Total") && !dealer.equalsIgnoreCase("All")) {
            zone = dao.loadString("select distinct zone from factor_aggregate where dealer=\"" + dealer + "\" limit 1");
            topregion = dao.loadString("select distinct region from factor_aggregate where dealer=\"" + dealer + "\" limit 1");
            studytotalLable = "Study Total";
            Zonelable = zone;
            regionlable = topregion;
//             String zone = request.getParameter("zone");
        } else if (topregion != null && !topregion.equalsIgnoreCase("Study Total") && !topregion.equalsIgnoreCase("All")) {
            zone = dao.loadString("select distinct zone from region_factor_aggregate where region=\"" + topregion + "\" limit 1");
            dealer = "%";
            studytotalLable = "Study Total";
            Zonelable = zone;
            regionlable = topregion;
        } else if (zone != null && !zone.equalsIgnoreCase("Study Total") && !zone.equalsIgnoreCase("All")) {
            topregion = "%";
            dealer = "%";
            studytotalLable = "Study Total";
            Zonelable = zone;
            regionlable = topregion;
        } else {
            zone = "%";
            topregion = "%";
            dealer = "%";
        }

        String query1 = queryRecord1.getQuery();

        query1 = query1.replaceAll("!region!", topregion);

        query1 = query1.replaceAll("!zone!", zone);
        query1 = query1.replaceAll("!zonedealer!", zone);
        query1 = query1.replaceAll("!dealer!", dealer);
        query1 = query1.replaceAll("!3rdlable!", regionlable);
        query1 = query1.replaceAll("!2ndlable!", Zonelable);
        List<String> filterList1 = new ArrayList<String>();
        filterList1.add("year");
        filterList1.add("zone");
        filterList1.add("biannual");
        filterList1.add("monthtable");
        String where_condtion1 = service.buildWherecondition(filterList1, request);
        query1 = StringUtils.replaceString(query1, service.strWhereCluse, where_condtion1, true);
        LICsidealerscoretableviewRecord[] records1 = serviceObj.loadLICsidealerscoretableviewRecords(query1);

        for (int k = 0; k < records1.length; k++) {
            JSONObject obj = records1[k].getJSONObject();
            String study2 = (String) obj.get("dealer");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                study2 = service.getTranslateString(study2, langCode);
                obj.put("dealer", study2);
            }
            finalarray.add(obj);
        }

        out.println(finalarray.toJSONString());

        return;
    }
}
