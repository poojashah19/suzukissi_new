    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.suzukithdashboard.webactionsDealer;

import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.service.LIReportquirescsiService;
import com.leadics.suzukithdashboard.service.LIReportquiresssiService;
import com.leadics.suzukithdashboard.to.LIReportquirescsiRecord;
import com.leadics.suzukithdashboard.to.LIReportquiresssiRecord;
import com.leadics.utils.LogUtils;
import com.leadics.utils.PropertyUtil;
import com.leadics.utils.StringUtils;
import com.lowagie.text.pdf.hyphenation.TernaryTree;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author varma.sagi
 */
public class SummaryWebactions {

    String langCode = "EN";
    static LogUtils logger = new LogUtils(SummaryWebactions.class.getName());

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");

        try (PrintWriter out = response.getWriter()) {
            try {
                String lang = request.getParameter("langCode");
                if (lang != null) {
                    langCode = lang;
                }
                if (!StringUtils.isNullOrEmpty(actionType)) {
                    
                    if (actionType.equals("getNationalScoresYear")) {
                        getNationalScoresYear(request, response);
                        return;
                    }
                    if (actionType.equals("getCityScore")) {
                        getCityScore(request, response);
                        return;
                    }
                    if (actionType.equals("getNationalScoresBiannual")) {
                        getNationalScoresBiannual(request, response);
                        return;
                    }
                    if (actionType.equals("getNationalScoresMonth")) {
                        getNationalScoresMonth(request, response);
                        return;
                    }
                    //top1 graph
                    if (actionType.equals("getSummaryRegionFactorScores")) {
                        getSummaryRegionFactorScores(request, response);
                        return;
                    }
                    //bot1 graph
                    if (actionType.equals("getRegionalAttributeAverage")) {
                        getRegionalAttributeAverage(request, response);
                        return;
                    }
                    if (actionType.equals("getTop5Bottom5SOP")) {
                        getTop5Bottom5SOP(request, response);
                        return;
                    }
                    
                     if (actionType.equals("getBenchmark")) {
                        getBenchmark(request, response);
                        return;
                    }
                      if (actionType.equals("getBenchmarkyear")) {
                        getBenchmarkyear(request, response);
                        return;
                    }
                      if (actionType.equals("getBenchmarkDealerRegion")) {
                        getBenchmarkDealerRegion(request, response);
                        return;
                    }
                       if (actionType.equals("getBenchmarkDealercity")) {
                        getBenchmarkDealercity(request, response);
                        return;
                    }
                       if (actionType.equals("getBenchmarkbiannual")) {
                        getBenchmarkbiannual(request, response);
                        return;
                    }
                        if (actionType.equals("getBenchmarkmonth")) {
                        getBenchmarkmonth(request, response);
                        return;
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }
  
     
    
     public void getBenchmark(HttpServletRequest request, HttpServletResponse response) throws Exception  {
         String strMeasure=request.getParameter("benchmarkmeasure");
         if(strMeasure!=null && strMeasure.equalsIgnoreCase("best")){
             getBenchmarkbest(request, response);
         } else{
             getBenchmarkaverage(request, response);
         }
     }
       public void getBenchmarkyear(HttpServletRequest request, HttpServletResponse response) throws Exception  {
         String strMeasure=request.getParameter("benchmarkmeasure");
         if(strMeasure!=null && strMeasure.equalsIgnoreCase("best")){
             getBenchmarkbestyear(request, response);
         } else{
             getBenchmarkaverage(request, response);
         }
     }
         public void getBenchmarkbiannual(HttpServletRequest request, HttpServletResponse response) throws Exception  {
         String strMeasure=request.getParameter("benchmarkmeasure");
         if(strMeasure!=null && strMeasure.equalsIgnoreCase("best")){
             getBenchmarkbestbiannual(request, response);
         } else{
             getBenchmarkaverage(request, response);
         }
     }
           public void getBenchmarkmonth(HttpServletRequest request, HttpServletResponse response) throws Exception  {
         String strMeasure=request.getParameter("benchmarkmeasure");
         if(strMeasure!=null && strMeasure.equalsIgnoreCase("best")){
             getBenchmarkbestmonth(request, response);
         } else{
             getBenchmarkaverage(request, response);
         }
     }

    
      public void getBenchmarkaverage(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_dealer WHERE reportname='getBenchmarkaverage'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();
filterList.add("dealergroup");
        filterList.add("region");
        filterList.add("year");
        filterList.add("biannual");
        filterList.add("month");

        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
       
        String strRegion = request.getParameter("region");
        String benchmarkmeasure=request.getParameter("benchmarkmeasure");
        String benchmarkscope=request.getParameter("benchmarkscope");
        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);
        String strYear=request.getParameter("year");
        strQuery = StringUtils.replaceString(strQuery, "!year!",strYear , true);    
        
        if (benchmarkscope.equalsIgnoreCase("Study")) {
             strQuery = StringUtils.replaceString(strQuery, "!benchmarkscope!", " 1=1 ", true);
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!benchmarkscope!", "region like '"+strRegion+"'", true);
           
        }
        
        strQuery = StringUtils.replaceString(strQuery, "!benchmarkmeasure!",benchmarkmeasure , true);
      
        
        
        JSONArray jsonArray = dao.load2Vals(strQuery, "Wave", "wave", "Score", "Score");
        out.println(jsonArray.toJSONString());
        return;
    }
     
    
     public void getBenchmarkbest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_dealer WHERE reportname='getBenchmarkbest'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

        filterList.add("region");
        filterList.add("year");
        filterList.add("biannual");
        filterList.add("month");

        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
       
        String strRegion = request.getParameter("region");
        String benchmarkmeasure=request.getParameter("benchmarkmeasure");
        String benchmarkscope=request.getParameter("benchmarkscope");
        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);
         String strYear=request.getParameter("year");
        strQuery = StringUtils.replaceString(strQuery, "!year!",strYear , true);       
        
        if (benchmarkscope.equalsIgnoreCase("Study")) {
             strQuery = StringUtils.replaceString(strQuery, "!benchmarkscope!", " 1=1 ", true);
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!benchmarkscope!", "region like '"+strRegion+"'", true);
           
        }
        
        strQuery = StringUtils.replaceString(strQuery, "!benchmarkmeasure!",benchmarkmeasure , true);
      
        JSONArray jsonArray = dao.load2Vals(strQuery, "Wave", "wave", "Score", "Score");
         Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String wave = (String) tmpJson.get("Wave");
            String Score = (String) tmpJson.get("Score");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                wave = service.getTranslateString(wave, langCode);
                System.out.println("wave:" + wave);
            }
               if (Score == null) {
            wave = "";
        }
            addingJson.put("Wave", wave);
            addingJson.put("Score", Score);
            finalArray.add(addingJson);
        }
        out.println(finalArray.toJSONString());
        return;
    }
     
     
      public void getBenchmarkbestyear(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_dealer WHERE reportname='getBenchmarkavgyear'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

        filterList.add("region");
        filterList.add("dealergroup");
//        filterList.add("biannual");
//        filterList.add("month");

        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
       
        String strRegion = request.getParameter("region");
        String benchmarkmeasure=request.getParameter("benchmarkmeasure");
        String benchmarkscope=request.getParameter("benchmarkscope");
        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);
         String strYear=request.getParameter("year");
//         if (strYear == null) {
//            strYear = "2015";
//        }
        strQuery = StringUtils.replaceString(strQuery, "!year!",strYear , true);       
        
        if (benchmarkscope.equalsIgnoreCase("Study")) {
             strQuery = StringUtils.replaceString(strQuery, "!benchmarkscope!", " 1=1 ", true);
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!benchmarkscope!", "region like '"+strRegion+"'", true);
           
        }
        
        strQuery = StringUtils.replaceString(strQuery, "!benchmarkmeasure!",benchmarkmeasure , true);
      
        JSONArray jsonArray = dao.load2Vals(strQuery, "year", "year", "Score", "Score");
         Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String wave = (String) tmpJson.get("year");
            String Score = (String) tmpJson.get("Score");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                wave = service.getTranslateString(wave, langCode);
                System.out.println("wave:" + wave);
            }
            
               if (Score == null) {
            wave = "";
        }
            addingJson.put("Wave", wave);
            addingJson.put("Score", Score);
            finalArray.add(addingJson);
        }
        out.println(finalArray.toJSONString());
        return;
    }
      public void getBenchmarkDealerRegion(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_dealer WHERE reportname='getBenchmarkDealerRegion'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();
//filterList.add("city");
//       filterList.add("region");
//        filterList.add("dealergroup");
//        filterList.add("biannual");
//        filterList.add("month");

        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
       
        String strRegion = request.getParameter("region");
        String benchmarkmeasure=request.getParameter("benchmarkmeasure");
        String benchmarkscope=request.getParameter("benchmarkscope");
        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);
         String strYear=request.getParameter("year");
//         if (strYear == null) {
//            strYear = "2015";
//        }
        strQuery = StringUtils.replaceString(strQuery, "!year!",strYear , true);       
        
        if (benchmarkscope.equalsIgnoreCase("Study")) {
             strQuery = StringUtils.replaceString(strQuery, "!benchmarkscope!", " 1=1 ", true);
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!benchmarkscope!", "", true);
           
        }
        
        strQuery = StringUtils.replaceString(strQuery, "!benchmarkmeasure!",benchmarkmeasure , true);
      
        JSONArray jsonArray = dao.load2Vals(strQuery, "year", "year", "Score", "Score");
         Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String wave = (String) tmpJson.get("year");
            String Score = (String) tmpJson.get("Score");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                wave = service.getTranslateString(wave, langCode);
                System.out.println("wave:" + wave);
            }
            
               if (Score == null) {
            wave = "";
        }
            addingJson.put("Wave", wave);
            addingJson.put("Score", Score);
            finalArray.add(addingJson);
        }
        out.println(finalArray.toJSONString());
        return;
    }
      public void getBenchmarkDealercity(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_dealer WHERE reportname='getBenchmarkcity'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();
//filterList.add("city");
//        filterList.add("region");
//        filterList.add("dealergroup");
//        filterList.add("biannual");
//        filterList.add("month");

        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
       
        String strRegion = request.getParameter("region");
        String benchmarkmeasure=request.getParameter("benchmarkmeasure");
        String benchmarkscope=request.getParameter("benchmarkscope");
        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);
         String strYear=request.getParameter("year");
//         if (strYear == null) {
//            strYear = "2015";
//        }
        strQuery = StringUtils.replaceString(strQuery, "!year!",strYear , true);       
        
        if (benchmarkscope.equalsIgnoreCase("Study")) {
             strQuery = StringUtils.replaceString(strQuery, "!benchmarkscope!", " 1=1 ", true);
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!benchmarkscope!", "AND region like '"+strRegion+"'", true);
           
        }
        
        strQuery = StringUtils.replaceString(strQuery, "!benchmarkmeasure!",benchmarkmeasure , true);
      
        JSONArray jsonArray = dao.load2Vals(strQuery, "year", "year", "Score", "Score");
         Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String wave = (String) tmpJson.get("year");
            String Score = (String) tmpJson.get("Score");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                wave = service.getTranslateString(wave, langCode);
                System.out.println("wave:" + wave);
            }
            
               if (Score == null) {
            wave = "";
        }
            addingJson.put("Wave", wave);
            addingJson.put("Score", Score);
            finalArray.add(addingJson);
        }
        out.println(finalArray.toJSONString());
        return;
    }
      
       public void getBenchmarkbestbiannual(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_dealer WHERE reportname='getBenchmarkavgbiannual'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

        filterList.add("region");
//        filterList.add("year");
//        filterList.add("biannual");
//        filterList.add("month");

        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
       
        String strRegion = request.getParameter("region");
        String benchmarkmeasure=request.getParameter("benchmarkmeasure");
        String benchmarkscope=request.getParameter("benchmarkscope");
        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);
        String year = request.getParameter("biannual");
       
       
        strQuery = StringUtils.replaceString(strQuery, "!quarter!", year, true);     
        
        if (benchmarkscope.equalsIgnoreCase("Study")) {
             strQuery = StringUtils.replaceString(strQuery, "!benchmarkscope!", " 1=1 ", true);
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!benchmarkscope!", "region like '"+strRegion+"'", true);
           
        }
        
        strQuery = StringUtils.replaceString(strQuery, "!benchmarkmeasure!",benchmarkmeasure , true);
      
        JSONArray jsonArray = dao.load2Vals(strQuery, "Wave", "wave", "Score", "Score");
         Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String wave = (String) tmpJson.get("Wave");
            String Score = (String) tmpJson.get("Score");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                wave = service.getTranslateString(wave, langCode);
                System.out.println("wave:" + wave);
            }
            addingJson.put("Wave", wave);
            addingJson.put("Score", Score);
            finalArray.add(addingJson);
        }
        out.println(finalArray.toJSONString());
        return;
    }
       
        public void getBenchmarkbestmonth(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_dealer WHERE reportname='getBenchmarkavgmonth'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

        filterList.add("region");
//        filterList.add("year");
//        filterList.add("biannual");
//        filterList.add("month");

        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
       
        String strRegion = request.getParameter("region");
        String benchmarkmeasure=request.getParameter("benchmarkmeasure");
        String benchmarkscope=request.getParameter("benchmarkscope");
        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);
        String month = request.getParameter("month");
        if (month == null ||month.equalsIgnoreCase("Study Total") ) {
            month = "Dec'16";
        }
        strQuery = StringUtils.replaceString(strQuery, "!year!", month, true);       
        
        if (benchmarkscope.equalsIgnoreCase("Study")) {
             strQuery = StringUtils.replaceString(strQuery, "!benchmarkscope!", " 1=1 ", true);
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!benchmarkscope!", "region like '"+strRegion+"'", true);
           
        }
        
        strQuery = StringUtils.replaceString(strQuery, "!benchmarkmeasure!",benchmarkmeasure , true);
      
        JSONArray jsonArray = dao.load2Vals(strQuery, "Wave", "wave", "Score", "Score");
         Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String wave = (String) tmpJson.get("Wave");
            String Score = (String) tmpJson.get("Score");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                wave = service.getTranslateString(wave, langCode);
                System.out.println("wave:" + wave);
            }
            addingJson.put("Wave", wave);
            addingJson.put("Score", Score);
            finalArray.add(addingJson);
        }
        out.println(finalArray.toJSONString());
        return;
    }
     
     
//    public void getNationalScores(HttpServletRequest request, HttpServletResponse response) throws Exception {
//        PrintWriter out = response.getWriter();
//        LIReportquirescsiService queryService = new LIReportquirescsiService();
//        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_dealer WHERE reportname='getNationalScores'");
//        String strQuery = queryRecord.getQuery();
//        String strCountQuery = queryRecord.getCountquery();
//        LIDAO dao = new LIDAO();
//        LIService service = new LIService();
//
//        List<String> filterList = new ArrayList<String>();
//
//        filterList.add("dealer");
//        filterList.add("year");
//        filterList.add("biannual");
//        filterList.add("month");
//        String year = request.getParameter("year");
//        if (year == null) {
//            year = "2016";
//        }
//
//        if (!langCode.equalsIgnoreCase("EN")) {
//            System.out.println("Lang Code :" + langCode);
//            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
//        }
//
//        strQuery = StringUtils.replaceString(strQuery, "!year!", year, true);
//        String where_condtion = service.buildWherecondition(filterList, request);
//        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
//        strCountQuery = StringUtils.replaceString(strCountQuery, service.strWhereCluse, where_condtion, true);
//        JSONArray jsonArray = dao.load2Vals(strQuery, "Wave", "wave", "Score", "Score");
//
//        Iterator i = jsonArray.iterator();
//        JSONArray finalArray = new JSONArray();
//
//        while (i.hasNext()) {
//            JSONObject tmpJson = (JSONObject) i.next();
//            JSONObject addingJson = new JSONObject();
//            String wave = (String) tmpJson.get("Wave");
//            String Score = (String) tmpJson.get("Score");
//            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
//                wave = service.getTranslateString(wave, langCode);
//                System.out.println("wave:" + wave);
//            }
//            addingJson.put("Wave", wave);
//            addingJson.put("Score", Score);
//            finalArray.add(addingJson);
//        }
//
//        String count = dao.loadString(strCountQuery);
//        JSONObject obj = new JSONObject();
//        obj.put("samplecount", count);
//        obj.put("data", finalArray);
//
//        out.println(obj.toJSONString());
//        return;
//    }

         public void getNationalScoresYear(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getNationalScoresYear'");
        String strQuery = queryRecord.getQuery();
        String strCountQuery = queryRecord.getCountquery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

//        filterList.add("zone");
       filterList.add("region");
        filterList.add("dealer");
        filterList.add("model");
         filterList.add("dealergroup");
       

        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }


        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strCountQuery = StringUtils.replaceString(strCountQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.load2Vals(strQuery, "year", "year", "Score", "Score");

        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();

        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String wave = (String) tmpJson.get("year");
            String Score = (String) tmpJson.get("Score");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                wave = service.getTranslateString(wave, langCode);
                System.out.println("wave:" + wave);
            }
            if (Score == null) {
            wave = "";
        }
            
            addingJson.put("Wave", wave);
            addingJson.put("Score", Score);
            finalArray.add(addingJson);
        }

        String count = dao.loadString(strCountQuery);
        JSONObject obj = new JSONObject();
        obj.put("samplecount", count);
        obj.put("data", finalArray);

        out.println(obj.toJSONString());
        return;
    }
         public void getCityScore(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getCityScore'");
        String strQuery = queryRecord.getQuery();
        String strCountQuery = queryRecord.getCountquery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

//        filterList.add("zone");
//        filterList.add("region");
        filterList.add("dealer");
        filterList.add("model");
         filterList.add("dealergroup");
        String year = request.getParameter("year");
        if (year == null) {
            year = "2015";
        }

        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        strQuery = StringUtils.replaceString(strQuery, "!year!", year, true);
        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strCountQuery = StringUtils.replaceString(strCountQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.load2Vals(strQuery, "year", "year", "Score", "Score");

        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();

        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String wave = (String) tmpJson.get("year");
            String Score = (String) tmpJson.get("Score");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                wave = service.getTranslateString(wave, langCode);
                System.out.println("wave:" + wave);
            }
            if (Score == null) {
            wave = "";
        }
            
            addingJson.put("Wave", wave);
            addingJson.put("Score", Score);
            finalArray.add(addingJson);
        }

        String count = dao.loadString(strCountQuery);
        JSONObject obj = new JSONObject();
        obj.put("samplecount", count);
        obj.put("data", finalArray);

        out.println(obj.toJSONString());
        return;
    }
    
    
    public void getNationalScoresBiannual(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getNationalScoresBiannual'");
        String strQuery = queryRecord.getQuery();
        String strCountQuery = queryRecord.getCountquery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

        filterList.add("zone");
        filterList.add("region");
        filterList.add("dealer");
        filterList.add("model");
        String year = request.getParameter("biannual");
//        

        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        strQuery = StringUtils.replaceString(strQuery, "!quarter!", year, true);
        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strCountQuery = StringUtils.replaceString(strCountQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.load2Vals(strQuery, "Wave", "wave", "Score", "Score");

        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();

        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String wave = (String) tmpJson.get("Wave");
            String Score = (String) tmpJson.get("Score");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                wave = service.getTranslateString(wave, langCode);
                System.out.println("wave:" + wave);
            }
            
              if (Score == null) {
            wave = "";
        }
            addingJson.put("Wave", wave);
            addingJson.put("Score", Score);
            finalArray.add(addingJson);
        }

        String count = dao.loadString(strCountQuery);
        JSONObject obj = new JSONObject();
        obj.put("samplecount", count);
        obj.put("data", finalArray);

        out.println(obj.toJSONString());
        return;
    }
    
    
    
    public void getNationalScoresMonth(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getNationalScoresMonth'");
        String strQuery = queryRecord.getQuery();
        String strCountQuery = queryRecord.getCountquery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

        filterList.add("zone");
        filterList.add("region");
        filterList.add("dealer");
        filterList.add("model");
        String month = request.getParameter("month");
        if (month == null ||month.equalsIgnoreCase("Study Total") ) {
            month = "Dec'16";
        }

        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        strQuery = StringUtils.replaceString(strQuery, "!year!", month, true);
        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strCountQuery = StringUtils.replaceString(strCountQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.load2Vals(strQuery, "Wave", "wave", "Score", "Score");

        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();

        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String wave = (String) tmpJson.get("Wave");
            String Score = (String) tmpJson.get("Score");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                wave = service.getTranslateString(wave, langCode);
                System.out.println("wave:" + wave);
            }
            addingJson.put("Wave", wave);
            addingJson.put("Score", Score);
            finalArray.add(addingJson);
        }

        String count = dao.loadString(strCountQuery);
        JSONObject obj = new JSONObject();
        obj.put("samplecount", count);
        obj.put("data", finalArray);

        out.println(obj.toJSONString());
        return;
    }
     
    

     public void getRegionalAttributeAverage(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getRegionalAttributeAverage'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> bestfilterList = new ArrayList<String>();
        List<String> filterList = new ArrayList<String>();
filterList.add("dealergroup");
        filterList.add("attributeFactor");
        filterList.add("region");
        filterList.add("model");
        filterList.add("dealer");
        filterList.add("year");
        filterList.add("dealer");
        filterList.add("biannual");
        filterList.add("month");
//        bestfilterList.add("dealer");
//        bestfilterList.add("region");
        bestfilterList.add("year");
//        bestfilterList.add("biannual");
//        bestfilterList.add("month");
        bestfilterList.add("attributeFactor");
        
        if (!langCode.equalsIgnoreCase("EN")) {
                System.out.println("Lang Code :" + langCode);
                strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
            }
        
        String where_condtion = service.buildWherecondition(filterList, request);
        String best_where_condtion = service.buildWherecondition(bestfilterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strBestWhereCluse, best_where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);

        String strBestString = service.getBestString(request);
        String strWorstString = service.getWorstString(request);

        JSONArray jsonArray = dao.load4Vals(strQuery, "Attribute", "Attribute", "Score", "score", "BestScore", "BestScore", "WorstScore", "WorstScore");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        String model = request.getParameter("model");
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String Attribute = (String) tmpJson.get("Attribute");
            String Score = (String) tmpJson.get("Score");
            String bestscore = (String) tmpJson.get("BestScore");
            String worstscore = (String) tmpJson.get("WorstScore");
            if (model != null && !model.equalsIgnoreCase("ALL") && !model.equalsIgnoreCase("Study Total")) {
                bestscore = null;
            }
            if(langCode!=null && !langCode.equalsIgnoreCase("EN")){
                Attribute = service.getTranslateString(Attribute, langCode);
//                strBestString="การศึกษาที่ดีที่สุด"; 
//                strWorstString="การศึกษาที่เลวร้ายที่สุด";
                System.out.println("Dealer:"+Attribute);
            }
            addingJson.put("name", Attribute);
            addingJson.put("bartitle", Score);
            addingJson.put("y", Double.parseDouble(Score));
            addingJson.put("tooltip", Attribute + "<br>Attribute Score: <b>" + Score + "</b>");
            addingJson.put("scatter", strBestString);
            if (bestscore != null && !bestscore.equalsIgnoreCase("0") && !bestscore.equalsIgnoreCase("null")) {
                addingJson.put("svalue", Double.parseDouble(bestscore));
            } else {
                addingJson.put("svalue", null);
            }
            addingJson.put("scatter1", strWorstString);
            if (worstscore != null && !worstscore.equalsIgnoreCase("0") && !worstscore.equalsIgnoreCase("null")) {
                addingJson.put("svalue1", Double.parseDouble(worstscore));
            } else {
                addingJson.put("svalue1", null);
            }
            finalArray.add(addingJson);
        }

        out.println(finalArray.toJSONString());
        return;
    }

    public void getTop5Bottom5SOP(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_dealer WHERE reportname='getTop5Bottom5SOP'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();
filterList.add("dealergroup");
        filterList.add("region");
        filterList.add("dealer");
        filterList.add("year");
        filterList.add("biannual");
        filterList.add("month");

        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.load3Vals(strQuery, "Attribute", "Attribute", "Score", "score", "TopOrBottom", "top_or_bottom");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        JSONArray Attributes = new JSONArray();
        JSONArray Scores = new JSONArray();
        JSONArray TopORBottoms = new JSONArray();
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();

            String Attribute = (String) tmpJson.get("Attribute");
            String Score = (String) tmpJson.get("Score");
            String tipkey = " <br>Attribute Score <b>";
            if (Score == null) {
                Score = "0";
            }

            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                int flag=0;
                if(Attribute.contains("**")){
                    flag=2;  }
                else if(Attribute.contains("*")){ 
                    flag=1; }
                System.out.println("Attributeflagvalue "+flag);
                System.out.println("Attribute value b4 "+Attribute);
                
                Attribute = Attribute.replaceAll("\\*", "");
                Attribute = service.getTranslateString(Attribute, langCode);
                if(flag==2){
                Attribute = Attribute.concat("**");}
                else if(flag==1){
                Attribute = Attribute.concat("*");}
                Attribute = service.getTranslateString(Attribute, langCode);
//                tipkey = "<br>คะแนนหัวข้อย่อย : <b>";
                System.out.println("Attribute:" + Attribute);
            }

            String toporbootom = (String) tmpJson.get("TopOrBottom");
            addingJson.put("name", Attribute);
            addingJson.put("bartitle", Score + "%");
            addingJson.put("y", Integer.parseInt(Score));
            addingJson.put("tooltip", Attribute + tipkey + Score + "</b>");
            if (toporbootom.equalsIgnoreCase("Top")) {
                addingJson.put("color", "#7E57C2");
            } else if (toporbootom.equalsIgnoreCase("Bottom")) {
                addingJson.put("color", "#757575");
            }
            finalArray.add(addingJson);
        }

        out.println(finalArray.toJSONString());
        return;
    }

    public void getSummaryRegionFactorScores(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getSummaryRegionFactorScores'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> bestfilterList = new ArrayList<String>();
        List<String> filterList = new ArrayList<String>();
      filterList.add("dealergroup");   
        filterList.add("region");
        filterList.add("dealer");
        filterList.add("year");
        filterList.add("biannual");
        filterList.add("month");

        bestfilterList.add("year");
//        bestfilterList.add("biannual");
//        bestfilterList.add("month");
//        bestfilterList.add("region");
        String model = request.getParameter("model");

        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        String where_condtion = service.buildWherecondition(filterList, request);
        String best_where_condtion = service.buildWherecondition(bestfilterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strBestWhereCluse, best_where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);

        JSONArray jsonArray = dao.load4Vals(strQuery, "Factor", "Factor", "Score", "score", "BestScore", "maxscore", "WorstScore", "minscore");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        String strBestString = service.getBestString(request);
        String strWorstString = service.getWorstString(request);
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            System.out.println(tmpJson.toJSONString());
            JSONObject addingJson = new JSONObject();
            String factor = (String) tmpJson.get("Factor");
            String Score = (String) tmpJson.get("Score");
            String bestscore = (String) tmpJson.get("BestScore");
            String worstscore = (String) tmpJson.get("WorstScore");
            String tipkey = "<br>Factor Score: <b>";
            if (model != null && !model.equalsIgnoreCase("ALL") && !model.equalsIgnoreCase("Study Total")) {
                bestscore = null;
            }
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                factor = service.getTranslateString(factor, langCode);
//                tipkey = "<br>คะแนนปัจจัย: <b>";
                 strBestString = service.getTranslateString(strBestString, langCode);
                strWorstString = service.getTranslateString(strWorstString, langCode);
                System.out.println("Dealer:" + factor);
            }
            addingJson.put("name", factor);
            addingJson.put("bartitle", Score);
            addingJson.put("y", Integer.parseInt(Score));
            addingJson.put("tooltip", factor + tipkey + Score + "</b>");
            addingJson.put("scatter", strBestString);
            if (bestscore != null && !bestscore.equalsIgnoreCase("0") && !bestscore.equalsIgnoreCase("null")) {
                addingJson.put("svalue", Integer.parseInt(bestscore));
            } else {
                addingJson.put("svalue", null);
            }
            addingJson.put("scatter1", strWorstString);
            if (worstscore != null && !worstscore.equalsIgnoreCase("0") && !worstscore.equalsIgnoreCase("null")) {
                addingJson.put("svalue1", Integer.parseInt(worstscore));
            } else {
                addingJson.put("svalue1", null);
            }
            finalArray.add(addingJson);
        }

        out.println(finalArray.toJSONString());
        return;
    }


}
