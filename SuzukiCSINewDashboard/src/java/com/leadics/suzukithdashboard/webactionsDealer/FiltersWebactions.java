/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.suzukithdashboard.webactionsDealer;

import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.service.LIReportquirescsiService;
import com.leadics.suzukithdashboard.to.LIReportquirescsiRecord;
import com.leadics.utils.LogUtils;
import com.leadics.utils.PropertyUtil;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author varma.sagi
 */
public class FiltersWebactions {

    static LogUtils logger = new LogUtils(FiltersWebactions.class.getName());
    String langCode = "EN";

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {

                if (!StringUtils.isNullOrEmpty(actionType)) {
                    if (actionType.equals("getCSIState")) {
                        getCSIState(request, response);
                        return;
                    }

                    if (actionType.equals("getYears")) {
                        getYears(request, response);
                        return;
                    }

                    if (actionType.equals("getWaves")) {
                        getWaves(request, response);
                        return;
                    }

                    if (actionType.equals("getModels")) {
                        getModels(request, response);
                        return;
                    }
                    if (actionType.equals("getRegion")) {
                        getRegion(request, response);
                        return;
                    }
                    if (actionType.equals("getCity")) {
                        getCity(request, response);
                        return;
                    }

                    if (actionType.equals("getDealers")) {
                        getDealers(request, response);
                        return;
                    }

                    if (actionType.equals("getFactors")) {
                        getFactors(request, response);
                        return;
                    }
                    if (actionType.equals("getSOPAttributes")) {
                        getSOPAttributes(request, response);
                        return;
                    }
                    if (actionType.equals("getPeriods")) {
                        getPeriods(request, response);
                        return;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    public void getFactors(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getCSIFactors'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        JSONArray jsonArray = dao.loadColumnListAsJSON(strQuery, "factor", false);
        out.println(jsonArray.toJSONString());
        return;
    }

    public void getCSIState(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getCSIState'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        List<String> filterList = new ArrayList<String>();
        filterList.add("region");
        String where_condtion = service.buildCSIWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.loadColumnListAsJSON(strQuery, "state", false);
        out.println(jsonArray.toJSONString());
        return;
    }

    public void getYears(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getCSIYears'");
        String strQuery = queryRecord.getQuery();
        String year = PropertyUtil.getProperty("yearfilter");
        if (year != null) {
            strQuery = strQuery.replaceAll("<<WHERE CONDITION>>", "AND year >= " + year);
        } else {
            strQuery = strQuery.replaceAll("<<WHERE CONDITION>>", "");
        }
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        JSONArray jsonArray = dao.loadColumnListAsJSON(strQuery, "Year", false);
        //added
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            String Name = (String) i.next();
            String value = Name;
            JSONObject Obj1 = new JSONObject();
            Obj1.put("name", Name);
            String langCode = request.getParameter("langCode");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                System.out.println("Lang Code :" + langCode);
                value = service.getTranslateString(Name, langCode);
                if (value == null) {
                    value = Name;
                }
            }
            System.out.println("value : " + value);
            Obj1.put("value", value);
            finalArray.add(Obj1);
        }
        out.println(jsonArray.toJSONString());
        return;
    }

    public void getWaves(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getCSIWaves'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        List<String> filterList = new ArrayList<String>();
        filterList.add("year");
        String where_condtion = service.buildCSIWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.loadColumnListAsJSON(strQuery, "Wave", false);
        out.println(jsonArray.toJSONString());
        return;
    }

    public void getModels(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getCSIModels'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        JSONArray jsonArray = dao.loadColumnListAsJSON(strQuery, "Model", false);
        out.println(jsonArray.toJSONString());
        return;
    }

    public void getSOPAttributes(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getSOPAttributes'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        JSONArray jsonArray = dao.loadColumnListAsJSON(strQuery, "attributename", false);
        out.println(jsonArray.toJSONString());
        return;
    }

    public void getRegion(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getCSIRegion'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();
        filterList.add("state");
        String where_condtion = service.buildCSIWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.loadColumnListAsJSON(strQuery, "Region", false);
        out.println(jsonArray.toJSONString());
        return;
    }

    public void getCity(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getCSICity'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        List<String> filterList = new ArrayList<String>();
        filterList.add("region");
        filterList.add("state");
        String where_condtion = service.buildCSIWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.loadColumnListAsJSON(strQuery, "City", false);
        out.println(jsonArray.toJSONString());
        return;
    }

    public void getDealers(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getCSIDealers'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        List<String> filterList = new ArrayList<String>();
        filterList.add("region");
        filterList.add("state");
        filterList.add("city");
        String where_condtion = service.buildCSIWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.loadColumnListAsJSON(strQuery, "dealer", false);
        out.println(jsonArray.toJSONString());
        return;
    }

    public void getPeriods(HttpServletRequest request, HttpServletResponse response) throws Exception {

        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getPeriods'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        List<String> filterList = new ArrayList<String>();
        String where_condtion = service.buildCSIWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.loadColumnListAsJSON(strQuery, "dealer", false);
        //added
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            String Name = (String) i.next();
            String value = Name;
            JSONObject Obj1 = new JSONObject();
            Obj1.put("name", Name);
            if (!langCode.equalsIgnoreCase("EN")) {
                System.out.println("Lang Code :" + langCode);
                value = service.getTranslateString(Name, langCode);
                if (value == null) {
                    value = Name;
                }
            }
            System.out.println("value : " + value);
            Obj1.put("value", value);
            finalArray.add(Obj1);
        }

        //adding complete
        out.println(finalArray.toJSONString());
        return;
    }
}
