/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.suzukithdashboard.webactionsDealer;

import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.service.LICsidealerscoretableviewService;
import com.leadics.suzukithdashboard.service.LIOtherstableviewService;
import com.leadics.suzukithdashboard.service.LIReportquirescsiService;
import com.leadics.suzukithdashboard.to.LICsidealerscoretableviewRecord;
import com.leadics.suzukithdashboard.to.LIOtherstableviewRecord;
import com.leadics.suzukithdashboard.to.LIReportquirescsiRecord;
import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author varma.sagi
 */
public class OthersWebactions {

    static LogUtils logger = new LogUtils(OthersWebactions.class.getName());

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {

                if (!StringUtils.isNullOrEmpty(actionType)) {
                    if (actionType.equals("getCSIOthersTable")) {
                        getCSIOthersTable(request, response);
                        return;
                    }
                    if (actionType.equals("getCSIOthersCharts1")) {
                        getCSIOthersCharts(request, response, "getCSIOthersCharts1");
                        return;
                    }
                    if (actionType.equals("getCSIOthersCharts")) {
                        getCSIOthersCharts(request, response, "getCSIOthersCharts");
                        return;
                    }
                    if (actionType.equals("getCSIOthersChartsWaitedfor")) {
                        getCSIOthersCharts(request, response, "getCSIOthersChartsWaitedfor");
                        return;
                    }
                    if (actionType.equals("getCSIOthersChartsTimeatworkshop")) {
                        getCSIOthersCharts(request, response, "getCSIOthersChartsTimeatworkshop");
                        return;
                    }
                    if (actionType.equals("getCSIOthersChartsAppointedCustomer")) {
                        getCSIOthersCharts(request, response, "getCSIOthersChartsAppointedCustomer");
                        return;
                    }
                    if (actionType.equals("getCSIOthersChartsWalkinCustomer")) {
                        getCSIOthersCharts(request, response, "getCSIOthersChartsWalkinCustomer");
                        return;
                    }
                    if (actionType.equals("getCSIOthersChartsNoDaysBodyRepair")) {
                        getCSIOthersCharts(request, response, "getCSIOthersChartsNoDaysBodyRepair");
                        return;
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    public void getCSIOthersTable(HttpServletRequest request, HttpServletResponse response) throws Exception {
//        PrintWriter out = response.getWriter();
//        LIReportquirescsiService queryService = new LIReportquirescsiService();
//        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_dealercsi WHERE reportname='getCSIOthersTable'");
//        String strQuery = queryRecord.getQuery();
//        LIDAO dao = new LIDAO();
//        LIService service = new LIService();
//        String Heading1 = "Ford Average";
//        String Heading2 = "Ford Average";
//        String Heading3 = "Ford Best";
//        String Heading4 = "Ford Best";
//        String region = request.getParameter("region");
//        String dealer = request.getParameter("dealer");
//        String state = request.getParameter("state");
//        if (dealer != null && !dealer.equalsIgnoreCase("ALL")) {
//            String region1 = dao.loadString("Select distinct region from others_aggregate where dealer like'" + dealer + "' limit 1");
//            Heading1 = "Dealer Average";
//            Heading2 = region1 + " Average";
//            Heading3 = region1 + " Best";
//            strQuery = strQuery.replaceAll("#region#", " AND region=\"" + region1 + "\" ");
//            strQuery = strQuery.replaceAll("#dealer#", " AND dealer=\"" + dealer + "\" ");
//            strQuery = strQuery.replaceAll("#state#", " ");
//        } else if (state != null && !state.equalsIgnoreCase("ALL")) {
//            String region1 = dao.loadString("Select distinct region from others_aggregate where state like'" + state + "' limit 1");
//            Heading1 = "State Average";
//            Heading2 = region1 + " Average";
//            Heading3 = region1 + " Best";
//            strQuery = strQuery.replaceAll("#region#", " AND region=\"" + region1 + "\" ");
//            strQuery = strQuery.replaceAll("#dealer#", " ");
//            strQuery = strQuery.replaceAll("#state#", " AND state=\"" + state + "\" ");
//        } else if (region != null && !region.equalsIgnoreCase("ALL")) {
//            Heading1 = region + " Average";
//            Heading2 = region + " Average";
//            Heading3 = region + " Best";
//            strQuery = strQuery.replaceAll("#region#", " AND region=\"" + region + "\" ");
//            strQuery = strQuery.replaceAll("#dealer#", " ");
//            strQuery = strQuery.replaceAll("#state#", " ");
//        } else {
//            strQuery = strQuery.replaceAll("#region#", " ");
//            strQuery = strQuery.replaceAll("#dealer#", " ");
//            strQuery = strQuery.replaceAll("#state#", " ");
//        }
//
//        List<String> filterList = new ArrayList<String>();
//
//        filterList.add("wave");
//        filterList.add("model");
//
//        String where_condtion = service.buildCSIWherecondition(filterList, request);
//        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
//
//        LIOtherstableviewService serviceObj = new LIOtherstableviewService();
//        LIOtherstableviewRecord[] records = serviceObj.loadLIOtherstableviewRecords(strQuery);
//        JSONArray array = new JSONArray();
//
//        for (int i = 0; i < records.length; i++) {
//            JSONObject jsontempObj = new JSONObject();
//            String attribute = records[i].getAttributename();
//            String dealerAVg = records[i].getDealeraverage();
//            String regionAVg = records[i].getRegionaverage();
//            String regionBest = records[i].getRegionbest();
//            String StudyBest = records[i].getStudybest();
//            jsontempObj.put("AttributeName", attribute);
//            jsontempObj.put("Heading1", Heading1);
//            jsontempObj.put("Value1", dealerAVg);
//            jsontempObj.put("Heading2", Heading2);
//            jsontempObj.put("Value2", regionAVg);
//            jsontempObj.put("Heading3", Heading3);
//            jsontempObj.put("Value3", regionBest);
//            jsontempObj.put("Heading4", Heading4);
//            jsontempObj.put("Value4", StudyBest);
//            array.add(jsontempObj);
//        }
//        out.println(array.toJSONString());
//        return;
        
        
        
        
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getCSIOthersTable'");
        String strQuery = queryRecord.getQuery();
        String strCountQuery = queryRecord.getCountquery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String Heading1 = "Ford Average";
        String Heading2 = "Ford Average";
        String Heading3 = "Ford Best";
        String Heading4 = "Ford Best";
        String region = request.getParameter("region");
        String dealer = request.getParameter("dealer");
        String state = request.getParameter("state");
        if (dealer != null && !dealer.equalsIgnoreCase("ALL")) {
            String region1 = dao.loadString("Select distinct region from others_table_aggregate where dealer like'" + dealer + "' limit 1");
            Heading1 = "Dealer Average";
            Heading2 = region1 + " Average";
            Heading3 = region1 + " Best";
            strQuery = strQuery.replaceAll("#region#", " AND region=\"" + region1 + "\" ");
            strQuery = strQuery.replaceAll("#dealer#", " AND dealer=\"" + dealer + "\" ");
            strQuery = strQuery.replaceAll("#state#", " ");
            strCountQuery = strCountQuery.replaceAll("#region#", " AND region=\"" + region1 + "\" ");
            strCountQuery = strCountQuery.replaceAll("#dealer#", " AND dealer=\"" + dealer + "\" ");
            strCountQuery = strCountQuery.replaceAll("#state#", " ");
        } else if (state != null && !state.equalsIgnoreCase("ALL")) {
            String region1 = dao.loadString("Select distinct region from others_table_aggregate where state like'" + state + "' limit 1");
            Heading1 = "State Average";
            Heading2 = region1 + " Average";
            Heading3 = region1 + " Best";
            strQuery = strQuery.replaceAll("#region#", " AND region=\"" + region1 + "\" ");
            strQuery = strQuery.replaceAll("#dealer#", " ");
            strQuery = strQuery.replaceAll("#state#", " AND state=\"" + state + "\" ");
            strCountQuery = strCountQuery.replaceAll("#region#", " AND region=\"" + region1 + "\" ");
            strCountQuery = strCountQuery.replaceAll("#dealer#", " ");
            strCountQuery = strCountQuery.replaceAll("#state#", " AND state=\"" + state + "\" ");
        } else if (region != null && !region.equalsIgnoreCase("ALL")) {
            Heading1 = region + " Average";
            Heading2 = region + " Average";
            Heading3 = region + " Best";
            strQuery = strQuery.replaceAll("#region#", " AND region=\"" + region + "\" ");
            strQuery = strQuery.replaceAll("#dealer#", " ");
            strQuery = strQuery.replaceAll("#state#", " ");
            strCountQuery = strCountQuery.replaceAll("#region#", " AND region=\"" + region + "\" ");
            strCountQuery = strCountQuery.replaceAll("#dealer#", " ");
            strCountQuery = strCountQuery.replaceAll("#state#", " ");
        } else {
            strQuery = strQuery.replaceAll("#region#", " ");
            strQuery = strQuery.replaceAll("#dealer#", " ");
            strQuery = strQuery.replaceAll("#state#", " ");
            strCountQuery = strCountQuery.replaceAll("#region#", " ");
            strCountQuery = strCountQuery.replaceAll("#dealer#", " ");
            strCountQuery = strCountQuery.replaceAll("#state#", " ");
        }

        List<String> filterList = new ArrayList<String>();

        filterList.add("wave");
        filterList.add("model");

        String where_condtion = service.buildCSIWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strCountQuery = StringUtils.replaceString(strCountQuery, service.strWhereCluse, where_condtion, true);

        LIOtherstableviewService serviceObj = new LIOtherstableviewService();
        LIOtherstableviewRecord[] records = serviceObj.loadLIOtherstableviewRecords(strQuery);
        JSONArray array = new JSONArray();

        for (int i = 0; i < records.length; i++) {
            JSONObject jsontempObj = new JSONObject();
            String attribute = records[i].getAttributename();
            String dealerAVg = records[i].getDealeraverage();
            String regionAVg = records[i].getRegionaverage();
            String regionBest = records[i].getRegionbest();
            String StudyBest = records[i].getStudybest();
            jsontempObj.put("AttributeName", attribute);
            jsontempObj.put("Heading1", Heading1);
            jsontempObj.put("Value1", dealerAVg);
            jsontempObj.put("Heading2", Heading2);
            jsontempObj.put("Value2", regionAVg);
            jsontempObj.put("Heading3", Heading3);
            jsontempObj.put("Value3", regionBest);
            jsontempObj.put("Heading4", Heading4);
            jsontempObj.put("Value4", StudyBest);
            array.add(jsontempObj);
        }
         String count=dao.loadString(strCountQuery);
        JSONObject obj=new JSONObject();
        obj.put("samplecount", count);
        obj.put("data", array);
        out.println(obj.toJSONString());
//        out.println(array.toJSONString());
        return;
    }

    public void getCSIOthersCharts(HttpServletRequest request, HttpServletResponse response, String ChartName) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_dealercsi WHERE reportname='" + ChartName + "'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String Heading1 = "Volvo Average";
        String Heading2 = "Volvo Average";
        String Heading3 = "Volvo Average";

        String region = request.getParameter("region");
        String dealer = request.getParameter("dealer");
        String state = request.getParameter("state");
          
        
        
        if (dealer != null && !dealer.equalsIgnoreCase("ALL")) {
            String region1 = dao.loadString("Select distinct region from others_charts_aggregate where dealer like'" + dealer + "'");
            Heading1 = "Dealer Average";
            Heading2 = region1 + " Average";
           strQuery = strQuery.replaceAll("#region#", " AND region=\"" + region1 + "\" ");
            strQuery = strQuery.replaceAll("#dealer#", " AND dealer=\"" + dealer + "\" ");
            strQuery = strQuery.replaceAll("#state#", " ");
        }
        else if (state != null && !state.equalsIgnoreCase("ALL")) {
            String region1 = dao.loadString("Select distinct region from others_charts_aggregate where state like'" + state + "' limit 1");
            Heading1 = "State Average";
            Heading2 = region1 + " Average";
            strQuery = strQuery.replaceAll("#region#", " AND region=\"" + region1 + "\" ");
            strQuery = strQuery.replaceAll("#dealer#", " ");
            strQuery = strQuery.replaceAll("#state#", " AND state=\"" + state + "\" ");
        } 
        else if (region != null && !region.equalsIgnoreCase("ALL")) {
            Heading1 = region + " Average";
            Heading2 = region + " Average";
            strQuery = strQuery.replaceAll("#region#", " AND region=\"" + region + "\" ");
            strQuery = strQuery.replaceAll("#dealer#", " ");
            strQuery = strQuery.replaceAll("#state#", " ");
        } else {
            strQuery = strQuery.replaceAll("#region#", " ");
            strQuery = strQuery.replaceAll("#dealer#", " ");
            strQuery = strQuery.replaceAll("#state#", " ");
        }

        List<String> filterList = new ArrayList<String>();

//        filterList.add("state");
        filterList.add("wave");
      
        filterList.add("model");

        String where_condtion = service.buildCSIWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);

        JSONArray jsonArray = dao.load5Vals(strQuery, "AttributeName", "AttributeName", "value", "value", "dealerscore", "dealerscore", "regionscore", "regionscore", "studyscore", "studyscore");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        LinkedHashMap attributes = new LinkedHashMap();
        LinkedHashMap values = new LinkedHashMap();
        LinkedHashMap checkcomb = new LinkedHashMap();
        LinkedHashMap dealerscores = new LinkedHashMap();
        LinkedHashMap regionscores = new LinkedHashMap();
        LinkedHashMap studyscores = new LinkedHashMap();
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            String Attribute = (String) tmpJson.get("AttributeName");
            String value = (String) tmpJson.get("value");
            String dealerscore = (String) tmpJson.get("dealerscore");
            String regionscore = (String) tmpJson.get("regionscore");
            String studyscore = (String) tmpJson.get("studyscore");
            Attribute = Attribute.trim();
            value = value.trim();

            attributes.put(Attribute, Attribute);
            values.put(value, value);
            checkcomb.put(Attribute + "-" + value, new Boolean(true));
            dealerscores.put(Attribute + "-" + value, dealerscore);
            regionscores.put(Attribute + "-" + value, regionscore);
            studyscores.put(Attribute + "-" + value, studyscore);
        }

        Iterator iattributes = attributes.keySet().iterator();
        while (iattributes.hasNext()) {
            JSONObject jsonbject = new JSONObject();
            String attribute = (String) iattributes.next();
            jsonbject.put("headingname", attribute);
            Iterator ivalues = values.keySet().iterator();
            JSONArray tmpJsonArray = new JSONArray();
            while (ivalues.hasNext()) {
                JSONObject tmpJsonObj = new JSONObject();
                String value = (String) ivalues.next();
                if (checkcomb.containsKey(attribute + "-" + value)) {
                    String dealerscore = (String) dealerscores.get(attribute + "-" + value);
                    String regionscore = (String) regionscores.get(attribute + "-" + value);
                    String studyscore = (String) studyscores.get(attribute + "-" + value);
                    if (dealerscore == null) {
                        dealerscore = "0";
                    }
                    if (regionscore == null) {
                        regionscore = "0";
                    }
                    if (studyscore == null) {
                        studyscore = "0";
                    }
                    JSONObject jsonobj = new JSONObject();
                    jsonobj.put("name", value);
                    JSONArray subarrray = new JSONArray();

                    JSONObject dealerscoreObj = new JSONObject();
                    dealerscoreObj.put("y", Integer.parseInt(dealerscore));
                    dealerscoreObj.put("bartitle", dealerscore);
                    dealerscoreObj.put("name", Heading1);
                    dealerscoreObj.put("tooltip", "<b>" + value + "<br>" + dealerscore + "%</b>");
                    subarrray.add(dealerscoreObj);

                    JSONObject regionscoreObj = new JSONObject();
                    regionscoreObj.put("y", Integer.parseInt(regionscore));
                    regionscoreObj.put("bartitle", regionscore);
                    regionscoreObj.put("name", Heading2);
                    regionscoreObj.put("tooltip", "<b>" + value + "<br>" + regionscore + "%</b>");
                    subarrray.add(regionscoreObj);

                    JSONObject studyscoreObj = new JSONObject();
                    studyscoreObj.put("y", Integer.parseInt(studyscore));
                    studyscoreObj.put("bartitle", studyscore);
                    studyscoreObj.put("name", Heading3);
                    studyscoreObj.put("tooltip", "<b>" + value + "<br>" + studyscore + "%</b>");
                    subarrray.add(studyscoreObj);

                    jsonobj.put("data", subarrray);
                    tmpJsonArray.add(jsonobj);
                }
            }
            jsonbject.put("headingdata", tmpJsonArray);
            finalArray.add(jsonbject);
        }
        out.println(finalArray.toJSONString());
        return;
    }

}
