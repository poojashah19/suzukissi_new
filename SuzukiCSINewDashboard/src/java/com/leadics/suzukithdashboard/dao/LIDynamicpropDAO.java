
/*
 * LIDynamicpropDAO.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.dao;
import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.to.LIDynamicpropRecord;
import java.sql.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LIDynamicpropDAO extends LIDAO
{
	static LogUtils logger = new LogUtils(LIDynamicpropDAO.class.getName());


	public LIDynamicpropRecord[] loadLIDynamicpropRecords(String query, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			query = query + MAX_RECORD_LIMIT_APPENDER;
			query = updateQuery(query);
			logger.trace("loadLIDynamicpropRecords\t" + closeConnection + "\t" + query);
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			ArrayList recordSet = new ArrayList();
			while(rs.next())
			{
				LIDynamicpropRecord record = new LIDynamicpropRecord();
				record.setId(rs.getString("Id"));
				record.setPcategory(rs.getString("PCATEGORY"));
				record.setPropname(rs.getString("PROPNAME"));
				record.setPropval(rs.getString("PROPVAL"));
				record.setPdesc(rs.getString("PDESC"));
				record.setPchgbyenv(rs.getString("PCHGBYENV"));
				record.setPactive(rs.getString("PACTIVE"));
				record.setRstatus(rs.getString("RSTATUS"));
				record.setCreatedby(rs.getString("CREATED_BY"));
				record.setCreatedat(formatDBDateTime(rs.getTimestamp("CREATED_AT")));
				record.setModifiedat(formatDBDateTime(rs.getTimestamp("MODIFIED_AT")));
				record.setModifiedby(rs.getString("MODIFIED_BY"));
				record.setMadeby(rs.getString("MADE_BY"));
				record.setMadeat(formatDBDateTime(rs.getTimestamp("MADE_AT")));
				record.setCheckedby(rs.getString("CHECKED_BY"));
				record.setCheckedat(formatDBDateTime(rs.getTimestamp("CHECKED_AT")));
				record.setMakerlastcmt(rs.getString("MAKER_LAST_CMT"));
				record.setCheckerlastcmt(rs.getString("CHECKER_LAST_CMT"));
				record.setCurrappstatus(rs.getString("CURR_APP_STATUS"));
				record.setAdminlastcmt(rs.getString("ADMIN_LAST_CMT"));
				record.setInstitutionid(rs.getString("INSTITUTION_ID"));
				recordSet.add(record);
			}
			logger.trace("loadLIDynamicpropRecords:Records Fetched:" + recordSet.size());
			LIDynamicpropRecord[] tempLIDynamicpropRecords = new LIDynamicpropRecord[recordSet.size()];
			for (int index = 0; index < recordSet.size(); index++)
			{
				tempLIDynamicpropRecords[index] = (LIDynamicpropRecord)(recordSet.get(index));
			}
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			return tempLIDynamicpropRecords;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}


	public LIDynamicpropRecord[] loadLIDynamicpropRecords(String query)
	throws Exception
	{
		return loadLIDynamicpropRecords(query, null, true);
	}


	public LIDynamicpropRecord loadFirstLIDynamicpropRecord(String query)
	throws Exception
	{
		LIDynamicpropRecord[] results = loadLIDynamicpropRecords(query);
		if (results == null) return null;
		if(results.length < 1) return null;
		return results[0];
	}

	public LIDynamicpropRecord loadLIDynamicpropRecord(String id, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			String Query = "SELECT * FROM dynamic_prop WHERE (Id = ?)";
			Query = updateQuery(Query);
			logger.trace("loadLIDynamicpropRecord\t" + closeConnection + "\t" + id + "\t" + Query);
			ps = con.prepareStatement(Query);
			ps.setString(1,id);
			rs = ps.executeQuery();
			if (!rs.next())
			{
				ps.close();
				releaseDatabaseConnection(con, closeConnection);
				return null;
			}
			LIDynamicpropRecord record = new LIDynamicpropRecord();
			record.setId(rs.getString("Id"));
			record.setPcategory(rs.getString("PCATEGORY"));
			record.setPropname(rs.getString("PROPNAME"));
			record.setPropval(rs.getString("PROPVAL"));
			record.setPdesc(rs.getString("PDESC"));
			record.setPchgbyenv(rs.getString("PCHGBYENV"));
			record.setPactive(rs.getString("PACTIVE"));
			record.setRstatus(rs.getString("RSTATUS"));
			record.setCreatedby(rs.getString("CREATED_BY"));
			record.setCreatedat(formatDBDateTime(rs.getTimestamp("CREATED_AT")));
			record.setModifiedat(formatDBDateTime(rs.getTimestamp("MODIFIED_AT")));
			record.setModifiedby(rs.getString("MODIFIED_BY"));
			record.setMadeby(rs.getString("MADE_BY"));
			record.setMadeat(formatDBDateTime(rs.getTimestamp("MADE_AT")));
			record.setCheckedby(rs.getString("CHECKED_BY"));
			record.setCheckedat(formatDBDateTime(rs.getTimestamp("CHECKED_AT")));
			record.setMakerlastcmt(rs.getString("MAKER_LAST_CMT"));
			record.setCheckerlastcmt(rs.getString("CHECKER_LAST_CMT"));
			record.setCurrappstatus(rs.getString("CURR_APP_STATUS"));
			record.setAdminlastcmt(rs.getString("ADMIN_LAST_CMT"));
			record.setInstitutionid(rs.getString("INSTITUTION_ID"));
			ps.close();
			logger.trace("loadLIDynamicpropRecord\t" + record + "\t");
			releaseDatabaseConnection(con, closeConnection);
			return record;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public LIDynamicpropRecord loadLIDynamicpropRecord(String id)
	throws Exception
	{
		return loadLIDynamicpropRecord(id, null, true);
	}

	public int insertLIDynamicpropRecord(LIDynamicpropRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			String Query = "INSERT INTO dynamic_prop ";
			Query += "(";
			Query += "PCATEGORY,PROPNAME,PROPVAL,PDESC,PCHGBYENV,PACTIVE,RSTATUS,CREATED_BY,CREATED_AT,MODIFIED_AT,MODIFIED_BY,MADE_BY,MADE_AT,CHECKED_BY,CHECKED_AT,MAKER_LAST_CMT,CHECKER_LAST_CMT,CURR_APP_STATUS,ADMIN_LAST_CMT,INSTITUTION_ID";
			Query += ")";
			Query += " VALUES ";
			Query += "(";
			Query += "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?";
			Query += ")";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			record.setModifiedby(StringUtils.noNull(record.getModifiedby(),record.getCreatedby()));
			record.setMadeat(StringUtils.noNull(record.getMadeat(),record.getCreatedat()));
			record.setMadeby(StringUtils.noNull(record.getMadeby(),record.getCreatedby()));
			record.setInstitutionid(StringUtils.noNull(record.getInstitutionid(),PropertyUtil.getProperty("DefaultInstitutionId")));
			Query = updateQuery(Query);
			logger.trace("insertLIDynamicpropRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			if (isOracleDatabase()) 
			{
				ps = con.prepareStatement(Query,new String[]{"ID"});
			}
			else
			{
				ps = con.prepareStatement(Query,Statement.RETURN_GENERATED_KEYS);
			}
			setStringValue(ps, 1, record.getPcategory());
			setStringValue(ps, 2, record.getPropname());
			setStringValue(ps, 3, record.getPropval());
			setStringValue(ps, 4, record.getPdesc());
			setStringValue(ps, 5, record.getPchgbyenv());
			setStringValue(ps, 6, record.getPactive());
			setStringValue(ps, 7, record.getRstatus());
			setStringValue(ps, 8, record.getCreatedby());
			setDateValue(ps, 9, fd.getCurrentSQLDateObject());
			setDateValue(ps, 10, fd.getCurrentSQLDateObject());
			setStringValue(ps, 11, record.getModifiedby());
			setStringValue(ps, 12, record.getMadeby());
			setDateValue(ps, 13, fd.getSQLDateObject(record.getMadeat(), "yyyyMMddHHmmss"));
			setStringValue(ps, 14, record.getCheckedby());
			setDateValue(ps, 15, fd.getSQLDateObject(record.getCheckedat(), "yyyyMMddHHmmss"));
			setStringValue(ps, 16, record.getMakerlastcmt());
			setStringValue(ps, 17, record.getCheckerlastcmt());
			setStringValue(ps, 18, record.getCurrappstatus());
			setStringValue(ps, 19, record.getAdminlastcmt());
			setStringValue(ps, 20, record.getInstitutionid());
			boolean result = ps.execute();
			logger.trace("insertLIDynamicpropRecord\t" + result + "\t");
			int resultID = -1;
			rs = ps.getGeneratedKeys();
			if (rs.next()) 
			{
				resultID = rs.getInt(1); 
			}
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			createAuditRecord("dynamic_prop","INSERT",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), record.getInstitutionid(), record.getActionSource());
			return resultID;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public int insertLIDynamicpropRecord(LIDynamicpropRecord record)
	throws Exception
	{
		return insertLIDynamicpropRecord(record, null, true);
	}

	public boolean updateLIDynamicpropRecord(LIDynamicpropRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			LIDynamicpropRecord currentRecord = loadLIDynamicpropRecord(record.getId());
			String currentRecordContent = StringUtils.noNull(currentRecord);

			String Query = "UPDATE dynamic_prop SET ";
			Query += "PCATEGORY = ?, ";
			Query += "PROPNAME = ?, ";
			Query += "PROPVAL = ?, ";
			Query += "PDESC = ?, ";
			Query += "PCHGBYENV = ?, ";
			Query += "PACTIVE = ?, ";
			Query += "RSTATUS = ?, ";
			Query += "CREATED_BY = ?, ";
			Query += "CREATED_AT = ?, ";
			Query += "MODIFIED_AT = ?, ";
			Query += "MODIFIED_BY = ?, ";
			Query += "MADE_BY = ?, ";
			Query += "MADE_AT = ?, ";
			Query += "CHECKED_BY = ?, ";
			Query += "CHECKED_AT = ?, ";
			Query += "MAKER_LAST_CMT = ?, ";
			Query += "CHECKER_LAST_CMT = ?, ";
			Query += "CURR_APP_STATUS = ?, ";
			Query += "ADMIN_LAST_CMT = ?, ";
			Query += "INSTITUTION_ID = ? ";
			Query += "WHERE (Id = ?) ";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			Query = updateQuery(Query);
			logger.trace("updateLIDynamicpropRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			ps = con.prepareStatement(Query);
			setStringValue(ps, 1, record.getPcategory());
			setStringValue(ps, 2, record.getPropname());
			setStringValue(ps, 3, record.getPropval());
			setStringValue(ps, 4, record.getPdesc());
			setStringValue(ps, 5, record.getPchgbyenv());
			setStringValue(ps, 6, record.getPactive());
			setStringValue(ps, 7, record.getRstatus());
			setStringValue(ps, 8, record.getCreatedby());
			setDateValue(ps, 9, fd.getSQLDateObject(record.getCreatedat(), "yyyyMMddHHmmss"));
			setDateValue(ps, 10, fd.getCurrentSQLDateObject());
			setStringValue(ps, 11, record.getModifiedby());
			setStringValue(ps, 12, record.getMadeby());
			setDateValue(ps, 13, fd.getSQLDateObject(record.getMadeat(), "yyyyMMddHHmmss"));
			setStringValue(ps, 14, record.getCheckedby());
			setDateValue(ps, 15, fd.getSQLDateObject(record.getCheckedat(), "yyyyMMddHHmmss"));
			setStringValue(ps, 16, record.getMakerlastcmt());
			setStringValue(ps, 17, record.getCheckerlastcmt());
			setStringValue(ps, 18, record.getCurrappstatus());
			setStringValue(ps, 19, record.getAdminlastcmt());
			setStringValue(ps, 20, record.getInstitutionid());
			ps.setString(21, noNull(record.getId()));
			boolean result = ps.execute();
			logger.trace("updateLIDynamicpropRecord\t" + result + "\t");
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			createAuditRecord("dynamic_prop","UPDATE",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), record.getInstitutionid(), currentRecordContent, record.getActionSource());
			return result;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public boolean updateLIDynamicpropRecord(LIDynamicpropRecord record)
	throws Exception
	{
		return updateLIDynamicpropRecord(record, null, true);
	}

	public boolean deleteLIDynamicpropRecord(LIDynamicpropRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			String Query = "DELETE FROM dynamic_prop WHERE (Id = ?)";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			Query = updateQuery(Query);
			logger.trace("deleteLIDynamicpropRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			ps = con.prepareStatement(Query);
			ps.setString(1, noNull(record.getId()));
			boolean result = ps.execute();
			logger.trace("deleteLIDynamicpropRecord\t" + result + "\t");
			ps.close();
			createAuditRecord("dynamic_prop","DELETE",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), record.getInstitutionid(), record.getActionSource());
			releaseDatabaseConnection(con, closeConnection);
			return result;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public boolean deleteLIDynamicpropRecord(LIDynamicpropRecord record)
	throws Exception
	{
		return deleteLIDynamicpropRecord(record, null, true);
	}

	public LIDynamicpropRecord[] searchLIDynamicpropRecords(LIDynamicpropRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PCATEGORY", formatSearchField(searchRecord.getPcategory()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PROPNAME", formatSearchField(searchRecord.getPropname()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PROPVAL", formatSearchField(searchRecord.getPropval()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PDESC", formatSearchField(searchRecord.getPdesc()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "PCHGBYENV", formatSearchField(searchRecord.getPchgbyenv()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PACTIVE", formatSearchField(searchRecord.getPactive()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MADE_BY", formatSearchField(searchRecord.getMadeby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MADE_AT", formatSearchField(searchRecord.getMadeat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKED_BY", formatSearchField(searchRecord.getCheckedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CHECKED_AT", formatSearchField(searchRecord.getCheckedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MAKER_LAST_CMT", formatSearchField(searchRecord.getMakerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CHECKER_LAST_CMT", formatSearchField(searchRecord.getCheckerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CURR_APP_STATUS", formatSearchField(searchRecord.getCurrappstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ADMIN_LAST_CMT", formatSearchField(searchRecord.getAdminlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "INSTITUTION_ID", formatSearchField(searchRecord.getInstitutionid()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from dynamic_prop " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM dynamic_prop ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM dynamic_prop $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLIDynamicpropRecords(Query);
	}

	public LIDynamicpropRecord[] searchLIDynamicpropRecordsExact(LIDynamicpropRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PCATEGORY", formatSearchField(searchRecord.getPcategory()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PROPNAME", formatSearchField(searchRecord.getPropname()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PROPVAL", formatSearchField(searchRecord.getPropval()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PDESC", formatSearchField(searchRecord.getPdesc()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PCHGBYENV", formatSearchField(searchRecord.getPchgbyenv()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PACTIVE", formatSearchField(searchRecord.getPactive()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MADE_BY", formatSearchField(searchRecord.getMadeby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MADE_AT", formatSearchField(searchRecord.getMadeat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKED_BY", formatSearchField(searchRecord.getCheckedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKED_AT", formatSearchField(searchRecord.getCheckedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MAKER_LAST_CMT", formatSearchField(searchRecord.getMakerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKER_LAST_CMT", formatSearchField(searchRecord.getCheckerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CURR_APP_STATUS", formatSearchField(searchRecord.getCurrappstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ADMIN_LAST_CMT", formatSearchField(searchRecord.getAdminlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "INSTITUTION_ID", formatSearchField(searchRecord.getInstitutionid()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from dynamic_prop " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM dynamic_prop ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM dynamic_prop $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLIDynamicpropRecords(Query);
	}

	public LIDynamicpropRecord[] searchLIDynamicpropRecordsExactUpper(LIDynamicpropRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "PCATEGORY", formatSearchField(searchRecord.getPcategory()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "PROPNAME", formatSearchField(searchRecord.getPropname()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "PROPVAL", formatSearchField(searchRecord.getPropval()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "PDESC", formatSearchField(searchRecord.getPdesc()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "PCHGBYENV", formatSearchField(searchRecord.getPchgbyenv()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "PACTIVE", formatSearchField(searchRecord.getPactive()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MADE_BY", formatSearchField(searchRecord.getMadeby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MADE_AT", formatSearchField(searchRecord.getMadeat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CHECKED_BY", formatSearchField(searchRecord.getCheckedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CHECKED_AT", formatSearchField(searchRecord.getCheckedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MAKER_LAST_CMT", formatSearchField(searchRecord.getMakerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CHECKER_LAST_CMT", formatSearchField(searchRecord.getCheckerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CURR_APP_STATUS", formatSearchField(searchRecord.getCurrappstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ADMIN_LAST_CMT", formatSearchField(searchRecord.getAdminlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "INSTITUTION_ID", formatSearchField(searchRecord.getInstitutionid()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from dynamic_prop " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM dynamic_prop ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM dynamic_prop $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLIDynamicpropRecords(Query);
	}

	public int loadLIDynamicpropRecordCount(LIDynamicpropRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PCATEGORY", formatSearchField(searchRecord.getPcategory()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PROPNAME", formatSearchField(searchRecord.getPropname()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PROPVAL", formatSearchField(searchRecord.getPropval()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PDESC", formatSearchField(searchRecord.getPdesc()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "PCHGBYENV", formatSearchField(searchRecord.getPchgbyenv()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PACTIVE", formatSearchField(searchRecord.getPactive()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MADE_BY", formatSearchField(searchRecord.getMadeby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MADE_AT", formatSearchField(searchRecord.getMadeat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKED_BY", formatSearchField(searchRecord.getCheckedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CHECKED_AT", formatSearchField(searchRecord.getCheckedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MAKER_LAST_CMT", formatSearchField(searchRecord.getMakerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CHECKER_LAST_CMT", formatSearchField(searchRecord.getCheckerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CURR_APP_STATUS", formatSearchField(searchRecord.getCurrappstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ADMIN_LAST_CMT", formatSearchField(searchRecord.getAdminlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "INSTITUTION_ID", formatSearchField(searchRecord.getInstitutionid()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select count(*) from dynamic_prop " + WhereCondition;
		Query = updateQuery(Query);
		logger.trace("Search Count Query	" + Query + "\t");
		return loadCount(Query);
	}

	public int loadLIDynamicpropRecordCountExact(LIDynamicpropRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PCATEGORY", formatSearchField(searchRecord.getPcategory()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PROPNAME", formatSearchField(searchRecord.getPropname()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PROPVAL", formatSearchField(searchRecord.getPropval()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PDESC", formatSearchField(searchRecord.getPdesc()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PCHGBYENV", formatSearchField(searchRecord.getPchgbyenv()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PACTIVE", formatSearchField(searchRecord.getPactive()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "MADE_BY", formatSearchField(searchRecord.getMadeby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MADE_AT", formatSearchField(searchRecord.getMadeat()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "CHECKED_BY", formatSearchField(searchRecord.getCheckedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKED_AT", formatSearchField(searchRecord.getCheckedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MAKER_LAST_CMT", formatSearchField(searchRecord.getMakerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKER_LAST_CMT", formatSearchField(searchRecord.getCheckerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "CURR_APP_STATUS", formatSearchField(searchRecord.getCurrappstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ADMIN_LAST_CMT", formatSearchField(searchRecord.getAdminlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "INSTITUTION_ID", formatSearchField(searchRecord.getInstitutionid()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select count(*) from dynamic_prop " + WhereCondition;
		Query = updateQuery(Query);
		logger.trace("Search Count Query	" + Query + "\t");
		return loadCount(Query);
	}
}
