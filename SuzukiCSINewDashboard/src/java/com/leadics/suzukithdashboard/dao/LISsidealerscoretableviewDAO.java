
/*
 * LISsidealerscoretableviewDAO.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.dao;
import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.to.LISsidealerscoretableviewRecord;
import java.sql.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LISsidealerscoretableviewDAO extends LIDAO
{
	static LogUtils logger = new LogUtils(LISsidealerscoretableviewDAO.class.getName());


	public LISsidealerscoretableviewRecord[] loadLISsidealerscoretableviewRecords(String query, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
                        Statement stmt=con.createStatement();
                        int i=stmt.executeUpdate("SET @r1=0,  @r2=0,  @r3=0,  @r4=0,  @r5=0,  @r6=0,  @r7=0, @r8=0, @r9=0,@rsum=0");
                        stmt.close();
			query = query + MAX_RECORD_LIMIT_APPENDER;
			query = updateQuery(query);
			logger.trace("loadLISsidealerscoretableviewRecords\t" + closeConnection + "\t" + query);
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			ArrayList recordSet = new ArrayList();
			while(rs.next())
			{
				LISsidealerscoretableviewRecord record = new LISsidealerscoretableviewRecord();
				record.setRegion(rs.getString("region"));
				record.setDealer(rs.getString("dealer"));
				record.setSsi(rs.getString("ssi"));
				record.setSsicolor(rs.getString("ssicolor"));
				record.setSalesinitiation(rs.getString("SalesInitiation"));
				record.setSalesinitiationcolor(rs.getString("SalesInitiationColor"));
				record.setDealerfacility(rs.getString("DealerFacility"));
				record.setDealerfacilitycolor(rs.getString("DealerFacilityColor"));
				record.setDeal(rs.getString("Deal"));
				record.setDealcolor(rs.getString("DealColor"));
				record.setSalesperson(rs.getString("Salesperson"));
				record.setSalespersoncolor(rs.getString("SalespersonColor"));
				record.setDeliverytiming(rs.getString("DeliveryTiming"));
				record.setDeliverytimingcolor(rs.getString("DeliveryTimingColor"));
				record.setDeliveryprocess(rs.getString("DeliveryProcess"));
				record.setDeliveryprocesscolor(rs.getString("DeliveryProcessColor"));
				record.setSop(rs.getString("sop"));
				record.setSopcolor(rs.getString("sopColor"));
				recordSet.add(record);
			}
			logger.trace("loadLISsidealerscoretableviewRecords:Records Fetched:" + recordSet.size());
			LISsidealerscoretableviewRecord[] tempLISsidealerscoretableviewRecords = new LISsidealerscoretableviewRecord[recordSet.size()];
			for (int index = 0; index < recordSet.size(); index++)
			{
				tempLISsidealerscoretableviewRecords[index] = (LISsidealerscoretableviewRecord)(recordSet.get(index));
			}
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			return tempLISsidealerscoretableviewRecords;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}


	public LISsidealerscoretableviewRecord[] loadLISsidealerscoretableviewRecords(String query)
	throws Exception
	{
		return loadLISsidealerscoretableviewRecords(query, null, true);
	}


	public LISsidealerscoretableviewRecord loadFirstLISsidealerscoretableviewRecord(String query)
	throws Exception
	{
		LISsidealerscoretableviewRecord[] results = loadLISsidealerscoretableviewRecords(query);
		if (results == null) return null;
		if(results.length < 1) return null;
		return results[0];
	}

	public LISsidealerscoretableviewRecord loadLISsidealerscoretableviewRecord(String $pk$, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			String Query = "SELECT * FROM ssi_dealerscoretable_view WHERE ($PK$ = ?)";
			Query = updateQuery(Query);
			logger.trace("loadLISsidealerscoretableviewRecord\t" + closeConnection + "\t" + $pk$ + "\t" + Query);
			ps = con.prepareStatement(Query);
			ps.setString(1,$pk$);
			rs = ps.executeQuery();
			if (!rs.next())
			{
				ps.close();
				releaseDatabaseConnection(con, closeConnection);
				return null;
			}
			LISsidealerscoretableviewRecord record = new LISsidealerscoretableviewRecord();
			record.setRegion(rs.getString("region"));
			record.setDealer(rs.getString("dealer"));
			record.setSsi(rs.getString("ssi"));
			record.setSsicolor(rs.getString("ssicolor"));
			record.setSalesinitiation(rs.getString("SalesInitiation"));
			record.setSalesinitiationcolor(rs.getString("SalesInitiationColor"));
			record.setDealerfacility(rs.getString("DealerFacility"));
			record.setDealerfacilitycolor(rs.getString("DealerFacilityColor"));
			record.setDeal(rs.getString("Deal"));
			record.setDealcolor(rs.getString("DealColor"));
			record.setSalesperson(rs.getString("Salesperson"));
			record.setSalespersoncolor(rs.getString("SalespersonColor"));
			record.setDeliverytiming(rs.getString("DeliveryTiming"));
			record.setDeliverytimingcolor(rs.getString("DeliveryTimingColor"));
			record.setDeliveryprocess(rs.getString("DeliveryProcess"));
			record.setDeliveryprocesscolor(rs.getString("DeliveryProcessColor"));
			record.setSop(rs.getString("sop"));
			record.setSopcolor(rs.getString("sopColor"));
			ps.close();
			logger.trace("loadLISsidealerscoretableviewRecord\t" + record + "\t");
			releaseDatabaseConnection(con, closeConnection);
			return record;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public LISsidealerscoretableviewRecord loadLISsidealerscoretableviewRecord(String $pk$)
	throws Exception
	{
		return loadLISsidealerscoretableviewRecord($pk$, null, true);
	}

	public LISsidealerscoretableviewRecord[] searchLISsidealerscoretableviewRecords(LISsidealerscoretableviewRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "region", formatSearchField(searchRecord.getRegion()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "dealer", formatSearchField(searchRecord.getDealer()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ssi", formatSearchField(searchRecord.getSsi()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ssicolor", formatSearchField(searchRecord.getSsicolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SalesInitiation", formatSearchField(searchRecord.getSalesinitiation()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SalesInitiationColor", formatSearchField(searchRecord.getSalesinitiationcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "DealerFacility", formatSearchField(searchRecord.getDealerfacility()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "DealerFacilityColor", formatSearchField(searchRecord.getDealerfacilitycolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "Deal", formatSearchField(searchRecord.getDeal()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "DealColor", formatSearchField(searchRecord.getDealcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "Salesperson", formatSearchField(searchRecord.getSalesperson()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SalespersonColor", formatSearchField(searchRecord.getSalespersoncolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "DeliveryTiming", formatSearchField(searchRecord.getDeliverytiming()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "DeliveryTimingColor", formatSearchField(searchRecord.getDeliverytimingcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "DeliveryProcess", formatSearchField(searchRecord.getDeliveryprocess()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "DeliveryProcessColor", formatSearchField(searchRecord.getDeliveryprocesscolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "sop", formatSearchField(searchRecord.getSop()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "sopColor", formatSearchField(searchRecord.getSopcolor()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from ssi_dealerscoretable_view " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM ssi_dealerscoretable_view ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM ssi_dealerscoretable_view $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLISsidealerscoretableviewRecords(Query);
	}

	public LISsidealerscoretableviewRecord[] searchLISsidealerscoretableviewRecordsExact(LISsidealerscoretableviewRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "region", formatSearchField(searchRecord.getRegion()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "dealer", formatSearchField(searchRecord.getDealer()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ssi", formatSearchField(searchRecord.getSsi()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ssicolor", formatSearchField(searchRecord.getSsicolor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SalesInitiation", formatSearchField(searchRecord.getSalesinitiation()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SalesInitiationColor", formatSearchField(searchRecord.getSalesinitiationcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "DealerFacility", formatSearchField(searchRecord.getDealerfacility()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "DealerFacilityColor", formatSearchField(searchRecord.getDealerfacilitycolor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Deal", formatSearchField(searchRecord.getDeal()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "DealColor", formatSearchField(searchRecord.getDealcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Salesperson", formatSearchField(searchRecord.getSalesperson()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SalespersonColor", formatSearchField(searchRecord.getSalespersoncolor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "DeliveryTiming", formatSearchField(searchRecord.getDeliverytiming()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "DeliveryTimingColor", formatSearchField(searchRecord.getDeliverytimingcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "DeliveryProcess", formatSearchField(searchRecord.getDeliveryprocess()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "DeliveryProcessColor", formatSearchField(searchRecord.getDeliveryprocesscolor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "sop", formatSearchField(searchRecord.getSop()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "sopColor", formatSearchField(searchRecord.getSopcolor()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from ssi_dealerscoretable_view " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM ssi_dealerscoretable_view ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM ssi_dealerscoretable_view $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLISsidealerscoretableviewRecords(Query);
	}

	public LISsidealerscoretableviewRecord[] searchLISsidealerscoretableviewRecordsExactUpper(LISsidealerscoretableviewRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "region", formatSearchField(searchRecord.getRegion()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "dealer", formatSearchField(searchRecord.getDealer()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ssi", formatSearchField(searchRecord.getSsi()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ssicolor", formatSearchField(searchRecord.getSsicolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "SalesInitiation", formatSearchField(searchRecord.getSalesinitiation()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "SalesInitiationColor", formatSearchField(searchRecord.getSalesinitiationcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "DealerFacility", formatSearchField(searchRecord.getDealerfacility()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "DealerFacilityColor", formatSearchField(searchRecord.getDealerfacilitycolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "Deal", formatSearchField(searchRecord.getDeal()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "DealColor", formatSearchField(searchRecord.getDealcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "Salesperson", formatSearchField(searchRecord.getSalesperson()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "SalespersonColor", formatSearchField(searchRecord.getSalespersoncolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "DeliveryTiming", formatSearchField(searchRecord.getDeliverytiming()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "DeliveryTimingColor", formatSearchField(searchRecord.getDeliverytimingcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "DeliveryProcess", formatSearchField(searchRecord.getDeliveryprocess()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "DeliveryProcessColor", formatSearchField(searchRecord.getDeliveryprocesscolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "sop", formatSearchField(searchRecord.getSop()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "sopColor", formatSearchField(searchRecord.getSopcolor()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from ssi_dealerscoretable_view " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM ssi_dealerscoretable_view ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM ssi_dealerscoretable_view $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLISsidealerscoretableviewRecords(Query);
	}

	public int loadLISsidealerscoretableviewRecordCount(LISsidealerscoretableviewRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "region", formatSearchField(searchRecord.getRegion()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "dealer", formatSearchField(searchRecord.getDealer()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ssi", formatSearchField(searchRecord.getSsi()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ssicolor", formatSearchField(searchRecord.getSsicolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SalesInitiation", formatSearchField(searchRecord.getSalesinitiation()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SalesInitiationColor", formatSearchField(searchRecord.getSalesinitiationcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "DealerFacility", formatSearchField(searchRecord.getDealerfacility()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "DealerFacilityColor", formatSearchField(searchRecord.getDealerfacilitycolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "Deal", formatSearchField(searchRecord.getDeal()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "DealColor", formatSearchField(searchRecord.getDealcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "Salesperson", formatSearchField(searchRecord.getSalesperson()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SalespersonColor", formatSearchField(searchRecord.getSalespersoncolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "DeliveryTiming", formatSearchField(searchRecord.getDeliverytiming()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "DeliveryTimingColor", formatSearchField(searchRecord.getDeliverytimingcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "DeliveryProcess", formatSearchField(searchRecord.getDeliveryprocess()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "DeliveryProcessColor", formatSearchField(searchRecord.getDeliveryprocesscolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "sop", formatSearchField(searchRecord.getSop()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "sopColor", formatSearchField(searchRecord.getSopcolor()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select count(*) from ssi_dealerscoretable_view " + WhereCondition;
		Query = updateQuery(Query);
		logger.trace("Search Count Query	" + Query + "\t");
		return loadCount(Query);
	}

	public int loadLISsidealerscoretableviewRecordCountExact(LISsidealerscoretableviewRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "region", formatSearchField(searchRecord.getRegion()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "dealer", formatSearchField(searchRecord.getDealer()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ssi", formatSearchField(searchRecord.getSsi()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ssicolor", formatSearchField(searchRecord.getSsicolor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SalesInitiation", formatSearchField(searchRecord.getSalesinitiation()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SalesInitiationColor", formatSearchField(searchRecord.getSalesinitiationcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "DealerFacility", formatSearchField(searchRecord.getDealerfacility()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "DealerFacilityColor", formatSearchField(searchRecord.getDealerfacilitycolor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Deal", formatSearchField(searchRecord.getDeal()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "DealColor", formatSearchField(searchRecord.getDealcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Salesperson", formatSearchField(searchRecord.getSalesperson()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SalespersonColor", formatSearchField(searchRecord.getSalespersoncolor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "DeliveryTiming", formatSearchField(searchRecord.getDeliverytiming()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "DeliveryTimingColor", formatSearchField(searchRecord.getDeliverytimingcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "DeliveryProcess", formatSearchField(searchRecord.getDeliveryprocess()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "DeliveryProcessColor", formatSearchField(searchRecord.getDeliveryprocesscolor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "sop", formatSearchField(searchRecord.getSop()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "sopColor", formatSearchField(searchRecord.getSopcolor()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select count(*) from ssi_dealerscoretable_view " + WhereCondition;
		Query = updateQuery(Query);
		logger.trace("Search Count Query	" + Query + "\t");
		return loadCount(Query);
	}
}
