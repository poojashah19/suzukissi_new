
/*
 * LISystemuserDAO.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.dao;
import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.to.LISystemuserRecord;
import java.sql.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LISystemuserDAO extends LIDAO
{
	static LogUtils logger = new LogUtils(LISystemuserDAO.class.getName());


	public LISystemuserRecord[] loadLISystemuserRecords(String query, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			query = query + MAX_RECORD_LIMIT_APPENDER;
			query = updateQuery(query);
			logger.trace("loadLISystemuserRecords\t" + closeConnection + "\t" + query);
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			ArrayList recordSet = new ArrayList();
			while(rs.next())
			{
				LISystemuserRecord record = new LISystemuserRecord();
				record.setId(rs.getString("Id"));
				record.setUserid(rs.getString("USER_ID"));
				record.setUserpassword(rs.getString("USER_PASSWORD"));
				record.setPrefix(rs.getString("PREFIX"));
				record.setFirstname(rs.getString("FIRST_NAME"));
				record.setMiddlename(rs.getString("MIDDLE_NAME"));
				record.setLastname(rs.getString("LAST_NAME"));
				record.setEmail(rs.getString("EMAIL"));
				record.setMobile(rs.getString("MOBILE"));
				record.setInstitutionid(rs.getString("INSTITUTION_ID"));
				record.setCountry(rs.getString("COUNTRY"));
				record.setLastlogindate(formatDBDateTime(rs.getTimestamp("LAST_LOGIN_DATE")));
				record.setLastloginfdate(formatDBDateTime(rs.getTimestamp("LAST_LOGIN_FDATE")));
				record.setLoginattempts(rs.getString("LOGIN_ATTEMPTS"));
				record.setPreflang(rs.getString("PREF_LANG"));
				record.setPrefcomm(rs.getString("PREF_COMM"));
				record.setUserroleid(rs.getString("USER_ROLE_ID"));
				record.setSupid(rs.getString("SUP_ID"));
				record.setSeqq1(rs.getString("SEQ_Q1"));
				record.setSeqq2(rs.getString("SEQ_Q2"));
				record.setSeqq3(rs.getString("SEQ_Q3"));
				record.setSeqa1(rs.getString("SEQ_A1"));
				record.setSeqa2(rs.getString("SEQ_A2"));
				record.setSeqa3(rs.getString("SEQ_A3"));
				record.setMadeby(rs.getString("MADE_BY"));
				record.setMadeat(formatDBDateTime(rs.getTimestamp("MADE_AT")));
				record.setCheckedby(rs.getString("CHECKED_BY"));
				record.setCheckedat(formatDBDateTime(rs.getTimestamp("CHECKED_AT")));
				record.setMakerlastcmt(rs.getString("MAKER_LAST_CMT"));
				record.setCheckerlastcmt(rs.getString("CHECKER_LAST_CMT"));
				record.setCurrappstatus(rs.getString("CURR_APP_STATUS"));
				record.setAdminlastcmt(rs.getString("ADMIN_LAST_CMT"));
				record.setRstatus(rs.getString("RSTATUS"));
				record.setCreatedby(rs.getString("CREATED_BY"));
				record.setCreatedat(formatDBDateTime(rs.getTimestamp("CREATED_AT")));
				record.setModifiedat(formatDBDateTime(rs.getTimestamp("MODIFIED_AT")));
				record.setModifiedby(rs.getString("MODIFIED_BY"));
				record.setBlockedflag(rs.getString("BLOCKED_FLAG"));
				record.setPwdsetdate(formatDBDateTime(rs.getTimestamp("PWDSETDATE")));
				record.setInitialpwd(rs.getString("INITIAL_PWD"));
				record.setGroupcode(rs.getString("GROUP_CODE"));
				record.setOtpgen(rs.getString("OTP_GEN"));
				record.setOtpgenat(rs.getString("OTP_GEN_AT"));
				record.setAddress(rs.getString("address"));
				record.setCity(rs.getString("city"));
				record.setDregion(rs.getString("dregion"));
				record.setDgroup(rs.getString("dgroup"));
				recordSet.add(record);
			}
			logger.trace("loadLISystemuserRecords:Records Fetched:" + recordSet.size());
			LISystemuserRecord[] tempLISystemuserRecords = new LISystemuserRecord[recordSet.size()];
			for (int index = 0; index < recordSet.size(); index++)
			{
				tempLISystemuserRecords[index] = (LISystemuserRecord)(recordSet.get(index));
			}
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			return tempLISystemuserRecords;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}


	public LISystemuserRecord[] loadLISystemuserRecords(String query)
	throws Exception
	{
		return loadLISystemuserRecords(query, null, true);
	}


	public LISystemuserRecord loadFirstLISystemuserRecord(String query)
	throws Exception
	{
		LISystemuserRecord[] results = loadLISystemuserRecords(query);
		if (results == null) return null;
		if(results.length < 1) return null;
		return results[0];
	}

	public LISystemuserRecord loadLISystemuserRecord(String id, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			String Query = "SELECT * FROM system_user WHERE (Id = ?)";
			Query = updateQuery(Query);
			logger.trace("loadLISystemuserRecord\t" + closeConnection + "\t" + id + "\t" + Query);
			ps = con.prepareStatement(Query);
			ps.setString(1,id);
			rs = ps.executeQuery();
			if (!rs.next())
			{
				ps.close();
				releaseDatabaseConnection(con, closeConnection);
				return null;
			}
			LISystemuserRecord record = new LISystemuserRecord();
			record.setId(rs.getString("Id"));
			record.setUserid(rs.getString("USER_ID"));
			record.setUserpassword(rs.getString("USER_PASSWORD"));
			record.setPrefix(rs.getString("PREFIX"));
			record.setFirstname(rs.getString("FIRST_NAME"));
			record.setMiddlename(rs.getString("MIDDLE_NAME"));
			record.setLastname(rs.getString("LAST_NAME"));
			record.setEmail(rs.getString("EMAIL"));
			record.setMobile(rs.getString("MOBILE"));
			record.setInstitutionid(rs.getString("INSTITUTION_ID"));
			record.setCountry(rs.getString("COUNTRY"));
			record.setLastlogindate(formatDBDateTime(rs.getTimestamp("LAST_LOGIN_DATE")));
			record.setLastloginfdate(formatDBDateTime(rs.getTimestamp("LAST_LOGIN_FDATE")));
			record.setLoginattempts(rs.getString("LOGIN_ATTEMPTS"));
			record.setPreflang(rs.getString("PREF_LANG"));
			record.setPrefcomm(rs.getString("PREF_COMM"));
			record.setUserroleid(rs.getString("USER_ROLE_ID"));
			record.setSupid(rs.getString("SUP_ID"));
			record.setSeqq1(rs.getString("SEQ_Q1"));
			record.setSeqq2(rs.getString("SEQ_Q2"));
			record.setSeqq3(rs.getString("SEQ_Q3"));
			record.setSeqa1(rs.getString("SEQ_A1"));
			record.setSeqa2(rs.getString("SEQ_A2"));
			record.setSeqa3(rs.getString("SEQ_A3"));
			record.setMadeby(rs.getString("MADE_BY"));
			record.setMadeat(formatDBDateTime(rs.getTimestamp("MADE_AT")));
			record.setCheckedby(rs.getString("CHECKED_BY"));
			record.setCheckedat(formatDBDateTime(rs.getTimestamp("CHECKED_AT")));
			record.setMakerlastcmt(rs.getString("MAKER_LAST_CMT"));
			record.setCheckerlastcmt(rs.getString("CHECKER_LAST_CMT"));
			record.setCurrappstatus(rs.getString("CURR_APP_STATUS"));
			record.setAdminlastcmt(rs.getString("ADMIN_LAST_CMT"));
			record.setRstatus(rs.getString("RSTATUS"));
			record.setCreatedby(rs.getString("CREATED_BY"));
			record.setCreatedat(formatDBDateTime(rs.getTimestamp("CREATED_AT")));
			record.setModifiedat(formatDBDateTime(rs.getTimestamp("MODIFIED_AT")));
			record.setModifiedby(rs.getString("MODIFIED_BY"));
			record.setBlockedflag(rs.getString("BLOCKED_FLAG"));
			record.setPwdsetdate(formatDBDateTime(rs.getTimestamp("PWDSETDATE")));
			record.setInitialpwd(rs.getString("INITIAL_PWD"));
			record.setGroupcode(rs.getString("GROUP_CODE"));
			record.setOtpgen(rs.getString("OTP_GEN"));
			record.setOtpgenat(rs.getString("OTP_GEN_AT"));
			record.setAddress(rs.getString("address"));
			record.setCity(rs.getString("city"));
			record.setDregion(rs.getString("dregion"));
			record.setDgroup(rs.getString("dgroup"));
			ps.close();
			logger.trace("loadLISystemuserRecord\t" + record + "\t");
			releaseDatabaseConnection(con, closeConnection);
			return record;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public LISystemuserRecord loadLISystemuserRecord(String id)
	throws Exception
	{
		return loadLISystemuserRecord(id, null, true);
	}

	public int insertLISystemuserRecord(LISystemuserRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			String Query = "INSERT INTO system_user ";
			Query += "(";
			Query += "USER_ID,USER_PASSWORD,PREFIX,FIRST_NAME,MIDDLE_NAME,LAST_NAME,EMAIL,MOBILE,INSTITUTION_ID,COUNTRY,LAST_LOGIN_DATE,LAST_LOGIN_FDATE,LOGIN_ATTEMPTS,PREF_LANG,PREF_COMM,USER_ROLE_ID,SUP_ID,SEQ_Q1,SEQ_Q2,SEQ_Q3,SEQ_A1,SEQ_A2,SEQ_A3,MADE_BY,MADE_AT,CHECKED_BY,CHECKED_AT,MAKER_LAST_CMT,CHECKER_LAST_CMT,CURR_APP_STATUS,ADMIN_LAST_CMT,RSTATUS,CREATED_BY,CREATED_AT,MODIFIED_AT,MODIFIED_BY,BLOCKED_FLAG,PWDSETDATE,INITIAL_PWD,GROUP_CODE,OTP_GEN,OTP_GEN_AT,address,city,dregion,dgroup";
			Query += ")";
			Query += " VALUES ";
			Query += "(";
			Query += "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?";
			Query += ")";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			record.setModifiedby(StringUtils.noNull(record.getModifiedby(),record.getCreatedby()));
			record.setMadeat(StringUtils.noNull(record.getMadeat(),record.getCreatedat()));
			record.setMadeby(StringUtils.noNull(record.getMadeby(),record.getCreatedby()));
			record.setInstitutionid(StringUtils.noNull(record.getInstitutionid(),PropertyUtil.getProperty("DefaultInstitutionId")));
			Query = updateQuery(Query);
			logger.trace("insertLISystemuserRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			if (isOracleDatabase()) 
			{
				ps = con.prepareStatement(Query,new String[]{"ID"});
			}
			else
			{
				ps = con.prepareStatement(Query,Statement.RETURN_GENERATED_KEYS);
			}
			setStringValue(ps, 1, record.getUserid());
			setStringValue(ps, 2, record.getUserpassword());
			setStringValue(ps, 3, record.getPrefix());
			setStringValue(ps, 4, record.getFirstname());
			setStringValue(ps, 5, record.getMiddlename());
			setStringValue(ps, 6, record.getLastname());
			setStringValue(ps, 7, record.getEmail());
			setStringValue(ps, 8, record.getMobile());
			setStringValue(ps, 9, record.getInstitutionid());
			setStringValue(ps, 10, record.getCountry());
			setDateValue(ps, 11, fd.getSQLDateObject(record.getLastlogindate(), "yyyyMMddHHmmss"));
			setDateValue(ps, 12, fd.getSQLDateObject(record.getLastloginfdate(), "yyyyMMddHHmmss"));
			setStringValue(ps, 13, record.getLoginattempts());
			setStringValue(ps, 14, record.getPreflang());
			setStringValue(ps, 15, record.getPrefcomm());
			setStringValue(ps, 16, record.getUserroleid());
			setStringValue(ps, 17, record.getSupid());
			setStringValue(ps, 18, record.getSeqq1());
			setStringValue(ps, 19, record.getSeqq2());
			setStringValue(ps, 20, record.getSeqq3());
			setStringValue(ps, 21, record.getSeqa1());
			setStringValue(ps, 22, record.getSeqa2());
			setStringValue(ps, 23, record.getSeqa3());
			setStringValue(ps, 24, record.getMadeby());
			setDateValue(ps, 25, fd.getSQLDateObject(record.getMadeat(), "yyyyMMddHHmmss"));
			setStringValue(ps, 26, record.getCheckedby());
			setDateValue(ps, 27, fd.getSQLDateObject(record.getCheckedat(), "yyyyMMddHHmmss"));
			setStringValue(ps, 28, record.getMakerlastcmt());
			setStringValue(ps, 29, record.getCheckerlastcmt());
			setStringValue(ps, 30, record.getCurrappstatus());
			setStringValue(ps, 31, record.getAdminlastcmt());
			setStringValue(ps, 32, record.getRstatus());
			setStringValue(ps, 33, record.getCreatedby());
			setDateValue(ps, 34, fd.getCurrentSQLDateObject());
			setDateValue(ps, 35, fd.getCurrentSQLDateObject());
			setStringValue(ps, 36, record.getModifiedby());
			setStringValue(ps, 37, record.getBlockedflag());
			setDateValue(ps, 38, fd.getSQLDateObject(record.getPwdsetdate(), "yyyyMMddHHmmss"));
			setStringValue(ps, 39, record.getInitialpwd());
			setStringValue(ps, 40, record.getGroupcode());
			setStringValue(ps, 41, record.getOtpgen());
			setStringValue(ps, 42, record.getOtpgenat());
			setStringValue(ps, 43, record.getAddress());
			setStringValue(ps, 44, record.getCity());
			setStringValue(ps, 45, record.getDregion());
			setStringValue(ps, 46, record.getDgroup());
			boolean result = ps.execute();
			logger.trace("insertLISystemuserRecord\t" + result + "\t");
			int resultID = -1;
			rs = ps.getGeneratedKeys();
			if (rs.next()) 
			{
				resultID = rs.getInt(1); 
			}
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			createAuditRecord("system_user","INSERT",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), record.getInstitutionid(), record.getActionSource());
			return resultID;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public int insertLISystemuserRecord(LISystemuserRecord record)
	throws Exception
	{
		return insertLISystemuserRecord(record, null, true);
	}

	public boolean updateLISystemuserRecord(LISystemuserRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			LISystemuserRecord currentRecord = loadLISystemuserRecord(record.getId());
			String currentRecordContent = StringUtils.noNull(currentRecord);

			String Query = "UPDATE system_user SET ";
			Query += "USER_ID = ?, ";
			Query += "USER_PASSWORD = ?, ";
			Query += "PREFIX = ?, ";
			Query += "FIRST_NAME = ?, ";
			Query += "MIDDLE_NAME = ?, ";
			Query += "LAST_NAME = ?, ";
			Query += "EMAIL = ?, ";
			Query += "MOBILE = ?, ";
			Query += "INSTITUTION_ID = ?, ";
			Query += "COUNTRY = ?, ";
			Query += "LAST_LOGIN_DATE = ?, ";
			Query += "LAST_LOGIN_FDATE = ?, ";
			Query += "LOGIN_ATTEMPTS = ?, ";
			Query += "PREF_LANG = ?, ";
			Query += "PREF_COMM = ?, ";
			Query += "USER_ROLE_ID = ?, ";
			Query += "SUP_ID = ?, ";
			Query += "SEQ_Q1 = ?, ";
			Query += "SEQ_Q2 = ?, ";
			Query += "SEQ_Q3 = ?, ";
			Query += "SEQ_A1 = ?, ";
			Query += "SEQ_A2 = ?, ";
			Query += "SEQ_A3 = ?, ";
			Query += "MADE_BY = ?, ";
			Query += "MADE_AT = ?, ";
			Query += "CHECKED_BY = ?, ";
			Query += "CHECKED_AT = ?, ";
			Query += "MAKER_LAST_CMT = ?, ";
			Query += "CHECKER_LAST_CMT = ?, ";
			Query += "CURR_APP_STATUS = ?, ";
			Query += "ADMIN_LAST_CMT = ?, ";
			Query += "RSTATUS = ?, ";
			Query += "CREATED_BY = ?, ";
			Query += "CREATED_AT = ?, ";
			Query += "MODIFIED_AT = ?, ";
			Query += "MODIFIED_BY = ?, ";
			Query += "BLOCKED_FLAG = ?, ";
			Query += "PWDSETDATE = ?, ";
			Query += "INITIAL_PWD = ?, ";
			Query += "GROUP_CODE = ?, ";
			Query += "OTP_GEN = ?, ";
			Query += "OTP_GEN_AT = ?, ";
			Query += "address = ?, ";
			Query += "city = ?, ";
			Query += "dregion = ?, ";
			Query += "dgroup = ? ";
			Query += "WHERE (Id = ?) ";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			Query = updateQuery(Query);
			logger.trace("updateLISystemuserRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			ps = con.prepareStatement(Query);
			setStringValue(ps, 1, record.getUserid());
			setStringValue(ps, 2, record.getUserpassword());
			setStringValue(ps, 3, record.getPrefix());
			setStringValue(ps, 4, record.getFirstname());
			setStringValue(ps, 5, record.getMiddlename());
			setStringValue(ps, 6, record.getLastname());
			setStringValue(ps, 7, record.getEmail());
			setStringValue(ps, 8, record.getMobile());
			setStringValue(ps, 9, record.getInstitutionid());
			setStringValue(ps, 10, record.getCountry());
			setDateValue(ps, 11, fd.getSQLDateObject(record.getLastlogindate(), "yyyyMMddHHmmss"));
			setDateValue(ps, 12, fd.getSQLDateObject(record.getLastloginfdate(), "yyyyMMddHHmmss"));
			setStringValue(ps, 13, record.getLoginattempts());
			setStringValue(ps, 14, record.getPreflang());
			setStringValue(ps, 15, record.getPrefcomm());
			setStringValue(ps, 16, record.getUserroleid());
			setStringValue(ps, 17, record.getSupid());
			setStringValue(ps, 18, record.getSeqq1());
			setStringValue(ps, 19, record.getSeqq2());
			setStringValue(ps, 20, record.getSeqq3());
			setStringValue(ps, 21, record.getSeqa1());
			setStringValue(ps, 22, record.getSeqa2());
			setStringValue(ps, 23, record.getSeqa3());
			setStringValue(ps, 24, record.getMadeby());
			setDateValue(ps, 25, fd.getSQLDateObject(record.getMadeat(), "yyyyMMddHHmmss"));
			setStringValue(ps, 26, record.getCheckedby());
			setDateValue(ps, 27, fd.getSQLDateObject(record.getCheckedat(), "yyyyMMddHHmmss"));
			setStringValue(ps, 28, record.getMakerlastcmt());
			setStringValue(ps, 29, record.getCheckerlastcmt());
			setStringValue(ps, 30, record.getCurrappstatus());
			setStringValue(ps, 31, record.getAdminlastcmt());
			setStringValue(ps, 32, record.getRstatus());
			setStringValue(ps, 33, record.getCreatedby());
			setDateValue(ps, 34, fd.getSQLDateObject(record.getCreatedat(), "yyyyMMddHHmmss"));
			setDateValue(ps, 35, fd.getCurrentSQLDateObject());
			setStringValue(ps, 36, record.getModifiedby());
			setStringValue(ps, 37, record.getBlockedflag());
			setDateValue(ps, 38, fd.getSQLDateObject(record.getPwdsetdate(), "yyyyMMddHHmmss"));
			setStringValue(ps, 39, record.getInitialpwd());
			setStringValue(ps, 40, record.getGroupcode());
			setStringValue(ps, 41, record.getOtpgen());
			setStringValue(ps, 42, record.getOtpgenat());
			setStringValue(ps, 43, record.getAddress());
			setStringValue(ps, 44, record.getCity());
			setStringValue(ps, 45, record.getDregion());
			setStringValue(ps, 46, record.getDgroup());
			ps.setString(47, noNull(record.getId()));
			boolean result = ps.execute();
			logger.trace("updateLISystemuserRecord\t" + result + "\t");
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			createAuditRecord("system_user","UPDATE",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), record.getInstitutionid(), currentRecordContent, record.getActionSource());
			return result;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public boolean updateLISystemuserRecord(LISystemuserRecord record)
	throws Exception
	{
		return updateLISystemuserRecord(record, null, true);
	}

	public boolean deleteLISystemuserRecord(LISystemuserRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			String Query = "DELETE FROM system_user WHERE (Id = ?)";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			Query = updateQuery(Query);
			logger.trace("deleteLISystemuserRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			ps = con.prepareStatement(Query);
			ps.setString(1, noNull(record.getId()));
			boolean result = ps.execute();
			logger.trace("deleteLISystemuserRecord\t" + result + "\t");
			ps.close();
			createAuditRecord("system_user","DELETE",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), record.getInstitutionid(), record.getActionSource());
			releaseDatabaseConnection(con, closeConnection);
			return result;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public boolean deleteLISystemuserRecord(LISystemuserRecord record)
	throws Exception
	{
		return deleteLISystemuserRecord(record, null, true);
	}

	public LISystemuserRecord[] searchLISystemuserRecords(LISystemuserRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "USER_ID", formatSearchField(searchRecord.getUserid()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "USER_PASSWORD", formatSearchField(searchRecord.getUserpassword()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PREFIX", formatSearchField(searchRecord.getPrefix()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "FIRST_NAME", formatSearchField(searchRecord.getFirstname()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MIDDLE_NAME", formatSearchField(searchRecord.getMiddlename()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "LAST_NAME", formatSearchField(searchRecord.getLastname()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "EMAIL", formatSearchField(searchRecord.getEmail()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MOBILE", formatSearchField(searchRecord.getMobile()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "INSTITUTION_ID", formatSearchField(searchRecord.getInstitutionid()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "COUNTRY", formatSearchField(searchRecord.getCountry()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "LAST_LOGIN_DATE", formatSearchField(searchRecord.getLastlogindate()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "LAST_LOGIN_FDATE", formatSearchField(searchRecord.getLastloginfdate()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "LOGIN_ATTEMPTS", formatSearchField(searchRecord.getLoginattempts()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PREF_LANG", formatSearchField(searchRecord.getPreflang()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PREF_COMM", formatSearchField(searchRecord.getPrefcomm()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "USER_ROLE_ID", formatSearchField(searchRecord.getUserroleid()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SUP_ID", formatSearchField(searchRecord.getSupid()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SEQ_Q1", formatSearchField(searchRecord.getSeqq1()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SEQ_Q2", formatSearchField(searchRecord.getSeqq2()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SEQ_Q3", formatSearchField(searchRecord.getSeqq3()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SEQ_A1", formatSearchField(searchRecord.getSeqa1()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SEQ_A2", formatSearchField(searchRecord.getSeqa2()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SEQ_A3", formatSearchField(searchRecord.getSeqa3()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MADE_BY", formatSearchField(searchRecord.getMadeby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MADE_AT", formatSearchField(searchRecord.getMadeat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKED_BY", formatSearchField(searchRecord.getCheckedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CHECKED_AT", formatSearchField(searchRecord.getCheckedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MAKER_LAST_CMT", formatSearchField(searchRecord.getMakerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CHECKER_LAST_CMT", formatSearchField(searchRecord.getCheckerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CURR_APP_STATUS", formatSearchField(searchRecord.getCurrappstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ADMIN_LAST_CMT", formatSearchField(searchRecord.getAdminlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "BLOCKED_FLAG", formatSearchField(searchRecord.getBlockedflag()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PWDSETDATE", formatSearchField(searchRecord.getPwdsetdate()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "INITIAL_PWD", formatSearchField(searchRecord.getInitialpwd()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "GROUP_CODE", formatSearchField(searchRecord.getGroupcode()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "OTP_GEN", formatSearchField(searchRecord.getOtpgen()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "OTP_GEN_AT", formatSearchField(searchRecord.getOtpgenat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "address", formatSearchField(searchRecord.getAddress()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "city", formatSearchField(searchRecord.getCity()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "dregion", formatSearchField(searchRecord.getDregion()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "dgroup", formatSearchField(searchRecord.getDgroup()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from system_user " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM system_user ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM system_user $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLISystemuserRecords(Query);
	}

	public LISystemuserRecord[] searchLISystemuserRecordsExact(LISystemuserRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "USER_ID", formatSearchField(searchRecord.getUserid()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "USER_PASSWORD", formatSearchField(searchRecord.getUserpassword()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PREFIX", formatSearchField(searchRecord.getPrefix()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "FIRST_NAME", formatSearchField(searchRecord.getFirstname()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MIDDLE_NAME", formatSearchField(searchRecord.getMiddlename()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "LAST_NAME", formatSearchField(searchRecord.getLastname()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "EMAIL", formatSearchField(searchRecord.getEmail()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MOBILE", formatSearchField(searchRecord.getMobile()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "INSTITUTION_ID", formatSearchField(searchRecord.getInstitutionid()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "COUNTRY", formatSearchField(searchRecord.getCountry()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "LAST_LOGIN_DATE", formatSearchField(searchRecord.getLastlogindate()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "LAST_LOGIN_FDATE", formatSearchField(searchRecord.getLastloginfdate()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "LOGIN_ATTEMPTS", formatSearchField(searchRecord.getLoginattempts()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PREF_LANG", formatSearchField(searchRecord.getPreflang()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PREF_COMM", formatSearchField(searchRecord.getPrefcomm()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "USER_ROLE_ID", formatSearchField(searchRecord.getUserroleid()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SUP_ID", formatSearchField(searchRecord.getSupid()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SEQ_Q1", formatSearchField(searchRecord.getSeqq1()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SEQ_Q2", formatSearchField(searchRecord.getSeqq2()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SEQ_Q3", formatSearchField(searchRecord.getSeqq3()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SEQ_A1", formatSearchField(searchRecord.getSeqa1()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SEQ_A2", formatSearchField(searchRecord.getSeqa2()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SEQ_A3", formatSearchField(searchRecord.getSeqa3()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MADE_BY", formatSearchField(searchRecord.getMadeby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MADE_AT", formatSearchField(searchRecord.getMadeat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKED_BY", formatSearchField(searchRecord.getCheckedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKED_AT", formatSearchField(searchRecord.getCheckedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MAKER_LAST_CMT", formatSearchField(searchRecord.getMakerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKER_LAST_CMT", formatSearchField(searchRecord.getCheckerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CURR_APP_STATUS", formatSearchField(searchRecord.getCurrappstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ADMIN_LAST_CMT", formatSearchField(searchRecord.getAdminlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "BLOCKED_FLAG", formatSearchField(searchRecord.getBlockedflag()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PWDSETDATE", formatSearchField(searchRecord.getPwdsetdate()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "INITIAL_PWD", formatSearchField(searchRecord.getInitialpwd()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "GROUP_CODE", formatSearchField(searchRecord.getGroupcode()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "OTP_GEN", formatSearchField(searchRecord.getOtpgen()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "OTP_GEN_AT", formatSearchField(searchRecord.getOtpgenat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "address", formatSearchField(searchRecord.getAddress()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "city", formatSearchField(searchRecord.getCity()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "dregion", formatSearchField(searchRecord.getDregion()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "dgroup", formatSearchField(searchRecord.getDgroup()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from system_user " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM system_user ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM system_user $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLISystemuserRecords(Query);
	}

	public LISystemuserRecord[] searchLISystemuserRecordsExactUpper(LISystemuserRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "USER_ID", formatSearchField(searchRecord.getUserid()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "USER_PASSWORD", formatSearchField(searchRecord.getUserpassword()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "PREFIX", formatSearchField(searchRecord.getPrefix()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "FIRST_NAME", formatSearchField(searchRecord.getFirstname()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MIDDLE_NAME", formatSearchField(searchRecord.getMiddlename()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "LAST_NAME", formatSearchField(searchRecord.getLastname()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "EMAIL", formatSearchField(searchRecord.getEmail()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MOBILE", formatSearchField(searchRecord.getMobile()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "INSTITUTION_ID", formatSearchField(searchRecord.getInstitutionid()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "COUNTRY", formatSearchField(searchRecord.getCountry()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "LAST_LOGIN_DATE", formatSearchField(searchRecord.getLastlogindate()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "LAST_LOGIN_FDATE", formatSearchField(searchRecord.getLastloginfdate()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "LOGIN_ATTEMPTS", formatSearchField(searchRecord.getLoginattempts()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "PREF_LANG", formatSearchField(searchRecord.getPreflang()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "PREF_COMM", formatSearchField(searchRecord.getPrefcomm()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "USER_ROLE_ID", formatSearchField(searchRecord.getUserroleid()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "SUP_ID", formatSearchField(searchRecord.getSupid()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "SEQ_Q1", formatSearchField(searchRecord.getSeqq1()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "SEQ_Q2", formatSearchField(searchRecord.getSeqq2()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "SEQ_Q3", formatSearchField(searchRecord.getSeqq3()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "SEQ_A1", formatSearchField(searchRecord.getSeqa1()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "SEQ_A2", formatSearchField(searchRecord.getSeqa2()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "SEQ_A3", formatSearchField(searchRecord.getSeqa3()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MADE_BY", formatSearchField(searchRecord.getMadeby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MADE_AT", formatSearchField(searchRecord.getMadeat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CHECKED_BY", formatSearchField(searchRecord.getCheckedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CHECKED_AT", formatSearchField(searchRecord.getCheckedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MAKER_LAST_CMT", formatSearchField(searchRecord.getMakerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CHECKER_LAST_CMT", formatSearchField(searchRecord.getCheckerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CURR_APP_STATUS", formatSearchField(searchRecord.getCurrappstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ADMIN_LAST_CMT", formatSearchField(searchRecord.getAdminlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "BLOCKED_FLAG", formatSearchField(searchRecord.getBlockedflag()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "PWDSETDATE", formatSearchField(searchRecord.getPwdsetdate()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "INITIAL_PWD", formatSearchField(searchRecord.getInitialpwd()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "GROUP_CODE", formatSearchField(searchRecord.getGroupcode()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "OTP_GEN", formatSearchField(searchRecord.getOtpgen()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "OTP_GEN_AT", formatSearchField(searchRecord.getOtpgenat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "address", formatSearchField(searchRecord.getAddress()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "city", formatSearchField(searchRecord.getCity()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "dregion", formatSearchField(searchRecord.getDregion()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "dgroup", formatSearchField(searchRecord.getDgroup()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from system_user " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM system_user ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM system_user $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLISystemuserRecords(Query);
	}

	public int loadLISystemuserRecordCount(LISystemuserRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "USER_ID", formatSearchField(searchRecord.getUserid()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "USER_PASSWORD", formatSearchField(searchRecord.getUserpassword()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PREFIX", formatSearchField(searchRecord.getPrefix()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "FIRST_NAME", formatSearchField(searchRecord.getFirstname()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MIDDLE_NAME", formatSearchField(searchRecord.getMiddlename()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "LAST_NAME", formatSearchField(searchRecord.getLastname()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "EMAIL", formatSearchField(searchRecord.getEmail()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MOBILE", formatSearchField(searchRecord.getMobile()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "INSTITUTION_ID", formatSearchField(searchRecord.getInstitutionid()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "COUNTRY", formatSearchField(searchRecord.getCountry()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "LAST_LOGIN_DATE", formatSearchField(searchRecord.getLastlogindate()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "LAST_LOGIN_FDATE", formatSearchField(searchRecord.getLastloginfdate()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "LOGIN_ATTEMPTS", formatSearchField(searchRecord.getLoginattempts()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PREF_LANG", formatSearchField(searchRecord.getPreflang()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PREF_COMM", formatSearchField(searchRecord.getPrefcomm()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "USER_ROLE_ID", formatSearchField(searchRecord.getUserroleid()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SUP_ID", formatSearchField(searchRecord.getSupid()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SEQ_Q1", formatSearchField(searchRecord.getSeqq1()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SEQ_Q2", formatSearchField(searchRecord.getSeqq2()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SEQ_Q3", formatSearchField(searchRecord.getSeqq3()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SEQ_A1", formatSearchField(searchRecord.getSeqa1()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SEQ_A2", formatSearchField(searchRecord.getSeqa2()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SEQ_A3", formatSearchField(searchRecord.getSeqa3()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MADE_BY", formatSearchField(searchRecord.getMadeby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MADE_AT", formatSearchField(searchRecord.getMadeat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKED_BY", formatSearchField(searchRecord.getCheckedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CHECKED_AT", formatSearchField(searchRecord.getCheckedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MAKER_LAST_CMT", formatSearchField(searchRecord.getMakerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CHECKER_LAST_CMT", formatSearchField(searchRecord.getCheckerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CURR_APP_STATUS", formatSearchField(searchRecord.getCurrappstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ADMIN_LAST_CMT", formatSearchField(searchRecord.getAdminlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "BLOCKED_FLAG", formatSearchField(searchRecord.getBlockedflag()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PWDSETDATE", formatSearchField(searchRecord.getPwdsetdate()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "INITIAL_PWD", formatSearchField(searchRecord.getInitialpwd()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "GROUP_CODE", formatSearchField(searchRecord.getGroupcode()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "OTP_GEN", formatSearchField(searchRecord.getOtpgen()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "OTP_GEN_AT", formatSearchField(searchRecord.getOtpgenat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "address", formatSearchField(searchRecord.getAddress()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "city", formatSearchField(searchRecord.getCity()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "dregion", formatSearchField(searchRecord.getDregion()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "dgroup", formatSearchField(searchRecord.getDgroup()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select count(*) from system_user " + WhereCondition;
		Query = updateQuery(Query);
		logger.trace("Search Count Query	" + Query + "\t");
		return loadCount(Query);
	}

	public int loadLISystemuserRecordCountExact(LISystemuserRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "USER_ID", formatSearchField(searchRecord.getUserid()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "USER_PASSWORD", formatSearchField(searchRecord.getUserpassword()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PREFIX", formatSearchField(searchRecord.getPrefix()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "FIRST_NAME", formatSearchField(searchRecord.getFirstname()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MIDDLE_NAME", formatSearchField(searchRecord.getMiddlename()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "LAST_NAME", formatSearchField(searchRecord.getLastname()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "EMAIL", formatSearchField(searchRecord.getEmail()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "MOBILE", formatSearchField(searchRecord.getMobile()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "INSTITUTION_ID", formatSearchField(searchRecord.getInstitutionid()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "COUNTRY", formatSearchField(searchRecord.getCountry()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "LAST_LOGIN_DATE", formatSearchField(searchRecord.getLastlogindate()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "LAST_LOGIN_FDATE", formatSearchField(searchRecord.getLastloginfdate()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "LOGIN_ATTEMPTS", formatSearchField(searchRecord.getLoginattempts()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PREF_LANG", formatSearchField(searchRecord.getPreflang()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PREF_COMM", formatSearchField(searchRecord.getPrefcomm()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "USER_ROLE_ID", formatSearchField(searchRecord.getUserroleid()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "SUP_ID", formatSearchField(searchRecord.getSupid()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SEQ_Q1", formatSearchField(searchRecord.getSeqq1()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SEQ_Q2", formatSearchField(searchRecord.getSeqq2()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SEQ_Q3", formatSearchField(searchRecord.getSeqq3()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SEQ_A1", formatSearchField(searchRecord.getSeqa1()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SEQ_A2", formatSearchField(searchRecord.getSeqa2()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SEQ_A3", formatSearchField(searchRecord.getSeqa3()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "MADE_BY", formatSearchField(searchRecord.getMadeby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MADE_AT", formatSearchField(searchRecord.getMadeat()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "CHECKED_BY", formatSearchField(searchRecord.getCheckedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKED_AT", formatSearchField(searchRecord.getCheckedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MAKER_LAST_CMT", formatSearchField(searchRecord.getMakerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKER_LAST_CMT", formatSearchField(searchRecord.getCheckerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "CURR_APP_STATUS", formatSearchField(searchRecord.getCurrappstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ADMIN_LAST_CMT", formatSearchField(searchRecord.getAdminlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "BLOCKED_FLAG", formatSearchField(searchRecord.getBlockedflag()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PWDSETDATE", formatSearchField(searchRecord.getPwdsetdate()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "INITIAL_PWD", formatSearchField(searchRecord.getInitialpwd()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "GROUP_CODE", formatSearchField(searchRecord.getGroupcode()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "OTP_GEN", formatSearchField(searchRecord.getOtpgen()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "OTP_GEN_AT", formatSearchField(searchRecord.getOtpgenat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "address", formatSearchField(searchRecord.getAddress()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "city", formatSearchField(searchRecord.getCity()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "dregion", formatSearchField(searchRecord.getDregion()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "dgroup", formatSearchField(searchRecord.getDgroup()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select count(*) from system_user " + WhereCondition;
		Query = updateQuery(Query);
		logger.trace("Search Count Query	" + Query + "\t");
		return loadCount(Query);
	}
}
