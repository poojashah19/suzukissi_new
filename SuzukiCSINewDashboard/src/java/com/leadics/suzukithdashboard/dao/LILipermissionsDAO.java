
/*
 * LILipermissionsDAO.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.dao;
import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.to.LILipermissionsRecord;
import java.sql.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LILipermissionsDAO extends LIDAO
{
	static LogUtils logger = new LogUtils(LILipermissionsDAO.class.getName());


	public LILipermissionsRecord[] loadLILipermissionsRecords(String query, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			query = query + MAX_RECORD_LIMIT_APPENDER;
			query = updateQuery(query);
			logger.trace("loadLILipermissionsRecords\t" + closeConnection + "\t" + query);
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			ArrayList recordSet = new ArrayList();
			while(rs.next())
			{
				LILipermissionsRecord record = new LILipermissionsRecord();
				record.setId(rs.getString("Id"));
				record.setCategory(rs.getString("CATEGORY"));
				record.setRolecode(rs.getString("ROLE_CODE"));
				record.setFeaturecode(rs.getString("FEATURE_CODE"));
				record.setPermit(rs.getString("PERMIT"));
				record.setBlock(rs.getString("BLOCK"));
				record.setInstitutionid(rs.getString("INSTITUTION_ID"));
				record.setMadeby(rs.getString("MADE_BY"));
				record.setMadeat(formatDBDateTime(rs.getTimestamp("MADE_AT")));
				record.setCheckedby(rs.getString("CHECKED_BY"));
				record.setCheckedat(formatDBDateTime(rs.getTimestamp("CHECKED_AT")));
				record.setMakerlastcmt(rs.getString("MAKER_LAST_CMT"));
				record.setCheckerlastcmt(rs.getString("CHECKER_LAST_CMT"));
				record.setCurrappstatus(rs.getString("CURR_APP_STATUS"));
				record.setAdminlastcmt(rs.getString("ADMIN_LAST_CMT"));
				record.setRstatus(rs.getString("RSTATUS"));
				record.setCreatedby(rs.getString("CREATED_BY"));
				record.setCreatedat(formatDBDateTime(rs.getTimestamp("CREATED_AT")));
				record.setModifiedat(formatDBDateTime(rs.getTimestamp("MODIFIED_AT")));
				record.setModifiedby(rs.getString("MODIFIED_BY"));
				recordSet.add(record);
			}
			logger.trace("loadLILipermissionsRecords:Records Fetched:" + recordSet.size());
			LILipermissionsRecord[] tempLILipermissionsRecords = new LILipermissionsRecord[recordSet.size()];
			for (int index = 0; index < recordSet.size(); index++)
			{
				tempLILipermissionsRecords[index] = (LILipermissionsRecord)(recordSet.get(index));
			}
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			return tempLILipermissionsRecords;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}


	public LILipermissionsRecord[] loadLILipermissionsRecords(String query)
	throws Exception
	{
		return loadLILipermissionsRecords(query, null, true);
	}


	public LILipermissionsRecord loadFirstLILipermissionsRecord(String query)
	throws Exception
	{
		LILipermissionsRecord[] results = loadLILipermissionsRecords(query);
		if (results == null) return null;
		if(results.length < 1) return null;
		return results[0];
	}

	public LILipermissionsRecord loadLILipermissionsRecord(String id, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			String Query = "SELECT * FROM li_permissions WHERE (Id = ?)";
			Query = updateQuery(Query);
			logger.trace("loadLILipermissionsRecord\t" + closeConnection + "\t" + id + "\t" + Query);
			ps = con.prepareStatement(Query);
			ps.setString(1,id);
			rs = ps.executeQuery();
			if (!rs.next())
			{
				ps.close();
				releaseDatabaseConnection(con, closeConnection);
				return null;
			}
			LILipermissionsRecord record = new LILipermissionsRecord();
			record.setId(rs.getString("Id"));
			record.setCategory(rs.getString("CATEGORY"));
			record.setRolecode(rs.getString("ROLE_CODE"));
			record.setFeaturecode(rs.getString("FEATURE_CODE"));
			record.setPermit(rs.getString("PERMIT"));
			record.setBlock(rs.getString("BLOCK"));
			record.setInstitutionid(rs.getString("INSTITUTION_ID"));
			record.setMadeby(rs.getString("MADE_BY"));
			record.setMadeat(formatDBDateTime(rs.getTimestamp("MADE_AT")));
			record.setCheckedby(rs.getString("CHECKED_BY"));
			record.setCheckedat(formatDBDateTime(rs.getTimestamp("CHECKED_AT")));
			record.setMakerlastcmt(rs.getString("MAKER_LAST_CMT"));
			record.setCheckerlastcmt(rs.getString("CHECKER_LAST_CMT"));
			record.setCurrappstatus(rs.getString("CURR_APP_STATUS"));
			record.setAdminlastcmt(rs.getString("ADMIN_LAST_CMT"));
			record.setRstatus(rs.getString("RSTATUS"));
			record.setCreatedby(rs.getString("CREATED_BY"));
			record.setCreatedat(formatDBDateTime(rs.getTimestamp("CREATED_AT")));
			record.setModifiedat(formatDBDateTime(rs.getTimestamp("MODIFIED_AT")));
			record.setModifiedby(rs.getString("MODIFIED_BY"));
			ps.close();
			logger.trace("loadLILipermissionsRecord\t" + record + "\t");
			releaseDatabaseConnection(con, closeConnection);
			return record;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public LILipermissionsRecord loadLILipermissionsRecord(String id)
	throws Exception
	{
		return loadLILipermissionsRecord(id, null, true);
	}

	public int insertLILipermissionsRecord(LILipermissionsRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			String Query = "INSERT INTO li_permissions ";
			Query += "(";
			Query += "CATEGORY,ROLE_CODE,FEATURE_CODE,PERMIT,BLOCK,INSTITUTION_ID,MADE_BY,MADE_AT,CHECKED_BY,CHECKED_AT,MAKER_LAST_CMT,CHECKER_LAST_CMT,CURR_APP_STATUS,ADMIN_LAST_CMT,RSTATUS,CREATED_BY,CREATED_AT,MODIFIED_AT,MODIFIED_BY";
			Query += ")";
			Query += " VALUES ";
			Query += "(";
			Query += "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?";
			Query += ")";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			record.setModifiedby(StringUtils.noNull(record.getModifiedby(),record.getCreatedby()));
			record.setMadeat(StringUtils.noNull(record.getMadeat(),record.getCreatedat()));
			record.setMadeby(StringUtils.noNull(record.getMadeby(),record.getCreatedby()));
			record.setInstitutionid(StringUtils.noNull(record.getInstitutionid(),PropertyUtil.getProperty("DefaultInstitutionId")));
			Query = updateQuery(Query);
			logger.trace("insertLILipermissionsRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			if (isOracleDatabase()) 
			{
				ps = con.prepareStatement(Query,new String[]{"ID"});
			}
			else
			{
				ps = con.prepareStatement(Query,Statement.RETURN_GENERATED_KEYS);
			}
			setStringValue(ps, 1, record.getCategory());
			setStringValue(ps, 2, record.getRolecode());
			setStringValue(ps, 3, record.getFeaturecode());
			setStringValue(ps, 4, record.getPermit());
			setStringValue(ps, 5, record.getBlock());
			setStringValue(ps, 6, record.getInstitutionid());
			setStringValue(ps, 7, record.getMadeby());
			setDateValue(ps, 8, fd.getSQLDateObject(record.getMadeat(), "yyyyMMddHHmmss"));
			setStringValue(ps, 9, record.getCheckedby());
			setDateValue(ps, 10, fd.getSQLDateObject(record.getCheckedat(), "yyyyMMddHHmmss"));
			setStringValue(ps, 11, record.getMakerlastcmt());
			setStringValue(ps, 12, record.getCheckerlastcmt());
			setStringValue(ps, 13, record.getCurrappstatus());
			setStringValue(ps, 14, record.getAdminlastcmt());
			setStringValue(ps, 15, record.getRstatus());
			setStringValue(ps, 16, record.getCreatedby());
			setDateValue(ps, 17, fd.getCurrentSQLDateObject());
			setDateValue(ps, 18, fd.getCurrentSQLDateObject());
			setStringValue(ps, 19, record.getModifiedby());
			boolean result = ps.execute();
			logger.trace("insertLILipermissionsRecord\t" + result + "\t");
			int resultID = -1;
			rs = ps.getGeneratedKeys();
			if (rs.next()) 
			{
				resultID = rs.getInt(1); 
			}
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			createAuditRecord("li_permissions","INSERT",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), record.getInstitutionid(), record.getActionSource());
			return resultID;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public int insertLILipermissionsRecord(LILipermissionsRecord record)
	throws Exception
	{
		return insertLILipermissionsRecord(record, null, true);
	}

	public boolean updateLILipermissionsRecord(LILipermissionsRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			LILipermissionsRecord currentRecord = loadLILipermissionsRecord(record.getId());
			String currentRecordContent = StringUtils.noNull(currentRecord);

			String Query = "UPDATE li_permissions SET ";
			Query += "CATEGORY = ?, ";
			Query += "ROLE_CODE = ?, ";
			Query += "FEATURE_CODE = ?, ";
			Query += "PERMIT = ?, ";
			Query += "BLOCK = ?, ";
			Query += "INSTITUTION_ID = ?, ";
			Query += "MADE_BY = ?, ";
			Query += "MADE_AT = ?, ";
			Query += "CHECKED_BY = ?, ";
			Query += "CHECKED_AT = ?, ";
			Query += "MAKER_LAST_CMT = ?, ";
			Query += "CHECKER_LAST_CMT = ?, ";
			Query += "CURR_APP_STATUS = ?, ";
			Query += "ADMIN_LAST_CMT = ?, ";
			Query += "RSTATUS = ?, ";
			Query += "CREATED_BY = ?, ";
			Query += "CREATED_AT = ?, ";
			Query += "MODIFIED_AT = ?, ";
			Query += "MODIFIED_BY = ? ";
			Query += "WHERE (Id = ?) ";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			Query = updateQuery(Query);
			logger.trace("updateLILipermissionsRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			ps = con.prepareStatement(Query);
			setStringValue(ps, 1, record.getCategory());
			setStringValue(ps, 2, record.getRolecode());
			setStringValue(ps, 3, record.getFeaturecode());
			setStringValue(ps, 4, record.getPermit());
			setStringValue(ps, 5, record.getBlock());
			setStringValue(ps, 6, record.getInstitutionid());
			setStringValue(ps, 7, record.getMadeby());
			setDateValue(ps, 8, fd.getSQLDateObject(record.getMadeat(), "yyyyMMddHHmmss"));
			setStringValue(ps, 9, record.getCheckedby());
			setDateValue(ps, 10, fd.getSQLDateObject(record.getCheckedat(), "yyyyMMddHHmmss"));
			setStringValue(ps, 11, record.getMakerlastcmt());
			setStringValue(ps, 12, record.getCheckerlastcmt());
			setStringValue(ps, 13, record.getCurrappstatus());
			setStringValue(ps, 14, record.getAdminlastcmt());
			setStringValue(ps, 15, record.getRstatus());
			setStringValue(ps, 16, record.getCreatedby());
			setDateValue(ps, 17, fd.getSQLDateObject(record.getCreatedat(), "yyyyMMddHHmmss"));
			setDateValue(ps, 18, fd.getCurrentSQLDateObject());
			setStringValue(ps, 19, record.getModifiedby());
			ps.setString(20, noNull(record.getId()));
			boolean result = ps.execute();
			logger.trace("updateLILipermissionsRecord\t" + result + "\t");
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			createAuditRecord("li_permissions","UPDATE",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), record.getInstitutionid(), currentRecordContent, record.getActionSource());
			return result;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public boolean updateLILipermissionsRecord(LILipermissionsRecord record)
	throws Exception
	{
		return updateLILipermissionsRecord(record, null, true);
	}

	public boolean deleteLILipermissionsRecord(LILipermissionsRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			String Query = "DELETE FROM li_permissions WHERE (Id = ?)";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			Query = updateQuery(Query);
			logger.trace("deleteLILipermissionsRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			ps = con.prepareStatement(Query);
			ps.setString(1, noNull(record.getId()));
			boolean result = ps.execute();
			logger.trace("deleteLILipermissionsRecord\t" + result + "\t");
			ps.close();
			createAuditRecord("li_permissions","DELETE",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), record.getInstitutionid(), record.getActionSource());
			releaseDatabaseConnection(con, closeConnection);
			return result;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public boolean deleteLILipermissionsRecord(LILipermissionsRecord record)
	throws Exception
	{
		return deleteLILipermissionsRecord(record, null, true);
	}

	public LILipermissionsRecord[] searchLILipermissionsRecords(LILipermissionsRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CATEGORY", formatSearchField(searchRecord.getCategory()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ROLE_CODE", formatSearchField(searchRecord.getRolecode()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "FEATURE_CODE", formatSearchField(searchRecord.getFeaturecode()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PERMIT", formatSearchField(searchRecord.getPermit()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "BLOCK", formatSearchField(searchRecord.getBlock()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "INSTITUTION_ID", formatSearchField(searchRecord.getInstitutionid()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MADE_BY", formatSearchField(searchRecord.getMadeby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MADE_AT", formatSearchField(searchRecord.getMadeat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKED_BY", formatSearchField(searchRecord.getCheckedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CHECKED_AT", formatSearchField(searchRecord.getCheckedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MAKER_LAST_CMT", formatSearchField(searchRecord.getMakerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CHECKER_LAST_CMT", formatSearchField(searchRecord.getCheckerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CURR_APP_STATUS", formatSearchField(searchRecord.getCurrappstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ADMIN_LAST_CMT", formatSearchField(searchRecord.getAdminlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from li_permissions " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM li_permissions ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM li_permissions $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLILipermissionsRecords(Query);
	}

	public LILipermissionsRecord[] searchLILipermissionsRecordsExact(LILipermissionsRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CATEGORY", formatSearchField(searchRecord.getCategory()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ROLE_CODE", formatSearchField(searchRecord.getRolecode()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "FEATURE_CODE", formatSearchField(searchRecord.getFeaturecode()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PERMIT", formatSearchField(searchRecord.getPermit()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "BLOCK", formatSearchField(searchRecord.getBlock()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "INSTITUTION_ID", formatSearchField(searchRecord.getInstitutionid()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MADE_BY", formatSearchField(searchRecord.getMadeby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MADE_AT", formatSearchField(searchRecord.getMadeat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKED_BY", formatSearchField(searchRecord.getCheckedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKED_AT", formatSearchField(searchRecord.getCheckedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MAKER_LAST_CMT", formatSearchField(searchRecord.getMakerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKER_LAST_CMT", formatSearchField(searchRecord.getCheckerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CURR_APP_STATUS", formatSearchField(searchRecord.getCurrappstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ADMIN_LAST_CMT", formatSearchField(searchRecord.getAdminlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from li_permissions " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM li_permissions ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM li_permissions $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLILipermissionsRecords(Query);
	}

	public LILipermissionsRecord[] searchLILipermissionsRecordsExactUpper(LILipermissionsRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CATEGORY", formatSearchField(searchRecord.getCategory()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ROLE_CODE", formatSearchField(searchRecord.getRolecode()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "FEATURE_CODE", formatSearchField(searchRecord.getFeaturecode()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "PERMIT", formatSearchField(searchRecord.getPermit()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "BLOCK", formatSearchField(searchRecord.getBlock()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "INSTITUTION_ID", formatSearchField(searchRecord.getInstitutionid()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MADE_BY", formatSearchField(searchRecord.getMadeby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MADE_AT", formatSearchField(searchRecord.getMadeat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CHECKED_BY", formatSearchField(searchRecord.getCheckedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CHECKED_AT", formatSearchField(searchRecord.getCheckedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MAKER_LAST_CMT", formatSearchField(searchRecord.getMakerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CHECKER_LAST_CMT", formatSearchField(searchRecord.getCheckerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CURR_APP_STATUS", formatSearchField(searchRecord.getCurrappstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ADMIN_LAST_CMT", formatSearchField(searchRecord.getAdminlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from li_permissions " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM li_permissions ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM li_permissions $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLILipermissionsRecords(Query);
	}

	public int loadLILipermissionsRecordCount(LILipermissionsRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CATEGORY", formatSearchField(searchRecord.getCategory()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ROLE_CODE", formatSearchField(searchRecord.getRolecode()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "FEATURE_CODE", formatSearchField(searchRecord.getFeaturecode()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "PERMIT", formatSearchField(searchRecord.getPermit()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "BLOCK", formatSearchField(searchRecord.getBlock()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "INSTITUTION_ID", formatSearchField(searchRecord.getInstitutionid()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MADE_BY", formatSearchField(searchRecord.getMadeby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MADE_AT", formatSearchField(searchRecord.getMadeat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKED_BY", formatSearchField(searchRecord.getCheckedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CHECKED_AT", formatSearchField(searchRecord.getCheckedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MAKER_LAST_CMT", formatSearchField(searchRecord.getMakerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CHECKER_LAST_CMT", formatSearchField(searchRecord.getCheckerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CURR_APP_STATUS", formatSearchField(searchRecord.getCurrappstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ADMIN_LAST_CMT", formatSearchField(searchRecord.getAdminlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select count(*) from li_permissions " + WhereCondition;
		Query = updateQuery(Query);
		logger.trace("Search Count Query	" + Query + "\t");
		return loadCount(Query);
	}

	public int loadLILipermissionsRecordCountExact(LILipermissionsRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CATEGORY", formatSearchField(searchRecord.getCategory()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "ROLE_CODE", formatSearchField(searchRecord.getRolecode()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "FEATURE_CODE", formatSearchField(searchRecord.getFeaturecode()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "PERMIT", formatSearchField(searchRecord.getPermit()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "BLOCK", formatSearchField(searchRecord.getBlock()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "INSTITUTION_ID", formatSearchField(searchRecord.getInstitutionid()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "MADE_BY", formatSearchField(searchRecord.getMadeby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MADE_AT", formatSearchField(searchRecord.getMadeat()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "CHECKED_BY", formatSearchField(searchRecord.getCheckedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKED_AT", formatSearchField(searchRecord.getCheckedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MAKER_LAST_CMT", formatSearchField(searchRecord.getMakerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CHECKER_LAST_CMT", formatSearchField(searchRecord.getCheckerlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "CURR_APP_STATUS", formatSearchField(searchRecord.getCurrappstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ADMIN_LAST_CMT", formatSearchField(searchRecord.getAdminlastcmt()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select count(*) from li_permissions " + WhereCondition;
		Query = updateQuery(Query);
		logger.trace("Search Count Query	" + Query + "\t");
		return loadCount(Query);
	}
}
