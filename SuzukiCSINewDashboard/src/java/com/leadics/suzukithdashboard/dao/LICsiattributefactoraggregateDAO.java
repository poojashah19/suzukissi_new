
/*
 * LICsiattributefactoraggregateDAO.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.dao;
import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.to.LICsiattributefactoraggregateRecord;
import java.sql.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LICsiattributefactoraggregateDAO extends LIDAO
{
	static LogUtils logger = new LogUtils(LICsiattributefactoraggregateDAO.class.getName());


	public LICsiattributefactoraggregateRecord[] loadLICsiattributefactoraggregateRecords(String query, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			query = query + MAX_RECORD_LIMIT_APPENDER;
			query = updateQuery(query);
			logger.trace("loadLICsiattributefactoraggregateRecords\t" + closeConnection + "\t" + query);
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			ArrayList recordSet = new ArrayList();
			while(rs.next())
			{
				LICsiattributefactoraggregateRecord record = new LICsiattributefactoraggregateRecord();
				record.setId(rs.getString("Id"));
				record.setAttribute(rs.getString("attribute"));
				record.setAttributename(rs.getString("AttributeName"));
				record.setAttributeindex(rs.getString("AttributeIndex"));
				record.setFactor(rs.getString("factor"));
				record.setRegion(rs.getString("region"));
				record.setState(rs.getString("state"));
				record.setCity(rs.getString("city"));
				record.setDealer(rs.getString("dealer"));
				record.setModel(rs.getString("model"));
				record.setBiweekly(rs.getString("Bi_Weekly"));
				record.setMonth(rs.getString("MONTH"));
				record.setQuarter(rs.getString("QUARTER"));
				record.setDealerattributesum(rs.getString("dealer_attribute_sum"));
				record.setDealerattributecount(rs.getString("dealer_attribute_count"));
				record.setTotalcount(rs.getString("totalcount"));
				record.setDealerdissatisfiedcount(rs.getString("dealer_Dissatisfied_count"));
				record.setRstatus(rs.getString("RSTATUS"));
				record.setLastacc(formatDBDateTime(rs.getTimestamp("LASTACC")));
				record.setCreatedby(rs.getString("CREATED_BY"));
				record.setCreatedat(formatDBDateTime(rs.getTimestamp("CREATED_AT")));
				record.setModifiedat(formatDBDateTime(rs.getTimestamp("MODIFIED_AT")));
				record.setModifiedby(rs.getString("MODIFIED_BY"));
				record.setClosetype(rs.getString("CLOSE_TYPE"));
				record.setClosedat(rs.getString("CLOSED_AT"));
				recordSet.add(record);
			}
			logger.trace("loadLICsiattributefactoraggregateRecords:Records Fetched:" + recordSet.size());
			LICsiattributefactoraggregateRecord[] tempLICsiattributefactoraggregateRecords = new LICsiattributefactoraggregateRecord[recordSet.size()];
			for (int index = 0; index < recordSet.size(); index++)
			{
				tempLICsiattributefactoraggregateRecords[index] = (LICsiattributefactoraggregateRecord)(recordSet.get(index));
			}
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			return tempLICsiattributefactoraggregateRecords;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}


	public LICsiattributefactoraggregateRecord[] loadLICsiattributefactoraggregateRecords(String query)
	throws Exception
	{
		return loadLICsiattributefactoraggregateRecords(query, null, true);
	}


	public LICsiattributefactoraggregateRecord loadFirstLICsiattributefactoraggregateRecord(String query)
	throws Exception
	{
		LICsiattributefactoraggregateRecord[] results = loadLICsiattributefactoraggregateRecords(query);
		if (results == null) return null;
		if(results.length < 1) return null;
		return results[0];
	}

	public LICsiattributefactoraggregateRecord loadLICsiattributefactoraggregateRecord(String id, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			String Query = "SELECT * FROM attribute_factor_aggregate WHERE (Id = ?)";
			Query = updateQuery(Query);
			logger.trace("loadLICsiattributefactoraggregateRecord\t" + closeConnection + "\t" + id + "\t" + Query);
			ps = con.prepareStatement(Query);
			ps.setString(1,id);
			rs = ps.executeQuery();
			if (!rs.next())
			{
				ps.close();
				releaseDatabaseConnection(con, closeConnection);
				return null;
			}
			LICsiattributefactoraggregateRecord record = new LICsiattributefactoraggregateRecord();
			record.setId(rs.getString("Id"));
			record.setAttribute(rs.getString("attribute"));
			record.setAttributename(rs.getString("AttributeName"));
			record.setAttributeindex(rs.getString("AttributeIndex"));
			record.setFactor(rs.getString("factor"));
			record.setRegion(rs.getString("region"));
			record.setState(rs.getString("state"));
			record.setCity(rs.getString("city"));
			record.setDealer(rs.getString("dealer"));
			record.setModel(rs.getString("model"));
			record.setBiweekly(rs.getString("Bi_Weekly"));
			record.setMonth(rs.getString("MONTH"));
			record.setQuarter(rs.getString("QUARTER"));
			record.setDealerattributesum(rs.getString("dealer_attribute_sum"));
			record.setDealerattributecount(rs.getString("dealer_attribute_count"));
			record.setTotalcount(rs.getString("totalcount"));
			record.setDealerdissatisfiedcount(rs.getString("dealer_Dissatisfied_count"));
			record.setRstatus(rs.getString("RSTATUS"));
			record.setLastacc(formatDBDateTime(rs.getTimestamp("LASTACC")));
			record.setCreatedby(rs.getString("CREATED_BY"));
			record.setCreatedat(formatDBDateTime(rs.getTimestamp("CREATED_AT")));
			record.setModifiedat(formatDBDateTime(rs.getTimestamp("MODIFIED_AT")));
			record.setModifiedby(rs.getString("MODIFIED_BY"));
			record.setClosetype(rs.getString("CLOSE_TYPE"));
			record.setClosedat(rs.getString("CLOSED_AT"));
			ps.close();
			logger.trace("loadLICsiattributefactoraggregateRecord\t" + record + "\t");
			releaseDatabaseConnection(con, closeConnection);
			return record;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public LICsiattributefactoraggregateRecord loadLICsiattributefactoraggregateRecord(String id)
	throws Exception
	{
		return loadLICsiattributefactoraggregateRecord(id, null, true);
	}

	public int insertLICsiattributefactoraggregateRecord(LICsiattributefactoraggregateRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			String Query = "INSERT INTO attribute_factor_aggregate ";
			Query += "(";
			Query += "attribute,AttributeName,AttributeIndex,factor,region,state,city,dealer,model,Bi_Weekly,MONTH,QUARTER,dealer_attribute_sum,dealer_attribute_count,totalcount,dealer_Dissatisfied_count,RSTATUS,LASTACC,CREATED_BY,CREATED_AT,MODIFIED_AT,MODIFIED_BY,CLOSE_TYPE,CLOSED_AT";
			Query += ")";
			Query += " VALUES ";
			Query += "(";
			Query += "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?";
			Query += ")";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			record.setModifiedby(StringUtils.noNull(record.getModifiedby(),record.getCreatedby()));
			Query = updateQuery(Query);
			logger.trace("insertLICsiattributefactoraggregateRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			if (isOracleDatabase()) 
			{
				ps = con.prepareStatement(Query,new String[]{"ID"});
			}
			else
			{
				ps = con.prepareStatement(Query,Statement.RETURN_GENERATED_KEYS);
			}
			setStringValue(ps, 1, record.getAttribute());
			setStringValue(ps, 2, record.getAttributename());
			setStringValue(ps, 3, record.getAttributeindex());
			setStringValue(ps, 4, record.getFactor());
			setStringValue(ps, 5, record.getRegion());
			setStringValue(ps, 6, record.getState());
			setStringValue(ps, 7, record.getCity());
			setStringValue(ps, 8, record.getDealer());
			setStringValue(ps, 9, record.getModel());
			setStringValue(ps, 10, record.getBiweekly());
			setStringValue(ps, 11, record.getMonth());
			setStringValue(ps, 12, record.getQuarter());
			setStringValue(ps, 13, record.getDealerattributesum());
			setStringValue(ps, 14, record.getDealerattributecount());
			setStringValue(ps, 15, record.getTotalcount());
			setStringValue(ps, 16, record.getDealerdissatisfiedcount());
			setStringValue(ps, 17, record.getRstatus());
			setDateValue(ps, 18, fd.getSQLDateObject(record.getLastacc(), "yyyyMMddHHmmss"));
			setStringValue(ps, 19, record.getCreatedby());
			setDateValue(ps, 20, fd.getCurrentSQLDateObject());
			setDateValue(ps, 21, fd.getCurrentSQLDateObject());
			setStringValue(ps, 22, record.getModifiedby());
			setStringValue(ps, 23, record.getClosetype());
			setStringValue(ps, 24, record.getClosedat());
			boolean result = ps.execute();
			logger.trace("insertLICsiattributefactoraggregateRecord\t" + result + "\t");
			int resultID = -1;
			rs = ps.getGeneratedKeys();
			if (rs.next()) 
			{
				resultID = rs.getInt(1); 
			}
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			createAuditRecord("attribute_factor_aggregate","INSERT",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, record.getActionSource());
			return resultID;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public int insertLICsiattributefactoraggregateRecord(LICsiattributefactoraggregateRecord record)
	throws Exception
	{
		return insertLICsiattributefactoraggregateRecord(record, null, true);
	}

	public boolean updateLICsiattributefactoraggregateRecord(LICsiattributefactoraggregateRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			LICsiattributefactoraggregateRecord currentRecord = loadLICsiattributefactoraggregateRecord(record.getId());
			String currentRecordContent = StringUtils.noNull(currentRecord);

			String Query = "UPDATE attribute_factor_aggregate SET ";
			Query += "attribute = ?, ";
			Query += "AttributeName = ?, ";
			Query += "AttributeIndex = ?, ";
			Query += "factor = ?, ";
			Query += "region = ?, ";
			Query += "state = ?, ";
			Query += "city = ?, ";
			Query += "dealer = ?, ";
			Query += "model = ?, ";
			Query += "Bi_Weekly = ?, ";
			Query += "MONTH = ?, ";
			Query += "QUARTER = ?, ";
			Query += "dealer_attribute_sum = ?, ";
			Query += "dealer_attribute_count = ?, ";
			Query += "totalcount = ?, ";
			Query += "dealer_Dissatisfied_count = ?, ";
			Query += "RSTATUS = ?, ";
			Query += "LASTACC = ?, ";
			Query += "CREATED_BY = ?, ";
			Query += "CREATED_AT = ?, ";
			Query += "MODIFIED_AT = ?, ";
			Query += "MODIFIED_BY = ?, ";
			Query += "CLOSE_TYPE = ?, ";
			Query += "CLOSED_AT = ? ";
			Query += "WHERE (Id = ?) ";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			Query = updateQuery(Query);
			logger.trace("updateLICsiattributefactoraggregateRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			ps = con.prepareStatement(Query);
			setStringValue(ps, 1, record.getAttribute());
			setStringValue(ps, 2, record.getAttributename());
			setStringValue(ps, 3, record.getAttributeindex());
			setStringValue(ps, 4, record.getFactor());
			setStringValue(ps, 5, record.getRegion());
			setStringValue(ps, 6, record.getState());
			setStringValue(ps, 7, record.getCity());
			setStringValue(ps, 8, record.getDealer());
			setStringValue(ps, 9, record.getModel());
			setStringValue(ps, 10, record.getBiweekly());
			setStringValue(ps, 11, record.getMonth());
			setStringValue(ps, 12, record.getQuarter());
			setStringValue(ps, 13, record.getDealerattributesum());
			setStringValue(ps, 14, record.getDealerattributecount());
			setStringValue(ps, 15, record.getTotalcount());
			setStringValue(ps, 16, record.getDealerdissatisfiedcount());
			setStringValue(ps, 17, record.getRstatus());
			setDateValue(ps, 18, fd.getSQLDateObject(record.getLastacc(), "yyyyMMddHHmmss"));
			setStringValue(ps, 19, record.getCreatedby());
			setDateValue(ps, 20, fd.getSQLDateObject(record.getCreatedat(), "yyyyMMddHHmmss"));
			setDateValue(ps, 21, fd.getCurrentSQLDateObject());
			setStringValue(ps, 22, record.getModifiedby());
			setStringValue(ps, 23, record.getClosetype());
			setStringValue(ps, 24, record.getClosedat());
			ps.setString(25, noNull(record.getId()));
			boolean result = ps.execute();
			logger.trace("updateLICsiattributefactoraggregateRecord\t" + result + "\t");
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			createAuditRecord("attribute_factor_aggregate","UPDATE",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, currentRecordContent, record.getActionSource());
			return result;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public boolean updateLICsiattributefactoraggregateRecord(LICsiattributefactoraggregateRecord record)
	throws Exception
	{
		return updateLICsiattributefactoraggregateRecord(record, null, true);
	}

	public boolean deleteLICsiattributefactoraggregateRecord(LICsiattributefactoraggregateRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			String Query = "DELETE FROM attribute_factor_aggregate WHERE (Id = ?)";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			Query = updateQuery(Query);
			logger.trace("deleteLICsiattributefactoraggregateRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			ps = con.prepareStatement(Query);
			ps.setString(1, noNull(record.getId()));
			boolean result = ps.execute();
			logger.trace("deleteLICsiattributefactoraggregateRecord\t" + result + "\t");
			ps.close();
			createAuditRecord("attribute_factor_aggregate","DELETE",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, record.getActionSource());
			releaseDatabaseConnection(con, closeConnection);
			return result;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public boolean deleteLICsiattributefactoraggregateRecord(LICsiattributefactoraggregateRecord record)
	throws Exception
	{
		return deleteLICsiattributefactoraggregateRecord(record, null, true);
	}

	public LICsiattributefactoraggregateRecord[] searchLICsiattributefactoraggregateRecords(LICsiattributefactoraggregateRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "attribute", formatSearchField(searchRecord.getAttribute()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "AttributeName", formatSearchField(searchRecord.getAttributename()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "AttributeIndex", formatSearchField(searchRecord.getAttributeindex()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "factor", formatSearchField(searchRecord.getFactor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "region", formatSearchField(searchRecord.getRegion()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "state", formatSearchField(searchRecord.getState()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "city", formatSearchField(searchRecord.getCity()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "dealer", formatSearchField(searchRecord.getDealer()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "model", formatSearchField(searchRecord.getModel()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "Bi_Weekly", formatSearchField(searchRecord.getBiweekly()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MONTH", formatSearchField(searchRecord.getMonth()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "QUARTER", formatSearchField(searchRecord.getQuarter()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "dealer_attribute_sum", formatSearchField(searchRecord.getDealerattributesum()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "dealer_attribute_count", formatSearchField(searchRecord.getDealerattributecount()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "totalcount", formatSearchField(searchRecord.getTotalcount()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "dealer_Dissatisfied_count", formatSearchField(searchRecord.getDealerdissatisfiedcount()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "LASTACC", formatSearchField(searchRecord.getLastacc()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CLOSE_TYPE", formatSearchField(searchRecord.getClosetype()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CLOSED_AT", formatSearchField(searchRecord.getClosedat()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from attribute_factor_aggregate " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM attribute_factor_aggregate ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM attribute_factor_aggregate $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLICsiattributefactoraggregateRecords(Query);
	}

	public LICsiattributefactoraggregateRecord[] searchLICsiattributefactoraggregateRecordsExact(LICsiattributefactoraggregateRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "attribute", formatSearchField(searchRecord.getAttribute()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "AttributeName", formatSearchField(searchRecord.getAttributename()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "AttributeIndex", formatSearchField(searchRecord.getAttributeindex()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "factor", formatSearchField(searchRecord.getFactor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "region", formatSearchField(searchRecord.getRegion()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "state", formatSearchField(searchRecord.getState()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "city", formatSearchField(searchRecord.getCity()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "dealer", formatSearchField(searchRecord.getDealer()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "model", formatSearchField(searchRecord.getModel()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Bi_Weekly", formatSearchField(searchRecord.getBiweekly()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MONTH", formatSearchField(searchRecord.getMonth()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "QUARTER", formatSearchField(searchRecord.getQuarter()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "dealer_attribute_sum", formatSearchField(searchRecord.getDealerattributesum()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "dealer_attribute_count", formatSearchField(searchRecord.getDealerattributecount()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "totalcount", formatSearchField(searchRecord.getTotalcount()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "dealer_Dissatisfied_count", formatSearchField(searchRecord.getDealerdissatisfiedcount()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "LASTACC", formatSearchField(searchRecord.getLastacc()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CLOSE_TYPE", formatSearchField(searchRecord.getClosetype()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CLOSED_AT", formatSearchField(searchRecord.getClosedat()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from attribute_factor_aggregate " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM attribute_factor_aggregate ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM attribute_factor_aggregate $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLICsiattributefactoraggregateRecords(Query);
	}

	public LICsiattributefactoraggregateRecord[] searchLICsiattributefactoraggregateRecordsExactUpper(LICsiattributefactoraggregateRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "attribute", formatSearchField(searchRecord.getAttribute()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "AttributeName", formatSearchField(searchRecord.getAttributename()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "AttributeIndex", formatSearchField(searchRecord.getAttributeindex()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "factor", formatSearchField(searchRecord.getFactor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "region", formatSearchField(searchRecord.getRegion()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "state", formatSearchField(searchRecord.getState()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "city", formatSearchField(searchRecord.getCity()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "dealer", formatSearchField(searchRecord.getDealer()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "model", formatSearchField(searchRecord.getModel()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "Bi_Weekly", formatSearchField(searchRecord.getBiweekly()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MONTH", formatSearchField(searchRecord.getMonth()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "QUARTER", formatSearchField(searchRecord.getQuarter()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "dealer_attribute_sum", formatSearchField(searchRecord.getDealerattributesum()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "dealer_attribute_count", formatSearchField(searchRecord.getDealerattributecount()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "totalcount", formatSearchField(searchRecord.getTotalcount()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "dealer_Dissatisfied_count", formatSearchField(searchRecord.getDealerdissatisfiedcount()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "LASTACC", formatSearchField(searchRecord.getLastacc()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CLOSE_TYPE", formatSearchField(searchRecord.getClosetype()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CLOSED_AT", formatSearchField(searchRecord.getClosedat()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from attribute_factor_aggregate " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM attribute_factor_aggregate ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM attribute_factor_aggregate $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLICsiattributefactoraggregateRecords(Query);
	}

	public int loadLICsiattributefactoraggregateRecordCount(LICsiattributefactoraggregateRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "attribute", formatSearchField(searchRecord.getAttribute()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "AttributeName", formatSearchField(searchRecord.getAttributename()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "AttributeIndex", formatSearchField(searchRecord.getAttributeindex()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "factor", formatSearchField(searchRecord.getFactor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "region", formatSearchField(searchRecord.getRegion()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "state", formatSearchField(searchRecord.getState()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "city", formatSearchField(searchRecord.getCity()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "dealer", formatSearchField(searchRecord.getDealer()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "model", formatSearchField(searchRecord.getModel()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "Bi_Weekly", formatSearchField(searchRecord.getBiweekly()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MONTH", formatSearchField(searchRecord.getMonth()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "QUARTER", formatSearchField(searchRecord.getQuarter()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "dealer_attribute_sum", formatSearchField(searchRecord.getDealerattributesum()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "dealer_attribute_count", formatSearchField(searchRecord.getDealerattributecount()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "totalcount", formatSearchField(searchRecord.getTotalcount()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "dealer_Dissatisfied_count", formatSearchField(searchRecord.getDealerdissatisfiedcount()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "LASTACC", formatSearchField(searchRecord.getLastacc()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CLOSE_TYPE", formatSearchField(searchRecord.getClosetype()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CLOSED_AT", formatSearchField(searchRecord.getClosedat()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select count(*) from attribute_factor_aggregate " + WhereCondition;
		Query = updateQuery(Query);
		logger.trace("Search Count Query	" + Query + "\t");
		return loadCount(Query);
	}

	public int loadLICsiattributefactoraggregateRecordCountExact(LICsiattributefactoraggregateRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "attribute", formatSearchField(searchRecord.getAttribute()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "AttributeName", formatSearchField(searchRecord.getAttributename()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "AttributeIndex", formatSearchField(searchRecord.getAttributeindex()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "factor", formatSearchField(searchRecord.getFactor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "region", formatSearchField(searchRecord.getRegion()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "state", formatSearchField(searchRecord.getState()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "city", formatSearchField(searchRecord.getCity()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "dealer", formatSearchField(searchRecord.getDealer()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "model", formatSearchField(searchRecord.getModel()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Bi_Weekly", formatSearchField(searchRecord.getBiweekly()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MONTH", formatSearchField(searchRecord.getMonth()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "QUARTER", formatSearchField(searchRecord.getQuarter()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "dealer_attribute_sum", formatSearchField(searchRecord.getDealerattributesum()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "dealer_attribute_count", formatSearchField(searchRecord.getDealerattributecount()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "totalcount", formatSearchField(searchRecord.getTotalcount()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "dealer_Dissatisfied_count", formatSearchField(searchRecord.getDealerdissatisfiedcount()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "LASTACC", formatSearchField(searchRecord.getLastacc()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CLOSE_TYPE", formatSearchField(searchRecord.getClosetype()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CLOSED_AT", formatSearchField(searchRecord.getClosedat()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select count(*) from attribute_factor_aggregate " + WhereCondition;
		Query = updateQuery(Query);
		logger.trace("Search Count Query	" + Query + "\t");
		return loadCount(Query);
	}
}
