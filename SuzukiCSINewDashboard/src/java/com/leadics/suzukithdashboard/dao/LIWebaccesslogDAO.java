
/*
 * LIWebaccesslogDAO.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.dao;
import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.to.LIWebaccesslogRecord;
import java.sql.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LIWebaccesslogDAO extends LIDAO
{
	static LogUtils logger = new LogUtils(LIWebaccesslogDAO.class.getName());


	public LIWebaccesslogRecord[] loadLIWebaccesslogRecords(String query, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			query = query + MAX_RECORD_LIMIT_APPENDER;
			query = updateQuery(query);
			logger.trace("loadLIWebaccesslogRecords\t" + closeConnection + "\t" + query);
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			ArrayList recordSet = new ArrayList();
			while(rs.next())
			{
				LIWebaccesslogRecord record = new LIWebaccesslogRecord();
				record.setId(rs.getString("Id"));
				record.setUserid(rs.getString("USER_ID"));
				record.setSource(rs.getString("SOURCE"));
				record.setService(rs.getString("SERVICE"));
				record.setResult(rs.getString("RESULT"));
				record.setSessionid(rs.getString("SESSIONID"));
				record.setCreatedby(rs.getString("CREATED_BY"));
				record.setCreatedat(formatDBDateTime(rs.getTimestamp("CREATED_AT")));
				record.setModifiedat(formatDBDateTime(rs.getTimestamp("MODIFIED_AT")));
				record.setModifiedby(rs.getString("MODIFIED_BY"));
				recordSet.add(record);
			}
			logger.trace("loadLIWebaccesslogRecords:Records Fetched:" + recordSet.size());
			LIWebaccesslogRecord[] tempLIWebaccesslogRecords = new LIWebaccesslogRecord[recordSet.size()];
			for (int index = 0; index < recordSet.size(); index++)
			{
				tempLIWebaccesslogRecords[index] = (LIWebaccesslogRecord)(recordSet.get(index));
			}
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			return tempLIWebaccesslogRecords;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}


	public LIWebaccesslogRecord[] loadLIWebaccesslogRecords(String query)
	throws Exception
	{
		return loadLIWebaccesslogRecords(query, null, true);
	}


	public LIWebaccesslogRecord loadFirstLIWebaccesslogRecord(String query)
	throws Exception
	{
		LIWebaccesslogRecord[] results = loadLIWebaccesslogRecords(query);
		if (results == null) return null;
		if(results.length < 1) return null;
		return results[0];
	}

	public LIWebaccesslogRecord loadLIWebaccesslogRecord(String id, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			String Query = "SELECT * FROM web_access_log WHERE (Id = ?)";
			Query = updateQuery(Query);
			logger.trace("loadLIWebaccesslogRecord\t" + closeConnection + "\t" + id + "\t" + Query);
			ps = con.prepareStatement(Query);
			ps.setString(1,id);
			rs = ps.executeQuery();
			if (!rs.next())
			{
				ps.close();
				releaseDatabaseConnection(con, closeConnection);
				return null;
			}
			LIWebaccesslogRecord record = new LIWebaccesslogRecord();
			record.setId(rs.getString("Id"));
			record.setUserid(rs.getString("USER_ID"));
			record.setSource(rs.getString("SOURCE"));
			record.setService(rs.getString("SERVICE"));
			record.setResult(rs.getString("RESULT"));
			record.setSessionid(rs.getString("SESSIONID"));
			record.setCreatedby(rs.getString("CREATED_BY"));
			record.setCreatedat(formatDBDateTime(rs.getTimestamp("CREATED_AT")));
			record.setModifiedat(formatDBDateTime(rs.getTimestamp("MODIFIED_AT")));
			record.setModifiedby(rs.getString("MODIFIED_BY"));
			ps.close();
			logger.trace("loadLIWebaccesslogRecord\t" + record + "\t");
			releaseDatabaseConnection(con, closeConnection);
			return record;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public LIWebaccesslogRecord loadLIWebaccesslogRecord(String id)
	throws Exception
	{
		return loadLIWebaccesslogRecord(id, null, true);
	}

	public int insertLIWebaccesslogRecord(LIWebaccesslogRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			String Query = "INSERT INTO web_access_log ";
			Query += "(";
			Query += "USER_ID,SOURCE,SERVICE,RESULT,SESSIONID,CREATED_BY,CREATED_AT,MODIFIED_AT,MODIFIED_BY";
			Query += ")";
			Query += " VALUES ";
			Query += "(";
			Query += "?,?,?,?,?,?,?,?,?";
			Query += ")";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			record.setModifiedby(StringUtils.noNull(record.getModifiedby(),record.getCreatedby()));
			Query = updateQuery(Query);
			logger.trace("insertLIWebaccesslogRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			if (isOracleDatabase()) 
			{
				ps = con.prepareStatement(Query,new String[]{"ID"});
			}
			else
			{
				ps = con.prepareStatement(Query,Statement.RETURN_GENERATED_KEYS);
			}
			setStringValue(ps, 1, record.getUserid());
			setStringValue(ps, 2, record.getSource());
			setStringValue(ps, 3, record.getService());
			setStringValue(ps, 4, record.getResult());
			setStringValue(ps, 5, record.getSessionid());
			setStringValue(ps, 6, record.getCreatedby());
			setDateValue(ps, 7, fd.getCurrentSQLDateObject());
			setDateValue(ps, 8, fd.getCurrentSQLDateObject());
			setStringValue(ps, 9, record.getModifiedby());
			boolean result = ps.execute();
			logger.trace("insertLIWebaccesslogRecord\t" + result + "\t");
			int resultID = -1;
			rs = ps.getGeneratedKeys();
			if (rs.next()) 
			{
				resultID = rs.getInt(1); 
			}
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			createAuditRecord("web_access_log","INSERT",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, record.getActionSource());
			return resultID;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public int insertLIWebaccesslogRecord(LIWebaccesslogRecord record)
	throws Exception
	{
		return insertLIWebaccesslogRecord(record, null, true);
	}

	public boolean updateLIWebaccesslogRecord(LIWebaccesslogRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			LIWebaccesslogRecord currentRecord = loadLIWebaccesslogRecord(record.getId());
			String currentRecordContent = StringUtils.noNull(currentRecord);

			String Query = "UPDATE web_access_log SET ";
			Query += "USER_ID = ?, ";
			Query += "SOURCE = ?, ";
			Query += "SERVICE = ?, ";
			Query += "RESULT = ?, ";
			Query += "SESSIONID = ?, ";
			Query += "CREATED_BY = ?, ";
			Query += "CREATED_AT = ?, ";
			Query += "MODIFIED_AT = ?, ";
			Query += "MODIFIED_BY = ? ";
			Query += "WHERE (Id = ?) ";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			Query = updateQuery(Query);
			logger.trace("updateLIWebaccesslogRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			ps = con.prepareStatement(Query);
			setStringValue(ps, 1, record.getUserid());
			setStringValue(ps, 2, record.getSource());
			setStringValue(ps, 3, record.getService());
			setStringValue(ps, 4, record.getResult());
			setStringValue(ps, 5, record.getSessionid());
			setStringValue(ps, 6, record.getCreatedby());
			setDateValue(ps, 7, fd.getSQLDateObject(record.getCreatedat(), "yyyyMMddHHmmss"));
			setDateValue(ps, 8, fd.getCurrentSQLDateObject());
			setStringValue(ps, 9, record.getModifiedby());
			ps.setString(10, noNull(record.getId()));
			boolean result = ps.execute();
			logger.trace("updateLIWebaccesslogRecord\t" + result + "\t");
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			createAuditRecord("web_access_log","UPDATE",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, currentRecordContent, record.getActionSource());
			return result;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public boolean updateLIWebaccesslogRecord(LIWebaccesslogRecord record)
	throws Exception
	{
		return updateLIWebaccesslogRecord(record, null, true);
	}

	public boolean deleteLIWebaccesslogRecord(LIWebaccesslogRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			String Query = "DELETE FROM web_access_log WHERE (Id = ?)";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			Query = updateQuery(Query);
			logger.trace("deleteLIWebaccesslogRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			ps = con.prepareStatement(Query);
			ps.setString(1, noNull(record.getId()));
			boolean result = ps.execute();
			logger.trace("deleteLIWebaccesslogRecord\t" + result + "\t");
			ps.close();
			createAuditRecord("web_access_log","DELETE",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, record.getActionSource());
			releaseDatabaseConnection(con, closeConnection);
			return result;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public boolean deleteLIWebaccesslogRecord(LIWebaccesslogRecord record)
	throws Exception
	{
		return deleteLIWebaccesslogRecord(record, null, true);
	}

	public LIWebaccesslogRecord[] searchLIWebaccesslogRecords(LIWebaccesslogRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "USER_ID", formatSearchField(searchRecord.getUserid()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SOURCE", formatSearchField(searchRecord.getSource()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SERVICE", formatSearchField(searchRecord.getService()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "RESULT", formatSearchField(searchRecord.getResult()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SESSIONID", formatSearchField(searchRecord.getSessionid()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from web_access_log " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM web_access_log ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM web_access_log $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLIWebaccesslogRecords(Query);
	}

	public LIWebaccesslogRecord[] searchLIWebaccesslogRecordsExact(LIWebaccesslogRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "USER_ID", formatSearchField(searchRecord.getUserid()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SOURCE", formatSearchField(searchRecord.getSource()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SERVICE", formatSearchField(searchRecord.getService()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RESULT", formatSearchField(searchRecord.getResult()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SESSIONID", formatSearchField(searchRecord.getSessionid()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from web_access_log " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM web_access_log ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM web_access_log $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLIWebaccesslogRecords(Query);
	}

	public LIWebaccesslogRecord[] searchLIWebaccesslogRecordsExactUpper(LIWebaccesslogRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "USER_ID", formatSearchField(searchRecord.getUserid()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "SOURCE", formatSearchField(searchRecord.getSource()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "SERVICE", formatSearchField(searchRecord.getService()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "RESULT", formatSearchField(searchRecord.getResult()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "SESSIONID", formatSearchField(searchRecord.getSessionid()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from web_access_log " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM web_access_log ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM web_access_log $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLIWebaccesslogRecords(Query);
	}

	public int loadLIWebaccesslogRecordCount(LIWebaccesslogRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "USER_ID", formatSearchField(searchRecord.getUserid()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SOURCE", formatSearchField(searchRecord.getSource()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "SERVICE", formatSearchField(searchRecord.getService()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "RESULT", formatSearchField(searchRecord.getResult()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SESSIONID", formatSearchField(searchRecord.getSessionid()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select count(*) from web_access_log " + WhereCondition;
		Query = updateQuery(Query);
		logger.trace("Search Count Query	" + Query + "\t");
		return loadCount(Query);
	}

	public int loadLIWebaccesslogRecordCountExact(LIWebaccesslogRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "USER_ID", formatSearchField(searchRecord.getUserid()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "SOURCE", formatSearchField(searchRecord.getSource()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "SERVICE", formatSearchField(searchRecord.getService()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RESULT", formatSearchField(searchRecord.getResult()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "SESSIONID", formatSearchField(searchRecord.getSessionid()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select count(*) from web_access_log " + WhereCondition;
		Query = updateQuery(Query);
		logger.trace("Search Count Query	" + Query + "\t");
		return loadCount(Query);
	}
}
