
/*
 * LICsidealerscoretableviewDAO.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.dao;

import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.to.LICsidealerscoretableviewRecord;
import java.sql.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;

public class LICsidealerscoretableviewDAO extends LIDAO {
    
    
    
	static LogUtils logger = new LogUtils(LICsidealerscoretableviewDAO.class.getName());


	public LICsidealerscoretableviewRecord[] loadLICsidealerscoretableviewRecords(String query, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
                        Statement stmt=con.createStatement();
                        int i = stmt.executeUpdate("SET @r1=0,  @r2=0,  @r3=0,  @r4=0,  @r5=0,  @r6=0,  @r7=0, @r8=0, @r9=0, @rsum=0,@rorder=0,@trank=0,@rcsi=0");
                        stmt.close();
                        
			query = query + MAX_RECORD_LIMIT_APPENDER;
			query = updateQuery(query);
			logger.trace("loadLICsidealerscoretableviewRecords\t" + closeConnection + "\t" + query);
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			ArrayList recordSet = new ArrayList();
			while(rs.next())
			{
				LICsidealerscoretableviewRecord record = new LICsidealerscoretableviewRecord();
				
                                record.setRegion(rs.getString("region"));
                                record.setRank(rs.getString("rank"));
				record.setDealer(rs.getString("dealer"));
				record.setCsi(rs.getString("csi"));
				record.setCsicolor(rs.getString("csicolor"));
				record.setServiceadvisor(rs.getString("ServiceAdvisor"));
				record.setServiceadvisorcolor(rs.getString("ServiceAdvisorColor"));
				record.setServicefacility(rs.getString("Servicefacility"));
				record.setServicefacilitycolor(rs.getString("ServicefacilityColor"));
				record.setServiceinitiation(rs.getString("ServiceInitiation"));
				record.setServiceinitiationcolor(rs.getString("ServiceInitiationColor"));
				record.setServicequality(rs.getString("ServiceQuality"));
				record.setServicequalitycolor(rs.getString("ServiceQualityColor"));
				record.setVehiclepickup(rs.getString("VehiclePickup"));
				record.setVehiclepickupcolor(rs.getString("VehiclePickupColor"));
				record.setSop(rs.getString("sop"));
				record.setSopcolor(rs.getString("sopcolor"));
				recordSet.add(record);
			}
			logger.trace("loadLICsidealerscoretableviewRecords:Records Fetched:" + recordSet.size());
			LICsidealerscoretableviewRecord[] tempLICsidealerscoretableviewRecords = new LICsidealerscoretableviewRecord[recordSet.size()];
			for (int index = 0; index < recordSet.size(); index++)
			{
				tempLICsidealerscoretableviewRecords[index] = (LICsidealerscoretableviewRecord)(recordSet.get(index));
			}
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			return tempLICsidealerscoretableviewRecords;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

        
        public LICsidealerscoretableviewRecord[] loadLICmodelscoretableviewRecords(String query, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
                        Statement stmt=con.createStatement();
                        int i=stmt.executeUpdate("SET @r1=0,  @r2=0,  @r3=0,  @r4=0,  @r5=0,  @r6=0,  @r7=0, @r8=0, @r9=0, @rsum=0,@rorder=0");
                        stmt.close();
                        
			query = query + MAX_RECORD_LIMIT_APPENDER;
			query = updateQuery(query);
			logger.trace("loadLICmodelscoretableviewRecords\t" + closeConnection + "\t" + query);
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			ArrayList recordSet = new ArrayList();
			while(rs.next())
			{
				LICsidealerscoretableviewRecord record = new LICsidealerscoretableviewRecord();
				
                                record.setRegion(rs.getString("model"));
				record.setCsi(rs.getString("csi"));
				record.setCsicolor(rs.getString("csicolor"));
				record.setServiceadvisor(rs.getString("ServiceAdvisor"));
				record.setServiceadvisorcolor(rs.getString("ServiceAdvisorColor"));
				record.setServicefacility(rs.getString("Servicefacility"));
				record.setServicefacilitycolor(rs.getString("ServicefacilityColor"));
				record.setServiceinitiation(rs.getString("ServiceInitiation"));
				record.setServiceinitiationcolor(rs.getString("ServiceInitiationColor"));
				record.setServicequality(rs.getString("ServiceQuality"));
				record.setServicequalitycolor(rs.getString("ServiceQualityColor"));
				record.setVehiclepickup(rs.getString("VehiclePickup"));
				record.setVehiclepickupcolor(rs.getString("VehiclePickupColor"));
				record.setSop("0");
				record.setSopcolor("FFFFFF");
				recordSet.add(record);
			}
			logger.trace("loadLICmodelscoretableviewRecords:Records Fetched:" + recordSet.size());
			LICsidealerscoretableviewRecord[] tempLICsidealerscoretableviewRecords = new LICsidealerscoretableviewRecord[recordSet.size()];
			for (int index = 0; index < recordSet.size(); index++)
			{
				tempLICsidealerscoretableviewRecords[index] = (LICsidealerscoretableviewRecord)(recordSet.get(index));
			}
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			return tempLICsidealerscoretableviewRecords;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}
        
        
	public LICsidealerscoretableviewRecord[] loadLICsidealerscoretableviewRecords(String query)
	throws Exception
	{
		return loadLICsidealerscoretableviewRecords(query, null, true);
	}


	public LICsidealerscoretableviewRecord loadFirstLICsidealerscoretableviewRecord(String query)
	throws Exception
	{
		LICsidealerscoretableviewRecord[] results = loadLICsidealerscoretableviewRecords(query);
		if (results == null) return null;
		if(results.length < 1) return null;
		return results[0];
	}

	public LICsidealerscoretableviewRecord loadLICsidealerscoretableviewRecord(String $pk$, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			String Query = "SELECT * FROM csidealerscoretable_view WHERE ($PK$ = ?)";
			Query = updateQuery(Query);
			logger.trace("loadLICsidealerscoretableviewRecord\t" + closeConnection + "\t" + $pk$ + "\t" + Query);
			ps = con.prepareStatement(Query);
			ps.setString(1,$pk$);
			rs = ps.executeQuery();
			if (!rs.next())
			{
				ps.close();
				releaseDatabaseConnection(con, closeConnection);
				return null;
			}
			LICsidealerscoretableviewRecord record = new LICsidealerscoretableviewRecord();
			record.setRegion(rs.getString("region"));
			record.setDealer(rs.getString("dealer"));
			record.setCsi(rs.getString("csi"));
			record.setCsicolor(rs.getString("csicolor"));
			record.setServiceadvisor(rs.getString("ServiceAdvisor"));
			record.setServiceadvisorcolor(rs.getString("ServiceAdvisorColor"));
			record.setServicefacility(rs.getString("Servicefacility"));
			record.setServicefacilitycolor(rs.getString("ServicefacilityColor"));
			record.setServiceinitiation(rs.getString("ServiceInitiation"));
			record.setServiceinitiationcolor(rs.getString("ServiceInitiationColor"));
			record.setServicequality(rs.getString("ServiceQuality"));
			record.setServicequalitycolor(rs.getString("ServiceQualityColor"));
			record.setVehiclepickup(rs.getString("VehiclePickup"));
			record.setVehiclepickupcolor(rs.getString("VehiclePickupColor"));
			record.setSop("0");
				record.setSopcolor("FFFFFF");
			ps.close();
			logger.trace("loadLICsidealerscoretableviewRecord\t" + record + "\t");
			releaseDatabaseConnection(con, closeConnection);
			return record;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public LICsidealerscoretableviewRecord loadLICsidealerscoretableviewRecord(String $pk$)
	throws Exception
	{
		return loadLICsidealerscoretableviewRecord($pk$, null, true);
	}

	public LICsidealerscoretableviewRecord[] searchLICsidealerscoretableviewRecords(LICsidealerscoretableviewRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "region", formatSearchField(searchRecord.getRegion()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "dealer", formatSearchField(searchRecord.getDealer()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "csi", formatSearchField(searchRecord.getCsi()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "csicolor", formatSearchField(searchRecord.getCsicolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ServiceAdvisor", formatSearchField(searchRecord.getServiceadvisor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ServiceAdvisorColor", formatSearchField(searchRecord.getServiceadvisorcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "Servicefacility", formatSearchField(searchRecord.getServicefacility()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ServicefacilityColor", formatSearchField(searchRecord.getServicefacilitycolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ServiceInitiation", formatSearchField(searchRecord.getServiceinitiation()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ServiceInitiationColor", formatSearchField(searchRecord.getServiceinitiationcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ServiceQuality", formatSearchField(searchRecord.getServicequality()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ServiceQualityColor", formatSearchField(searchRecord.getServicequalitycolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "VehiclePickup", formatSearchField(searchRecord.getVehiclepickup()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "VehiclePickupColor", formatSearchField(searchRecord.getVehiclepickupcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "sop", formatSearchField(searchRecord.getSop()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "sopColor", formatSearchField(searchRecord.getSopcolor()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from csidealerscoretable_view " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM csidealerscoretable_view ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM csidealerscoretable_view $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLICsidealerscoretableviewRecords(Query);
	}

	public LICsidealerscoretableviewRecord[] searchLICsidealerscoretableviewRecordsExact(LICsidealerscoretableviewRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "region", formatSearchField(searchRecord.getRegion()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "dealer", formatSearchField(searchRecord.getDealer()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "csi", formatSearchField(searchRecord.getCsi()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "csicolor", formatSearchField(searchRecord.getCsicolor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ServiceAdvisor", formatSearchField(searchRecord.getServiceadvisor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ServiceAdvisorColor", formatSearchField(searchRecord.getServiceadvisorcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Servicefacility", formatSearchField(searchRecord.getServicefacility()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ServicefacilityColor", formatSearchField(searchRecord.getServicefacilitycolor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ServiceInitiation", formatSearchField(searchRecord.getServiceinitiation()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ServiceInitiationColor", formatSearchField(searchRecord.getServiceinitiationcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ServiceQuality", formatSearchField(searchRecord.getServicequality()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ServiceQualityColor", formatSearchField(searchRecord.getServicequalitycolor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "VehiclePickup", formatSearchField(searchRecord.getVehiclepickup()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "VehiclePickupColor", formatSearchField(searchRecord.getVehiclepickupcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "sop", formatSearchField(searchRecord.getSop()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "sopColor", formatSearchField(searchRecord.getSopcolor()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from csidealerscoretable_view " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM csidealerscoretable_view ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM csidealerscoretable_view $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLICsidealerscoretableviewRecords(Query);
	}

	public LICsidealerscoretableviewRecord[] searchLICsidealerscoretableviewRecordsExactUpper(LICsidealerscoretableviewRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "region", formatSearchField(searchRecord.getRegion()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "dealer", formatSearchField(searchRecord.getDealer()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "csi", formatSearchField(searchRecord.getCsi()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "csicolor", formatSearchField(searchRecord.getCsicolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ServiceAdvisor", formatSearchField(searchRecord.getServiceadvisor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ServiceAdvisorColor", formatSearchField(searchRecord.getServiceadvisorcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "Servicefacility", formatSearchField(searchRecord.getServicefacility()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ServicefacilityColor", formatSearchField(searchRecord.getServicefacilitycolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ServiceInitiation", formatSearchField(searchRecord.getServiceinitiation()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ServiceInitiationColor", formatSearchField(searchRecord.getServiceinitiationcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ServiceQuality", formatSearchField(searchRecord.getServicequality()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ServiceQualityColor", formatSearchField(searchRecord.getServicequalitycolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "VehiclePickup", formatSearchField(searchRecord.getVehiclepickup()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "VehiclePickupColor", formatSearchField(searchRecord.getVehiclepickupcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "sop", formatSearchField(searchRecord.getSop()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "sopColor", formatSearchField(searchRecord.getSopcolor()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from csidealerscoretable_view " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM csidealerscoretable_view ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM csidealerscoretable_view $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLICsidealerscoretableviewRecords(Query);
	}

	public int loadLICsidealerscoretableviewRecordCount(LICsidealerscoretableviewRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "region", formatSearchField(searchRecord.getRegion()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "dealer", formatSearchField(searchRecord.getDealer()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "csi", formatSearchField(searchRecord.getCsi()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "csicolor", formatSearchField(searchRecord.getCsicolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ServiceAdvisor", formatSearchField(searchRecord.getServiceadvisor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ServiceAdvisorColor", formatSearchField(searchRecord.getServiceadvisorcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "Servicefacility", formatSearchField(searchRecord.getServicefacility()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ServicefacilityColor", formatSearchField(searchRecord.getServicefacilitycolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ServiceInitiation", formatSearchField(searchRecord.getServiceinitiation()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ServiceInitiationColor", formatSearchField(searchRecord.getServiceinitiationcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ServiceQuality", formatSearchField(searchRecord.getServicequality()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ServiceQualityColor", formatSearchField(searchRecord.getServicequalitycolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "VehiclePickup", formatSearchField(searchRecord.getVehiclepickup()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "VehiclePickupColor", formatSearchField(searchRecord.getVehiclepickupcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "sop", formatSearchField(searchRecord.getSop()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "sopColor", formatSearchField(searchRecord.getSopcolor()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select count(*) from csidealerscoretable_view " + WhereCondition;
		Query = updateQuery(Query);
		logger.trace("Search Count Query	" + Query + "\t");
		return loadCount(Query);
	}

	public int loadLICsidealerscoretableviewRecordCountExact(LICsidealerscoretableviewRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "region", formatSearchField(searchRecord.getRegion()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "dealer", formatSearchField(searchRecord.getDealer()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "csi", formatSearchField(searchRecord.getCsi()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "csicolor", formatSearchField(searchRecord.getCsicolor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ServiceAdvisor", formatSearchField(searchRecord.getServiceadvisor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ServiceAdvisorColor", formatSearchField(searchRecord.getServiceadvisorcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Servicefacility", formatSearchField(searchRecord.getServicefacility()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ServicefacilityColor", formatSearchField(searchRecord.getServicefacilitycolor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ServiceInitiation", formatSearchField(searchRecord.getServiceinitiation()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ServiceInitiationColor", formatSearchField(searchRecord.getServiceinitiationcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ServiceQuality", formatSearchField(searchRecord.getServicequality()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ServiceQualityColor", formatSearchField(searchRecord.getServicequalitycolor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "VehiclePickup", formatSearchField(searchRecord.getVehiclepickup()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "VehiclePickupColor", formatSearchField(searchRecord.getVehiclepickupcolor()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "sop", formatSearchField(searchRecord.getSop()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "sopColor", formatSearchField(searchRecord.getSopcolor()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select count(*) from csidealerscoretable_view " + WhereCondition;
		Query = updateQuery(Query);
		logger.trace("Search Count Query	" + Query + "\t");
		return loadCount(Query);
	}

    
}
