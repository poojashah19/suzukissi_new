
/*
 * LISsiloyaltyandadvocacyaggregateDAO.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.dao;
import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.to.LISsiloyaltyandadvocacyaggregateRecord;
import java.sql.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LISsiloyaltyandadvocacyaggregateDAO extends LIDAO
{
	static LogUtils logger = new LogUtils(LISsiloyaltyandadvocacyaggregateDAO.class.getName());


	public LISsiloyaltyandadvocacyaggregateRecord[] loadLISsiloyaltyandadvocacyaggregateRecords(String query, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			query = query + MAX_RECORD_LIMIT_APPENDER;
			query = updateQuery(query);
			logger.trace("loadLISsiloyaltyandadvocacyaggregateRecords\t" + closeConnection + "\t" + query);
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			ArrayList recordSet = new ArrayList();
			while(rs.next())
			{
				LISsiloyaltyandadvocacyaggregateRecord record = new LISsiloyaltyandadvocacyaggregateRecord();
				record.setId(rs.getString("Id"));
				record.setAttribute(rs.getString("attribute"));
				record.setAttributename(rs.getString("AttributeName"));
				record.setAttributeindex(rs.getString("AttributeIndex"));
				record.setRegion(rs.getString("region"));
				record.setState(rs.getString("state"));
				record.setCity(rs.getString("city"));
				record.setDealer(rs.getString("dealer"));
				record.setModel(rs.getString("model"));
				record.setBiweekly(rs.getString("Bi_Weekly"));
				record.setMonth(rs.getString("MONTH"));
				record.setQuarter(rs.getString("QUARTER"));
				record.setNindex(rs.getString("nINDEX"));
				record.setDefinitelywouldnotcount(rs.getString("DefinitelyWouldNotCount"));
				record.setDefinitelywouldcount(rs.getString("DefinitelyWouldCount"));
				record.setProbablywouldcount(rs.getString("ProbablyWouldCount"));
				record.setProbablywouldnotcount(rs.getString("ProbablyWouldNotCount"));
				record.setTotalcount(rs.getString("totalcount"));
				record.setRstatus(rs.getString("RSTATUS"));
				record.setLastacc(formatDBDateTime(rs.getTimestamp("LASTACC")));
				record.setCreatedby(rs.getString("CREATED_BY"));
				record.setCreatedat(formatDBDateTime(rs.getTimestamp("CREATED_AT")));
				record.setModifiedat(formatDBDateTime(rs.getTimestamp("MODIFIED_AT")));
				record.setModifiedby(rs.getString("MODIFIED_BY"));
				record.setClosetype(rs.getString("CLOSE_TYPE"));
				record.setClosedat(rs.getString("CLOSED_AT"));
				recordSet.add(record);
			}
			logger.trace("loadLISsiloyaltyandadvocacyaggregateRecords:Records Fetched:" + recordSet.size());
			LISsiloyaltyandadvocacyaggregateRecord[] tempLISsiloyaltyandadvocacyaggregateRecords = new LISsiloyaltyandadvocacyaggregateRecord[recordSet.size()];
			for (int index = 0; index < recordSet.size(); index++)
			{
				tempLISsiloyaltyandadvocacyaggregateRecords[index] = (LISsiloyaltyandadvocacyaggregateRecord)(recordSet.get(index));
			}
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			return tempLISsiloyaltyandadvocacyaggregateRecords;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}


	public LISsiloyaltyandadvocacyaggregateRecord[] loadLISsiloyaltyandadvocacyaggregateRecords(String query)
	throws Exception
	{
		return loadLISsiloyaltyandadvocacyaggregateRecords(query, null, true);
	}


	public LISsiloyaltyandadvocacyaggregateRecord loadFirstLISsiloyaltyandadvocacyaggregateRecord(String query)
	throws Exception
	{
		LISsiloyaltyandadvocacyaggregateRecord[] results = loadLISsiloyaltyandadvocacyaggregateRecords(query);
		if (results == null) return null;
		if(results.length < 1) return null;
		return results[0];
	}

	public LISsiloyaltyandadvocacyaggregateRecord loadLISsiloyaltyandadvocacyaggregateRecord(String id, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			String Query = "SELECT * FROM ssi_loyaltyandadvocacy_aggregate WHERE (Id = ?)";
			Query = updateQuery(Query);
			logger.trace("loadLISsiloyaltyandadvocacyaggregateRecord\t" + closeConnection + "\t" + id + "\t" + Query);
			ps = con.prepareStatement(Query);
			ps.setString(1,id);
			rs = ps.executeQuery();
			if (!rs.next())
			{
				ps.close();
				releaseDatabaseConnection(con, closeConnection);
				return null;
			}
			LISsiloyaltyandadvocacyaggregateRecord record = new LISsiloyaltyandadvocacyaggregateRecord();
			record.setId(rs.getString("Id"));
			record.setAttribute(rs.getString("attribute"));
			record.setAttributename(rs.getString("AttributeName"));
			record.setAttributeindex(rs.getString("AttributeIndex"));
			record.setRegion(rs.getString("region"));
			record.setState(rs.getString("state"));
			record.setCity(rs.getString("city"));
			record.setDealer(rs.getString("dealer"));
			record.setModel(rs.getString("model"));
			record.setBiweekly(rs.getString("Bi_Weekly"));
			record.setMonth(rs.getString("MONTH"));
			record.setQuarter(rs.getString("QUARTER"));
			record.setNindex(rs.getString("nINDEX"));
			record.setDefinitelywouldnotcount(rs.getString("DefinitelyWouldNotCount"));
			record.setDefinitelywouldcount(rs.getString("DefinitelyWouldCount"));
			record.setProbablywouldcount(rs.getString("ProbablyWouldCount"));
			record.setProbablywouldnotcount(rs.getString("ProbablyWouldNotCount"));
			record.setTotalcount(rs.getString("totalcount"));
			record.setRstatus(rs.getString("RSTATUS"));
			record.setLastacc(formatDBDateTime(rs.getTimestamp("LASTACC")));
			record.setCreatedby(rs.getString("CREATED_BY"));
			record.setCreatedat(formatDBDateTime(rs.getTimestamp("CREATED_AT")));
			record.setModifiedat(formatDBDateTime(rs.getTimestamp("MODIFIED_AT")));
			record.setModifiedby(rs.getString("MODIFIED_BY"));
			record.setClosetype(rs.getString("CLOSE_TYPE"));
			record.setClosedat(rs.getString("CLOSED_AT"));
			ps.close();
			logger.trace("loadLISsiloyaltyandadvocacyaggregateRecord\t" + record + "\t");
			releaseDatabaseConnection(con, closeConnection);
			return record;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public LISsiloyaltyandadvocacyaggregateRecord loadLISsiloyaltyandadvocacyaggregateRecord(String id)
	throws Exception
	{
		return loadLISsiloyaltyandadvocacyaggregateRecord(id, null, true);
	}

	public int insertLISsiloyaltyandadvocacyaggregateRecord(LISsiloyaltyandadvocacyaggregateRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			String Query = "INSERT INTO ssi_loyaltyandadvocacy_aggregate ";
			Query += "(";
			Query += "attribute,AttributeName,AttributeIndex,region,state,city,dealer,model,Bi_Weekly,MONTH,QUARTER,nINDEX,DefinitelyWouldNotCount,DefinitelyWouldCount,ProbablyWouldCount,ProbablyWouldNotCount,totalcount,RSTATUS,LASTACC,CREATED_BY,CREATED_AT,MODIFIED_AT,MODIFIED_BY,CLOSE_TYPE,CLOSED_AT";
			Query += ")";
			Query += " VALUES ";
			Query += "(";
			Query += "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?";
			Query += ")";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			record.setModifiedby(StringUtils.noNull(record.getModifiedby(),record.getCreatedby()));
			Query = updateQuery(Query);
			logger.trace("insertLISsiloyaltyandadvocacyaggregateRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			if (isOracleDatabase()) 
			{
				ps = con.prepareStatement(Query,new String[]{"ID"});
			}
			else
			{
				ps = con.prepareStatement(Query,Statement.RETURN_GENERATED_KEYS);
			}
			setStringValue(ps, 1, record.getAttribute());
			setStringValue(ps, 2, record.getAttributename());
			setStringValue(ps, 3, record.getAttributeindex());
			setStringValue(ps, 4, record.getRegion());
			setStringValue(ps, 5, record.getState());
			setStringValue(ps, 6, record.getCity());
			setStringValue(ps, 7, record.getDealer());
			setStringValue(ps, 8, record.getModel());
			setStringValue(ps, 9, record.getBiweekly());
			setStringValue(ps, 10, record.getMonth());
			setStringValue(ps, 11, record.getQuarter());
			setStringValue(ps, 12, record.getNindex());
			setStringValue(ps, 13, record.getDefinitelywouldnotcount());
			setStringValue(ps, 14, record.getDefinitelywouldcount());
			setStringValue(ps, 15, record.getProbablywouldcount());
			setStringValue(ps, 16, record.getProbablywouldnotcount());
			setStringValue(ps, 17, record.getTotalcount());
			setStringValue(ps, 18, record.getRstatus());
			setDateValue(ps, 19, fd.getSQLDateObject(record.getLastacc(), "yyyyMMddHHmmss"));
			setStringValue(ps, 20, record.getCreatedby());
			setDateValue(ps, 21, fd.getCurrentSQLDateObject());
			setDateValue(ps, 22, fd.getCurrentSQLDateObject());
			setStringValue(ps, 23, record.getModifiedby());
			setStringValue(ps, 24, record.getClosetype());
			setStringValue(ps, 25, record.getClosedat());
			boolean result = ps.execute();
			logger.trace("insertLISsiloyaltyandadvocacyaggregateRecord\t" + result + "\t");
			int resultID = -1;
			rs = ps.getGeneratedKeys();
			if (rs.next()) 
			{
				resultID = rs.getInt(1); 
			}
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			createAuditRecord("ssi_loyaltyandadvocacy_aggregate","INSERT",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, record.getActionSource());
			return resultID;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public int insertLISsiloyaltyandadvocacyaggregateRecord(LISsiloyaltyandadvocacyaggregateRecord record)
	throws Exception
	{
		return insertLISsiloyaltyandadvocacyaggregateRecord(record, null, true);
	}

	public boolean updateLISsiloyaltyandadvocacyaggregateRecord(LISsiloyaltyandadvocacyaggregateRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			LISsiloyaltyandadvocacyaggregateRecord currentRecord = loadLISsiloyaltyandadvocacyaggregateRecord(record.getId());
			String currentRecordContent = StringUtils.noNull(currentRecord);

			String Query = "UPDATE ssi_loyaltyandadvocacy_aggregate SET ";
			Query += "attribute = ?, ";
			Query += "AttributeName = ?, ";
			Query += "AttributeIndex = ?, ";
			Query += "region = ?, ";
			Query += "state = ?, ";
			Query += "city = ?, ";
			Query += "dealer = ?, ";
			Query += "model = ?, ";
			Query += "Bi_Weekly = ?, ";
			Query += "MONTH = ?, ";
			Query += "QUARTER = ?, ";
			Query += "nINDEX = ?, ";
			Query += "DefinitelyWouldNotCount = ?, ";
			Query += "DefinitelyWouldCount = ?, ";
			Query += "ProbablyWouldCount = ?, ";
			Query += "ProbablyWouldNotCount = ?, ";
			Query += "totalcount = ?, ";
			Query += "RSTATUS = ?, ";
			Query += "LASTACC = ?, ";
			Query += "CREATED_BY = ?, ";
			Query += "CREATED_AT = ?, ";
			Query += "MODIFIED_AT = ?, ";
			Query += "MODIFIED_BY = ?, ";
			Query += "CLOSE_TYPE = ?, ";
			Query += "CLOSED_AT = ? ";
			Query += "WHERE (Id = ?) ";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			Query = updateQuery(Query);
			logger.trace("updateLISsiloyaltyandadvocacyaggregateRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			ps = con.prepareStatement(Query);
			setStringValue(ps, 1, record.getAttribute());
			setStringValue(ps, 2, record.getAttributename());
			setStringValue(ps, 3, record.getAttributeindex());
			setStringValue(ps, 4, record.getRegion());
			setStringValue(ps, 5, record.getState());
			setStringValue(ps, 6, record.getCity());
			setStringValue(ps, 7, record.getDealer());
			setStringValue(ps, 8, record.getModel());
			setStringValue(ps, 9, record.getBiweekly());
			setStringValue(ps, 10, record.getMonth());
			setStringValue(ps, 11, record.getQuarter());
			setStringValue(ps, 12, record.getNindex());
			setStringValue(ps, 13, record.getDefinitelywouldnotcount());
			setStringValue(ps, 14, record.getDefinitelywouldcount());
			setStringValue(ps, 15, record.getProbablywouldcount());
			setStringValue(ps, 16, record.getProbablywouldnotcount());
			setStringValue(ps, 17, record.getTotalcount());
			setStringValue(ps, 18, record.getRstatus());
			setDateValue(ps, 19, fd.getSQLDateObject(record.getLastacc(), "yyyyMMddHHmmss"));
			setStringValue(ps, 20, record.getCreatedby());
			setDateValue(ps, 21, fd.getSQLDateObject(record.getCreatedat(), "yyyyMMddHHmmss"));
			setDateValue(ps, 22, fd.getCurrentSQLDateObject());
			setStringValue(ps, 23, record.getModifiedby());
			setStringValue(ps, 24, record.getClosetype());
			setStringValue(ps, 25, record.getClosedat());
			ps.setString(26, noNull(record.getId()));
			boolean result = ps.execute();
			logger.trace("updateLISsiloyaltyandadvocacyaggregateRecord\t" + result + "\t");
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			createAuditRecord("ssi_loyaltyandadvocacy_aggregate","UPDATE",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, currentRecordContent, record.getActionSource());
			return result;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public boolean updateLISsiloyaltyandadvocacyaggregateRecord(LISsiloyaltyandadvocacyaggregateRecord record)
	throws Exception
	{
		return updateLISsiloyaltyandadvocacyaggregateRecord(record, null, true);
	}

	public boolean deleteLISsiloyaltyandadvocacyaggregateRecord(LISsiloyaltyandadvocacyaggregateRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			String Query = "DELETE FROM ssi_loyaltyandadvocacy_aggregate WHERE (Id = ?)";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			Query = updateQuery(Query);
			logger.trace("deleteLISsiloyaltyandadvocacyaggregateRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			ps = con.prepareStatement(Query);
			ps.setString(1, noNull(record.getId()));
			boolean result = ps.execute();
			logger.trace("deleteLISsiloyaltyandadvocacyaggregateRecord\t" + result + "\t");
			ps.close();
			createAuditRecord("ssi_loyaltyandadvocacy_aggregate","DELETE",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, record.getActionSource());
			releaseDatabaseConnection(con, closeConnection);
			return result;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public boolean deleteLISsiloyaltyandadvocacyaggregateRecord(LISsiloyaltyandadvocacyaggregateRecord record)
	throws Exception
	{
		return deleteLISsiloyaltyandadvocacyaggregateRecord(record, null, true);
	}

	public LISsiloyaltyandadvocacyaggregateRecord[] searchLISsiloyaltyandadvocacyaggregateRecords(LISsiloyaltyandadvocacyaggregateRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "attribute", formatSearchField(searchRecord.getAttribute()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "AttributeName", formatSearchField(searchRecord.getAttributename()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "AttributeIndex", formatSearchField(searchRecord.getAttributeindex()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "region", formatSearchField(searchRecord.getRegion()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "state", formatSearchField(searchRecord.getState()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "city", formatSearchField(searchRecord.getCity()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "dealer", formatSearchField(searchRecord.getDealer()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "model", formatSearchField(searchRecord.getModel()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "Bi_Weekly", formatSearchField(searchRecord.getBiweekly()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MONTH", formatSearchField(searchRecord.getMonth()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "QUARTER", formatSearchField(searchRecord.getQuarter()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "nINDEX", formatSearchField(searchRecord.getNindex()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "DefinitelyWouldNotCount", formatSearchField(searchRecord.getDefinitelywouldnotcount()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "DefinitelyWouldCount", formatSearchField(searchRecord.getDefinitelywouldcount()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ProbablyWouldCount", formatSearchField(searchRecord.getProbablywouldcount()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ProbablyWouldNotCount", formatSearchField(searchRecord.getProbablywouldnotcount()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "totalcount", formatSearchField(searchRecord.getTotalcount()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "LASTACC", formatSearchField(searchRecord.getLastacc()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CLOSE_TYPE", formatSearchField(searchRecord.getClosetype()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CLOSED_AT", formatSearchField(searchRecord.getClosedat()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from ssi_loyaltyandadvocacy_aggregate " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM ssi_loyaltyandadvocacy_aggregate ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM ssi_loyaltyandadvocacy_aggregate $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLISsiloyaltyandadvocacyaggregateRecords(Query);
	}

	public LISsiloyaltyandadvocacyaggregateRecord[] searchLISsiloyaltyandadvocacyaggregateRecordsExact(LISsiloyaltyandadvocacyaggregateRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "attribute", formatSearchField(searchRecord.getAttribute()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "AttributeName", formatSearchField(searchRecord.getAttributename()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "AttributeIndex", formatSearchField(searchRecord.getAttributeindex()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "region", formatSearchField(searchRecord.getRegion()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "state", formatSearchField(searchRecord.getState()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "city", formatSearchField(searchRecord.getCity()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "dealer", formatSearchField(searchRecord.getDealer()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "model", formatSearchField(searchRecord.getModel()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Bi_Weekly", formatSearchField(searchRecord.getBiweekly()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MONTH", formatSearchField(searchRecord.getMonth()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "QUARTER", formatSearchField(searchRecord.getQuarter()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "nINDEX", formatSearchField(searchRecord.getNindex()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "DefinitelyWouldNotCount", formatSearchField(searchRecord.getDefinitelywouldnotcount()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "DefinitelyWouldCount", formatSearchField(searchRecord.getDefinitelywouldcount()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ProbablyWouldCount", formatSearchField(searchRecord.getProbablywouldcount()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ProbablyWouldNotCount", formatSearchField(searchRecord.getProbablywouldnotcount()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "totalcount", formatSearchField(searchRecord.getTotalcount()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "LASTACC", formatSearchField(searchRecord.getLastacc()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CLOSE_TYPE", formatSearchField(searchRecord.getClosetype()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CLOSED_AT", formatSearchField(searchRecord.getClosedat()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from ssi_loyaltyandadvocacy_aggregate " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM ssi_loyaltyandadvocacy_aggregate ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM ssi_loyaltyandadvocacy_aggregate $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLISsiloyaltyandadvocacyaggregateRecords(Query);
	}

	public LISsiloyaltyandadvocacyaggregateRecord[] searchLISsiloyaltyandadvocacyaggregateRecordsExactUpper(LISsiloyaltyandadvocacyaggregateRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "attribute", formatSearchField(searchRecord.getAttribute()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "AttributeName", formatSearchField(searchRecord.getAttributename()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "AttributeIndex", formatSearchField(searchRecord.getAttributeindex()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "region", formatSearchField(searchRecord.getRegion()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "state", formatSearchField(searchRecord.getState()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "city", formatSearchField(searchRecord.getCity()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "dealer", formatSearchField(searchRecord.getDealer()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "model", formatSearchField(searchRecord.getModel()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "Bi_Weekly", formatSearchField(searchRecord.getBiweekly()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MONTH", formatSearchField(searchRecord.getMonth()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "QUARTER", formatSearchField(searchRecord.getQuarter()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "nINDEX", formatSearchField(searchRecord.getNindex()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "DefinitelyWouldNotCount", formatSearchField(searchRecord.getDefinitelywouldnotcount()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "DefinitelyWouldCount", formatSearchField(searchRecord.getDefinitelywouldcount()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ProbablyWouldCount", formatSearchField(searchRecord.getProbablywouldcount()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ProbablyWouldNotCount", formatSearchField(searchRecord.getProbablywouldnotcount()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "totalcount", formatSearchField(searchRecord.getTotalcount()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "LASTACC", formatSearchField(searchRecord.getLastacc()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CLOSE_TYPE", formatSearchField(searchRecord.getClosetype()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CLOSED_AT", formatSearchField(searchRecord.getClosedat()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from ssi_loyaltyandadvocacy_aggregate " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM ssi_loyaltyandadvocacy_aggregate ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM ssi_loyaltyandadvocacy_aggregate $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLISsiloyaltyandadvocacyaggregateRecords(Query);
	}

	public int loadLISsiloyaltyandadvocacyaggregateRecordCount(LISsiloyaltyandadvocacyaggregateRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "attribute", formatSearchField(searchRecord.getAttribute()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "AttributeName", formatSearchField(searchRecord.getAttributename()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "AttributeIndex", formatSearchField(searchRecord.getAttributeindex()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "region", formatSearchField(searchRecord.getRegion()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "state", formatSearchField(searchRecord.getState()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "city", formatSearchField(searchRecord.getCity()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "dealer", formatSearchField(searchRecord.getDealer()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "model", formatSearchField(searchRecord.getModel()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "Bi_Weekly", formatSearchField(searchRecord.getBiweekly()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MONTH", formatSearchField(searchRecord.getMonth()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "QUARTER", formatSearchField(searchRecord.getQuarter()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "nINDEX", formatSearchField(searchRecord.getNindex()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "DefinitelyWouldNotCount", formatSearchField(searchRecord.getDefinitelywouldnotcount()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "DefinitelyWouldCount", formatSearchField(searchRecord.getDefinitelywouldcount()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "ProbablyWouldCount", formatSearchField(searchRecord.getProbablywouldcount()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "ProbablyWouldNotCount", formatSearchField(searchRecord.getProbablywouldnotcount()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "totalcount", formatSearchField(searchRecord.getTotalcount()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "LASTACC", formatSearchField(searchRecord.getLastacc()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CLOSE_TYPE", formatSearchField(searchRecord.getClosetype()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CLOSED_AT", formatSearchField(searchRecord.getClosedat()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select count(*) from ssi_loyaltyandadvocacy_aggregate " + WhereCondition;
		Query = updateQuery(Query);
		logger.trace("Search Count Query	" + Query + "\t");
		return loadCount(Query);
	}

	public int loadLISsiloyaltyandadvocacyaggregateRecordCountExact(LISsiloyaltyandadvocacyaggregateRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "Id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "attribute", formatSearchField(searchRecord.getAttribute()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "AttributeName", formatSearchField(searchRecord.getAttributename()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "AttributeIndex", formatSearchField(searchRecord.getAttributeindex()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "region", formatSearchField(searchRecord.getRegion()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "state", formatSearchField(searchRecord.getState()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "city", formatSearchField(searchRecord.getCity()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "dealer", formatSearchField(searchRecord.getDealer()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "model", formatSearchField(searchRecord.getModel()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Bi_Weekly", formatSearchField(searchRecord.getBiweekly()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MONTH", formatSearchField(searchRecord.getMonth()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "QUARTER", formatSearchField(searchRecord.getQuarter()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "nINDEX", formatSearchField(searchRecord.getNindex()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "DefinitelyWouldNotCount", formatSearchField(searchRecord.getDefinitelywouldnotcount()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "DefinitelyWouldCount", formatSearchField(searchRecord.getDefinitelywouldcount()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ProbablyWouldCount", formatSearchField(searchRecord.getProbablywouldcount()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "ProbablyWouldNotCount", formatSearchField(searchRecord.getProbablywouldnotcount()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "totalcount", formatSearchField(searchRecord.getTotalcount()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "LASTACC", formatSearchField(searchRecord.getLastacc()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CLOSE_TYPE", formatSearchField(searchRecord.getClosetype()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CLOSED_AT", formatSearchField(searchRecord.getClosedat()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select count(*) from ssi_loyaltyandadvocacy_aggregate " + WhereCondition;
		Query = updateQuery(Query);
		logger.trace("Search Count Query	" + Query + "\t");
		return loadCount(Query);
	}
}
