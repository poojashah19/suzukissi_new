
/*
 * LISsicalenderfilterDAO.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.dao;
import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.to.LISsicalenderfilterRecord;
import java.sql.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LISsicalenderfilterDAO extends LIDAO
{
	static LogUtils logger = new LogUtils(LISsicalenderfilterDAO.class.getName());


	public LISsicalenderfilterRecord[] loadLISsicalenderfilterRecords(String query, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			query = query + MAX_RECORD_LIMIT_APPENDER;
			query = updateQuery(query);
			logger.trace("loadLISsicalenderfilterRecords\t" + closeConnection + "\t" + query);
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			ArrayList recordSet = new ArrayList();
			while(rs.next())
			{
				LISsicalenderfilterRecord record = new LISsicalenderfilterRecord();
				record.setId(rs.getString("id"));
				record.setQuarter(rs.getString("Quarter"));
				record.setMonth(rs.getString("Month"));
				record.setBiweekly(rs.getString("biweekly"));
				record.setDate(formatDBDateTime(rs.getTimestamp("date")));
				record.setYear(rs.getString("year"));
				record.setRstatus(rs.getString("RSTATUS"));
				record.setLastacc(formatDBDateTime(rs.getTimestamp("LASTACC")));
				record.setCreatedby(rs.getString("CREATED_BY"));
				record.setCreatedat(formatDBDateTime(rs.getTimestamp("CREATED_AT")));
				record.setModifiedat(formatDBDateTime(rs.getTimestamp("MODIFIED_AT")));
				record.setModifiedby(rs.getString("MODIFIED_BY"));
				record.setClosetype(rs.getString("CLOSE_TYPE"));
				record.setClosedat(rs.getString("CLOSED_AT"));
				recordSet.add(record);
			}
			logger.trace("loadLISsicalenderfilterRecords:Records Fetched:" + recordSet.size());
			LISsicalenderfilterRecord[] tempLISsicalenderfilterRecords = new LISsicalenderfilterRecord[recordSet.size()];
			for (int index = 0; index < recordSet.size(); index++)
			{
				tempLISsicalenderfilterRecords[index] = (LISsicalenderfilterRecord)(recordSet.get(index));
			}
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			return tempLISsicalenderfilterRecords;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}


	public LISsicalenderfilterRecord[] loadLISsicalenderfilterRecords(String query)
	throws Exception
	{
		return loadLISsicalenderfilterRecords(query, null, true);
	}


	public LISsicalenderfilterRecord loadFirstLISsicalenderfilterRecord(String query)
	throws Exception
	{
		LISsicalenderfilterRecord[] results = loadLISsicalenderfilterRecords(query);
		if (results == null) return null;
		if(results.length < 1) return null;
		return results[0];
	}

	public LISsicalenderfilterRecord loadLISsicalenderfilterRecord(String id, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			String Query = "SELECT * FROM ssi_calender_filter WHERE (id = ?)";
			Query = updateQuery(Query);
			logger.trace("loadLISsicalenderfilterRecord\t" + closeConnection + "\t" + id + "\t" + Query);
			ps = con.prepareStatement(Query);
			ps.setString(1,id);
			rs = ps.executeQuery();
			if (!rs.next())
			{
				ps.close();
				releaseDatabaseConnection(con, closeConnection);
				return null;
			}
			LISsicalenderfilterRecord record = new LISsicalenderfilterRecord();
			record.setId(rs.getString("id"));
			record.setQuarter(rs.getString("Quarter"));
			record.setMonth(rs.getString("Month"));
			record.setBiweekly(rs.getString("biweekly"));
			record.setDate(formatDBDateTime(rs.getTimestamp("date")));
			record.setYear(rs.getString("year"));
			record.setRstatus(rs.getString("RSTATUS"));
			record.setLastacc(formatDBDateTime(rs.getTimestamp("LASTACC")));
			record.setCreatedby(rs.getString("CREATED_BY"));
			record.setCreatedat(formatDBDateTime(rs.getTimestamp("CREATED_AT")));
			record.setModifiedat(formatDBDateTime(rs.getTimestamp("MODIFIED_AT")));
			record.setModifiedby(rs.getString("MODIFIED_BY"));
			record.setClosetype(rs.getString("CLOSE_TYPE"));
			record.setClosedat(rs.getString("CLOSED_AT"));
			ps.close();
			logger.trace("loadLISsicalenderfilterRecord\t" + record + "\t");
			releaseDatabaseConnection(con, closeConnection);
			return record;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public LISsicalenderfilterRecord loadLISsicalenderfilterRecord(String id)
	throws Exception
	{
		return loadLISsicalenderfilterRecord(id, null, true);
	}

	public int insertLISsicalenderfilterRecord(LISsicalenderfilterRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			String Query = "INSERT INTO ssi_calender_filter ";
			Query += "(";
			Query += "Quarter,Month,biweekly,date,year,RSTATUS,LASTACC,CREATED_BY,CREATED_AT,MODIFIED_AT,MODIFIED_BY,CLOSE_TYPE,CLOSED_AT";
			Query += ")";
			Query += " VALUES ";
			Query += "(";
			Query += "?,?,?,?,?,?,?,?,?,?,?,?,?";
			Query += ")";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			record.setModifiedby(StringUtils.noNull(record.getModifiedby(),record.getCreatedby()));
			Query = updateQuery(Query);
			logger.trace("insertLISsicalenderfilterRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			if (isOracleDatabase()) 
			{
				ps = con.prepareStatement(Query,new String[]{"ID"});
			}
			else
			{
				ps = con.prepareStatement(Query,Statement.RETURN_GENERATED_KEYS);
			}
			setStringValue(ps, 1, record.getQuarter());
			setStringValue(ps, 2, record.getMonth());
			setStringValue(ps, 3, record.getBiweekly());
			setDateValue(ps, 4, fd.getSQLDateObject(record.getDate(), "yyyyMMddHHmmss"));
			setStringValue(ps, 5, record.getYear());
			setStringValue(ps, 6, record.getRstatus());
			setDateValue(ps, 7, fd.getSQLDateObject(record.getLastacc(), "yyyyMMddHHmmss"));
			setStringValue(ps, 8, record.getCreatedby());
			setDateValue(ps, 9, fd.getCurrentSQLDateObject());
			setDateValue(ps, 10, fd.getCurrentSQLDateObject());
			setStringValue(ps, 11, record.getModifiedby());
			setStringValue(ps, 12, record.getClosetype());
			setStringValue(ps, 13, record.getClosedat());
			boolean result = ps.execute();
			logger.trace("insertLISsicalenderfilterRecord\t" + result + "\t");
			int resultID = -1;
			rs = ps.getGeneratedKeys();
			if (rs.next()) 
			{
				resultID = rs.getInt(1); 
			}
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			createAuditRecord("ssi_calender_filter","INSERT",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, record.getActionSource());
			return resultID;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public int insertLISsicalenderfilterRecord(LISsicalenderfilterRecord record)
	throws Exception
	{
		return insertLISsicalenderfilterRecord(record, null, true);
	}

	public boolean updateLISsicalenderfilterRecord(LISsicalenderfilterRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			LISsicalenderfilterRecord currentRecord = loadLISsicalenderfilterRecord(record.getId());
			String currentRecordContent = StringUtils.noNull(currentRecord);

			String Query = "UPDATE ssi_calender_filter SET ";
			Query += "Quarter = ?, ";
			Query += "Month = ?, ";
			Query += "biweekly = ?, ";
			Query += "date = ?, ";
			Query += "year = ?, ";
			Query += "RSTATUS = ?, ";
			Query += "LASTACC = ?, ";
			Query += "CREATED_BY = ?, ";
			Query += "CREATED_AT = ?, ";
			Query += "MODIFIED_AT = ?, ";
			Query += "MODIFIED_BY = ?, ";
			Query += "CLOSE_TYPE = ?, ";
			Query += "CLOSED_AT = ? ";
			Query += "WHERE (id = ?) ";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			Query = updateQuery(Query);
			logger.trace("updateLISsicalenderfilterRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			ps = con.prepareStatement(Query);
			setStringValue(ps, 1, record.getQuarter());
			setStringValue(ps, 2, record.getMonth());
			setStringValue(ps, 3, record.getBiweekly());
			setDateValue(ps, 4, fd.getSQLDateObject(record.getDate(), "yyyyMMddHHmmss"));
			setStringValue(ps, 5, record.getYear());
			setStringValue(ps, 6, record.getRstatus());
			setDateValue(ps, 7, fd.getSQLDateObject(record.getLastacc(), "yyyyMMddHHmmss"));
			setStringValue(ps, 8, record.getCreatedby());
			setDateValue(ps, 9, fd.getSQLDateObject(record.getCreatedat(), "yyyyMMddHHmmss"));
			setDateValue(ps, 10, fd.getCurrentSQLDateObject());
			setStringValue(ps, 11, record.getModifiedby());
			setStringValue(ps, 12, record.getClosetype());
			setStringValue(ps, 13, record.getClosedat());
			ps.setString(14, noNull(record.getId()));
			boolean result = ps.execute();
			logger.trace("updateLISsicalenderfilterRecord\t" + result + "\t");
			ps.close();
			releaseDatabaseConnection(con, closeConnection);
			createAuditRecord("ssi_calender_filter","UPDATE",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, currentRecordContent, record.getActionSource());
			return result;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public boolean updateLISsicalenderfilterRecord(LISsicalenderfilterRecord record)
	throws Exception
	{
		return updateLISsicalenderfilterRecord(record, null, true);
	}

	public boolean deleteLISsicalenderfilterRecord(LISsicalenderfilterRecord record, Connection con, boolean closeConnection)
	throws Exception
	{
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			String Query = "DELETE FROM ssi_calender_filter WHERE (id = ?)";
			if (con == null)
			{
				con = getCPDatabaseConnection();
			}
			Query = updateQuery(Query);
			logger.trace("deleteLISsicalenderfilterRecord\t" + closeConnection + "\t" + record + "\t" + Query + "\t");
			ps = con.prepareStatement(Query);
			ps.setString(1, noNull(record.getId()));
			boolean result = ps.execute();
			logger.trace("deleteLISsicalenderfilterRecord\t" + result + "\t");
			ps.close();
			createAuditRecord("ssi_calender_filter","DELETE",record.getCreatedby(), record.getModifiedby(), record.getCreatedat(), record.getModifiedat(), record.toString(), null, record.getActionSource());
			releaseDatabaseConnection(con, closeConnection);
			return result;
		}
		finally
		{
			releaseDatabaseConnection(rs, ps, con, closeConnection);
		}
	}

	public boolean deleteLISsicalenderfilterRecord(LISsicalenderfilterRecord record)
	throws Exception
	{
		return deleteLISsicalenderfilterRecord(record, null, true);
	}

	public LISsicalenderfilterRecord[] searchLISsicalenderfilterRecords(LISsicalenderfilterRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "Quarter", formatSearchField(searchRecord.getQuarter()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "Month", formatSearchField(searchRecord.getMonth()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "biweekly", formatSearchField(searchRecord.getBiweekly()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "date", formatSearchField(searchRecord.getDate()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "year", formatSearchField(searchRecord.getYear()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "LASTACC", formatSearchField(searchRecord.getLastacc()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CLOSE_TYPE", formatSearchField(searchRecord.getClosetype()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CLOSED_AT", formatSearchField(searchRecord.getClosedat()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from ssi_calender_filter " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM ssi_calender_filter ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM ssi_calender_filter $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLISsicalenderfilterRecords(Query);
	}

	public LISsicalenderfilterRecord[] searchLISsicalenderfilterRecordsExact(LISsicalenderfilterRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Quarter", formatSearchField(searchRecord.getQuarter()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Month", formatSearchField(searchRecord.getMonth()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "biweekly", formatSearchField(searchRecord.getBiweekly()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "date", formatSearchField(searchRecord.getDate()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "year", formatSearchField(searchRecord.getYear()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "LASTACC", formatSearchField(searchRecord.getLastacc()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CLOSE_TYPE", formatSearchField(searchRecord.getClosetype()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CLOSED_AT", formatSearchField(searchRecord.getClosedat()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from ssi_calender_filter " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM ssi_calender_filter ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM ssi_calender_filter $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLISsicalenderfilterRecords(Query);
	}

	public LISsicalenderfilterRecord[] searchLISsicalenderfilterRecordsExactUpper(LISsicalenderfilterRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "Quarter", formatSearchField(searchRecord.getQuarter()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "Month", formatSearchField(searchRecord.getMonth()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "biweekly", formatSearchField(searchRecord.getBiweekly()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "date", formatSearchField(searchRecord.getDate()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "year", formatSearchField(searchRecord.getYear()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "LASTACC", formatSearchField(searchRecord.getLastacc()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CLOSE_TYPE", formatSearchField(searchRecord.getClosetype()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CLOSED_AT", formatSearchField(searchRecord.getClosedat()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select * from ssi_calender_filter " + WhereCondition + " order by " + ORDERBYSTRING;
		if (isMSSQL8())
		{
			Query = "select * from ( SELECT *,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) as rownum FROM ssi_calender_filter ) acvmfs " + WhereCondition;
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadMSSQL8OrderByID(ORDERBYSTRING),true);
		}
		if (isOracleDatabase())
		{
			Query = "select * from ( SELECT C.*,ROW_NUMBER() OVER (ORDER BY $ORDERBYSTRING$) R FROM (SELECT * FROM ssi_calender_filter $WHERECONDITION$) C ) WHERE (1=1) $OUTERLIMITCONDITION$";
			Query = StringUtils.replaceString(Query, "$WHERECONDITION$", WhereCondition,true);
			Query = StringUtils.replaceString(Query, "$OUTERLIMITCONDITION$", getOuterLimitCondition(),true);
			Query = StringUtils.replaceString(Query, "$ORDERBYSTRING$", loadOracleOrderByID(ORDERBYSTRING),true);
		}
		Query = updateQuery(Query);
		logger.trace("Search Query	" + Query + "\t");
		return loadLISsicalenderfilterRecords(Query);
	}

	public int loadLISsicalenderfilterRecordCount(LISsicalenderfilterRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "Quarter", formatSearchField(searchRecord.getQuarter()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "Month", formatSearchField(searchRecord.getMonth()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "biweekly", formatSearchField(searchRecord.getBiweekly()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "date", formatSearchField(searchRecord.getDate()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "year", formatSearchField(searchRecord.getYear()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "LASTACC", formatSearchField(searchRecord.getLastacc()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) = UPPER('$COLVAL$'))", "", "CLOSE_TYPE", formatSearchField(searchRecord.getClosetype()));
		WhereCondition = addSearchCondition(WhereCondition, "(UPPER($COLNAME$) LIKE UPPER('%$COLVAL$%'))", "", "CLOSED_AT", formatSearchField(searchRecord.getClosedat()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select count(*) from ssi_calender_filter " + WhereCondition;
		Query = updateQuery(Query);
		logger.trace("Search Count Query	" + Query + "\t");
		return loadCount(Query);
	}

	public int loadLISsicalenderfilterRecordCountExact(LISsicalenderfilterRecord searchRecord)
	throws Exception
	{
		String WhereCondition = "";
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "id", formatSearchField(searchRecord.getId()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Quarter", formatSearchField(searchRecord.getQuarter()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "Month", formatSearchField(searchRecord.getMonth()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "biweekly", formatSearchField(searchRecord.getBiweekly()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "date", formatSearchField(searchRecord.getDate()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "year", formatSearchField(searchRecord.getYear()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "RSTATUS", formatSearchField(searchRecord.getRstatus()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "LASTACC", formatSearchField(searchRecord.getLastacc()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "CREATED_BY", formatSearchField(searchRecord.getCreatedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CREATED_AT", formatSearchField(searchRecord.getCreatedat()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "MODIFIED_AT", formatSearchField(searchRecord.getModifiedat()));
		WhereCondition = addSearchCondition(WhereCondition, "(($COLNAME$) = ('$COLVAL$'))", "", "MODIFIED_BY", formatSearchField(searchRecord.getModifiedby()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CLOSE_TYPE", formatSearchField(searchRecord.getClosetype()));
		WhereCondition = addSearchCondition(WhereCondition, "($COLNAME$ = '$COLVAL$')", "", "CLOSED_AT", formatSearchField(searchRecord.getClosedat()));
		if (isNull(WhereCondition))
		{
			WhereCondition += "(1=1)";
		}
		if (!isNull(WhereCondition))
		{
			WhereCondition = "WHERE " + WhereCondition;
		}
		if (hasCustomCondition())
		{
			WhereCondition += getCustomCondition();
		}
		String Query = "select count(*) from ssi_calender_filter " + WhereCondition;
		Query = updateQuery(Query);
		logger.trace("Search Count Query	" + Query + "\t");
		return loadCount(Query);
	}
}
