/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.suzukithdashboard.webactions;

import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.service.LIReportquirescsiService;
import com.leadics.suzukithdashboard.service.LIReportquirescsiService;
import com.leadics.suzukithdashboard.service.LIReportquiresssiService;
import com.leadics.suzukithdashboard.to.LIReportquirescsiRecord;
import com.leadics.suzukithdashboard.to.LIReportquiresssiRecord;
import com.leadics.utils.LogUtils;
import com.leadics.utils.PropertyUtil;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

//git pushed
/**
 *
 * @author varma.sagi
 */
public class DealerSanpshotWebactions {

    String langCode = "EN";

    static LogUtils logger = new LogUtils(DealerSanpshotWebactions.class.getName());

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {
                String lang = request.getParameter("langCode");
                if (lang != null) {
                    langCode = lang;
                }

                if (!StringUtils.isNullOrEmpty(actionType)) {
                    if (actionType.equals("getDealerFactorScores")) {
                        getDealerFactorScores(request, response);
                        return;
                    }
                    if (actionType.equals("getDealerSOPTopBottom")) {
                        getDealerSOPTopBottom(request, response);
                        return;
                    }
                    if (actionType.equals("getDealerFactorTrend")) {
                        getDealerFactorTrend(request, response);
                        return;
                    }
                    if (actionType.equals("getDealerAttributeTrend")) {
                        getDealerAttributeTrend(request, response);
                        return;
                    }
                    if (actionType.equals("getDealerScoresYear")) {
                        getDealerScoresYear(request, response);
                        return;
                    }
                    if (actionType.equals("getDealerScoresBiannual")) {
                        getDealerScoresBiannual(request, response);
                        return;
                    }
                    if (actionType.equals("getDealerScoresMonth")) {
                        getDealerScoresMonth(request, response);
                        return;
                    }
                    if (actionType.equals("getAttributeTrendByWave")) {
                        getAttributeTrendByWave(request, response);
                        return;
                    }
                    if (actionType.equals("getDissatisfied")) {
                        getDissatisfied(request, response);
                        return;
                    }
                    if (actionType.equals("getDelighted")) {
                        getDelighted(request, response);
                        return;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    public void getDealerScoresYear(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDealerScoresYear'");
        String strQuery = queryRecord.getQuery();
        String strCountQuery = queryRecord.getCountquery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

//        filterList.add("zone");
//        filterList.add("region");
        filterList.add("dealer");
      

//        filterList.add("year");
        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

   
        String where_condtion = service.buildWherecondition(filterList, request);

   
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strCountQuery = StringUtils.replaceString(strCountQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.load2Vals(strQuery, "year", "year", "Score", "Score");

        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();

        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String wave = (String) tmpJson.get("year");
            String Score = (String) tmpJson.get("Score");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                wave = service.getTranslateString(wave, langCode);
                System.out.println("wave:" + wave);
            }
            
             if (Score == null) {
            wave = "";
        }
            addingJson.put("Wave", wave);
            addingJson.put("Score", Score);
            finalArray.add(addingJson);
        }

        String count = dao.loadString(strCountQuery);
        JSONObject obj = new JSONObject();
        obj.put("samplecount", count);
        obj.put("data", finalArray);
        out.println(obj.toJSONString());
//        out.println(jsonArray.toJSONString());
        return;
    }

    public void getDealerScoresBiannual(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDealerScoresBiannual'");
        String strQuery = queryRecord.getQuery();
        String strCountQuery = queryRecord.getCountquery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

        filterList.add("zone");
        filterList.add("region");
        filterList.add("dealer");
        String year = request.getParameter("biannual");
        if (year == null || year.equalsIgnoreCase("Study Total")) {
            year = "2016H2 (Jul'16 ~ Dec'16)";
        }

//        filterList.add("year");
        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        strQuery = StringUtils.replaceString(strQuery, "!quarter!", year, true);
        String where_condtion = service.buildWherecondition(filterList, request);

        String strDealer = request.getParameter("dealer");
        if (strDealer != null && !strDealer.equalsIgnoreCase("ALL")) {
            strQuery = StringUtils.replaceString(strQuery, "!dealer!", strDealer, true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!dealer!", "dealer like '" + strDealer + "'", true);
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!dealer!", "%", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!dealer!", " 1=1 ", true);
        }

        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strCountQuery = StringUtils.replaceString(strCountQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.load2Vals(strQuery, "Wave", "wave", "Score", "Score");

        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();

        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String wave = (String) tmpJson.get("Wave");
            String Score = (String) tmpJson.get("Score");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                wave = service.getTranslateString(wave, langCode);
                System.out.println("wave:" + wave);
            }
            addingJson.put("Wave", wave);
            addingJson.put("Score", Score);
            finalArray.add(addingJson);
        }

        String count = dao.loadString(strCountQuery);
        JSONObject obj = new JSONObject();
        obj.put("samplecount", count);
        obj.put("data", finalArray);
        out.println(obj.toJSONString());
//        out.println(jsonArray.toJSONString());
        return;
    }

    public void getDealerScoresMonth(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDealerScoresMonth'");
        String strQuery = queryRecord.getQuery();
        String strCountQuery = queryRecord.getCountquery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

        filterList.add("zone");
        filterList.add("region");
        filterList.add("dealer");
        String month = request.getParameter("month");
        if (month == null || month.equalsIgnoreCase("Study Total")) {
            month = "Dec'16";
        }

//        filterList.add("year");
        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        strQuery = StringUtils.replaceString(strQuery, "!year!", month, true);
        String where_condtion = service.buildWherecondition(filterList, request);

        String strDealer = request.getParameter("dealer");
        if (strDealer != null && !strDealer.equalsIgnoreCase("ALL")) {
            strQuery = StringUtils.replaceString(strQuery, "!dealer!", strDealer, true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!dealer!", "dealer like '" + strDealer + "'", true);
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!dealer!", "%", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!dealer!", " 1=1 ", true);
        }

        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strCountQuery = StringUtils.replaceString(strCountQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.load2Vals(strQuery, "Wave", "wave", "Score", "Score");

        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();

        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String wave = (String) tmpJson.get("Wave");
            String Score = (String) tmpJson.get("Score");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                wave = service.getTranslateString(wave, langCode);
                System.out.println("wave:" + wave);
            }
            addingJson.put("Wave", wave);
            addingJson.put("Score", Score);
            finalArray.add(addingJson);
        }

        String count = dao.loadString(strCountQuery);
        JSONObject obj = new JSONObject();
        obj.put("samplecount", count);
        obj.put("data", finalArray);
        out.println(obj.toJSONString());
//        out.println(jsonArray.toJSONString());
        return;
    }

    public void getDealerFactorScores(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDealerFactorScores'");
        String strQuery = queryRecord.getQuery();
         String strCountQuery = queryRecord.getCountquery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();
        List<String> bestfilterList = new ArrayList<String>();

        filterList.add("dealergroup");
        filterList.add("region");
        filterList.add("dealer");
        filterList.add("year");
      
       
          bestfilterList.add("year");

        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        String where_condtion = service.buildWherecondition(filterList, request);
        String best_where_condtion = service.buildWherecondition(bestfilterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strBestWhereCluse, best_where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);
       strCountQuery = StringUtils.replaceString(strCountQuery, service.strWhereCluse, where_condtion, true);
        String strBestString = service.getBestString(request);
        String strWorstString = service.getWorstString(request);
        String strDealer = request.getParameter("dealer");
         if (strDealer != null && !strDealer.equalsIgnoreCase("ALL")) {
            strQuery = StringUtils.replaceString(strQuery, "!dealer!", strDealer, true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!dealer!", "dealer like '" + strDealer + "'", true);
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!dealer!", "%", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!dealer!", " 1=1 ", true);
        }

        JSONArray jsonArray = dao.load4Vals(strQuery, "Factor", "Factor", "Score", "score", "BestScore", "maxscore", "WorstScore", "minscore");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();

        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String factor = (String) tmpJson.get("Factor");
            String factorname = factor;
            String Score = (String) tmpJson.get("Score");
            String bestscore = (String) tmpJson.get("BestScore");
            String worstscore = (String) tmpJson.get("WorstScore");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                factor = service.getTranslateString(factor, langCode);
                System.out.println("Dealer:" + factor);
            }
            addingJson.put("name", factor);
            addingJson.put("factorname", factorname);
            addingJson.put("bartitle", Score);
            addingJson.put("y", Integer.parseInt(Score));
            addingJson.put("tooltip", factor + "<br>Factor Score: <b>" + Score + "</b>");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                strBestString = service.getTranslateString(strBestString, langCode);
                strWorstString = service.getTranslateString(strWorstString, langCode);
            }
            addingJson.put("scatter", strBestString);
            if (bestscore != null && !bestscore.equalsIgnoreCase("0") && !bestscore.equalsIgnoreCase("null")) {
                addingJson.put("svalue", Integer.parseInt(bestscore));
            } else {
                addingJson.put("svalue", null);
            }
            addingJson.put("scatter1", strWorstString);
            if (worstscore != null && !worstscore.equalsIgnoreCase("0") && !worstscore.equalsIgnoreCase("null")) {
                addingJson.put("svalue1", Integer.parseInt(worstscore));
            } else {
                addingJson.put("svalue1", null);
            }
            finalArray.add(addingJson);
        }

       String count = dao.loadString(strCountQuery);
        JSONObject obj = new JSONObject();
        obj.put("samplecount", count);
        obj.put("data", finalArray);
        out.println(obj.toJSONString());
        return;
    }

    public void getDealerSOPTopBottom(HttpServletRequest request, HttpServletResponse response) throws Exception {

        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDealerSOPTopBottom'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

        filterList.add("zone");
        filterList.add("region");
        filterList.add("dealer");
        filterList.add("year");
        filterList.add("biannual");
        filterList.add("month");
        filterList.add("dealergroup");

        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.load3Vals(strQuery, "Attribute", "Attribute", "Score", "score", "TopOrBottom", "top_or_bottom");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        JSONArray Attributes = new JSONArray();
        JSONArray Scores = new JSONArray();
        JSONArray TopORBottoms = new JSONArray();
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String Attribute = (String) tmpJson.get("Attribute");
            String Score = (String) tmpJson.get("Score");
              String tipkey = "KPI Score";
            if(Score== null){
                  Score = "0";
            }
            String toporbootom = (String) tmpJson.get("TopOrBottom");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                Attribute = service.getTranslateString(Attribute, langCode);
                 tipkey = service.getTranslateString(tipkey, langCode);
                System.out.println("Attribute:" + Attribute);
            }
            addingJson.put("name", Attribute);
            addingJson.put("bartitle", Score + "%");
            addingJson.put("y", Integer.parseInt(Score));
//            addingJson.put("tooltip", Attribute + " <br>SOP Score <b>" + Score + "</b>");
            addingJson.put("tooltip", Attribute + " : <b>" + Score + "</b>");
            if (toporbootom.equalsIgnoreCase("Top")) {
                addingJson.put("color", "#7E57C2");
            } else if (toporbootom.equalsIgnoreCase("Bottom")) {
                addingJson.put("color", "#757575");
            }
            finalArray.add(addingJson);
        }

        out.println(finalArray.toJSONString());
        return;
    }

    public void getDealerFactorTrend(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDealerFactorTrend'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

        filterList.add("zone");
        filterList.add("region");
        filterList.add("dealer");
        filterList.add("year");
        filterList.add("biannual");
        filterList.add("month");

        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        String where_condtion = service.buildWherecondition(filterList, request);

        String strDealer = request.getParameter("dealer");
        if (strDealer != null && !strDealer.equalsIgnoreCase("ALL")) {
            strQuery = StringUtils.replaceString(strQuery, "!dealer!", strDealer, true);
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!dealer!", "%", true);
        }

        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.load3Vals(strQuery, "period", "period", "factor_score", "factor_score", "bestscore", "bestscore");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();

        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject jsonobj = new JSONObject();
            String Period = (String) tmpJson.get("period");
            String factor_score = (String) tmpJson.get("factor_score");
            String bestscore = (String) tmpJson.get("bestscore");

            if (bestscore == null) {
                bestscore = "0";
            }
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                Period = service.getTranslateString(Period, langCode);
                System.out.println("Period:" + Period);
            }
            jsonobj.put("name", Period);
            jsonobj.put("bartitle", factor_score);
            jsonobj.put("y", Integer.parseInt(factor_score));
            jsonobj.put("tooltip", Period + "<br>Factor Score : <b>" + factor_score + "</b>");
            jsonobj.put("svalue", Integer.parseInt(bestscore));
            finalArray.add(jsonobj);
        }

        out.println(finalArray.toJSONString());
        return;
    }

    public void getDissatisfied(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDissatisfied'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

        filterList.add("attributeFactor");
        filterList.add("dealer");
        filterList.add("year");
        filterList.add("biannual");
        filterList.add("month");

        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.load3Vals(strQuery, "Attribute", "Attribute", "Score", "score", "BestScore", "BestScore");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();

        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String Attribute = (String) tmpJson.get("Attribute");
            String Score = (String) tmpJson.get("Score");
            String bestscore = (String) tmpJson.get("BestScore");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                Attribute = service.getTranslateString(Attribute, langCode);
                System.out.println("Dealer:" + Attribute);
            }
            addingJson.put("name", Attribute);
            addingJson.put("bartitle", Score + "%");
            addingJson.put("y", Integer.parseInt(Score));
            addingJson.put("tooltip", Attribute + "<br>Factor Score: <b>" + Score + "</b>");
            finalArray.add(addingJson);
        }

        out.println(finalArray.toJSONString());
        return;
    }

    public void getAttributeTrendByWave(HttpServletRequest request, HttpServletResponse response) {
        try {
            PrintWriter out = response.getWriter();
            LIReportquirescsiService queryService = new LIReportquirescsiService();

            String factor = request.getParameter("factor");

            LIReportquirescsiRecord queryRecord=new LIReportquirescsiRecord();
             List<String> filterList = new ArrayList<String>();
//             if(factor.equalsIgnoreCase("CSI")){
//                  System.out.println("iffffffffffffff");
//                queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getAttributeTrendByWaveTop5'");
//              }else{
                queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getAttributeTrendByWave'");   
                 filterList.add("attributeFactor");              
//              }
            String periodSwitch = request.getParameter("period");
 

            if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Year")) {
                if (factor != null && factor.equalsIgnoreCase("")) {
                    queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getAttributeTrendByWaveYear'");
                } else {
                    queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getAttributeTrendByWaveYear'");
                }
//            } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Bi-Annual Period")) {
//                if (csiswitch != null && csiswitch.equalsIgnoreCase("")) {
//                    queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getAttributeTrendByWaveBi'");
//                } else {
//                    queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getAttributeTrendByWaveNotBi'");
//                }
//            } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Month")) {
//                if (csiswitch != null && csiswitch.equalsIgnoreCase("")) {
//                    queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getAttributeTrendByWaveMonth'");
//                } else {
//                    queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getAttributeTrendByWaveNotMonth'");
//                }
            }
            String strQuery = queryRecord.getQuery();
            LIDAO dao = new LIDAO();
            LIService service = new LIService();
           
            filterList.add("dealer");
//                   filterList.add("year");
//            filterList.add("biannual");
           
            //***************************************************************************************
            // commented by varma on  28 mar 2017 not clear why we are replacing langcode in query.
//            if (!langCode.equalsIgnoreCase("EN")) {
//                System.out.println("Lang Code :" + langCode);
//                strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
//            }

            String where_condtion = service.buildWherecondition(filterList, request);
            strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
            JSONArray jsonArray = dao.load4Vals(strQuery, "period", "period", "attribute", "attribute", "score", "score", "bestscore", "bestscore");
            System.out.println(jsonArray.toJSONString());
            Iterator i = jsonArray.iterator();
            JSONArray finalArray = new JSONArray();
            LinkedHashMap periods = new LinkedHashMap();
            LinkedHashMap attributes = new LinkedHashMap();
            LinkedHashMap scores = new LinkedHashMap();
            LinkedHashMap bestscores = new LinkedHashMap();

            while (i.hasNext()) {
                JSONObject tmpJson = (JSONObject) i.next();
                String period = (String) tmpJson.get("period");
                String attribute = (String) tmpJson.get("attribute");
                String score = (String) tmpJson.get("score");
                String bestscore = (String) tmpJson.get("bestscore");
                System.out.println(tmpJson.toJSONString());
                periods.put(period, period);
                attributes.put(attribute.trim(), attribute.trim());
                scores.put(period + "-" + attribute.trim(), score);
                bestscores.put(period + "-" + attribute.trim(), bestscore);
            }
            Iterator iattribute = attributes.keySet().iterator();

//          
            while (iattribute.hasNext()) {
                JSONObject jsonbject = new JSONObject();
                String attribute = (String) iattribute.next();
                String attributetemp = attribute;
                System.out.println("timeline:" + attribute);
                if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
//                     attributetemp=" "+attributetemp;
                    attributetemp = service.getTranslateString(attribute, langCode);
                    System.out.println("timeline:" + attribute);
                }
                jsonbject.put("name", attributetemp);
                Iterator itimeline = periods.keySet().iterator();
                JSONArray tmpJsonArray = new JSONArray();
                while (itimeline.hasNext()) {
                    JSONObject tmpJsonObj = new JSONObject();
                    String timeline = (String) itimeline.next();
                    String score = (String) scores.get(timeline + "-" + attribute);
                    if (score != null) {
                        tmpJsonObj.put("y", Double.parseDouble(score));
                    } else {
                        tmpJsonObj.put("y", Double.parseDouble("0.0"));
                    }
                    tmpJsonObj.put("bartitle", score);
                    if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                        timeline = "" + timeline;
                        timeline = service.getTranslateString(timeline, langCode);
                        System.out.println("attribute:" + timeline);
                    }
                    tmpJsonObj.put("name", timeline.trim());
                    tmpJsonObj.put("tooltip", "<b>" + timeline + "<br>" + score + "</b>");
                    tmpJsonArray.add(tmpJsonObj);
                }
                System.out.println(tmpJsonArray.toJSONString());
                jsonbject.put("data", tmpJsonArray);
                finalArray.add(jsonbject);
            }

            out.println(finalArray.toJSONString());
            return;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getDealerAttributeTrend(HttpServletRequest request, HttpServletResponse response) {
        try {
            PrintWriter out = response.getWriter();
            LIReportquirescsiService queryService = new LIReportquirescsiService();
            LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDealerAttributeTrend'");
            String strQuery = queryRecord.getQuery();
            LIDAO dao = new LIDAO();
            LIService service = new LIService();
            List<String> filterList = new ArrayList<String>();
            filterList.add("zone");
            filterList.add("region");
            filterList.add("dealer");
            filterList.add("factor");
            filterList.add("year");
            filterList.add("biannual");
            filterList.add("month");

            if (!langCode.equalsIgnoreCase("EN")) {
                System.out.println("Lang Code :" + langCode);
                strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
            }

            String where_condtion = service.buildWherecondition(filterList, request);
            String strDealer = request.getParameter("dealer");
            if (strDealer != null && !strDealer.equalsIgnoreCase("ALL")) {
                strQuery = StringUtils.replaceString(strQuery, "!dealer!", strDealer, true);
            } else {
                strQuery = StringUtils.replaceString(strQuery, "!dealer!", "%", true);
            }
            strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
            JSONArray jsonArray = dao.load4Vals(strQuery, "period", "period", "attribute", "attribute", "score", "score", "bestscore", "bestscore");
            System.out.println(jsonArray.toJSONString());
            Iterator i = jsonArray.iterator();
            JSONArray finalArray = new JSONArray();
            LinkedHashMap periods = new LinkedHashMap();
            LinkedHashMap attributes = new LinkedHashMap();
            LinkedHashMap scores = new LinkedHashMap();
            LinkedHashMap bestscores = new LinkedHashMap();

            while (i.hasNext()) {
                JSONObject tmpJson = (JSONObject) i.next();
                String period = (String) tmpJson.get("period");
                String attribute = (String) tmpJson.get("attribute");
                String score = (String) tmpJson.get("score");
                String bestscore = (String) tmpJson.get("bestscore");
                System.out.println(tmpJson.toJSONString());
                periods.put(period, period);
                attributes.put(attribute.trim(), attribute.trim());
                scores.put(period + "-" + attribute.trim(), score);
                bestscores.put(period + "-" + attribute.trim(), bestscore);
            }
            Iterator itimeline = periods.keySet().iterator();

            while (itimeline.hasNext()) {
                JSONObject jsonbject = new JSONObject();
                String timeline = (String) itimeline.next();
                System.out.println("timeline:" + timeline);
                if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                    timeline = service.getTranslateString(timeline, langCode);
                    System.out.println("timeline:" + timeline);
                }
                jsonbject.put("name", timeline);
                Iterator iattribute = attributes.keySet().iterator();
                JSONArray tmpJsonArray = new JSONArray();
                while (iattribute.hasNext()) {
                    JSONObject tmpJsonObj = new JSONObject();
                    String attribute = (String) iattribute.next();
                    String score = (String) scores.get(timeline + "-" + attribute);
                    if (score != null) {
                        tmpJsonObj.put("y", Double.parseDouble(score));
                    } else {
                        tmpJsonObj.put("y", Double.parseDouble("0.0"));
                    }
                    tmpJsonObj.put("bartitle", score);
                    if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                        attribute = service.getTranslateString(attribute, langCode);
                        System.out.println("attribute:" + attribute);
                    }
                    tmpJsonObj.put("name", attribute.trim());
                    tmpJsonObj.put("tooltip", "<b>" + attribute + "<br>" + score + "</b>");
                    tmpJsonArray.add(tmpJsonObj);
                }
                System.out.println(tmpJsonArray.toJSONString());
                jsonbject.put("data", tmpJsonArray);
                finalArray.add(jsonbject);
            }

            out.println(finalArray.toJSONString());
            return;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //////////////
      public void getDelighted(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        String factor=request.getParameter("factor");
         List<String> filterList = new ArrayList<String>();
        List<String> bestfilterList = new ArrayList<String>();
         LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDelighted'");
//        if(factor.equalsIgnoreCase(PropertyUtil.getProperty("factor")))
//        {
//            queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDelightedTop5Bottom5snap'");
//        }
//        else
//        {
//            filterList.add("attributeFactor");
//            bestfilterList.add("attributeFactor");
//            
//            queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDelightedDealerSnap'");
//        }
       
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

       filterList.add("attributeFactor");
           

       // filterList.add("dealergroup");
        filterList.add("region");
//        filterList.add("model");
        filterList.add("year");
        filterList.add("dealer");
        filterList.add("dealergroup");
//        filterList.add("month");
        //filterList.add("regiona");
        //filterList.add("quarter");
        //filterList.add("city");

        
       // bestfilterList.add("region");
       // bestfilterList.add("dealergroup");
        bestfilterList.add("year");
         bestfilterList.add("attributeFactor");
       // bestfilterList.add("month");
     
       // bestfilterList.add("quarter");
      

        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        String where_condtion = service.buildWherecondition(filterList, request);
        String best_where_condtion = service.buildWherecondition(bestfilterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strBestWhereCluse, best_where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
  JSONArray jsonArray = dao.load8Vals(strQuery, "Attribute", "Attribute", "Score", "score", "BestScore", "BestScore", "WorstScore", "WorstScore","second_score","second_score","mean","mean","bestscorepos","bestscorepos","worstscorepos","worstscorepos");
         Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        JSONObject config=new JSONObject();
        ArrayList series1 = new ArrayList();
        ArrayList series2 = new ArrayList();
        ArrayList categories = new ArrayList();
        ArrayList best = new ArrayList();
        ArrayList worst = new ArrayList();
     
      
        String strBestString = service.getBestString(request);
        String strWorstString = service.getWorstString(request);
        String region = request.getParameter("region");
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            JSONObject addingJson2 = new JSONObject();
            JSONObject bestpos = new JSONObject();
            JSONObject worstpos = new JSONObject();
 
            String Attribute = (String) tmpJson.get("Attribute");
            String Score = (String) tmpJson.get("Score");
            String bestscore = (String) tmpJson.get("BestScore");
            String worstscore = (String) tmpJson.get("WorstScore");
            String mean = (String) tmpJson.get("mean");
            String second_score = (String) tmpJson.get("second_score");
            Double bestscorepos = Double.parseDouble((String) tmpJson.get("bestscorepos"));
            Double worstscorepos = Double.parseDouble((String) tmpJson.get("worstscorepos"));
   
            System.out.println("scond_score"+second_score);
            Attribute = Attribute;
            strBestString = strBestString;
            strWorstString = strWorstString;
               if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
//                 Attribute= " "+Attribute;
                Attribute = service.getTranslateString(Attribute, langCode);
                strBestString = service.getTranslateString(strBestString, langCode);
                strWorstString = service.getTranslateString(strWorstString, langCode);
                System.out.println("Dealer:" + Attribute);
            }
            
             
            if (bestscore != null && !bestscore.equalsIgnoreCase("0") && !bestscore.equalsIgnoreCase("null")) {
                bestpos.put("y",bestscorepos);
                 bestpos.put("label",bestscore);
                best.add(bestpos);
            } else {
                best.add(null);
            }
          
           
            if (worstscore != null && !worstscore.equalsIgnoreCase("0") && !worstscore.equalsIgnoreCase("null")) {
                 worstpos.put("y",worstscorepos);
                 worstpos.put("label",worstscore);
                worst.add(worstpos);
            } else {
                worst.add(null);
            }
             
            categories.add(Attribute);
          if (Score != null && !Score.equalsIgnoreCase("0") && !Score.equalsIgnoreCase("null")) {
                 addingJson2.put('y',Integer.parseInt(Score));
            } else {
                    addingJson2.put('y',null);
            }
            
            addingJson2.put("label",Score+'%');
            addingJson2.put("bartitle", Attribute + "<br>: <b>" + Score + "%</b>");
            series2.add(addingJson2);
             if (second_score != null && !second_score.equalsIgnoreCase("0") && !second_score.equalsIgnoreCase("null")) {
                 addingJson.put("y",Integer.parseInt(second_score));
            } else {
                 addingJson.put("y",null);
            }
           if(second_score.equalsIgnoreCase("0")||mean.equalsIgnoreCase("0")){
                addingJson.put("label","-");
           }
           else
           {
            addingJson.put("label",mean);
           }
            series1.add(addingJson);
           
          

            
           
           
        
        }
        
      
         HashMap seriesdata1=new HashMap();
         HashMap seriesdata2=new HashMap();
         HashMap worstdata=new HashMap();
         HashMap bestdata=new HashMap();
         seriesdata1.put("name","FirstSeries");
         seriesdata1.put("data",series1);
         seriesdata2.put("name","SecondSeries");
         seriesdata2.put("data",series2);
         worstdata.put("name",strWorstString);
         worstdata.put("data",worst);
         bestdata.put("name",strBestString);
         bestdata.put("data",best);
         finalArray.add(seriesdata1);
         finalArray.add(seriesdata2);
         finalArray.add(bestdata);
         finalArray.add(worstdata);
        
        config.put("categories", categories);
        config.put("data",finalArray);

        
        out.println(config);
        return;
    }
////////////////////
    public void getDelightedold(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDelighted'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
 List<String> bestfilterList = new ArrayList<String>();
        List<String> filterList = new ArrayList<String>();

        filterList.add("attributeFactor");
        filterList.add("region");
        filterList.add("model");
        filterList.add("year");
         filterList.add("dealer");
        filterList.add("biannual");
        filterList.add("month");
bestfilterList.add("year");
        bestfilterList.add("attributeFactor");
        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        String where_condtion = service.buildWherecondition(filterList, request);
        String best_where_condtion = service.buildWherecondition(bestfilterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strBestWhereCluse, best_where_condtion, true);   
     
        JSONArray jsonArray = dao.load5Vals(strQuery, "Attribute", "Attribute", "Score", "score", "BestScore", "BestScore", "WorstScore", "WorstScore","meanscore","meanscore");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        

        String strBestString = service.getBestString(request);
        String strWorstString = service.getWorstString(request);

        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String Attribute = (String) tmpJson.get("Attribute");
            String Score = (String) tmpJson.get("Score");
            String bestscore = (String) tmpJson.get("BestScore");
            String worstscore = (String) tmpJson.get("WorstScore");
            String meanscore = (String) tmpJson.get("meanscore");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
//                 Attribute= " "+Attribute;
                Attribute = service.getTranslateString(Attribute, langCode);
                strBestString = service.getTranslateString(strBestString, langCode);
                strWorstString = service.getTranslateString(strWorstString, langCode);
                System.out.println("Dealer:" + Attribute);
            }
            addingJson.put("scatter", strBestString);
            if (bestscore != null && !bestscore.equalsIgnoreCase("0") && !bestscore.equalsIgnoreCase("null")) {
                addingJson.put("svalue", Integer.parseInt(bestscore));
            } else {
                addingJson.put("svalue", null);
            }
            addingJson.put("scatter1", strWorstString);
            if (worstscore != null && !worstscore.equalsIgnoreCase("0") && !worstscore.equalsIgnoreCase("null")) {
                addingJson.put("svalue1", Integer.parseInt(worstscore));
            } else {
                addingJson.put("svalue1", null);
            }
            addingJson.put("name", Attribute);
            addingJson.put("bartitle", Score + "%");
            addingJson.put("y", Integer.parseInt(Score));
            addingJson.put("tooltip", Attribute + "<br>Factor Score: <b>" + Score + "</b>");
            addingJson.put("mean",meanscore);
            finalArray.add(addingJson);
        }

        out.println(finalArray.toJSONString());
        return;
    }

}
