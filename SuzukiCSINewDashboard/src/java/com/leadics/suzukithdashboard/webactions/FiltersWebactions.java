/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.suzukithdashboard.webactions;

import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.service.LIReportquirescsiService;
import com.leadics.suzukithdashboard.to.LIReportquirescsiRecord;
import com.leadics.utils.LogUtils;
import com.leadics.utils.PropertyUtil;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author varma.sagi
 */
public class FiltersWebactions {

    String langCode = "EN";

    static LogUtils logger = new LogUtils(FiltersWebactions.class.getName());

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
// Content-type: application/json; charset=utf-8 
        try (PrintWriter out = response.getWriter()) {
            try {
                String lang = request.getParameter("langCode");
                if (lang != null) {
                    langCode = lang;
                }
                if (!StringUtils.isNullOrEmpty(actionType)) {

                    if (actionType.equals("getYear")) {
                        getYears(request, response);
                        return;
                    }

                    if (actionType.equals("getYearfilter")) {
                        getYearsfiltered(request, response);
                        return;
                    }
                    if (actionType.equals("getContent")) {
                        getContents(request, response);
                        return;
                    }

                    if (actionType.equals("getBiannual")) {
                        getBiannual(request, response);
                        return;
                    }

                    if (actionType.equals("getModels")) {
                        getModels(request, response);
                        return;
                    }
                    if (actionType.equals("getRegion")) {
                        getRegion(request, response);
                        return;
                    }
                    if (actionType.equals("getdgRegion")) {
                        getdgRegion(request, response);
                        return;
                    }
                    if (actionType.equals("getMonth")) {
                        getMonth(request, response);
                        return;
                    }

                    if (actionType.equals("getDealers")) {
                        getDealers(request, response);
                        return;
                    }

                    if (actionType.equals("getfilteredDealers")) {
                        getfilteredDealers(request, response);
                        return;
                    }

                    if (actionType.equals("getFactors")) {
                        getFactors(request, response);
                        return;
                    }
                    if (actionType.equals("getSOPAttributes")) {
                        getSOPAttributes(request, response);
                        return;
                    }
                    if (actionType.equals("getPeriods")) {
                        getPeriods(request, response);
                        return;
                    }
                    if (actionType.equals("getZone")) {
                        getZone(request, response);
                        return;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    public void getYears(HttpServletRequest request, HttpServletResponse response) throws Exception {

        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getYears'");
        String strQuery = queryRecord.getQuery();
        String year = PropertyUtil.getProperty("yearfilter");

        if (year != null) {
            strQuery = strQuery.replaceAll("<<WHERE CONDITION>>", "AND year >= " + year);
        } else {
            strQuery = strQuery.replaceAll("<<WHERE CONDITION>>", "");
        }
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        JSONArray jsonArray = dao.loadColumnListAsJSON(strQuery, "Year", false);
        //added
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            String Name = (String) i.next();
            String value = Name;
            JSONObject Obj1 = new JSONObject();
            Obj1.put("name", Name);
            String langCode = request.getParameter("langCode");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                System.out.println("Lang Code :" + langCode);
                value = service.getTranslateString(Name, langCode);
                if (value == null) {
                    value = Name;
                }
            }
            System.out.println("value : " + value);
            Obj1.put("value", value);
            finalArray.add(Obj1);
        }

        //adding complete
        out.println(finalArray.toJSONString());
        return;
    }

    public void getYearsfiltered(HttpServletRequest request, HttpServletResponse response) throws Exception {

        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getYearsfiltered'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        JSONArray jsonArray = dao.loadColumnListAsJSON(strQuery, "Year", false);
        //added
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            String Name = (String) i.next();
            String value = Name;
            JSONObject Obj1 = new JSONObject();
            Obj1.put("name", Name);
            String langCode = request.getParameter("langCode");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                System.out.println("Lang Code :" + langCode);
                value = service.getTranslateString(Name, langCode);
                if (value == null) {
                    value = Name;
                }
            }
            System.out.println("value : " + value);
            Obj1.put("value", value);
            finalArray.add(Obj1);
        }

        //adding complete
        out.println(finalArray.toJSONString());
        return;
    }

    public void getBiannual(HttpServletRequest request, HttpServletResponse response) throws Exception {

        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getBiannual'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        List<String> filterList = new ArrayList<String>();
        filterList.add("year");
        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.loadColumnListAsJSON(strQuery, "Biannual", false);
        //added
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            String Name = (String) i.next();
            String value = Name;
            JSONObject Obj1 = new JSONObject();
            Obj1.put("name", Name);
            if (!langCode.equalsIgnoreCase("EN")) {
                System.out.println("Lang Code :" + langCode);
                value = service.getTranslateString(Name, langCode);
                if (value == null) {
                    value = Name;
                }
            }
            System.out.println("value : " + value);
            Obj1.put("value", value);
            finalArray.add(Obj1);
        }

        //adding complete
        out.println(finalArray.toJSONString());
        return;
    }

    public void getMonth(HttpServletRequest request, HttpServletResponse response) throws Exception {

        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getMonth'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        List<String> filterList = new ArrayList<String>();
        filterList.add("Year");
        filterList.add("biannual");
        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.loadColumnListAsJSON(strQuery, "Month", false);
        //added
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            String Name = (String) i.next();
            String value = Name;
            JSONObject Obj1 = new JSONObject();
            Obj1.put("name", Name);
            if (!langCode.equalsIgnoreCase("EN")) {
                System.out.println("Lang Code :" + langCode);
                value = service.getTranslateString(Name, langCode);
                if (value == null) {
                    value = Name;
                }
            }
            System.out.println("value : " + value);
            Obj1.put("value", value);
            finalArray.add(Obj1);
        }

        //adding complete
        out.println(finalArray.toJSONString());
        return;
    }

    public void getdealergroup(HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setContentType("text/html; charset=UTF-8");
//        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDealergroup'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        List<String> filterList = new ArrayList<String>();
        filterList.add("region");
        filterList.add("yeargreater");
        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.loadColumnListAsJSON(strQuery, "dealergroup", false);
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            String Name = (String) i.next();
            String value = Name;
            JSONObject Obj1 = new JSONObject();
            Obj1.put("name", Name);
            if (!langCode.equalsIgnoreCase("EN")) {
                System.out.println("Lang Code :" + langCode);
                value = service.getTranslateString(Name, langCode);
                if (value == null) {
                    value = Name;
                }
            }
            System.out.println("value : " + value);
            Obj1.put("value", value);
            finalArray.add(Obj1);
        }

        //adding complete
        out.println(finalArray.toJSONString());
        return;
    }

    public void getZone(HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getZone'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        List<String> filterList = new ArrayList<String>();
        filterList.add("year");
        filterList.add("biannual");
        filterList.add("month");
        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.loadColumnListAsJSON(strQuery, "zone", false);
        //added
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            String Name = (String) i.next();
            String value = Name;
            JSONObject Obj1 = new JSONObject();
            Obj1.put("name", Name);
            if (!langCode.equalsIgnoreCase("EN")) {
                System.out.println("Lang Code :" + langCode);
                value = service.getTranslateString(Name, langCode);
                if (value == null) {
                    value = Name;
                }
            }
            System.out.println("value : " + value);
            Obj1.put("value", value);
            finalArray.add(Obj1);
        }

        //adding complete
        out.println(finalArray.toJSONString());
        return;
    }

    public void getRegion(HttpServletRequest request, HttpServletResponse response) throws Exception {

        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getRegion'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        List<String> filterList = new ArrayList<String>();
        filterList.add("year");
        filterList.add("biannual");
        filterList.add("month");
        filterList.add("zone");
        System.out.println("zone is " + request.getParameter("zone"));

        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.loadColumnListAsJSON(strQuery, "Region", false);
        //added
        String langCode = request.getParameter("langCode");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            String Name = (String) i.next();
            String value = Name;
            JSONObject Obj1 = new JSONObject();
            Obj1.put("name", Name);
            if (!langCode.equalsIgnoreCase("EN")) {
                System.out.println("Lang Code :" + langCode);
                value = service.getTranslateString(Name, langCode);
                if (value == null) {
                    value = Name;
                }
            }
            System.out.println("value : " + value);
            Obj1.put("value", value);
            finalArray.add(Obj1);
        }

        //adding complete
        out.println(finalArray.toJSONString());
        return;
    }

    public void getdgRegion(HttpServletRequest request, HttpServletResponse response) throws Exception {

        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getRegiondg'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        List<String> filterList = new ArrayList<String>();
        filterList.add("zone");
        filterList.add("yeargreater");
        filterList.add("dealergroup");
        System.out.println("dealerfroup is:: " + request.getParameter("dealergroup"));

        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.loadColumnListAsJSON(strQuery, "Region", false);
        //added
        String langCode = request.getParameter("langCode");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            String Name = (String) i.next();
            String value = Name;
            JSONObject Obj1 = new JSONObject();
            Obj1.put("name", Name);
            if (!langCode.equalsIgnoreCase("EN")) {
                System.out.println("Lang Code :" + langCode);
                value = service.getTranslateString(Name, langCode);
                if (value == null) {
                    value = Name;
                }
            }
            System.out.println("value : " + value);
            Obj1.put("value", value);
            finalArray.add(Obj1);
        }

        //adding complete
        out.println(finalArray.toJSONString());
        return;
    }

    public void getModels(HttpServletRequest request, HttpServletResponse response) throws Exception {

        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getModels'");
        String strQuery = queryRecord.getQuery();

        String year = PropertyUtil.getProperty("yearfilter");

        if (year != null) {
            strQuery = strQuery.replaceAll("<<WHERE CONDITION>>", "AND year >= " + year);
        } else {
            strQuery = strQuery.replaceAll("<<WHERE CONDITION>>", "");
        }
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        JSONArray jsonArray = dao.loadColumnListAsJSON(strQuery, "Model", false);
        //added
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            String Name = (String) i.next();
            String value = Name;
            JSONObject Obj1 = new JSONObject();
            Obj1.put("name", Name);
            if (!langCode.equalsIgnoreCase("EN")) {
                System.out.println("Lang Code :" + langCode);
                value = service.getTranslateString(Name, langCode);
                if (value == null) {
                    value = Name;
                }
            }
            System.out.println("value : " + value);
            Obj1.put("value", value);
            finalArray.add(Obj1);
        }

        //adding complete
        out.println(finalArray.toJSONString());
        return;
    }

    public void getContents(HttpServletRequest request, HttpServletResponse response) throws Exception {

        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getContents'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        JSONArray jsonArray = dao.loadColumnListAsJSON(strQuery, "Model", false);
        //added
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            String Name = (String) i.next();
            String value = Name;
            JSONObject Obj1 = new JSONObject();
            Obj1.put("name", Name);
            if (!langCode.equalsIgnoreCase("EN")) {
                System.out.println("Lang Code :" + langCode);
                value = service.getTranslateString(Name, langCode);
                if (value == null) {
                    value = Name;
                }
            }
            System.out.println("value : " + value);
            Obj1.put("value", value);
            finalArray.add(Obj1);
        }

        //adding complete
        out.println(finalArray.toJSONString());
        return;
    }

    public void getDealers(HttpServletRequest request, HttpServletResponse response) throws Exception {

        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDealers'");
        String strQuery = queryRecord.getQuery();

        String year = PropertyUtil.getProperty("yearfilter");

        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        List<String> filterList = new ArrayList<String>();
        filterList.add("year");
        filterList.add("biannual");
        filterList.add("month");
        filterList.add("zone");
        filterList.add("region");
        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.loadColumnListAsJSON(strQuery, "dealer", false);
        //added
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            String Name = (String) i.next();
            String value = Name;
            JSONObject Obj1 = new JSONObject();
            Obj1.put("name", Name);
            if (!langCode.equalsIgnoreCase("EN")) {
                System.out.println("Lang Code :" + langCode);
                value = service.getTranslateString(Name, langCode);
                if (value == null) {
                    value = Name;
                }
            }
            System.out.println("value : " + value);
            Obj1.put("value", value);
            finalArray.add(Obj1);
        }

        //adding complete
        out.println(finalArray.toJSONString());
        return;
    }

    public void getfilteredDealers(HttpServletRequest request, HttpServletResponse response) throws Exception {

        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();

        String tablename = request.getParameter("tablename");
//        LIReportquirescsiService queryService = new LIReportquirescsiService();
//        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDealers'");
//        String strQuery = queryRecord.getQuery();

        String dealers = null;
        if (tablename.equalsIgnoreCase("redalert")) {
            dealers = ("SELECT distinct dealer  FROM redalert2017thversion_jan17th WHERE 1=1  <<WHERE_CONDITION>>");
        } else if (tablename.equalsIgnoreCase("othersfeedback")) {
            dealers = ("SELECT distinct dealer  FROM otherfeedbackengversion_jan17eng WHERE 1=1  <<WHERE_CONDITION>>");
        }

        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        List<String> filterList = new ArrayList<String>();

        filterList.add("zone");
        filterList.add("region");
        filterList.add("yeargreater");
        String where_condtion = service.buildWherecondition(filterList, request);
        dealers = StringUtils.replaceString(dealers, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.loadColumnListAsJSON(dealers, "dealer", false);
        //added

        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            String Name = (String) i.next();
            String value = Name;
            JSONObject Obj1 = new JSONObject();
            Obj1.put("name", Name);
            if (!langCode.equalsIgnoreCase("EN")) {
                System.out.println("Lang Code :" + langCode);
                value = service.getTranslateString(Name, langCode);
                if (value == null) {
                    value = Name;
                }
            }
            System.out.println("value : " + value);
            Obj1.put("value", value);
            finalArray.add(Obj1);
        }

        //adding complete
        out.println(finalArray.toJSONString());
        return;
    }

    public void getSOPAttributes(HttpServletRequest request, HttpServletResponse response) throws Exception {

        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getSOPAttributes'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        JSONArray jsonArray = dao.loadColumnListAsJSON(strQuery, "attributename", false);
        //added
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            String Name = (String) i.next();
            String value = Name;
            JSONObject Obj1 = new JSONObject();
            Obj1.put("name", Name);
            if (!langCode.equalsIgnoreCase("EN")) {
                System.out.println("Lang Code :" + langCode);
                value = service.getTranslateString(Name, langCode);
                if (value == null) {
                    value = Name;
                }
            }
            System.out.println("value : " + value);
            Obj1.put("value", value);
            finalArray.add(Obj1);
        }

        //adding complete
        out.println(finalArray.toJSONString());
        return;
    }

    public void getFactors(HttpServletRequest request, HttpServletResponse response) throws Exception {

        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getFactors'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        JSONArray jsonArray = dao.loadColumnListAsJSON(strQuery, "factor", false);
        //added
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            String Name = (String) i.next();
            String value = Name;
            JSONObject Obj1 = new JSONObject();
            Obj1.put("name", Name);
            if (!langCode.equalsIgnoreCase("EN")) {
                System.out.println("Lang Code :" + langCode);
                value = service.getTranslateString(Name, langCode);
                if (value == null) {
                    value = Name;
                }
            }
            System.out.println("value : " + value);
            Obj1.put("value", value);
            finalArray.add(Obj1);
        }

        //adding complete
        out.println(finalArray.toJSONString());
        return;
    }

    public void getPeriods(HttpServletRequest request, HttpServletResponse response) throws Exception {

        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getPeriods'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        List<String> filterList = new ArrayList<String>();
        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.loadColumnListAsJSON(strQuery, "dealer", false);
        //added
        String langCode = request.getParameter("langCode");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            String Name = (String) i.next();
            String value = Name;
            JSONObject Obj1 = new JSONObject();
            Obj1.put("name", Name);
            if (!langCode.equalsIgnoreCase("EN")) {
                System.out.println("Lang Code :" + langCode);
                value = service.getTranslateString(Name, langCode);
                if (value == null) {
                    value = Name;
                }
            }
            System.out.println("value : " + value);
            Obj1.put("value", value);
            finalArray.add(Obj1);
        }

        //adding complete
        out.println(finalArray.toJSONString());
        return;
    }
}
