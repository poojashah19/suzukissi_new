/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.suzukithdashboard.webactions;

import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.service.LIReportquirescsiService;
import com.leadics.suzukithdashboard.service.LIReportquiresssiService;
import com.leadics.suzukithdashboard.to.LIReportquirescsiRecord;
import com.leadics.suzukithdashboard.to.LIReportquiresssiRecord;
import com.leadics.utils.LogUtils;
import com.leadics.utils.PropertyUtil;
import com.leadics.utils.StringUtils;
import com.lowagie.text.pdf.hyphenation.TernaryTree;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author varma.sagi
 */
public class SummaryWebactions {

    String langCode = "EN";

    static LogUtils logger = new LogUtils(SummaryWebactions.class.getName());

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");

        try (PrintWriter out = response.getWriter()) {
            try {
                String lang = request.getParameter("langCode");
                if (lang != null) {
                    langCode = lang;
                }
                if (!StringUtils.isNullOrEmpty(actionType)) {
                    if (actionType.equals("RegionWise")) {
                        regionWise(request, response);
                        return;
                    }
                    if (actionType.equals("getNationalScoresYear")) {
                        getNationalScoresYear(request, response);
                        return;
                    }
                    if (actionType.equals("getNationalScoresYearBM")) {
                        getNationalScoresYearBM(request, response);
                        return;
                    }
                    if (actionType.equals("getNationalScoresBiannual")) {
                        getNationalScoresBiannual(request, response);
                        return;
                    }
                    if (actionType.equals("getNationalScoresMonth")) {
                        getNationalScoresMonth(request, response);
                        return;
                    }
                    if (actionType.equals("getSummaryRegionFactorScores")) {
                        getSummaryRegionFactorScores(request, response);
                        return;
                    }
                    if (actionType.equals("getTop5Bottom5")) {
                        getTop5Bottom5(request, response);
                        return;
                    }
                    if (actionType.equals("getTop5Bottom5Attributes")) {
                        getTop5Bottom5Attributes(request, response);
                        return;
                    }
                    if (actionType.equals("getTop5Bottom5AttributesMean")) {
                        getTop5Bottom5AttributesMean(request, response);
                        return;
                    }
                    if (actionType.equals("getTop5Bottom5SOP")) {
                        getTop5Bottom5SOP(request, response);
                        return;
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    public void getNationalScoresYear(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getNationalScoresYear'");
        String strQuery = queryRecord.getQuery();
        String strCountQuery = queryRecord.getCountquery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

        filterList.add("region");
        filterList.add("year");
        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }
        String year = request.getParameter("year");
        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, "!year!", year, true);
        strCountQuery = StringUtils.replaceString(strCountQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.load2Vals(strQuery, "wave", "wave ", "Score", "Score");

        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();

        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String wave = (String) tmpJson.get("wave");
            String Score = (String) tmpJson.get("Score");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                wave = service.getTranslateString(wave, langCode);
                System.out.println("wave:" + wave);
            }

            if (Score == null) {
                wave = "";
            }
            addingJson.put("Wave", wave);
            addingJson.put("Score", Score);
            finalArray.add(addingJson);
        }

        String count = dao.loadString(strCountQuery);
        JSONObject obj = new JSONObject();
        obj.put("samplecount", count);
        obj.put("data", finalArray);

        out.println(obj.toJSONString());
        return;
    }

    public void getNationalScoresYearBM(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getNationalScoresYear'");
        String strQuery = queryRecord.getQuery();
        String strCountQuery = queryRecord.getCountquery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

        filterList.add("dealer");
        filterList.add("model");
//        filterList.add("dealergroup");
//        filterList.add("region");

        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        //to display single region for dg present in 2 regions - govind add
        String strdealergroup = request.getParameter("dealergroup");
        strQuery = StringUtils.replaceString(strQuery, "!region!", "AND region IN (SELECT region FROM dg_region WHERE dealergroup='" + strdealergroup + "')", true);
        strCountQuery = StringUtils.replaceString(strCountQuery, "!region!", "AND region IN (SELECT region FROM dg_region WHERE dealergroup='" + strdealergroup + "')", true);

        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strCountQuery = StringUtils.replaceString(strCountQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.load2Vals(strQuery, "year", "year", "Score", "Score");

        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();

        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String wave = (String) tmpJson.get("year");
            String Score = (String) tmpJson.get("Score");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                wave = service.getTranslateString(wave, langCode);
                System.out.println("wave:" + wave);
            }

            if (Score == null) {
                wave = "";
            }
            addingJson.put("Wave", wave);
            addingJson.put("Score", Score);
            finalArray.add(addingJson);
        }

        String count = dao.loadString(strCountQuery);
        JSONObject obj = new JSONObject();
        obj.put("samplecount", count);
        obj.put("data", finalArray);

        out.println(obj.toJSONString());
        return;
    }

    public void getNationalScoresBiannual(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getNationalScoresBiannual'");
        String strQuery = queryRecord.getQuery();
        String strCountQuery = queryRecord.getCountquery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

        filterList.add("zone");
        filterList.add("region");
        filterList.add("dealer");
        filterList.add("model");

        String year = request.getParameter("biannual");
        if (year == null || year.equalsIgnoreCase("Study Total")) {
            year = "2016H2 (Jul'16 ~ Dec'16)";
        }

        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        strQuery = StringUtils.replaceString(strQuery, "!quarter!", year, true);
        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strCountQuery = StringUtils.replaceString(strCountQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.load2Vals(strQuery, "Wave", "wave", "Score", "Score");

        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();

        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String wave = (String) tmpJson.get("Wave");
            String Score = (String) tmpJson.get("Score");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                wave = service.getTranslateString(wave, langCode);
                System.out.println("wave:" + wave);
            }
            addingJson.put("Wave", wave);
            addingJson.put("Score", Score);
            finalArray.add(addingJson);
        }

        String count = dao.loadString(strCountQuery);
        JSONObject obj = new JSONObject();
        obj.put("samplecount", count);
        obj.put("data", finalArray);

        out.println(obj.toJSONString());
        return;
    }

    public void getNationalScoresMonth(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getNationalScoresMonth'");
        String strQuery = queryRecord.getQuery();
        String strCountQuery = queryRecord.getCountquery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

        filterList.add("zone");
        filterList.add("region");
        filterList.add("dealer");
        filterList.add("model");
        String month = request.getParameter("month");
        if (month == null || month.equalsIgnoreCase("Study Total") || month.equalsIgnoreCase("All")) {
            month = "Mar'17";
        }

        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        strQuery = StringUtils.replaceString(strQuery, "!year!", month, true);
        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strCountQuery = StringUtils.replaceString(strCountQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.load2Vals(strQuery, "Wave", "wave", "Score", "Score");

        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();

        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String wave = (String) tmpJson.get("Wave");
            String Score = (String) tmpJson.get("Score");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                wave = service.getTranslateString(wave, langCode);
                System.out.println("wave:" + wave);
            }
            addingJson.put("Wave", wave);
            addingJson.put("Score", Score);
            finalArray.add(addingJson);
        }

        String count = dao.loadString(strCountQuery);
        JSONObject obj = new JSONObject();
        obj.put("samplecount", count);
        obj.put("data", finalArray);

        out.println(obj.toJSONString());
        return;
    }

    public void regionWise(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='RegionWise'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

//        filterList.add("bestregion");
        filterList.add("zone");
        filterList.add("region");
        filterList.add("model");
//        filterList.add("dealergroupregion");
        filterList.add("year");
        String dealergroup = request.getParameter("dealergroup");
        System.out.println("query----->" + strQuery);

        if (dealergroup != null && !dealergroup.equalsIgnoreCase("ALL") && !dealergroup.equalsIgnoreCase("Study Total")) {
            String region = dao.loadString("Select distinct region from factor_aggregate where dealergroup like'" + dealergroup + "'");
            request.setAttribute("region", region);
            strQuery = strQuery.replaceAll("#region#", "  region in (select distinct region from factor_aggregate where \n"
                    + "dealergroup=\"" + dealergroup + "\") ");

        } else {
            strQuery = strQuery.replaceAll("#region#", "1=1  ");
        }

        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.load3Vals(strQuery, "Region", "region", "csi_score", "csi_score", "count", "count");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject jsonobj = new JSONObject();
            String region = (String) tmpJson.get("Region");

            String totalresp = "Total Respondents";
            String regiontemp = region;
            String csi_score = (String) tmpJson.get("csi_score");
            String count = (String) tmpJson.get("count");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                regiontemp = service.getTranslateString(region, langCode);
                totalresp = service.getTranslateString(totalresp, langCode);
                System.out.println("Dealer:" + region);
            }
            jsonobj.put("code", region);
            jsonobj.put("region", region);
            jsonobj.put("regioncnvt", regiontemp);
            jsonobj.put("repondents", Integer.parseInt(count));
            jsonobj.put("name", "<b>" + regiontemp + "</b>: <b>" + csi_score + "<br>" + totalresp + ": " + count + "<b>");
            jsonobj.put("value", Integer.parseInt(csi_score));
            finalArray.add(jsonobj);
        }

        out.println(finalArray.toJSONString());
        return;
    }

    public void getTop5Bottom5(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getTop5Bottom5'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String langCode = request.getParameter("langCode");
        List<String> filterList = new ArrayList<String>();

//        filterList.add("dealer");
        filterList.add("region");
        filterList.add("year");
//       

        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);
        JSONArray jsonArray = dao.load3Vals(strQuery, "Dealer", "Dealer", "Score", "score", "TopOrBottom", "top_or_bottom");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        JSONArray Attributes = new JSONArray();
        JSONArray Scores = new JSONArray();
        JSONArray Ranks = new JSONArray();
        JSONArray TopORBottoms = new JSONArray();

        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String Dealer = (String) tmpJson.get("Dealer");

            String Score = (String) tmpJson.get("Score");
            String toporbootom = (String) tmpJson.get("TopOrBottom");
            String tipkey = "Score";
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                Dealer = service.getTranslateString(Dealer, langCode);
                tipkey = service.getTranslateString(tipkey, langCode);
                System.out.println("Attribute:" + Dealer);
            }
            addingJson.put("name", Dealer);
            addingJson.put("dealername", Dealer);
            addingJson.put("bartitle", Score);
            addingJson.put("y", Integer.parseInt(Score));
            addingJson.put("tooltip", Dealer + " <br>" + tipkey + " :<b>" + Score + "</b>");
            if (toporbootom.equalsIgnoreCase("Top")) {
                addingJson.put("color", "#7E57C2");
            } else if (toporbootom.equalsIgnoreCase("Bottom")) {
                addingJson.put("color", "#757575");
            }
            finalArray.add(addingJson);
        }

        out.println(finalArray.toJSONString());
        return;
    }

    //////////

    public void getTop5Bottom5Attributes(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getTop5Bottom5Attribute'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> bestfilterList = new ArrayList<String>();
        List<String> filterList = new ArrayList<String>();

        filterList.add("region");
        filterList.add("model");
        filterList.add("dealer");
        filterList.add("year");
        filterList.add("dealergroup");

        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        String where_condtion = service.buildWherecondition(filterList, request);

        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);

        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);

        String strBestString = service.getBestString(request);
        String strWorstString = service.getWorstString(request);

        JSONArray jsonArray = dao.load2Vals(strQuery, "Attribute", "Attribute", "Score", "score");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        String model = request.getParameter("model");
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String Attribute = (String) tmpJson.get("Attribute");
            String Score = (String) tmpJson.get("Score");
            String bestscore = (String) tmpJson.get("BestScore");
            String worstscore = (String) tmpJson.get("WorstScore");
            if (model != null && !model.equalsIgnoreCase("ALL") && !model.equalsIgnoreCase("Study Total")) {
                bestscore = null;
            }
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                Attribute = service.getTranslateString(Attribute, langCode);
                strBestString = service.getTranslateString(strBestString, langCode);
                strWorstString = service.getTranslateString(strWorstString, langCode);
                System.out.println("Dealer:" + Attribute);
            }
            addingJson.put("name", Attribute);
            addingJson.put("bartitle", Score);
            addingJson.put("y", Double.parseDouble(Score));
            addingJson.put("tooltip", Attribute + "<br>Attribute Score: <b>" + Score + "</b>");
            addingJson.put("scatter", strBestString);
            if (bestscore != null && !bestscore.equalsIgnoreCase("0") && !bestscore.equalsIgnoreCase("null")) {
                addingJson.put("svalue", Double.parseDouble(bestscore));
            } else {
                addingJson.put("svalue", null);
            }
            addingJson.put("scatter1", strWorstString);
            if (worstscore != null && !worstscore.equalsIgnoreCase("0") && !worstscore.equalsIgnoreCase("null")) {
                addingJson.put("svalue1", Double.parseDouble(worstscore));
            } else {
                addingJson.put("svalue1", null);
            }
            finalArray.add(addingJson);
        }

        out.println(finalArray.toJSONString());
        return;
    }

    public void getTop5Bottom5AttributesMean(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getTop5Bottom5Attribute'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> bestfilterList = new ArrayList<String>();
        List<String> filterList = new ArrayList<String>();

        //  filterList.add("attributeFactor");
        filterList.add("region");
        filterList.add("model");
        filterList.add("dealer");
        filterList.add("year");
        filterList.add("dealer");

        filterList.add("dealergroup");
        bestfilterList.add("year");
        bestfilterList.add("dealer");
        bestfilterList.add("bestregion");
      //  bestfilterList.add("attributeFactor");

        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        String where_condtion = service.buildWherecondition(filterList, request);
        String best_where_condtion = service.buildWherecondition(bestfilterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strBestWhereCluse, best_where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);

        String strBestString = service.getBestString(request);
        String strWorstString = service.getWorstString(request);

        JSONArray jsonArray = dao.load3Vals(strQuery, "Attribute", "Attribute", "Score", "score", "top_or_bottom", "top_or_bottom");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        String model = request.getParameter("model");
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String Attribute = (String) tmpJson.get("Attribute");
            String Score = (String) tmpJson.get("Score");
            String bestscore = (String) tmpJson.get("BestScore");
            String worstscore = (String) tmpJson.get("WorstScore");
            String toporbootom = (String) tmpJson.get("top_or_bottom");
            System.out.println(toporbootom);
            System.out.println("345667");
            if (model != null && !model.equalsIgnoreCase("ALL") && !model.equalsIgnoreCase("Study Total")) {
                bestscore = null;
            }
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                Attribute = service.getTranslateString(Attribute, langCode);
                strBestString = service.getTranslateString(strBestString, langCode);
                strWorstString = service.getTranslateString(strWorstString, langCode);
                System.out.println("Dealer:" + Attribute);
            }
            addingJson.put("name", Attribute);
            addingJson.put("bartitle", Score);
            addingJson.put("y", Double.parseDouble(Score));
            addingJson.put("tooltip", Attribute + "<br>Attribute Score: <b>" + Score + "</b>");
            addingJson.put("scatter", strBestString);
            if (bestscore != null && !bestscore.equalsIgnoreCase("0") && !bestscore.equalsIgnoreCase("null")) {
                addingJson.put("svalue", Double.parseDouble(bestscore));
            } else {
                addingJson.put("svalue", null);
            }
            addingJson.put("scatter1", strWorstString);
            if (worstscore != null && !worstscore.equalsIgnoreCase("0") && !worstscore.equalsIgnoreCase("null")) {
                addingJson.put("svalue1", Double.parseDouble(worstscore));
            } else {
                addingJson.put("svalue1", null);
            }
            if (toporbootom.equalsIgnoreCase("top")) {
                addingJson.put("color", "#7E57C2");
            } else if (toporbootom.equalsIgnoreCase("bottom")) {
                addingJson.put("color", "#757575");
            }
            finalArray.add(addingJson);
        }

        out.println(finalArray.toJSONString());
        return;
    }

    public void getTop5Bottom5SOP(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getTop5Bottom5SOP'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

        filterList.add("region");
        filterList.add("model");
        filterList.add("dealer");
        filterList.add("year");
        filterList.add("dealergroup");
        filterList.add("month");

        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.load3Vals(strQuery, "Attribute", "Attribute", "Score", "score", "TopOrBottom", "top_or_bottom");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        JSONArray Attributes = new JSONArray();
        JSONArray Scores = new JSONArray();
        JSONArray TopORBottoms = new JSONArray();
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();

            String Attribute = (String) tmpJson.get("Attribute");
            String Score = (String) tmpJson.get("Score");
            String tipkey = "%Delighted";
            if (Score == null) {
                Score = "0";
            }

            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                int flag = 0;
                if (Attribute.contains("**")) {
                    flag = 2;
                } else if (Attribute.contains("*")) {
                    flag = 1;
                }
                System.out.println("Attributeflagvalue " + flag);
                System.out.println("Attribute value b4 " + Attribute);

                Attribute = Attribute.replaceAll("\\*", "");
                Attribute = service.getTranslateString(Attribute, langCode);
                if (flag == 2) {
                    Attribute = Attribute.concat("**");
                } else if (flag == 1) {
                    Attribute = Attribute.concat("*");
                }
                Attribute = service.getTranslateString(Attribute, langCode);
                tipkey = service.getTranslateString(tipkey, langCode);
//                tipkey = "<br>คะแนนหัวข้อย่อย : <b>";
                System.out.println("Attribute:" + Attribute);
            }

            String toporbootom = (String) tmpJson.get("TopOrBottom");
            addingJson.put("name", Attribute);
            addingJson.put("bartitle", Score + "");
            addingJson.put("y", Integer.parseInt(Score));
            addingJson.put("tooltip", Attribute + "<br>" + tipkey + ":" + Score + "</b>");
            if (toporbootom.equalsIgnoreCase("Top")) {
                addingJson.put("color", "#7E57C2");
            } else if (toporbootom.equalsIgnoreCase("Bottom")) {
                addingJson.put("color", "#757575");
            }
            finalArray.add(addingJson);
        }

        out.println(finalArray.toJSONString());
        return;
    }

    public void getSummaryRegionFactorScores(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getSummaryRegionFactorScores'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> bestfilterList = new ArrayList<String>();
        List<String> filterList = new ArrayList<String>();

        filterList.add("zone");
        filterList.add("region");
        filterList.add("model");
        filterList.add("dealer");
        filterList.add("year");
        bestfilterList.add("year");

//        bestfilterList.add("region");
        String model = request.getParameter("model");
        String langCode = request.getParameter("langCode");
        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        String where_condtion = service.buildWherecondition(filterList, request);
        String best_where_condtion = service.buildWherecondition(bestfilterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strBestWhereCluse, best_where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);

        JSONArray jsonArray = dao.load4Vals(strQuery, "Factor", "Factor", "Score", "score", "BestScore", "maxscore", "WorstScore", "minscore");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        String strBestString = service.getBestString(request);
        String strWorstString = service.getWorstString(request);
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            System.out.println(tmpJson.toJSONString());
            JSONObject addingJson = new JSONObject();
            String factor = (String) tmpJson.get("Factor");
            String Score = (String) tmpJson.get("Score");
            String bestscore = (String) tmpJson.get("BestScore");
            String worstscore = (String) tmpJson.get("WorstScore");
            String tipkey = "Factor Score";
            if (model != null && !model.equalsIgnoreCase("ALL") && !model.equalsIgnoreCase("Study Total")) {
                bestscore = null;
            }
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                factor = service.getTranslateString(factor, langCode);
                strBestString = service.getTranslateString(strBestString, langCode);
                strWorstString = service.getTranslateString(strWorstString, langCode);
                tipkey = service.getTranslateString(tipkey, langCode);
                System.out.println("Dealer:" + factor);
            }
            addingJson.put("name", factor);
            addingJson.put("bartitle", Score);
            addingJson.put("y", Integer.parseInt(Score));
            addingJson.put("tooltip", factor + "<br></b>" + tipkey + ":</b>" + Score + "</b>");
            addingJson.put("scatter", strBestString);
            if (bestscore != null && !bestscore.equalsIgnoreCase("0") && !bestscore.equalsIgnoreCase("null")) {
                addingJson.put("svalue", Integer.parseInt(bestscore));
            } else {
                addingJson.put("svalue", null);
            }
            addingJson.put("scatter1", strWorstString);
            if (worstscore != null && !worstscore.equalsIgnoreCase("0") && !worstscore.equalsIgnoreCase("null")) {
                addingJson.put("svalue1", Integer.parseInt(worstscore));
            } else {
                addingJson.put("svalue1", null);
            }
            finalArray.add(addingJson);
        }

        out.println(finalArray.toJSONString());
        return;
    }

}
