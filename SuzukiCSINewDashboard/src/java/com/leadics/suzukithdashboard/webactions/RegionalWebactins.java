/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.suzukithdashboard.webactions;

import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.service.LIReportquirescsiService;
import com.leadics.suzukithdashboard.service.LIReportquiresssiService;
import com.leadics.suzukithdashboard.to.LIReportquirescsiRecord;
import com.leadics.suzukithdashboard.to.LIReportquiresssiRecord;
import com.leadics.utils.LogUtils;
import com.leadics.utils.PropertyUtil;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author varma.sagi
 */
public class RegionalWebactins {

    String langCode = "EN";
    static LogUtils logger = new LogUtils(RegionalWebactins.class.getName());

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {
                String lang = request.getParameter("langCode");
                if (lang != null) {
                    langCode = lang;
                }
                if (!StringUtils.isNullOrEmpty(actionType)) {
                    if (actionType.equals("getFactorAverage")) {
                        getFactorAverage(request, response);
                        return;
                    }
                    if (actionType.equals("getRegionalFactorAverage")) {
                        getRegionalFactorAverage(request, response);
                        return;
                    }
                    if (actionType.equals("getRegionalAttributeAverage")) {
                        getRegionalAttributeAverage(request, response);
                        return;
                    }
                    if (actionType.equals("getDissatisfied")) {
                        getDissatisfied(request, response);
                        return;
                    }
                    if (actionType.equals("getDelighted")) {
                        getDelighted(request, response);
                        return;
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    public void getRegionalFactorAverage(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getRegionalFactorAverage'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> bestfilterList = new ArrayList<String>();
        List<String> filterList = new ArrayList<String>();

        filterList.add("year");
        filterList.add("biannual");
        filterList.add("month");
        filterList.add("factor");
        filterList.add("region");
        filterList.add("zone");
        bestfilterList.add("year");

        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        String where_condtion = service.buildWherecondition(filterList, request);
        String best_where_condtion = service.buildWherecondition(bestfilterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strBestWhereCluse, best_where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);

        String strBestString = "Best";
        String strWorstString = "Worst";
        String tipkey = "Factor Score";

        JSONArray jsonArray = dao.load5Vals(strQuery, "Region", "Region", "FactorScore", "csi_score", "BestScore", "BestScore", "WorstScore", "WorstScore", "catg", "catg");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();

        String model = request.getParameter("model");

        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String Region = (String) tmpJson.get("Region");
            String Regionnane = Region;
            String Score = (String) tmpJson.get("FactorScore");
            String bestscore = (String) tmpJson.get("BestScore");
            String worstscore = (String) tmpJson.get("WorstScore");
            String samplecount = (String) tmpJson.get("catg");
            Integer count = Integer.parseInt(samplecount);
            String astersik = "";
//            System.out.println("NSK:::::::::::::::" + count);
            if (count == 3) {
                astersik = "**";
            } else if (count == 2) {
                astersik = "*";
            } else {
                astersik = "";
            }

            if (model != null && !model.equalsIgnoreCase("All") && !model.equalsIgnoreCase("Study Total")) {
                bestscore = null;
            }
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                Region = service.getTranslateString(Region, langCode);
                strBestString = service.getTranslateString(strBestString, langCode);
                strWorstString = service.getTranslateString(strWorstString, langCode);
                tipkey = service.getTranslateString(tipkey, langCode);

                System.out.println("Dealer:" + Region);
            }
            addingJson.put("name", Region + astersik);
            addingJson.put("regionname", Regionnane);
            addingJson.put("bartitle", Score);
            addingJson.put("y", Integer.parseInt(Score));
            addingJson.put("tooltip", Region + "<br>" + tipkey + ": <b>" + Score + "</b>");
            addingJson.put("scatter", strBestString);

            if (bestscore != null && !bestscore.equalsIgnoreCase("0") && !bestscore.equalsIgnoreCase("null")) {
                addingJson.put("svalue", Integer.parseInt(bestscore));
            } else {
                addingJson.put("svalue", null);
            }
            addingJson.put("scatter1", strWorstString);
            if (worstscore != null && !worstscore.equalsIgnoreCase("0") && !worstscore.equalsIgnoreCase("null")) {
                addingJson.put("svalue1", Integer.parseInt(worstscore));
            } else {
                addingJson.put("svalue1", null);
            }
            finalArray.add(addingJson);
        }
        out.println(finalArray.toJSONString());
        return;
    }

    public void getDelighted(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDelighted'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();
        List<String> bestfilterList = new ArrayList<String>();
        filterList.add("attributeFactor");
        filterList.add("region");
        filterList.add("year");
        //in the case of dealer login these will be used
        filterList.add("dealer");
        filterList.add("dealergroup");
        //
        bestfilterList.add("year");
        bestfilterList.add("attributeFactor");

        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        String where_condtion = service.buildWherecondition(filterList, request);
        String best_where_condtion = service.buildWherecondition(bestfilterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strBestWhereCluse, best_where_condtion, true);
        JSONArray jsonArray = dao.load4Vals(strQuery, "Attribute", "Attribute", "Score", "score", "BestScore", "BestScore", "WorstScore", "WorstScore");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();

        String strBestString = service.getBestString(request);
        String strWorstString = service.getWorstString(request);

        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String Attribute = (String) tmpJson.get("Attribute");
            String Score = (String) tmpJson.get("Score");
            String bestscore = (String) tmpJson.get("BestScore");
            String worstscore = (String) tmpJson.get("WorstScore");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                Attribute = service.getTranslateString(Attribute, langCode);
                strBestString = service.getTranslateString(strBestString, langCode);
                strWorstString = service.getTranslateString(strWorstString, langCode);
                System.out.println("Dealer:" + Attribute);
            }
            addingJson.put("scatter", strBestString);
            if (bestscore != null && !bestscore.equalsIgnoreCase("0") && !bestscore.equalsIgnoreCase("null")) {
                addingJson.put("svalue", Integer.parseInt(bestscore));
            } else {
                addingJson.put("svalue", null);
            }
            addingJson.put("scatter1", strWorstString);
            if (worstscore != null && !worstscore.equalsIgnoreCase("0") && !worstscore.equalsIgnoreCase("null")) {
                addingJson.put("svalue1", Integer.parseInt(worstscore));
            } else {
                addingJson.put("svalue1", null);
            }
            addingJson.put("name", Attribute);
            if (Score.equalsIgnoreCase("0")) {
                addingJson.put("bartitle", "0%");
            } else {
                addingJson.put("bartitle", Score + "%");
            }
//            if(Score.equalsIgnoreCase("0")){
//            addingJson.put("y", "0");}else{
            addingJson.put("y", Integer.parseInt(Score));
//        }
            addingJson.put("tooltip", Attribute + "<br>Factor Score: <b>" + Score + "</b>");
            finalArray.add(addingJson);
        }

        out.println(finalArray.toJSONString());
        return;
    }

    public void getDissatisfied(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDissatisfied'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

        filterList.add("attributeFactor");
        filterList.add("region");
        filterList.add("dealer");
        filterList.add("model");
        filterList.add("year");
//        filterList.add("biannual");
//        filterList.add("month");
        filterList.add("dealergroup");
        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.load4Vals(strQuery, "Attribute", "Attribute", "Score", "score", "BestScore", "BestScore", "WorstScore", "WorstScore");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();

        String strBestString = service.getBestString(request);
        String strWorstString = service.getWorstString(request);

        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String Attribute = (String) tmpJson.get("Attribute");
            String Score = (String) tmpJson.get("Score");
            String bestscore = (String) tmpJson.get("BestScore");
            String worstscore = (String) tmpJson.get("WorstScore");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                Attribute = service.getTranslateString(Attribute, langCode);
                strBestString = service.getTranslateString(strBestString, langCode);
                strWorstString = service.getTranslateString(strWorstString, langCode);
                System.out.println("Dealer:" + Attribute);
            }
//             addingJson.put("scatter", strBestString);
//            if (bestscore != null && !bestscore.equalsIgnoreCase("0") && !bestscore.equalsIgnoreCase("null")) {
//                addingJson.put("svalue", Integer.parseInt(bestscore));
//            } else {
//                addingJson.put("svalue", 0);
//            }
//            addingJson.put("scatter1", strWorstString);
//            if (worstscore != null && !worstscore.equalsIgnoreCase("0") && !worstscore.equalsIgnoreCase("null")) {
//                addingJson.put("svalue1", Integer.parseInt(worstscore));
//            } else {
//                addingJson.put("svalue1", 0);
//            }
            addingJson.put("name", Attribute);
            addingJson.put("bartitle", Score + "%");
            addingJson.put("y", Integer.parseInt(Score));
            addingJson.put("tooltip", Attribute + "<br>Factor Score: <b>" + Score + "</b>");
            finalArray.add(addingJson);
        }

        out.println(finalArray.toJSONString());
        return;
    }

    public void getRegionalAttributeAverage(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getRegionalAttributeAverage'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> bestfilterList = new ArrayList<String>();
        List<String> filterList = new ArrayList<String>();

        filterList.add("attributeFactor");
        filterList.add("region");
        filterList.add("year");
        //in the case of dealer login these will be used
        filterList.add("dealer");
        filterList.add("dealergroup");
        //
        bestfilterList.add("year");
        bestfilterList.add("attributeFactor");

        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        String where_condtion = service.buildWherecondition(filterList, request);
        String best_where_condtion = service.buildWherecondition(bestfilterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strBestWhereCluse, best_where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);

        String strBestString = service.getBestString(request);
        String strWorstString = service.getWorstString(request);

        JSONArray jsonArray = dao.load4Vals(strQuery, "Attribute", "Attribute", "Score", "score", "BestScore", "BestScore", "WorstScore", "WorstScore");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        String model = request.getParameter("model");
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String Attribute = (String) tmpJson.get("Attribute");
            String Score = (String) tmpJson.get("Score");
            String bestscore = (String) tmpJson.get("BestScore");
            String worstscore = (String) tmpJson.get("WorstScore");
            if (model != null && !model.equalsIgnoreCase("ALL") && !model.equalsIgnoreCase("Study Total")) {
                bestscore = null;
            }
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                Attribute = service.getTranslateString(Attribute, langCode);
                strBestString = service.getTranslateString(strBestString, langCode);
                strWorstString = service.getTranslateString(strWorstString, langCode);
                System.out.println("Dealer:" + Attribute);
            }
            addingJson.put("name", Attribute);
            addingJson.put("bartitle", Score);
            addingJson.put("y", Double.parseDouble(Score));
            addingJson.put("tooltip", Attribute + "<br>Attribute Score: <b>" + Score + "</b>");
            addingJson.put("scatter", strBestString);
            if (bestscore != null && !bestscore.equalsIgnoreCase("0") && !bestscore.equalsIgnoreCase("null")) {
                addingJson.put("svalue", Double.parseDouble(bestscore));
            } else {
                addingJson.put("svalue", null);
            }
            addingJson.put("scatter1", strWorstString);
            if (worstscore != null && !worstscore.equalsIgnoreCase("0") && !worstscore.equalsIgnoreCase("null")) {
                addingJson.put("svalue1", Double.parseDouble(worstscore));
            } else {
                addingJson.put("svalue1", null);
            }
            finalArray.add(addingJson);
        }

        out.println(finalArray.toJSONString());
        return;
    }

    public void getFactorAverage(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getFactorAverage'");
        String strQuery = queryRecord.getQuery();
        String strCountQuery = queryRecord.getCountquery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

        filterList.add("factor");
        filterList.add("year");

        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strCountQuery = StringUtils.replaceString(strCountQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.load2Vals(strQuery, "Factor", "Factor", "Score", "score");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            String Factor = (String) tmpJson.get("Factor");
            String Score = (String) tmpJson.get("Score");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                Factor = service.getTranslateString(Factor, langCode);
            }
            addingJson.put("Factor", Factor);
            addingJson.put("Score", Score);

            finalArray.add(addingJson);
        }
        String count = dao.loadString(strCountQuery, "samplecount");
        JSONObject obj = new JSONObject();
        obj.put("samplecount", count);
        obj.put("data", finalArray);
        out.println(obj.toJSONString());
        return;
    }
}
