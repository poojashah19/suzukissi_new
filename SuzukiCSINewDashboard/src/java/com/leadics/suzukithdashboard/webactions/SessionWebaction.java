/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.suzukithdashboard.webactions;

import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.service.LIWebsessionService;
import com.leadics.suzukithdashboard.to.LIWebsessionRecord;

import com.leadics.utils.*;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.http.*;

/**
 *
 * @author Krishna-Kanth
 */
public class SessionWebaction {

    static LogUtils logger = new LogUtils(LIWebsessionService.class.getName());

    public static String createSession(String userName) throws Exception {
        LIWebsessionService liWebsessionService = new LIWebsessionService();
        LIWebsessionRecord record = new LIWebsessionRecord();
        record.setUserid(userName);
        String uniqueID = "" + (UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE);
        record.setSessionid(uniqueID);
        record.setSource("Web");
        record.setRstatus("1");;
        int result = liWebsessionService.insertLIWebsessionRecord(record);
        if (result < 0) {
            throw new Exception("Cant create the session");
        }

        return uniqueID;
    }

    public static boolean isValidSession(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String userId = request.getParameter("loginsessionuserid");
        String sessionId = request.getParameter("userSessionId");
        
        boolean isValid = isValidSessionOrNot(userId, sessionId);
        
        boolean searchRecord = searchByUser(userId, sessionId);
        if (searchRecord == false) {
            return false;
        }
        

        if (isValid) {
            
            updateLIWebsessionLastAccessedRecord(userId,sessionId);
        } else {
            
            updateLIWebsessionClosedRecord(userId,sessionId,"Expire");;


        }

        return isValid;
    }

    private static boolean searchByUser(String userName, String sessionId) throws Exception {
        String query = "select * from web_session where "
                + " SESSIONID =\"" + sessionId + "\" and CLOSED_AT is null order by id desc limit 1;";

        return loadFirstLIWebsessionRecord(query);

    }

    private static boolean isValidSessionOrNot(String userId, String sessionId) {
        try {
            String query = "";
            int maxTimeDiff = Integer.parseInt(PropertyUtil.getProperty("websessiontimeout"));

            LogUtils.println("DB Check 1:");

            query += "SELECT count(*) CNT FROM web_session  ";
            query += "WHERE (TIMESTAMPDIFF(MINUTE,LASTACC,NOW()) < '" + maxTimeDiff + "') ";
            query += "AND (UPPER(USER_ID) = UPPER('" + userId + "')) ";
            query += "AND (UPPER(SESSIONID) = UPPER('" + sessionId + "')) ";
            query += "AND (RSTATUS = '1') ";

            LIDAO dao = new LIDAO();

            if ((dao.isMSSQL()) || (dao.isMSSQL8())) {
                query = "SELECT count(*) CNT FROM WEB_SESSION ";
                query += "WHERE (datediff(MINUTE,LASTACC,GETDATE()) < '" + maxTimeDiff + "') ";
                query += "AND (UPPER(USER_ID) = UPPER('" + userId + "')) ";
                query += "AND (UPPER(SESSIONID) = UPPER('" + sessionId + "')) ";
                query += "AND (RSTATUS = '1') ";
            }

            if (dao.isOracleDatabase()) {
                query = "SELECT count(*) CNT FROM WEB_SESSION ";
                query += "WHERE (((SYSDATE - LASTACC)*24*60) < '" + maxTimeDiff + "') ";
                query += "AND (UPPER(USER_ID) = UPPER('" + userId + "')) ";
                query += "AND (UPPER(SESSIONID) = UPPER('" + sessionId + "')) ";
                query += "AND (RSTATUS = '1') ";

                query = StringUtils.replaceString(query, "#SYSDATE#", DateUtils.getCurrentDateTime(), true);
            }

            LogUtils.println("Query for Session Check:" + query);
            LogUtils.println("DB Check 2:" + dao.isMSSQL());
            LogUtils.println("DB Check 2:" + dao.isMSSQL8());

            int count = dao.loadCount(query);
            LogUtils.println("Query Output:" + count);
            return (count > 0);
        } catch (Exception exception) {
            logger.error(exception);

            return false;
        }

    }

    private static Connection getCPDatabaseConnection() throws ClassNotFoundException, Exception {
        Class.forName(PropertyUtil.getDbdriver());
        Connection con = DriverManager.getConnection(PropertyUtil.getDburl(), PropertyUtil.getDbuid(), (new LISecurityUtil()).decryptRegular(PropertyUtil.getDbpwd()));
        return con;
    }

    private static boolean loadFirstLIWebsessionRecord(String query) {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = null;
        try {
            if (con == null) {
                con = getCPDatabaseConnection();
            }
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            ArrayList recordSet = new ArrayList();
            while (rs.next()) {
                LIWebsessionRecord record = new LIWebsessionRecord();
                record.setId(rs.getString("Id"));
                record.setUserid(rs.getString("USER_ID"));
                record.setSessionid(rs.getString("SESSIONID"));
                record.setSource(rs.getString("SOURCE"));
                record.setRstatus(rs.getString("RSTATUS"));
                record.setLastacc(formatDBDateTime(rs.getTimestamp("LASTACC")));
                record.setCreatedby(rs.getString("CREATED_BY"));
                record.setCreatedat(formatDBDateTime(rs.getTimestamp("CREATED_AT")));
                record.setModifiedat(formatDBDateTime(rs.getTimestamp("MODIFIED_AT")));
                record.setModifiedby(rs.getString("MODIFIED_BY"));
                record.setClosetype(rs.getString("CLOSE_TYPE"));
                record.setClosedat(rs.getString("CLOSED_AT"));
                recordSet.add(record);
            }
            logger.trace("loadLIWebsessionRecords:Records Fetched:" + recordSet.size());
            LIWebsessionRecord[] tempLIWebsessionRecords = new LIWebsessionRecord[recordSet.size()];
            for (int index = 0; index < recordSet.size(); index++) {
                tempLIWebsessionRecords[index] = (LIWebsessionRecord) (recordSet.get(index));
            }
            ps.close();
            releaseDatabaseConnection(con);
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    public static String formatDBDateTime(java.sql.Timestamp anDate)
            throws Exception {
        if (anDate == null) {
            return "";
        }
        SimpleDateFormat sd = new SimpleDateFormat("yyyyMMddHHmmss");
        return sd.format(anDate);
    }

    private static void releaseDatabaseConnection(Connection con) throws SQLException {
        con.close();
    }

    public static boolean updateLIWebsessionLastAccessedRecord(String userId,String sessionId)
            throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {

            String Query = "UPDATE web_session SET ";
            
            Query += "LASTACC = now() ";

            Query += "WHERE (SESSIONID = ?) ";
            if (con == null) {
                con = getCPDatabaseConnection();
            }

            ps = con.prepareStatement(Query);

            ps.setString(1, sessionId);

            boolean result = ps.execute();
            logger.trace("updateLIWebsessionRecord\t" + result + "\t");
            ps.close();
            releaseDatabaseConnection(con);

            return result;
        } finally {
            releaseDatabaseConnection(con);
        }
    }

    public static void updateLIWebsessionClosedRecord(String userId,String sessionId,String typeofclose) throws Exception {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {

            String Query = "UPDATE web_session SET ";
            
            Query += "CLOSE_TYPE = ?, ";
            Query += "CLOSED_AT = now() ";

            Query += "WHERE (SESSIONID = ?) ";

            if (con == null) {
                con = getCPDatabaseConnection();
            }

            ps = con.prepareStatement(Query);

            ps.setString(1, typeofclose);
            ps.setString(2, sessionId);
           
            boolean result = ps.execute();
            logger.trace("updateLIWebsessionRecord\t" + result + "\t");
            ps.close();
            releaseDatabaseConnection(con);

            
        } finally {
            releaseDatabaseConnection(con);
        }

    }
}


