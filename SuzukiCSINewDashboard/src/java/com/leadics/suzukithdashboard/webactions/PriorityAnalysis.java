/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.suzukithdashboard.webactions;

import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.service.LIReportquirescsiService;
import com.leadics.suzukithdashboard.to.LIReportquirescsiRecord;
import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author varma.sagi
 */
public class PriorityAnalysis {

    String langCode = "EN";

    static LogUtils logger = new LogUtils(PriorityAnalysis.class.getName());

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
//        response.setContentType("text/html");
        response.setContentType("application/json;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        try (PrintWriter out = response.getWriter()) {
            try {
                String lang = request.getParameter("langCode");
                if (lang != null) {
                    langCode = lang;
                }
                if (!StringUtils.isNullOrEmpty(actionType)) {
                    if (actionType.equals("getRegionalPriority")) {
                        getRegionalPriority(request, response);
                        return;
                    }
                    if (actionType.equals("getDealerPriority")) {
                        getDealerPriority(request, response);
                        return;
                    }
                    
                     if (actionType.equals("getDealergroupPriority")) {
                        getDealerGroupPriority(request, response);
                        return;
                    }
                    
                    
                    if (actionType.equals("getPriority")) {
                        getPriority(request, response);
                        return;
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    public void getPriority(HttpServletRequest request, HttpServletResponse response) {
        String strMeasure = request.getParameter("measure");
        if (strMeasure != null && strMeasure.equalsIgnoreCase("dealer")) {
            getDealerPriority(request, response);
        } else if (strMeasure != null && strMeasure.equalsIgnoreCase("dealer group")) {
            getDealerGroupPriority(request, response);
        } else {
            getRegionalPriority(request, response);
        }
    }

    public void getRegionalPriority(HttpServletRequest request, HttpServletResponse response) {
        try {
            PrintWriter out = response.getWriter();
            LIReportquirescsiService queryService = new LIReportquirescsiService();
            LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getRegionPriority'");
            String strQuery = queryRecord.getQuery();
            LIDAO dao = new LIDAO();
            LIService service = new LIService();
            List<String> filterList = new ArrayList<String>();
           
//            filterList.add("year");
            
           

            if (!langCode.equalsIgnoreCase("EN")) {
                System.out.println("Lang Code :" + langCode);
                strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
            }

            String region1 = request.getParameter("region1");
            String region2 = request.getParameter("region2");
            
           
              
            if (region1 != null && !region1.equalsIgnoreCase("ALL")) {
                strQuery = StringUtils.replaceString(strQuery, "!region1!", region1, true);
            } else {
                strQuery = StringUtils.replaceString(strQuery, "!region1!", "Bangkok & Greater", true);
            }

            if (region2 != null && !region2.equalsIgnoreCase("ALL")) {
                strQuery = StringUtils.replaceString(strQuery, "!region2!", region2, true);
            } else {
                strQuery = StringUtils.replaceString(strQuery, "!region2!", "Central and East", true);
            }

            String where_condtion = service.buildWherecondition(filterList, request);
            strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
            JSONArray jsonArray = dao.load4Vals(strQuery, "region", "region", "attribute", "attribute", "diffrance", "diffrance", "studyavg", "studyavg");
            System.out.println(jsonArray.toJSONString());
            Iterator i = jsonArray.iterator();
            JSONArray finalArray = new JSONArray();
            LinkedHashMap timelines = new LinkedHashMap();
            LinkedHashMap attributes = new LinkedHashMap();
            LinkedHashMap timelinesattributes = new LinkedHashMap();
            LinkedHashMap studyavg = new LinkedHashMap();

            while (i.hasNext()) {
                JSONObject tmpJson = (JSONObject) i.next();
                String region = (String) tmpJson.get("region");
                String Attribute = (String) tmpJson.get("attribute");
                String Attributetemp=Attribute;
                if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                    Attributetemp = service.getTranslateString(Attribute, langCode);
                    System.out.println("Dealer:" + Attributetemp);
                }
                String diffrance = (String) tmpJson.get("diffrance");
                String Studyavg = (String) tmpJson.get("studyavg");
               
                System.out.println(tmpJson.toJSONString());
                timelines.put(region, region);
                attributes.put(Attribute.trim(), Attributetemp.trim());
                timelinesattributes.put(region + "-" + Attribute.trim(), diffrance);
                studyavg.put(region + "-" + Attribute, Studyavg);
            }
            Iterator itimeline = timelines.keySet().iterator();

            while (itimeline.hasNext()) {
                JSONObject jsonbject = new JSONObject();
                String timeline = (String) itimeline.next();
                String timelinetemp=timeline;
                System.out.println("timeline:" + timeline);
                 try {
                         if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                             
                              int flag = 0;
               
                if (timelinetemp.contains("**")) {
                    flag = 2;
                } else if (timelinetemp.contains("*")) {
                    flag = 1;
                }
                 timelinetemp = timelinetemp.replaceAll("\\*", "");
                             timelinetemp = service.getTranslateString(timelinetemp, langCode);
                             
                             
                             if (flag == 2) {
                    timelinetemp = timelinetemp.concat("**");
                } else if (flag == 1) {
                    timelinetemp = timelinetemp.concat("*");
                }
                             System.out.println("timelinetemp:" + timelinetemp);
                         }
                     } catch (Exception e) {
                         e.printStackTrace();
                     }
//                if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
//                    timelinetemp = service.getTranslateString(timeline, langCode);
//                    System.out.println("timelinetemp:" + timelinetemp);
//                }
                jsonbject.put("name", timelinetemp);
                Iterator iattribute = attributes.keySet().iterator();
                JSONArray tmpJsonArray = new JSONArray();
                while (iattribute.hasNext()) {
                    JSONObject tmpJsonObj = new JSONObject();
                    String attribute = (String) iattribute.next();
                    String score = (String) timelinesattributes.get(timeline + "-" + attribute);
                    if (score != null) {
                        tmpJsonObj.put("y", Double.parseDouble(score));
                    } else {
                        tmpJsonObj.put("y", Double.parseDouble("0.0"));
                    }
                    if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                        attribute = service.getTranslateString(attribute, langCode);
                        System.out.println("attribute:" + attribute);
                    }
                    tmpJsonObj.put("name", attribute.trim());
                    tmpJsonObj.put("tooltip", attribute.trim() + ":<br>" + score);

                    tmpJsonArray.add(tmpJsonObj);
                }
                System.out.println(tmpJsonArray.toJSONString());
                jsonbject.put("data", tmpJsonArray);
                finalArray.add(jsonbject);
            }

            itimeline = timelines.keySet().iterator();

            while (itimeline.hasNext()) {
                JSONObject jsonbject = new JSONObject();
                String timeline = (String) itimeline.next();
                System.out.println("timeline:" + timeline);
                String studybest="Best Overall";
                 if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                  studybest = service.getTranslateString(studybest, langCode);
                }
                jsonbject.put("name", studybest);
                Iterator iattribute = attributes.keySet().iterator();
                JSONArray tmpJsonArray = new JSONArray();
                while (iattribute.hasNext()) {
                    JSONObject tmpJsonObj = new JSONObject();
                    String attribute = (String) iattribute.next();
                    String score = (String) studyavg.get(timeline + "-" + attribute);
                    if (score != null) {
                        tmpJsonObj.put("y", Double.parseDouble(score));
                    } else {
                        tmpJsonObj.put("y", Double.parseDouble("0.0"));
                                studybest=null;;
                    }
                    try {
                        if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                            attribute = service.getTranslateString(attribute, langCode);
                            System.out.println("attribute:" + attribute);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    tmpJsonObj.put("name", attribute.trim());
                    tmpJsonObj.put("tooltip", attribute.trim() + ":<br>" + score);

                    tmpJsonArray.add(tmpJsonObj);
                }
                System.out.println(tmpJsonArray.toJSONString());
                jsonbject.put("data", tmpJsonArray); if(studybest!=null){
                    finalArray.add(jsonbject);}
                
                break;
            }

            out.println(finalArray.toJSONString());
            return;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getDealerPriority(HttpServletRequest request, HttpServletResponse response) {
        try {
            PrintWriter out = response.getWriter();
            LIReportquirescsiService queryService = new LIReportquirescsiService();
            LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDealerPriority'");
            String strQuery = queryRecord.getQuery();
            LIDAO dao = new LIDAO();
            LIService service = new LIService();
            List<String> filterList = new ArrayList<String>();
           
//            filterList.add("year");
          

            if (!langCode.equalsIgnoreCase("EN")) {
                System.out.println("Lang Code :" + langCode);
                strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
            }

            String dealer1 = request.getParameter("dealer1");
            String dealer2 = request.getParameter("dealer2");
            String region = "";
            if (dealer1 != null && !dealer1.equalsIgnoreCase("ALL")) {
                strQuery = StringUtils.replaceString(strQuery, "!dealer1!", dealer1, true);
                region = dao.loadString("SELECT DISTINCT region FROM priority_aggregate WHERE dealer LIKE \"" + dealer1 + "\" LIMIT 1");

            } else {
                strQuery = StringUtils.replaceString(strQuery, "!dealer1!", "TI Ford, Guwahati", true);
                region = dao.loadString("SELECT DISTINCT region FROM priority_aggregate WHERE dealer LIKE \"TI Ford, Guwahati\" LIMIT 1");
            }

            if (dealer2 != null && !dealer2.equalsIgnoreCase("ALL")) {
                strQuery = StringUtils.replaceString(strQuery, "!dealer2!", dealer2, true);
            } else {
                strQuery = StringUtils.replaceString(strQuery, "!dealer2!", "North 2", true);
            }

            String where_condtion = service.buildWherecondition(filterList, request);
            strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
            JSONArray jsonArray = dao.load4Vals(strQuery, "dealer", "dealer", "attribute", "attribute", "diffrance", "diffrance", "studyavg", "studyavg");
            System.out.println(jsonArray.toJSONString());
            Iterator i = jsonArray.iterator();
            JSONArray finalArray = new JSONArray();
            LinkedHashMap timelines = new LinkedHashMap();
            LinkedHashMap attributes = new LinkedHashMap();
            LinkedHashMap timelinesattributes = new LinkedHashMap();
         
            LinkedHashMap studyavg = new LinkedHashMap();

            while (i.hasNext()) {
                JSONObject tmpJson = (JSONObject) i.next();
                String dealer = (String) tmpJson.get("dealer");
                System.out.println("Dealerrr:" + dealer);
                
                String Attribute = (String) tmpJson.get("attribute");
                String Attributetemp=Attribute;
                if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                    Attributetemp = service.getTranslateString(Attribute, langCode);
                    
                }
                String diffrance = (String) tmpJson.get("diffrance");
                String Studyavg = (String) tmpJson.get("studyavg");
               
                System.out.println(tmpJson.toJSONString());
                timelines.put(dealer, dealer);
                attributes.put(Attribute.trim(), Attributetemp.trim());
                timelinesattributes.put(dealer + "-" + Attribute.trim(), diffrance);
                studyavg.put(dealer + "-" + Attribute, Studyavg);
               
            }
            Iterator itimeline = timelines.keySet().iterator();

            while (itimeline.hasNext()) {
                JSONObject jsonbject = new JSONObject();
                String timeline = (String) itimeline.next();
                String timelinetemp=timeline;
                System.out.println("timelinetemp:" + timelinetemp);
                 try {
                         if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                             
                              int flag = 0;
               
                if (timelinetemp.contains("**")) {
                    flag = 2;
                } else if (timelinetemp.contains("*")) {
                    flag = 1;
                }
                 timelinetemp = timelinetemp.replaceAll("\\*", "");
                             timelinetemp = service.getTranslateString(timelinetemp, langCode);
                             
                             
                             if (flag == 2) {
                    timelinetemp = timelinetemp.concat("**");
                } else if (flag == 1) {
                    timelinetemp = timelinetemp.concat("*");
                }
                             System.out.println("timelinetemp:" + timelinetemp);
                         }
                     } catch (Exception e) {
                         e.printStackTrace();
                     }
                jsonbject.put("name", timelinetemp);
                Iterator iattribute = attributes.keySet().iterator();
                JSONArray tmpJsonArray = new JSONArray();
                while (iattribute.hasNext()) {
                    JSONObject tmpJsonObj = new JSONObject();
                    String attribute = (String) iattribute.next();
                    String score = (String) timelinesattributes.get(timeline + "-" + attribute);
                    if (score != null) {
                        tmpJsonObj.put("y", Double.parseDouble(score));
                    } else {
                        tmpJsonObj.put("y", Double.parseDouble("0.0"));
                    }
                    try {
                         if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                             attribute = service.getTranslateString(attribute, langCode);
                             System.out.println("attribute:" + attribute);
                         }
                     } catch (Exception e) {
                         e.printStackTrace();
                     }
                    tmpJsonObj.put("name", attribute.trim());
                    tmpJsonObj.put("tooltip", attribute.trim() + ":<br>" + score);

                    tmpJsonArray.add(tmpJsonObj);
                }
                System.out.println(tmpJsonArray.toJSONString());
                jsonbject.put("data", tmpJsonArray);
                finalArray.add(jsonbject);
            }

            itimeline = timelines.keySet().iterator();

            while (itimeline.hasNext()) {
                JSONObject jsonbject = new JSONObject();
                String timeline = (String) itimeline.next();
                System.out.println("timeline:" + timeline);
                String studybest="Best Overall";
                 if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                  studybest = service.getTranslateString(studybest, langCode);
                }
                jsonbject.put("name", studybest);
                Iterator iattribute = attributes.keySet().iterator();
                JSONArray tmpJsonArray = new JSONArray();
                while (iattribute.hasNext()) {
                    JSONObject tmpJsonObj = new JSONObject();
                    String attribute = (String) iattribute.next();
                    String score = (String) studyavg.get(timeline + "-" + attribute);
                    if (score != null) {
                        tmpJsonObj.put("y", Double.parseDouble(score));
                        
                    } else {
                        tmpJsonObj.put("y", Double.parseDouble("0.0"));
                                studybest=null;;
                    }
                    if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                        attribute = service.getTranslateString(attribute, langCode);
                        System.out.println("attribute:" + attribute);
                    }
                    tmpJsonObj.put("name", attribute.trim());
                    tmpJsonObj.put("tooltip", attribute.trim() + ":<br>" + score);

                    tmpJsonArray.add(tmpJsonObj);
                }
                System.out.println(tmpJsonArray.toJSONString());
                jsonbject.put("data", tmpJsonArray);
                if(studybest!=null){
                    finalArray.add(jsonbject);}
                
                break;
            }
            

           

            out.println(finalArray.toJSONString());
            return;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    
    
    public void getDealerGroupPriority(HttpServletRequest request, HttpServletResponse response) {
        try {
            PrintWriter out = response.getWriter();
            LIReportquirescsiService queryService = new LIReportquirescsiService();
            LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getDealerGroupPriority'");
            String strQuery = queryRecord.getQuery();
            LIDAO dao = new LIDAO();
            LIService service = new LIService();
            List<String> filterList = new ArrayList<String>();
           
//            filterList.add("year");
          

            if (!langCode.equalsIgnoreCase("EN")) {
                System.out.println("Lang Code :" + langCode);
                strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
            }

            String dealergroup1 = request.getParameter("dealergroup1");
            String dealergroup2 = request.getParameter("dealergroup2");
            System.out.println("dealergroup:" + dealergroup1);
            System.out.println("dealergroup2:" + dealergroup2);
            
            String region = "";
            if (dealergroup1 != null && !dealergroup1.equalsIgnoreCase("ALL")) {
                strQuery = StringUtils.replaceString(strQuery, "!dealergroup1!", dealergroup1, true);
                region = dao.loadString("SELECT DISTINCT region FROM priority_aggregate WHERE dealergroup LIKE \"" + dealergroup1 + "\" LIMIT 1");
            }
//            } else {
//                strQuery = StringUtils.replaceString(strQuery, "!dealer1!", "TI Ford, Guwahati", true);
//                region = dao.loadString("SELECT DISTINCT region FROM priority_aggregate WHERE dealergroup LIKE \"TI Ford, Guwahati\" LIMIT 1");
//            }

            if (dealergroup2 != null && !dealergroup2.equalsIgnoreCase("ALL")) {
                strQuery = StringUtils.replaceString(strQuery, "!dealergroup2!", dealergroup2, true);
            }
//            } else {
//                strQuery = StringUtils.replaceString(strQuery, "!dealergroup2!", "North 2", true);
//            }

            String where_condtion = service.buildWherecondition(filterList, request);
            strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
            JSONArray jsonArray = dao.load4Vals(strQuery, "dealergroup", "dealergroup", "attribute", "attribute", "diffrance", "diffrance", "studyavg", "studyavg");
            System.out.println(jsonArray.toJSONString());
            Iterator i = jsonArray.iterator();
            JSONArray finalArray = new JSONArray();
            LinkedHashMap timelines = new LinkedHashMap();
            LinkedHashMap attributes = new LinkedHashMap();
            LinkedHashMap timelinesattributes = new LinkedHashMap();
           
            LinkedHashMap studyavg = new LinkedHashMap();

            while (i.hasNext()) {
                JSONObject tmpJson = (JSONObject) i.next();
                String dealergroup = (String) tmpJson.get("dealergroup");
                String Attribute = (String) tmpJson.get("attribute");
                String Attributetemp=Attribute;
                if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                    Attributetemp = service.getTranslateString(Attribute, langCode);
                    System.out.println("Dealer:" + Attributetemp);
                }
                String diffrance = (String) tmpJson.get("diffrance");
                String Studyavg = (String) tmpJson.get("studyavg");
                String regavg = (String) tmpJson.get("regionavg");
                System.out.println(tmpJson.toJSONString());
                timelines.put(dealergroup, dealergroup);
                attributes.put(Attribute.trim(), Attributetemp.trim());
                timelinesattributes.put(dealergroup + "-" + Attribute.trim(), diffrance);
                studyavg.put(dealergroup + "-" + Attribute, Studyavg);
               
            }
            Iterator itimeline = timelines.keySet().iterator();

            while (itimeline.hasNext()) {
                JSONObject jsonbject = new JSONObject();
                String timeline = (String) itimeline.next();
                String timelinetemp=timeline;
                System.out.println("timelinetemp:" + timelinetemp);
                 try {
                         if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                             
                              int flag = 0;
               
                if (timelinetemp.contains("**")) {
                    flag = 2;
                } else if (timelinetemp.contains("*")) {
                    flag = 1;
                }
                 timelinetemp = timelinetemp.replaceAll("\\*", "");
                             timelinetemp = service.getTranslateString(timelinetemp, langCode);
                             
                             
                             if (flag == 2) {
                    timelinetemp = timelinetemp.concat("**");
                } else if (flag == 1) {
                    timelinetemp = timelinetemp.concat("*");
                }
                             System.out.println("timelinetemp:" + timelinetemp);
                         }
                     } catch (Exception e) {
                         e.printStackTrace();
                     }
                jsonbject.put("name", timelinetemp);
                Iterator iattribute = attributes.keySet().iterator();
                JSONArray tmpJsonArray = new JSONArray();
                while (iattribute.hasNext()) {
                    JSONObject tmpJsonObj = new JSONObject();
                    String attribute = (String) iattribute.next();
                    String score = (String) timelinesattributes.get(timeline + "-" + attribute);
                    if (score != null) {
                        tmpJsonObj.put("y", Double.parseDouble(score));
                    } else {
                        tmpJsonObj.put("y", Double.parseDouble("0.0"));
                    }
                    try {
                         if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                             attribute = service.getTranslateString(attribute, langCode);
                             System.out.println("attribute:" + attribute);
                         }
                     } catch (Exception e) {
                         e.printStackTrace();
                     }
                    tmpJsonObj.put("name", attribute.trim());
                    tmpJsonObj.put("tooltip", attribute.trim() + ":<br>" + score);

                    tmpJsonArray.add(tmpJsonObj);
                }
                System.out.println(tmpJsonArray.toJSONString());
                jsonbject.put("data", tmpJsonArray);
                finalArray.add(jsonbject);
            }

            itimeline = timelines.keySet().iterator();

            while (itimeline.hasNext()) {
                JSONObject jsonbject = new JSONObject();
                String timeline = (String) itimeline.next();
                System.out.println("timeline:" + timeline);
                String studybest="Best Overall";
                if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                  studybest = service.getTranslateString(studybest, langCode);
                }
                jsonbject.put("name", studybest);
                Iterator iattribute = attributes.keySet().iterator();
                JSONArray tmpJsonArray = new JSONArray();
                while (iattribute.hasNext()) {
                    JSONObject tmpJsonObj = new JSONObject();
                    String attribute = (String) iattribute.next();
                    String score = (String) studyavg.get(timeline + "-" + attribute);
                    if (score != null) {
                        tmpJsonObj.put("y", Double.parseDouble(score));
                        
                    } else {
                        tmpJsonObj.put("y", Double.parseDouble("0.0"));
                                studybest=null;;
                    }
                    if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                        attribute = service.getTranslateString(attribute, langCode);
                        System.out.println("attribute:" + attribute);
                    }
                    tmpJsonObj.put("name", attribute.trim());
                    tmpJsonObj.put("tooltip", attribute.trim() + ":<br>" + score);

                    tmpJsonArray.add(tmpJsonObj);
                }
                System.out.println(tmpJsonArray.toJSONString());
                jsonbject.put("data", tmpJsonArray);
                if(studybest!=null){
                    finalArray.add(jsonbject);}
                
                break;
            }
            

            

            out.println(finalArray.toJSONString());
            return;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
