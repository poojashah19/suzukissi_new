/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.suzukithdashboard.webactions;

import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.service.LIReportquirescsiService;
import com.leadics.suzukithdashboard.service.LIReportquiresssiService;
import com.leadics.suzukithdashboard.to.LIReportquirescsiRecord;
import com.leadics.suzukithdashboard.to.LIReportquiresssiRecord;
import com.leadics.utils.LogUtils;
import com.leadics.utils.PropertyUtil;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author varma.sagi
 */
public class ModelAnalysisWebactions {

    String langCode = "EN";

    static LogUtils logger = new LogUtils(ModelAnalysisWebactions.class.getName());

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {
                String lang = request.getParameter("langCode");
                if (lang != null) {
                    langCode = lang;
                }
                if (!StringUtils.isNullOrEmpty(actionType)) {
                    if (actionType.equals("getModelScores")) {
                        getModelScores(request, response);
                        return;
                    }
                    //table
                    if (actionType.equals("getModelFactorScores")) {
                        getModelFactorScores(request, response);
                        return;
                    }
                    if (actionType.equals("getModelFactorTrend")) {
                        getModelFactorTrend(request, response);
                        return;
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    public void getModelFactorTrend(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        String periodSwitch = request.getParameter("period");
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getModelFactorTrend'");
        if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Year")) {
            queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getModelFactorTrendYear'");
        } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Bi-Annual Period")) {
            queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getModelFactorTrendBi'");
        } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Month")) {
            queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getModelFactorTrendMonth'");
        }
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

//        filterList.add("zone");
//        filterList.add("region");
        filterList.add("model");
        filterList.add("factor");
        filterList.add("year");
        
        if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Year")) {
        } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Bi-Annual")) {
            filterList.add("biannual");
        } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Month")) {

        }
        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        String totalcountQuery = "select sum(totalcount) as totalcount FROM factor_aggregate where factor like '!factor!' " + where_condtion;

        String strCity = request.getParameter("city");

        if (strCity != null && !strCity.equalsIgnoreCase("All")) {
            strQuery = strQuery.replaceAll("!city!", strCity);
            totalcountQuery = totalcountQuery.replaceAll("!city!", strCity);

        } else {
            strQuery = strQuery.replaceAll("!city!", "%");
            totalcountQuery = totalcountQuery.replaceAll("!city!", "%");
        }

        String strFactor = request.getParameter("factor");

        if (strFactor != null) {
            strQuery = strQuery.replaceAll("!factor!", strFactor);
            totalcountQuery = totalcountQuery.replaceAll("!factor!", strFactor);
        } else {
            strQuery = strQuery.replaceAll("!factor!", "Service Advisor");
            totalcountQuery = totalcountQuery.replaceAll("!factor!", "Service Advisor");
        }
        String totalcount = dao.loadString(totalcountQuery);

        JSONArray jsonArray = dao.load4Vals(strQuery, "period", "period", "factor_score", "factor_score", "bestscore", "bestscore","samplecount","samplecount");
        Iterator i = jsonArray.iterator();
         String factorscr = "Factor Score";
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject jsonobj = new JSONObject();
            String Period = (String) tmpJson.get("period");
            String factor_score = (String) tmpJson.get("factor_score");
            String bestscore = (String) tmpJson.get("bestscore");
            
            String samplecnt = (String) tmpJson.get("samplecount");
Integer count = Integer.valueOf(Integer.parseInt(samplecnt));
        String astersik = "";
//            System.out.println("NSK:::::::::::::::" + count);
        if (count < 10) {
            astersik = "**";
        } else if (count >= 10 && count < 30) {
            astersik = "*";
        } else {
            astersik = "";
        }

            if (bestscore == null) {
                bestscore = "0";
            }
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                Period = service.getTranslateString(Period, langCode);
                 factorscr = service.getTranslateString(factorscr, langCode);
//                factorscr = "<br>ปัจจัย คะแนน : <b>";
                System.out.println("Period:" + Period);
            }
            jsonobj.put("name", Period+astersik);
            jsonobj.put("bartitle", factor_score);
            jsonobj.put("y", Integer.parseInt(factor_score));
             jsonobj.put("tooltip", Period +"<br>"+ factorscr+ " :"+ factor_score + "</b>");
            jsonobj.put("svalue", Integer.parseInt(bestscore));
            finalArray.add(jsonobj);
        }
        JSONObject obj = new JSONObject();
        obj.put("totalcount", totalcount);
        obj.put("data", finalArray);
        out.println(obj.toJSONString());
        return;
    }
//table
//    public void getModelFactorScores(HttpServletRequest request, HttpServletResponse response) {
//        try {
//            PrintWriter out = response.getWriter();
//            LIReportquirescsiService queryService = new LIReportquirescsiService();
//            LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getModelFactorScores'");
//            String strQuery = queryRecord.getQuery();
//            LIDAO dao = new LIDAO();
//            LIService service = new LIService();
//            List<String> filterList = new ArrayList<String>();
//            filterList.add("zone");
//            filterList.add("region");
//            filterList.add("model");
//            filterList.add("year");
//           
//            
//        if (!langCode.equalsIgnoreCase("EN")) {
//                System.out.println("Lang Code :" + langCode);
//                strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
//            }
//        
//            String where_condtion = service.buildWherecondition(filterList, request);
//            strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
//            JSONArray jsonArray = dao.load11Vals(strQuery, "model", "model", "ServiceAdvisor", "ServiceAdvisor", "ServiceAdvisorcolor", "ServiceAdvisorcolor", "Servicefacility", "Servicefacility", "Servicefacilitycolor", "Servicefacilitycolor", "ServiceInitiation", "ServiceInitiation", "ServiceInitiationcolor", "ServiceInitiationcolor", "ServiceQuality", "ServiceQuality", "ServiceQualitycolor", "ServiceQualitycolor", "VehiclePickup", "VehiclePickup", "VehiclePickupcolor", "VehiclePickupcolor");
//            System.out.println(jsonArray.toJSONString());
//            System.out.println("size of array"+jsonArray.size());
//            Iterator i = jsonArray.iterator();
//            JSONArray finalArray = new JSONArray();
//            LinkedHashMap models = new LinkedHashMap();
//            LinkedHashMap factors = new LinkedHashMap();
//            LinkedHashMap scores = new LinkedHashMap();
//
//            System.out.println("size of iModels 1"+models.size());
//            while (i.hasNext()) {
//                JSONObject tmpJson = (JSONObject) i.next();
//                String model = (String) tmpJson.get("model");
//                String factor = (String) tmpJson.get("factor");
//                String score = (String) tmpJson.get("score");
//                String bestscore = (String) tmpJson.get("bestscore");
//                System.out.println(tmpJson.toJSONString());
//
//            System.out.println("Models "+model);
//                models.put(model, model);
//                factors.put(factor.trim(), factor.trim());
//                scores.put(model + "-" + factor.trim(), score);
//            }
//            
//            System.out.println("Models "+models);
//            Iterator iModels = models.keySet().iterator();
//            System.out.println("size of iModels"+models.size());
//            while (iModels.hasNext()) {
//                JSONObject jsonbject = new JSONObject();
//                String model = (String) iModels.next();
//                System.out.println("model:" + model);
//                Iterator ifactor = factors.keySet().iterator();
//                 if(langCode!=null && !langCode.equalsIgnoreCase("EN")){
//                        model = service.getTranslateString(model, langCode);
//                        System.out.println("Dealer:"+model);
//                    }
//                jsonbject.put("model", model);
//                while (ifactor.hasNext()) {
//                    JSONObject tmpJsonObj = new JSONObject();
//                    String factor = (String) ifactor.next();
//                    String modeltemp=model.replace("*", "");
//                     
//                     System.out.println(model + "-" + factor);
//                    String score = (String) scores.get(model + "-" + factor);
//                    System.out.println("model:"+score+model+modeltemp);
//                    if(score == null){
//                        score = "NA";
//                    }
//                    jsonbject.put(factor.trim().replaceAll(" ", ""), score);
//                }
//                finalArray.add(jsonbject);
//            }
//
//            out.println(finalArray.toJSONString());
//            return;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
   public void getModelFactorScores(HttpServletRequest request, HttpServletResponse response) {
        try {
            PrintWriter out = response.getWriter();
            LIReportquiresssiService queryService = new LIReportquiresssiService();
            LIReportquiresssiRecord queryRecord = queryService.loadFirstLIReportquiresssiRecord("SELECT * from reportquires_csi WHERE reportname='getModelFactorScores1'");
            String strQuery = queryRecord.getQuery();
            LIDAO dao = new LIDAO();
            LIService service = new LIService();
            List<String> filterList = new ArrayList<String>();
            String langCode=request.getParameter("langCode");
            filterList.add("regiona");
            filterList.add("region");
   
            filterList.add("year");
            filterList.add("quarter");
            filterList.add("month");
            filterList.add("dealer");


            if (!langCode.equalsIgnoreCase("EN")) {
                System.out.println("Lang Code :" + langCode);
                strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
            }
             
            String where_condtion = service.buildWherecondition(filterList, request);
            strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
            strQuery = StringUtils.replaceString(strQuery, service.strModelCountLimit,PropertyUtil.getProperty("modelcountlimit"), true);
              strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit,PropertyUtil.getProperty("samplelimit"), true);
            JSONArray jsonArray = dao.load5Vals(strQuery, "colour", "colour", "model", "model", "factorName", "factorName", "score", "score", "samplecount", "samplecount");
            System.out.println(jsonArray.toJSONString());
            Iterator i = jsonArray.iterator();
            JSONArray finalArray = new JSONArray();
            LinkedHashMap<String, JSONObject> models = new LinkedHashMap();
                    String strBestString = service.getBestString(request);
        String strWorstString = service.getWorstString(request);
            //@deepika kothamasu 6-1-2018 for optimization purpose
            while (i.hasNext()) {
                JSONObject tmpJson = (JSONObject) i.next();
                String model = (String) tmpJson.get("model");
                String colour = (String) tmpJson.get("colour");
                String factor = (String) tmpJson.get("factorName");
                String score = (String) tmpJson.get("score");
          
                JSONObject newJsonObj = null;

                // add if item is not object in list
                if (models.containsKey(model)) {
                    newJsonObj = models.get(model);
                    newJsonObj.put(factor, score);
                    newJsonObj.put(factor + "color", colour);

                } else {
                    newJsonObj = new JSONObject();
                    newJsonObj.put(factor, score);
                    newJsonObj.put(factor + "color", colour);
                }
                models.put(model, newJsonObj);
                String FactorModel=factor+"_"+model;
        
            
            }

            Set<String> keys = models.keySet();
            Iterator<String> itr = keys.iterator();;
            while (itr.hasNext()) {

                String keyofmap = itr.next();
                JSONObject obj = models.get(keyofmap);
               if(langCode!=null && !langCode.equalsIgnoreCase("EN")){
                            keyofmap = service.getTranslateString(keyofmap, langCode);
                           
                        }   
             
               
                   obj.put("model", keyofmap);

                finalArray.add(obj);
            }
            JSONObject blankobj=new JSONObject();
             finalArray.add(blankobj);
             JSONObject newJsonObj1 = new JSONObject();
             newJsonObj1.put("best", strBestString);
             newJsonObj1.put("worst", strWorstString);
              JSONArray finalArray1 = new JSONArray();
             finalArray1.add(newJsonObj1);
              finalArray1.add(finalArray);
            out.println(finalArray1.toJSONString());
            return;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
                
      

    public void getModelScores(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getModelScores'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();

//        filterList.add("zone");
//        filterList.add("region");
//        filterList.add("model");
        filterList.add("year");
       
      

        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.load2Vals(strQuery, "model", "model", "csi_score", "csi_score");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        String csiavg = "Average";
        while (i.hasNext()) {

            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject jsonobj = new JSONObject();
            String model = (String) tmpJson.get("model");
            String modelname = model;
            String modeltemp = model;
            String csi_score = (String) tmpJson.get("csi_score");

            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                int flag = 0;
//                csiavg = "<br> เฉลี่ย : <b>";
                if (modeltemp.contains("**")) {
                    flag = 2;
                } else if (modeltemp.contains("*")) {
                    flag = 1;
                }

                modeltemp = modeltemp.replaceAll("\\*", "");
                modeltemp = service.getTranslateString(modeltemp, langCode);
                  csiavg = service.getTranslateString(csiavg, langCode);
                if (flag == 2) {
                    modeltemp = modeltemp.concat("**");
                } else if (flag == 1) {
                    modeltemp = modeltemp.concat("*");
                }
            }
            jsonobj.put("name", modeltemp);
            jsonobj.put("modelname", modelname);
            jsonobj.put("bartitle", csi_score);
            jsonobj.put("y", Integer.parseInt(csi_score));
             jsonobj.put("tooltip", modeltemp + "<br>"+csiavg +" <b>:"+ csi_score + "</b>");
            finalArray.add(jsonobj);
        }

        out.println(finalArray.toJSONString());
        return;
    }

}
