/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.suzukithdashboard.webactions;

import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.service.LIReportquirescsiService;
import com.leadics.suzukithdashboard.service.LIReportquiresssiService;
import com.leadics.suzukithdashboard.to.LIReportquirescsiRecord;
import com.leadics.suzukithdashboard.to.LIReportquiresssiRecord;
import com.leadics.utils.LogUtils;
import com.leadics.utils.PropertyUtil;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author varma.sagi
 */
public class SOPPerformance {
 String langCode = "EN";

    static LogUtils logger = new LogUtils(PriorityAnalysis.class.getName());

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {
                String lang = request.getParameter("langCode");
                if (lang != null) {
                    langCode = lang;
                }
                if (!StringUtils.isNullOrEmpty(actionType)) {
                    if (actionType.equals("getSOPPerformance")) {
                        getSOPPerformance(request, response);
                        return;
                    }
                    if (actionType.equals("getSOPPerformanceImpact")) {
                        getSOPPerformanceImpact(request, response);
                        return;
                    }
                    if (actionType.equals("getSOPTrend")) {
                        getSOPTrend(request, response);
                        return;
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    public void getSOPTrend(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        String periodSwitch = request.getParameter("period");
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getSOPTrendRegion'");

        if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Year")) {
            queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getSOPTrendRegionYear'");
        } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Quarter")) {
            queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getSOPTrendRegionQuarter'");
        } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Month")) {
            queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getSOPTrendRegionMonth'");
        }
        String strQuery = queryRecord.getQuery();
        String strCountQuery = queryRecord.getCountquery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> filterList = new ArrayList<String>();
        List<String> bestFilterList = new  ArrayList<String>();

        // filterList.add("regiona");
        filterList.add("year");
        filterList.add("attribute");
        filterList.add("model");
        filterList.add("state");
        filterList.add("dealer");
        
        bestFilterList.add("year");
        bestFilterList.add("quarter");
        bestFilterList.add("attribute");
        bestFilterList.add("model");

        if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Year")) {
            filterList.add("year");
        } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Month")) {
            filterList.add("month");
        }
        if (!langCode.equalsIgnoreCase("EN")) {
//            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }
        
        

        String where_condtion = service.buildWherecondition(filterList, request);
        String best_where_condtion = service.buildWherecondition(bestFilterList, request);
        
        
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strBestWhereCluse, best_where_condtion, true);
        
        strCountQuery = StringUtils.replaceString(strCountQuery, service.strWhereCluse, where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);

        String regionName = "region";
        String zoneName = "zone";
        String mainSelection = "Study Total";
        
         if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
            mainSelection = service.getTranslateString(mainSelection, langCode);
        }

        String strRegion = request.getParameter("region");
        if (strRegion != null && !strRegion.equalsIgnoreCase("ALL") && !strRegion.equalsIgnoreCase("Study Total")) {
            strQuery = StringUtils.replaceString(strQuery, "!region!", "region like \"" + strRegion + "\"", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!region!", "region like \"" + strRegion + "\"", true);
            regionName = strRegion;
            mainSelection = strRegion;

            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
            regionName = service.getTranslateString(strRegion, langCode);
             mainSelection = regionName;
        }
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!region!", " 1=1 ", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!region!", " 1=1 ", true);
        }

        String dealergroup = request.getParameter("dealergroup");
//        System.out.println(dealergroup);

        if (dealergroup != null && !dealergroup.equalsIgnoreCase("ALL") && !dealergroup.equalsIgnoreCase("Study Total")) {
            strQuery = StringUtils.replaceString(strQuery, "!dealergroup!", "dealergroup like '" + dealergroup + "'", true);
            strQuery = StringUtils.replaceString(strQuery, "!state!", "1=1", true);
            strCountQuery = StringUtils.replaceString(strCountQuery,  "!dealergroup!", "dealergroup like '" + dealergroup + "'", true);
             strCountQuery = StringUtils.replaceString(strCountQuery,  "!state!", "1=1", true);
             regionName = dao.loadString("SELECT DISTINCT region FROM sop_aggregate WHERE dealergroup like '" + dealergroup + "' limit 1");
//            if (strRegion.equalsIgnoreCase("ALL") || strRegion.equalsIgnoreCase("Study Total")) {
            mainSelection = dealergroup;
//            };
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
            regionName = service.getTranslateString(dealergroup, langCode);
             mainSelection = regionName;
        }
            
         
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!dealergroup!", " 1=1 ", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!dealergroup!", " 1=1 ", true);
        }

        String city = request.getParameter("city");
        String strArea = request.getParameter("state");
//        System.out.println(city);

        if (city != null && !city.equalsIgnoreCase("ALL") && !city.equalsIgnoreCase("Study Total")) {
            strQuery = StringUtils.replaceString(strQuery, "!city!", "city like \"" + city + "\"", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!city!", "city like \"" + city + "\"", true);
            regionName = dao.loadString("SELECT DISTINCT region FROM sop_aggregate WHERE city like '" + city + "' limit 1");
//            if (strRegion.equalsIgnoreCase("ALL") || strRegion.equalsIgnoreCase("Study Total")) {
            mainSelection = city;
//            };

        } else {
            strQuery = StringUtils.replaceString(strQuery, "!city!", " 1=1 ", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!city!", " 1=1 ", true);
        }
        
        
        if (strArea != null && !strArea.equalsIgnoreCase("ALL") && !strArea.equalsIgnoreCase("Study Total")) {
            strQuery = StringUtils.replaceString(strQuery, "!state!", "1=1", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!state!", "area like \"" + strArea + "\"", true);
            regionName = dao.loadString("SELECT DISTINCT region FROM sop_aggregate WHERE area like '" + strArea + "' limit 1");
//            if (strRegion.equalsIgnoreCase("ALL") || strRegion.equalsIgnoreCase("Study Total")) {
            mainSelection = strArea;
//            };

        } else {
            strQuery = StringUtils.replaceString(strQuery, "!city!", " 1=1 ", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!city!", " 1=1 ", true);
        }
        String strDealer = request.getParameter("dealer");
        String strCompareTo = request.getParameter("compareto");
        String dgroup = request.getParameter("dealergroup");

        if (strDealer != null && !strDealer.equalsIgnoreCase("ALL") && !strDealer.equalsIgnoreCase("Study Total")) {
            strQuery = StringUtils.replaceString(strQuery, "!dealer!", "dealer like '" + strDealer + "'", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!dealer!", "dealer like '" + strDealer + "'", true);
            regionName = dao.loadString("SELECT DISTINCT region FROM sop_aggregate WHERE dealer like '" + strDealer + "' limit 1");
            //         zoneName = dao.loadString("SELECT DISTINCT zone FROM sop_aggregate WHERE dealer like '" + strDealer + "' limit 1");
            mainSelection = strDealer;
            
             if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
            regionName = service.getTranslateString(regionName, langCode);
             mainSelection = service.getTranslateString(strDealer, langCode);
            
        }
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!dealer!", " 1=1 ", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!dealer!", " 1=1 ", true);
        }
        
        

//         if (dgroup != null && !dgroup.equalsIgnoreCase("ALL") && !dgroup.equalsIgnoreCase("Study Total")) {
//             //dealergroup
//            strQuery = StringUtils.replaceString(strQuery, "!dealergroup!", "dealergroup like '" + dgroup + "'", true);
//            strCountQuery = StringUtils.replaceString(strCountQuery, "!dealergroup!", "dealergroup like '" + dgroup + "'", true);
//            regionName = dao.loadString("SELECT DISTINCT region FROM sop_aggregate WHERE dealergroup like '" + dgroup + "' limit 1");
//            //         zoneName = dao.loadString("SELECT DISTINCT zone FROM sop_aggregate WHERE dealer like '" + strDealer + "' limit 1");
//            mainSelection = dgroup;
//            
//             if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
//            regionName = service.getTranslateString(regionName, langCode);
//             mainSelection = service.getTranslateString(dgroup, langCode);
//            
//        }
//        } else {
//            strQuery = StringUtils.replaceString(strQuery, "!dealergroup!", " 1=1 ", true);
//            strCountQuery = StringUtils.replaceString(strCountQuery, "!dealergroup!", " 1=1 ", true);
//        }
         
         
         
        String averaget = " Average";
        String bestt = "Best ";
        String avgString = "Study Total Average";
        String BestString = "Best Overall";
        
        if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
            averaget = service.getTranslateString(averaget, langCode);
             bestt = service.getTranslateString(bestt, langCode);
             avgString = service.getTranslateString(avgString, langCode);
        }

        
        
        
        if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strRegion != null && !strRegion.equalsIgnoreCase("ALL") && !strRegion.equalsIgnoreCase("Study Total")) {
            
            //region
            avgString = regionName + averaget;
            BestString = bestt + regionName;
            strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", "region like \"" + strRegion + "\"", true);
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strDealer != null && !strDealer.equalsIgnoreCase("ALL") && !strDealer.equalsIgnoreCase("Study Total")) {
            //dealer
            avgString = regionName + averaget;
            BestString = bestt + regionName;
            strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", " region like (SELECT DISTINCT region FROM sop_aggregate WHERE dealer like '" + strDealer + "' limit 1) ", true);
        }        else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strArea != null && !strArea.equalsIgnoreCase("ALL") && !strArea.equalsIgnoreCase("Study Total")) {
                  
            //area
            avgString = regionName + averaget;
                  BestString = bestt + regionName;
                 strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", " region like (SELECT DISTINCT region FROM sop_aggregate WHERE area like '" + strArea + "' limit 1) ", true);
              } 
      
        
         else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && dgroup != null && !dgroup.equalsIgnoreCase("ALL") && !dgroup.equalsIgnoreCase("Study Total")) {
                  
            //dealergroup
                    avgString = regionName + averaget;
                  BestString = bestt + regionName;
                 strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", " region like (SELECT DISTINCT region FROM sop_aggregate WHERE dealergroup like '" + dgroup + "' limit 1) ", true);
              } 
        else{
            strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", " 1=1 ", true);
        }

        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.load5Vals(strQuery, "period", "period", "score", "score", "RegionAvg", "RegionAvg", "RegionBest", "RegionBest", "samplecount", "samplecount");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject jsonobj = new JSONObject();
            JSONObject studypos = new JSONObject();
             JSONObject bestpos = new JSONObject();
            String period = (String) tmpJson.get("period");
            String score = (String) tmpJson.get("score");
            String average = (String) tmpJson.get("RegionAvg");
            String best = (String) tmpJson.get("RegionBest");
            String scount = (String) tmpJson.get("samplecount");

            Integer count = Integer.valueOf(Integer.parseInt(scount));
            String asterisk = "";
            if (count < 10) {
                asterisk = "**";
            } else if (count >= 10 && count < 20) {
                asterisk = "*";
            } else {
                asterisk = "";
            }

            jsonobj.put("name", period + asterisk);
            jsonobj.put("bartitle", score + "%");
            jsonobj.put("y", Integer.parseInt(score));
            jsonobj.put("tooltip", period + " <br> <b>" + score + "%</b>");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                period = service.getTranslateString(period, langCode);
                BestString = service.getTranslateString(BestString, langCode);
                avgString = service.getTranslateString(avgString, langCode);
                mainSelection = service.getTranslateString(mainSelection, langCode);
                System.out.println("Dealer:" + period);
            }

            jsonobj.put("mainSelectionName", mainSelection);
            jsonobj.put("name", period + asterisk);
            jsonobj.put("linename", avgString);
            if (average == null) {
                jsonobj.put("linescore", null);
            } else {
                jsonobj.put("linescore", Integer.parseInt(average));
            }
            
            
//            if (rgionbest != null && !rgionbest.equalsIgnoreCase("0") && !rgionbest.equalsIgnoreCase("null")) {
//                bestpos.put("x", j + 0.00001);
//                bestpos.put("y", Integer.parseInt(rgionbest));
//                bestpos.put("label", rgionbest);
//                bestpos.put("bartitle", BestString + "<br></br>" + rgionbest + "%");
//                rgbest.add(bestpos);
//            } else {
//                rgbest.add(null);
//            }
//            if (regionavg != null && !regionavg.equalsIgnoreCase("0") && !regionavg.equalsIgnoreCase("null")) {
//                studypos.put("y", Integer.parseInt(regionavg));
//                studypos.put("label", regionavg);
//                studypos.put("bartitle", avgString + "<br></br>" + regionavg + "%");
//                rgstudy.add(studypos);
//            } else {
//                rgstudy.add(null);
//            }
            

            jsonobj.put("bestname", BestString);
            if (best == null) {
                jsonobj.put("bestvalue", null);
                jsonobj.put("bestvalueName", null);
            } else {
                jsonobj.put("bestvalue", Integer.parseInt(best));
                jsonobj.put("bestvalueName",  best + "%");
                jsonobj.put("bestvaluetooltip", BestString + "<br></br>" + best + "%");
            }

            finalArray.add(jsonobj);
        }

        String count = dao.loadString(strCountQuery);
        JSONObject obj = new JSONObject();
        obj.put("samplecount", count);
        obj.put("data", finalArray);
        out.println(obj.toJSONString());
        return;
    }

//    public void getSOPPerformance(HttpServletRequest request, HttpServletResponse response) throws Exception {
//       PrintWriter out = response.getWriter();
//        LIReportquirescsiService queryService = new LIReportquirescsiService();
//        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getKPIPerformance'");
//        String strQuery = queryRecord.getQuery();
//        String strCountQuery = queryRecord.getCountquery();
//        LIDAO dao = new LIDAO();
//        LIService service = new LIService();
//        List<String> filterList = new ArrayList<String>();
////       
//        filterList.add("year");
//        filterList.add("regiona");
//        filterList.add("quarter");
//        filterList.add("model");
//        filterList.add("region");
//        filterList.add("dealer");
//
//        if (!langCode.equalsIgnoreCase("EN")) {
//            System.out.println("Lang Code :" + langCode);
//            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
//        }
//        List<String> countFilterList = new ArrayList<String>();
//        countFilterList.add("year");
//        countFilterList.add("biannual");
//        countFilterList.add("month");
//        countFilterList.add("dealer");
//        countFilterList.add("region");
//        countFilterList.add("quarter");
//        String countWhereCondtion = service.buildCSIWherecondition(countFilterList, request);
//        String smplcount = dao.loadString("select sum(totalcount) from sop_aggregate where 1=1 " + countWhereCondtion + " group by attribute limit 1");
//        int samplecount = 0;
//        System.out.println("smplcount :" + smplcount);
//        if (smplcount != null) {
//            samplecount = Integer.parseInt(smplcount);
//            System.out.println("samplecount      :" + samplecount);
//        }
//        String where_condtion = service.buildWherecondition(filterList, request);
//        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
//        strQuery = StringUtils.replaceString(strQuery, service.strBestWhereCluse, where_condtion, true);
//        strQuery = StringUtils.replaceString(strQuery, service.strStudyWhereCluse, where_condtion, true);
//        
//        strCountQuery = StringUtils.replaceString(strCountQuery, service.strWhereCluse, where_condtion, true);
//        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
//        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);
//        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
//        String strDealer = request.getParameter("dealer");
//        String strRegion = request.getParameter("region");
//        String dealergroup = request.getParameter("dealergroup");
//        String strCity = request.getParameter("city");
//        String regionName = "region";
//        String zoneName = "zone";
//        String mainSelection = "Study Total";
//        String strCompareTo = request.getParameter("compareto");
//
//        if (strRegion != null && !strRegion.equalsIgnoreCase("ALL") && !strRegion.equalsIgnoreCase("Study Total")) {
//            strQuery = StringUtils.replaceString(strQuery, "!region!", "region like \"" + strRegion + "\"", true);
//            strCountQuery = StringUtils.replaceString(strCountQuery, "!region!", "region like \"" + strRegion + "\"", true);
//            regionName = strRegion;
//            mainSelection = strRegion;
////            zoneName = dao.loadString("SELECT DISTINCT zone FROM sop_aggregate WHERE region like \"" + strRegion + "\" limit 1");
//        } else {
//            strQuery = StringUtils.replaceString(strQuery, "!region!", " 1=1 ", true);
//            strCountQuery = StringUtils.replaceString(strCountQuery, "!region!", " 1=1 ", true);
//        }
//
//        String strZone = request.getParameter("dealergroup");
//        // System.out.println("dealergrouppppp"+strZone);
//        if (dealergroup != null && !dealergroup.equalsIgnoreCase("ALL") && !dealergroup.equalsIgnoreCase("Study Total")) {
//            strQuery = StringUtils.replaceString(strQuery, "!state!", "dealergroup like \"" + dealergroup + "\"", true);
//            strCountQuery = StringUtils.replaceString(strCountQuery, "!state!", "dealergroup like \"" + dealergroup + "\"", true);
//            zoneName = dealergroup;
////            if (strRegion.equalsIgnoreCase("ALL") || strRegion.equalsIgnoreCase("Study Total")) {
//                mainSelection = dealergroup;
////            }
//
//        } else {
//            strQuery = StringUtils.replaceString(strQuery, "!state!", " 1=1 ", true);
//            strCountQuery = StringUtils.replaceString(strCountQuery, "!state!", " 1=1 ", true);
//        }
//        if (strCity != null && !strCity.equalsIgnoreCase("ALL") && !strCity.equalsIgnoreCase("Study Total")) {
//           strQuery = StringUtils.replaceString(strQuery, "!city!", "city like '" + strCity + "'", true);
//           strCountQuery = StringUtils.replaceString(strCountQuery, "!city!", "city like '" + strCity + "'", true);
//           regionName = dao.loadString("SELECT DISTINCT region FROM sop_aggregate WHERE city like '" + strCity + "' limit 1");
////            zoneName = dao.loadString("SELECT DISTINCT zone FROM sop_aggregate WHERE dealer like \"" + strDealer + "\" limit 1");
//           mainSelection = strCity;
//       } else {
//           strQuery = StringUtils.replaceString(strQuery, "!city!", " 1=1 ", true);
//           strCountQuery = StringUtils.replaceString(strCountQuery, "!city!", " 1=1 ", true);
//       }
//        if (strDealer != null && !strDealer.equalsIgnoreCase("ALL") && !strDealer.equalsIgnoreCase("Study Total")) {
//            strQuery = StringUtils.replaceString(strQuery, "!dealer!", "dealer like '" + strDealer + "'", true);
//            strCountQuery = StringUtils.replaceString(strCountQuery, "!dealer!", "dealer like '" + strDealer + "'", true);
//            regionName = dao.loadString("SELECT DISTINCT region FROM sop_aggregate WHERE dealer like '" + strDealer + "' limit 1");
////            zoneName = dao.loadString("SELECT DISTINCT zone FROM sop_aggregate WHERE dealer like \"" + strDealer + "\" limit 1");
//            mainSelection = strDealer;
//        } else {
//            strQuery = StringUtils.replaceString(strQuery, "!dealer!", " 1=1 ", true);
//            strCountQuery = StringUtils.replaceString(strCountQuery, "!dealer!", " 1=1 ", true);
//        }
//
//        String average = " Average";
//        String best = "Best ";
//        String avgString = "Study Total Average";
//        String BestString = "Best Overall";
////        if(langCode!=null && !langCode.equalsIgnoreCase("EN")){
////                 average = "เฉลี่ย";
////                 avgString="การศึกษาเฉลี่ย";
////            }
//       if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strRegion != null && !strRegion.equalsIgnoreCase("ALL") && !strRegion.equalsIgnoreCase("Study Total")) {
//            avgString = regionName + average;
//            BestString = best + regionName;
//            strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", "region like \"" + strRegion + "\"", true);
//        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strDealer != null && !strDealer.equalsIgnoreCase("ALL") && !strDealer.equalsIgnoreCase("Study Total")) {
//            avgString = regionName + average;
//            BestString = best + regionName;
//            strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", " region like (SELECT DISTINCT region FROM sop_aggregate WHERE dealer like '" + strDealer + "' limit 1) ", true);
//        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strCity != null && !strCity.equalsIgnoreCase("ALL") && !strCity.equalsIgnoreCase("Study Total")) {
//            avgString = regionName + average;
//            BestString = best + regionName;
//            strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", " region like (SELECT DISTINCT region FROM sop_aggregate WHERE city like '" + strCity + "' limit 1) ", true);
//        }else {
//            strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", " 1=1 ", true);
//        }
//
//        JSONArray jsonArray = dao.load4Vals(strQuery, "attribute", "attribute", "dealerscore", "dealerscore", "regionavg", "regionavg", "regionbest", "regionbest");
//        Iterator i = jsonArray.iterator();
//        JSONArray finalArray = new JSONArray();
//        while (i.hasNext()) {
//            JSONObject tmpJson = (JSONObject) i.next();
//            JSONObject jsonobj = new JSONObject();
//            String attribute = (String) tmpJson.get("attribute");
//            String dealerscore = (String) tmpJson.get("dealerscore");
//            String regionavg = (String) tmpJson.get("regionavg");
//            String rgionbest = (String) tmpJson.get("regionbest");
//            System.out.println("regionavg:" + regionavg);
//            System.out.println("rgionbest:" + rgionbest);
////            if (regionavg == null) {
////                regionavg = "0";
////            }
////            if (rgionbest == null) {
////                rgionbest = "0";
////            }
//            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
//                attribute = service.getTranslateString(attribute, langCode);
//                BestString = service.getTranslateString(BestString, langCode);
//                avgString = service.getTranslateString(avgString, langCode);
//                mainSelection = service.getTranslateString(mainSelection, langCode);
////                    strWorstString = service.getTranslateString(strWorstString, langCode);
//                System.out.println("Dealer:" + attribute);
//            }
//            if (samplecount < 10) {
//                mainSelection = mainSelection + "**";
//            } else if (samplecount < 30) {
//                mainSelection = mainSelection + "*";
//            }
//            jsonobj.put("name", attribute);
//            jsonobj.put("mainSelectionName", mainSelection);
//            jsonobj.put("bartitle", dealerscore + "%");
//            jsonobj.put("y", Integer.parseInt(dealerscore));
//            jsonobj.put("tooltip", attribute + "<br>" + dealerscore + "%");
//            jsonobj.put("scatterbest", BestString);
//            jsonobj.put("scatteravg", avgString);
//            if (regionavg == null) {
//                jsonobj.put("svalue", null);
//            } else {
//                jsonobj.put("svalue", Integer.parseInt(regionavg));
//            }
//
//            if (rgionbest == null) {
//                jsonobj.put("bvalue", null);
//            } else {
//                jsonobj.put("bvalue", Integer.parseInt(rgionbest));
//            }
//
//            finalArray.add(jsonobj);
//        }
//        String count = dao.loadString(strCountQuery);
//        JSONObject obj = new JSONObject();
//        obj.put("samplecount", count);
//        obj.put("data", finalArray);
//        out.println(obj.toJSONString());
//        return;
//    }
    public void getSOPPerformance(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getKPIPerformance'");
        String strQuery = queryRecord.getQuery();
        String strCountQuery = queryRecord.getCountquery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        List<String> filterList = new ArrayList<String>();
        List<String> bestFilterList = new ArrayList<String>();
        List<String> studyFilterList = new ArrayList<String>();
//       
        filterList.add("year");
//        filterList.add("quarter");
//        filterList.add("regiona");
        filterList.add("model");
//        filterList.add("regiona");
        filterList.add("dealer");
        filterList.add("dealergroup");
        filterList.add("state");
        filterList.add("city");
//        filterList.add("month");

        //studay Total average
        studyFilterList.add("year");
         studyFilterList.add("quarter");
         studyFilterList.add("attribute");
         studyFilterList.add("model");
         
        
//        studyFilterList.add("regiona");
       
//        studyFilterList.add("model");

//        bestFilterList.add("regiona");
        bestFilterList.add("year");
        bestFilterList.add("quarter");
        bestFilterList.add("attribute");
        bestFilterList.add("model");

        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }
        List<String> countFilterList = new ArrayList<String>();
//        countFilterList.add("year");
//        countFilterList.add("biannual");
//        countFilterList.add("month");
        countFilterList.add("dealer");
        countFilterList.add("region");
        countFilterList.add("city");
//        countFilterList.add("quarter");
        String countWhereCondtion = service.buildCSIWherecondition(countFilterList, request);
        String smplcount = dao.loadString("select sum(totalcount) from sop_aggregate where 1=1 " + countWhereCondtion + " group by attribute limit 1");
        int samplecount = 0;
        System.out.println("smplcount :" + smplcount);
        if (smplcount != null) {
            samplecount = Integer.parseInt(smplcount);
            System.out.println("samplecount      :" + samplecount);
        }
        String where_condtion = service.buildWherecondition(filterList, request);
        String study_where_condtion = service.buildWherecondition(studyFilterList, request);
        String best_where_condtion = service.buildWherecondition(bestFilterList, request);
       
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strStudyWhereCluse, study_where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strBestWhereCluse, best_where_condtion, true);
        strCountQuery = StringUtils.replaceString(strCountQuery, service.strWhereCluse, where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        String strDealer = request.getParameter("dealer");
        String strDealergrp  = request.getParameter("dealergroup");
        String strRegion = request.getParameter("region");
        String regionName = "region";
        String zoneName = "zone";
        String mainSelection = "Study Total";
        String strCompareTo = request.getParameter("compareto");
        String jdpap = request.getParameter("regiona");
        String city = request.getParameter("city");

        
               if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                     mainSelection = service.getTranslateString(mainSelection, langCode);
                }
        
        ////////////////  mainSelection ///////////////
        if (strRegion != null && !strRegion.equalsIgnoreCase("ALL") && !strRegion.equalsIgnoreCase("Study Total")) {//strCompareTo.equalsIgnoreCase("Region") &&
            strQuery = StringUtils.replaceString(strQuery, "!region!", "region like \"" + strRegion + "\"", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!region!", "region like \"" + strRegion + "\"", true);
            regionName = strRegion;
            mainSelection = strRegion;
//            zoneName = dao.loadString("SELECT DISTINCT zone FROM sop_aggregate WHERE region like \"" + strRegion + "\" limit 1");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                regionName = service.getTranslateString(strRegion, langCode);
                mainSelection = regionName;
            }
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!region!", " 1=1 ", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!region!", " 1=1 ", true);
        }

        String strArea = request.getParameter("state");
        if (strArea != null && !strArea.equalsIgnoreCase("ALL") && !strArea.equalsIgnoreCase("Study Total")) {
            strQuery = StringUtils.replaceString(strQuery, "!state!", "area like \"" + strArea + "\"", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!state!", "area like \"" + strArea + "\"", true);
            regionName = dao.loadString("SELECT DISTINCT region FROM sop_aggregate WHERE area like '" + strArea + "' limit 1");
            mainSelection = strArea;

//            if (strRegion.equalsIgnoreCase("ALL") || strRegion.equalsIgnoreCase("Study Total")) {
//            }
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!state!", " 1=1 ", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!state!", " 1=1 ", true);
        }

        if (city != null && !city.equalsIgnoreCase("ALL") && !city.equalsIgnoreCase("Study Total")) {
            strQuery = StringUtils.replaceString(strQuery, "!city!", "city like \"" + city + "\"", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!city!", "city like \"" + city + "\"", true);
            regionName = city;
            mainSelection = city;

//            zoneName = dao.loadString("SELECT DISTINCT zone FROM sop_aggregate WHERE region like \"" + strRegion + "\" limit 1");
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!city!", " 1=1 ", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!city!", " 1=1 ", true);
        }

        if (strDealer != null && !strDealer.equalsIgnoreCase("ALL") && !strDealer.equalsIgnoreCase("Study Total")) {
            strQuery = StringUtils.replaceString(strQuery, "!dealer!", "dealer like '" + strDealer + "'", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!dealer!", "dealer like '" + strDealer + "'", true);
            if (city != null && !city.equalsIgnoreCase("ALL") && !city.equalsIgnoreCase("Study Total")) {
                regionName = dao.loadString("SELECT DISTINCT city FROM sop_aggregate WHERE dealer like '" + strDealer + "' limit 1");
            } else {
                regionName = dao.loadString("SELECT DISTINCT region FROM sop_aggregate WHERE dealer like '" + strDealer + "' limit 1");
            }
            //            zoneName = dao.loadString("SELECT DISTINCT zone FROM sop_aggregate WHERE dealer like \"" + strDealer + "\" limit 1");
            mainSelection = strDealer;
              if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                regionName = service.getTranslateString(regionName, langCode);
                 mainSelection = service.getTranslateString(mainSelection, langCode);
            }
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!dealer!", " 1=1 ", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!dealer!", " 1=1 ", true);
        }
        
        if (strDealergrp != null && !strDealergrp.equalsIgnoreCase("ALL") && !strDealergrp.equalsIgnoreCase("Study Total")) {
            strQuery = StringUtils.replaceString(strQuery, "!dealergroup!", "dealergroup like '" + strDealergrp + "'", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!dealergroup!", "dealergroup like '" + strDealergrp + "'", true);
            regionName = dao.loadString("SELECT DISTINCT region FROM sop_aggregate WHERE dealergroup like '" + strDealergrp + "' limit 1");
           
            //            zoneName = dao.loadString("SELECT DISTINCT zone FROM sop_aggregate WHERE dealer like \"" + strDealer + "\" limit 1");
            mainSelection = strDealergrp;
              if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                regionName = service.getTranslateString(regionName, langCode);
                 mainSelection = service.getTranslateString(mainSelection, langCode);
            }
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!dealergroup!", " 1=1 ", true);
            strCountQuery = StringUtils.replaceString(strCountQuery, "!dealergroup!", " 1=1 ", true);
        }
//        
        String average = " Average";
        String best = "Best ";
        String avgString = "Study Total ";
        String BestString = "Best Overall";
//     
           if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
            average = service.getTranslateString(average, langCode);
            best = service.getTranslateString(best, langCode);
            avgString = service.getTranslateString(avgString, langCode);
            BestString = service.getTranslateString(BestString, langCode);
        }
              
////////// avgString & BestString /////////////

//
//        if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strDealergrp != null && !strDealergrp.equalsIgnoreCase("ALL") && !strDealergrp.equalsIgnoreCase("Study Total")) {
//            //dealergroup
//            regionName = dao.loadString("SELECT DISTINCT region FROM sop_aggregate WHERE dealergroup like '" + strDealergrp + "' limit 1");
//
////                jdpap = dao.loadString("SELECT DISTINCT region FROM sop_aggregate WHERE city like '" + city + "' limit 1");
//            strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", "region like \"" + regionName + "\"", true);
//            jdpap = "";
//
//            avgString = regionName + average;
//            BestString = best + " " + regionName;
//             System.out.println("***********************  212121233      :" );
//        }
       if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strDealer != null && !strDealer.equalsIgnoreCase("ALL") && !strDealer.equalsIgnoreCase("Study Total")) {

            //dealer
            jdpap = dao.loadString("SELECT DISTINCT region FROM sop_aggregate WHERE dealer like '" + strDealer + "' limit 1");
            regionName = dao.loadString("SELECT DISTINCT region FROM sop_aggregate WHERE dealer like '" + strDealer + "' limit 1");
//               
            strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", " region = '" + regionName + "'AND region = '" + jdpap + "' ", true);

            avgString = regionName + average;
            mainSelection = strDealer;
            BestString = best + " " + regionName;
             System.out.println("***********************  1      :" );
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strArea != null && !strArea.equalsIgnoreCase("ALL") && !strArea.equalsIgnoreCase("Study Total")) {
            //area
            regionName = dao.loadString("SELECT DISTINCT region FROM sop_aggregate WHERE area like '" + strArea + "' limit 1");

//                jdpap = dao.loadString("SELECT DISTINCT region FROM sop_aggregate WHERE city like '" + city + "' limit 1");
            strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", "region like \"" + regionName + "\"", true);
            jdpap = "";

            avgString = regionName + average;
            BestString = best + " " + regionName;
             System.out.println("***********************  2      :" );
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strRegion != null && !strRegion.equalsIgnoreCase("ALL") && !strRegion.equalsIgnoreCase("Study Total")) {
            //region
            strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", "region like \"" + strRegion + "\"", true);
//                jdpap = dao.loadString("SELECT DISTINCT region FROM sop_aggregate WHERE region like '" + strRegion + "' limit 1");
            jdpap = "";

            avgString = regionName + average;
            BestString = best + " " + regionName;
             System.out.println("***********************  3      :" );
        } 
        
        
        
        
        
        else if (strCompareTo != null && jdpap != null && !jdpap.equalsIgnoreCase("All") && jdpap != null) {
//            strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", "region = '" + jdpap + "' ", true);
             strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", " 1=1 ", true);
//            avgString = jdpap + " " + average;
//            BestString = jdpap + " " + BestString;
             System.out.println("*********************** 4      :" );
        }
        else {
            if (strDealer != null && !strDealer.equalsIgnoreCase("All")) {
                jdpap = dao.loadString("SELECT DISTINCT region FROM sop_aggregate WHERE dealer like '" + strDealer + "' limit 1");
//             
                strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", " 1=1 ", true);
                
                 mainSelection = strDealer;
              if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
               
                 mainSelection = service.getTranslateString(mainSelection, langCode);
            }
                
             System.out.println("***********************  5      :" );
            } else {
                strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", " 1=1 ", true);
                
             System.out.println("***********************  6      :" );
            }
        }

        //////////
        int countsample = 0;
        JSONArray jsonArray = dao.load5Vals(strQuery, "attribute", "attribute", "dealerscore", "dealerscore", "regionavg", "regionavg", "regionbest", "regionbest", "impact", "impact");
        Iterator i = jsonArray.iterator();
        ArrayList series1 = new ArrayList();
        ArrayList series2 = new ArrayList();
        ArrayList categories = new ArrayList();
        ArrayList rgbest = new ArrayList();
        ArrayList rgstudy = new ArrayList();
        JSONArray finalArray = new JSONArray();
        int j = 0;
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject addingJson = new JSONObject();
            JSONObject addingJson2 = new JSONObject();
            JSONObject bestpos = new JSONObject();
            JSONObject studypos = new JSONObject();

            JSONObject jsonobj = new JSONObject();
            String attribute = (String) tmpJson.get("attribute");
             String dealerscore =  ((String) tmpJson.get("dealerscore"))!=null ?((String) tmpJson.get("dealerscore")):"0";
//            String dealerscore = (String) tmpJson.get("dealerscore");
            String regionavg = (String) tmpJson.get("regionavg");
            String rgionbest = (String) tmpJson.get("regionbest");
            String impact = (String) tmpJson.get("impact");
            String second_score = "200";
//            Double regionavgpos = Double.parseDouble((String) tmpJson.get("regionavgpos"));
//            Double rgionbestpos = Double.parseDouble((String) tmpJson.get("regionbestpos"));

//            if (regionavg == null) {
//                regionavg = "0";
//            }
//            if (rgionbest == null) {
//                rgionbest = "0";
//            }
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                attribute = service.getTranslateString(attribute, langCode);
                BestString = service.getTranslateString(BestString, langCode);
                avgString = service.getTranslateString(avgString, langCode);
                mainSelection = service.getTranslateString(mainSelection, langCode);
                impact = service.getTranslateString(impact, langCode);
                System.out.println("language trans of impact : "+ impact);
//                    strWorstString = service.getTranslateString(strWorstString, langCode);

            }
            // Star appending  / * for main selection
            if (countsample == 0) {
                if (samplecount < 10) {
                    mainSelection = mainSelection + "**";
                } else if (samplecount < 30) {
                    mainSelection = mainSelection + "*";
                }
                countsample++;
            }
            // End appending  / * for main selection
            if (rgionbest != null && !rgionbest.equalsIgnoreCase("0") && !rgionbest.equalsIgnoreCase("null")) {
                bestpos.put("x", j + 0.00001);
                bestpos.put("y", Integer.parseInt(rgionbest));
                bestpos.put("label", rgionbest);
                bestpos.put("bartitle", BestString + "<br></br>" + rgionbest + "%");
                rgbest.add(bestpos);
            } else {
                rgbest.add(null);
            }
            if (regionavg != null && !regionavg.equalsIgnoreCase("0") && !regionavg.equalsIgnoreCase("null")) {
                studypos.put("y", Integer.parseInt(regionavg));
                studypos.put("label", regionavg);
                studypos.put("bartitle", avgString + "<br></br>" + regionavg + "%");
                rgstudy.add(studypos);
            } else {
                rgstudy.add(null);
            }
            categories.add(attribute);
            if (dealerscore != null && !dealerscore.equalsIgnoreCase("0") && !dealerscore.equalsIgnoreCase("null")) {
                addingJson2.put('y', Integer.parseInt(dealerscore));
            } else {
                addingJson2.put('y', 0);
            }

            addingJson2.put("label", dealerscore + '%');
            addingJson2.put("bartitle", attribute + "<br></br>" + dealerscore + "%");
            series2.add(addingJson2);
            if (second_score != null && !second_score.equalsIgnoreCase("0") && !second_score.equalsIgnoreCase("null")) {
                addingJson.put("y", Integer.parseInt(second_score));
            } else {
                addingJson.put("y", null);
            }

            addingJson.put("label", impact);

            series1.add(addingJson);
            j++;
        }
        HashMap seriesdata1 = new HashMap();
        HashMap seriesdata2 = new HashMap();
        HashMap rgstudydata = new HashMap();
        HashMap rgbestdata = new HashMap();
        seriesdata1.put("name", "Imapctseries");
        seriesdata1.put("data", series1);
        seriesdata2.put("name", mainSelection);
        seriesdata2.put("data", series2);
        rgstudydata.put("name", avgString);
        rgstudydata.put("data", rgstudy);
        rgbestdata.put("name", BestString);
        rgbestdata.put("data", rgbest);
        finalArray.add(seriesdata1);
        finalArray.add(seriesdata2);
        finalArray.add(rgbestdata);
        finalArray.add(rgstudydata);
        String count = dao.loadString(strCountQuery);
        JSONObject obj = new JSONObject();
        obj.put("samplecount", count);
        obj.put("categories", categories);
        obj.put("data", finalArray);
        out.println(obj.toJSONString());
        return;
    }

    private void getSOPPerformanceImpact(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getSOPPerformanceImpact'");
        String strQuery = queryRecord.getQuery();
        //String strCountQuery = queryRecord.getCountquery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        List<String> filterList = new ArrayList<String>();
//        filterList.add("year");
//        //filterList.add("regiona");
//        filterList.add("quarter");
//        filterList.add("model");

        if (!langCode.equalsIgnoreCase("EN")) {
//            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        //strCountQuery = StringUtils.replaceString(strCountQuery, service.strWhereCluse, where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        String strDealer = request.getParameter("dealer");
        String strRegion = request.getParameter("region");
        String regionName = "region";
        String zoneName = "zone";
        String mainSelection = "Study Total";
        String strCompareTo = request.getParameter("compareto");

        if (strRegion != null && !strRegion.equalsIgnoreCase("ALL") && !strRegion.equalsIgnoreCase("Study Total")) {
            strQuery = StringUtils.replaceString(strQuery, "!region!", "region like \"" + strRegion + "\"", true);
//            strCountQuery = StringUtils.replaceString(strCountQuery, "!region!", "region like \"" + strRegion + "\"", true);
            regionName = strRegion;
            mainSelection = strRegion;
            zoneName = dao.loadString("SELECT DISTINCT zone FROM sop_aggregate WHERE region like \"" + strRegion + "\" limit 1");
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!region!", " 1=1 ", true);
//            strCountQuery = StringUtils.replaceString(strCountQuery, "!region!", " 1=1 ", true);
        }

        String strZone = request.getParameter("dealergroup");
        System.out.println("dealergrouppppp" + strZone);
        if (strZone != null && !strZone.equalsIgnoreCase("ALL") && !strZone.equalsIgnoreCase("Study Total")) {
            strQuery = StringUtils.replaceString(strQuery, "!state!", "dealergroup like \"" + strZone + "\"", true);
//            strCountQuery = StringUtils.replaceString(strCountQuery, "!state!", "dealergroup like \"" + strZone + "\"", true);
            zoneName = strZone;
            if (strRegion.equalsIgnoreCase("ALL") || strRegion.equalsIgnoreCase("Study Total")) {
                mainSelection = strZone;
            }

        } else {
            strQuery = StringUtils.replaceString(strQuery, "!state!", " 1=1 ", true);
//            strCountQuery = StringUtils.replaceString(strCountQuery, "!state!", " 1=1 ", true);
        }

        if (strDealer != null && !strDealer.equalsIgnoreCase("ALL") && !strDealer.equalsIgnoreCase("Study Total")) {
            strQuery = StringUtils.replaceString(strQuery, "!dealer!", "dealer like '" + strDealer + "'", true);
//            strCountQuery = StringUtils.replaceString(strCountQuery, "!dealer!", "dealer like '" + strDealer + "'", true);
            regionName = dao.loadString("SELECT DISTINCT region FROM sop_aggregate WHERE dealer like '" + strDealer + "' limit 1");
            zoneName = dao.loadString("SELECT DISTINCT zone FROM sop_aggregate WHERE dealer like \"" + strDealer + "\" limit 1");
            mainSelection = strDealer;
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!dealer!", " 1=1 ", true);
//           strCountQuery = StringUtils.replaceString(strCountQuery, "!dealer!", " 1=1 ", true);
        }

        String average = " Average";
        String best = "Best ";
        String avgString = "Study Total Average";
        String BestString = "Best Overall";
        if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
            average = "เฉลี่ย";
            avgString = "การศึกษาเฉลี่ย";
        }
        if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strRegion != null && !strRegion.equalsIgnoreCase("ALL") && !strRegion.equalsIgnoreCase("Study Total")) {
            avgString = regionName + average;
            BestString = best + regionName;
            strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", "region like \"" + strRegion + "\"", true);
        } else if (strCompareTo != null && strCompareTo.equalsIgnoreCase("region") && strDealer != null && !strDealer.equalsIgnoreCase("ALL") && !strDealer.equalsIgnoreCase("Study Total")) {
            avgString = regionName + average;
            BestString = best + regionName;
            strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", " region like (SELECT DISTINCT region FROM sop_aggregate WHERE dealer like '" + strDealer + "' limit 1) ", true);
        } else {
            strQuery = StringUtils.replaceString(strQuery, "!regiondealer!", " 1=1 ", true);
        }

        JSONArray jsonArray = dao.load3Vals(strQuery, "attribute", "sops", "yesscore", "yesimpact", "noscore", "noimpact");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        JSONArray yesImpactArray = new JSONArray();
        JSONArray noImpactArray = new JSONArray();
        JSONObject tmpObjyes = new JSONObject();
        JSONObject tmpObjno = new JSONObject();
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject jsonobjyes = new JSONObject();
            JSONObject jsonobjno = new JSONObject();
            String attribute = (String) tmpJson.get("attribute");
            String yesScore = (String) tmpJson.get("yesscore");
            String noScore = (String) tmpJson.get("noscore");
            String scount = (String) tmpJson.get("samplecount");

            Integer count = Integer.valueOf(Integer.parseInt(scount));

            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                attribute = service.getTranslateString(attribute, langCode);
                BestString = service.getTranslateString(BestString, langCode);
                avgString = service.getTranslateString(avgString, langCode);
                mainSelection = service.getTranslateString(mainSelection, langCode);
//         strWorstString = service.getTranslateString(strWorstString, langCode);
                System.out.println("Dealer:" + attribute);
            }

            jsonobjyes.put("name", attribute);
            jsonobjyes.put("bartitle", yesScore);
            jsonobjyes.put("tooltip", attribute + "<br>" + yesScore);
            jsonobjno.put("name", attribute);
            jsonobjno.put("bartitle", noScore);
            jsonobjno.put("tooltip", attribute + "<br>" + yesScore);
            jsonobjyes.put("y", Double.parseDouble(yesScore));
            jsonobjno.put("y", Double.parseDouble(noScore));
            yesImpactArray.add(jsonobjyes);
            noImpactArray.add(jsonobjno);

            tmpObjyes.put("name", "Yes Impact");
            tmpObjyes.put("data", yesImpactArray);
            tmpObjno.put("name", "No Impact");
            tmpObjno.put("data", noImpactArray);
        }
        finalArray.add(tmpObjyes);
        finalArray.add(tmpObjno);
        //String count = dao.loadString(strCountQuery);
        JSONObject obj = new JSONObject();
        //obj.put("samplecount", count);
        obj.put("data", finalArray);
        out.println(obj.toJSONString());
        return;
    }
}
