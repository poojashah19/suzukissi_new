/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.suzukithdashboard.webactions;

import com.leadics.suzukithdashboard.controller.LISystemuserController;
import com.leadics.suzukithdashboard.service.LISystemuserService;
import com.leadics.suzukithdashboard.to.LISystemuserRecord;
import com.leadics.suzukithdashboard.service.LIReportquirescsiService;
import com.leadics.suzukithdashboard.to.LIReportquirescsiRecord;
import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.service.LIWebaccesslogService;
import com.leadics.suzukithdashboard.service.LIFailedloginService;
import com.leadics.suzukithdashboard.to.LIFailedloginRecord;
import com.leadics.suzukithdashboard.to.LIWebaccesslogRecord;
import com.leadics.suzukithdashboard.service.LIControllermapService;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import com.leadics.utils.LogUtils;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author varma.sagi
 */
public class SystemuserviewWebactions extends LISystemuserController {

    String langCode = "EN";

    static LogUtils logger = new LogUtils(SystemuserviewWebactions.class.getName());

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {
                super.processWebRequest(request, response, actionType);
                String lang = request.getParameter("langCode");
                if (lang != null) {
                    langCode = lang;
                }

                if (!StringUtils.isNullOrEmpty(actionType)) {
                    if (actionType.equals("Login")) {
                        validateLogin(request, response);
                        return;
                    }
                    if (actionType.equals("logOut")) {
                        logOut(request, response);
                        return;
                    }
                    
                    
                    
                    if (actionType.equals("getLoginData")) {
                        getLoginData(request, response);
                        return;
                    }
                    
                     if (actionType.equals("getWebAccesslogs")) {
                        getWebAccesslogs(request, response);
                        return;
                    }
                    if (actionType.equals("getTitles")) {
                        getTitles(request, response);
                        return;
                    }
                    if (actionType.equals("getFootnotes")) {
                        getFootnote(request, response);
                        return;
                    }
                    if (actionType.equals("getPagenote")) {
                        getPagenote(request, response);
                        return;
                    }
                    if (actionType.equals("getName")) {
                        getName(request, response);
                        return;
                    }
                    if (actionType.equals("getNamethai")) {
                        getNamethai(request, response);
                        return;
                    }
                    if (actionType.equals("getRgionDealers")) {
                        getRgionDealers(request, response);
                        return;
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }


    public void getTitles(HttpServletRequest request, HttpServletResponse response) throws Exception {
//        PrintWriter out = response.getWriter();
//        LIDAO dao = new LIDAO();
//        LIService service = new LIService();
//        String query = " SELECT * FROM titles order by indexvalue";
//
//        JSONArray array = dao.load4Vals(query, "pageCode", "pageCode", "titleCode", "titleCode", "contentEn", "contentEn", "contentTh", "contentTh");
//        Iterator i = array.iterator();
//        HashMap map = new HashMap();
//
//        JSONObject Object = new JSONObject();
//        JSONObject Object1 = new JSONObject();
//        while (i.hasNext()) {
//            JSONObject tmpJson = (JSONObject) i.next();
//
//            String pageCode = tmpJson.get("pageCode").toString();
//            String titleCode = tmpJson.get("titleCode").toString();
//            String contentEn = tmpJson.get("contentEn").toString();
//            String contentTh = tmpJson.get("contentTh").toString();
//            Object.put(pageCode + titleCode, contentEn);
//            
//            Object1.put(pageCode + titleCode, contentTh);
//        }
//
////        JSONObject jsonObj = new JSONObject(map);
//        out.println(Object.toJSONString());
//        out.println("****************************************");
//        out.println(Object1.toJSONString());
//        return;
        
        PrintWriter out = response.getWriter();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query = "  SELECT id,ContentEN,ContentReganal,UniqueID FROM lang_conversion WHERE FieldType='OEM_CSI_Menu'";
        JSONArray array = dao.load4Vals(query, "id", "id", "ContentEN", "ContentEN", "ContentReganal", "ContentReganal", "UniqueID", "UniqueID");
        out.println(array.toJSONString());
        return;        
    }

    public void getRgionDealers(HttpServletRequest request, HttpServletResponse response) throws Exception {

        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getCSIRegionDealers'");
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        List<String> filterList = new ArrayList<String>();
        filterList.add("zone");
        filterList.add("region");
        String where_condtion = service.buildCSIWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        JSONArray jsonArray = dao.load2Vals(strQuery, "region", "region", "dealer", "dealer");
//        added
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject Obj1 = new JSONObject();
            String dealer = (String) tmpJson.get("dealer");
            String region = (String) tmpJson.get("region");
            String dealerEN = (String) tmpJson.get("dealer");
            String regionEN = (String) tmpJson.get("region");
            if (!langCode.equalsIgnoreCase("EN")) {
                System.out.println("Lang Code :" + langCode);
                dealer = service.getTranslateString(dealer, langCode);
                region = service.getTranslateString(region, langCode);

            }
            Obj1.put("region", region);
            Obj1.put("dealer", dealer);
            Obj1.put("regionEN", regionEN);
            Obj1.put("dealerEN", dealerEN);
            finalArray.add(Obj1);
        }

//        adding complete
//        out.println(jsonArray.toJSONString());
        out.println(finalArray.toJSONString());
        return;
    }

    public void getFootnote(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query = "  SELECT id,ContentEN,ContentReganal,UniqueID FROM lang_conversion WHERE FieldType='FootNote'";
        JSONArray array = dao.load4Vals(query, "id", "id", "ContentEN", "ContentEN", "ContentReganal", "ContentReganal", "UniqueID", "UniqueID");
        out.println(array.toJSONString());
        return;
    }
public void logOut(HttpServletRequest request, HttpServletResponse response) throws Exception {

        SessionWebaction.updateLIWebsessionClosedRecord(request.getParameter("loginsessionuserid"),request.getParameter("userSessionId"),"LogOut");
        LIControllermapService service = new LIControllermapService();
        service.auditAcesslogsCheck(request, response, "Logout");
    }
    public void getPagenote(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String query = "  SELECT id,ContentEN,ContentReganal,UniqueID FROM lang_conversion WHERE FieldType='PageNotes'";
        JSONArray array = dao.load4Vals(query, "id", "id", "ContentEN", "ContentEN", "ContentReganal", "ContentReganal", "UniqueID", "UniqueID");
        out.println(array.toJSONString());
        return;
    }

    public void getName(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String english = request.getParameter("english");
        String query = "  SELECT ContentEN, ContentReganal FROM lang_conversion WHERE ContentEN='" + english + "' LIMIT 1";
        JSONArray array = dao.load2Vals(query, "ContentEN", "ContentEN", "ContentReganal", "ContentReganal");
        out.println(array.toJSONString());
        return;
    }

    public void getNamethai(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        String english = request.getParameter("thai");
        String query = "  SELECT ContentEN, ContentReganal FROM lang_conversion WHERE ContentReganal='" + english + "' LIMIT 1";
        JSONArray array = dao.load2Vals(query, "ContentEN", "ContentEN", "ContentReganal", "ContentReganal");
        out.println(array.toJSONString());
        return;
    }

    public void validateLogin(HttpServletRequest request, HttpServletResponse response) throws Exception {

        PrintWriter out = response.getWriter();
        LISystemuserController objLISystemuserController = new LISystemuserController();
        LISystemuserRecord objLISystemuserRecord = objLISystemuserController.loadJSONFormLISystemuserRecord(request, response);
        LISystemuserService objLISystemuserService = new LISystemuserService();
        objLISystemuserRecord = objLISystemuserService.searchFirstLISystemuserRecord(objLISystemuserRecord);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
//sessions
        String sessionId = "N.A";
        String userName = request.getParameter("user_id");
        sessionId = SessionWebaction.createSession(userName);
        System.out.println("sessionId"+sessionId);
        //
        if (objLISystemuserRecord != null) {
            LIWebaccesslogRecord record = new LIWebaccesslogRecord();
            record.setUserid(objLISystemuserRecord.getUserid());
            record.setSource(request.getRemoteAddr());
            record.setCreatedat(sdf.format(new Date()));
            record.setService("login");
            int i = new LIWebaccesslogService().insertLIWebaccesslogRecord(record);
             //krishna session
            JSONObject obj = objLISystemuserRecord.getJSONObject();                     
            obj.put("userSessionId", sessionId);
            out.println(obj.toJSONString());
            //out.println(objLISystemuserRecord.getJSONObject().toJSONString());
            return;
        } else {
            LIFailedloginRecord record = new LIFailedloginRecord();
            record.setUsername(request.getParameter("user_id"));
            record.setSource(request.getRemoteAddr());
            record.setCreatedat(sdf.format(new Date()));
            int i = new LIFailedloginService().insertLIFailedloginRecord(record);
            out.println("Not valid user");
            return;
        }

    }
    
     public void getWebAccesslogs(HttpServletRequest request, HttpServletResponse response) throws Exception {

        PrintWriter out = response.getWriter();

      String  strQuery ="SELECT USER_ID AS dealer ,COUNT(USER_ID) AS logincount,MAX(MODIFIED_AT) AS latestLogin FROM web_access_log  WHERE  USER_ID LIKE 'DV_%'  GROUP BY USER_ID";
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        JSONArray jsonArray = dao.load3Vals(strQuery, "dealer", "dealer", "logincount", "logincount", "latestLogin", "latestLogin");

        JSONObject obj = new JSONObject();
        obj.put("data", jsonArray);
        out.println(obj.toJSONString());
        return;
    }

    public void getLoginData(HttpServletRequest request, HttpServletResponse response) throws Exception {

        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        String idStr = request.getParameter("id");
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getLoginData'");
        String strQuery = queryRecord.getQuery();
        strQuery = StringUtils.replaceString(strQuery, "!idStr!", idStr, true);
        LIDAO dao = new LIDAO();
        LIService service = new LIService();
        JSONArray jsonArray = dao.load3Vals(strQuery, "FIRST_NAME", "FIRST_NAME", "USER_ROLE_ID", "USER_ROLE_ID", "City", "city");

        JSONObject obj = new JSONObject();
        obj.put("data", jsonArray);
        out.println(obj.toJSONString());
        return;
    }

}
