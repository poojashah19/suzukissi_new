/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.suzukithdashboard.webactions;

import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.service.LICsidealerscoretableviewService;
import com.leadics.suzukithdashboard.service.LIOtherstableviewService;
import com.leadics.suzukithdashboard.service.LIReportquirescsiService;
import com.leadics.suzukithdashboard.service.LIReportquiresssiService;
import com.leadics.suzukithdashboard.to.LICsidealerscoretableviewRecord;
import com.leadics.suzukithdashboard.to.LIOtherstableviewRecord;
import com.leadics.suzukithdashboard.to.LIReportquirescsiRecord;
import com.leadics.suzukithdashboard.to.LIReportquiresssiRecord;
import com.leadics.utils.LogUtils;
import com.leadics.utils.PropertyUtil;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author varma.sagi
 */
public class OthersWebactions {

    String langCode = "EN";

    static LogUtils logger = new LogUtils(OthersWebactions.class.getName());

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {
                String lang = request.getParameter("langCode");
                if (lang != null) {
                    langCode = lang;
                }
                if (!StringUtils.isNullOrEmpty(actionType)) {
                    if (actionType.equals("getOthersTable")) {
                        getOthersTable(request, response);
                        return;
                    }
                    if (actionType.equals("getOthersCharts")) {
                        getOthersCharts(request, response, "getOthersCharts");
                        return;
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    public void getOthersTable(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getOthersTable'");
        String strQuery = queryRecord.getQuery();
        String strCountQuery = queryRecord.getCountquery();
        String strCountQuery2 = queryRecord.getCountquery();
        LIDAO dao = new LIDAO();
            LIService service = new LIService();
        String oem = "Nissan";
        String average ="Average";
        String best = "Best";
        String dealername = "Dealer";
        String dealergroupname = "dealergroup";
    
        if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
           average = service.getTranslateString("Average", langCode);
            best = service.getTranslateString(best, langCode);
            dealername = service.getTranslateString(dealername, langCode);
            dealergroupname = service.getTranslateString(dealergroupname, langCode);
        };

        String Heading1 = oem + " " + average;
        String Heading2 = oem + " " + average;
        String Heading3 = oem + " " + average;
        String Heading4 = oem + " " + average;

        String region = request.getParameter("region");
        String dealer = request.getParameter("dealer");
        String dealergroup = request.getParameter("dealergroup");

        if (dealer != null && !dealer.equalsIgnoreCase("ALL") && !dealer.equalsIgnoreCase("Study Total")) {
            System.out.println("**************************************111111");
            String region1 = dao.loadString("Select distinct region from others_table_aggregate where dealer like'" + dealer + "' limit 1");

            String dealernametemp = dealer;
            String regiontemp = region1;
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                regiontemp = service.getTranslateString(region1, langCode);

                dealernametemp = service.getTranslateString(dealer, langCode);
            }
            Heading1 = dealernametemp;
            Heading2 = regiontemp + " " + average;

            strQuery = strQuery.replaceAll("#region#", " AND region=\"" + region1 + "\" ");
            strQuery = strQuery.replaceAll("#region1#", " AND region=\"" + region1 + "\" ");
            strQuery = strQuery.replaceAll("#dealer#", " AND dealer=\"" + dealer + "\" ");
            strQuery = strQuery.replaceAll("#dealergroup#", " ");

            strCountQuery = strCountQuery.replaceAll("#region#", " AND region=\"" + region1 + "\" ");
            strCountQuery = strCountQuery.replaceAll("#dealer#", " AND dealer=\"" + dealer + "\" ");
            strCountQuery = strCountQuery.replaceAll("#dealergroup#", " ");

            strCountQuery2 = strCountQuery2.replaceAll("#region#", " and region=\""+region1+"\" ");
            strCountQuery2 = strCountQuery2.replaceAll("#dealer#", " ");
            strCountQuery2 = strCountQuery2.replaceAll("#dealergroup#", " ");

        } else if (dealergroup != null && !dealergroup.equalsIgnoreCase("ALL") && !dealergroup.equalsIgnoreCase("Study Total")) {
            System.out.println("**************************************2222222222222222");
            String region1 = "";
            if (region != null && !region.equalsIgnoreCase("ALL") && !region.equalsIgnoreCase("Study Total")) {
                System.out.println("**************************************3333333333");
//             region1 = dao.loadString("Select distinct region from others_table_aggregate where dealergroup like'" + dealergroup + "' limit 1");
                region1 = region;
                strQuery = strQuery.replaceAll("#region1#", " AND region=\"" + region + "\" ");
                 strQuery = strQuery.replaceAll("#dealergroup#", " AND dealergroup=\"" + dealergroup + "\" ");
            } else {
                System.out.println("**************************************444444444444444444");
                region1 = dao.loadString("Select distinct region from dg_region where dealergroup like'" + dealergroup + "' limit 1");
                strQuery = strQuery.replaceAll("#region1#", " AND region=\"" + region1 + "\" ");
            }

//            String region1 = dao.loadString("Select distinct region from others_table_aggregate where dealergroup like'" + dealergroup + "' limit 1");
            String dealernametemp = dealergroup;
            String regiontemp = region1;
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                regiontemp = service.getTranslateString(region1, langCode);

                dealernametemp = service.getTranslateString(dealergroup, langCode);
            }

            Heading1 = dealernametemp;
            Heading2 = regiontemp + " " + average;
            System.out.println("**************************************5555555555555");
//            strQuery = strQuery.replaceAll("#region#", " AND region=\"" + region1 + "\" ");
            strQuery = strQuery.replaceAll("#region#", "  ");
            strQuery = strQuery.replaceAll("#dealergroup#", " AND dealergroup=\"" + dealergroup + "\" ");
            strQuery = strQuery.replaceAll("#dealer#", " ");

            strCountQuery = strCountQuery.replaceAll("#region#", " AND region=\"" + region1 + "\" ");
            strCountQuery = strCountQuery.replaceAll("#dealergroup#", " AND dealergroup=\"" + dealergroup + "\" ");
            strCountQuery = strCountQuery.replaceAll("#dealer#", " ");

            strCountQuery2 = strCountQuery2.replaceAll("#region#", " AND region=\"" + region1 + "\"");
            strCountQuery2 = strCountQuery2.replaceAll("#dealergroup#", "  ");
            strCountQuery2 = strCountQuery2.replaceAll("#dealer#", " ");
        } else if (region != null && !region.equalsIgnoreCase("ALL") && !region.equalsIgnoreCase("Study Total")) {
            System.out.println("**************************************66666666666666666666666");
            String regiontemp = region;
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                regiontemp = service.getTranslateString(region, langCode);

            }
            Heading1 = regiontemp + " " + average;
            Heading2 = regiontemp + " " + average;

            strQuery = strQuery.replaceAll("#region#", " AND region=\"" + region + "\" ");
            strQuery = strQuery.replaceAll("#region1#", " AND region=\"" + region + "\" ");
            strQuery = strQuery.replaceAll("#dealer#", " ");
            strQuery = strQuery.replaceAll("#dealergroup#", " ");
            strCountQuery = strCountQuery.replaceAll("#region#", " AND region=\"" + region + "\" ");
            strCountQuery = strCountQuery.replaceAll("#dealer#", " ");
            strCountQuery = strCountQuery.replaceAll("#dealergroup#", " ");

            strCountQuery2 = strCountQuery2.replaceAll("#region#", " AND region=\"" + region + "\" ");
            strCountQuery2 = strCountQuery2.replaceAll("#dealer#", " ");
            strCountQuery2 = strCountQuery2.replaceAll("#dealergroup#", " ");
        } else {
            System.out.println("**************************************777777777777777");
            strQuery = strQuery.replaceAll("#region#", " ");
            strQuery = strQuery.replaceAll("#region1#", " ");

            strQuery = strQuery.replaceAll("#dealer#", " ");
            strQuery = strQuery.replaceAll("#dealergroup#", " ");
            strCountQuery = strCountQuery.replaceAll("#region#", " ");
            strCountQuery = strCountQuery.replaceAll("#dealer#", " ");
            strCountQuery = strCountQuery.replaceAll("#dealergroup#", " ");

            strCountQuery2 = strCountQuery2.replaceAll("#region#", "  ");
            strCountQuery2 = strCountQuery2.replaceAll("#dealer#", " ");
            strCountQuery2 = strCountQuery2.replaceAll("#dealergroup#", " ");
        }

        List<String> filterList = new ArrayList<String>();

        filterList.add("year");

    
//       
        if (!langCode.equalsIgnoreCase("EN")) {
//            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

       String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
     
        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
          strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit,PropertyUtil.getProperty("dealercountlimit"), true);
        JSONArray jsonArray = dao.load8Vals(strQuery,"attributename","attributename","dealeraverage","dealeraverage","regionaverage","regionaverage","studybest","studybest","per","per","samplecount1","samplecount1","samplecount2","samplecount2","savg","savg");
            strCountQuery = StringUtils.replaceString(strCountQuery, service.strWhereCluse, where_condtion, true);
         strCountQuery2 = StringUtils.replaceString(strCountQuery2, service.strWhereCluse, where_condtion, true);
        int Samplecount1 =Integer.valueOf(dao.loadString(strCountQuery));
            int Samplecount2 = Integer.valueOf(dao.loadString(strCountQuery2));
            
           
            
            
        
        
        

        
      
        JSONArray array = new JSONArray();
        int i=0;
        Iterator itr=jsonArray.iterator();
             while(itr.hasNext()){
            JSONObject jsontempObj =(JSONObject) itr.next();
            String attribute =(String)jsontempObj.get("attributename");
            String dealerAVg = (String)jsontempObj.get("dealeraverage");
            String regionAVg = (String)jsontempObj.get("regionaverage");
            String StudyBest =(String)jsontempObj.get("studybest");
            String Per = (String)jsontempObj.get("per");
           

              
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                attribute = service.getTranslateString(attribute, langCode);
   
            }
            jsontempObj.put("AttributeName", attribute);
            if(i==0){
            if(Samplecount1<10){
                Heading1= Heading1.concat("**"); 
            }
            else if(Samplecount1<30)
            {
                Heading1= Heading1.concat("*");
            }   
            }
            jsontempObj.put("Heading1",Heading1);
            if(Per.equalsIgnoreCase("Per")&&!dealerAVg.equalsIgnoreCase("NA")){
                dealerAVg=dealerAVg.concat("%");
            }
            if(Per.equalsIgnoreCase("Per")&&!regionAVg.equalsIgnoreCase("NA"))
            {
                regionAVg=regionAVg.concat("%");  
            }
           
            if(Per.equalsIgnoreCase("Per")&&!StudyBest.equalsIgnoreCase("NA"))
            {  
                StudyBest=StudyBest.concat("%");        
            }
            jsontempObj.put("Value1", dealerAVg);
            if(i==0){
            if(Samplecount2<10){
                Heading2= Heading2.concat("**");
            }
            else if(Samplecount2<30)
            {
                Heading2= Heading2.concat("*");
            } 
            }
            jsontempObj.put("Heading2",Heading2);
            jsontempObj.put("Value2", regionAVg);
    //no need samplecount for best col 3,4
          
            
                
            jsontempObj.put("Heading4",Heading4);
            jsontempObj.put("Value4", StudyBest); 
            array.add(jsontempObj);
            i++;
        }
        String count = dao.loadString(strCountQuery);
        JSONObject obj = new JSONObject();
        obj.put("samplecount", count);
        obj.put("data", array);
        out.println(obj.toJSONString());
//        out.println(array.toJSONString());
        return;
    }

    public void getOthersCharts(HttpServletRequest request, HttpServletResponse response, String ChartName) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='" + ChartName + "'");
        String strQuery = queryRecord.getQuery();
        String strCountQuery = queryRecord.getCountquery();
        String strCountQuery2 = queryRecord.getCountquery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        String oem = "Nissan";
        String average = "Average";
        String best = "Best";
        String dealername = "Dealer";

        String regionname = "Region";
        if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
            average = service.getTranslateString(average, langCode);
            best = service.getTranslateString(best, langCode);
            dealername = service.getTranslateString(dealername, langCode);

        };

        String Heading1 = oem + " " + average;
        String Heading2 = oem + " " + average;
        String Heading3 = oem + " " + average;
        String Heading4 = oem + " " + average;

        String region = request.getParameter("region");
        String dealer = request.getParameter("dealer");
        String dealergroup = request.getParameter("dealergroup");

        if (dealer != null && !dealer.equalsIgnoreCase("ALL") && !dealer.equalsIgnoreCase("Study Total")) {
            String region1 = dao.loadString("Select distinct region from others_charts_aggregate where dealer like'" + dealer + "'");
//            String zone1 = dao.loadString("Select distinct zone from others_charts_aggregate where dealer like'" + dealer + "'");
            String region1temp = region1;
//            String zonetemp=zone1;
            String dealertemp = dealer;
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                region1temp = service.getTranslateString(region1, langCode);
                dealertemp = service.getTranslateString(dealer, langCode);
//                    zonetemp= service.getTranslateString(zone1, langCode);
            }
            Heading1 = dealertemp+ " " + average;;
            Heading2 = region1temp+ " " + average;;
//            Heading3 = zonetemp +" "+average;
            strQuery = strQuery.replaceAll("#region#", " AND region=\"" + region1 + "\" ");
            strQuery = strQuery.replaceAll("#regionx#", " AND region=\"" + region1 + "\" ");
            strQuery = strQuery.replaceAll("#dealer#", " AND dealer=\"" + dealer + "\" ");
            strQuery = strQuery.replaceAll("#dealergroup#", "");
            strQuery = strQuery.replaceAll("#state#", " ");
            
            strCountQuery = strCountQuery.replaceAll("#region#", " AND region=\"" + region1 + "\" ");
            strCountQuery = strCountQuery.replaceAll("#dealergroup#", "  ");
            strCountQuery = strCountQuery.replaceAll("#dealer#", "  AND dealer=\"" + dealer + "\" ");

            strCountQuery2 = strCountQuery2.replaceAll("#region#", " AND region=\"" + region1 + "\"");
            strCountQuery2 = strCountQuery2.replaceAll("#dealergroup#", "  ");
            strCountQuery2 = strCountQuery2.replaceAll("#dealer#", " ");
          

//            strQuery = strQuery.replaceAll("#state#"," AND zone=\"" + zone1 + "\" ");
        } else if (dealergroup != null && !dealergroup.equalsIgnoreCase("ALL") && !dealergroup.equalsIgnoreCase("Study Total")) {

            String region1 = "";
            if (region != null && !region.equalsIgnoreCase("ALL") && !region.equalsIgnoreCase("Study Total")) {
                strQuery = strQuery.replaceAll("#regionx#", " AND region=\"" + region + "\" ");
                strCountQuery2 = strCountQuery2.replaceAll("#region#", " AND region=\"" + region + "\"");
//                region1 = dao.loadString("Select distinct region from csi_others_charts_aggregate where dealergroup like'" + dealergroup + "'");
                region1 = region;
            } else {
                
                region1 = dao.loadString("Select distinct region from dg_region where dealergroup like'" + dealergroup + "'");
                strQuery = strQuery.replaceAll("#regionx#", "AND region=\"" + region1 + "\" ");
                 strCountQuery2 = strCountQuery2.replaceAll("#region#", "AND region=\"" + region1 + "\" ");
            }

//            String region1 = dao.loadString("Select distinct region from others_charts_aggregate where dealergroup like'" + dealergroup + "'");
//            String zone1 = dao.loadString("Select distinct zone from others_charts_aggregate where dealer like'" + dealer + "'");
            String region1temp = region1;
//            System.out.println("::::::::::::::::::::regionn");
//            System.out.println(region1temp);
//            String zonetemp=zone1;
            String dealergrouptemp = dealergroup;
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                region1temp = service.getTranslateString(region1temp, langCode);
                dealergrouptemp = service.getTranslateString(dealergrouptemp, langCode);
//                    zonetemp= service.getTranslateString(zone1, langCode);
            }
            Heading1 = dealergrouptemp+ " " + average;;
            Heading2 = region1temp+ " " + average;;
//            Heading3 = zonetemp +" "+average;
            strQuery = strQuery.replaceAll("#region#", " AND region=\"" + region1 + "\" ");

            strQuery = strQuery.replaceAll("#dealergroup#", " AND dealergroup=\"" + dealergroup + "\" ");
            strQuery = strQuery.replaceAll("#dealer#", " ");
            strQuery = strQuery.replaceAll("#dealergroup#", "");
            strQuery = strQuery.replaceAll("#state#", " ");
            
             strCountQuery = strCountQuery.replaceAll("#region#", " AND region=\"" + region1 + "\" ");
            strCountQuery = strCountQuery.replaceAll("#dealergroup#", " AND dealergroup=\"" + dealergroup + "\" ");
            strCountQuery = strCountQuery.replaceAll("#dealer#", "  ");

           
            strCountQuery2 = strCountQuery2.replaceAll("#dealergroup#", "  ");
            strCountQuery2 = strCountQuery2.replaceAll("#dealer#", " ");
    

//            strQuery = strQuery.replaceAll("#state#"," AND zone=\"" + zone1 + "\" ");
        } else if (region != null && !region.equalsIgnoreCase("ALL") && !region.equalsIgnoreCase("Study Total")) {
//            String zone1 = dao.loadString("Select distinct zone from others_charts_aggregate where region like'" + region + "' limit 1");
            String region1temp = region;
//            String zonetemp=zone1;
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                region1temp = service.getTranslateString(region, langCode);

            }
            Heading1 = region1temp+ " " + average;;
            Heading2 = region1temp+ " " + average;;
//            Heading3 = zonetemp +" "+average;
            strQuery = strQuery.replaceAll("#region#", " AND region=\"" + region + "\" ");
            strQuery = strQuery.replaceAll("#regionx#", " AND region=\"" + region + "\" ");
            strQuery = strQuery.replaceAll("#dealer#", " ");
            strQuery = strQuery.replaceAll("#dealergroup#", "");
            strQuery = strQuery.replaceAll("#state#", " ");

             strCountQuery = strCountQuery.replaceAll("#region#", " AND region=\"" + region + "\" ");
            strCountQuery = strCountQuery.replaceAll("#dealergroup#", "  ");
            strCountQuery = strCountQuery.replaceAll("#dealer#", " ");

            strCountQuery2 = strCountQuery2.replaceAll("#region#", " AND region=\"" + region + "\"");
            strCountQuery2 = strCountQuery2.replaceAll("#dealergroup#", "  ");
            strCountQuery2 = strCountQuery2.replaceAll("#dealer#", " ");

//            strQuery = strQuery.replaceAll("#state#", " AND zone=\"" + zone1 + "\" ");
        } //        else if (zone != null && !zone.equalsIgnoreCase("ALL")&& !zone.equalsIgnoreCase("Study Total")) {
        //            String zonetemp=zone;
        //            if(langCode!=null && !langCode.equalsIgnoreCase("EN")){
        //                    zonetemp = service.getTranslateString(zone, langCode);
        //                }
        //            Heading1 = zonetemp +" "+average;
        //            Heading2 = zonetemp +" "+average;
        //            Heading3 = zonetemp +" "+average;
        //            strQuery = strQuery.replaceAll("#region#", " AND zone=\"" + zone + "\" ");
        //            strQuery = strQuery.replaceAll("#dealer#", " ");
        //            strQuery = strQuery.replaceAll("#state#", " ");
        //        }
        else {
            strQuery = strQuery.replaceAll("#region#", " ");
            strQuery = strQuery.replaceAll("#regionx#", " ");
            strQuery = strQuery.replaceAll("#dealer#", " ");
            strQuery = strQuery.replaceAll("#state#", " ");
            strQuery = strQuery.replaceAll("#dealergroup#", "");
             
               strCountQuery = strCountQuery.replaceAll("#region#", " ");
            strCountQuery = strCountQuery.replaceAll("#dealergroup#", "  ");
            strCountQuery = strCountQuery.replaceAll("#dealer#", " ");

            strCountQuery2 = strCountQuery2.replaceAll("#region#", " ");
            strCountQuery2 = strCountQuery2.replaceAll("#dealergroup#", "  ");
            strCountQuery2 = strCountQuery2.replaceAll("#dealer#", " ");
          
        }

        List<String> filterList = new ArrayList<String>();

        filterList.add("year");

        filterList.add("model");
//        filterList.add("dealergroup");

        if (!langCode.equalsIgnoreCase("EN")) {
//            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        String where_condtion = service.buildWherecondition(filterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
           strCountQuery = StringUtils.replaceString(strCountQuery, service.strWhereCluse, where_condtion, true);
         strCountQuery2 = StringUtils.replaceString(strCountQuery2, service.strWhereCluse, where_condtion, true);
 

        JSONArray jsonArray = dao.load5Vals(strQuery, "AttributeName", "AttributeName", "value", "value", "dealerscore", "dealerscore", "regionscore", "regionscore", "studyscore", "studyscore");
        Iterator i = jsonArray.iterator();
        int samplecount_1 = Integer.valueOf(dao.loadString(strCountQuery));
         int    samplecount_2 =  Integer.valueOf(dao.loadString(strCountQuery2));
      
        JSONArray finalArray = new JSONArray();
        LinkedHashMap attributes = new LinkedHashMap();
        LinkedHashMap values = new LinkedHashMap();
        LinkedHashMap checkcomb = new LinkedHashMap();
        LinkedHashMap dealerscores = new LinkedHashMap();
        LinkedHashMap regionscores = new LinkedHashMap();
//        LinkedHashMap zonescores = new LinkedHashMap();
        LinkedHashMap studyscores = new LinkedHashMap();
        
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            String Attribute = (String) tmpJson.get("AttributeName");
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                Attribute = service.getTranslateString(Attribute, langCode);

            }
            String value = (String) tmpJson.get("value");
            String dealerscore = (String) tmpJson.get("dealerscore");
            String regionscore = (String) tmpJson.get("regionscore");

            String studyscore = (String) tmpJson.get("studyscore");
            
            Attribute = Attribute.trim();
            value = value.trim();

            attributes.put(Attribute, Attribute);
            values.put(Attribute + "_" + value, value);
            checkcomb.put(Attribute + "_" + value, new Boolean(true));
            dealerscores.put(Attribute + "_" + value, dealerscore);
            regionscores.put(Attribute + "_" + value, regionscore);
//            zonescores.put(Attribute + "_" + value, zonescore);
            studyscores.put(Attribute + "_" + value, studyscore);
        }

        Iterator iattributes = attributes.keySet().iterator();
        while (iattributes.hasNext()) {
            JSONObject jsonbject = new JSONObject();
            String attribute = (String) iattributes.next();

            jsonbject.put("headingname", attribute);
            Iterator ivalues = values.keySet().iterator();
            JSONArray tmpJsonArray = new JSONArray();

            String astersik = "";
//            System.out.println("NSK:::::::::::::::" + count);
            if (samplecount_1 < 10) {
                astersik = "**";
            } else if (samplecount_1 >= 10 && samplecount_1 < 30) {
                astersik = "*";
            } else {
                astersik = "";
            }

            String astersik2 = "";
//            System.out.println("NSK:::::::::::::::" + count);
            if (samplecount_2 < 10) {
                astersik2 = "**";
            } else if (samplecount_2 >= 10 && samplecount_2 < 30) {
                astersik2 = "*";
            } else {
                astersik2 = "";
            }
            while (ivalues.hasNext()) {
                JSONObject tmpJsonObj = new JSONObject();
                String attribute_and_value = (String) ivalues.next();

                String strattribute = attribute_and_value.substring(0, attribute_and_value.indexOf("_"));

                String value = attribute_and_value.substring(attribute_and_value.indexOf("_") + 1);

                if (!strattribute.equals(attribute)) {
                    continue;
                }
                if (checkcomb.containsKey(attribute + "_" + value)) {
                    String dealerscore = (String) dealerscores.get(attribute + "_" + value);
                    String regionscore = (String) regionscores.get(attribute + "_" + value);

                    String studyscore = (String) studyscores.get(attribute + "_" + value);
                    String dealerscoreTitle = dealerscore + " %";
                    String regionscoreTitle = regionscore + " %";

                    String studyscoreTitle = studyscore + " %";

                    if (dealerscore == null) {
                        dealerscore = "0";
                        dealerscoreTitle = " ";
                    }

                    if (regionscore == null) {
                        regionscore = "0";
                        regionscoreTitle = " ";
                    }
                    if (studyscore == null) {
                        studyscore = "0";
                        studyscoreTitle = " ";
                    }
                    JSONObject jsonobj = new JSONObject();
                    if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                        value = service.getTranslateString(value, langCode);

                    }
                    jsonobj.put("name", value);
                    JSONArray subarrray = new JSONArray();

                    JSONObject dealerscoreObj = new JSONObject();
                    dealerscoreObj.put("y", Integer.parseInt(dealerscore));
                    dealerscoreObj.put("bartitle", dealerscoreTitle);

                    dealerscoreObj.put("name", Heading1 + astersik);
                    dealerscoreObj.put("tooltip", "<b>" + value + "<br>" + dealerscore + "%</b>");
                    subarrray.add(dealerscoreObj);

                    JSONObject regionscoreObj = new JSONObject();

                    regionscoreObj.put("y", Integer.parseInt(regionscore));
                    regionscoreObj.put("bartitle", regionscoreTitle);

                    regionscoreObj.put("name", Heading2 + astersik2);
                    regionscoreObj.put("tooltip", "<b>" + value + "<br>" + regionscore + "%</b>");
                    subarrray.add(regionscoreObj);

                    JSONObject studyscoreObj = new JSONObject();
                    studyscoreObj.put("y", Integer.parseInt(studyscore));
                    studyscoreObj.put("bartitle", studyscoreTitle);

                    studyscoreObj.put("name", Heading4);
                    studyscoreObj.put("tooltip", "<b>" + value + "<br>" + studyscore + "%</b>");
                    subarrray.add(studyscoreObj);

                    jsonobj.put("data", subarrray);

                    tmpJsonArray.add(jsonobj);
                }

            }
            jsonbject.put("headingdata", tmpJsonArray);
            finalArray.add(jsonbject);
        }
        out.println(finalArray.toJSONString());
        return;
    }

}
