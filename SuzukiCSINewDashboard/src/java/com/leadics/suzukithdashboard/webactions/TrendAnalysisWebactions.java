/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leadics.suzukithdashboard.webactions;

import com.leadics.suzukithdashboard.common.LIDAO;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.service.LIReportquirescsiService;
import com.leadics.suzukithdashboard.service.LIReportquiresssiService;
import com.leadics.suzukithdashboard.to.LIReportquirescsiRecord;
import com.leadics.suzukithdashboard.to.LIReportquiresssiRecord;
import com.leadics.utils.LogUtils;
import com.leadics.utils.PropertyUtil;
import com.leadics.utils.StringUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author varma.sagi
 */
public class TrendAnalysisWebactions {

    String langCode = "EN";

    static LogUtils logger = new LogUtils(TrendAnalysisWebactions.class.getName());

    public void processWebRequest(HttpServletRequest request, HttpServletResponse response, String actionType)
            throws ServletException, IOException {
        logger.debug("Action Type:" + actionType + "");
        response.setContentType("text/html");

        try (PrintWriter out = response.getWriter()) {
            try {
                String lang = request.getParameter("langCode");
                if (lang != null) {
                    langCode = lang;
                }
                if (!StringUtils.isNullOrEmpty(actionType)) {
                    if (actionType.equals("getTrendByWave")) {
                        getTrendByWave(request, response);
                        return;
                    }
                    if (actionType.equals("getFactorTrendByWave")) {
                        getFactorTrendByWave(request, response);
                        return;
                    }
                    if (actionType.equals("getAttributeTrendByWave")) {
                        getAttributeTrendByWave(request, response);
                        return;
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
                out.println("-1");
                return;
            }
        }

    }

    public void getTrendByWave(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String periodSwitch = request.getParameter("period");
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getTrendByWave'");
        if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Year")) {
            queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getTrendByWaveYear'");
        } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Bi-Annual Period")) {
            queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getTrendByWaveBi'");
        } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Month")) {
            queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getTrendByWaveMonth'");
        }
        String strQuery = queryRecord.getQuery();
        String strCountQuery = queryRecord.getCountquery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> bestfilterList = new ArrayList<String>();
        List<String> filterList = new ArrayList<String>();

        
        filterList.add("region");
        filterList.add("dealer");
       filterList.add("dealergroup");
       

        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        String where_condtion = service.buildWherecondition(filterList, request);

        String best_where_condtion = service.buildWherecondition(bestfilterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strCountQuery = StringUtils.replaceString(strCountQuery, service.strWhereCluse, where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strBestWhereCluse, best_where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);
        String strBestString = service.getBestString(request);
        String strWorstString = service.getWorstString(request);

        String samplecount = dao.loadString(strCountQuery, "samplecount");

        JSONArray jsonArray = dao.load5Vals(strQuery, "period", "period", "csi_score", "csi_score", "bestscore", "bestscore", "worstscore", "worstscore","samplecount","samplecount");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();
        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject jsonobj = new JSONObject();
            String Period = (String) tmpJson.get("period");
            String csi_score = (String) tmpJson.get("csi_score");
            String bestscore = (String) tmpJson.get("bestscore");
            String worstscore = (String) tmpJson.get("worstscore");
            
             String samplecnt = (String) tmpJson.get("samplecount");
Integer count = Integer.valueOf(Integer.parseInt(samplecnt));
        String astersik = "";
//            System.out.println("NSK:::::::::::::::" + count);
        if (count < 10) {
            astersik = "**";
        } else if (count >= 10 && count < 30) {
            astersik = "*";
        } else {
            astersik = "";
        }

            if (worstscore == null) {
                worstscore = "0";
            }
            if (bestscore == null) {
                bestscore = "0";
            }
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                Period = service.getTranslateString(Period, langCode);
                strBestString = service.getTranslateString(strBestString, langCode);
                strWorstString = service.getTranslateString(strWorstString, langCode);
                System.out.println("Dealer:" + Period);
            }
            jsonobj.put("name", Period+astersik);
            jsonobj.put("bartitle", csi_score);
            jsonobj.put("y", Integer.parseInt(csi_score));
            jsonobj.put("tooltip", Period + "<br>CSI Score : <b>" + csi_score + "</b>");
            jsonobj.put("scatter", strBestString);
            if (bestscore != null && !bestscore.equalsIgnoreCase("0") && !bestscore.equalsIgnoreCase("null")) {
                jsonobj.put("svalue", Integer.parseInt(bestscore));
            } else {
                jsonobj.put("svalue", null);
            }
            jsonobj.put("scatter1", strWorstString);
            if (worstscore != null && !worstscore.equalsIgnoreCase("0") && !worstscore.equalsIgnoreCase("null")) {
                jsonobj.put("svalue1", Integer.parseInt(worstscore));
            } else {
                jsonobj.put("svalue1", null);
            }
            finalArray.add(jsonobj);
        }
        JSONObject obj = new JSONObject();
        obj.put("samplecount", samplecount);
        obj.put("data", finalArray);
        out.println(obj.toJSONString());
        return;
    }

    public void getFactorTrendByWave(HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        LIReportquirescsiService queryService = new LIReportquirescsiService();
        String periodSwitch = request.getParameter("period");
        LIReportquirescsiRecord queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getFactorTrendByWave'");
        if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Year")) {
            queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getFactorTrendByWaveYear'");
        } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Bi-Annual Period")) {
            queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getFactorTrendByWaveBi'");
        } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Month")) {
            queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getFactorTrendByWaveMonth'");
        }
        String strQuery = queryRecord.getQuery();
        LIDAO dao = new LIDAO();
        LIService service = new LIService();

        List<String> bestfilterList = new ArrayList<String>();
        List<String> filterList = new ArrayList<String>();

        
        filterList.add("region");
        filterList.add("factor");
        filterList.add("year");
        filterList.add("dealer");      
        filterList.add("dealergroup");
        
        bestfilterList.add("Factor");
         bestfilterList.add("year");

        if (!langCode.equalsIgnoreCase("EN")) {
            System.out.println("Lang Code :" + langCode);
            strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
        }

        String where_condtion = service.buildWherecondition(filterList, request);
        String best_where_condtion = service.buildWherecondition(bestfilterList, request);
        strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strBestWhereCluse, best_where_condtion, true);
        strQuery = StringUtils.replaceString(strQuery, service.strSamplelimit, PropertyUtil.getProperty("samplelimit"), true);
        strQuery = StringUtils.replaceString(strQuery, service.strDealerCountLimit, PropertyUtil.getProperty("dealercountlimit"), true);
        String strBestString = service.getBestString(request);
        String strWorstString = service.getWorstString(request);

        JSONArray jsonArray = dao.load5Vals(strQuery, "period", "period", "factor_score", "factor_score", "bestscore", "bestscore", "worstscore", "worstscore","samplecount","samplecount");
        Iterator i = jsonArray.iterator();
        JSONArray finalArray = new JSONArray();

        while (i.hasNext()) {
            JSONObject tmpJson = (JSONObject) i.next();
            JSONObject jsonobj = new JSONObject();
            String Period = (String) tmpJson.get("period");
            String factor_score = (String) tmpJson.get("factor_score");
            String bestscore = (String) tmpJson.get("bestscore");
            String worstscore = (String) tmpJson.get("worstscore");
             String samplecnt = (String) tmpJson.get("samplecount");
             
            
Integer count = Integer.valueOf(Integer.parseInt(samplecnt));
        String astersik = "";
//            System.out.println("NSK:::::::::::::::" + count);
        if (count < 10) {
            astersik = "**";
        } else if (count >= 10 && count < 30) {
            astersik = "*";
        } else {
            astersik = "";
        }

            if (worstscore == null) {
                worstscore = "0";
            }
            if (worstscore == null) {
                worstscore = "0";
            }
            if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                Period = service.getTranslateString(Period, langCode);
                strBestString = service.getTranslateString(strBestString, langCode);
                strWorstString = service.getTranslateString(strWorstString, langCode);
                System.out.println("Dealer:" + Period);
            }
            jsonobj.put("name", Period+astersik);
            jsonobj.put("bartitle", factor_score);
            jsonobj.put("y", Integer.parseInt(factor_score));
            jsonobj.put("tooltip", Period + "<br>Factor Score : <b>" + factor_score + "</b>");
            jsonobj.put("scatter", strBestString);
            if (bestscore != null && !bestscore.equalsIgnoreCase("0") && !bestscore.equalsIgnoreCase("null")) {
                jsonobj.put("svalue", Integer.parseInt(bestscore));
            } else {
                jsonobj.put("svalue", null);
            }
            jsonobj.put("scatter1", strWorstString);
            if (worstscore != null && !worstscore.equalsIgnoreCase("0") && !worstscore.equalsIgnoreCase("null")) {
                jsonobj.put("svalue1", Integer.parseInt(worstscore));
            } else {
                jsonobj.put("svalue1", null);
            }
            finalArray.add(jsonobj);
        }

        out.println(finalArray.toJSONString());
        return;
    }

    public void getAttributeTrendByWave(HttpServletRequest request, HttpServletResponse response) {
        try {
            PrintWriter out = response.getWriter();
            LIReportquirescsiService queryService = new LIReportquirescsiService();
            String periodSwitch = request.getParameter("period");
             String factor=request.getParameter("factor");
             LIReportquirescsiRecord queryRecord=new LIReportquirescsiRecord();
             List<String> filterList = new ArrayList<String>();
//              if(factor.equalsIgnoreCase("CSI")){
//                  System.out.println("iffffffffffffff");
//                queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getAttributeTrendByWaveTop5'");
//              }else{
                queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getAttributeTrendByWave'");   
                 filterList.add("attributeFactor");              
//              }
            if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Year")) {
                queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getAttributeTrendByWaveYear'");
            } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Bi-Annual Period")) {
                queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getAttributeTrendByWaveBi'");
            } else if (periodSwitch != null && periodSwitch.equalsIgnoreCase("Month")) {
                queryRecord = queryService.loadFirstLIReportquirescsiRecord("SELECT * from reportquires_csi WHERE reportname='getAttributeTrendByWaveMonth'");
            }
            String strQuery = queryRecord.getQuery();
            LIDAO dao = new LIDAO();
            LIService service = new LIService();
            
           
            filterList.add("region");
           
            filterList.add("year");
            filterList.add("dealer");
           
             filterList.add("dealergroup");

            if (!langCode.equalsIgnoreCase("EN")) {
                System.out.println("Lang Code :" + langCode);
                strQuery = StringUtils.replaceString(strQuery, "!langCode!", langCode, true);
            }

            String where_condtion = service.buildWherecondition(filterList, request);
            strQuery = StringUtils.replaceString(strQuery, service.strWhereCluse, where_condtion, true);
            JSONArray jsonArray = dao.load5Vals(strQuery, "period", "period", "attribute", "attribute", "score", "score", "bestscore", "bestscore","samplecount","samplecount");
            System.out.println(jsonArray.toJSONString());
            Iterator i = jsonArray.iterator();
            JSONArray finalArray = new JSONArray();
            LinkedHashMap periods = new LinkedHashMap();
            LinkedHashMap attributes = new LinkedHashMap();
            LinkedHashMap scores = new LinkedHashMap();
             LinkedHashMap samplescores = new LinkedHashMap();
            LinkedHashMap bestscores = new LinkedHashMap();

            while (i.hasNext()) {
                JSONObject tmpJson = (JSONObject) i.next();
                String period = (String) tmpJson.get("period");
                String attribute = (String) tmpJson.get("attribute");
                String score = (String) tmpJson.get("score");
                String bestscore = (String) tmpJson.get("bestscore");
                 String samplecnt = (String) tmpJson.get("samplecount");
                System.out.println(tmpJson.toJSONString());
                periods.put(period, period);
                attributes.put(attribute.trim(), attribute.trim());
                scores.put(period + "-" + attribute.trim(), score);
                 samplescores.put(period + "-" + attribute.trim(), samplecnt);
                bestscores.put(period + "-" + attribute.trim(), bestscore);
            }
            Iterator iattribute = attributes.keySet().iterator();

//          
            while (iattribute.hasNext()) {
                JSONObject jsonbject = new JSONObject();
                String attribute = (String) iattribute.next();
                String attributetemp = attribute;
                System.out.println("timeline:" + attribute);
                if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                    attributetemp = service.getTranslateString(attribute, langCode);
                    System.out.println("timeline:" + attribute);
                }
                jsonbject.put("name", attributetemp);
                Iterator itimeline = periods.keySet().iterator();
                JSONArray tmpJsonArray = new JSONArray();
                while (itimeline.hasNext()) {
                    JSONObject tmpJsonObj = new JSONObject();
                    String timeline = (String) itimeline.next();
                    String score = (String) scores.get(timeline + "-" + attribute);
                      String samplescr = (String) samplescores.get(timeline + "-" + attribute);
                       Integer count = Integer.valueOf(Integer.parseInt(samplescr));
        String astersik = "";
//            System.out.println("NSK:::::::::::::::" + count);
        if (count < 10) {
            astersik = "**";
        } else if (count >= 10 && count < 30) {
            astersik = "*";
        } else {
            astersik = "";
        }
                    if (score != null) {
                        tmpJsonObj.put("y", Double.parseDouble(score));
                    } else {
                        tmpJsonObj.put("y", Double.parseDouble("0.0"));
                    }
                    tmpJsonObj.put("bartitle", score);
                    if (langCode != null && !langCode.equalsIgnoreCase("EN")) {
                        timeline = service.getTranslateString(timeline, langCode);
                        System.out.println("attribute:" + timeline);
                    }
                    tmpJsonObj.put("name", timeline.trim()+astersik);
                     tmpJsonObj.put("category", attributetemp);
                    tmpJsonObj.put("tooltip", "<b>" + timeline + "<br>"+attributetemp +": <b>"+ score + "</b>");
                    tmpJsonArray.add(tmpJsonObj);
                }
                System.out.println(tmpJsonArray.toJSONString());
                jsonbject.put("data", tmpJsonArray);
//                 jsonbject.put("name", attributeFactorname);
                finalArray.add(jsonbject);
            }

            out.println(finalArray.toJSONString());
            return;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
