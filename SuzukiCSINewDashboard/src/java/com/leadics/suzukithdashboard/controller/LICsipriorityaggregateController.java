
/*
 * LICsipriorityaggregateController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.controller;
import com.leadics.suzukithdashboard.common.LIController;
import com.leadics.suzukithdashboard.service.LICsipriorityaggregateService;
import com.leadics.suzukithdashboard.to.LICsipriorityaggregateRecord;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LICsipriorityaggregateController extends LIController
{
	static LogUtils logger = new LogUtils(LICsipriorityaggregateController.class.getName());

	public LICsipriorityaggregateRecord loadFormLICsipriorityaggregateRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLICsipriorityaggregateRecord", null);
		LICsipriorityaggregateRecord record = new LICsipriorityaggregateRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setAttribute(getFormFieldValue(req, res, "tfAttribute"));
		record.setAttributename(getFormFieldValue(req, res, "tfAttributename"));
		record.setAttributeindex(getFormFieldValue(req, res, "tfAttributeindex"));
		record.setFactor(getFormFieldValue(req, res, "tfFactor"));
		record.setRegion(getFormFieldValue(req, res, "tfRegion"));
		record.setState(getFormFieldValue(req, res, "tfState"));
		record.setCity(getFormFieldValue(req, res, "tfCity"));
		record.setDealer(getFormFieldValue(req, res, "tfDealer"));
		record.setModel(getFormFieldValue(req, res, "tfModel"));
		record.setBiweekly(getFormFieldValue(req, res, "tfBiweekly"));
		record.setMonth(getFormFieldValue(req, res, "tfMonth"));
		record.setQuarter(getFormFieldValue(req, res, "tfQuarter"));
		record.setDealerprioritysum(getFormFieldValue(req, res, "tfDealerprioritysum"));
		record.setDealerprioritycount(getFormFieldValue(req, res, "tfDealerprioritycount"));
		record.setTotalcount(getFormFieldValue(req, res, "tfTotalcount"));
		record.setRstatus(getFormFieldValue(req, res, "tfRstatus"));
		record.setLastacc(getFormFieldValue(req, res, "tfLastacc"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setClosetype(getFormFieldValue(req, res, "tfClosetype"));
		record.setClosedat(getFormFieldValue(req, res, "tfClosedat"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLICsipriorityaggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LICsipriorityaggregateRecord loadJSONFormLICsipriorityaggregateRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLICsipriorityaggregateRecord", null);
		LICsipriorityaggregateRecord record = new LICsipriorityaggregateRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setAttribute(getFormFieldValue(req, res, "attribute"));
		record.setAttributename(getFormFieldValue(req, res, "attributename"));
		record.setAttributeindex(getFormFieldValue(req, res, "attributeindex"));
		record.setFactor(getFormFieldValue(req, res, "factor"));
		record.setRegion(getFormFieldValue(req, res, "region"));
		record.setState(getFormFieldValue(req, res, "state"));
		record.setCity(getFormFieldValue(req, res, "city"));
		record.setDealer(getFormFieldValue(req, res, "dealer"));
		record.setModel(getFormFieldValue(req, res, "model"));
		record.setBiweekly(getFormFieldValue(req, res, "bi_weekly"));
		record.setMonth(getFormFieldValue(req, res, "month"));
		record.setQuarter(getFormFieldValue(req, res, "quarter"));
		record.setDealerprioritysum(getFormFieldValue(req, res, "dealer_priority_sum"));
		record.setDealerprioritycount(getFormFieldValue(req, res, "dealer_priority_count"));
		record.setTotalcount(getFormFieldValue(req, res, "totalcount"));
		record.setRstatus(getFormFieldValue(req, res, "rstatus"));
		record.setLastacc(getFormFieldValue(req, res, "lastacc"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setClosetype(getFormFieldValue(req, res, "close_type"));
		record.setClosedat(getFormFieldValue(req, res, "closed_at"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLICsipriorityaggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LICsipriorityaggregateRecord loadJSONFormLICsipriorityaggregateRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLICsipriorityaggregateRecord", null);
		LICsipriorityaggregateRecord record = new LICsipriorityaggregateRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setAttribute(getFormFieldValueEncode(req, res, "attribute"));
		record.setAttributename(getFormFieldValueEncode(req, res, "attributename"));
		record.setAttributeindex(getFormFieldValueEncode(req, res, "attributeindex"));
		record.setFactor(getFormFieldValueEncode(req, res, "factor"));
		record.setRegion(getFormFieldValueEncode(req, res, "region"));
		record.setState(getFormFieldValueEncode(req, res, "state"));
		record.setCity(getFormFieldValueEncode(req, res, "city"));
		record.setDealer(getFormFieldValueEncode(req, res, "dealer"));
		record.setModel(getFormFieldValueEncode(req, res, "model"));
		record.setBiweekly(getFormFieldValueEncode(req, res, "bi_weekly"));
		record.setMonth(getFormFieldValueEncode(req, res, "month"));
		record.setQuarter(getFormFieldValueEncode(req, res, "quarter"));
		record.setDealerprioritysum(getFormFieldValueEncode(req, res, "dealer_priority_sum"));
		record.setDealerprioritycount(getFormFieldValueEncode(req, res, "dealer_priority_count"));
		record.setTotalcount(getFormFieldValueEncode(req, res, "totalcount"));
		record.setRstatus(getFormFieldValueEncode(req, res, "rstatus"));
		record.setLastacc(getFormFieldValueEncode(req, res, "lastacc"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setClosetype(getFormFieldValueEncode(req, res, "close_type"));
		record.setClosedat(getFormFieldValueEncode(req, res, "closed_at"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLICsipriorityaggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LICsipriorityaggregateRecord loadMapLICsipriorityaggregateRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLICsipriorityaggregateRecord", null);
		LICsipriorityaggregateRecord record = new LICsipriorityaggregateRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setAttribute(getMapValue(inputMap, "attribute"));
		record.setAttributename(getMapValue(inputMap, "attributename"));
		record.setAttributeindex(getMapValue(inputMap, "attributeindex"));
		record.setFactor(getMapValue(inputMap, "factor"));
		record.setRegion(getMapValue(inputMap, "region"));
		record.setState(getMapValue(inputMap, "state"));
		record.setCity(getMapValue(inputMap, "city"));
		record.setDealer(getMapValue(inputMap, "dealer"));
		record.setModel(getMapValue(inputMap, "model"));
		record.setBiweekly(getMapValue(inputMap, "bi_weekly"));
		record.setMonth(getMapValue(inputMap, "month"));
		record.setQuarter(getMapValue(inputMap, "quarter"));
		record.setDealerprioritysum(getMapValue(inputMap, "dealer_priority_sum"));
		record.setDealerprioritycount(getMapValue(inputMap, "dealer_priority_count"));
		record.setTotalcount(getMapValue(inputMap, "totalcount"));
		record.setRstatus(getMapValue(inputMap, "rstatus"));
		record.setLastacc(getMapValue(inputMap, "lastacc"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		record.setClosetype(getMapValue(inputMap, "close_type"));
		record.setClosedat(getMapValue(inputMap, "closed_at"));
		logger.trace("loadMapLICsipriorityaggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLICsipriorityaggregateRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLICsipriorityaggregateRecord", null);
		LICsipriorityaggregateService service = new LICsipriorityaggregateService();

		try
		{
			LICsipriorityaggregateRecord record = loadFormLICsipriorityaggregateRecord(req, res);
			record.setCreatedby(getLoggedInUserId(req,res));
			int resultId = service.insertLICsipriorityaggregateRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Record Creation Successful -" + resultId);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csipriorityaggregate.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create Csipriorityaggregate");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create Csipriorityaggregate " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csipriorityaggregate.jsp");
		}
	}

	public void processUpdateLICsipriorityaggregateRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLICsipriorityaggregateRecord", null);
		LICsipriorityaggregateService service = new LICsipriorityaggregateService();

		try
		{
			LICsipriorityaggregateRecord record = loadFormLICsipriorityaggregateRecord(req, res);
			record.setModifiedby(getLoggedInUserId(req,res));
			service.updateLICsipriorityaggregateRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "Csipriorityaggregate data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csipriorityaggregate.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update Csipriorityaggregate");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update Csipriorityaggregate " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csipriorityaggregate.jsp");
		}
	}

	public void processDeleteLICsipriorityaggregateRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLICsipriorityaggregateRecord", null);
		LICsipriorityaggregateService service = new LICsipriorityaggregateService();

		try
		{
			LICsipriorityaggregateRecord record = loadFormLICsipriorityaggregateRecord(req, res);
			service.deleteLICsipriorityaggregateRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Csipriorityaggregate deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csipriorityaggregate.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete Csipriorityaggregate");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete Csipriorityaggregate " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csipriorityaggregate.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertCsipriorityaggregateRecord"))
		{
			processInsertLICsipriorityaggregateRecord(req, res);
			return;
		}

		if (actionType.equals("UpdateCsipriorityaggregateRecord"))
		{
			processUpdateLICsipriorityaggregateRecord(req, res);
			return;
		}

		if (actionType.equals("DeleteCsipriorityaggregateRecord"))
		{
			processDeleteLICsipriorityaggregateRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}
