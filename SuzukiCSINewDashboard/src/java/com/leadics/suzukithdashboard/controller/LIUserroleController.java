
/*
 * LIUserroleController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.controller;
import com.leadics.suzukithdashboard.common.LIController;
import com.leadics.suzukithdashboard.service.LIUserroleService;
import com.leadics.suzukithdashboard.to.LIUserroleRecord;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LIUserroleController extends LIController
{
	static LogUtils logger = new LogUtils(LIUserroleController.class.getName());

	public LIUserroleRecord loadFormLIUserroleRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLIUserroleRecord", null);
		LIUserroleRecord record = new LIUserroleRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setRolename(getFormFieldValue(req, res, "tfRolename"));
		record.setAdminperm(getFormFieldValue(req, res, "tfAdminperm"));
		record.setMakerperm(getFormFieldValue(req, res, "tfMakerperm"));
		record.setCheckerperm(getFormFieldValue(req, res, "tfCheckerperm"));
		record.setGlobaladminperm(getFormFieldValue(req, res, "tfGlobaladminperm"));
		record.setReportperm(getFormFieldValue(req, res, "tfReportperm"));
		record.setAuditperm(getFormFieldValue(req, res, "tfAuditperm"));
		record.setMadeby(getFormFieldValue(req, res, "tfMadeby"));
		record.setMadeat(getFormFieldValue(req, res, "tfMadeat"));
		record.setCheckedby(getFormFieldValue(req, res, "tfCheckedby"));
		record.setCheckedat(getFormFieldValue(req, res, "tfCheckedat"));
		record.setMakerlastcmt(getFormFieldValue(req, res, "tfMakerlastcmt"));
		record.setCheckerlastcmt(getFormFieldValue(req, res, "tfCheckerlastcmt"));
		record.setCurrappstatus(getFormFieldValue(req, res, "tfCurrappstatus"));
		record.setAdminlastcmt(getFormFieldValue(req, res, "tfAdminlastcmt"));
		record.setRstatus(getFormFieldValue(req, res, "tfRstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setInstitutionid(getFormFieldValue(req, res, "tfInstitutionid"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLIUserroleRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIUserroleRecord loadJSONFormLIUserroleRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIUserroleRecord", null);
		LIUserroleRecord record = new LIUserroleRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setRolename(getFormFieldValue(req, res, "rolename"));
		record.setAdminperm(getFormFieldValue(req, res, "admin_perm"));
		record.setMakerperm(getFormFieldValue(req, res, "maker_perm"));
		record.setCheckerperm(getFormFieldValue(req, res, "checker_perm"));
		record.setGlobaladminperm(getFormFieldValue(req, res, "global_admin_perm"));
		record.setReportperm(getFormFieldValue(req, res, "report_perm"));
		record.setAuditperm(getFormFieldValue(req, res, "audit_perm"));
		record.setMadeby(getFormFieldValue(req, res, "made_by"));
		record.setMadeat(getFormFieldValue(req, res, "made_at"));
		record.setCheckedby(getFormFieldValue(req, res, "checked_by"));
		record.setCheckedat(getFormFieldValue(req, res, "checked_at"));
		record.setMakerlastcmt(getFormFieldValue(req, res, "maker_last_cmt"));
		record.setCheckerlastcmt(getFormFieldValue(req, res, "checker_last_cmt"));
		record.setCurrappstatus(getFormFieldValue(req, res, "curr_app_status"));
		record.setAdminlastcmt(getFormFieldValue(req, res, "admin_last_cmt"));
		record.setRstatus(getFormFieldValue(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setInstitutionid(getFormFieldValue(req, res, "institution_id"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIUserroleRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIUserroleRecord loadJSONFormLIUserroleRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIUserroleRecord", null);
		LIUserroleRecord record = new LIUserroleRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setRolename(getFormFieldValueEncode(req, res, "rolename"));
		record.setAdminperm(getFormFieldValueEncode(req, res, "admin_perm"));
		record.setMakerperm(getFormFieldValueEncode(req, res, "maker_perm"));
		record.setCheckerperm(getFormFieldValueEncode(req, res, "checker_perm"));
		record.setGlobaladminperm(getFormFieldValueEncode(req, res, "global_admin_perm"));
		record.setReportperm(getFormFieldValueEncode(req, res, "report_perm"));
		record.setAuditperm(getFormFieldValueEncode(req, res, "audit_perm"));
		record.setMadeby(getFormFieldValueEncode(req, res, "made_by"));
		record.setMadeat(getFormFieldValueEncode(req, res, "made_at"));
		record.setCheckedby(getFormFieldValueEncode(req, res, "checked_by"));
		record.setCheckedat(getFormFieldValueEncode(req, res, "checked_at"));
		record.setMakerlastcmt(getFormFieldValueEncode(req, res, "maker_last_cmt"));
		record.setCheckerlastcmt(getFormFieldValueEncode(req, res, "checker_last_cmt"));
		record.setCurrappstatus(getFormFieldValueEncode(req, res, "curr_app_status"));
		record.setAdminlastcmt(getFormFieldValueEncode(req, res, "admin_last_cmt"));
		record.setRstatus(getFormFieldValueEncode(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setInstitutionid(getFormFieldValueEncode(req, res, "institution_id"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIUserroleRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIUserroleRecord loadMapLIUserroleRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLIUserroleRecord", null);
		LIUserroleRecord record = new LIUserroleRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setRolename(getMapValue(inputMap, "rolename"));
		record.setAdminperm(getMapValue(inputMap, "admin_perm"));
		record.setMakerperm(getMapValue(inputMap, "maker_perm"));
		record.setCheckerperm(getMapValue(inputMap, "checker_perm"));
		record.setGlobaladminperm(getMapValue(inputMap, "global_admin_perm"));
		record.setReportperm(getMapValue(inputMap, "report_perm"));
		record.setAuditperm(getMapValue(inputMap, "audit_perm"));
		record.setMadeby(getMapValue(inputMap, "made_by"));
		record.setMadeat(getMapValue(inputMap, "made_at"));
		record.setCheckedby(getMapValue(inputMap, "checked_by"));
		record.setCheckedat(getMapValue(inputMap, "checked_at"));
		record.setMakerlastcmt(getMapValue(inputMap, "maker_last_cmt"));
		record.setCheckerlastcmt(getMapValue(inputMap, "checker_last_cmt"));
		record.setCurrappstatus(getMapValue(inputMap, "curr_app_status"));
		record.setAdminlastcmt(getMapValue(inputMap, "admin_last_cmt"));
		record.setRstatus(getMapValue(inputMap, "rstatus"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		record.setInstitutionid(getMapValue(inputMap, "institution_id"));
		logger.trace("loadMapLIUserroleRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLIUserroleRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIUserroleRecord", null);
		LIUserroleService service = new LIUserroleService();

		try
		{
			LIUserroleRecord record = loadFormLIUserroleRecord(req, res);
			record.setCreatedby(getLoggedInUserId(req,res));
			int resultId = service.insertLIUserroleRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Record Creation Successful -" + resultId);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("userrole.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create Userrole");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create Userrole " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("userrole.jsp");
		}
	}

	public void processUpdateLIUserroleRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIUserroleRecord", null);
		LIUserroleService service = new LIUserroleService();

		try
		{
			LIUserroleRecord record = loadFormLIUserroleRecord(req, res);
			record.setModifiedby(getLoggedInUserId(req,res));
			service.updateLIUserroleRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "Userrole data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("userrole.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update Userrole");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update Userrole " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("userrole.jsp");
		}
	}

	public void processDeleteLIUserroleRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIUserroleRecord", null);
		LIUserroleService service = new LIUserroleService();

		try
		{
			LIUserroleRecord record = loadFormLIUserroleRecord(req, res);
			service.deleteLIUserroleRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Userrole deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("userrole.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete Userrole");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete Userrole " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("userrole.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertUserroleRecord"))
		{
			processInsertLIUserroleRecord(req, res);
			return;
		}

		if (actionType.equals("UpdateUserroleRecord"))
		{
			processUpdateLIUserroleRecord(req, res);
			return;
		}

		if (actionType.equals("DeleteUserroleRecord"))
		{
			processDeleteLIUserroleRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}
