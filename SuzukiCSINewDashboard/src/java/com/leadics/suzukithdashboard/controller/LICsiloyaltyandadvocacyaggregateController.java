
/*
 * LICsiloyaltyandadvocacyaggregateController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.controller;
import com.leadics.suzukithdashboard.common.LIController;
import com.leadics.suzukithdashboard.service.LICsiloyaltyandadvocacyaggregateService;
import com.leadics.suzukithdashboard.to.LICsiloyaltyandadvocacyaggregateRecord;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LICsiloyaltyandadvocacyaggregateController extends LIController
{
	static LogUtils logger = new LogUtils(LICsiloyaltyandadvocacyaggregateController.class.getName());

	public LICsiloyaltyandadvocacyaggregateRecord loadFormLICsiloyaltyandadvocacyaggregateRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLICsiloyaltyandadvocacyaggregateRecord", null);
		LICsiloyaltyandadvocacyaggregateRecord record = new LICsiloyaltyandadvocacyaggregateRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setAttribute(getFormFieldValue(req, res, "tfAttribute"));
		record.setAttributename(getFormFieldValue(req, res, "tfAttributename"));
		record.setAttributeindex(getFormFieldValue(req, res, "tfAttributeindex"));
		record.setRegion(getFormFieldValue(req, res, "tfRegion"));
		record.setState(getFormFieldValue(req, res, "tfState"));
		record.setCity(getFormFieldValue(req, res, "tfCity"));
		record.setDealer(getFormFieldValue(req, res, "tfDealer"));
		record.setModel(getFormFieldValue(req, res, "tfModel"));
		record.setBiweekly(getFormFieldValue(req, res, "tfBiweekly"));
		record.setMonth(getFormFieldValue(req, res, "tfMonth"));
		record.setQuarter(getFormFieldValue(req, res, "tfQuarter"));
		record.setNindex(getFormFieldValue(req, res, "tfNindex"));
		record.setDefinitelywouldnotcount(getFormFieldValue(req, res, "tfDefinitelywouldnotcount"));
		record.setDefinitelywouldcount(getFormFieldValue(req, res, "tfDefinitelywouldcount"));
		record.setProbablywouldcount(getFormFieldValue(req, res, "tfProbablywouldcount"));
		record.setProbablywouldnotcount(getFormFieldValue(req, res, "tfProbablywouldnotcount"));
		record.setTotalcount(getFormFieldValue(req, res, "tfTotalcount"));
		record.setRstatus(getFormFieldValue(req, res, "tfRstatus"));
		record.setLastacc(getFormFieldValue(req, res, "tfLastacc"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setClosetype(getFormFieldValue(req, res, "tfClosetype"));
		record.setClosedat(getFormFieldValue(req, res, "tfClosedat"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLICsiloyaltyandadvocacyaggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LICsiloyaltyandadvocacyaggregateRecord loadJSONFormLICsiloyaltyandadvocacyaggregateRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLICsiloyaltyandadvocacyaggregateRecord", null);
		LICsiloyaltyandadvocacyaggregateRecord record = new LICsiloyaltyandadvocacyaggregateRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setAttribute(getFormFieldValue(req, res, "attribute"));
		record.setAttributename(getFormFieldValue(req, res, "attributename"));
		record.setAttributeindex(getFormFieldValue(req, res, "attributeindex"));
		record.setRegion(getFormFieldValue(req, res, "region"));
		record.setState(getFormFieldValue(req, res, "state"));
		record.setCity(getFormFieldValue(req, res, "city"));
		record.setDealer(getFormFieldValue(req, res, "dealer"));
		record.setModel(getFormFieldValue(req, res, "model"));
		record.setBiweekly(getFormFieldValue(req, res, "bi_weekly"));
		record.setMonth(getFormFieldValue(req, res, "month"));
		record.setQuarter(getFormFieldValue(req, res, "quarter"));
		record.setNindex(getFormFieldValue(req, res, "nindex"));
		record.setDefinitelywouldnotcount(getFormFieldValue(req, res, "definitelywouldnotcount"));
		record.setDefinitelywouldcount(getFormFieldValue(req, res, "definitelywouldcount"));
		record.setProbablywouldcount(getFormFieldValue(req, res, "probablywouldcount"));
		record.setProbablywouldnotcount(getFormFieldValue(req, res, "probablywouldnotcount"));
		record.setTotalcount(getFormFieldValue(req, res, "totalcount"));
		record.setRstatus(getFormFieldValue(req, res, "rstatus"));
		record.setLastacc(getFormFieldValue(req, res, "lastacc"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setClosetype(getFormFieldValue(req, res, "close_type"));
		record.setClosedat(getFormFieldValue(req, res, "closed_at"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLICsiloyaltyandadvocacyaggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LICsiloyaltyandadvocacyaggregateRecord loadJSONFormLICsiloyaltyandadvocacyaggregateRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLICsiloyaltyandadvocacyaggregateRecord", null);
		LICsiloyaltyandadvocacyaggregateRecord record = new LICsiloyaltyandadvocacyaggregateRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setAttribute(getFormFieldValueEncode(req, res, "attribute"));
		record.setAttributename(getFormFieldValueEncode(req, res, "attributename"));
		record.setAttributeindex(getFormFieldValueEncode(req, res, "attributeindex"));
		record.setRegion(getFormFieldValueEncode(req, res, "region"));
		record.setState(getFormFieldValueEncode(req, res, "state"));
		record.setCity(getFormFieldValueEncode(req, res, "city"));
		record.setDealer(getFormFieldValueEncode(req, res, "dealer"));
		record.setModel(getFormFieldValueEncode(req, res, "model"));
		record.setBiweekly(getFormFieldValueEncode(req, res, "bi_weekly"));
		record.setMonth(getFormFieldValueEncode(req, res, "month"));
		record.setQuarter(getFormFieldValueEncode(req, res, "quarter"));
		record.setNindex(getFormFieldValueEncode(req, res, "nindex"));
		record.setDefinitelywouldnotcount(getFormFieldValueEncode(req, res, "definitelywouldnotcount"));
		record.setDefinitelywouldcount(getFormFieldValueEncode(req, res, "definitelywouldcount"));
		record.setProbablywouldcount(getFormFieldValueEncode(req, res, "probablywouldcount"));
		record.setProbablywouldnotcount(getFormFieldValueEncode(req, res, "probablywouldnotcount"));
		record.setTotalcount(getFormFieldValueEncode(req, res, "totalcount"));
		record.setRstatus(getFormFieldValueEncode(req, res, "rstatus"));
		record.setLastacc(getFormFieldValueEncode(req, res, "lastacc"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setClosetype(getFormFieldValueEncode(req, res, "close_type"));
		record.setClosedat(getFormFieldValueEncode(req, res, "closed_at"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLICsiloyaltyandadvocacyaggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LICsiloyaltyandadvocacyaggregateRecord loadMapLICsiloyaltyandadvocacyaggregateRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLICsiloyaltyandadvocacyaggregateRecord", null);
		LICsiloyaltyandadvocacyaggregateRecord record = new LICsiloyaltyandadvocacyaggregateRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setAttribute(getMapValue(inputMap, "attribute"));
		record.setAttributename(getMapValue(inputMap, "attributename"));
		record.setAttributeindex(getMapValue(inputMap, "attributeindex"));
		record.setRegion(getMapValue(inputMap, "region"));
		record.setState(getMapValue(inputMap, "state"));
		record.setCity(getMapValue(inputMap, "city"));
		record.setDealer(getMapValue(inputMap, "dealer"));
		record.setModel(getMapValue(inputMap, "model"));
		record.setBiweekly(getMapValue(inputMap, "bi_weekly"));
		record.setMonth(getMapValue(inputMap, "month"));
		record.setQuarter(getMapValue(inputMap, "quarter"));
		record.setNindex(getMapValue(inputMap, "nindex"));
		record.setDefinitelywouldnotcount(getMapValue(inputMap, "definitelywouldnotcount"));
		record.setDefinitelywouldcount(getMapValue(inputMap, "definitelywouldcount"));
		record.setProbablywouldcount(getMapValue(inputMap, "probablywouldcount"));
		record.setProbablywouldnotcount(getMapValue(inputMap, "probablywouldnotcount"));
		record.setTotalcount(getMapValue(inputMap, "totalcount"));
		record.setRstatus(getMapValue(inputMap, "rstatus"));
		record.setLastacc(getMapValue(inputMap, "lastacc"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		record.setClosetype(getMapValue(inputMap, "close_type"));
		record.setClosedat(getMapValue(inputMap, "closed_at"));
		logger.trace("loadMapLICsiloyaltyandadvocacyaggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLICsiloyaltyandadvocacyaggregateRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLICsiloyaltyandadvocacyaggregateRecord", null);
		LICsiloyaltyandadvocacyaggregateService service = new LICsiloyaltyandadvocacyaggregateService();

		try
		{
			LICsiloyaltyandadvocacyaggregateRecord record = loadFormLICsiloyaltyandadvocacyaggregateRecord(req, res);
			record.setCreatedby(getLoggedInUserId(req,res));
			int resultId = service.insertLICsiloyaltyandadvocacyaggregateRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Record Creation Successful -" + resultId);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csiloyaltyandadvocacyaggregate.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create Csiloyaltyandadvocacyaggregate");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create Csiloyaltyandadvocacyaggregate " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csiloyaltyandadvocacyaggregate.jsp");
		}
	}

	public void processUpdateLICsiloyaltyandadvocacyaggregateRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLICsiloyaltyandadvocacyaggregateRecord", null);
		LICsiloyaltyandadvocacyaggregateService service = new LICsiloyaltyandadvocacyaggregateService();

		try
		{
			LICsiloyaltyandadvocacyaggregateRecord record = loadFormLICsiloyaltyandadvocacyaggregateRecord(req, res);
			record.setModifiedby(getLoggedInUserId(req,res));
			service.updateLICsiloyaltyandadvocacyaggregateRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "Csiloyaltyandadvocacyaggregate data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csiloyaltyandadvocacyaggregate.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update Csiloyaltyandadvocacyaggregate");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update Csiloyaltyandadvocacyaggregate " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csiloyaltyandadvocacyaggregate.jsp");
		}
	}

	public void processDeleteLICsiloyaltyandadvocacyaggregateRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLICsiloyaltyandadvocacyaggregateRecord", null);
		LICsiloyaltyandadvocacyaggregateService service = new LICsiloyaltyandadvocacyaggregateService();

		try
		{
			LICsiloyaltyandadvocacyaggregateRecord record = loadFormLICsiloyaltyandadvocacyaggregateRecord(req, res);
			service.deleteLICsiloyaltyandadvocacyaggregateRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Csiloyaltyandadvocacyaggregate deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csiloyaltyandadvocacyaggregate.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete Csiloyaltyandadvocacyaggregate");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete Csiloyaltyandadvocacyaggregate " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csiloyaltyandadvocacyaggregate.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertCsiloyaltyandadvocacyaggregateRecord"))
		{
			processInsertLICsiloyaltyandadvocacyaggregateRecord(req, res);
			return;
		}

		if (actionType.equals("UpdateCsiloyaltyandadvocacyaggregateRecord"))
		{
			processUpdateLICsiloyaltyandadvocacyaggregateRecord(req, res);
			return;
		}

		if (actionType.equals("DeleteCsiloyaltyandadvocacyaggregateRecord"))
		{
			processDeleteLICsiloyaltyandadvocacyaggregateRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}
