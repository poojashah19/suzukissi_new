
/*
 * LIIntfpermittedipController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.controller;
import com.leadics.suzukithdashboard.common.LIController;
import com.leadics.suzukithdashboard.service.LIIntfpermittedipService;
import com.leadics.suzukithdashboard.to.LIIntfpermittedipRecord;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LIIntfpermittedipController extends LIController
{
	static LogUtils logger = new LogUtils(LIIntfpermittedipController.class.getName());

	public LIIntfpermittedipRecord loadFormLIIntfpermittedipRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLIIntfpermittedipRecord", null);
		LIIntfpermittedipRecord record = new LIIntfpermittedipRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setIntfcode(getFormFieldValue(req, res, "tfIntfcode"));
		record.setPermittedip(getFormFieldValue(req, res, "tfPermittedip"));
		record.setRstatus(getFormFieldValue(req, res, "tfRstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLIIntfpermittedipRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIIntfpermittedipRecord loadJSONFormLIIntfpermittedipRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIIntfpermittedipRecord", null);
		LIIntfpermittedipRecord record = new LIIntfpermittedipRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setIntfcode(getFormFieldValue(req, res, "intf_code"));
		record.setPermittedip(getFormFieldValue(req, res, "permitted_ip"));
		record.setRstatus(getFormFieldValue(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIIntfpermittedipRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIIntfpermittedipRecord loadJSONFormLIIntfpermittedipRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIIntfpermittedipRecord", null);
		LIIntfpermittedipRecord record = new LIIntfpermittedipRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setIntfcode(getFormFieldValueEncode(req, res, "intf_code"));
		record.setPermittedip(getFormFieldValueEncode(req, res, "permitted_ip"));
		record.setRstatus(getFormFieldValueEncode(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIIntfpermittedipRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIIntfpermittedipRecord loadMapLIIntfpermittedipRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLIIntfpermittedipRecord", null);
		LIIntfpermittedipRecord record = new LIIntfpermittedipRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setIntfcode(getMapValue(inputMap, "intf_code"));
		record.setPermittedip(getMapValue(inputMap, "permitted_ip"));
		record.setRstatus(getMapValue(inputMap, "rstatus"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		logger.trace("loadMapLIIntfpermittedipRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLIIntfpermittedipRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIIntfpermittedipRecord", null);
		LIIntfpermittedipService service = new LIIntfpermittedipService();

		try
		{
			LIIntfpermittedipRecord record = loadFormLIIntfpermittedipRecord(req, res);
			record.setCreatedby(getLoggedInUserId(req,res));
			int resultId = service.insertLIIntfpermittedipRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Record Creation Successful -" + resultId);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("intfpermittedip.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create Intfpermittedip");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create Intfpermittedip " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("intfpermittedip.jsp");
		}
	}

	public void processUpdateLIIntfpermittedipRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIIntfpermittedipRecord", null);
		LIIntfpermittedipService service = new LIIntfpermittedipService();

		try
		{
			LIIntfpermittedipRecord record = loadFormLIIntfpermittedipRecord(req, res);
			record.setModifiedby(getLoggedInUserId(req,res));
			service.updateLIIntfpermittedipRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "Intfpermittedip data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("intfpermittedip.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update Intfpermittedip");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update Intfpermittedip " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("intfpermittedip.jsp");
		}
	}

	public void processDeleteLIIntfpermittedipRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIIntfpermittedipRecord", null);
		LIIntfpermittedipService service = new LIIntfpermittedipService();

		try
		{
			LIIntfpermittedipRecord record = loadFormLIIntfpermittedipRecord(req, res);
			service.deleteLIIntfpermittedipRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Intfpermittedip deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("intfpermittedip.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete Intfpermittedip");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete Intfpermittedip " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("intfpermittedip.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertIntfpermittedipRecord"))
		{
			processInsertLIIntfpermittedipRecord(req, res);
			return;
		}

		if (actionType.equals("UpdateIntfpermittedipRecord"))
		{
			processUpdateLIIntfpermittedipRecord(req, res);
			return;
		}

		if (actionType.equals("DeleteIntfpermittedipRecord"))
		{
			processDeleteLIIntfpermittedipRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}
