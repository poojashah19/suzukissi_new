
/*
 * LIInstitutionController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.controller;
import com.leadics.suzukithdashboard.common.LIController;
import com.leadics.suzukithdashboard.service.LIInstitutionService;
import com.leadics.suzukithdashboard.to.LIInstitutionRecord;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LIInstitutionController extends LIController
{
	static LogUtils logger = new LogUtils(LIInstitutionController.class.getName());

	public LIInstitutionRecord loadFormLIInstitutionRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLIInstitutionRecord", null);
		LIInstitutionRecord record = new LIInstitutionRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setIname(getFormFieldValue(req, res, "tfIname"));
		record.setIlocation(getFormFieldValue(req, res, "tfIlocation"));
		record.setMadeby(getFormFieldValue(req, res, "tfMadeby"));
		record.setMadeat(getFormFieldValue(req, res, "tfMadeat"));
		record.setCheckedby(getFormFieldValue(req, res, "tfCheckedby"));
		record.setCheckedat(getFormFieldValue(req, res, "tfCheckedat"));
		record.setMakerlastcmt(getFormFieldValue(req, res, "tfMakerlastcmt"));
		record.setCheckerlastcmt(getFormFieldValue(req, res, "tfCheckerlastcmt"));
		record.setCurrappstatus(getFormFieldValue(req, res, "tfCurrappstatus"));
		record.setAdminlastcmt(getFormFieldValue(req, res, "tfAdminlastcmt"));
		record.setRstatus(getFormFieldValue(req, res, "tfRstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setInstitutionid(getFormFieldValue(req, res, "tfInstitutionid"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLIInstitutionRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIInstitutionRecord loadJSONFormLIInstitutionRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIInstitutionRecord", null);
		LIInstitutionRecord record = new LIInstitutionRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setIname(getFormFieldValue(req, res, "iname"));
		record.setIlocation(getFormFieldValue(req, res, "ilocation"));
		record.setMadeby(getFormFieldValue(req, res, "made_by"));
		record.setMadeat(getFormFieldValue(req, res, "made_at"));
		record.setCheckedby(getFormFieldValue(req, res, "checked_by"));
		record.setCheckedat(getFormFieldValue(req, res, "checked_at"));
		record.setMakerlastcmt(getFormFieldValue(req, res, "maker_last_cmt"));
		record.setCheckerlastcmt(getFormFieldValue(req, res, "checker_last_cmt"));
		record.setCurrappstatus(getFormFieldValue(req, res, "curr_app_status"));
		record.setAdminlastcmt(getFormFieldValue(req, res, "admin_last_cmt"));
		record.setRstatus(getFormFieldValue(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setInstitutionid(getFormFieldValue(req, res, "institution_id"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIInstitutionRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIInstitutionRecord loadJSONFormLIInstitutionRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIInstitutionRecord", null);
		LIInstitutionRecord record = new LIInstitutionRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setIname(getFormFieldValueEncode(req, res, "iname"));
		record.setIlocation(getFormFieldValueEncode(req, res, "ilocation"));
		record.setMadeby(getFormFieldValueEncode(req, res, "made_by"));
		record.setMadeat(getFormFieldValueEncode(req, res, "made_at"));
		record.setCheckedby(getFormFieldValueEncode(req, res, "checked_by"));
		record.setCheckedat(getFormFieldValueEncode(req, res, "checked_at"));
		record.setMakerlastcmt(getFormFieldValueEncode(req, res, "maker_last_cmt"));
		record.setCheckerlastcmt(getFormFieldValueEncode(req, res, "checker_last_cmt"));
		record.setCurrappstatus(getFormFieldValueEncode(req, res, "curr_app_status"));
		record.setAdminlastcmt(getFormFieldValueEncode(req, res, "admin_last_cmt"));
		record.setRstatus(getFormFieldValueEncode(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setInstitutionid(getFormFieldValueEncode(req, res, "institution_id"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIInstitutionRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIInstitutionRecord loadMapLIInstitutionRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLIInstitutionRecord", null);
		LIInstitutionRecord record = new LIInstitutionRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setIname(getMapValue(inputMap, "iname"));
		record.setIlocation(getMapValue(inputMap, "ilocation"));
		record.setMadeby(getMapValue(inputMap, "made_by"));
		record.setMadeat(getMapValue(inputMap, "made_at"));
		record.setCheckedby(getMapValue(inputMap, "checked_by"));
		record.setCheckedat(getMapValue(inputMap, "checked_at"));
		record.setMakerlastcmt(getMapValue(inputMap, "maker_last_cmt"));
		record.setCheckerlastcmt(getMapValue(inputMap, "checker_last_cmt"));
		record.setCurrappstatus(getMapValue(inputMap, "curr_app_status"));
		record.setAdminlastcmt(getMapValue(inputMap, "admin_last_cmt"));
		record.setRstatus(getMapValue(inputMap, "rstatus"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		record.setInstitutionid(getMapValue(inputMap, "institution_id"));
		logger.trace("loadMapLIInstitutionRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLIInstitutionRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIInstitutionRecord", null);
		LIInstitutionService service = new LIInstitutionService();

		try
		{
			LIInstitutionRecord record = loadFormLIInstitutionRecord(req, res);
			record.setCreatedby(getLoggedInUserId(req,res));
			int resultId = service.insertLIInstitutionRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Record Creation Successful -" + resultId);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("institution.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create Institution");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create Institution " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("institution.jsp");
		}
	}

	public void processUpdateLIInstitutionRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIInstitutionRecord", null);
		LIInstitutionService service = new LIInstitutionService();

		try
		{
			LIInstitutionRecord record = loadFormLIInstitutionRecord(req, res);
			record.setModifiedby(getLoggedInUserId(req,res));
			service.updateLIInstitutionRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "Institution data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("institution.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update Institution");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update Institution " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("institution.jsp");
		}
	}

	public void processDeleteLIInstitutionRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIInstitutionRecord", null);
		LIInstitutionService service = new LIInstitutionService();

		try
		{
			LIInstitutionRecord record = loadFormLIInstitutionRecord(req, res);
			service.deleteLIInstitutionRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Institution deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("institution.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete Institution");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete Institution " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("institution.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertInstitutionRecord"))
		{
			processInsertLIInstitutionRecord(req, res);
			return;
		}

		if (actionType.equals("UpdateInstitutionRecord"))
		{
			processUpdateLIInstitutionRecord(req, res);
			return;
		}

		if (actionType.equals("DeleteInstitutionRecord"))
		{
			processDeleteLIInstitutionRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}
