
/*
 * LIReportquiresssiController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.controller;
import com.leadics.suzukithdashboard.common.LIController;
import com.leadics.suzukithdashboard.service.LIReportquiresssiService;
import com.leadics.suzukithdashboard.to.LIReportquiresssiRecord;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LIReportquiresssiController extends LIController
{
	static LogUtils logger = new LogUtils(LIReportquiresssiController.class.getName());

	public LIReportquiresssiRecord loadFormLIReportquiresssiRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLIReportquiresssiRecord", null);
		LIReportquiresssiRecord record = new LIReportquiresssiRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setReporttype(getFormFieldValue(req, res, "tfReporttype"));
		record.setReportname(getFormFieldValue(req, res, "tfReportname"));
		record.setQuery(getFormFieldValue(req, res, "tfQuery"));
		record.setCountquery(getFormFieldValue(req, res, "tfCountquery"));
		record.setJsontemplate(getFormFieldValue(req, res, "tfJsontemplate"));
		record.setRstatus(getFormFieldValue(req, res, "tfRstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setInstitutionid(getFormFieldValue(req, res, "tfInstitutionid"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLIReportquiresssiRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIReportquiresssiRecord loadJSONFormLIReportquiresssiRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIReportquiresssiRecord", null);
		LIReportquiresssiRecord record = new LIReportquiresssiRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setReporttype(getFormFieldValue(req, res, "reporttype"));
		record.setReportname(getFormFieldValue(req, res, "reportname"));
		record.setQuery(getFormFieldValue(req, res, "query"));
		record.setCountquery(getFormFieldValue(req, res, "countquery"));
		record.setJsontemplate(getFormFieldValue(req, res, "jsontemplate"));
		record.setRstatus(getFormFieldValue(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setInstitutionid(getFormFieldValue(req, res, "institution_id"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIReportquiresssiRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIReportquiresssiRecord loadJSONFormLIReportquiresssiRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIReportquiresssiRecord", null);
		LIReportquiresssiRecord record = new LIReportquiresssiRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setReporttype(getFormFieldValueEncode(req, res, "reporttype"));
		record.setReportname(getFormFieldValueEncode(req, res, "reportname"));
		record.setQuery(getFormFieldValueEncode(req, res, "query"));
		record.setCountquery(getFormFieldValueEncode(req, res, "countquery"));
		record.setJsontemplate(getFormFieldValueEncode(req, res, "jsontemplate"));
		record.setRstatus(getFormFieldValueEncode(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setInstitutionid(getFormFieldValueEncode(req, res, "institution_id"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIReportquiresssiRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIReportquiresssiRecord loadMapLIReportquiresssiRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLIReportquiresssiRecord", null);
		LIReportquiresssiRecord record = new LIReportquiresssiRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setReporttype(getMapValue(inputMap, "reporttype"));
		record.setReportname(getMapValue(inputMap, "reportname"));
		record.setQuery(getMapValue(inputMap, "query"));
		record.setCountquery(getMapValue(inputMap, "countquery"));
		record.setJsontemplate(getMapValue(inputMap, "jsontemplate"));
		record.setRstatus(getMapValue(inputMap, "rstatus"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		record.setInstitutionid(getMapValue(inputMap, "institution_id"));
		logger.trace("loadMapLIReportquiresssiRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLIReportquiresssiRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIReportquiresssiRecord", null);
		LIReportquiresssiService service = new LIReportquiresssiService();

		try
		{
			LIReportquiresssiRecord record = loadFormLIReportquiresssiRecord(req, res);
			record.setCreatedby(getLoggedInUserId(req,res));
			int resultId = service.insertLIReportquiresssiRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Record Creation Successful -" + resultId);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("reportquiresssi.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create Reportquiresssi");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create Reportquiresssi " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("reportquiresssi.jsp");
		}
	}

	public void processUpdateLIReportquiresssiRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIReportquiresssiRecord", null);
		LIReportquiresssiService service = new LIReportquiresssiService();

		try
		{
			LIReportquiresssiRecord record = loadFormLIReportquiresssiRecord(req, res);
			record.setModifiedby(getLoggedInUserId(req,res));
			service.updateLIReportquiresssiRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "Reportquiresssi data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("reportquiresssi.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update Reportquiresssi");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update Reportquiresssi " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("reportquiresssi.jsp");
		}
	}

	public void processDeleteLIReportquiresssiRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIReportquiresssiRecord", null);
		LIReportquiresssiService service = new LIReportquiresssiService();

		try
		{
			LIReportquiresssiRecord record = loadFormLIReportquiresssiRecord(req, res);
			service.deleteLIReportquiresssiRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Reportquiresssi deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("reportquiresssi.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete Reportquiresssi");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete Reportquiresssi " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("reportquiresssi.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertReportquiresssiRecord"))
		{
			processInsertLIReportquiresssiRecord(req, res);
			return;
		}

		if (actionType.equals("UpdateReportquiresssiRecord"))
		{
			processUpdateLIReportquiresssiRecord(req, res);
			return;
		}

		if (actionType.equals("DeleteReportquiresssiRecord"))
		{
			processDeleteLIReportquiresssiRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}
