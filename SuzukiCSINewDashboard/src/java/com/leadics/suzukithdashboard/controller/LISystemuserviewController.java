
/*
 * LISystemuserviewController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.controller;
import com.leadics.suzukithdashboard.common.LIController;
import com.leadics.suzukithdashboard.to.LISystemuserviewRecord;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LISystemuserviewController extends LIController
{
	static LogUtils logger = new LogUtils(LISystemuserviewController.class.getName());

	public LISystemuserviewRecord loadFormLISystemuserviewRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLISystemuserviewRecord", null);
		LISystemuserviewRecord record = new LISystemuserviewRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setUserid(getFormFieldValue(req, res, "tfUserid"));
		record.setUserpassword(getFormFieldValue(req, res, "tfUserpassword"));
		record.setPrefix(getFormFieldValue(req, res, "tfPrefix"));
		record.setFirstname(getFormFieldValue(req, res, "tfFirstname"));
		record.setMiddlename(getFormFieldValue(req, res, "tfMiddlename"));
		record.setLastname(getFormFieldValue(req, res, "tfLastname"));
		record.setEmail(getFormFieldValue(req, res, "tfEmail"));
		record.setMobile(getFormFieldValue(req, res, "tfMobile"));
		record.setInstitutionid(getFormFieldValue(req, res, "tfInstitutionid"));
		record.setCountry(getFormFieldValue(req, res, "tfCountry"));
		record.setLastlogindate(getFormFieldValue(req, res, "tfLastlogindate"));
		record.setLastloginfdate(getFormFieldValue(req, res, "tfLastloginfdate"));
		record.setLoginattempts(getFormFieldValue(req, res, "tfLoginattempts"));
		record.setPreflang(getFormFieldValue(req, res, "tfPreflang"));
		record.setPrefcomm(getFormFieldValue(req, res, "tfPrefcomm"));
		record.setUserroleid(getFormFieldValue(req, res, "tfUserroleid"));
		record.setSupid(getFormFieldValue(req, res, "tfSupid"));
		record.setSeqq1(getFormFieldValue(req, res, "tfSeqq1"));
		record.setSeqq2(getFormFieldValue(req, res, "tfSeqq2"));
		record.setSeqq3(getFormFieldValue(req, res, "tfSeqq3"));
		record.setSeqa1(getFormFieldValue(req, res, "tfSeqa1"));
		record.setSeqa2(getFormFieldValue(req, res, "tfSeqa2"));
		record.setSeqa3(getFormFieldValue(req, res, "tfSeqa3"));
		record.setMadeby(getFormFieldValue(req, res, "tfMadeby"));
		record.setMadeat(getFormFieldValue(req, res, "tfMadeat"));
		record.setCheckedby(getFormFieldValue(req, res, "tfCheckedby"));
		record.setCheckedat(getFormFieldValue(req, res, "tfCheckedat"));
		record.setMakerlastcmt(getFormFieldValue(req, res, "tfMakerlastcmt"));
		record.setCheckerlastcmt(getFormFieldValue(req, res, "tfCheckerlastcmt"));
		record.setCurrappstatus(getFormFieldValue(req, res, "tfCurrappstatus"));
		record.setAdminlastcmt(getFormFieldValue(req, res, "tfAdminlastcmt"));
		record.setRstatus(getFormFieldValue(req, res, "tfRstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setBlockedflag(getFormFieldValue(req, res, "tfBlockedflag"));
		record.setPwdsetdate(getFormFieldValue(req, res, "tfPwdsetdate"));
		record.setInitialpwd(getFormFieldValue(req, res, "tfInitialpwd"));
		record.setGroupcode(getFormFieldValue(req, res, "tfGroupcode"));
		record.setOtpgen(getFormFieldValue(req, res, "tfOtpgen"));
		record.setOtpgenat(getFormFieldValue(req, res, "tfOtpgenat"));
		record.setAddress(getFormFieldValue(req, res, "tfAddress"));
		record.setCity(getFormFieldValue(req, res, "tfCity"));
		record.setDregion(getFormFieldValue(req, res, "tfDregion"));
		record.setDgroup(getFormFieldValue(req, res, "tfDgroup"));
		record.setStatusname(getFormFieldValue(req, res, "tfStatusname"));
		record.setCurrappstatusname(getFormFieldValue(req, res, "tfCurrappstatusname"));
		record.setIname(getFormFieldValue(req, res, "tfIname"));
		record.setRolename(getFormFieldValue(req, res, "tfRolename"));
		record.setAdminperm(getFormFieldValue(req, res, "tfAdminperm"));
		record.setMakerperm(getFormFieldValue(req, res, "tfMakerperm"));
		record.setCheckerperm(getFormFieldValue(req, res, "tfCheckerperm"));
		record.setGlobaladminperm(getFormFieldValue(req, res, "tfGlobaladminperm"));
		record.setReportperm(getFormFieldValue(req, res, "tfReportperm"));
		record.setAuditperm(getFormFieldValue(req, res, "tfAuditperm"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLISystemuserviewRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LISystemuserviewRecord loadJSONFormLISystemuserviewRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLISystemuserviewRecord", null);
		LISystemuserviewRecord record = new LISystemuserviewRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setUserid(getFormFieldValue(req, res, "user_id"));
		record.setUserpassword(getFormFieldValue(req, res, "user_password"));
		record.setPrefix(getFormFieldValue(req, res, "prefix"));
		record.setFirstname(getFormFieldValue(req, res, "first_name"));
		record.setMiddlename(getFormFieldValue(req, res, "middle_name"));
		record.setLastname(getFormFieldValue(req, res, "last_name"));
		record.setEmail(getFormFieldValue(req, res, "email"));
		record.setMobile(getFormFieldValue(req, res, "mobile"));
		record.setInstitutionid(getFormFieldValue(req, res, "institution_id"));
		record.setCountry(getFormFieldValue(req, res, "country"));
		record.setLastlogindate(getFormFieldValue(req, res, "last_login_date"));
		record.setLastloginfdate(getFormFieldValue(req, res, "last_login_fdate"));
		record.setLoginattempts(getFormFieldValue(req, res, "login_attempts"));
		record.setPreflang(getFormFieldValue(req, res, "pref_lang"));
		record.setPrefcomm(getFormFieldValue(req, res, "pref_comm"));
		record.setUserroleid(getFormFieldValue(req, res, "user_role_id"));
		record.setSupid(getFormFieldValue(req, res, "sup_id"));
		record.setSeqq1(getFormFieldValue(req, res, "seq_q1"));
		record.setSeqq2(getFormFieldValue(req, res, "seq_q2"));
		record.setSeqq3(getFormFieldValue(req, res, "seq_q3"));
		record.setSeqa1(getFormFieldValue(req, res, "seq_a1"));
		record.setSeqa2(getFormFieldValue(req, res, "seq_a2"));
		record.setSeqa3(getFormFieldValue(req, res, "seq_a3"));
		record.setMadeby(getFormFieldValue(req, res, "made_by"));
		record.setMadeat(getFormFieldValue(req, res, "made_at"));
		record.setCheckedby(getFormFieldValue(req, res, "checked_by"));
		record.setCheckedat(getFormFieldValue(req, res, "checked_at"));
		record.setMakerlastcmt(getFormFieldValue(req, res, "maker_last_cmt"));
		record.setCheckerlastcmt(getFormFieldValue(req, res, "checker_last_cmt"));
		record.setCurrappstatus(getFormFieldValue(req, res, "curr_app_status"));
		record.setAdminlastcmt(getFormFieldValue(req, res, "admin_last_cmt"));
		record.setRstatus(getFormFieldValue(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setBlockedflag(getFormFieldValue(req, res, "blocked_flag"));
		record.setPwdsetdate(getFormFieldValue(req, res, "pwdsetdate"));
		record.setInitialpwd(getFormFieldValue(req, res, "initial_pwd"));
		record.setGroupcode(getFormFieldValue(req, res, "group_code"));
		record.setOtpgen(getFormFieldValue(req, res, "otp_gen"));
		record.setOtpgenat(getFormFieldValue(req, res, "otp_gen_at"));
		record.setAddress(getFormFieldValue(req, res, "address"));
		record.setCity(getFormFieldValue(req, res, "city"));
		record.setDregion(getFormFieldValue(req, res, "dregion"));
		record.setDgroup(getFormFieldValue(req, res, "dgroup"));
		record.setStatusname(getFormFieldValue(req, res, "status_name"));
		record.setCurrappstatusname(getFormFieldValue(req, res, "curr_app_status_name"));
		record.setIname(getFormFieldValue(req, res, "iname"));
		record.setRolename(getFormFieldValue(req, res, "rolename"));
		record.setAdminperm(getFormFieldValue(req, res, "admin_perm"));
		record.setMakerperm(getFormFieldValue(req, res, "maker_perm"));
		record.setCheckerperm(getFormFieldValue(req, res, "checker_perm"));
		record.setGlobaladminperm(getFormFieldValue(req, res, "global_admin_perm"));
		record.setReportperm(getFormFieldValue(req, res, "report_perm"));
		record.setAuditperm(getFormFieldValue(req, res, "audit_perm"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLISystemuserviewRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LISystemuserviewRecord loadJSONFormLISystemuserviewRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLISystemuserviewRecord", null);
		LISystemuserviewRecord record = new LISystemuserviewRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setUserid(getFormFieldValueEncode(req, res, "user_id"));
		record.setUserpassword(getFormFieldValueEncode(req, res, "user_password"));
		record.setPrefix(getFormFieldValueEncode(req, res, "prefix"));
		record.setFirstname(getFormFieldValueEncode(req, res, "first_name"));
		record.setMiddlename(getFormFieldValueEncode(req, res, "middle_name"));
		record.setLastname(getFormFieldValueEncode(req, res, "last_name"));
		record.setEmail(getFormFieldValueEncode(req, res, "email"));
		record.setMobile(getFormFieldValueEncode(req, res, "mobile"));
		record.setInstitutionid(getFormFieldValueEncode(req, res, "institution_id"));
		record.setCountry(getFormFieldValueEncode(req, res, "country"));
		record.setLastlogindate(getFormFieldValueEncode(req, res, "last_login_date"));
		record.setLastloginfdate(getFormFieldValueEncode(req, res, "last_login_fdate"));
		record.setLoginattempts(getFormFieldValueEncode(req, res, "login_attempts"));
		record.setPreflang(getFormFieldValueEncode(req, res, "pref_lang"));
		record.setPrefcomm(getFormFieldValueEncode(req, res, "pref_comm"));
		record.setUserroleid(getFormFieldValueEncode(req, res, "user_role_id"));
		record.setSupid(getFormFieldValueEncode(req, res, "sup_id"));
		record.setSeqq1(getFormFieldValueEncode(req, res, "seq_q1"));
		record.setSeqq2(getFormFieldValueEncode(req, res, "seq_q2"));
		record.setSeqq3(getFormFieldValueEncode(req, res, "seq_q3"));
		record.setSeqa1(getFormFieldValueEncode(req, res, "seq_a1"));
		record.setSeqa2(getFormFieldValueEncode(req, res, "seq_a2"));
		record.setSeqa3(getFormFieldValueEncode(req, res, "seq_a3"));
		record.setMadeby(getFormFieldValueEncode(req, res, "made_by"));
		record.setMadeat(getFormFieldValueEncode(req, res, "made_at"));
		record.setCheckedby(getFormFieldValueEncode(req, res, "checked_by"));
		record.setCheckedat(getFormFieldValueEncode(req, res, "checked_at"));
		record.setMakerlastcmt(getFormFieldValueEncode(req, res, "maker_last_cmt"));
		record.setCheckerlastcmt(getFormFieldValueEncode(req, res, "checker_last_cmt"));
		record.setCurrappstatus(getFormFieldValueEncode(req, res, "curr_app_status"));
		record.setAdminlastcmt(getFormFieldValueEncode(req, res, "admin_last_cmt"));
		record.setRstatus(getFormFieldValueEncode(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setBlockedflag(getFormFieldValueEncode(req, res, "blocked_flag"));
		record.setPwdsetdate(getFormFieldValueEncode(req, res, "pwdsetdate"));
		record.setInitialpwd(getFormFieldValueEncode(req, res, "initial_pwd"));
		record.setGroupcode(getFormFieldValueEncode(req, res, "group_code"));
		record.setOtpgen(getFormFieldValueEncode(req, res, "otp_gen"));
		record.setOtpgenat(getFormFieldValueEncode(req, res, "otp_gen_at"));
		record.setAddress(getFormFieldValueEncode(req, res, "address"));
		record.setCity(getFormFieldValueEncode(req, res, "city"));
		record.setDregion(getFormFieldValueEncode(req, res, "dregion"));
		record.setDgroup(getFormFieldValueEncode(req, res, "dgroup"));
		record.setStatusname(getFormFieldValueEncode(req, res, "status_name"));
		record.setCurrappstatusname(getFormFieldValueEncode(req, res, "curr_app_status_name"));
		record.setIname(getFormFieldValueEncode(req, res, "iname"));
		record.setRolename(getFormFieldValueEncode(req, res, "rolename"));
		record.setAdminperm(getFormFieldValueEncode(req, res, "admin_perm"));
		record.setMakerperm(getFormFieldValueEncode(req, res, "maker_perm"));
		record.setCheckerperm(getFormFieldValueEncode(req, res, "checker_perm"));
		record.setGlobaladminperm(getFormFieldValueEncode(req, res, "global_admin_perm"));
		record.setReportperm(getFormFieldValueEncode(req, res, "report_perm"));
		record.setAuditperm(getFormFieldValueEncode(req, res, "audit_perm"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLISystemuserviewRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LISystemuserviewRecord loadMapLISystemuserviewRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLISystemuserviewRecord", null);
		LISystemuserviewRecord record = new LISystemuserviewRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setUserid(getMapValue(inputMap, "user_id"));
		record.setUserpassword(getMapValue(inputMap, "user_password"));
		record.setPrefix(getMapValue(inputMap, "prefix"));
		record.setFirstname(getMapValue(inputMap, "first_name"));
		record.setMiddlename(getMapValue(inputMap, "middle_name"));
		record.setLastname(getMapValue(inputMap, "last_name"));
		record.setEmail(getMapValue(inputMap, "email"));
		record.setMobile(getMapValue(inputMap, "mobile"));
		record.setInstitutionid(getMapValue(inputMap, "institution_id"));
		record.setCountry(getMapValue(inputMap, "country"));
		record.setLastlogindate(getMapValue(inputMap, "last_login_date"));
		record.setLastloginfdate(getMapValue(inputMap, "last_login_fdate"));
		record.setLoginattempts(getMapValue(inputMap, "login_attempts"));
		record.setPreflang(getMapValue(inputMap, "pref_lang"));
		record.setPrefcomm(getMapValue(inputMap, "pref_comm"));
		record.setUserroleid(getMapValue(inputMap, "user_role_id"));
		record.setSupid(getMapValue(inputMap, "sup_id"));
		record.setSeqq1(getMapValue(inputMap, "seq_q1"));
		record.setSeqq2(getMapValue(inputMap, "seq_q2"));
		record.setSeqq3(getMapValue(inputMap, "seq_q3"));
		record.setSeqa1(getMapValue(inputMap, "seq_a1"));
		record.setSeqa2(getMapValue(inputMap, "seq_a2"));
		record.setSeqa3(getMapValue(inputMap, "seq_a3"));
		record.setMadeby(getMapValue(inputMap, "made_by"));
		record.setMadeat(getMapValue(inputMap, "made_at"));
		record.setCheckedby(getMapValue(inputMap, "checked_by"));
		record.setCheckedat(getMapValue(inputMap, "checked_at"));
		record.setMakerlastcmt(getMapValue(inputMap, "maker_last_cmt"));
		record.setCheckerlastcmt(getMapValue(inputMap, "checker_last_cmt"));
		record.setCurrappstatus(getMapValue(inputMap, "curr_app_status"));
		record.setAdminlastcmt(getMapValue(inputMap, "admin_last_cmt"));
		record.setRstatus(getMapValue(inputMap, "rstatus"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		record.setBlockedflag(getMapValue(inputMap, "blocked_flag"));
		record.setPwdsetdate(getMapValue(inputMap, "pwdsetdate"));
		record.setInitialpwd(getMapValue(inputMap, "initial_pwd"));
		record.setGroupcode(getMapValue(inputMap, "group_code"));
		record.setOtpgen(getMapValue(inputMap, "otp_gen"));
		record.setOtpgenat(getMapValue(inputMap, "otp_gen_at"));
		record.setAddress(getMapValue(inputMap, "address"));
		record.setCity(getMapValue(inputMap, "city"));
		record.setDregion(getMapValue(inputMap, "dregion"));
		record.setDgroup(getMapValue(inputMap, "dgroup"));
		record.setStatusname(getMapValue(inputMap, "status_name"));
		record.setCurrappstatusname(getMapValue(inputMap, "curr_app_status_name"));
		record.setIname(getMapValue(inputMap, "iname"));
		record.setRolename(getMapValue(inputMap, "rolename"));
		record.setAdminperm(getMapValue(inputMap, "admin_perm"));
		record.setMakerperm(getMapValue(inputMap, "maker_perm"));
		record.setCheckerperm(getMapValue(inputMap, "checker_perm"));
		record.setGlobaladminperm(getMapValue(inputMap, "global_admin_perm"));
		record.setReportperm(getMapValue(inputMap, "report_perm"));
		record.setAuditperm(getMapValue(inputMap, "audit_perm"));
		logger.trace("loadMapLISystemuserviewRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}
}
