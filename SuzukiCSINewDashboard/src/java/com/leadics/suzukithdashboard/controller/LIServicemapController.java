
/*
 * LIServicemapController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.controller;
import com.leadics.suzukithdashboard.common.LIController;
import com.leadics.suzukithdashboard.service.LIServicemapService;
import com.leadics.suzukithdashboard.to.LIServicemapRecord;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LIServicemapController extends LIController
{
	static LogUtils logger = new LogUtils(LIServicemapController.class.getName());

	public LIServicemapRecord loadFormLIServicemapRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLIServicemapRecord", null);
		LIServicemapRecord record = new LIServicemapRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setServicecode(getFormFieldValue(req, res, "tfServicecode"));
		record.setServiceclass(getFormFieldValue(req, res, "tfServiceclass"));
		record.setServicemethod(getFormFieldValue(req, res, "tfServicemethod"));
		record.setIntenv(getFormFieldValue(req, res, "tfIntenv"));
		record.setRstatus(getFormFieldValue(req, res, "tfRstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setServicetitle(getFormFieldValue(req, res, "tfServicetitle"));
		record.setInstid(getFormFieldValue(req, res, "tfInstid"));
		record.setMadeby(getFormFieldValue(req, res, "tfMadeby"));
		record.setMadeat(getFormFieldValue(req, res, "tfMadeat"));
		record.setCheckedby(getFormFieldValue(req, res, "tfCheckedby"));
		record.setCheckedat(getFormFieldValue(req, res, "tfCheckedat"));
		record.setMakerlastcmt(getFormFieldValue(req, res, "tfMakerlastcmt"));
		record.setCheckerlastcmt(getFormFieldValue(req, res, "tfCheckerlastcmt"));
		record.setCurrappstatus(getFormFieldValue(req, res, "tfCurrappstatus"));
		record.setAdminlastcmt(getFormFieldValue(req, res, "tfAdminlastcmt"));
		record.setInstitutionid(getFormFieldValue(req, res, "tfInstitutionid"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLIServicemapRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIServicemapRecord loadJSONFormLIServicemapRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIServicemapRecord", null);
		LIServicemapRecord record = new LIServicemapRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setServicecode(getFormFieldValue(req, res, "service_code"));
		record.setServiceclass(getFormFieldValue(req, res, "service_class"));
		record.setServicemethod(getFormFieldValue(req, res, "service_method"));
		record.setIntenv(getFormFieldValue(req, res, "int_env"));
		record.setRstatus(getFormFieldValue(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setServicetitle(getFormFieldValue(req, res, "service_title"));
		record.setInstid(getFormFieldValue(req, res, "inst_id"));
		record.setMadeby(getFormFieldValue(req, res, "made_by"));
		record.setMadeat(getFormFieldValue(req, res, "made_at"));
		record.setCheckedby(getFormFieldValue(req, res, "checked_by"));
		record.setCheckedat(getFormFieldValue(req, res, "checked_at"));
		record.setMakerlastcmt(getFormFieldValue(req, res, "maker_last_cmt"));
		record.setCheckerlastcmt(getFormFieldValue(req, res, "checker_last_cmt"));
		record.setCurrappstatus(getFormFieldValue(req, res, "curr_app_status"));
		record.setAdminlastcmt(getFormFieldValue(req, res, "admin_last_cmt"));
		record.setInstitutionid(getFormFieldValue(req, res, "institution_id"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIServicemapRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIServicemapRecord loadJSONFormLIServicemapRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIServicemapRecord", null);
		LIServicemapRecord record = new LIServicemapRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setServicecode(getFormFieldValueEncode(req, res, "service_code"));
		record.setServiceclass(getFormFieldValueEncode(req, res, "service_class"));
		record.setServicemethod(getFormFieldValueEncode(req, res, "service_method"));
		record.setIntenv(getFormFieldValueEncode(req, res, "int_env"));
		record.setRstatus(getFormFieldValueEncode(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setServicetitle(getFormFieldValueEncode(req, res, "service_title"));
		record.setInstid(getFormFieldValueEncode(req, res, "inst_id"));
		record.setMadeby(getFormFieldValueEncode(req, res, "made_by"));
		record.setMadeat(getFormFieldValueEncode(req, res, "made_at"));
		record.setCheckedby(getFormFieldValueEncode(req, res, "checked_by"));
		record.setCheckedat(getFormFieldValueEncode(req, res, "checked_at"));
		record.setMakerlastcmt(getFormFieldValueEncode(req, res, "maker_last_cmt"));
		record.setCheckerlastcmt(getFormFieldValueEncode(req, res, "checker_last_cmt"));
		record.setCurrappstatus(getFormFieldValueEncode(req, res, "curr_app_status"));
		record.setAdminlastcmt(getFormFieldValueEncode(req, res, "admin_last_cmt"));
		record.setInstitutionid(getFormFieldValueEncode(req, res, "institution_id"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIServicemapRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIServicemapRecord loadMapLIServicemapRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLIServicemapRecord", null);
		LIServicemapRecord record = new LIServicemapRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setServicecode(getMapValue(inputMap, "service_code"));
		record.setServiceclass(getMapValue(inputMap, "service_class"));
		record.setServicemethod(getMapValue(inputMap, "service_method"));
		record.setIntenv(getMapValue(inputMap, "int_env"));
		record.setRstatus(getMapValue(inputMap, "rstatus"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		record.setServicetitle(getMapValue(inputMap, "service_title"));
		record.setInstid(getMapValue(inputMap, "inst_id"));
		record.setMadeby(getMapValue(inputMap, "made_by"));
		record.setMadeat(getMapValue(inputMap, "made_at"));
		record.setCheckedby(getMapValue(inputMap, "checked_by"));
		record.setCheckedat(getMapValue(inputMap, "checked_at"));
		record.setMakerlastcmt(getMapValue(inputMap, "maker_last_cmt"));
		record.setCheckerlastcmt(getMapValue(inputMap, "checker_last_cmt"));
		record.setCurrappstatus(getMapValue(inputMap, "curr_app_status"));
		record.setAdminlastcmt(getMapValue(inputMap, "admin_last_cmt"));
		record.setInstitutionid(getMapValue(inputMap, "institution_id"));
		logger.trace("loadMapLIServicemapRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLIServicemapRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIServicemapRecord", null);
		LIServicemapService service = new LIServicemapService();

		try
		{
			LIServicemapRecord record = loadFormLIServicemapRecord(req, res);
			record.setCreatedby(getLoggedInUserId(req,res));
			int resultId = service.insertLIServicemapRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Record Creation Successful -" + resultId);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("servicemap.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create Servicemap");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create Servicemap " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("servicemap.jsp");
		}
	}

	public void processUpdateLIServicemapRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIServicemapRecord", null);
		LIServicemapService service = new LIServicemapService();

		try
		{
			LIServicemapRecord record = loadFormLIServicemapRecord(req, res);
			record.setModifiedby(getLoggedInUserId(req,res));
			service.updateLIServicemapRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "Servicemap data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("servicemap.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update Servicemap");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update Servicemap " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("servicemap.jsp");
		}
	}

	public void processDeleteLIServicemapRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIServicemapRecord", null);
		LIServicemapService service = new LIServicemapService();

		try
		{
			LIServicemapRecord record = loadFormLIServicemapRecord(req, res);
			service.deleteLIServicemapRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Servicemap deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("servicemap.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete Servicemap");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete Servicemap " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("servicemap.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertServicemapRecord"))
		{
			processInsertLIServicemapRecord(req, res);
			return;
		}

		if (actionType.equals("UpdateServicemapRecord"))
		{
			processUpdateLIServicemapRecord(req, res);
			return;
		}

		if (actionType.equals("DeleteServicemapRecord"))
		{
			processDeleteLIServicemapRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}
