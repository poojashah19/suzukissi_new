
/*
 * LIcalenderfilterController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.controller;
import com.leadics.suzukithdashboard.common.LIController;
import com.leadics.suzukithdashboard.service.LIcalenderfilterService;
import com.leadics.suzukithdashboard.to.LIcalenderfilterRecord;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LIcalenderfilterController extends LIController
{
	static LogUtils logger = new LogUtils(LIcalenderfilterController.class.getName());

	public LIcalenderfilterRecord loadFormLIcalenderfilterRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLIcalenderfilterRecord", null);
		LIcalenderfilterRecord record = new LIcalenderfilterRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setQuarter(getFormFieldValue(req, res, "tfQuarter"));
		record.setMonth(getFormFieldValue(req, res, "tfMonth"));
		record.setBiweekly(getFormFieldValue(req, res, "tfBiweekly"));
		record.setDate(getFormFieldValue(req, res, "tfDate"));
		record.setYear(getFormFieldValue(req, res, "tfYear"));
		record.setRstatus(getFormFieldValue(req, res, "tfRstatus"));
		record.setLastacc(getFormFieldValue(req, res, "tfLastacc"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setClosetype(getFormFieldValue(req, res, "tfClosetype"));
		record.setClosedat(getFormFieldValue(req, res, "tfClosedat"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLIcalenderfilterRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIcalenderfilterRecord loadJSONFormLIcalenderfilterRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIcalenderfilterRecord", null);
		LIcalenderfilterRecord record = new LIcalenderfilterRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setQuarter(getFormFieldValue(req, res, "quarter"));
		record.setMonth(getFormFieldValue(req, res, "month"));
		record.setBiweekly(getFormFieldValue(req, res, "biweekly"));
		record.setDate(getFormFieldValue(req, res, "date"));
		record.setYear(getFormFieldValue(req, res, "year"));
		record.setRstatus(getFormFieldValue(req, res, "rstatus"));
		record.setLastacc(getFormFieldValue(req, res, "lastacc"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setClosetype(getFormFieldValue(req, res, "close_type"));
		record.setClosedat(getFormFieldValue(req, res, "closed_at"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIcalenderfilterRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIcalenderfilterRecord loadJSONFormLIcalenderfilterRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIcalenderfilterRecord", null);
		LIcalenderfilterRecord record = new LIcalenderfilterRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setQuarter(getFormFieldValueEncode(req, res, "quarter"));
		record.setMonth(getFormFieldValueEncode(req, res, "month"));
		record.setBiweekly(getFormFieldValueEncode(req, res, "biweekly"));
		record.setDate(getFormFieldValueEncode(req, res, "date"));
		record.setYear(getFormFieldValueEncode(req, res, "year"));
		record.setRstatus(getFormFieldValueEncode(req, res, "rstatus"));
		record.setLastacc(getFormFieldValueEncode(req, res, "lastacc"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setClosetype(getFormFieldValueEncode(req, res, "close_type"));
		record.setClosedat(getFormFieldValueEncode(req, res, "closed_at"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIcalenderfilterRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIcalenderfilterRecord loadMapLIcalenderfilterRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLIcalenderfilterRecord", null);
		LIcalenderfilterRecord record = new LIcalenderfilterRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setQuarter(getMapValue(inputMap, "quarter"));
		record.setMonth(getMapValue(inputMap, "month"));
		record.setBiweekly(getMapValue(inputMap, "biweekly"));
		record.setDate(getMapValue(inputMap, "date"));
		record.setYear(getMapValue(inputMap, "year"));
		record.setRstatus(getMapValue(inputMap, "rstatus"));
		record.setLastacc(getMapValue(inputMap, "lastacc"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		record.setClosetype(getMapValue(inputMap, "close_type"));
		record.setClosedat(getMapValue(inputMap, "closed_at"));
		logger.trace("loadMapLIcalenderfilterRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLIcalenderfilterRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIcalenderfilterRecord", null);
		LIcalenderfilterService service = new LIcalenderfilterService();

		try
		{
			LIcalenderfilterRecord record = loadFormLIcalenderfilterRecord(req, res);
			record.setCreatedby(getLoggedInUserId(req,res));
			int resultId = service.insertLIcalenderfilterRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Record Creation Successful -" + resultId);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("calenderfilter.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create calenderfilter");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create calenderfilter " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("calenderfilter.jsp");
		}
	}

	public void processUpdateLIcalenderfilterRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIcalenderfilterRecord", null);
		LIcalenderfilterService service = new LIcalenderfilterService();

		try
		{
			LIcalenderfilterRecord record = loadFormLIcalenderfilterRecord(req, res);
			record.setModifiedby(getLoggedInUserId(req,res));
			service.updateLIcalenderfilterRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "calenderfilter data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("calenderfilter.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update calenderfilter");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update calenderfilter " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("calenderfilter.jsp");
		}
	}

	public void processDeleteLIcalenderfilterRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIcalenderfilterRecord", null);
		LIcalenderfilterService service = new LIcalenderfilterService();

		try
		{
			LIcalenderfilterRecord record = loadFormLIcalenderfilterRecord(req, res);
			service.deleteLIcalenderfilterRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "calenderfilter deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("calenderfilter.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete calenderfilter");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete calenderfilter " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("calenderfilter.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertcalenderfilterRecord"))
		{
			processInsertLIcalenderfilterRecord(req, res);
			return;
		}

		if (actionType.equals("UpdatecalenderfilterRecord"))
		{
			processUpdateLIcalenderfilterRecord(req, res);
			return;
		}

		if (actionType.equals("DeletecalenderfilterRecord"))
		{
			processDeleteLIcalenderfilterRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}
