
/*
 * LICsiothersaggregateController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.controller;
import com.leadics.suzukithdashboard.common.LIController;
import com.leadics.suzukithdashboard.service.LICsiothersaggregateService;
import com.leadics.suzukithdashboard.to.LICsiothersaggregateRecord;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LICsiothersaggregateController extends LIController
{
	static LogUtils logger = new LogUtils(LICsiothersaggregateController.class.getName());

	public LICsiothersaggregateRecord loadFormLICsiothersaggregateRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLICsiothersaggregateRecord", null);
		LICsiothersaggregateRecord record = new LICsiothersaggregateRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setAttribute(getFormFieldValue(req, res, "tfAttribute"));
		record.setAttributename(getFormFieldValue(req, res, "tfAttributename"));
		record.setAttributeindex(getFormFieldValue(req, res, "tfAttributeindex"));
		record.setFactor(getFormFieldValue(req, res, "tfFactor"));
		record.setRegion(getFormFieldValue(req, res, "tfRegion"));
		record.setState(getFormFieldValue(req, res, "tfState"));
		record.setCity(getFormFieldValue(req, res, "tfCity"));
		record.setDealer(getFormFieldValue(req, res, "tfDealer"));
		record.setModel(getFormFieldValue(req, res, "tfModel"));
		record.setBiweekly(getFormFieldValue(req, res, "tfBiweekly"));
		record.setMonth(getFormFieldValue(req, res, "tfMonth"));
		record.setQuarter(getFormFieldValue(req, res, "tfQuarter"));
		record.setValue(getFormFieldValue(req, res, "tfValue"));
		record.setDealerothercount(getFormFieldValue(req, res, "tfDealerothercount"));
		record.setRstatus(getFormFieldValue(req, res, "tfRstatus"));
		record.setLastacc(getFormFieldValue(req, res, "tfLastacc"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setClosetype(getFormFieldValue(req, res, "tfClosetype"));
		record.setClosedat(getFormFieldValue(req, res, "tfClosedat"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLICsiothersaggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LICsiothersaggregateRecord loadJSONFormLICsiothersaggregateRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLICsiothersaggregateRecord", null);
		LICsiothersaggregateRecord record = new LICsiothersaggregateRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setAttribute(getFormFieldValue(req, res, "attribute"));
		record.setAttributename(getFormFieldValue(req, res, "attributename"));
		record.setAttributeindex(getFormFieldValue(req, res, "attributeindex"));
		record.setFactor(getFormFieldValue(req, res, "factor"));
		record.setRegion(getFormFieldValue(req, res, "region"));
		record.setState(getFormFieldValue(req, res, "state"));
		record.setCity(getFormFieldValue(req, res, "city"));
		record.setDealer(getFormFieldValue(req, res, "dealer"));
		record.setModel(getFormFieldValue(req, res, "model"));
		record.setBiweekly(getFormFieldValue(req, res, "bi_weekly"));
		record.setMonth(getFormFieldValue(req, res, "month"));
		record.setQuarter(getFormFieldValue(req, res, "quarter"));
		record.setValue(getFormFieldValue(req, res, "value"));
		record.setDealerothercount(getFormFieldValue(req, res, "dealer_other_count"));
		record.setRstatus(getFormFieldValue(req, res, "rstatus"));
		record.setLastacc(getFormFieldValue(req, res, "lastacc"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setClosetype(getFormFieldValue(req, res, "close_type"));
		record.setClosedat(getFormFieldValue(req, res, "closed_at"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLICsiothersaggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LICsiothersaggregateRecord loadJSONFormLICsiothersaggregateRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLICsiothersaggregateRecord", null);
		LICsiothersaggregateRecord record = new LICsiothersaggregateRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setAttribute(getFormFieldValueEncode(req, res, "attribute"));
		record.setAttributename(getFormFieldValueEncode(req, res, "attributename"));
		record.setAttributeindex(getFormFieldValueEncode(req, res, "attributeindex"));
		record.setFactor(getFormFieldValueEncode(req, res, "factor"));
		record.setRegion(getFormFieldValueEncode(req, res, "region"));
		record.setState(getFormFieldValueEncode(req, res, "state"));
		record.setCity(getFormFieldValueEncode(req, res, "city"));
		record.setDealer(getFormFieldValueEncode(req, res, "dealer"));
		record.setModel(getFormFieldValueEncode(req, res, "model"));
		record.setBiweekly(getFormFieldValueEncode(req, res, "bi_weekly"));
		record.setMonth(getFormFieldValueEncode(req, res, "month"));
		record.setQuarter(getFormFieldValueEncode(req, res, "quarter"));
		record.setValue(getFormFieldValueEncode(req, res, "value"));
		record.setDealerothercount(getFormFieldValueEncode(req, res, "dealer_other_count"));
		record.setRstatus(getFormFieldValueEncode(req, res, "rstatus"));
		record.setLastacc(getFormFieldValueEncode(req, res, "lastacc"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setClosetype(getFormFieldValueEncode(req, res, "close_type"));
		record.setClosedat(getFormFieldValueEncode(req, res, "closed_at"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLICsiothersaggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LICsiothersaggregateRecord loadMapLICsiothersaggregateRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLICsiothersaggregateRecord", null);
		LICsiothersaggregateRecord record = new LICsiothersaggregateRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setAttribute(getMapValue(inputMap, "attribute"));
		record.setAttributename(getMapValue(inputMap, "attributename"));
		record.setAttributeindex(getMapValue(inputMap, "attributeindex"));
		record.setFactor(getMapValue(inputMap, "factor"));
		record.setRegion(getMapValue(inputMap, "region"));
		record.setState(getMapValue(inputMap, "state"));
		record.setCity(getMapValue(inputMap, "city"));
		record.setDealer(getMapValue(inputMap, "dealer"));
		record.setModel(getMapValue(inputMap, "model"));
		record.setBiweekly(getMapValue(inputMap, "bi_weekly"));
		record.setMonth(getMapValue(inputMap, "month"));
		record.setQuarter(getMapValue(inputMap, "quarter"));
		record.setValue(getMapValue(inputMap, "value"));
		record.setDealerothercount(getMapValue(inputMap, "dealer_other_count"));
		record.setRstatus(getMapValue(inputMap, "rstatus"));
		record.setLastacc(getMapValue(inputMap, "lastacc"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		record.setClosetype(getMapValue(inputMap, "close_type"));
		record.setClosedat(getMapValue(inputMap, "closed_at"));
		logger.trace("loadMapLICsiothersaggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLICsiothersaggregateRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLICsiothersaggregateRecord", null);
		LICsiothersaggregateService service = new LICsiothersaggregateService();

		try
		{
			LICsiothersaggregateRecord record = loadFormLICsiothersaggregateRecord(req, res);
			record.setCreatedby(getLoggedInUserId(req,res));
			int resultId = service.insertLICsiothersaggregateRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Record Creation Successful -" + resultId);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csiothersaggregate.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create Csiothersaggregate");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create Csiothersaggregate " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csiothersaggregate.jsp");
		}
	}

	public void processUpdateLICsiothersaggregateRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLICsiothersaggregateRecord", null);
		LICsiothersaggregateService service = new LICsiothersaggregateService();

		try
		{
			LICsiothersaggregateRecord record = loadFormLICsiothersaggregateRecord(req, res);
			record.setModifiedby(getLoggedInUserId(req,res));
			service.updateLICsiothersaggregateRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "Csiothersaggregate data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csiothersaggregate.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update Csiothersaggregate");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update Csiothersaggregate " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csiothersaggregate.jsp");
		}
	}

	public void processDeleteLICsiothersaggregateRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLICsiothersaggregateRecord", null);
		LICsiothersaggregateService service = new LICsiothersaggregateService();

		try
		{
			LICsiothersaggregateRecord record = loadFormLICsiothersaggregateRecord(req, res);
			service.deleteLICsiothersaggregateRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Csiothersaggregate deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csiothersaggregate.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete Csiothersaggregate");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete Csiothersaggregate " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csiothersaggregate.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertCsiothersaggregateRecord"))
		{
			processInsertLICsiothersaggregateRecord(req, res);
			return;
		}

		if (actionType.equals("UpdateCsiothersaggregateRecord"))
		{
			processUpdateLICsiothersaggregateRecord(req, res);
			return;
		}

		if (actionType.equals("DeleteCsiothersaggregateRecord"))
		{
			processDeleteLICsiothersaggregateRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}
