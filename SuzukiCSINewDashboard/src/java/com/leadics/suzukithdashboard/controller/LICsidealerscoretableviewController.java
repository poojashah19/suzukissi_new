
/*
 * LICsidealerscoretableviewController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.controller;
import com.leadics.suzukithdashboard.common.LIController;
import com.leadics.suzukithdashboard.to.LICsidealerscoretableviewRecord;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LICsidealerscoretableviewController extends LIController
{
	static LogUtils logger = new LogUtils(LICsidealerscoretableviewController.class.getName());

	public LICsidealerscoretableviewRecord loadFormLICsidealerscoretableviewRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLICsidealerscoretableviewRecord", null);
		LICsidealerscoretableviewRecord record = new LICsidealerscoretableviewRecord();
		record.setRegion(getFormFieldValue(req, res, "tfRegion"));
		record.setDealer(getFormFieldValue(req, res, "tfDealer"));
		record.setCsi(getFormFieldValue(req, res, "tfCsi"));
		record.setCsicolor(getFormFieldValue(req, res, "tfCsicolor"));
		record.setServiceadvisor(getFormFieldValue(req, res, "tfServiceadvisor"));
		record.setServiceadvisorcolor(getFormFieldValue(req, res, "tfServiceadvisorcolor"));
		record.setServicefacility(getFormFieldValue(req, res, "tfServicefacility"));
		record.setServicefacilitycolor(getFormFieldValue(req, res, "tfServicefacilitycolor"));
		record.setServiceinitiation(getFormFieldValue(req, res, "tfServiceinitiation"));
		record.setServiceinitiationcolor(getFormFieldValue(req, res, "tfServiceinitiationcolor"));
		record.setServicequality(getFormFieldValue(req, res, "tfServicequality"));
		record.setServicequalitycolor(getFormFieldValue(req, res, "tfServicequalitycolor"));
		record.setVehiclepickup(getFormFieldValue(req, res, "tfVehiclepickup"));
		record.setVehiclepickupcolor(getFormFieldValue(req, res, "tfVehiclepickupcolor"));
		record.setSop(getFormFieldValue(req, res, "tfSop"));
		record.setSopcolor(getFormFieldValue(req, res, "tfSopcolor"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLICsidealerscoretableviewRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LICsidealerscoretableviewRecord loadJSONFormLICsidealerscoretableviewRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLICsidealerscoretableviewRecord", null);
		LICsidealerscoretableviewRecord record = new LICsidealerscoretableviewRecord();
		record.setRegion(getFormFieldValue(req, res, "region"));
		record.setDealer(getFormFieldValue(req, res, "dealer"));
		record.setCsi(getFormFieldValue(req, res, "csi"));
		record.setCsicolor(getFormFieldValue(req, res, "csicolor"));
		record.setServiceadvisor(getFormFieldValue(req, res, "serviceadvisor"));
		record.setServiceadvisorcolor(getFormFieldValue(req, res, "serviceadvisorcolor"));
		record.setServicefacility(getFormFieldValue(req, res, "servicefacility"));
		record.setServicefacilitycolor(getFormFieldValue(req, res, "servicefacilitycolor"));
		record.setServiceinitiation(getFormFieldValue(req, res, "serviceinitiation"));
		record.setServiceinitiationcolor(getFormFieldValue(req, res, "serviceinitiationcolor"));
		record.setServicequality(getFormFieldValue(req, res, "servicequality"));
		record.setServicequalitycolor(getFormFieldValue(req, res, "servicequalitycolor"));
		record.setVehiclepickup(getFormFieldValue(req, res, "vehiclepickup"));
		record.setVehiclepickupcolor(getFormFieldValue(req, res, "vehiclepickupcolor"));
		record.setSop(getFormFieldValue(req, res, "sop"));
		record.setSopcolor(getFormFieldValue(req, res, "sopcolor"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLICsidealerscoretableviewRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LICsidealerscoretableviewRecord loadJSONFormLICsidealerscoretableviewRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLICsidealerscoretableviewRecord", null);
		LICsidealerscoretableviewRecord record = new LICsidealerscoretableviewRecord();
		record.setRegion(getFormFieldValueEncode(req, res, "region"));
		record.setDealer(getFormFieldValueEncode(req, res, "dealer"));
		record.setCsi(getFormFieldValueEncode(req, res, "csi"));
		record.setCsicolor(getFormFieldValueEncode(req, res, "csicolor"));
		record.setServiceadvisor(getFormFieldValueEncode(req, res, "serviceadvisor"));
		record.setServiceadvisorcolor(getFormFieldValueEncode(req, res, "serviceadvisorcolor"));
		record.setServicefacility(getFormFieldValueEncode(req, res, "servicefacility"));
		record.setServicefacilitycolor(getFormFieldValueEncode(req, res, "servicefacilitycolor"));
		record.setServiceinitiation(getFormFieldValueEncode(req, res, "serviceinitiation"));
		record.setServiceinitiationcolor(getFormFieldValueEncode(req, res, "serviceinitiationcolor"));
		record.setServicequality(getFormFieldValueEncode(req, res, "servicequality"));
		record.setServicequalitycolor(getFormFieldValueEncode(req, res, "servicequalitycolor"));
		record.setVehiclepickup(getFormFieldValueEncode(req, res, "vehiclepickup"));
		record.setVehiclepickupcolor(getFormFieldValueEncode(req, res, "vehiclepickupcolor"));
		record.setSop(getFormFieldValueEncode(req, res, "sop"));
		record.setSopcolor(getFormFieldValueEncode(req, res, "sopcolor"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLICsidealerscoretableviewRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LICsidealerscoretableviewRecord loadMapLICsidealerscoretableviewRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLICsidealerscoretableviewRecord", null);
		LICsidealerscoretableviewRecord record = new LICsidealerscoretableviewRecord();
		record.setRegion(getMapValue(inputMap, "region"));
		record.setDealer(getMapValue(inputMap, "dealer"));
		record.setCsi(getMapValue(inputMap, "csi"));
		record.setCsicolor(getMapValue(inputMap, "csicolor"));
		record.setServiceadvisor(getMapValue(inputMap, "serviceadvisor"));
		record.setServiceadvisorcolor(getMapValue(inputMap, "serviceadvisorcolor"));
		record.setServicefacility(getMapValue(inputMap, "servicefacility"));
		record.setServicefacilitycolor(getMapValue(inputMap, "servicefacilitycolor"));
		record.setServiceinitiation(getMapValue(inputMap, "serviceinitiation"));
		record.setServiceinitiationcolor(getMapValue(inputMap, "serviceinitiationcolor"));
		record.setServicequality(getMapValue(inputMap, "servicequality"));
		record.setServicequalitycolor(getMapValue(inputMap, "servicequalitycolor"));
		record.setVehiclepickup(getMapValue(inputMap, "vehiclepickup"));
		record.setVehiclepickupcolor(getMapValue(inputMap, "vehiclepickupcolor"));
		record.setSop(getMapValue(inputMap, "sop"));
		record.setSopcolor(getMapValue(inputMap, "sopcolor"));
		logger.trace("loadMapLICsidealerscoretableviewRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}
}
