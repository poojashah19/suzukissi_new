
/*
 * LIOtherstableviewController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.controller;
import com.leadics.suzukithdashboard.common.LIController;
import com.leadics.suzukithdashboard.to.LIOtherstableviewRecord;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LIOtherstableviewController extends LIController
{
	static LogUtils logger = new LogUtils(LIOtherstableviewController.class.getName());

	public LIOtherstableviewRecord loadFormLIOtherstableviewRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLIOtherstableviewRecord", null);
		LIOtherstableviewRecord record = new LIOtherstableviewRecord();
		record.setAttributename(getFormFieldValue(req, res, "tfAttributename"));
		record.setDealeraverage(getFormFieldValue(req, res, "tfDealeraverage"));
		record.setRegionaverage(getFormFieldValue(req, res, "tfRegionaverage"));
		record.setRegionbest(getFormFieldValue(req, res, "tfRegionbest"));
		record.setStudybest(getFormFieldValue(req, res, "tfStudybest"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLIOtherstableviewRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIOtherstableviewRecord loadJSONFormLIOtherstableviewRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIOtherstableviewRecord", null);
		LIOtherstableviewRecord record = new LIOtherstableviewRecord();
		record.setAttributename(getFormFieldValue(req, res, "attributename"));
		record.setDealeraverage(getFormFieldValue(req, res, "dealeraverage"));
		record.setRegionaverage(getFormFieldValue(req, res, "regionaverage"));
		record.setRegionbest(getFormFieldValue(req, res, "regionbest"));
		record.setStudybest(getFormFieldValue(req, res, "studybest"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIOtherstableviewRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIOtherstableviewRecord loadJSONFormLIOtherstableviewRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIOtherstableviewRecord", null);
		LIOtherstableviewRecord record = new LIOtherstableviewRecord();
		record.setAttributename(getFormFieldValueEncode(req, res, "attributename"));
		record.setDealeraverage(getFormFieldValueEncode(req, res, "dealeraverage"));
		record.setRegionaverage(getFormFieldValueEncode(req, res, "regionaverage"));
		record.setRegionbest(getFormFieldValueEncode(req, res, "regionbest"));
		record.setStudybest(getFormFieldValueEncode(req, res, "studybest"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIOtherstableviewRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIOtherstableviewRecord loadMapLIOtherstableviewRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLIOtherstableviewRecord", null);
		LIOtherstableviewRecord record = new LIOtherstableviewRecord();
		record.setAttributename(getMapValue(inputMap, "attributename"));
		record.setDealeraverage(getMapValue(inputMap, "dealeraverage"));
		record.setRegionaverage(getMapValue(inputMap, "regionaverage"));
		record.setRegionbest(getMapValue(inputMap, "regionbest"));
		record.setStudybest(getMapValue(inputMap, "studybest"));
		logger.trace("loadMapLIOtherstableviewRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}
}
