
/*
 * LIRegionsettingController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.controller;
import com.leadics.suzukithdashboard.common.LIController;
import com.leadics.suzukithdashboard.service.LIRegionsettingService;
import com.leadics.suzukithdashboard.to.LIRegionsettingRecord;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LIRegionsettingController extends LIController
{
	static LogUtils logger = new LogUtils(LIRegionsettingController.class.getName());

	public LIRegionsettingRecord loadFormLIRegionsettingRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLIRegionsettingRecord", null);
		LIRegionsettingRecord record = new LIRegionsettingRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setRegion(getFormFieldValue(req, res, "tfRegion"));
		record.setCity(getFormFieldValue(req, res, "tfCity"));
		record.setCode(getFormFieldValue(req, res, "tfCode"));
		record.setColor(getFormFieldValue(req, res, "tfColor"));
		record.setRstatus(getFormFieldValue(req, res, "tfRstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setInstitutionid(getFormFieldValue(req, res, "tfInstitutionid"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLIRegionsettingRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIRegionsettingRecord loadJSONFormLIRegionsettingRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIRegionsettingRecord", null);
		LIRegionsettingRecord record = new LIRegionsettingRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setRegion(getFormFieldValue(req, res, "region"));
		record.setCity(getFormFieldValue(req, res, "city"));
		record.setCode(getFormFieldValue(req, res, "code"));
		record.setColor(getFormFieldValue(req, res, "color"));
		record.setRstatus(getFormFieldValue(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setInstitutionid(getFormFieldValue(req, res, "institution_id"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIRegionsettingRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIRegionsettingRecord loadJSONFormLIRegionsettingRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIRegionsettingRecord", null);
		LIRegionsettingRecord record = new LIRegionsettingRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setRegion(getFormFieldValueEncode(req, res, "region"));
		record.setCity(getFormFieldValueEncode(req, res, "city"));
		record.setCode(getFormFieldValueEncode(req, res, "code"));
		record.setColor(getFormFieldValueEncode(req, res, "color"));
		record.setRstatus(getFormFieldValueEncode(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setInstitutionid(getFormFieldValueEncode(req, res, "institution_id"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIRegionsettingRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIRegionsettingRecord loadMapLIRegionsettingRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLIRegionsettingRecord", null);
		LIRegionsettingRecord record = new LIRegionsettingRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setRegion(getMapValue(inputMap, "region"));
		record.setCity(getMapValue(inputMap, "city"));
		record.setCode(getMapValue(inputMap, "code"));
		record.setColor(getMapValue(inputMap, "color"));
		record.setRstatus(getMapValue(inputMap, "rstatus"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		record.setInstitutionid(getMapValue(inputMap, "institution_id"));
		logger.trace("loadMapLIRegionsettingRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLIRegionsettingRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIRegionsettingRecord", null);
		LIRegionsettingService service = new LIRegionsettingService();

		try
		{
			LIRegionsettingRecord record = loadFormLIRegionsettingRecord(req, res);
			record.setCreatedby(getLoggedInUserId(req,res));
			int resultId = service.insertLIRegionsettingRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Record Creation Successful -" + resultId);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("regionsetting.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create Regionsetting");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create Regionsetting " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("regionsetting.jsp");
		}
	}

	public void processUpdateLIRegionsettingRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIRegionsettingRecord", null);
		LIRegionsettingService service = new LIRegionsettingService();

		try
		{
			LIRegionsettingRecord record = loadFormLIRegionsettingRecord(req, res);
			record.setModifiedby(getLoggedInUserId(req,res));
			service.updateLIRegionsettingRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "Regionsetting data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("regionsetting.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update Regionsetting");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update Regionsetting " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("regionsetting.jsp");
		}
	}

	public void processDeleteLIRegionsettingRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIRegionsettingRecord", null);
		LIRegionsettingService service = new LIRegionsettingService();

		try
		{
			LIRegionsettingRecord record = loadFormLIRegionsettingRecord(req, res);
			service.deleteLIRegionsettingRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Regionsetting deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("regionsetting.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete Regionsetting");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete Regionsetting " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("regionsetting.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertRegionsettingRecord"))
		{
			processInsertLIRegionsettingRecord(req, res);
			return;
		}

		if (actionType.equals("UpdateRegionsettingRecord"))
		{
			processUpdateLIRegionsettingRecord(req, res);
			return;
		}

		if (actionType.equals("DeleteRegionsettingRecord"))
		{
			processDeleteLIRegionsettingRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}
