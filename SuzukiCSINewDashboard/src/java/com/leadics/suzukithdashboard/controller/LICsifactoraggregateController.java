
/*
 * LICsifactoraggregateController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.controller;
import com.leadics.suzukithdashboard.common.LIController;
import com.leadics.suzukithdashboard.service.LICsifactoraggregateService;
import com.leadics.suzukithdashboard.to.LICsifactoraggregateRecord;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LICsifactoraggregateController extends LIController
{
	static LogUtils logger = new LogUtils(LICsifactoraggregateController.class.getName());

	public LICsifactoraggregateRecord loadFormLICsifactoraggregateRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLICsifactoraggregateRecord", null);
		LICsifactoraggregateRecord record = new LICsifactoraggregateRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setFactor(getFormFieldValue(req, res, "tfFactor"));
		record.setFactorname(getFormFieldValue(req, res, "tfFactorname"));
		record.setFactorindex(getFormFieldValue(req, res, "tfFactorindex"));
		record.setRegion(getFormFieldValue(req, res, "tfRegion"));
		record.setState(getFormFieldValue(req, res, "tfState"));
		record.setCity(getFormFieldValue(req, res, "tfCity"));
		record.setDealer(getFormFieldValue(req, res, "tfDealer"));
		record.setModel(getFormFieldValue(req, res, "tfModel"));
		record.setBiweekly(getFormFieldValue(req, res, "tfBiweekly"));
		record.setMonth(getFormFieldValue(req, res, "tfMonth"));
		record.setQuarter(getFormFieldValue(req, res, "tfQuarter"));
		record.setDealerfactorsum(getFormFieldValue(req, res, "tfDealerfactorsum"));
		record.setDealerfactorcount(getFormFieldValue(req, res, "tfDealerfactorcount"));
		record.setTotalcount(getFormFieldValue(req, res, "tfTotalcount"));
		record.setRstatus(getFormFieldValue(req, res, "tfRstatus"));
		record.setLastacc(getFormFieldValue(req, res, "tfLastacc"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setClosetype(getFormFieldValue(req, res, "tfClosetype"));
		record.setClosedat(getFormFieldValue(req, res, "tfClosedat"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLICsifactoraggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LICsifactoraggregateRecord loadJSONFormLICsifactoraggregateRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLICsifactoraggregateRecord", null);
		LICsifactoraggregateRecord record = new LICsifactoraggregateRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setFactor(getFormFieldValue(req, res, "factor"));
		record.setFactorname(getFormFieldValue(req, res, "factorname"));
		record.setFactorindex(getFormFieldValue(req, res, "factorindex"));
		record.setRegion(getFormFieldValue(req, res, "region"));
		record.setState(getFormFieldValue(req, res, "state"));
		record.setCity(getFormFieldValue(req, res, "city"));
		record.setDealer(getFormFieldValue(req, res, "dealer"));
		record.setModel(getFormFieldValue(req, res, "model"));
		record.setBiweekly(getFormFieldValue(req, res, "bi_weekly"));
		record.setMonth(getFormFieldValue(req, res, "month"));
		record.setQuarter(getFormFieldValue(req, res, "quarter"));
		record.setDealerfactorsum(getFormFieldValue(req, res, "dealer_factor_sum"));
		record.setDealerfactorcount(getFormFieldValue(req, res, "dealer_factor_count"));
		record.setTotalcount(getFormFieldValue(req, res, "totalcount"));
		record.setRstatus(getFormFieldValue(req, res, "rstatus"));
		record.setLastacc(getFormFieldValue(req, res, "lastacc"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setClosetype(getFormFieldValue(req, res, "close_type"));
		record.setClosedat(getFormFieldValue(req, res, "closed_at"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLICsifactoraggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LICsifactoraggregateRecord loadJSONFormLICsifactoraggregateRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLICsifactoraggregateRecord", null);
		LICsifactoraggregateRecord record = new LICsifactoraggregateRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setFactor(getFormFieldValueEncode(req, res, "factor"));
		record.setFactorname(getFormFieldValueEncode(req, res, "factorname"));
		record.setFactorindex(getFormFieldValueEncode(req, res, "factorindex"));
		record.setRegion(getFormFieldValueEncode(req, res, "region"));
		record.setState(getFormFieldValueEncode(req, res, "state"));
		record.setCity(getFormFieldValueEncode(req, res, "city"));
		record.setDealer(getFormFieldValueEncode(req, res, "dealer"));
		record.setModel(getFormFieldValueEncode(req, res, "model"));
		record.setBiweekly(getFormFieldValueEncode(req, res, "bi_weekly"));
		record.setMonth(getFormFieldValueEncode(req, res, "month"));
		record.setQuarter(getFormFieldValueEncode(req, res, "quarter"));
		record.setDealerfactorsum(getFormFieldValueEncode(req, res, "dealer_factor_sum"));
		record.setDealerfactorcount(getFormFieldValueEncode(req, res, "dealer_factor_count"));
		record.setTotalcount(getFormFieldValueEncode(req, res, "totalcount"));
		record.setRstatus(getFormFieldValueEncode(req, res, "rstatus"));
		record.setLastacc(getFormFieldValueEncode(req, res, "lastacc"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setClosetype(getFormFieldValueEncode(req, res, "close_type"));
		record.setClosedat(getFormFieldValueEncode(req, res, "closed_at"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLICsifactoraggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LICsifactoraggregateRecord loadMapLICsifactoraggregateRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLICsifactoraggregateRecord", null);
		LICsifactoraggregateRecord record = new LICsifactoraggregateRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setFactor(getMapValue(inputMap, "factor"));
		record.setFactorname(getMapValue(inputMap, "factorname"));
		record.setFactorindex(getMapValue(inputMap, "factorindex"));
		record.setRegion(getMapValue(inputMap, "region"));
		record.setState(getMapValue(inputMap, "state"));
		record.setCity(getMapValue(inputMap, "city"));
		record.setDealer(getMapValue(inputMap, "dealer"));
		record.setModel(getMapValue(inputMap, "model"));
		record.setBiweekly(getMapValue(inputMap, "bi_weekly"));
		record.setMonth(getMapValue(inputMap, "month"));
		record.setQuarter(getMapValue(inputMap, "quarter"));
		record.setDealerfactorsum(getMapValue(inputMap, "dealer_factor_sum"));
		record.setDealerfactorcount(getMapValue(inputMap, "dealer_factor_count"));
		record.setTotalcount(getMapValue(inputMap, "totalcount"));
		record.setRstatus(getMapValue(inputMap, "rstatus"));
		record.setLastacc(getMapValue(inputMap, "lastacc"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		record.setClosetype(getMapValue(inputMap, "close_type"));
		record.setClosedat(getMapValue(inputMap, "closed_at"));
		logger.trace("loadMapLICsifactoraggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLICsifactoraggregateRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLICsifactoraggregateRecord", null);
		LICsifactoraggregateService service = new LICsifactoraggregateService();

		try
		{
			LICsifactoraggregateRecord record = loadFormLICsifactoraggregateRecord(req, res);
			record.setCreatedby(getLoggedInUserId(req,res));
			int resultId = service.insertLICsifactoraggregateRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Record Creation Successful -" + resultId);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csifactoraggregate.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create Csifactoraggregate");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create Csifactoraggregate " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csifactoraggregate.jsp");
		}
	}

	public void processUpdateLICsifactoraggregateRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLICsifactoraggregateRecord", null);
		LICsifactoraggregateService service = new LICsifactoraggregateService();

		try
		{
			LICsifactoraggregateRecord record = loadFormLICsifactoraggregateRecord(req, res);
			record.setModifiedby(getLoggedInUserId(req,res));
			service.updateLICsifactoraggregateRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "Csifactoraggregate data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csifactoraggregate.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update Csifactoraggregate");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update Csifactoraggregate " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csifactoraggregate.jsp");
		}
	}

	public void processDeleteLICsifactoraggregateRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLICsifactoraggregateRecord", null);
		LICsifactoraggregateService service = new LICsifactoraggregateService();

		try
		{
			LICsifactoraggregateRecord record = loadFormLICsifactoraggregateRecord(req, res);
			service.deleteLICsifactoraggregateRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Csifactoraggregate deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csifactoraggregate.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete Csifactoraggregate");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete Csifactoraggregate " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csifactoraggregate.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertCsifactoraggregateRecord"))
		{
			processInsertLICsifactoraggregateRecord(req, res);
			return;
		}

		if (actionType.equals("UpdateCsifactoraggregateRecord"))
		{
			processUpdateLICsifactoraggregateRecord(req, res);
			return;
		}

		if (actionType.equals("DeleteCsifactoraggregateRecord"))
		{
			processDeleteLICsifactoraggregateRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}
