
/*
 * LISsifactoraggregateController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.controller;
import com.leadics.suzukithdashboard.common.LIController;
import com.leadics.suzukithdashboard.service.LISsifactoraggregateService;
import com.leadics.suzukithdashboard.to.LISsifactoraggregateRecord;
import javax.servlet.http.*;
import java.util.*;
import com.leadics.utils.*;
public class LISsifactoraggregateController extends LIController
{
	static LogUtils logger = new LogUtils(LISsifactoraggregateController.class.getName());

	public LISsifactoraggregateRecord loadFormLISsifactoraggregateRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLISsifactoraggregateRecord", null);
		LISsifactoraggregateRecord record = new LISsifactoraggregateRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setFactor(getFormFieldValue(req, res, "tfFactor"));
		record.setFactorname(getFormFieldValue(req, res, "tfFactorname"));
		record.setFactorindex(getFormFieldValue(req, res, "tfFactorindex"));
		record.setRegion(getFormFieldValue(req, res, "tfRegion"));
		record.setState(getFormFieldValue(req, res, "tfState"));
		record.setCity(getFormFieldValue(req, res, "tfCity"));
		record.setDealer(getFormFieldValue(req, res, "tfDealer"));
		record.setModel(getFormFieldValue(req, res, "tfModel"));
		record.setBiweekly(getFormFieldValue(req, res, "tfBiweekly"));
		record.setMonth(getFormFieldValue(req, res, "tfMonth"));
		record.setQuarter(getFormFieldValue(req, res, "tfQuarter"));
		record.setDealerfactorsum(getFormFieldValue(req, res, "tfDealerfactorsum"));
		record.setDealerfactorcount(getFormFieldValue(req, res, "tfDealerfactorcount"));
		record.setTotalcount(getFormFieldValue(req, res, "tfTotalcount"));
		record.setRstatus(getFormFieldValue(req, res, "tfRstatus"));
		record.setLastacc(getFormFieldValue(req, res, "tfLastacc"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setClosetype(getFormFieldValue(req, res, "tfClosetype"));
		record.setClosedat(getFormFieldValue(req, res, "tfClosedat"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLISsifactoraggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LISsifactoraggregateRecord loadJSONFormLISsifactoraggregateRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLISsifactoraggregateRecord", null);
		LISsifactoraggregateRecord record = new LISsifactoraggregateRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setFactor(getFormFieldValue(req, res, "factor"));
		record.setFactorname(getFormFieldValue(req, res, "factorname"));
		record.setFactorindex(getFormFieldValue(req, res, "factorindex"));
		record.setRegion(getFormFieldValue(req, res, "region"));
		record.setState(getFormFieldValue(req, res, "state"));
		record.setCity(getFormFieldValue(req, res, "city"));
		record.setDealer(getFormFieldValue(req, res, "dealer"));
		record.setModel(getFormFieldValue(req, res, "model"));
		record.setBiweekly(getFormFieldValue(req, res, "bi_weekly"));
		record.setMonth(getFormFieldValue(req, res, "month"));
		record.setQuarter(getFormFieldValue(req, res, "quarter"));
		record.setDealerfactorsum(getFormFieldValue(req, res, "dealer_factor_sum"));
		record.setDealerfactorcount(getFormFieldValue(req, res, "dealer_factor_count"));
		record.setTotalcount(getFormFieldValue(req, res, "totalcount"));
		record.setRstatus(getFormFieldValue(req, res, "rstatus"));
		record.setLastacc(getFormFieldValue(req, res, "lastacc"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setClosetype(getFormFieldValue(req, res, "close_type"));
		record.setClosedat(getFormFieldValue(req, res, "closed_at"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLISsifactoraggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LISsifactoraggregateRecord loadJSONFormLISsifactoraggregateRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLISsifactoraggregateRecord", null);
		LISsifactoraggregateRecord record = new LISsifactoraggregateRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setFactor(getFormFieldValueEncode(req, res, "factor"));
		record.setFactorname(getFormFieldValueEncode(req, res, "factorname"));
		record.setFactorindex(getFormFieldValueEncode(req, res, "factorindex"));
		record.setRegion(getFormFieldValueEncode(req, res, "region"));
		record.setState(getFormFieldValueEncode(req, res, "state"));
		record.setCity(getFormFieldValueEncode(req, res, "city"));
		record.setDealer(getFormFieldValueEncode(req, res, "dealer"));
		record.setModel(getFormFieldValueEncode(req, res, "model"));
		record.setBiweekly(getFormFieldValueEncode(req, res, "bi_weekly"));
		record.setMonth(getFormFieldValueEncode(req, res, "month"));
		record.setQuarter(getFormFieldValueEncode(req, res, "quarter"));
		record.setDealerfactorsum(getFormFieldValueEncode(req, res, "dealer_factor_sum"));
		record.setDealerfactorcount(getFormFieldValueEncode(req, res, "dealer_factor_count"));
		record.setTotalcount(getFormFieldValueEncode(req, res, "totalcount"));
		record.setRstatus(getFormFieldValueEncode(req, res, "rstatus"));
		record.setLastacc(getFormFieldValueEncode(req, res, "lastacc"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setClosetype(getFormFieldValueEncode(req, res, "close_type"));
		record.setClosedat(getFormFieldValueEncode(req, res, "closed_at"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLISsifactoraggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LISsifactoraggregateRecord loadMapLISsifactoraggregateRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLISsifactoraggregateRecord", null);
		LISsifactoraggregateRecord record = new LISsifactoraggregateRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setFactor(getMapValue(inputMap, "factor"));
		record.setFactorname(getMapValue(inputMap, "factorname"));
		record.setFactorindex(getMapValue(inputMap, "factorindex"));
		record.setRegion(getMapValue(inputMap, "region"));
		record.setState(getMapValue(inputMap, "state"));
		record.setCity(getMapValue(inputMap, "city"));
		record.setDealer(getMapValue(inputMap, "dealer"));
		record.setModel(getMapValue(inputMap, "model"));
		record.setBiweekly(getMapValue(inputMap, "bi_weekly"));
		record.setMonth(getMapValue(inputMap, "month"));
		record.setQuarter(getMapValue(inputMap, "quarter"));
		record.setDealerfactorsum(getMapValue(inputMap, "dealer_factor_sum"));
		record.setDealerfactorcount(getMapValue(inputMap, "dealer_factor_count"));
		record.setTotalcount(getMapValue(inputMap, "totalcount"));
		record.setRstatus(getMapValue(inputMap, "rstatus"));
		record.setLastacc(getMapValue(inputMap, "lastacc"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		record.setClosetype(getMapValue(inputMap, "close_type"));
		record.setClosedat(getMapValue(inputMap, "closed_at"));
		logger.trace("loadMapLISsifactoraggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLISsifactoraggregateRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLISsifactoraggregateRecord", null);
		LISsifactoraggregateService service = new LISsifactoraggregateService();

		try
		{
			LISsifactoraggregateRecord record = loadFormLISsifactoraggregateRecord(req, res);
			record.setCreatedby(getLoggedInUserId(req,res));
			int resultId = service.insertLISsifactoraggregateRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Record Creation Successful -" + resultId);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("ssifactoraggregate.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create Ssifactoraggregate");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create Ssifactoraggregate " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("ssifactoraggregate.jsp");
		}
	}

	public void processUpdateLISsifactoraggregateRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLISsifactoraggregateRecord", null);
		LISsifactoraggregateService service = new LISsifactoraggregateService();

		try
		{
			LISsifactoraggregateRecord record = loadFormLISsifactoraggregateRecord(req, res);
			record.setModifiedby(getLoggedInUserId(req,res));
			service.updateLISsifactoraggregateRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "Ssifactoraggregate data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("ssifactoraggregate.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update Ssifactoraggregate");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update Ssifactoraggregate " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("ssifactoraggregate.jsp");
		}
	}

	public void processDeleteLISsifactoraggregateRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLISsifactoraggregateRecord", null);
		LISsifactoraggregateService service = new LISsifactoraggregateService();

		try
		{
			LISsifactoraggregateRecord record = loadFormLISsifactoraggregateRecord(req, res);
			service.deleteLISsifactoraggregateRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Ssifactoraggregate deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("ssifactoraggregate.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete Ssifactoraggregate");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete Ssifactoraggregate " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("ssifactoraggregate.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertSsifactoraggregateRecord"))
		{
			processInsertLISsifactoraggregateRecord(req, res);
			return;
		}

		if (actionType.equals("UpdateSsifactoraggregateRecord"))
		{
			processUpdateLISsifactoraggregateRecord(req, res);
			return;
		}

		if (actionType.equals("DeleteSsifactoraggregateRecord"))
		{
			processDeleteLISsifactoraggregateRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}
