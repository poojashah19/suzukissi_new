
/*
 * LICsimeansopaggregateController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.controller;
import com.leadics.suzukithdashboard.common.LIController;
import com.leadics.suzukithdashboard.service.LICsimeansopaggregateService;
import com.leadics.suzukithdashboard.to.LICsimeansopaggregateRecord;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LICsimeansopaggregateController extends LIController
{
	static LogUtils logger = new LogUtils(LICsimeansopaggregateController.class.getName());

	public LICsimeansopaggregateRecord loadFormLICsimeansopaggregateRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLICsimeansopaggregateRecord", null);
		LICsimeansopaggregateRecord record = new LICsimeansopaggregateRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setSop(getFormFieldValue(req, res, "tfSop"));
		record.setRegion(getFormFieldValue(req, res, "tfRegion"));
		record.setState(getFormFieldValue(req, res, "tfState"));
		record.setCity(getFormFieldValue(req, res, "tfCity"));
		record.setDealer(getFormFieldValue(req, res, "tfDealer"));
		record.setModel(getFormFieldValue(req, res, "tfModel"));
		record.setBiweekly(getFormFieldValue(req, res, "tfBiweekly"));
		record.setMonth(getFormFieldValue(req, res, "tfMonth"));
		record.setQuarter(getFormFieldValue(req, res, "tfQuarter"));
		record.setDealersopsum(getFormFieldValue(req, res, "tfDealersopsum"));
		record.setDealersopcount(getFormFieldValue(req, res, "tfDealersopcount"));
		record.setRstatus(getFormFieldValue(req, res, "tfRstatus"));
		record.setLastacc(getFormFieldValue(req, res, "tfLastacc"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setClosetype(getFormFieldValue(req, res, "tfClosetype"));
		record.setClosedat(getFormFieldValue(req, res, "tfClosedat"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLICsimeansopaggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LICsimeansopaggregateRecord loadJSONFormLICsimeansopaggregateRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLICsimeansopaggregateRecord", null);
		LICsimeansopaggregateRecord record = new LICsimeansopaggregateRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setSop(getFormFieldValue(req, res, "sop"));
		record.setRegion(getFormFieldValue(req, res, "region"));
		record.setState(getFormFieldValue(req, res, "state"));
		record.setCity(getFormFieldValue(req, res, "city"));
		record.setDealer(getFormFieldValue(req, res, "dealer"));
		record.setModel(getFormFieldValue(req, res, "model"));
		record.setBiweekly(getFormFieldValue(req, res, "bi_weekly"));
		record.setMonth(getFormFieldValue(req, res, "month"));
		record.setQuarter(getFormFieldValue(req, res, "quarter"));
		record.setDealersopsum(getFormFieldValue(req, res, "dealer_sop_sum"));
		record.setDealersopcount(getFormFieldValue(req, res, "dealer_sop_count"));
		record.setRstatus(getFormFieldValue(req, res, "rstatus"));
		record.setLastacc(getFormFieldValue(req, res, "lastacc"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setClosetype(getFormFieldValue(req, res, "close_type"));
		record.setClosedat(getFormFieldValue(req, res, "closed_at"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLICsimeansopaggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LICsimeansopaggregateRecord loadJSONFormLICsimeansopaggregateRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLICsimeansopaggregateRecord", null);
		LICsimeansopaggregateRecord record = new LICsimeansopaggregateRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setSop(getFormFieldValueEncode(req, res, "sop"));
		record.setRegion(getFormFieldValueEncode(req, res, "region"));
		record.setState(getFormFieldValueEncode(req, res, "state"));
		record.setCity(getFormFieldValueEncode(req, res, "city"));
		record.setDealer(getFormFieldValueEncode(req, res, "dealer"));
		record.setModel(getFormFieldValueEncode(req, res, "model"));
		record.setBiweekly(getFormFieldValueEncode(req, res, "bi_weekly"));
		record.setMonth(getFormFieldValueEncode(req, res, "month"));
		record.setQuarter(getFormFieldValueEncode(req, res, "quarter"));
		record.setDealersopsum(getFormFieldValueEncode(req, res, "dealer_sop_sum"));
		record.setDealersopcount(getFormFieldValueEncode(req, res, "dealer_sop_count"));
		record.setRstatus(getFormFieldValueEncode(req, res, "rstatus"));
		record.setLastacc(getFormFieldValueEncode(req, res, "lastacc"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setClosetype(getFormFieldValueEncode(req, res, "close_type"));
		record.setClosedat(getFormFieldValueEncode(req, res, "closed_at"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLICsimeansopaggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LICsimeansopaggregateRecord loadMapLICsimeansopaggregateRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLICsimeansopaggregateRecord", null);
		LICsimeansopaggregateRecord record = new LICsimeansopaggregateRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setSop(getMapValue(inputMap, "sop"));
		record.setRegion(getMapValue(inputMap, "region"));
		record.setState(getMapValue(inputMap, "state"));
		record.setCity(getMapValue(inputMap, "city"));
		record.setDealer(getMapValue(inputMap, "dealer"));
		record.setModel(getMapValue(inputMap, "model"));
		record.setBiweekly(getMapValue(inputMap, "bi_weekly"));
		record.setMonth(getMapValue(inputMap, "month"));
		record.setQuarter(getMapValue(inputMap, "quarter"));
		record.setDealersopsum(getMapValue(inputMap, "dealer_sop_sum"));
		record.setDealersopcount(getMapValue(inputMap, "dealer_sop_count"));
		record.setRstatus(getMapValue(inputMap, "rstatus"));
		record.setLastacc(getMapValue(inputMap, "lastacc"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		record.setClosetype(getMapValue(inputMap, "close_type"));
		record.setClosedat(getMapValue(inputMap, "closed_at"));
		logger.trace("loadMapLICsimeansopaggregateRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLICsimeansopaggregateRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLICsimeansopaggregateRecord", null);
		LICsimeansopaggregateService service = new LICsimeansopaggregateService();

		try
		{
			LICsimeansopaggregateRecord record = loadFormLICsimeansopaggregateRecord(req, res);
			record.setCreatedby(getLoggedInUserId(req,res));
			int resultId = service.insertLICsimeansopaggregateRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Record Creation Successful -" + resultId);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csimeansopaggregate.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create Csimeansopaggregate");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create Csimeansopaggregate " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csimeansopaggregate.jsp");
		}
	}

	public void processUpdateLICsimeansopaggregateRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLICsimeansopaggregateRecord", null);
		LICsimeansopaggregateService service = new LICsimeansopaggregateService();

		try
		{
			LICsimeansopaggregateRecord record = loadFormLICsimeansopaggregateRecord(req, res);
			record.setModifiedby(getLoggedInUserId(req,res));
			service.updateLICsimeansopaggregateRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "Csimeansopaggregate data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csimeansopaggregate.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update Csimeansopaggregate");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update Csimeansopaggregate " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csimeansopaggregate.jsp");
		}
	}

	public void processDeleteLICsimeansopaggregateRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLICsimeansopaggregateRecord", null);
		LICsimeansopaggregateService service = new LICsimeansopaggregateService();

		try
		{
			LICsimeansopaggregateRecord record = loadFormLICsimeansopaggregateRecord(req, res);
			service.deleteLICsimeansopaggregateRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Csimeansopaggregate deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csimeansopaggregate.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete Csimeansopaggregate");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete Csimeansopaggregate " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("csimeansopaggregate.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertCsimeansopaggregateRecord"))
		{
			processInsertLICsimeansopaggregateRecord(req, res);
			return;
		}

		if (actionType.equals("UpdateCsimeansopaggregateRecord"))
		{
			processUpdateLICsimeansopaggregateRecord(req, res);
			return;
		}

		if (actionType.equals("DeleteCsimeansopaggregateRecord"))
		{
			processDeleteLICsimeansopaggregateRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}
