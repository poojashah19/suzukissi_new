
/*
 * LISsidealerscoretableviewController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.controller;
import com.leadics.suzukithdashboard.common.LIController;
import com.leadics.suzukithdashboard.to.LISsidealerscoretableviewRecord;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LISsidealerscoretableviewController extends LIController
{
	static LogUtils logger = new LogUtils(LISsidealerscoretableviewController.class.getName());

	public LISsidealerscoretableviewRecord loadFormLISsidealerscoretableviewRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLISsidealerscoretableviewRecord", null);
		LISsidealerscoretableviewRecord record = new LISsidealerscoretableviewRecord();
		record.setRegion(getFormFieldValue(req, res, "tfRegion"));
		record.setDealer(getFormFieldValue(req, res, "tfDealer"));
		record.setSsi(getFormFieldValue(req, res, "tfSsi"));
		record.setSsicolor(getFormFieldValue(req, res, "tfSsicolor"));
		record.setSalesinitiation(getFormFieldValue(req, res, "tfSalesinitiation"));
		record.setSalesinitiationcolor(getFormFieldValue(req, res, "tfSalesinitiationcolor"));
		record.setDealerfacility(getFormFieldValue(req, res, "tfDealerfacility"));
		record.setDealerfacilitycolor(getFormFieldValue(req, res, "tfDealerfacilitycolor"));
		record.setDeal(getFormFieldValue(req, res, "tfDeal"));
		record.setDealcolor(getFormFieldValue(req, res, "tfDealcolor"));
		record.setSalesperson(getFormFieldValue(req, res, "tfSalesperson"));
		record.setSalespersoncolor(getFormFieldValue(req, res, "tfSalespersoncolor"));
		record.setDeliverytiming(getFormFieldValue(req, res, "tfDeliverytiming"));
		record.setDeliverytimingcolor(getFormFieldValue(req, res, "tfDeliverytimingcolor"));
		record.setDeliveryprocess(getFormFieldValue(req, res, "tfDeliveryprocess"));
		record.setDeliveryprocesscolor(getFormFieldValue(req, res, "tfDeliveryprocesscolor"));
		record.setSop(getFormFieldValue(req, res, "tfSop"));
		record.setSopcolor(getFormFieldValue(req, res, "tfSopcolor"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLISsidealerscoretableviewRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LISsidealerscoretableviewRecord loadJSONFormLISsidealerscoretableviewRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLISsidealerscoretableviewRecord", null);
		LISsidealerscoretableviewRecord record = new LISsidealerscoretableviewRecord();
		record.setRegion(getFormFieldValue(req, res, "region"));
		record.setDealer(getFormFieldValue(req, res, "dealer"));
		record.setSsi(getFormFieldValue(req, res, "ssi"));
		record.setSsicolor(getFormFieldValue(req, res, "ssicolor"));
		record.setSalesinitiation(getFormFieldValue(req, res, "salesinitiation"));
		record.setSalesinitiationcolor(getFormFieldValue(req, res, "salesinitiationcolor"));
		record.setDealerfacility(getFormFieldValue(req, res, "dealerfacility"));
		record.setDealerfacilitycolor(getFormFieldValue(req, res, "dealerfacilitycolor"));
		record.setDeal(getFormFieldValue(req, res, "deal"));
		record.setDealcolor(getFormFieldValue(req, res, "dealcolor"));
		record.setSalesperson(getFormFieldValue(req, res, "salesperson"));
		record.setSalespersoncolor(getFormFieldValue(req, res, "salespersoncolor"));
		record.setDeliverytiming(getFormFieldValue(req, res, "deliverytiming"));
		record.setDeliverytimingcolor(getFormFieldValue(req, res, "deliverytimingcolor"));
		record.setDeliveryprocess(getFormFieldValue(req, res, "deliveryprocess"));
		record.setDeliveryprocesscolor(getFormFieldValue(req, res, "deliveryprocesscolor"));
		record.setSop(getFormFieldValue(req, res, "sop"));
		record.setSopcolor(getFormFieldValue(req, res, "sopcolor"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLISsidealerscoretableviewRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LISsidealerscoretableviewRecord loadJSONFormLISsidealerscoretableviewRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLISsidealerscoretableviewRecord", null);
		LISsidealerscoretableviewRecord record = new LISsidealerscoretableviewRecord();
		record.setRegion(getFormFieldValueEncode(req, res, "region"));
		record.setDealer(getFormFieldValueEncode(req, res, "dealer"));
		record.setSsi(getFormFieldValueEncode(req, res, "ssi"));
		record.setSsicolor(getFormFieldValueEncode(req, res, "ssicolor"));
		record.setSalesinitiation(getFormFieldValueEncode(req, res, "salesinitiation"));
		record.setSalesinitiationcolor(getFormFieldValueEncode(req, res, "salesinitiationcolor"));
		record.setDealerfacility(getFormFieldValueEncode(req, res, "dealerfacility"));
		record.setDealerfacilitycolor(getFormFieldValueEncode(req, res, "dealerfacilitycolor"));
		record.setDeal(getFormFieldValueEncode(req, res, "deal"));
		record.setDealcolor(getFormFieldValueEncode(req, res, "dealcolor"));
		record.setSalesperson(getFormFieldValueEncode(req, res, "salesperson"));
		record.setSalespersoncolor(getFormFieldValueEncode(req, res, "salespersoncolor"));
		record.setDeliverytiming(getFormFieldValueEncode(req, res, "deliverytiming"));
		record.setDeliverytimingcolor(getFormFieldValueEncode(req, res, "deliverytimingcolor"));
		record.setDeliveryprocess(getFormFieldValueEncode(req, res, "deliveryprocess"));
		record.setDeliveryprocesscolor(getFormFieldValueEncode(req, res, "deliveryprocesscolor"));
		record.setSop(getFormFieldValueEncode(req, res, "sop"));
		record.setSopcolor(getFormFieldValueEncode(req, res, "sopcolor"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLISsidealerscoretableviewRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LISsidealerscoretableviewRecord loadMapLISsidealerscoretableviewRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLISsidealerscoretableviewRecord", null);
		LISsidealerscoretableviewRecord record = new LISsidealerscoretableviewRecord();
		record.setRegion(getMapValue(inputMap, "region"));
		record.setDealer(getMapValue(inputMap, "dealer"));
		record.setSsi(getMapValue(inputMap, "ssi"));
		record.setSsicolor(getMapValue(inputMap, "ssicolor"));
		record.setSalesinitiation(getMapValue(inputMap, "salesinitiation"));
		record.setSalesinitiationcolor(getMapValue(inputMap, "salesinitiationcolor"));
		record.setDealerfacility(getMapValue(inputMap, "dealerfacility"));
		record.setDealerfacilitycolor(getMapValue(inputMap, "dealerfacilitycolor"));
		record.setDeal(getMapValue(inputMap, "deal"));
		record.setDealcolor(getMapValue(inputMap, "dealcolor"));
		record.setSalesperson(getMapValue(inputMap, "salesperson"));
		record.setSalespersoncolor(getMapValue(inputMap, "salespersoncolor"));
		record.setDeliverytiming(getMapValue(inputMap, "deliverytiming"));
		record.setDeliverytimingcolor(getMapValue(inputMap, "deliverytimingcolor"));
		record.setDeliveryprocess(getMapValue(inputMap, "deliveryprocess"));
		record.setDeliveryprocesscolor(getMapValue(inputMap, "deliveryprocesscolor"));
		record.setSop(getMapValue(inputMap, "sop"));
		record.setSopcolor(getMapValue(inputMap, "sopcolor"));
		logger.trace("loadMapLISsidealerscoretableviewRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}
}
