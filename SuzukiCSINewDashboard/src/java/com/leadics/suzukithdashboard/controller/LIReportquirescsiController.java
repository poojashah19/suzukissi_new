
/*
 * LIReportquirescsiController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.controller;
import com.leadics.suzukithdashboard.common.LIController;
import com.leadics.suzukithdashboard.service.LIReportquirescsiService;
import com.leadics.suzukithdashboard.to.LIReportquirescsiRecord;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LIReportquirescsiController extends LIController
{
	static LogUtils logger = new LogUtils(LIReportquirescsiController.class.getName());

	public LIReportquirescsiRecord loadFormLIReportquirescsiRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLIReportquirescsiRecord", null);
		LIReportquirescsiRecord record = new LIReportquirescsiRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setReporttype(getFormFieldValue(req, res, "tfReporttype"));
		record.setReportname(getFormFieldValue(req, res, "tfReportname"));
		record.setQuery(getFormFieldValue(req, res, "tfQuery"));
		record.setCountquery(getFormFieldValue(req, res, "tfCountquery"));
		record.setJsontemplate(getFormFieldValue(req, res, "tfJsontemplate"));
		record.setRstatus(getFormFieldValue(req, res, "tfRstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setInstitutionid(getFormFieldValue(req, res, "tfInstitutionid"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLIReportquirescsiRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIReportquirescsiRecord loadJSONFormLIReportquirescsiRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIReportquirescsiRecord", null);
		LIReportquirescsiRecord record = new LIReportquirescsiRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setReporttype(getFormFieldValue(req, res, "reporttype"));
		record.setReportname(getFormFieldValue(req, res, "reportname"));
		record.setQuery(getFormFieldValue(req, res, "query"));
		record.setCountquery(getFormFieldValue(req, res, "countquery"));
		record.setJsontemplate(getFormFieldValue(req, res, "jsontemplate"));
		record.setRstatus(getFormFieldValue(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setInstitutionid(getFormFieldValue(req, res, "institution_id"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIReportquirescsiRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIReportquirescsiRecord loadJSONFormLIReportquirescsiRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIReportquirescsiRecord", null);
		LIReportquirescsiRecord record = new LIReportquirescsiRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setReporttype(getFormFieldValueEncode(req, res, "reporttype"));
		record.setReportname(getFormFieldValueEncode(req, res, "reportname"));
		record.setQuery(getFormFieldValueEncode(req, res, "query"));
		record.setCountquery(getFormFieldValueEncode(req, res, "countquery"));
		record.setJsontemplate(getFormFieldValueEncode(req, res, "jsontemplate"));
		record.setRstatus(getFormFieldValueEncode(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setInstitutionid(getFormFieldValueEncode(req, res, "institution_id"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIReportquirescsiRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIReportquirescsiRecord loadMapLIReportquirescsiRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLIReportquirescsiRecord", null);
		LIReportquirescsiRecord record = new LIReportquirescsiRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setReporttype(getMapValue(inputMap, "reporttype"));
		record.setReportname(getMapValue(inputMap, "reportname"));
		record.setQuery(getMapValue(inputMap, "query"));
		record.setCountquery(getMapValue(inputMap, "countquery"));
		record.setJsontemplate(getMapValue(inputMap, "jsontemplate"));
		record.setRstatus(getMapValue(inputMap, "rstatus"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		record.setInstitutionid(getMapValue(inputMap, "institution_id"));
		logger.trace("loadMapLIReportquirescsiRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLIReportquirescsiRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIReportquirescsiRecord", null);
		LIReportquirescsiService service = new LIReportquirescsiService();

		try
		{
			LIReportquirescsiRecord record = loadFormLIReportquirescsiRecord(req, res);
			record.setCreatedby(getLoggedInUserId(req,res));
			int resultId = service.insertLIReportquirescsiRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Record Creation Successful -" + resultId);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("reportquirescsi.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create Reportquirescsi");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create Reportquirescsi " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("reportquirescsi.jsp");
		}
	}

	public void processUpdateLIReportquirescsiRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIReportquirescsiRecord", null);
		LIReportquirescsiService service = new LIReportquirescsiService();

		try
		{
			LIReportquirescsiRecord record = loadFormLIReportquirescsiRecord(req, res);
			record.setModifiedby(getLoggedInUserId(req,res));
			service.updateLIReportquirescsiRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "Reportquirescsi data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("reportquirescsi.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update Reportquirescsi");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update Reportquirescsi " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("reportquirescsi.jsp");
		}
	}

	public void processDeleteLIReportquirescsiRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIReportquirescsiRecord", null);
		LIReportquirescsiService service = new LIReportquirescsiService();

		try
		{
			LIReportquirescsiRecord record = loadFormLIReportquirescsiRecord(req, res);
			service.deleteLIReportquirescsiRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Reportquirescsi deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("reportquirescsi.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete Reportquirescsi");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete Reportquirescsi " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("reportquirescsi.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertReportquirescsiRecord"))
		{
			processInsertLIReportquirescsiRecord(req, res);
			return;
		}

		if (actionType.equals("UpdateReportquirescsiRecord"))
		{
			processUpdateLIReportquirescsiRecord(req, res);
			return;
		}

		if (actionType.equals("DeleteReportquirescsiRecord"))
		{
			processDeleteLIReportquirescsiRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}
