
/*
 * LIStatuscodeController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.controller;
import com.leadics.suzukithdashboard.common.LIController;
import com.leadics.suzukithdashboard.service.LIStatuscodeService;
import com.leadics.suzukithdashboard.to.LIStatuscodeRecord;
import javax.servlet.http.*;
import java.util.*;
import com.leadics.utils.*;
public class LIStatuscodeController extends LIController
{
	static LogUtils logger = new LogUtils(LIStatuscodeController.class.getName());

	public LIStatuscodeRecord loadFormLIStatuscodeRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLIStatuscodeRecord", null);
		LIStatuscodeRecord record = new LIStatuscodeRecord();
		record.setStatusid(getFormFieldValue(req, res, "tfStatusid"));
		record.setStatusname(getFormFieldValue(req, res, "tfStatusname"));
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setRstatus(getFormFieldValue(req, res, "tfRstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setInstitutionid(getFormFieldValue(req, res, "tfInstitutionid"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLIStatuscodeRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIStatuscodeRecord loadJSONFormLIStatuscodeRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIStatuscodeRecord", null);
		LIStatuscodeRecord record = new LIStatuscodeRecord();
		record.setStatusid(getFormFieldValue(req, res, "status_id"));
		record.setStatusname(getFormFieldValue(req, res, "status_name"));
		record.setId(getFormFieldValue(req, res, "id"));
		record.setRstatus(getFormFieldValue(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setInstitutionid(getFormFieldValue(req, res, "institution_id"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIStatuscodeRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIStatuscodeRecord loadJSONFormLIStatuscodeRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIStatuscodeRecord", null);
		LIStatuscodeRecord record = new LIStatuscodeRecord();
		record.setStatusid(getFormFieldValueEncode(req, res, "status_id"));
		record.setStatusname(getFormFieldValueEncode(req, res, "status_name"));
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setRstatus(getFormFieldValueEncode(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setInstitutionid(getFormFieldValueEncode(req, res, "institution_id"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIStatuscodeRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIStatuscodeRecord loadMapLIStatuscodeRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLIStatuscodeRecord", null);
		LIStatuscodeRecord record = new LIStatuscodeRecord();
		record.setStatusid(getMapValue(inputMap, "status_id"));
		record.setStatusname(getMapValue(inputMap, "status_name"));
		record.setId(getMapValue(inputMap, "id"));
		record.setRstatus(getMapValue(inputMap, "rstatus"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		record.setInstitutionid(getMapValue(inputMap, "institution_id"));
		logger.trace("loadMapLIStatuscodeRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLIStatuscodeRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIStatuscodeRecord", null);
		LIStatuscodeService service = new LIStatuscodeService();

		try
		{
			LIStatuscodeRecord record = loadFormLIStatuscodeRecord(req, res);
			record.setCreatedby(getLoggedInUserId(req,res));
			service.insertLIStatuscodeRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Statuscode Creation Successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("statuscode.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create Statuscode");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create Statuscode " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("statuscode.jsp");
		}
	}

	public void processUpdateLIStatuscodeRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIStatuscodeRecord", null);
		LIStatuscodeService service = new LIStatuscodeService();

		try
		{
			LIStatuscodeRecord record = loadFormLIStatuscodeRecord(req, res);
			record.setModifiedby(getLoggedInUserId(req,res));
			service.updateLIStatuscodeRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "Statuscode data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("statuscode.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update Statuscode");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update Statuscode " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("statuscode.jsp");
		}
	}

	public void processDeleteLIStatuscodeRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIStatuscodeRecord", null);
		LIStatuscodeService service = new LIStatuscodeService();

		try
		{
			LIStatuscodeRecord record = loadFormLIStatuscodeRecord(req, res);
			service.deleteLIStatuscodeRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Statuscode deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("statuscode.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete Statuscode");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete Statuscode " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("statuscode.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertStatuscodeRecord"))
		{
			processInsertLIStatuscodeRecord(req, res);
			return;
		}

		if (actionType.equals("UpdateStatuscodeRecord"))
		{
			processUpdateLIStatuscodeRecord(req, res);
			return;
		}

		if (actionType.equals("DeleteStatuscodeRecord"))
		{
			processDeleteLIStatuscodeRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}
