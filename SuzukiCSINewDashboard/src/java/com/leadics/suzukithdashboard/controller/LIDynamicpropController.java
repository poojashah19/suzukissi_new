
/*
 * LIDynamicpropController.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.controller;
import com.leadics.suzukithdashboard.common.LIController;
import com.leadics.suzukithdashboard.service.LIDynamicpropService;
import com.leadics.suzukithdashboard.to.LIDynamicpropRecord;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import org.slf4j.*;
import com.leadics.utils.*;
public class LIDynamicpropController extends LIController
{
	static LogUtils logger = new LogUtils(LIDynamicpropController.class.getName());

	public LIDynamicpropRecord loadFormLIDynamicpropRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFormLIDynamicpropRecord", null);
		LIDynamicpropRecord record = new LIDynamicpropRecord();
		record.setId(getFormFieldValue(req, res, "tfId"));
		record.setPcategory(getFormFieldValue(req, res, "tfPcategory"));
		record.setPropname(getFormFieldValue(req, res, "tfPropname"));
		record.setPropval(getFormFieldValue(req, res, "tfPropval"));
		record.setPdesc(getFormFieldValue(req, res, "tfPdesc"));
		record.setPchgbyenv(getFormFieldValue(req, res, "tfPchgbyenv"));
		record.setPactive(getFormFieldValue(req, res, "tfPactive"));
		record.setRstatus(getFormFieldValue(req, res, "tfRstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "tfCreatedby"));
		record.setCreatedat(getFormFieldValue(req, res, "tfCreatedat"));
		record.setModifiedat(getFormFieldValue(req, res, "tfModifiedat"));
		record.setModifiedby(getFormFieldValue(req, res, "tfModifiedby"));
		record.setMadeby(getFormFieldValue(req, res, "tfMadeby"));
		record.setMadeat(getFormFieldValue(req, res, "tfMadeat"));
		record.setCheckedby(getFormFieldValue(req, res, "tfCheckedby"));
		record.setCheckedat(getFormFieldValue(req, res, "tfCheckedat"));
		record.setMakerlastcmt(getFormFieldValue(req, res, "tfMakerlastcmt"));
		record.setCheckerlastcmt(getFormFieldValue(req, res, "tfCheckerlastcmt"));
		record.setCurrappstatus(getFormFieldValue(req, res, "tfCurrappstatus"));
		record.setAdminlastcmt(getFormFieldValue(req, res, "tfAdminlastcmt"));
		record.setInstitutionid(getFormFieldValue(req, res, "tfInstitutionid"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadFormLIDynamicpropRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIDynamicpropRecord loadJSONFormLIDynamicpropRecord(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIDynamicpropRecord", null);
		LIDynamicpropRecord record = new LIDynamicpropRecord();
		record.setId(getFormFieldValue(req, res, "id"));
		record.setPcategory(getFormFieldValue(req, res, "pcategory"));
		record.setPropname(getFormFieldValue(req, res, "propname"));
		record.setPropval(getFormFieldValue(req, res, "propval"));
		record.setPdesc(getFormFieldValue(req, res, "pdesc"));
		record.setPchgbyenv(getFormFieldValue(req, res, "pchgbyenv"));
		record.setPactive(getFormFieldValue(req, res, "pactive"));
		record.setRstatus(getFormFieldValue(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValue(req, res, "created_by"));
		record.setCreatedat(getFormFieldValue(req, res, "created_at"));
		record.setModifiedat(getFormFieldValue(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValue(req, res, "modified_by"));
		record.setMadeby(getFormFieldValue(req, res, "made_by"));
		record.setMadeat(getFormFieldValue(req, res, "made_at"));
		record.setCheckedby(getFormFieldValue(req, res, "checked_by"));
		record.setCheckedat(getFormFieldValue(req, res, "checked_at"));
		record.setMakerlastcmt(getFormFieldValue(req, res, "maker_last_cmt"));
		record.setCheckerlastcmt(getFormFieldValue(req, res, "checker_last_cmt"));
		record.setCurrappstatus(getFormFieldValue(req, res, "curr_app_status"));
		record.setAdminlastcmt(getFormFieldValue(req, res, "admin_last_cmt"));
		record.setInstitutionid(getFormFieldValue(req, res, "institution_id"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIDynamicpropRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIDynamicpropRecord loadJSONFormLIDynamicpropRecordEncode(HttpServletRequest req, HttpServletResponse res)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadJSONFormLIDynamicpropRecord", null);
		LIDynamicpropRecord record = new LIDynamicpropRecord();
		record.setId(getFormFieldValueEncode(req, res, "id"));
		record.setPcategory(getFormFieldValueEncode(req, res, "pcategory"));
		record.setPropname(getFormFieldValueEncode(req, res, "propname"));
		record.setPropval(getFormFieldValueEncode(req, res, "propval"));
		record.setPdesc(getFormFieldValueEncode(req, res, "pdesc"));
		record.setPchgbyenv(getFormFieldValueEncode(req, res, "pchgbyenv"));
		record.setPactive(getFormFieldValueEncode(req, res, "pactive"));
		record.setRstatus(getFormFieldValueEncode(req, res, "rstatus"));
		record.setCreatedby(getFormFieldValueEncode(req, res, "created_by"));
		record.setCreatedat(getFormFieldValueEncode(req, res, "created_at"));
		record.setModifiedat(getFormFieldValueEncode(req, res, "modified_at"));
		record.setModifiedby(getFormFieldValueEncode(req, res, "modified_by"));
		record.setMadeby(getFormFieldValueEncode(req, res, "made_by"));
		record.setMadeat(getFormFieldValueEncode(req, res, "made_at"));
		record.setCheckedby(getFormFieldValueEncode(req, res, "checked_by"));
		record.setCheckedat(getFormFieldValueEncode(req, res, "checked_at"));
		record.setMakerlastcmt(getFormFieldValueEncode(req, res, "maker_last_cmt"));
		record.setCheckerlastcmt(getFormFieldValueEncode(req, res, "checker_last_cmt"));
		record.setCurrappstatus(getFormFieldValueEncode(req, res, "curr_app_status"));
		record.setAdminlastcmt(getFormFieldValueEncode(req, res, "admin_last_cmt"));
		record.setInstitutionid(getFormFieldValueEncode(req, res, "institution_id"));
		record.setActionSource(getIPAddress(req));
		record.setLoggID(getLoggedInID(req));
		record.setReqComponent(getReqComponent(req));
		logger.trace("loadJSONFormLIDynamicpropRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public LIDynamicpropRecord loadMapLIDynamicpropRecord(HashMap inputMap)
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("loadMapLIDynamicpropRecord", null);
		LIDynamicpropRecord record = new LIDynamicpropRecord();
		record.setId(getMapValue(inputMap, "id"));
		record.setPcategory(getMapValue(inputMap, "pcategory"));
		record.setPropname(getMapValue(inputMap, "propname"));
		record.setPropval(getMapValue(inputMap, "propval"));
		record.setPdesc(getMapValue(inputMap, "pdesc"));
		record.setPchgbyenv(getMapValue(inputMap, "pchgbyenv"));
		record.setPactive(getMapValue(inputMap, "pactive"));
		record.setRstatus(getMapValue(inputMap, "rstatus"));
		record.setCreatedby(getMapValue(inputMap, "created_by"));
		record.setCreatedat(getMapValue(inputMap, "created_at"));
		record.setModifiedat(getMapValue(inputMap, "modified_at"));
		record.setModifiedby(getMapValue(inputMap, "modified_by"));
		record.setMadeby(getMapValue(inputMap, "made_by"));
		record.setMadeat(getMapValue(inputMap, "made_at"));
		record.setCheckedby(getMapValue(inputMap, "checked_by"));
		record.setCheckedat(getMapValue(inputMap, "checked_at"));
		record.setMakerlastcmt(getMapValue(inputMap, "maker_last_cmt"));
		record.setCheckerlastcmt(getMapValue(inputMap, "checker_last_cmt"));
		record.setCurrappstatus(getMapValue(inputMap, "curr_app_status"));
		record.setAdminlastcmt(getMapValue(inputMap, "admin_last_cmt"));
		record.setInstitutionid(getMapValue(inputMap, "institution_id"));
		logger.trace("loadMapLIDynamicpropRecord\t" + record + "\t");
		logger.generateFunctionEndTimerMessage(beginMesssage);
		return record;
	}

	public void processInsertLIDynamicpropRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIDynamicpropRecord", null);
		LIDynamicpropService service = new LIDynamicpropService();

		try
		{
			LIDynamicpropRecord record = loadFormLIDynamicpropRecord(req, res);
			record.setCreatedby(getLoggedInUserId(req,res));
			int resultId = service.insertLIDynamicpropRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Record Creation Successful -" + resultId);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("dynamicprop.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to Create Dynamicprop");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Create Dynamicprop " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("dynamicprop.jsp");
		}
	}

	public void processUpdateLIDynamicpropRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIDynamicpropRecord", null);
		LIDynamicpropService service = new LIDynamicpropService();

		try
		{
			LIDynamicpropRecord record = loadFormLIDynamicpropRecord(req, res);
			record.setModifiedby(getLoggedInUserId(req,res));
			service.updateLIDynamicpropRecordNonNull(record);
			setActionMessage(req, res, "AdminActionMessage", "Dynamicprop data updation successful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("dynamicprop.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to update Dynamicprop");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to Update Dynamicprop " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("dynamicprop.jsp");
		}
	}

	public void processDeleteLIDynamicpropRecord(HttpServletRequest req, HttpServletResponse res)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processInsertLIDynamicpropRecord", null);
		LIDynamicpropService service = new LIDynamicpropService();

		try
		{
			LIDynamicpropRecord record = loadFormLIDynamicpropRecord(req, res);
			service.deleteLIDynamicpropRecord(record);
			setActionMessage(req, res, "AdminActionMessage", "Dynamicprop deletion succeesful");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("dynamicprop.jsp");
		}
		catch(Exception e)
		{
			setActionMessage(req, res, "AdminActionMessage", "Failed to delete Dynamicprop");
			setActionMessage(req, res, "AdminActionMessageException", e);
			logger.error("Failed to delete Dynamicprop " + e);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			res.sendRedirect("dynamicprop.jsp");
		}
	}

	public void processWebRequest(HttpServletRequest req, HttpServletResponse res, String actionType)
	throws Exception
	{
		String beginMesssage = logger.generateFunctionBeginTimerMessage("processWebRequest", null);
		logger.trace("Action Type:" + actionType + "");

		if (actionType.equals("InsertDynamicpropRecord"))
		{
			processInsertLIDynamicpropRecord(req, res);
			return;
		}

		if (actionType.equals("UpdateDynamicpropRecord"))
		{
			processUpdateLIDynamicpropRecord(req, res);
			return;
		}

		if (actionType.equals("DeleteDynamicpropRecord"))
		{
			processDeleteLIDynamicpropRecord(req, res);
			return;
		}
		logger.generateFunctionEndTimerMessage(beginMesssage);
	}

}
