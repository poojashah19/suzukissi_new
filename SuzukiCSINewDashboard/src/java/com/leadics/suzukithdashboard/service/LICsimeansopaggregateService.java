
/*
 * LICsimeansopaggregateService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LICsimeansopaggregateDAO;
import com.leadics.suzukithdashboard.to.LICsimeansopaggregateRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LICsimeansopaggregateService extends LIService
{
	static LogUtils logger = new LogUtils(LICsimeansopaggregateService.class.getName());


	public LICsimeansopaggregateRecord[] loadLICsimeansopaggregateRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLICsimeansopaggregateRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsimeansopaggregateRecords", null);
			LICsimeansopaggregateDAO dao = new LICsimeansopaggregateDAO();
			LICsimeansopaggregateRecord[] results = dao.loadLICsimeansopaggregateRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLICsimeansopaggregateRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLICsimeansopaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LICsimeansopaggregateRecord loadFirstLICsimeansopaggregateRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLICsimeansopaggregateRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLICsimeansopaggregateRecord", null);
			LICsimeansopaggregateDAO dao = new LICsimeansopaggregateDAO();
			LICsimeansopaggregateRecord result = dao.loadFirstLICsimeansopaggregateRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLICsimeansopaggregateRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICsimeansopaggregateRecord searchFirstLICsimeansopaggregateRecord(LICsimeansopaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLICsimeansopaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLICsimeansopaggregateRecord", null);
			LICsimeansopaggregateDAO dao = new LICsimeansopaggregateDAO();
			LICsimeansopaggregateRecord[] records = dao.searchLICsimeansopaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLICsimeansopaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LICsimeansopaggregateRecord searchFirstLICsimeansopaggregateRecordExactUpper(LICsimeansopaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLICsimeansopaggregateRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLICsimeansopaggregateRecordsExactUpper", null);
			LICsimeansopaggregateDAO dao = new LICsimeansopaggregateDAO();
			LICsimeansopaggregateRecord[] records = dao.searchLICsimeansopaggregateRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLICsimeansopaggregateRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICsimeansopaggregateRecord[] searchLICsimeansopaggregateRecords(LICsimeansopaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLICsimeansopaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsimeansopaggregateRecords", null);
			LICsimeansopaggregateDAO dao = new LICsimeansopaggregateDAO();
			LICsimeansopaggregateRecord[] records = dao.searchLICsimeansopaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLICsimeansopaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLICsimeansopaggregateRecordCount(LICsimeansopaggregateRecord record)
	throws Exception
	{
		return loadLICsimeansopaggregateRecordCount(record, null);
	}


	public int loadLICsimeansopaggregateRecordCount(LICsimeansopaggregateRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLICsimeansopaggregateRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsimeansopaggregateRecordCount", null);
			LICsimeansopaggregateDAO dao = new LICsimeansopaggregateDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLICsimeansopaggregateRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLICsimeansopaggregateRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICsimeansopaggregateRecord loadLICsimeansopaggregateRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLICsimeansopaggregateRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsimeansopaggregateRecord", null);
			LICsimeansopaggregateDAO dao = new LICsimeansopaggregateDAO();
			LICsimeansopaggregateRecord result = dao.loadLICsimeansopaggregateRecord(key);
			logger.trace("loadLICsimeansopaggregateRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLICsimeansopaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLICsimeansopaggregateRecordSearchResultByPage(LICsimeansopaggregateRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLICsimeansopaggregateRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLICsimeansopaggregateRecordSearchResultByPage(LICsimeansopaggregateRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLICsimeansopaggregateRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLICsimeansopaggregateRecordSearchResult", null);
			LICsimeansopaggregateDAO dao = new LICsimeansopaggregateDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLICsimeansopaggregateRecordCount(record);
			dao.setLimits(offset, maxrows);
			LICsimeansopaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLICsimeansopaggregateRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLICsimeansopaggregateRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLICsimeansopaggregateRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLICsimeansopaggregateRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLICsimeansopaggregateRecordSearchResultByPageQuery", null);
			LICsimeansopaggregateDAO dao = new LICsimeansopaggregateDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LICsimeansopaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLICsimeansopaggregateRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLICsimeansopaggregateRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLICsimeansopaggregateRecord(LICsimeansopaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLICsimeansopaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLICsimeansopaggregateRecord", null);
			LICsimeansopaggregateDAO dao = new LICsimeansopaggregateDAO();
			int result = dao.insertLICsimeansopaggregateRecord(record);
			logger.trace("insertLICsimeansopaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLICsimeansopaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLICsimeansopaggregateRecord(LICsimeansopaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLICsimeansopaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLICsimeansopaggregateRecord", null);
			LICsimeansopaggregateDAO dao = new LICsimeansopaggregateDAO();
			boolean result = dao.updateLICsimeansopaggregateRecord(record);
			logger.trace("updateLICsimeansopaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLICsimeansopaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLICsimeansopaggregateRecordNonNull(LICsimeansopaggregateRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLICsimeansopaggregateRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLICsimeansopaggregateRecordNonNull", null);
			LICsimeansopaggregateDAO dao = new LICsimeansopaggregateDAO();
			LICsimeansopaggregateRecord dbRecord = dao.loadLICsimeansopaggregateRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLICsimeansopaggregateRecord(dbRecord);
			logger.trace("updateLICsimeansopaggregateRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLICsimeansopaggregateRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLICsimeansopaggregateRecord(LICsimeansopaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLICsimeansopaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLICsimeansopaggregateRecord", null);
			LICsimeansopaggregateDAO dao = new LICsimeansopaggregateDAO();
			boolean result = dao.deleteLICsimeansopaggregateRecord(record);
			logger.trace("deleteLICsimeansopaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLICsimeansopaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
