
/*
 * LIReportquiresService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LIReportquiresDAO;
import com.leadics.suzukithdashboard.to.LIReportquiresRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIReportquiresService extends LIService
{
	static LogUtils logger = new LogUtils(LIReportquiresService.class.getName());


	public LIReportquiresRecord[] loadLIReportquiresRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIReportquiresRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIReportquiresRecords", null);
			LIReportquiresDAO dao = new LIReportquiresDAO();
			LIReportquiresRecord[] results = dao.loadLIReportquiresRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIReportquiresRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIReportquiresRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIReportquiresRecord loadFirstLIReportquiresRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIReportquiresRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIReportquiresRecord", null);
			LIReportquiresDAO dao = new LIReportquiresDAO();
			LIReportquiresRecord result = dao.loadFirstLIReportquiresRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIReportquiresRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIReportquiresRecord searchFirstLIReportquiresRecord(LIReportquiresRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIReportquiresRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIReportquiresRecord", null);
			LIReportquiresDAO dao = new LIReportquiresDAO();
			LIReportquiresRecord[] records = dao.searchLIReportquiresRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIReportquiresRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIReportquiresRecord searchFirstLIReportquiresRecordExactUpper(LIReportquiresRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIReportquiresRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIReportquiresRecordsExactUpper", null);
			LIReportquiresDAO dao = new LIReportquiresDAO();
			LIReportquiresRecord[] records = dao.searchLIReportquiresRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIReportquiresRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIReportquiresRecord[] searchLIReportquiresRecords(LIReportquiresRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIReportquiresRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIReportquiresRecords", null);
			LIReportquiresDAO dao = new LIReportquiresDAO();
			LIReportquiresRecord[] records = dao.searchLIReportquiresRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIReportquiresRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIReportquiresRecordCount(LIReportquiresRecord record)
	throws Exception
	{
		return loadLIReportquiresRecordCount(record, null);
	}


	public int loadLIReportquiresRecordCount(LIReportquiresRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIReportquiresRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIReportquiresRecordCount", null);
			LIReportquiresDAO dao = new LIReportquiresDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIReportquiresRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIReportquiresRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIReportquiresRecord loadLIReportquiresRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIReportquiresRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIReportquiresRecord", null);
			LIReportquiresDAO dao = new LIReportquiresDAO();
			LIReportquiresRecord result = dao.loadLIReportquiresRecord(key);
			logger.trace("loadLIReportquiresRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIReportquiresRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIReportquiresRecordSearchResultByPage(LIReportquiresRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIReportquiresRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIReportquiresRecordSearchResultByPage(LIReportquiresRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIReportquiresRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIReportquiresRecordSearchResult", null);
			LIReportquiresDAO dao = new LIReportquiresDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIReportquiresRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIReportquiresRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIReportquiresRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIReportquiresRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIReportquiresRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIReportquiresRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIReportquiresRecordSearchResultByPageQuery", null);
			LIReportquiresDAO dao = new LIReportquiresDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIReportquiresRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIReportquiresRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIReportquiresRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIReportquiresRecord(LIReportquiresRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIReportquiresRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIReportquiresRecord", null);
			LIReportquiresDAO dao = new LIReportquiresDAO();
			int result = dao.insertLIReportquiresRecord(record);
			logger.trace("insertLIReportquiresRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIReportquiresRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIReportquiresRecord(LIReportquiresRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIReportquiresRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIReportquiresRecord", null);
			LIReportquiresDAO dao = new LIReportquiresDAO();
			boolean result = dao.updateLIReportquiresRecord(record);
			logger.trace("updateLIReportquiresRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIReportquiresRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIReportquiresRecordNonNull(LIReportquiresRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIReportquiresRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIReportquiresRecordNonNull", null);
			LIReportquiresDAO dao = new LIReportquiresDAO();
			LIReportquiresRecord dbRecord = dao.loadLIReportquiresRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIReportquiresRecord(dbRecord);
			logger.trace("updateLIReportquiresRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIReportquiresRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIReportquiresRecord(LIReportquiresRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIReportquiresRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIReportquiresRecord", null);
			LIReportquiresDAO dao = new LIReportquiresDAO();
			boolean result = dao.deleteLIReportquiresRecord(record);
			logger.trace("deleteLIReportquiresRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIReportquiresRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
