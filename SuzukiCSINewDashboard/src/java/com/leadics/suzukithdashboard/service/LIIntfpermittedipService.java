
/*
 * LIIntfpermittedipService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LIIntfpermittedipDAO;
import com.leadics.suzukithdashboard.to.LIIntfpermittedipRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIIntfpermittedipService extends LIService
{
	static LogUtils logger = new LogUtils(LIIntfpermittedipService.class.getName());


	public LIIntfpermittedipRecord[] loadLIIntfpermittedipRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIIntfpermittedipRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIIntfpermittedipRecords", null);
			LIIntfpermittedipDAO dao = new LIIntfpermittedipDAO();
			LIIntfpermittedipRecord[] results = dao.loadLIIntfpermittedipRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIIntfpermittedipRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIIntfpermittedipRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIIntfpermittedipRecord loadFirstLIIntfpermittedipRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIIntfpermittedipRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIIntfpermittedipRecord", null);
			LIIntfpermittedipDAO dao = new LIIntfpermittedipDAO();
			LIIntfpermittedipRecord result = dao.loadFirstLIIntfpermittedipRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIIntfpermittedipRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIIntfpermittedipRecord searchFirstLIIntfpermittedipRecord(LIIntfpermittedipRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIIntfpermittedipRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIIntfpermittedipRecord", null);
			LIIntfpermittedipDAO dao = new LIIntfpermittedipDAO();
			LIIntfpermittedipRecord[] records = dao.searchLIIntfpermittedipRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIIntfpermittedipRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIIntfpermittedipRecord searchFirstLIIntfpermittedipRecordExactUpper(LIIntfpermittedipRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIIntfpermittedipRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIIntfpermittedipRecordsExactUpper", null);
			LIIntfpermittedipDAO dao = new LIIntfpermittedipDAO();
			LIIntfpermittedipRecord[] records = dao.searchLIIntfpermittedipRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIIntfpermittedipRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIIntfpermittedipRecord[] searchLIIntfpermittedipRecords(LIIntfpermittedipRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIIntfpermittedipRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIIntfpermittedipRecords", null);
			LIIntfpermittedipDAO dao = new LIIntfpermittedipDAO();
			LIIntfpermittedipRecord[] records = dao.searchLIIntfpermittedipRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIIntfpermittedipRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIIntfpermittedipRecordCount(LIIntfpermittedipRecord record)
	throws Exception
	{
		return loadLIIntfpermittedipRecordCount(record, null);
	}


	public int loadLIIntfpermittedipRecordCount(LIIntfpermittedipRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIIntfpermittedipRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIIntfpermittedipRecordCount", null);
			LIIntfpermittedipDAO dao = new LIIntfpermittedipDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIIntfpermittedipRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIIntfpermittedipRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIIntfpermittedipRecord loadLIIntfpermittedipRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIIntfpermittedipRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIIntfpermittedipRecord", null);
			LIIntfpermittedipDAO dao = new LIIntfpermittedipDAO();
			LIIntfpermittedipRecord result = dao.loadLIIntfpermittedipRecord(key);
			logger.trace("loadLIIntfpermittedipRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIIntfpermittedipRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIIntfpermittedipRecordSearchResultByPage(LIIntfpermittedipRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIIntfpermittedipRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIIntfpermittedipRecordSearchResultByPage(LIIntfpermittedipRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIIntfpermittedipRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIIntfpermittedipRecordSearchResult", null);
			LIIntfpermittedipDAO dao = new LIIntfpermittedipDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIIntfpermittedipRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIIntfpermittedipRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIIntfpermittedipRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIIntfpermittedipRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIIntfpermittedipRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIIntfpermittedipRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIIntfpermittedipRecordSearchResultByPageQuery", null);
			LIIntfpermittedipDAO dao = new LIIntfpermittedipDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIIntfpermittedipRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIIntfpermittedipRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIIntfpermittedipRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIIntfpermittedipRecord(LIIntfpermittedipRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIIntfpermittedipRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIIntfpermittedipRecord", null);
			LIIntfpermittedipDAO dao = new LIIntfpermittedipDAO();
			int result = dao.insertLIIntfpermittedipRecord(record);
			logger.trace("insertLIIntfpermittedipRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIIntfpermittedipRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIIntfpermittedipRecord(LIIntfpermittedipRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIIntfpermittedipRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIIntfpermittedipRecord", null);
			LIIntfpermittedipDAO dao = new LIIntfpermittedipDAO();
			boolean result = dao.updateLIIntfpermittedipRecord(record);
			logger.trace("updateLIIntfpermittedipRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIIntfpermittedipRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIIntfpermittedipRecordNonNull(LIIntfpermittedipRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIIntfpermittedipRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIIntfpermittedipRecordNonNull", null);
			LIIntfpermittedipDAO dao = new LIIntfpermittedipDAO();
			LIIntfpermittedipRecord dbRecord = dao.loadLIIntfpermittedipRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIIntfpermittedipRecord(dbRecord);
			logger.trace("updateLIIntfpermittedipRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIIntfpermittedipRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIIntfpermittedipRecord(LIIntfpermittedipRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIIntfpermittedipRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIIntfpermittedipRecord", null);
			LIIntfpermittedipDAO dao = new LIIntfpermittedipDAO();
			boolean result = dao.deleteLIIntfpermittedipRecord(record);
			logger.trace("deleteLIIntfpermittedipRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIIntfpermittedipRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
