
/*
 * LISsiloyaltyandadvocacyaggregateService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LISsiloyaltyandadvocacyaggregateDAO;
import com.leadics.suzukithdashboard.to.LISsiloyaltyandadvocacyaggregateRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LISsiloyaltyandadvocacyaggregateService extends LIService
{
	static LogUtils logger = new LogUtils(LISsiloyaltyandadvocacyaggregateService.class.getName());


	public LISsiloyaltyandadvocacyaggregateRecord[] loadLISsiloyaltyandadvocacyaggregateRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLISsiloyaltyandadvocacyaggregateRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsiloyaltyandadvocacyaggregateRecords", null);
			LISsiloyaltyandadvocacyaggregateDAO dao = new LISsiloyaltyandadvocacyaggregateDAO();
			LISsiloyaltyandadvocacyaggregateRecord[] results = dao.loadLISsiloyaltyandadvocacyaggregateRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLISsiloyaltyandadvocacyaggregateRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLISsiloyaltyandadvocacyaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LISsiloyaltyandadvocacyaggregateRecord loadFirstLISsiloyaltyandadvocacyaggregateRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLISsiloyaltyandadvocacyaggregateRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLISsiloyaltyandadvocacyaggregateRecord", null);
			LISsiloyaltyandadvocacyaggregateDAO dao = new LISsiloyaltyandadvocacyaggregateDAO();
			LISsiloyaltyandadvocacyaggregateRecord result = dao.loadFirstLISsiloyaltyandadvocacyaggregateRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLISsiloyaltyandadvocacyaggregateRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISsiloyaltyandadvocacyaggregateRecord searchFirstLISsiloyaltyandadvocacyaggregateRecord(LISsiloyaltyandadvocacyaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLISsiloyaltyandadvocacyaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLISsiloyaltyandadvocacyaggregateRecord", null);
			LISsiloyaltyandadvocacyaggregateDAO dao = new LISsiloyaltyandadvocacyaggregateDAO();
			LISsiloyaltyandadvocacyaggregateRecord[] records = dao.searchLISsiloyaltyandadvocacyaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLISsiloyaltyandadvocacyaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LISsiloyaltyandadvocacyaggregateRecord searchFirstLISsiloyaltyandadvocacyaggregateRecordExactUpper(LISsiloyaltyandadvocacyaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLISsiloyaltyandadvocacyaggregateRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLISsiloyaltyandadvocacyaggregateRecordsExactUpper", null);
			LISsiloyaltyandadvocacyaggregateDAO dao = new LISsiloyaltyandadvocacyaggregateDAO();
			LISsiloyaltyandadvocacyaggregateRecord[] records = dao.searchLISsiloyaltyandadvocacyaggregateRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLISsiloyaltyandadvocacyaggregateRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISsiloyaltyandadvocacyaggregateRecord[] searchLISsiloyaltyandadvocacyaggregateRecords(LISsiloyaltyandadvocacyaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLISsiloyaltyandadvocacyaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsiloyaltyandadvocacyaggregateRecords", null);
			LISsiloyaltyandadvocacyaggregateDAO dao = new LISsiloyaltyandadvocacyaggregateDAO();
			LISsiloyaltyandadvocacyaggregateRecord[] records = dao.searchLISsiloyaltyandadvocacyaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLISsiloyaltyandadvocacyaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLISsiloyaltyandadvocacyaggregateRecordCount(LISsiloyaltyandadvocacyaggregateRecord record)
	throws Exception
	{
		return loadLISsiloyaltyandadvocacyaggregateRecordCount(record, null);
	}


	public int loadLISsiloyaltyandadvocacyaggregateRecordCount(LISsiloyaltyandadvocacyaggregateRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLISsiloyaltyandadvocacyaggregateRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsiloyaltyandadvocacyaggregateRecordCount", null);
			LISsiloyaltyandadvocacyaggregateDAO dao = new LISsiloyaltyandadvocacyaggregateDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLISsiloyaltyandadvocacyaggregateRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLISsiloyaltyandadvocacyaggregateRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISsiloyaltyandadvocacyaggregateRecord loadLISsiloyaltyandadvocacyaggregateRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLISsiloyaltyandadvocacyaggregateRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsiloyaltyandadvocacyaggregateRecord", null);
			LISsiloyaltyandadvocacyaggregateDAO dao = new LISsiloyaltyandadvocacyaggregateDAO();
			LISsiloyaltyandadvocacyaggregateRecord result = dao.loadLISsiloyaltyandadvocacyaggregateRecord(key);
			logger.trace("loadLISsiloyaltyandadvocacyaggregateRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLISsiloyaltyandadvocacyaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLISsiloyaltyandadvocacyaggregateRecordSearchResultByPage(LISsiloyaltyandadvocacyaggregateRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLISsiloyaltyandadvocacyaggregateRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLISsiloyaltyandadvocacyaggregateRecordSearchResultByPage(LISsiloyaltyandadvocacyaggregateRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLISsiloyaltyandadvocacyaggregateRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLISsiloyaltyandadvocacyaggregateRecordSearchResult", null);
			LISsiloyaltyandadvocacyaggregateDAO dao = new LISsiloyaltyandadvocacyaggregateDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLISsiloyaltyandadvocacyaggregateRecordCount(record);
			dao.setLimits(offset, maxrows);
			LISsiloyaltyandadvocacyaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLISsiloyaltyandadvocacyaggregateRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLISsiloyaltyandadvocacyaggregateRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLISsiloyaltyandadvocacyaggregateRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLISsiloyaltyandadvocacyaggregateRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLISsiloyaltyandadvocacyaggregateRecordSearchResultByPageQuery", null);
			LISsiloyaltyandadvocacyaggregateDAO dao = new LISsiloyaltyandadvocacyaggregateDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LISsiloyaltyandadvocacyaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLISsiloyaltyandadvocacyaggregateRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLISsiloyaltyandadvocacyaggregateRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLISsiloyaltyandadvocacyaggregateRecord(LISsiloyaltyandadvocacyaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLISsiloyaltyandadvocacyaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLISsiloyaltyandadvocacyaggregateRecord", null);
			LISsiloyaltyandadvocacyaggregateDAO dao = new LISsiloyaltyandadvocacyaggregateDAO();
			int result = dao.insertLISsiloyaltyandadvocacyaggregateRecord(record);
			logger.trace("insertLISsiloyaltyandadvocacyaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLISsiloyaltyandadvocacyaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLISsiloyaltyandadvocacyaggregateRecord(LISsiloyaltyandadvocacyaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLISsiloyaltyandadvocacyaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLISsiloyaltyandadvocacyaggregateRecord", null);
			LISsiloyaltyandadvocacyaggregateDAO dao = new LISsiloyaltyandadvocacyaggregateDAO();
			boolean result = dao.updateLISsiloyaltyandadvocacyaggregateRecord(record);
			logger.trace("updateLISsiloyaltyandadvocacyaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLISsiloyaltyandadvocacyaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLISsiloyaltyandadvocacyaggregateRecordNonNull(LISsiloyaltyandadvocacyaggregateRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLISsiloyaltyandadvocacyaggregateRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLISsiloyaltyandadvocacyaggregateRecordNonNull", null);
			LISsiloyaltyandadvocacyaggregateDAO dao = new LISsiloyaltyandadvocacyaggregateDAO();
			LISsiloyaltyandadvocacyaggregateRecord dbRecord = dao.loadLISsiloyaltyandadvocacyaggregateRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLISsiloyaltyandadvocacyaggregateRecord(dbRecord);
			logger.trace("updateLISsiloyaltyandadvocacyaggregateRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLISsiloyaltyandadvocacyaggregateRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLISsiloyaltyandadvocacyaggregateRecord(LISsiloyaltyandadvocacyaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLISsiloyaltyandadvocacyaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLISsiloyaltyandadvocacyaggregateRecord", null);
			LISsiloyaltyandadvocacyaggregateDAO dao = new LISsiloyaltyandadvocacyaggregateDAO();
			boolean result = dao.deleteLISsiloyaltyandadvocacyaggregateRecord(record);
			logger.trace("deleteLISsiloyaltyandadvocacyaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLISsiloyaltyandadvocacyaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
