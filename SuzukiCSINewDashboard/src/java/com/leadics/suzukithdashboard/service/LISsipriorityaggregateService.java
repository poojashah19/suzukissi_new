
/*
 * LISsipriorityaggregateService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LISsipriorityaggregateDAO;
import com.leadics.suzukithdashboard.to.LISsipriorityaggregateRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LISsipriorityaggregateService extends LIService
{
	static LogUtils logger = new LogUtils(LISsipriorityaggregateService.class.getName());


	public LISsipriorityaggregateRecord[] loadLISsipriorityaggregateRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLISsipriorityaggregateRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsipriorityaggregateRecords", null);
			LISsipriorityaggregateDAO dao = new LISsipriorityaggregateDAO();
			LISsipriorityaggregateRecord[] results = dao.loadLISsipriorityaggregateRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLISsipriorityaggregateRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLISsipriorityaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LISsipriorityaggregateRecord loadFirstLISsipriorityaggregateRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLISsipriorityaggregateRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLISsipriorityaggregateRecord", null);
			LISsipriorityaggregateDAO dao = new LISsipriorityaggregateDAO();
			LISsipriorityaggregateRecord result = dao.loadFirstLISsipriorityaggregateRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLISsipriorityaggregateRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISsipriorityaggregateRecord searchFirstLISsipriorityaggregateRecord(LISsipriorityaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLISsipriorityaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLISsipriorityaggregateRecord", null);
			LISsipriorityaggregateDAO dao = new LISsipriorityaggregateDAO();
			LISsipriorityaggregateRecord[] records = dao.searchLISsipriorityaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLISsipriorityaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LISsipriorityaggregateRecord searchFirstLISsipriorityaggregateRecordExactUpper(LISsipriorityaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLISsipriorityaggregateRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLISsipriorityaggregateRecordsExactUpper", null);
			LISsipriorityaggregateDAO dao = new LISsipriorityaggregateDAO();
			LISsipriorityaggregateRecord[] records = dao.searchLISsipriorityaggregateRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLISsipriorityaggregateRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISsipriorityaggregateRecord[] searchLISsipriorityaggregateRecords(LISsipriorityaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLISsipriorityaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsipriorityaggregateRecords", null);
			LISsipriorityaggregateDAO dao = new LISsipriorityaggregateDAO();
			LISsipriorityaggregateRecord[] records = dao.searchLISsipriorityaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLISsipriorityaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLISsipriorityaggregateRecordCount(LISsipriorityaggregateRecord record)
	throws Exception
	{
		return loadLISsipriorityaggregateRecordCount(record, null);
	}


	public int loadLISsipriorityaggregateRecordCount(LISsipriorityaggregateRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLISsipriorityaggregateRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsipriorityaggregateRecordCount", null);
			LISsipriorityaggregateDAO dao = new LISsipriorityaggregateDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLISsipriorityaggregateRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLISsipriorityaggregateRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISsipriorityaggregateRecord loadLISsipriorityaggregateRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLISsipriorityaggregateRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsipriorityaggregateRecord", null);
			LISsipriorityaggregateDAO dao = new LISsipriorityaggregateDAO();
			LISsipriorityaggregateRecord result = dao.loadLISsipriorityaggregateRecord(key);
			logger.trace("loadLISsipriorityaggregateRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLISsipriorityaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLISsipriorityaggregateRecordSearchResultByPage(LISsipriorityaggregateRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLISsipriorityaggregateRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLISsipriorityaggregateRecordSearchResultByPage(LISsipriorityaggregateRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLISsipriorityaggregateRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLISsipriorityaggregateRecordSearchResult", null);
			LISsipriorityaggregateDAO dao = new LISsipriorityaggregateDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLISsipriorityaggregateRecordCount(record);
			dao.setLimits(offset, maxrows);
			LISsipriorityaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLISsipriorityaggregateRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLISsipriorityaggregateRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLISsipriorityaggregateRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLISsipriorityaggregateRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLISsipriorityaggregateRecordSearchResultByPageQuery", null);
			LISsipriorityaggregateDAO dao = new LISsipriorityaggregateDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LISsipriorityaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLISsipriorityaggregateRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLISsipriorityaggregateRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLISsipriorityaggregateRecord(LISsipriorityaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLISsipriorityaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLISsipriorityaggregateRecord", null);
			LISsipriorityaggregateDAO dao = new LISsipriorityaggregateDAO();
			int result = dao.insertLISsipriorityaggregateRecord(record);
			logger.trace("insertLISsipriorityaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLISsipriorityaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLISsipriorityaggregateRecord(LISsipriorityaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLISsipriorityaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLISsipriorityaggregateRecord", null);
			LISsipriorityaggregateDAO dao = new LISsipriorityaggregateDAO();
			boolean result = dao.updateLISsipriorityaggregateRecord(record);
			logger.trace("updateLISsipriorityaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLISsipriorityaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLISsipriorityaggregateRecordNonNull(LISsipriorityaggregateRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLISsipriorityaggregateRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLISsipriorityaggregateRecordNonNull", null);
			LISsipriorityaggregateDAO dao = new LISsipriorityaggregateDAO();
			LISsipriorityaggregateRecord dbRecord = dao.loadLISsipriorityaggregateRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLISsipriorityaggregateRecord(dbRecord);
			logger.trace("updateLISsipriorityaggregateRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLISsipriorityaggregateRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLISsipriorityaggregateRecord(LISsipriorityaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLISsipriorityaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLISsipriorityaggregateRecord", null);
			LISsipriorityaggregateDAO dao = new LISsipriorityaggregateDAO();
			boolean result = dao.deleteLISsipriorityaggregateRecord(record);
			logger.trace("deleteLISsipriorityaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLISsipriorityaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
