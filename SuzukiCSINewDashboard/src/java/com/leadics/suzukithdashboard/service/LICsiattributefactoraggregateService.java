
/*
 * LICsiattributefactoraggregateService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LICsiattributefactoraggregateDAO;
import com.leadics.suzukithdashboard.to.LICsiattributefactoraggregateRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LICsiattributefactoraggregateService extends LIService
{
	static LogUtils logger = new LogUtils(LICsiattributefactoraggregateService.class.getName());


	public LICsiattributefactoraggregateRecord[] loadLICsiattributefactoraggregateRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLICsiattributefactoraggregateRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsiattributefactoraggregateRecords", null);
			LICsiattributefactoraggregateDAO dao = new LICsiattributefactoraggregateDAO();
			LICsiattributefactoraggregateRecord[] results = dao.loadLICsiattributefactoraggregateRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLICsiattributefactoraggregateRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLICsiattributefactoraggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LICsiattributefactoraggregateRecord loadFirstLICsiattributefactoraggregateRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLICsiattributefactoraggregateRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLICsiattributefactoraggregateRecord", null);
			LICsiattributefactoraggregateDAO dao = new LICsiattributefactoraggregateDAO();
			LICsiattributefactoraggregateRecord result = dao.loadFirstLICsiattributefactoraggregateRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLICsiattributefactoraggregateRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICsiattributefactoraggregateRecord searchFirstLICsiattributefactoraggregateRecord(LICsiattributefactoraggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLICsiattributefactoraggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLICsiattributefactoraggregateRecord", null);
			LICsiattributefactoraggregateDAO dao = new LICsiattributefactoraggregateDAO();
			LICsiattributefactoraggregateRecord[] records = dao.searchLICsiattributefactoraggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLICsiattributefactoraggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LICsiattributefactoraggregateRecord searchFirstLICsiattributefactoraggregateRecordExactUpper(LICsiattributefactoraggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLICsiattributefactoraggregateRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLICsiattributefactoraggregateRecordsExactUpper", null);
			LICsiattributefactoraggregateDAO dao = new LICsiattributefactoraggregateDAO();
			LICsiattributefactoraggregateRecord[] records = dao.searchLICsiattributefactoraggregateRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLICsiattributefactoraggregateRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICsiattributefactoraggregateRecord[] searchLICsiattributefactoraggregateRecords(LICsiattributefactoraggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLICsiattributefactoraggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsiattributefactoraggregateRecords", null);
			LICsiattributefactoraggregateDAO dao = new LICsiattributefactoraggregateDAO();
			LICsiattributefactoraggregateRecord[] records = dao.searchLICsiattributefactoraggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLICsiattributefactoraggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLICsiattributefactoraggregateRecordCount(LICsiattributefactoraggregateRecord record)
	throws Exception
	{
		return loadLICsiattributefactoraggregateRecordCount(record, null);
	}


	public int loadLICsiattributefactoraggregateRecordCount(LICsiattributefactoraggregateRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLICsiattributefactoraggregateRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsiattributefactoraggregateRecordCount", null);
			LICsiattributefactoraggregateDAO dao = new LICsiattributefactoraggregateDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLICsiattributefactoraggregateRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLICsiattributefactoraggregateRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICsiattributefactoraggregateRecord loadLICsiattributefactoraggregateRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLICsiattributefactoraggregateRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsiattributefactoraggregateRecord", null);
			LICsiattributefactoraggregateDAO dao = new LICsiattributefactoraggregateDAO();
			LICsiattributefactoraggregateRecord result = dao.loadLICsiattributefactoraggregateRecord(key);
			logger.trace("loadLICsiattributefactoraggregateRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLICsiattributefactoraggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLICsiattributefactoraggregateRecordSearchResultByPage(LICsiattributefactoraggregateRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLICsiattributefactoraggregateRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLICsiattributefactoraggregateRecordSearchResultByPage(LICsiattributefactoraggregateRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLICsiattributefactoraggregateRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLICsiattributefactoraggregateRecordSearchResult", null);
			LICsiattributefactoraggregateDAO dao = new LICsiattributefactoraggregateDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLICsiattributefactoraggregateRecordCount(record);
			dao.setLimits(offset, maxrows);
			LICsiattributefactoraggregateRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLICsiattributefactoraggregateRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLICsiattributefactoraggregateRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLICsiattributefactoraggregateRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLICsiattributefactoraggregateRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLICsiattributefactoraggregateRecordSearchResultByPageQuery", null);
			LICsiattributefactoraggregateDAO dao = new LICsiattributefactoraggregateDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LICsiattributefactoraggregateRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLICsiattributefactoraggregateRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLICsiattributefactoraggregateRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLICsiattributefactoraggregateRecord(LICsiattributefactoraggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLICsiattributefactoraggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLICsiattributefactoraggregateRecord", null);
			LICsiattributefactoraggregateDAO dao = new LICsiattributefactoraggregateDAO();
			int result = dao.insertLICsiattributefactoraggregateRecord(record);
			logger.trace("insertLICsiattributefactoraggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLICsiattributefactoraggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLICsiattributefactoraggregateRecord(LICsiattributefactoraggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLICsiattributefactoraggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLICsiattributefactoraggregateRecord", null);
			LICsiattributefactoraggregateDAO dao = new LICsiattributefactoraggregateDAO();
			boolean result = dao.updateLICsiattributefactoraggregateRecord(record);
			logger.trace("updateLICsiattributefactoraggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLICsiattributefactoraggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLICsiattributefactoraggregateRecordNonNull(LICsiattributefactoraggregateRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLICsiattributefactoraggregateRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLICsiattributefactoraggregateRecordNonNull", null);
			LICsiattributefactoraggregateDAO dao = new LICsiattributefactoraggregateDAO();
			LICsiattributefactoraggregateRecord dbRecord = dao.loadLICsiattributefactoraggregateRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLICsiattributefactoraggregateRecord(dbRecord);
			logger.trace("updateLICsiattributefactoraggregateRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLICsiattributefactoraggregateRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLICsiattributefactoraggregateRecord(LICsiattributefactoraggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLICsiattributefactoraggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLICsiattributefactoraggregateRecord", null);
			LICsiattributefactoraggregateDAO dao = new LICsiattributefactoraggregateDAO();
			boolean result = dao.deleteLICsiattributefactoraggregateRecord(record);
			logger.trace("deleteLICsiattributefactoraggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLICsiattributefactoraggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
