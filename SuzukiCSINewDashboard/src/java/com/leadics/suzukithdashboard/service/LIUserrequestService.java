
/*
 * LIUserrequestService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LIUserrequestDAO;
import com.leadics.suzukithdashboard.to.LIUserrequestRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIUserrequestService extends LIService
{
	static LogUtils logger = new LogUtils(LIUserrequestService.class.getName());


	public LIUserrequestRecord[] loadLIUserrequestRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIUserrequestRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIUserrequestRecords", null);
			LIUserrequestDAO dao = new LIUserrequestDAO();
			LIUserrequestRecord[] results = dao.loadLIUserrequestRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIUserrequestRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIUserrequestRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIUserrequestRecord loadFirstLIUserrequestRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIUserrequestRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIUserrequestRecord", null);
			LIUserrequestDAO dao = new LIUserrequestDAO();
			LIUserrequestRecord result = dao.loadFirstLIUserrequestRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIUserrequestRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIUserrequestRecord searchFirstLIUserrequestRecord(LIUserrequestRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIUserrequestRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIUserrequestRecord", null);
			LIUserrequestDAO dao = new LIUserrequestDAO();
			LIUserrequestRecord[] records = dao.searchLIUserrequestRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIUserrequestRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIUserrequestRecord searchFirstLIUserrequestRecordExactUpper(LIUserrequestRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIUserrequestRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIUserrequestRecordsExactUpper", null);
			LIUserrequestDAO dao = new LIUserrequestDAO();
			LIUserrequestRecord[] records = dao.searchLIUserrequestRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIUserrequestRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIUserrequestRecord[] searchLIUserrequestRecords(LIUserrequestRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIUserrequestRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIUserrequestRecords", null);
			LIUserrequestDAO dao = new LIUserrequestDAO();
			LIUserrequestRecord[] records = dao.searchLIUserrequestRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIUserrequestRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIUserrequestRecordCount(LIUserrequestRecord record)
	throws Exception
	{
		return loadLIUserrequestRecordCount(record, null);
	}


	public int loadLIUserrequestRecordCount(LIUserrequestRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIUserrequestRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIUserrequestRecordCount", null);
			LIUserrequestDAO dao = new LIUserrequestDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIUserrequestRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIUserrequestRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIUserrequestRecord loadLIUserrequestRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIUserrequestRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIUserrequestRecord", null);
			LIUserrequestDAO dao = new LIUserrequestDAO();
			LIUserrequestRecord result = dao.loadLIUserrequestRecord(key);
			logger.trace("loadLIUserrequestRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIUserrequestRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIUserrequestRecordSearchResultByPage(LIUserrequestRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIUserrequestRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIUserrequestRecordSearchResultByPage(LIUserrequestRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIUserrequestRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIUserrequestRecordSearchResult", null);
			LIUserrequestDAO dao = new LIUserrequestDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIUserrequestRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIUserrequestRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIUserrequestRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIUserrequestRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIUserrequestRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIUserrequestRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIUserrequestRecordSearchResultByPageQuery", null);
			LIUserrequestDAO dao = new LIUserrequestDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIUserrequestRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIUserrequestRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIUserrequestRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIUserrequestRecord(LIUserrequestRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIUserrequestRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIUserrequestRecord", null);
			LIUserrequestDAO dao = new LIUserrequestDAO();
			int result = dao.insertLIUserrequestRecord(record);
			logger.trace("insertLIUserrequestRecord:Result:" + result);			
			createMakerCheckerAuditEntry("user_request",result + "", "Create",record.getCreatedby(),"Created");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIUserrequestRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIUserrequestRecord(LIUserrequestRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIUserrequestRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIUserrequestRecord", null);
			LIUserrequestDAO dao = new LIUserrequestDAO();
			boolean result = dao.updateLIUserrequestRecord(record);
			createMakerCheckerAuditEntry("user_request",record.getId(), "Update",record.getModifiedby(),"Update");
			logger.trace("updateLIUserrequestRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIUserrequestRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIUserrequestRecordNonNull(LIUserrequestRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIUserrequestRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIUserrequestRecordNonNull", null);
			LIUserrequestDAO dao = new LIUserrequestDAO();
			LIUserrequestRecord dbRecord = dao.loadLIUserrequestRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIUserrequestRecord(dbRecord);
			createMakerCheckerAuditEntry("user_request",inputRecord.getId(), "Update",dbRecord.getModifiedby(),"Update");
			logger.trace("updateLIUserrequestRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIUserrequestRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIUserrequestRecord(LIUserrequestRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIUserrequestRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIUserrequestRecord", null);
			LIUserrequestDAO dao = new LIUserrequestDAO();
			boolean result = dao.deleteLIUserrequestRecord(record);
			logger.trace("deleteLIUserrequestRecord:Result:" + result);			
			createMakerCheckerAuditEntry("user_request",record.getId(), "Delete",null,"Deleted");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIUserrequestRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean approveLIUserrequestRecord(LIUserrequestRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("approveLIUserrequestRecord:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("approveLIUserrequestRecord", null);
			LIUserrequestDAO dao = new LIUserrequestDAO();
			LIUserrequestRecord updateRecord = dao.loadLIUserrequestRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CA-RNFA104:Record not found");
			}
			if (!isValidStatusForApproval(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CA-IS101:Cannot approve - Invalid Status for Approval");
			}
			if (StringUtils.isSame(updateRecord.getMadeby(), updateId))
			{
				throw new Exception("MF-ERR-CA-MCS102:Cannot Approve. Maker Checker cannot be same");
			}
			updateRecord.setRstatus("1");
			updateRecord.setCheckerlastcmt(comment);
			updateRecord.setCurrappstatus("1");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setCheckedat(DateUtils.getCurrentDateTime());
			updateRecord.setCheckedby(updateId);
			boolean result = dao.updateLIUserrequestRecord(updateRecord);
			logger.trace("approveLIUserrequestRecord:Result:" + result);			
			createMakerCheckerAuditEntry("user_request",updateRecord.getId(), "Approve",updateId,comment);
			HashMap approveEventMap = callActionEvent("Event_OnApprovalOf_LIUserrequestRecord", updateRecord.getId());
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("approveLIUserrequestRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean submitLIUserrequestRecord(LIUserrequestRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("submitLIUserrequestRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("submitLIUserrequestRecordNonNull", null);
			LIUserrequestDAO dao = new LIUserrequestDAO();
			LIUserrequestRecord updateRecord = dao.loadLIUserrequestRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CS-RNFA104:Record not found");
			}
			if (!isValidStatusForSubmission(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CS-IS101:Cannot submit - Invalid Status for Submission");
			}
			updateRecord.setRstatus("1");
			updateRecord.setMakerlastcmt(comment);
			updateRecord.setCurrappstatus("4");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setMadeat(DateUtils.getCurrentDateTime());
			updateRecord.setMadeby(updateId);
			boolean result = dao.updateLIUserrequestRecord(updateRecord);
			createMakerCheckerAuditEntry("user_request",updateRecord.getId(),"Submit",updateId,comment);
			HashMap submitEventMap = callActionEvent("Event_OnSubmitOf_LIUserrequestRecord", updateRecord.getId());
			logger.trace("submitLIUserrequestRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("submitLIUserrequestRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean denyLIUserrequestRecord(LIUserrequestRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("denyLIUserrequestRecord:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("denyLIUserrequestRecord", null);
			LIUserrequestDAO dao = new LIUserrequestDAO();
			LIUserrequestRecord updateRecord = dao.loadLIUserrequestRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CD-RNFA104:Record not found");
			}
			if (!isValidStatusForDeny(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CD-IS101:Cannot deny - Invalid Status for Deny");
			}
			if (StringUtils.isSame(updateRecord.getMadeby(), updateId))
			{
				throw new Exception("MF-ERR-CD-MCS101:Cannot Deny. Maker Checker cannot be same");
			}
			updateRecord.setCheckerlastcmt(comment);
			updateRecord.setCurrappstatus("5");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setCheckedat(DateUtils.getCurrentDateTime());
			updateRecord.setCheckedby(updateId);
			boolean result = dao.updateLIUserrequestRecord(updateRecord);
			createMakerCheckerAuditEntry("user_request",updateRecord.getId(),"Deny",updateId,comment);
			HashMap denyEventMap = callActionEvent("Event_OnDenyOf_LIUserrequestRecord", updateRecord.getId());
			logger.trace("denyLIUserrequestRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("denyLIUserrequestRecord" + getStackTrace(exception));
			throw exception;
		}
	}
	public boolean denyPermanantlyLIUserrequestRecord(LIUserrequestRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("denyPermanantlyLIUserrequestRecord:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("denyPermanantlyLIUserrequestRecord", null);
			LIUserrequestDAO dao = new LIUserrequestDAO();
			LIUserrequestRecord updateRecord = dao.loadLIUserrequestRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CD-RNFADP104:Record not found");
			}
			if (!isValidStatusForDeny(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CD-ISDP101:Cannot deny - Invalid Status for Deny");
			}
			if (StringUtils.isSame(updateRecord.getMadeby(), updateId))
			{
				throw new Exception("MF-ERR-CD-MCS101DP:Cannot Deny. Maker Checker cannot be same");
			}
			updateRecord.setCheckerlastcmt(comment);
			updateRecord.setCurrappstatus("58");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setCheckedat(DateUtils.getCurrentDateTime());
			updateRecord.setCheckedby(updateId);
			boolean result = dao.updateLIUserrequestRecord(updateRecord);
			createMakerCheckerAuditEntry("user_request",updateRecord.getId(),"DenyP",updateId,comment);
			HashMap denyEventMap = callActionEvent("Event_OnDenyPOf_LIUserrequestRecord", updateRecord.getId());
			logger.trace("denyPLIUserrequestRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("denyPLIUserrequestRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean remindApprovalLIUserrequestRecord(LIUserrequestRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("remindApprovalLIUserrequestRecord:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("remindApprovalLIUserrequestRecord", null);
			LIUserrequestDAO dao = new LIUserrequestDAO();
			LIUserrequestRecord updateRecord = dao.loadLIUserrequestRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CR-RNFA104:Record not found");
			}
			if (!isValidStatusForApproval(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CR-IS101:Cannot Remind - Invalid Status for Reminder");
			}
			createMakerCheckerAuditEntry("user_request",updateRecord.getId(), "Approval Reminder",updateId,comment);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return true;
		}
		catch(Exception exception)
		{
			logger.error("remindApprovalLIUserrequestRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean resetApprovalLIUserrequestRecord(LIUserrequestRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("resetApprovalLIUserrequestRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("resetApprovalLIUserrequestRecordNonNull", null);
			LIUserrequestDAO dao = new LIUserrequestDAO();
			LIUserrequestRecord updateRecord = dao.loadLIUserrequestRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-RA-RNFA104:Record not found");
			}
			if (!isValidStatusForReset(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-RA-IS101:Cannot submit - Invalid Status for Reset");
			}
			updateRecord.setRstatus("0");
			updateRecord.setAdminlastcmt(comment);
			updateRecord.setCurrappstatus("0");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setMadeat(DateUtils.getCurrentDateTime());
			updateRecord.setMadeby(updateId);
			boolean result = dao.updateLIUserrequestRecord(updateRecord);
			createMakerCheckerAuditEntry("user_request",updateRecord.getId(),"Reset Approval",updateId,comment);
			logger.trace("resetApprovalLIUserrequestRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("resetApprovalLIUserrequestRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
