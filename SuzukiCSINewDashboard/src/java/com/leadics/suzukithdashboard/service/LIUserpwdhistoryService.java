
/*
 * LIUserpwdhistoryService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LIUserpwdhistoryDAO;
import com.leadics.suzukithdashboard.to.LIUserpwdhistoryRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIUserpwdhistoryService extends LIService
{
	static LogUtils logger = new LogUtils(LIUserpwdhistoryService.class.getName());


	public LIUserpwdhistoryRecord[] loadLIUserpwdhistoryRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIUserpwdhistoryRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIUserpwdhistoryRecords", null);
			LIUserpwdhistoryDAO dao = new LIUserpwdhistoryDAO();
			LIUserpwdhistoryRecord[] results = dao.loadLIUserpwdhistoryRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIUserpwdhistoryRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIUserpwdhistoryRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIUserpwdhistoryRecord loadFirstLIUserpwdhistoryRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIUserpwdhistoryRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIUserpwdhistoryRecord", null);
			LIUserpwdhistoryDAO dao = new LIUserpwdhistoryDAO();
			LIUserpwdhistoryRecord result = dao.loadFirstLIUserpwdhistoryRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIUserpwdhistoryRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIUserpwdhistoryRecord searchFirstLIUserpwdhistoryRecord(LIUserpwdhistoryRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIUserpwdhistoryRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIUserpwdhistoryRecord", null);
			LIUserpwdhistoryDAO dao = new LIUserpwdhistoryDAO();
			LIUserpwdhistoryRecord[] records = dao.searchLIUserpwdhistoryRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIUserpwdhistoryRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIUserpwdhistoryRecord searchFirstLIUserpwdhistoryRecordExactUpper(LIUserpwdhistoryRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIUserpwdhistoryRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIUserpwdhistoryRecordsExactUpper", null);
			LIUserpwdhistoryDAO dao = new LIUserpwdhistoryDAO();
			LIUserpwdhistoryRecord[] records = dao.searchLIUserpwdhistoryRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIUserpwdhistoryRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIUserpwdhistoryRecord[] searchLIUserpwdhistoryRecords(LIUserpwdhistoryRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIUserpwdhistoryRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIUserpwdhistoryRecords", null);
			LIUserpwdhistoryDAO dao = new LIUserpwdhistoryDAO();
			LIUserpwdhistoryRecord[] records = dao.searchLIUserpwdhistoryRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIUserpwdhistoryRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIUserpwdhistoryRecordCount(LIUserpwdhistoryRecord record)
	throws Exception
	{
		return loadLIUserpwdhistoryRecordCount(record, null);
	}


	public int loadLIUserpwdhistoryRecordCount(LIUserpwdhistoryRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIUserpwdhistoryRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIUserpwdhistoryRecordCount", null);
			LIUserpwdhistoryDAO dao = new LIUserpwdhistoryDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIUserpwdhistoryRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIUserpwdhistoryRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIUserpwdhistoryRecord loadLIUserpwdhistoryRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIUserpwdhistoryRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIUserpwdhistoryRecord", null);
			LIUserpwdhistoryDAO dao = new LIUserpwdhistoryDAO();
			LIUserpwdhistoryRecord result = dao.loadLIUserpwdhistoryRecord(key);
			logger.trace("loadLIUserpwdhistoryRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIUserpwdhistoryRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIUserpwdhistoryRecordSearchResultByPage(LIUserpwdhistoryRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIUserpwdhistoryRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIUserpwdhistoryRecordSearchResultByPage(LIUserpwdhistoryRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIUserpwdhistoryRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIUserpwdhistoryRecordSearchResult", null);
			LIUserpwdhistoryDAO dao = new LIUserpwdhistoryDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIUserpwdhistoryRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIUserpwdhistoryRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIUserpwdhistoryRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIUserpwdhistoryRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIUserpwdhistoryRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIUserpwdhistoryRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIUserpwdhistoryRecordSearchResultByPageQuery", null);
			LIUserpwdhistoryDAO dao = new LIUserpwdhistoryDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIUserpwdhistoryRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIUserpwdhistoryRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIUserpwdhistoryRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIUserpwdhistoryRecord(LIUserpwdhistoryRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIUserpwdhistoryRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIUserpwdhistoryRecord", null);
			LIUserpwdhistoryDAO dao = new LIUserpwdhistoryDAO();
			int result = dao.insertLIUserpwdhistoryRecord(record);
			logger.trace("insertLIUserpwdhistoryRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIUserpwdhistoryRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIUserpwdhistoryRecord(LIUserpwdhistoryRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIUserpwdhistoryRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIUserpwdhistoryRecord", null);
			LIUserpwdhistoryDAO dao = new LIUserpwdhistoryDAO();
			boolean result = dao.updateLIUserpwdhistoryRecord(record);
			logger.trace("updateLIUserpwdhistoryRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIUserpwdhistoryRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIUserpwdhistoryRecordNonNull(LIUserpwdhistoryRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIUserpwdhistoryRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIUserpwdhistoryRecordNonNull", null);
			LIUserpwdhistoryDAO dao = new LIUserpwdhistoryDAO();
			LIUserpwdhistoryRecord dbRecord = dao.loadLIUserpwdhistoryRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIUserpwdhistoryRecord(dbRecord);
			logger.trace("updateLIUserpwdhistoryRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIUserpwdhistoryRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIUserpwdhistoryRecord(LIUserpwdhistoryRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIUserpwdhistoryRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIUserpwdhistoryRecord", null);
			LIUserpwdhistoryDAO dao = new LIUserpwdhistoryDAO();
			boolean result = dao.deleteLIUserpwdhistoryRecord(record);
			logger.trace("deleteLIUserpwdhistoryRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIUserpwdhistoryRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
