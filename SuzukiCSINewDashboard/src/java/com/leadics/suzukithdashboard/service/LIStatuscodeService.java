
/*
 * LIStatuscodeService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.to.LIStatuscodeRecord;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LIStatuscodeDAO;
import java.sql.*;
import org.json.simple.*;
import com.leadics.utils.*;
public class LIStatuscodeService extends LIService
{
	static LogUtils logger = new LogUtils(LIStatuscodeService.class.getName());


	public LIStatuscodeRecord[] loadLIStatuscodeRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIStatuscodeRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIStatuscodeRecords", null);
			LIStatuscodeDAO dao = new LIStatuscodeDAO();
			LIStatuscodeRecord[] results = dao.loadLIStatuscodeRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIStatuscodeRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIStatuscodeRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIStatuscodeRecord loadFirstLIStatuscodeRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIStatuscodeRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIStatuscodeRecord", null);
			LIStatuscodeDAO dao = new LIStatuscodeDAO();
			LIStatuscodeRecord result = dao.loadFirstLIStatuscodeRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIStatuscodeRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIStatuscodeRecord searchFirstLIStatuscodeRecord(LIStatuscodeRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIStatuscodeRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIStatuscodeRecord", null);
			LIStatuscodeDAO dao = new LIStatuscodeDAO();
			LIStatuscodeRecord[] records = dao.searchLIStatuscodeRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIStatuscodeRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIStatuscodeRecord searchFirstLIStatuscodeRecordExactUpper(LIStatuscodeRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIStatuscodeRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIStatuscodeRecordsExactUpper", null);
			LIStatuscodeDAO dao = new LIStatuscodeDAO();
			LIStatuscodeRecord[] records = dao.searchLIStatuscodeRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIStatuscodeRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIStatuscodeRecord[] searchLIStatuscodeRecords(LIStatuscodeRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIStatuscodeRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIStatuscodeRecords", null);
			LIStatuscodeDAO dao = new LIStatuscodeDAO();
			LIStatuscodeRecord[] records = dao.searchLIStatuscodeRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIStatuscodeRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIStatuscodeRecordCount(LIStatuscodeRecord record)
	throws Exception
	{
		return loadLIStatuscodeRecordCount(record, null);
	}


	public int loadLIStatuscodeRecordCount(LIStatuscodeRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIStatuscodeRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIStatuscodeRecordCount", null);
			LIStatuscodeDAO dao = new LIStatuscodeDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIStatuscodeRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIStatuscodeRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIStatuscodeRecord loadLIStatuscodeRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIStatuscodeRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIStatuscodeRecord", null);
			LIStatuscodeDAO dao = new LIStatuscodeDAO();
			LIStatuscodeRecord result = dao.loadLIStatuscodeRecord(key);
			logger.trace("loadLIStatuscodeRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIStatuscodeRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIStatuscodeRecordSearchResultByPage(LIStatuscodeRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIStatuscodeRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIStatuscodeRecordSearchResultByPage(LIStatuscodeRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIStatuscodeRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIStatuscodeRecordSearchResult", null);
			LIStatuscodeDAO dao = new LIStatuscodeDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIStatuscodeRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIStatuscodeRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIStatuscodeRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIStatuscodeRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIStatuscodeRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIStatuscodeRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIStatuscodeRecordSearchResultByPageQuery", null);
			LIStatuscodeDAO dao = new LIStatuscodeDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIStatuscodeRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIStatuscodeRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIStatuscodeRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public boolean insertLIStatuscodeRecord(LIStatuscodeRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIStatuscodeRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIStatuscodeRecord", null);
			LIStatuscodeDAO dao = new LIStatuscodeDAO();
			boolean result = dao.insertLIStatuscodeRecord(record);
			logger.trace("insertLIStatuscodeRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIStatuscodeRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIStatuscodeRecord(LIStatuscodeRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIStatuscodeRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIStatuscodeRecord", null);
			LIStatuscodeDAO dao = new LIStatuscodeDAO();
			boolean result = dao.updateLIStatuscodeRecord(record);
			logger.trace("updateLIStatuscodeRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIStatuscodeRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIStatuscodeRecordNonNull(LIStatuscodeRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIStatuscodeRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIStatuscodeRecordNonNull", null);
			LIStatuscodeDAO dao = new LIStatuscodeDAO();
			LIStatuscodeRecord dbRecord = dao.loadLIStatuscodeRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIStatuscodeRecord(dbRecord);
			logger.trace("updateLIStatuscodeRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIStatuscodeRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIStatuscodeRecord(LIStatuscodeRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIStatuscodeRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIStatuscodeRecord", null);
			LIStatuscodeDAO dao = new LIStatuscodeDAO();
			boolean result = dao.deleteLIStatuscodeRecord(record);
			logger.trace("deleteLIStatuscodeRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIStatuscodeRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
