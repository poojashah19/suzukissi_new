
/*
 * LISsiattributefactoraggregateService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LISsiattributefactoraggregateDAO;
import com.leadics.suzukithdashboard.to.LISsiattributefactoraggregateRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LISsiattributefactoraggregateService extends LIService
{
	static LogUtils logger = new LogUtils(LISsiattributefactoraggregateService.class.getName());


	public LISsiattributefactoraggregateRecord[] loadLISsiattributefactoraggregateRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLISsiattributefactoraggregateRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsiattributefactoraggregateRecords", null);
			LISsiattributefactoraggregateDAO dao = new LISsiattributefactoraggregateDAO();
			LISsiattributefactoraggregateRecord[] results = dao.loadLISsiattributefactoraggregateRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLISsiattributefactoraggregateRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLISsiattributefactoraggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LISsiattributefactoraggregateRecord loadFirstLISsiattributefactoraggregateRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLISsiattributefactoraggregateRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLISsiattributefactoraggregateRecord", null);
			LISsiattributefactoraggregateDAO dao = new LISsiattributefactoraggregateDAO();
			LISsiattributefactoraggregateRecord result = dao.loadFirstLISsiattributefactoraggregateRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLISsiattributefactoraggregateRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISsiattributefactoraggregateRecord searchFirstLISsiattributefactoraggregateRecord(LISsiattributefactoraggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLISsiattributefactoraggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLISsiattributefactoraggregateRecord", null);
			LISsiattributefactoraggregateDAO dao = new LISsiattributefactoraggregateDAO();
			LISsiattributefactoraggregateRecord[] records = dao.searchLISsiattributefactoraggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLISsiattributefactoraggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LISsiattributefactoraggregateRecord searchFirstLISsiattributefactoraggregateRecordExactUpper(LISsiattributefactoraggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLISsiattributefactoraggregateRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLISsiattributefactoraggregateRecordsExactUpper", null);
			LISsiattributefactoraggregateDAO dao = new LISsiattributefactoraggregateDAO();
			LISsiattributefactoraggregateRecord[] records = dao.searchLISsiattributefactoraggregateRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLISsiattributefactoraggregateRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISsiattributefactoraggregateRecord[] searchLISsiattributefactoraggregateRecords(LISsiattributefactoraggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLISsiattributefactoraggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsiattributefactoraggregateRecords", null);
			LISsiattributefactoraggregateDAO dao = new LISsiattributefactoraggregateDAO();
			LISsiattributefactoraggregateRecord[] records = dao.searchLISsiattributefactoraggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLISsiattributefactoraggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLISsiattributefactoraggregateRecordCount(LISsiattributefactoraggregateRecord record)
	throws Exception
	{
		return loadLISsiattributefactoraggregateRecordCount(record, null);
	}


	public int loadLISsiattributefactoraggregateRecordCount(LISsiattributefactoraggregateRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLISsiattributefactoraggregateRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsiattributefactoraggregateRecordCount", null);
			LISsiattributefactoraggregateDAO dao = new LISsiattributefactoraggregateDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLISsiattributefactoraggregateRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLISsiattributefactoraggregateRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISsiattributefactoraggregateRecord loadLISsiattributefactoraggregateRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLISsiattributefactoraggregateRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsiattributefactoraggregateRecord", null);
			LISsiattributefactoraggregateDAO dao = new LISsiattributefactoraggregateDAO();
			LISsiattributefactoraggregateRecord result = dao.loadLISsiattributefactoraggregateRecord(key);
			logger.trace("loadLISsiattributefactoraggregateRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLISsiattributefactoraggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLISsiattributefactoraggregateRecordSearchResultByPage(LISsiattributefactoraggregateRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLISsiattributefactoraggregateRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLISsiattributefactoraggregateRecordSearchResultByPage(LISsiattributefactoraggregateRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLISsiattributefactoraggregateRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLISsiattributefactoraggregateRecordSearchResult", null);
			LISsiattributefactoraggregateDAO dao = new LISsiattributefactoraggregateDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLISsiattributefactoraggregateRecordCount(record);
			dao.setLimits(offset, maxrows);
			LISsiattributefactoraggregateRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLISsiattributefactoraggregateRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLISsiattributefactoraggregateRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLISsiattributefactoraggregateRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLISsiattributefactoraggregateRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLISsiattributefactoraggregateRecordSearchResultByPageQuery", null);
			LISsiattributefactoraggregateDAO dao = new LISsiattributefactoraggregateDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LISsiattributefactoraggregateRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLISsiattributefactoraggregateRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLISsiattributefactoraggregateRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLISsiattributefactoraggregateRecord(LISsiattributefactoraggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLISsiattributefactoraggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLISsiattributefactoraggregateRecord", null);
			LISsiattributefactoraggregateDAO dao = new LISsiattributefactoraggregateDAO();
			int result = dao.insertLISsiattributefactoraggregateRecord(record);
			logger.trace("insertLISsiattributefactoraggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLISsiattributefactoraggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLISsiattributefactoraggregateRecord(LISsiattributefactoraggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLISsiattributefactoraggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLISsiattributefactoraggregateRecord", null);
			LISsiattributefactoraggregateDAO dao = new LISsiattributefactoraggregateDAO();
			boolean result = dao.updateLISsiattributefactoraggregateRecord(record);
			logger.trace("updateLISsiattributefactoraggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLISsiattributefactoraggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLISsiattributefactoraggregateRecordNonNull(LISsiattributefactoraggregateRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLISsiattributefactoraggregateRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLISsiattributefactoraggregateRecordNonNull", null);
			LISsiattributefactoraggregateDAO dao = new LISsiattributefactoraggregateDAO();
			LISsiattributefactoraggregateRecord dbRecord = dao.loadLISsiattributefactoraggregateRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLISsiattributefactoraggregateRecord(dbRecord);
			logger.trace("updateLISsiattributefactoraggregateRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLISsiattributefactoraggregateRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLISsiattributefactoraggregateRecord(LISsiattributefactoraggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLISsiattributefactoraggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLISsiattributefactoraggregateRecord", null);
			LISsiattributefactoraggregateDAO dao = new LISsiattributefactoraggregateDAO();
			boolean result = dao.deleteLISsiattributefactoraggregateRecord(record);
			logger.trace("deleteLISsiattributefactoraggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLISsiattributefactoraggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
