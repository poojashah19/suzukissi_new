
/*
 * LISsicalenderfilterService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LISsicalenderfilterDAO;
import com.leadics.suzukithdashboard.to.LISsicalenderfilterRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LISsicalenderfilterService extends LIService
{
	static LogUtils logger = new LogUtils(LISsicalenderfilterService.class.getName());


	public LISsicalenderfilterRecord[] loadLISsicalenderfilterRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLISsicalenderfilterRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsicalenderfilterRecords", null);
			LISsicalenderfilterDAO dao = new LISsicalenderfilterDAO();
			LISsicalenderfilterRecord[] results = dao.loadLISsicalenderfilterRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLISsicalenderfilterRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLISsicalenderfilterRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LISsicalenderfilterRecord loadFirstLISsicalenderfilterRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLISsicalenderfilterRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLISsicalenderfilterRecord", null);
			LISsicalenderfilterDAO dao = new LISsicalenderfilterDAO();
			LISsicalenderfilterRecord result = dao.loadFirstLISsicalenderfilterRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLISsicalenderfilterRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISsicalenderfilterRecord searchFirstLISsicalenderfilterRecord(LISsicalenderfilterRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLISsicalenderfilterRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLISsicalenderfilterRecord", null);
			LISsicalenderfilterDAO dao = new LISsicalenderfilterDAO();
			LISsicalenderfilterRecord[] records = dao.searchLISsicalenderfilterRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLISsicalenderfilterRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LISsicalenderfilterRecord searchFirstLISsicalenderfilterRecordExactUpper(LISsicalenderfilterRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLISsicalenderfilterRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLISsicalenderfilterRecordsExactUpper", null);
			LISsicalenderfilterDAO dao = new LISsicalenderfilterDAO();
			LISsicalenderfilterRecord[] records = dao.searchLISsicalenderfilterRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLISsicalenderfilterRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISsicalenderfilterRecord[] searchLISsicalenderfilterRecords(LISsicalenderfilterRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLISsicalenderfilterRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsicalenderfilterRecords", null);
			LISsicalenderfilterDAO dao = new LISsicalenderfilterDAO();
			LISsicalenderfilterRecord[] records = dao.searchLISsicalenderfilterRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLISsicalenderfilterRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLISsicalenderfilterRecordCount(LISsicalenderfilterRecord record)
	throws Exception
	{
		return loadLISsicalenderfilterRecordCount(record, null);
	}


	public int loadLISsicalenderfilterRecordCount(LISsicalenderfilterRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLISsicalenderfilterRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsicalenderfilterRecordCount", null);
			LISsicalenderfilterDAO dao = new LISsicalenderfilterDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLISsicalenderfilterRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLISsicalenderfilterRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISsicalenderfilterRecord loadLISsicalenderfilterRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLISsicalenderfilterRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsicalenderfilterRecord", null);
			LISsicalenderfilterDAO dao = new LISsicalenderfilterDAO();
			LISsicalenderfilterRecord result = dao.loadLISsicalenderfilterRecord(key);
			logger.trace("loadLISsicalenderfilterRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLISsicalenderfilterRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLISsicalenderfilterRecordSearchResultByPage(LISsicalenderfilterRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLISsicalenderfilterRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLISsicalenderfilterRecordSearchResultByPage(LISsicalenderfilterRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLISsicalenderfilterRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLISsicalenderfilterRecordSearchResult", null);
			LISsicalenderfilterDAO dao = new LISsicalenderfilterDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLISsicalenderfilterRecordCount(record);
			dao.setLimits(offset, maxrows);
			LISsicalenderfilterRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLISsicalenderfilterRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLISsicalenderfilterRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLISsicalenderfilterRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLISsicalenderfilterRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLISsicalenderfilterRecordSearchResultByPageQuery", null);
			LISsicalenderfilterDAO dao = new LISsicalenderfilterDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LISsicalenderfilterRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLISsicalenderfilterRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLISsicalenderfilterRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLISsicalenderfilterRecord(LISsicalenderfilterRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLISsicalenderfilterRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLISsicalenderfilterRecord", null);
			LISsicalenderfilterDAO dao = new LISsicalenderfilterDAO();
			int result = dao.insertLISsicalenderfilterRecord(record);
			logger.trace("insertLISsicalenderfilterRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLISsicalenderfilterRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLISsicalenderfilterRecord(LISsicalenderfilterRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLISsicalenderfilterRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLISsicalenderfilterRecord", null);
			LISsicalenderfilterDAO dao = new LISsicalenderfilterDAO();
			boolean result = dao.updateLISsicalenderfilterRecord(record);
			logger.trace("updateLISsicalenderfilterRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLISsicalenderfilterRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLISsicalenderfilterRecordNonNull(LISsicalenderfilterRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLISsicalenderfilterRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLISsicalenderfilterRecordNonNull", null);
			LISsicalenderfilterDAO dao = new LISsicalenderfilterDAO();
			LISsicalenderfilterRecord dbRecord = dao.loadLISsicalenderfilterRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLISsicalenderfilterRecord(dbRecord);
			logger.trace("updateLISsicalenderfilterRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLISsicalenderfilterRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLISsicalenderfilterRecord(LISsicalenderfilterRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLISsicalenderfilterRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLISsicalenderfilterRecord", null);
			LISsicalenderfilterDAO dao = new LISsicalenderfilterDAO();
			boolean result = dao.deleteLISsicalenderfilterRecord(record);
			logger.trace("deleteLISsicalenderfilterRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLISsicalenderfilterRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
