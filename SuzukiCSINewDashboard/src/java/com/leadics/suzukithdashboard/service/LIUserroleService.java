
/*
 * LIUserroleService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LIUserroleDAO;
import com.leadics.suzukithdashboard.to.LIUserroleRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIUserroleService extends LIService
{
	static LogUtils logger = new LogUtils(LIUserroleService.class.getName());


	public LIUserroleRecord[] loadLIUserroleRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIUserroleRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIUserroleRecords", null);
			LIUserroleDAO dao = new LIUserroleDAO();
			LIUserroleRecord[] results = dao.loadLIUserroleRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIUserroleRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIUserroleRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIUserroleRecord loadFirstLIUserroleRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIUserroleRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIUserroleRecord", null);
			LIUserroleDAO dao = new LIUserroleDAO();
			LIUserroleRecord result = dao.loadFirstLIUserroleRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIUserroleRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIUserroleRecord searchFirstLIUserroleRecord(LIUserroleRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIUserroleRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIUserroleRecord", null);
			LIUserroleDAO dao = new LIUserroleDAO();
			LIUserroleRecord[] records = dao.searchLIUserroleRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIUserroleRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIUserroleRecord searchFirstLIUserroleRecordExactUpper(LIUserroleRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIUserroleRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIUserroleRecordsExactUpper", null);
			LIUserroleDAO dao = new LIUserroleDAO();
			LIUserroleRecord[] records = dao.searchLIUserroleRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIUserroleRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIUserroleRecord[] searchLIUserroleRecords(LIUserroleRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIUserroleRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIUserroleRecords", null);
			LIUserroleDAO dao = new LIUserroleDAO();
			LIUserroleRecord[] records = dao.searchLIUserroleRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIUserroleRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIUserroleRecordCount(LIUserroleRecord record)
	throws Exception
	{
		return loadLIUserroleRecordCount(record, null);
	}


	public int loadLIUserroleRecordCount(LIUserroleRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIUserroleRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIUserroleRecordCount", null);
			LIUserroleDAO dao = new LIUserroleDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIUserroleRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIUserroleRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIUserroleRecord loadLIUserroleRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIUserroleRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIUserroleRecord", null);
			LIUserroleDAO dao = new LIUserroleDAO();
			LIUserroleRecord result = dao.loadLIUserroleRecord(key);
			logger.trace("loadLIUserroleRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIUserroleRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIUserroleRecordSearchResultByPage(LIUserroleRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIUserroleRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIUserroleRecordSearchResultByPage(LIUserroleRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIUserroleRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIUserroleRecordSearchResult", null);
			LIUserroleDAO dao = new LIUserroleDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIUserroleRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIUserroleRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIUserroleRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIUserroleRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIUserroleRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIUserroleRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIUserroleRecordSearchResultByPageQuery", null);
			LIUserroleDAO dao = new LIUserroleDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIUserroleRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIUserroleRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIUserroleRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIUserroleRecord(LIUserroleRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIUserroleRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIUserroleRecord", null);
			LIUserroleDAO dao = new LIUserroleDAO();
			int result = dao.insertLIUserroleRecord(record);
			logger.trace("insertLIUserroleRecord:Result:" + result);			
			createMakerCheckerAuditEntry("user_role",result + "", "Create",record.getCreatedby(),"Created");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIUserroleRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIUserroleRecord(LIUserroleRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIUserroleRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIUserroleRecord", null);
			LIUserroleDAO dao = new LIUserroleDAO();
			boolean result = dao.updateLIUserroleRecord(record);
			createMakerCheckerAuditEntry("user_role",record.getId(), "Update",record.getModifiedby(),"Update");
			logger.trace("updateLIUserroleRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIUserroleRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIUserroleRecordNonNull(LIUserroleRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIUserroleRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIUserroleRecordNonNull", null);
			LIUserroleDAO dao = new LIUserroleDAO();
			LIUserroleRecord dbRecord = dao.loadLIUserroleRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIUserroleRecord(dbRecord);
			createMakerCheckerAuditEntry("user_role",inputRecord.getId(), "Update",dbRecord.getModifiedby(),"Update");
			logger.trace("updateLIUserroleRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIUserroleRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIUserroleRecord(LIUserroleRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIUserroleRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIUserroleRecord", null);
			LIUserroleDAO dao = new LIUserroleDAO();
			boolean result = dao.deleteLIUserroleRecord(record);
			logger.trace("deleteLIUserroleRecord:Result:" + result);			
			createMakerCheckerAuditEntry("user_role",record.getId(), "Delete",null,"Deleted");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIUserroleRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean approveLIUserroleRecord(LIUserroleRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("approveLIUserroleRecord:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("approveLIUserroleRecord", null);
			LIUserroleDAO dao = new LIUserroleDAO();
			LIUserroleRecord updateRecord = dao.loadLIUserroleRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CA-RNFA104:Record not found");
			}
			if (!isValidStatusForApproval(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CA-IS101:Cannot approve - Invalid Status for Approval");
			}
			if (StringUtils.isSame(updateRecord.getMadeby(), updateId))
			{
				throw new Exception("MF-ERR-CA-MCS102:Cannot Approve. Maker Checker cannot be same");
			}
			updateRecord.setRstatus("1");
			updateRecord.setCheckerlastcmt(comment);
			updateRecord.setCurrappstatus("1");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setCheckedat(DateUtils.getCurrentDateTime());
			updateRecord.setCheckedby(updateId);
			boolean result = dao.updateLIUserroleRecord(updateRecord);
			logger.trace("approveLIUserroleRecord:Result:" + result);			
			createMakerCheckerAuditEntry("user_role",updateRecord.getId(), "Approve",updateId,comment);
			HashMap approveEventMap = callActionEvent("Event_OnApprovalOf_LIUserroleRecord", updateRecord.getId());
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("approveLIUserroleRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean submitLIUserroleRecord(LIUserroleRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("submitLIUserroleRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("submitLIUserroleRecordNonNull", null);
			LIUserroleDAO dao = new LIUserroleDAO();
			LIUserroleRecord updateRecord = dao.loadLIUserroleRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CS-RNFA104:Record not found");
			}
			if (!isValidStatusForSubmission(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CS-IS101:Cannot submit - Invalid Status for Submission");
			}
			updateRecord.setRstatus("1");
			updateRecord.setMakerlastcmt(comment);
			updateRecord.setCurrappstatus("4");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setMadeat(DateUtils.getCurrentDateTime());
			updateRecord.setMadeby(updateId);
			boolean result = dao.updateLIUserroleRecord(updateRecord);
			createMakerCheckerAuditEntry("user_role",updateRecord.getId(),"Submit",updateId,comment);
			HashMap submitEventMap = callActionEvent("Event_OnSubmitOf_LIUserroleRecord", updateRecord.getId());
			logger.trace("submitLIUserroleRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("submitLIUserroleRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean denyLIUserroleRecord(LIUserroleRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("denyLIUserroleRecord:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("denyLIUserroleRecord", null);
			LIUserroleDAO dao = new LIUserroleDAO();
			LIUserroleRecord updateRecord = dao.loadLIUserroleRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CD-RNFA104:Record not found");
			}
			if (!isValidStatusForDeny(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CD-IS101:Cannot deny - Invalid Status for Deny");
			}
			if (StringUtils.isSame(updateRecord.getMadeby(), updateId))
			{
				throw new Exception("MF-ERR-CD-MCS101:Cannot Deny. Maker Checker cannot be same");
			}
			updateRecord.setCheckerlastcmt(comment);
			updateRecord.setCurrappstatus("5");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setCheckedat(DateUtils.getCurrentDateTime());
			updateRecord.setCheckedby(updateId);
			boolean result = dao.updateLIUserroleRecord(updateRecord);
			createMakerCheckerAuditEntry("user_role",updateRecord.getId(),"Deny",updateId,comment);
			HashMap denyEventMap = callActionEvent("Event_OnDenyOf_LIUserroleRecord", updateRecord.getId());
			logger.trace("denyLIUserroleRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("denyLIUserroleRecord" + getStackTrace(exception));
			throw exception;
		}
	}
	public boolean denyPermanantlyLIUserroleRecord(LIUserroleRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("denyPermanantlyLIUserroleRecord:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("denyPermanantlyLIUserroleRecord", null);
			LIUserroleDAO dao = new LIUserroleDAO();
			LIUserroleRecord updateRecord = dao.loadLIUserroleRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CD-RNFADP104:Record not found");
			}
			if (!isValidStatusForDeny(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CD-ISDP101:Cannot deny - Invalid Status for Deny");
			}
			if (StringUtils.isSame(updateRecord.getMadeby(), updateId))
			{
				throw new Exception("MF-ERR-CD-MCS101DP:Cannot Deny. Maker Checker cannot be same");
			}
			updateRecord.setCheckerlastcmt(comment);
			updateRecord.setCurrappstatus("58");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setCheckedat(DateUtils.getCurrentDateTime());
			updateRecord.setCheckedby(updateId);
			boolean result = dao.updateLIUserroleRecord(updateRecord);
			createMakerCheckerAuditEntry("user_role",updateRecord.getId(),"DenyP",updateId,comment);
			HashMap denyEventMap = callActionEvent("Event_OnDenyPOf_LIUserroleRecord", updateRecord.getId());
			logger.trace("denyPLIUserroleRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("denyPLIUserroleRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean remindApprovalLIUserroleRecord(LIUserroleRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("remindApprovalLIUserroleRecord:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("remindApprovalLIUserroleRecord", null);
			LIUserroleDAO dao = new LIUserroleDAO();
			LIUserroleRecord updateRecord = dao.loadLIUserroleRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CR-RNFA104:Record not found");
			}
			if (!isValidStatusForApproval(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CR-IS101:Cannot Remind - Invalid Status for Reminder");
			}
			createMakerCheckerAuditEntry("user_role",updateRecord.getId(), "Approval Reminder",updateId,comment);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return true;
		}
		catch(Exception exception)
		{
			logger.error("remindApprovalLIUserroleRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean resetApprovalLIUserroleRecord(LIUserroleRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("resetApprovalLIUserroleRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("resetApprovalLIUserroleRecordNonNull", null);
			LIUserroleDAO dao = new LIUserroleDAO();
			LIUserroleRecord updateRecord = dao.loadLIUserroleRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-RA-RNFA104:Record not found");
			}
			if (!isValidStatusForReset(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-RA-IS101:Cannot submit - Invalid Status for Reset");
			}
			updateRecord.setRstatus("0");
			updateRecord.setAdminlastcmt(comment);
			updateRecord.setCurrappstatus("0");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setMadeat(DateUtils.getCurrentDateTime());
			updateRecord.setMadeby(updateId);
			boolean result = dao.updateLIUserroleRecord(updateRecord);
			createMakerCheckerAuditEntry("user_role",updateRecord.getId(),"Reset Approval",updateId,comment);
			logger.trace("resetApprovalLIUserroleRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("resetApprovalLIUserroleRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
