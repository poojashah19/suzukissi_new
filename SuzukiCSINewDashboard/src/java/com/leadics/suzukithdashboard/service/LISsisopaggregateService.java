
/*
 * LISsisopaggregateService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LISsisopaggregateDAO;
import com.leadics.suzukithdashboard.to.LISsisopaggregateRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LISsisopaggregateService extends LIService
{
	static LogUtils logger = new LogUtils(LISsisopaggregateService.class.getName());


	public LISsisopaggregateRecord[] loadLISsisopaggregateRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLISsisopaggregateRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsisopaggregateRecords", null);
			LISsisopaggregateDAO dao = new LISsisopaggregateDAO();
			LISsisopaggregateRecord[] results = dao.loadLISsisopaggregateRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLISsisopaggregateRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLISsisopaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LISsisopaggregateRecord loadFirstLISsisopaggregateRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLISsisopaggregateRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLISsisopaggregateRecord", null);
			LISsisopaggregateDAO dao = new LISsisopaggregateDAO();
			LISsisopaggregateRecord result = dao.loadFirstLISsisopaggregateRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLISsisopaggregateRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISsisopaggregateRecord searchFirstLISsisopaggregateRecord(LISsisopaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLISsisopaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLISsisopaggregateRecord", null);
			LISsisopaggregateDAO dao = new LISsisopaggregateDAO();
			LISsisopaggregateRecord[] records = dao.searchLISsisopaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLISsisopaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LISsisopaggregateRecord searchFirstLISsisopaggregateRecordExactUpper(LISsisopaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLISsisopaggregateRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLISsisopaggregateRecordsExactUpper", null);
			LISsisopaggregateDAO dao = new LISsisopaggregateDAO();
			LISsisopaggregateRecord[] records = dao.searchLISsisopaggregateRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLISsisopaggregateRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISsisopaggregateRecord[] searchLISsisopaggregateRecords(LISsisopaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLISsisopaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsisopaggregateRecords", null);
			LISsisopaggregateDAO dao = new LISsisopaggregateDAO();
			LISsisopaggregateRecord[] records = dao.searchLISsisopaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLISsisopaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLISsisopaggregateRecordCount(LISsisopaggregateRecord record)
	throws Exception
	{
		return loadLISsisopaggregateRecordCount(record, null);
	}


	public int loadLISsisopaggregateRecordCount(LISsisopaggregateRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLISsisopaggregateRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsisopaggregateRecordCount", null);
			LISsisopaggregateDAO dao = new LISsisopaggregateDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLISsisopaggregateRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLISsisopaggregateRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISsisopaggregateRecord loadLISsisopaggregateRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLISsisopaggregateRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsisopaggregateRecord", null);
			LISsisopaggregateDAO dao = new LISsisopaggregateDAO();
			LISsisopaggregateRecord result = dao.loadLISsisopaggregateRecord(key);
			logger.trace("loadLISsisopaggregateRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLISsisopaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLISsisopaggregateRecordSearchResultByPage(LISsisopaggregateRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLISsisopaggregateRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLISsisopaggregateRecordSearchResultByPage(LISsisopaggregateRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLISsisopaggregateRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLISsisopaggregateRecordSearchResult", null);
			LISsisopaggregateDAO dao = new LISsisopaggregateDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLISsisopaggregateRecordCount(record);
			dao.setLimits(offset, maxrows);
			LISsisopaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLISsisopaggregateRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLISsisopaggregateRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLISsisopaggregateRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLISsisopaggregateRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLISsisopaggregateRecordSearchResultByPageQuery", null);
			LISsisopaggregateDAO dao = new LISsisopaggregateDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LISsisopaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLISsisopaggregateRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLISsisopaggregateRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLISsisopaggregateRecord(LISsisopaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLISsisopaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLISsisopaggregateRecord", null);
			LISsisopaggregateDAO dao = new LISsisopaggregateDAO();
			int result = dao.insertLISsisopaggregateRecord(record);
			logger.trace("insertLISsisopaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLISsisopaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLISsisopaggregateRecord(LISsisopaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLISsisopaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLISsisopaggregateRecord", null);
			LISsisopaggregateDAO dao = new LISsisopaggregateDAO();
			boolean result = dao.updateLISsisopaggregateRecord(record);
			logger.trace("updateLISsisopaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLISsisopaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLISsisopaggregateRecordNonNull(LISsisopaggregateRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLISsisopaggregateRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLISsisopaggregateRecordNonNull", null);
			LISsisopaggregateDAO dao = new LISsisopaggregateDAO();
			LISsisopaggregateRecord dbRecord = dao.loadLISsisopaggregateRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLISsisopaggregateRecord(dbRecord);
			logger.trace("updateLISsisopaggregateRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLISsisopaggregateRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLISsisopaggregateRecord(LISsisopaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLISsisopaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLISsisopaggregateRecord", null);
			LISsisopaggregateDAO dao = new LISsisopaggregateDAO();
			boolean result = dao.deleteLISsisopaggregateRecord(record);
			logger.trace("deleteLISsisopaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLISsisopaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
