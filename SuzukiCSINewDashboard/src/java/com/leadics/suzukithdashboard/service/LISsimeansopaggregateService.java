
/*
 * LISsimeansopaggregateService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LISsimeansopaggregateDAO;
import com.leadics.suzukithdashboard.to.LISsimeansopaggregateRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LISsimeansopaggregateService extends LIService
{
	static LogUtils logger = new LogUtils(LISsimeansopaggregateService.class.getName());


	public LISsimeansopaggregateRecord[] loadLISsimeansopaggregateRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLISsimeansopaggregateRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsimeansopaggregateRecords", null);
			LISsimeansopaggregateDAO dao = new LISsimeansopaggregateDAO();
			LISsimeansopaggregateRecord[] results = dao.loadLISsimeansopaggregateRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLISsimeansopaggregateRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLISsimeansopaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LISsimeansopaggregateRecord loadFirstLISsimeansopaggregateRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLISsimeansopaggregateRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLISsimeansopaggregateRecord", null);
			LISsimeansopaggregateDAO dao = new LISsimeansopaggregateDAO();
			LISsimeansopaggregateRecord result = dao.loadFirstLISsimeansopaggregateRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLISsimeansopaggregateRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISsimeansopaggregateRecord searchFirstLISsimeansopaggregateRecord(LISsimeansopaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLISsimeansopaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLISsimeansopaggregateRecord", null);
			LISsimeansopaggregateDAO dao = new LISsimeansopaggregateDAO();
			LISsimeansopaggregateRecord[] records = dao.searchLISsimeansopaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLISsimeansopaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LISsimeansopaggregateRecord searchFirstLISsimeansopaggregateRecordExactUpper(LISsimeansopaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLISsimeansopaggregateRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLISsimeansopaggregateRecordsExactUpper", null);
			LISsimeansopaggregateDAO dao = new LISsimeansopaggregateDAO();
			LISsimeansopaggregateRecord[] records = dao.searchLISsimeansopaggregateRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLISsimeansopaggregateRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISsimeansopaggregateRecord[] searchLISsimeansopaggregateRecords(LISsimeansopaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLISsimeansopaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsimeansopaggregateRecords", null);
			LISsimeansopaggregateDAO dao = new LISsimeansopaggregateDAO();
			LISsimeansopaggregateRecord[] records = dao.searchLISsimeansopaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLISsimeansopaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLISsimeansopaggregateRecordCount(LISsimeansopaggregateRecord record)
	throws Exception
	{
		return loadLISsimeansopaggregateRecordCount(record, null);
	}


	public int loadLISsimeansopaggregateRecordCount(LISsimeansopaggregateRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLISsimeansopaggregateRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsimeansopaggregateRecordCount", null);
			LISsimeansopaggregateDAO dao = new LISsimeansopaggregateDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLISsimeansopaggregateRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLISsimeansopaggregateRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISsimeansopaggregateRecord loadLISsimeansopaggregateRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLISsimeansopaggregateRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsimeansopaggregateRecord", null);
			LISsimeansopaggregateDAO dao = new LISsimeansopaggregateDAO();
			LISsimeansopaggregateRecord result = dao.loadLISsimeansopaggregateRecord(key);
			logger.trace("loadLISsimeansopaggregateRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLISsimeansopaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLISsimeansopaggregateRecordSearchResultByPage(LISsimeansopaggregateRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLISsimeansopaggregateRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLISsimeansopaggregateRecordSearchResultByPage(LISsimeansopaggregateRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLISsimeansopaggregateRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLISsimeansopaggregateRecordSearchResult", null);
			LISsimeansopaggregateDAO dao = new LISsimeansopaggregateDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLISsimeansopaggregateRecordCount(record);
			dao.setLimits(offset, maxrows);
			LISsimeansopaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLISsimeansopaggregateRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLISsimeansopaggregateRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLISsimeansopaggregateRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLISsimeansopaggregateRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLISsimeansopaggregateRecordSearchResultByPageQuery", null);
			LISsimeansopaggregateDAO dao = new LISsimeansopaggregateDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LISsimeansopaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLISsimeansopaggregateRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLISsimeansopaggregateRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLISsimeansopaggregateRecord(LISsimeansopaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLISsimeansopaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLISsimeansopaggregateRecord", null);
			LISsimeansopaggregateDAO dao = new LISsimeansopaggregateDAO();
			int result = dao.insertLISsimeansopaggregateRecord(record);
			logger.trace("insertLISsimeansopaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLISsimeansopaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLISsimeansopaggregateRecord(LISsimeansopaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLISsimeansopaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLISsimeansopaggregateRecord", null);
			LISsimeansopaggregateDAO dao = new LISsimeansopaggregateDAO();
			boolean result = dao.updateLISsimeansopaggregateRecord(record);
			logger.trace("updateLISsimeansopaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLISsimeansopaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLISsimeansopaggregateRecordNonNull(LISsimeansopaggregateRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLISsimeansopaggregateRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLISsimeansopaggregateRecordNonNull", null);
			LISsimeansopaggregateDAO dao = new LISsimeansopaggregateDAO();
			LISsimeansopaggregateRecord dbRecord = dao.loadLISsimeansopaggregateRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLISsimeansopaggregateRecord(dbRecord);
			logger.trace("updateLISsimeansopaggregateRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLISsimeansopaggregateRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLISsimeansopaggregateRecord(LISsimeansopaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLISsimeansopaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLISsimeansopaggregateRecord", null);
			LISsimeansopaggregateDAO dao = new LISsimeansopaggregateDAO();
			boolean result = dao.deleteLISsimeansopaggregateRecord(record);
			logger.trace("deleteLISsimeansopaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLISsimeansopaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
