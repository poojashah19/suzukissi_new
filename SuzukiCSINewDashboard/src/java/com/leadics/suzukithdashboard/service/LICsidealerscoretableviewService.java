
/*
 * LICsidealerscoretableviewService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LICsidealerscoretableviewDAO;
import com.leadics.suzukithdashboard.to.LICsidealerscoretableviewRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LICsidealerscoretableviewService extends LIService 
{
	static LogUtils logger = new LogUtils(LICsidealerscoretableviewService.class.getName());

//        public String getDealername()
//	{
//		if (isNoNullForHTMLEnabled())
//		{
//			return StringUtils.noNullForHTML(dealer);
//		}
//		else if (isNoNullEnabled())
//		{
//			return StringUtils.noNull(dealer);
//		}
//		else
//		{
//			return dealer;
//		}
//	}
        
	public LICsidealerscoretableviewRecord[] loadLICsidealerscoretableviewRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLICsidealerscoretableviewRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsidealerscoretableviewRecords", null);
			LICsidealerscoretableviewDAO dao = new LICsidealerscoretableviewDAO();
			LICsidealerscoretableviewRecord[] results = dao.loadLICsidealerscoretableviewRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLICsidealerscoretableviewRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLICsidealerscoretableviewRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LICsidealerscoretableviewRecord loadFirstLICsidealerscoretableviewRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLICsidealerscoretableviewRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLICsidealerscoretableviewRecord", null);
			LICsidealerscoretableviewDAO dao = new LICsidealerscoretableviewDAO();
			LICsidealerscoretableviewRecord result = dao.loadFirstLICsidealerscoretableviewRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLICsidealerscoretableviewRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICsidealerscoretableviewRecord searchFirstLICsidealerscoretableviewRecord(LICsidealerscoretableviewRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLICsidealerscoretableviewRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLICsidealerscoretableviewRecord", null);
			LICsidealerscoretableviewDAO dao = new LICsidealerscoretableviewDAO();
			LICsidealerscoretableviewRecord[] records = dao.searchLICsidealerscoretableviewRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLICsidealerscoretableviewRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LICsidealerscoretableviewRecord searchFirstLICsidealerscoretableviewRecordExactUpper(LICsidealerscoretableviewRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLICsidealerscoretableviewRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLICsidealerscoretableviewRecordsExactUpper", null);
			LICsidealerscoretableviewDAO dao = new LICsidealerscoretableviewDAO();
			LICsidealerscoretableviewRecord[] records = dao.searchLICsidealerscoretableviewRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLICsidealerscoretableviewRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICsidealerscoretableviewRecord[] searchLICsidealerscoretableviewRecords(LICsidealerscoretableviewRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLICsidealerscoretableviewRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsidealerscoretableviewRecords", null);
			LICsidealerscoretableviewDAO dao = new LICsidealerscoretableviewDAO();
			LICsidealerscoretableviewRecord[] records = dao.searchLICsidealerscoretableviewRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLICsidealerscoretableviewRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLICsidealerscoretableviewRecordCount(LICsidealerscoretableviewRecord record)
	throws Exception
	{
		return loadLICsidealerscoretableviewRecordCount(record, null);
	}


	public int loadLICsidealerscoretableviewRecordCount(LICsidealerscoretableviewRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLICsidealerscoretableviewRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsidealerscoretableviewRecordCount", null);
			LICsidealerscoretableviewDAO dao = new LICsidealerscoretableviewDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLICsidealerscoretableviewRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLICsidealerscoretableviewRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICsidealerscoretableviewRecord loadLICsidealerscoretableviewRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLICsidealerscoretableviewRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsidealerscoretableviewRecord", null);
			LICsidealerscoretableviewDAO dao = new LICsidealerscoretableviewDAO();
			LICsidealerscoretableviewRecord result = dao.loadLICsidealerscoretableviewRecord(key);
			logger.trace("loadLICsidealerscoretableviewRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLICsidealerscoretableviewRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLICsidealerscoretableviewRecordSearchResultByPage(LICsidealerscoretableviewRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLICsidealerscoretableviewRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLICsidealerscoretableviewRecordSearchResultByPage(LICsidealerscoretableviewRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLICsidealerscoretableviewRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLICsidealerscoretableviewRecordSearchResult", null);
			LICsidealerscoretableviewDAO dao = new LICsidealerscoretableviewDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLICsidealerscoretableviewRecordCount(record);
			dao.setLimits(offset, maxrows);
			LICsidealerscoretableviewRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLICsidealerscoretableviewRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLICsidealerscoretableviewRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLICsidealerscoretableviewRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLICsidealerscoretableviewRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLICsidealerscoretableviewRecordSearchResultByPageQuery", null);
			LICsidealerscoretableviewDAO dao = new LICsidealerscoretableviewDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LICsidealerscoretableviewRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLICsidealerscoretableviewRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLICsidealerscoretableviewRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
}
