
/*
 * LICsiloyaltyandadvocacyaggregateService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LICsiloyaltyandadvocacyaggregateDAO;
import com.leadics.suzukithdashboard.to.LICsiloyaltyandadvocacyaggregateRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LICsiloyaltyandadvocacyaggregateService extends LIService
{
	static LogUtils logger = new LogUtils(LICsiloyaltyandadvocacyaggregateService.class.getName());


	public LICsiloyaltyandadvocacyaggregateRecord[] loadLICsiloyaltyandadvocacyaggregateRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLICsiloyaltyandadvocacyaggregateRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsiloyaltyandadvocacyaggregateRecords", null);
			LICsiloyaltyandadvocacyaggregateDAO dao = new LICsiloyaltyandadvocacyaggregateDAO();
			LICsiloyaltyandadvocacyaggregateRecord[] results = dao.loadLICsiloyaltyandadvocacyaggregateRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLICsiloyaltyandadvocacyaggregateRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLICsiloyaltyandadvocacyaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LICsiloyaltyandadvocacyaggregateRecord loadFirstLICsiloyaltyandadvocacyaggregateRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLICsiloyaltyandadvocacyaggregateRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLICsiloyaltyandadvocacyaggregateRecord", null);
			LICsiloyaltyandadvocacyaggregateDAO dao = new LICsiloyaltyandadvocacyaggregateDAO();
			LICsiloyaltyandadvocacyaggregateRecord result = dao.loadFirstLICsiloyaltyandadvocacyaggregateRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLICsiloyaltyandadvocacyaggregateRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICsiloyaltyandadvocacyaggregateRecord searchFirstLICsiloyaltyandadvocacyaggregateRecord(LICsiloyaltyandadvocacyaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLICsiloyaltyandadvocacyaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLICsiloyaltyandadvocacyaggregateRecord", null);
			LICsiloyaltyandadvocacyaggregateDAO dao = new LICsiloyaltyandadvocacyaggregateDAO();
			LICsiloyaltyandadvocacyaggregateRecord[] records = dao.searchLICsiloyaltyandadvocacyaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLICsiloyaltyandadvocacyaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LICsiloyaltyandadvocacyaggregateRecord searchFirstLICsiloyaltyandadvocacyaggregateRecordExactUpper(LICsiloyaltyandadvocacyaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLICsiloyaltyandadvocacyaggregateRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLICsiloyaltyandadvocacyaggregateRecordsExactUpper", null);
			LICsiloyaltyandadvocacyaggregateDAO dao = new LICsiloyaltyandadvocacyaggregateDAO();
			LICsiloyaltyandadvocacyaggregateRecord[] records = dao.searchLICsiloyaltyandadvocacyaggregateRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLICsiloyaltyandadvocacyaggregateRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICsiloyaltyandadvocacyaggregateRecord[] searchLICsiloyaltyandadvocacyaggregateRecords(LICsiloyaltyandadvocacyaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLICsiloyaltyandadvocacyaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsiloyaltyandadvocacyaggregateRecords", null);
			LICsiloyaltyandadvocacyaggregateDAO dao = new LICsiloyaltyandadvocacyaggregateDAO();
			LICsiloyaltyandadvocacyaggregateRecord[] records = dao.searchLICsiloyaltyandadvocacyaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLICsiloyaltyandadvocacyaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLICsiloyaltyandadvocacyaggregateRecordCount(LICsiloyaltyandadvocacyaggregateRecord record)
	throws Exception
	{
		return loadLICsiloyaltyandadvocacyaggregateRecordCount(record, null);
	}


	public int loadLICsiloyaltyandadvocacyaggregateRecordCount(LICsiloyaltyandadvocacyaggregateRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLICsiloyaltyandadvocacyaggregateRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsiloyaltyandadvocacyaggregateRecordCount", null);
			LICsiloyaltyandadvocacyaggregateDAO dao = new LICsiloyaltyandadvocacyaggregateDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLICsiloyaltyandadvocacyaggregateRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLICsiloyaltyandadvocacyaggregateRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICsiloyaltyandadvocacyaggregateRecord loadLICsiloyaltyandadvocacyaggregateRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLICsiloyaltyandadvocacyaggregateRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsiloyaltyandadvocacyaggregateRecord", null);
			LICsiloyaltyandadvocacyaggregateDAO dao = new LICsiloyaltyandadvocacyaggregateDAO();
			LICsiloyaltyandadvocacyaggregateRecord result = dao.loadLICsiloyaltyandadvocacyaggregateRecord(key);
			logger.trace("loadLICsiloyaltyandadvocacyaggregateRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLICsiloyaltyandadvocacyaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLICsiloyaltyandadvocacyaggregateRecordSearchResultByPage(LICsiloyaltyandadvocacyaggregateRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLICsiloyaltyandadvocacyaggregateRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLICsiloyaltyandadvocacyaggregateRecordSearchResultByPage(LICsiloyaltyandadvocacyaggregateRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLICsiloyaltyandadvocacyaggregateRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLICsiloyaltyandadvocacyaggregateRecordSearchResult", null);
			LICsiloyaltyandadvocacyaggregateDAO dao = new LICsiloyaltyandadvocacyaggregateDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLICsiloyaltyandadvocacyaggregateRecordCount(record);
			dao.setLimits(offset, maxrows);
			LICsiloyaltyandadvocacyaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLICsiloyaltyandadvocacyaggregateRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLICsiloyaltyandadvocacyaggregateRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLICsiloyaltyandadvocacyaggregateRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLICsiloyaltyandadvocacyaggregateRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLICsiloyaltyandadvocacyaggregateRecordSearchResultByPageQuery", null);
			LICsiloyaltyandadvocacyaggregateDAO dao = new LICsiloyaltyandadvocacyaggregateDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LICsiloyaltyandadvocacyaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLICsiloyaltyandadvocacyaggregateRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLICsiloyaltyandadvocacyaggregateRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLICsiloyaltyandadvocacyaggregateRecord(LICsiloyaltyandadvocacyaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLICsiloyaltyandadvocacyaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLICsiloyaltyandadvocacyaggregateRecord", null);
			LICsiloyaltyandadvocacyaggregateDAO dao = new LICsiloyaltyandadvocacyaggregateDAO();
			int result = dao.insertLICsiloyaltyandadvocacyaggregateRecord(record);
			logger.trace("insertLICsiloyaltyandadvocacyaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLICsiloyaltyandadvocacyaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLICsiloyaltyandadvocacyaggregateRecord(LICsiloyaltyandadvocacyaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLICsiloyaltyandadvocacyaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLICsiloyaltyandadvocacyaggregateRecord", null);
			LICsiloyaltyandadvocacyaggregateDAO dao = new LICsiloyaltyandadvocacyaggregateDAO();
			boolean result = dao.updateLICsiloyaltyandadvocacyaggregateRecord(record);
			logger.trace("updateLICsiloyaltyandadvocacyaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLICsiloyaltyandadvocacyaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLICsiloyaltyandadvocacyaggregateRecordNonNull(LICsiloyaltyandadvocacyaggregateRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLICsiloyaltyandadvocacyaggregateRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLICsiloyaltyandadvocacyaggregateRecordNonNull", null);
			LICsiloyaltyandadvocacyaggregateDAO dao = new LICsiloyaltyandadvocacyaggregateDAO();
			LICsiloyaltyandadvocacyaggregateRecord dbRecord = dao.loadLICsiloyaltyandadvocacyaggregateRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLICsiloyaltyandadvocacyaggregateRecord(dbRecord);
			logger.trace("updateLICsiloyaltyandadvocacyaggregateRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLICsiloyaltyandadvocacyaggregateRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLICsiloyaltyandadvocacyaggregateRecord(LICsiloyaltyandadvocacyaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLICsiloyaltyandadvocacyaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLICsiloyaltyandadvocacyaggregateRecord", null);
			LICsiloyaltyandadvocacyaggregateDAO dao = new LICsiloyaltyandadvocacyaggregateDAO();
			boolean result = dao.deleteLICsiloyaltyandadvocacyaggregateRecord(record);
			logger.trace("deleteLICsiloyaltyandadvocacyaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLICsiloyaltyandadvocacyaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
