
/*
 * LIFailedloginService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LIFailedloginDAO;
import com.leadics.suzukithdashboard.to.LIFailedloginRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIFailedloginService extends LIService
{
	static LogUtils logger = new LogUtils(LIFailedloginService.class.getName());


	public LIFailedloginRecord[] loadLIFailedloginRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIFailedloginRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIFailedloginRecords", null);
			LIFailedloginDAO dao = new LIFailedloginDAO();
			LIFailedloginRecord[] results = dao.loadLIFailedloginRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIFailedloginRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIFailedloginRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIFailedloginRecord loadFirstLIFailedloginRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIFailedloginRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIFailedloginRecord", null);
			LIFailedloginDAO dao = new LIFailedloginDAO();
			LIFailedloginRecord result = dao.loadFirstLIFailedloginRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIFailedloginRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIFailedloginRecord searchFirstLIFailedloginRecord(LIFailedloginRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIFailedloginRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIFailedloginRecord", null);
			LIFailedloginDAO dao = new LIFailedloginDAO();
			LIFailedloginRecord[] records = dao.searchLIFailedloginRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIFailedloginRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIFailedloginRecord searchFirstLIFailedloginRecordExactUpper(LIFailedloginRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIFailedloginRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIFailedloginRecordsExactUpper", null);
			LIFailedloginDAO dao = new LIFailedloginDAO();
			LIFailedloginRecord[] records = dao.searchLIFailedloginRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIFailedloginRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIFailedloginRecord[] searchLIFailedloginRecords(LIFailedloginRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIFailedloginRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIFailedloginRecords", null);
			LIFailedloginDAO dao = new LIFailedloginDAO();
			LIFailedloginRecord[] records = dao.searchLIFailedloginRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIFailedloginRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIFailedloginRecordCount(LIFailedloginRecord record)
	throws Exception
	{
		return loadLIFailedloginRecordCount(record, null);
	}


	public int loadLIFailedloginRecordCount(LIFailedloginRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIFailedloginRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIFailedloginRecordCount", null);
			LIFailedloginDAO dao = new LIFailedloginDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIFailedloginRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIFailedloginRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIFailedloginRecord loadLIFailedloginRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIFailedloginRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIFailedloginRecord", null);
			LIFailedloginDAO dao = new LIFailedloginDAO();
			LIFailedloginRecord result = dao.loadLIFailedloginRecord(key);
			logger.trace("loadLIFailedloginRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIFailedloginRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIFailedloginRecordSearchResultByPage(LIFailedloginRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIFailedloginRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIFailedloginRecordSearchResultByPage(LIFailedloginRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIFailedloginRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIFailedloginRecordSearchResult", null);
			LIFailedloginDAO dao = new LIFailedloginDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIFailedloginRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIFailedloginRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIFailedloginRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIFailedloginRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIFailedloginRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIFailedloginRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIFailedloginRecordSearchResultByPageQuery", null);
			LIFailedloginDAO dao = new LIFailedloginDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIFailedloginRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIFailedloginRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIFailedloginRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIFailedloginRecord(LIFailedloginRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIFailedloginRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIFailedloginRecord", null);
			LIFailedloginDAO dao = new LIFailedloginDAO();
			int result = dao.insertLIFailedloginRecord(record);
			logger.trace("insertLIFailedloginRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIFailedloginRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIFailedloginRecord(LIFailedloginRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIFailedloginRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIFailedloginRecord", null);
			LIFailedloginDAO dao = new LIFailedloginDAO();
			boolean result = dao.updateLIFailedloginRecord(record);
			logger.trace("updateLIFailedloginRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIFailedloginRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIFailedloginRecordNonNull(LIFailedloginRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIFailedloginRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIFailedloginRecordNonNull", null);
			LIFailedloginDAO dao = new LIFailedloginDAO();
			LIFailedloginRecord dbRecord = dao.loadLIFailedloginRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIFailedloginRecord(dbRecord);
			logger.trace("updateLIFailedloginRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIFailedloginRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIFailedloginRecord(LIFailedloginRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIFailedloginRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIFailedloginRecord", null);
			LIFailedloginDAO dao = new LIFailedloginDAO();
			boolean result = dao.deleteLIFailedloginRecord(record);
			logger.trace("deleteLIFailedloginRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIFailedloginRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
