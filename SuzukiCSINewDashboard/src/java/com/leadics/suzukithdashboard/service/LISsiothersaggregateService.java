
/*
 * LISsiothersaggregateService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LISsiothersaggregateDAO;
import com.leadics.suzukithdashboard.to.LISsiothersaggregateRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LISsiothersaggregateService extends LIService
{
	static LogUtils logger = new LogUtils(LISsiothersaggregateService.class.getName());


	public LISsiothersaggregateRecord[] loadLISsiothersaggregateRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLISsiothersaggregateRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsiothersaggregateRecords", null);
			LISsiothersaggregateDAO dao = new LISsiothersaggregateDAO();
			LISsiothersaggregateRecord[] results = dao.loadLISsiothersaggregateRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLISsiothersaggregateRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLISsiothersaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LISsiothersaggregateRecord loadFirstLISsiothersaggregateRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLISsiothersaggregateRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLISsiothersaggregateRecord", null);
			LISsiothersaggregateDAO dao = new LISsiothersaggregateDAO();
			LISsiothersaggregateRecord result = dao.loadFirstLISsiothersaggregateRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLISsiothersaggregateRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISsiothersaggregateRecord searchFirstLISsiothersaggregateRecord(LISsiothersaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLISsiothersaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLISsiothersaggregateRecord", null);
			LISsiothersaggregateDAO dao = new LISsiothersaggregateDAO();
			LISsiothersaggregateRecord[] records = dao.searchLISsiothersaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLISsiothersaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LISsiothersaggregateRecord searchFirstLISsiothersaggregateRecordExactUpper(LISsiothersaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLISsiothersaggregateRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLISsiothersaggregateRecordsExactUpper", null);
			LISsiothersaggregateDAO dao = new LISsiothersaggregateDAO();
			LISsiothersaggregateRecord[] records = dao.searchLISsiothersaggregateRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLISsiothersaggregateRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISsiothersaggregateRecord[] searchLISsiothersaggregateRecords(LISsiothersaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLISsiothersaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsiothersaggregateRecords", null);
			LISsiothersaggregateDAO dao = new LISsiothersaggregateDAO();
			LISsiothersaggregateRecord[] records = dao.searchLISsiothersaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLISsiothersaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLISsiothersaggregateRecordCount(LISsiothersaggregateRecord record)
	throws Exception
	{
		return loadLISsiothersaggregateRecordCount(record, null);
	}


	public int loadLISsiothersaggregateRecordCount(LISsiothersaggregateRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLISsiothersaggregateRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsiothersaggregateRecordCount", null);
			LISsiothersaggregateDAO dao = new LISsiothersaggregateDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLISsiothersaggregateRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLISsiothersaggregateRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISsiothersaggregateRecord loadLISsiothersaggregateRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLISsiothersaggregateRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsiothersaggregateRecord", null);
			LISsiothersaggregateDAO dao = new LISsiothersaggregateDAO();
			LISsiothersaggregateRecord result = dao.loadLISsiothersaggregateRecord(key);
			logger.trace("loadLISsiothersaggregateRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLISsiothersaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLISsiothersaggregateRecordSearchResultByPage(LISsiothersaggregateRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLISsiothersaggregateRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLISsiothersaggregateRecordSearchResultByPage(LISsiothersaggregateRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLISsiothersaggregateRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLISsiothersaggregateRecordSearchResult", null);
			LISsiothersaggregateDAO dao = new LISsiothersaggregateDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLISsiothersaggregateRecordCount(record);
			dao.setLimits(offset, maxrows);
			LISsiothersaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLISsiothersaggregateRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLISsiothersaggregateRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLISsiothersaggregateRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLISsiothersaggregateRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLISsiothersaggregateRecordSearchResultByPageQuery", null);
			LISsiothersaggregateDAO dao = new LISsiothersaggregateDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LISsiothersaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLISsiothersaggregateRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLISsiothersaggregateRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLISsiothersaggregateRecord(LISsiothersaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLISsiothersaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLISsiothersaggregateRecord", null);
			LISsiothersaggregateDAO dao = new LISsiothersaggregateDAO();
			int result = dao.insertLISsiothersaggregateRecord(record);
			logger.trace("insertLISsiothersaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLISsiothersaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLISsiothersaggregateRecord(LISsiothersaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLISsiothersaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLISsiothersaggregateRecord", null);
			LISsiothersaggregateDAO dao = new LISsiothersaggregateDAO();
			boolean result = dao.updateLISsiothersaggregateRecord(record);
			logger.trace("updateLISsiothersaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLISsiothersaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLISsiothersaggregateRecordNonNull(LISsiothersaggregateRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLISsiothersaggregateRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLISsiothersaggregateRecordNonNull", null);
			LISsiothersaggregateDAO dao = new LISsiothersaggregateDAO();
			LISsiothersaggregateRecord dbRecord = dao.loadLISsiothersaggregateRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLISsiothersaggregateRecord(dbRecord);
			logger.trace("updateLISsiothersaggregateRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLISsiothersaggregateRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLISsiothersaggregateRecord(LISsiothersaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLISsiothersaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLISsiothersaggregateRecord", null);
			LISsiothersaggregateDAO dao = new LISsiothersaggregateDAO();
			boolean result = dao.deleteLISsiothersaggregateRecord(record);
			logger.trace("deleteLISsiothersaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLISsiothersaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
