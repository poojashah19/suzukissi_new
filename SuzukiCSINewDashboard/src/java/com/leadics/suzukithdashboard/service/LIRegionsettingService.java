
/*
 * LIRegionsettingService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LIRegionsettingDAO;
import com.leadics.suzukithdashboard.to.LIRegionsettingRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIRegionsettingService extends LIService
{
	static LogUtils logger = new LogUtils(LIRegionsettingService.class.getName());


	public LIRegionsettingRecord[] loadLIRegionsettingRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIRegionsettingRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIRegionsettingRecords", null);
			LIRegionsettingDAO dao = new LIRegionsettingDAO();
			LIRegionsettingRecord[] results = dao.loadLIRegionsettingRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIRegionsettingRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIRegionsettingRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIRegionsettingRecord loadFirstLIRegionsettingRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIRegionsettingRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIRegionsettingRecord", null);
			LIRegionsettingDAO dao = new LIRegionsettingDAO();
			LIRegionsettingRecord result = dao.loadFirstLIRegionsettingRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIRegionsettingRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIRegionsettingRecord searchFirstLIRegionsettingRecord(LIRegionsettingRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIRegionsettingRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIRegionsettingRecord", null);
			LIRegionsettingDAO dao = new LIRegionsettingDAO();
			LIRegionsettingRecord[] records = dao.searchLIRegionsettingRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIRegionsettingRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIRegionsettingRecord searchFirstLIRegionsettingRecordExactUpper(LIRegionsettingRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIRegionsettingRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIRegionsettingRecordsExactUpper", null);
			LIRegionsettingDAO dao = new LIRegionsettingDAO();
			LIRegionsettingRecord[] records = dao.searchLIRegionsettingRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIRegionsettingRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIRegionsettingRecord[] searchLIRegionsettingRecords(LIRegionsettingRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIRegionsettingRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIRegionsettingRecords", null);
			LIRegionsettingDAO dao = new LIRegionsettingDAO();
			LIRegionsettingRecord[] records = dao.searchLIRegionsettingRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIRegionsettingRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIRegionsettingRecordCount(LIRegionsettingRecord record)
	throws Exception
	{
		return loadLIRegionsettingRecordCount(record, null);
	}


	public int loadLIRegionsettingRecordCount(LIRegionsettingRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIRegionsettingRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIRegionsettingRecordCount", null);
			LIRegionsettingDAO dao = new LIRegionsettingDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIRegionsettingRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIRegionsettingRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIRegionsettingRecord loadLIRegionsettingRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIRegionsettingRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIRegionsettingRecord", null);
			LIRegionsettingDAO dao = new LIRegionsettingDAO();
			LIRegionsettingRecord result = dao.loadLIRegionsettingRecord(key);
			logger.trace("loadLIRegionsettingRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIRegionsettingRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIRegionsettingRecordSearchResultByPage(LIRegionsettingRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIRegionsettingRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIRegionsettingRecordSearchResultByPage(LIRegionsettingRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIRegionsettingRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIRegionsettingRecordSearchResult", null);
			LIRegionsettingDAO dao = new LIRegionsettingDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIRegionsettingRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIRegionsettingRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIRegionsettingRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIRegionsettingRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIRegionsettingRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIRegionsettingRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIRegionsettingRecordSearchResultByPageQuery", null);
			LIRegionsettingDAO dao = new LIRegionsettingDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIRegionsettingRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIRegionsettingRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIRegionsettingRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIRegionsettingRecord(LIRegionsettingRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIRegionsettingRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIRegionsettingRecord", null);
			LIRegionsettingDAO dao = new LIRegionsettingDAO();
			int result = dao.insertLIRegionsettingRecord(record);
			logger.trace("insertLIRegionsettingRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIRegionsettingRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIRegionsettingRecord(LIRegionsettingRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIRegionsettingRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIRegionsettingRecord", null);
			LIRegionsettingDAO dao = new LIRegionsettingDAO();
			boolean result = dao.updateLIRegionsettingRecord(record);
			logger.trace("updateLIRegionsettingRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIRegionsettingRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIRegionsettingRecordNonNull(LIRegionsettingRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIRegionsettingRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIRegionsettingRecordNonNull", null);
			LIRegionsettingDAO dao = new LIRegionsettingDAO();
			LIRegionsettingRecord dbRecord = dao.loadLIRegionsettingRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIRegionsettingRecord(dbRecord);
			logger.trace("updateLIRegionsettingRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIRegionsettingRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIRegionsettingRecord(LIRegionsettingRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIRegionsettingRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIRegionsettingRecord", null);
			LIRegionsettingDAO dao = new LIRegionsettingDAO();
			boolean result = dao.deleteLIRegionsettingRecord(record);
			logger.trace("deleteLIRegionsettingRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIRegionsettingRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
