
/*
 * LICsitranslatedService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LICsitranslatedDAO;
import com.leadics.suzukithdashboard.to.LICsitranslatedRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LICsitranslatedService extends LIService
{
	static LogUtils logger = new LogUtils(LICsitranslatedService.class.getName());


	public LICsitranslatedRecord[] loadLICsitranslatedRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLICsitranslatedRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsitranslatedRecords", null);
			LICsitranslatedDAO dao = new LICsitranslatedDAO();
			LICsitranslatedRecord[] results = dao.loadLICsitranslatedRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLICsitranslatedRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLICsitranslatedRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LICsitranslatedRecord loadFirstLICsitranslatedRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLICsitranslatedRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLICsitranslatedRecord", null);
			LICsitranslatedDAO dao = new LICsitranslatedDAO();
			LICsitranslatedRecord result = dao.loadFirstLICsitranslatedRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLICsitranslatedRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICsitranslatedRecord searchFirstLICsitranslatedRecord(LICsitranslatedRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLICsitranslatedRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLICsitranslatedRecord", null);
			LICsitranslatedDAO dao = new LICsitranslatedDAO();
			LICsitranslatedRecord[] records = dao.searchLICsitranslatedRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLICsitranslatedRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LICsitranslatedRecord searchFirstLICsitranslatedRecordExactUpper(LICsitranslatedRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLICsitranslatedRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLICsitranslatedRecordsExactUpper", null);
			LICsitranslatedDAO dao = new LICsitranslatedDAO();
			LICsitranslatedRecord[] records = dao.searchLICsitranslatedRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLICsitranslatedRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICsitranslatedRecord[] searchLICsitranslatedRecords(LICsitranslatedRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLICsitranslatedRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsitranslatedRecords", null);
			LICsitranslatedDAO dao = new LICsitranslatedDAO();
			LICsitranslatedRecord[] records = dao.searchLICsitranslatedRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLICsitranslatedRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLICsitranslatedRecordCount(LICsitranslatedRecord record)
	throws Exception
	{
		return loadLICsitranslatedRecordCount(record, null);
	}


	public int loadLICsitranslatedRecordCount(LICsitranslatedRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLICsitranslatedRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsitranslatedRecordCount", null);
			LICsitranslatedDAO dao = new LICsitranslatedDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLICsitranslatedRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLICsitranslatedRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICsitranslatedRecord loadLICsitranslatedRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLICsitranslatedRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsitranslatedRecord", null);
			LICsitranslatedDAO dao = new LICsitranslatedDAO();
			LICsitranslatedRecord result = dao.loadLICsitranslatedRecord(key);
			logger.trace("loadLICsitranslatedRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLICsitranslatedRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLICsitranslatedRecordSearchResultByPage(LICsitranslatedRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLICsitranslatedRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLICsitranslatedRecordSearchResultByPage(LICsitranslatedRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLICsitranslatedRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLICsitranslatedRecordSearchResult", null);
			LICsitranslatedDAO dao = new LICsitranslatedDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLICsitranslatedRecordCount(record);
			dao.setLimits(offset, maxrows);
			LICsitranslatedRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLICsitranslatedRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLICsitranslatedRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLICsitranslatedRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLICsitranslatedRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLICsitranslatedRecordSearchResultByPageQuery", null);
			LICsitranslatedDAO dao = new LICsitranslatedDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LICsitranslatedRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLICsitranslatedRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLICsitranslatedRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLICsitranslatedRecord(LICsitranslatedRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLICsitranslatedRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLICsitranslatedRecord", null);
			LICsitranslatedDAO dao = new LICsitranslatedDAO();
			int result = dao.insertLICsitranslatedRecord(record);
			logger.trace("insertLICsitranslatedRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLICsitranslatedRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLICsitranslatedRecord(LICsitranslatedRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLICsitranslatedRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLICsitranslatedRecord", null);
			LICsitranslatedDAO dao = new LICsitranslatedDAO();
			boolean result = dao.updateLICsitranslatedRecord(record);
			logger.trace("updateLICsitranslatedRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLICsitranslatedRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLICsitranslatedRecordNonNull(LICsitranslatedRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLICsitranslatedRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLICsitranslatedRecordNonNull", null);
			LICsitranslatedDAO dao = new LICsitranslatedDAO();
			LICsitranslatedRecord dbRecord = dao.loadLICsitranslatedRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLICsitranslatedRecord(dbRecord);
			logger.trace("updateLICsitranslatedRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLICsitranslatedRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLICsitranslatedRecord(LICsitranslatedRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLICsitranslatedRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLICsitranslatedRecord", null);
			LICsitranslatedDAO dao = new LICsitranslatedDAO();
			boolean result = dao.deleteLICsitranslatedRecord(record);
			logger.trace("deleteLICsitranslatedRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLICsitranslatedRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
