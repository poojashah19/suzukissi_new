
/*
 * LIReportquirescsiService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LIReportquirescsiDAO;
import com.leadics.suzukithdashboard.to.LIReportquirescsiRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIReportquirescsiService extends LIService
{
	static LogUtils logger = new LogUtils(LIReportquirescsiService.class.getName());


	public LIReportquirescsiRecord[] loadLIReportquirescsiRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIReportquirescsiRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIReportquirescsiRecords", null);
			LIReportquirescsiDAO dao = new LIReportquirescsiDAO();
			LIReportquirescsiRecord[] results = dao.loadLIReportquirescsiRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIReportquirescsiRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIReportquirescsiRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIReportquirescsiRecord loadFirstLIReportquirescsiRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIReportquirescsiRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIReportquirescsiRecord", null);
			LIReportquirescsiDAO dao = new LIReportquirescsiDAO();
			LIReportquirescsiRecord result = dao.loadFirstLIReportquirescsiRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIReportquirescsiRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIReportquirescsiRecord searchFirstLIReportquirescsiRecord(LIReportquirescsiRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIReportquirescsiRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIReportquirescsiRecord", null);
			LIReportquirescsiDAO dao = new LIReportquirescsiDAO();
			LIReportquirescsiRecord[] records = dao.searchLIReportquirescsiRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIReportquirescsiRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIReportquirescsiRecord searchFirstLIReportquirescsiRecordExactUpper(LIReportquirescsiRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIReportquirescsiRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIReportquirescsiRecordsExactUpper", null);
			LIReportquirescsiDAO dao = new LIReportquirescsiDAO();
			LIReportquirescsiRecord[] records = dao.searchLIReportquirescsiRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIReportquirescsiRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIReportquirescsiRecord[] searchLIReportquirescsiRecords(LIReportquirescsiRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIReportquirescsiRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIReportquirescsiRecords", null);
			LIReportquirescsiDAO dao = new LIReportquirescsiDAO();
			LIReportquirescsiRecord[] records = dao.searchLIReportquirescsiRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIReportquirescsiRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIReportquirescsiRecordCount(LIReportquirescsiRecord record)
	throws Exception
	{
		return loadLIReportquirescsiRecordCount(record, null);
	}


	public int loadLIReportquirescsiRecordCount(LIReportquirescsiRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIReportquirescsiRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIReportquirescsiRecordCount", null);
			LIReportquirescsiDAO dao = new LIReportquirescsiDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIReportquirescsiRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIReportquirescsiRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIReportquirescsiRecord loadLIReportquirescsiRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIReportquirescsiRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIReportquirescsiRecord", null);
			LIReportquirescsiDAO dao = new LIReportquirescsiDAO();
			LIReportquirescsiRecord result = dao.loadLIReportquirescsiRecord(key);
			logger.trace("loadLIReportquirescsiRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIReportquirescsiRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIReportquirescsiRecordSearchResultByPage(LIReportquirescsiRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIReportquirescsiRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIReportquirescsiRecordSearchResultByPage(LIReportquirescsiRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIReportquirescsiRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIReportquirescsiRecordSearchResult", null);
			LIReportquirescsiDAO dao = new LIReportquirescsiDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIReportquirescsiRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIReportquirescsiRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIReportquirescsiRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIReportquirescsiRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIReportquirescsiRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIReportquirescsiRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIReportquirescsiRecordSearchResultByPageQuery", null);
			LIReportquirescsiDAO dao = new LIReportquirescsiDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIReportquirescsiRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIReportquirescsiRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIReportquirescsiRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIReportquirescsiRecord(LIReportquirescsiRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIReportquirescsiRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIReportquirescsiRecord", null);
			LIReportquirescsiDAO dao = new LIReportquirescsiDAO();
			int result = dao.insertLIReportquirescsiRecord(record);
			logger.trace("insertLIReportquirescsiRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIReportquirescsiRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIReportquirescsiRecord(LIReportquirescsiRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIReportquirescsiRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIReportquirescsiRecord", null);
			LIReportquirescsiDAO dao = new LIReportquirescsiDAO();
			boolean result = dao.updateLIReportquirescsiRecord(record);
			logger.trace("updateLIReportquirescsiRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIReportquirescsiRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIReportquirescsiRecordNonNull(LIReportquirescsiRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIReportquirescsiRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIReportquirescsiRecordNonNull", null);
			LIReportquirescsiDAO dao = new LIReportquirescsiDAO();
			LIReportquirescsiRecord dbRecord = dao.loadLIReportquirescsiRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIReportquirescsiRecord(dbRecord);
			logger.trace("updateLIReportquirescsiRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIReportquirescsiRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIReportquirescsiRecord(LIReportquirescsiRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIReportquirescsiRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIReportquirescsiRecord", null);
			LIReportquirescsiDAO dao = new LIReportquirescsiDAO();
			boolean result = dao.deleteLIReportquirescsiRecord(record);
			logger.trace("deleteLIReportquirescsiRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIReportquirescsiRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
