
/*
 * LIOtherstableviewService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LIOtherstableviewDAO;
import com.leadics.suzukithdashboard.to.LIOtherstableviewRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIOtherstableviewService extends LIService
{
	static LogUtils logger = new LogUtils(LIOtherstableviewService.class.getName());


	public LIOtherstableviewRecord[] loadLIOtherstableviewRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIOtherstableviewRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIOtherstableviewRecords", null);
			LIOtherstableviewDAO dao = new LIOtherstableviewDAO();
			LIOtherstableviewRecord[] results = dao.loadLIOtherstableviewRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIOtherstableviewRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIOtherstableviewRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIOtherstableviewRecord loadFirstLIOtherstableviewRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIOtherstableviewRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIOtherstableviewRecord", null);
			LIOtherstableviewDAO dao = new LIOtherstableviewDAO();
			LIOtherstableviewRecord result = dao.loadFirstLIOtherstableviewRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIOtherstableviewRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIOtherstableviewRecord searchFirstLIOtherstableviewRecord(LIOtherstableviewRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIOtherstableviewRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIOtherstableviewRecord", null);
			LIOtherstableviewDAO dao = new LIOtherstableviewDAO();
			LIOtherstableviewRecord[] records = dao.searchLIOtherstableviewRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIOtherstableviewRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIOtherstableviewRecord searchFirstLIOtherstableviewRecordExactUpper(LIOtherstableviewRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIOtherstableviewRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIOtherstableviewRecordsExactUpper", null);
			LIOtherstableviewDAO dao = new LIOtherstableviewDAO();
			LIOtherstableviewRecord[] records = dao.searchLIOtherstableviewRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIOtherstableviewRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIOtherstableviewRecord[] searchLIOtherstableviewRecords(LIOtherstableviewRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIOtherstableviewRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIOtherstableviewRecords", null);
			LIOtherstableviewDAO dao = new LIOtherstableviewDAO();
			LIOtherstableviewRecord[] records = dao.searchLIOtherstableviewRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIOtherstableviewRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIOtherstableviewRecordCount(LIOtherstableviewRecord record)
	throws Exception
	{
		return loadLIOtherstableviewRecordCount(record, null);
	}


	public int loadLIOtherstableviewRecordCount(LIOtherstableviewRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIOtherstableviewRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIOtherstableviewRecordCount", null);
			LIOtherstableviewDAO dao = new LIOtherstableviewDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIOtherstableviewRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIOtherstableviewRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIOtherstableviewRecord loadLIOtherstableviewRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIOtherstableviewRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIOtherstableviewRecord", null);
			LIOtherstableviewDAO dao = new LIOtherstableviewDAO();
			LIOtherstableviewRecord result = dao.loadLIOtherstableviewRecord(key);
			logger.trace("loadLIOtherstableviewRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIOtherstableviewRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIOtherstableviewRecordSearchResultByPage(LIOtherstableviewRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIOtherstableviewRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIOtherstableviewRecordSearchResultByPage(LIOtherstableviewRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIOtherstableviewRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIOtherstableviewRecordSearchResult", null);
			LIOtherstableviewDAO dao = new LIOtherstableviewDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIOtherstableviewRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIOtherstableviewRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIOtherstableviewRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIOtherstableviewRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIOtherstableviewRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIOtherstableviewRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIOtherstableviewRecordSearchResultByPageQuery", null);
			LIOtherstableviewDAO dao = new LIOtherstableviewDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIOtherstableviewRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIOtherstableviewRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIOtherstableviewRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
}
