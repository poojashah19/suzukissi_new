
/*
 * LISsifactoraggregateService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LISsifactoraggregateDAO;
import com.leadics.suzukithdashboard.to.LISsifactoraggregateRecord;
import java.sql.*;
import org.json.simple.*;
import com.leadics.utils.*;
public class LISsifactoraggregateService extends LIService
{
	static LogUtils logger = new LogUtils(LISsifactoraggregateService.class.getName());


	public LISsifactoraggregateRecord[] loadLISsifactoraggregateRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLISsifactoraggregateRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsifactoraggregateRecords", null);
			LISsifactoraggregateDAO dao = new LISsifactoraggregateDAO();
			LISsifactoraggregateRecord[] results = dao.loadLISsifactoraggregateRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLISsifactoraggregateRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLISsifactoraggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LISsifactoraggregateRecord loadFirstLISsifactoraggregateRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLISsifactoraggregateRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLISsifactoraggregateRecord", null);
			LISsifactoraggregateDAO dao = new LISsifactoraggregateDAO();
			LISsifactoraggregateRecord result = dao.loadFirstLISsifactoraggregateRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLISsifactoraggregateRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISsifactoraggregateRecord searchFirstLISsifactoraggregateRecord(LISsifactoraggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLISsifactoraggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLISsifactoraggregateRecord", null);
			LISsifactoraggregateDAO dao = new LISsifactoraggregateDAO();
			LISsifactoraggregateRecord[] records = dao.searchLISsifactoraggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLISsifactoraggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LISsifactoraggregateRecord searchFirstLISsifactoraggregateRecordExactUpper(LISsifactoraggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLISsifactoraggregateRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLISsifactoraggregateRecordsExactUpper", null);
			LISsifactoraggregateDAO dao = new LISsifactoraggregateDAO();
			LISsifactoraggregateRecord[] records = dao.searchLISsifactoraggregateRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLISsifactoraggregateRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISsifactoraggregateRecord[] searchLISsifactoraggregateRecords(LISsifactoraggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLISsifactoraggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsifactoraggregateRecords", null);
			LISsifactoraggregateDAO dao = new LISsifactoraggregateDAO();
			LISsifactoraggregateRecord[] records = dao.searchLISsifactoraggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLISsifactoraggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLISsifactoraggregateRecordCount(LISsifactoraggregateRecord record)
	throws Exception
	{
		return loadLISsifactoraggregateRecordCount(record, null);
	}


	public int loadLISsifactoraggregateRecordCount(LISsifactoraggregateRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLISsifactoraggregateRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsifactoraggregateRecordCount", null);
			LISsifactoraggregateDAO dao = new LISsifactoraggregateDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLISsifactoraggregateRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLISsifactoraggregateRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISsifactoraggregateRecord loadLISsifactoraggregateRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLISsifactoraggregateRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsifactoraggregateRecord", null);
			LISsifactoraggregateDAO dao = new LISsifactoraggregateDAO();
			LISsifactoraggregateRecord result = dao.loadLISsifactoraggregateRecord(key);
			logger.trace("loadLISsifactoraggregateRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLISsifactoraggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLISsifactoraggregateRecordSearchResultByPage(LISsifactoraggregateRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLISsifactoraggregateRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLISsifactoraggregateRecordSearchResultByPage(LISsifactoraggregateRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLISsifactoraggregateRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLISsifactoraggregateRecordSearchResult", null);
			LISsifactoraggregateDAO dao = new LISsifactoraggregateDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLISsifactoraggregateRecordCount(record);
			dao.setLimits(offset, maxrows);
			LISsifactoraggregateRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLISsifactoraggregateRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLISsifactoraggregateRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLISsifactoraggregateRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLISsifactoraggregateRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLISsifactoraggregateRecordSearchResultByPageQuery", null);
			LISsifactoraggregateDAO dao = new LISsifactoraggregateDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LISsifactoraggregateRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLISsifactoraggregateRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLISsifactoraggregateRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLISsifactoraggregateRecord(LISsifactoraggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLISsifactoraggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLISsifactoraggregateRecord", null);
			LISsifactoraggregateDAO dao = new LISsifactoraggregateDAO();
			int result = dao.insertLISsifactoraggregateRecord(record);
			logger.trace("insertLISsifactoraggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLISsifactoraggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLISsifactoraggregateRecord(LISsifactoraggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLISsifactoraggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLISsifactoraggregateRecord", null);
			LISsifactoraggregateDAO dao = new LISsifactoraggregateDAO();
			boolean result = dao.updateLISsifactoraggregateRecord(record);
			logger.trace("updateLISsifactoraggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLISsifactoraggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLISsifactoraggregateRecordNonNull(LISsifactoraggregateRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLISsifactoraggregateRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLISsifactoraggregateRecordNonNull", null);
			LISsifactoraggregateDAO dao = new LISsifactoraggregateDAO();
			LISsifactoraggregateRecord dbRecord = dao.loadLISsifactoraggregateRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLISsifactoraggregateRecord(dbRecord);
			logger.trace("updateLISsifactoraggregateRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLISsifactoraggregateRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLISsifactoraggregateRecord(LISsifactoraggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLISsifactoraggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLISsifactoraggregateRecord", null);
			LISsifactoraggregateDAO dao = new LISsifactoraggregateDAO();
			boolean result = dao.deleteLISsifactoraggregateRecord(record);
			logger.trace("deleteLISsifactoraggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLISsifactoraggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
