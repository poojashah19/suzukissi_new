
/*
 * LIcalenderfilterService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LIcalenderfilterDAO;
import com.leadics.suzukithdashboard.to.LIcalenderfilterRecord;
import java.sql.*;
import org.json.simple.*;
import com.leadics.utils.*;
public class LIcalenderfilterService extends LIService
{
	static LogUtils logger = new LogUtils(LIcalenderfilterService.class.getName());


	public LIcalenderfilterRecord[] loadLIcalenderfilterRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIcalenderfilterRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIcalenderfilterRecords", null);
			LIcalenderfilterDAO dao = new LIcalenderfilterDAO();
			LIcalenderfilterRecord[] results = dao.loadLIcalenderfilterRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIcalenderfilterRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIcalenderfilterRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIcalenderfilterRecord loadFirstLIcalenderfilterRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIcalenderfilterRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIcalenderfilterRecord", null);
			LIcalenderfilterDAO dao = new LIcalenderfilterDAO();
			LIcalenderfilterRecord result = dao.loadFirstLIcalenderfilterRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIcalenderfilterRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIcalenderfilterRecord searchFirstLIcalenderfilterRecord(LIcalenderfilterRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIcalenderfilterRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIcalenderfilterRecord", null);
			LIcalenderfilterDAO dao = new LIcalenderfilterDAO();
			LIcalenderfilterRecord[] records = dao.searchLIcalenderfilterRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIcalenderfilterRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIcalenderfilterRecord searchFirstLIcalenderfilterRecordExactUpper(LIcalenderfilterRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIcalenderfilterRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIcalenderfilterRecordsExactUpper", null);
			LIcalenderfilterDAO dao = new LIcalenderfilterDAO();
			LIcalenderfilterRecord[] records = dao.searchLIcalenderfilterRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIcalenderfilterRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIcalenderfilterRecord[] searchLIcalenderfilterRecords(LIcalenderfilterRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIcalenderfilterRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIcalenderfilterRecords", null);
			LIcalenderfilterDAO dao = new LIcalenderfilterDAO();
			LIcalenderfilterRecord[] records = dao.searchLIcalenderfilterRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIcalenderfilterRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIcalenderfilterRecordCount(LIcalenderfilterRecord record)
	throws Exception
	{
		return loadLIcalenderfilterRecordCount(record, null);
	}


	public int loadLIcalenderfilterRecordCount(LIcalenderfilterRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIcalenderfilterRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIcalenderfilterRecordCount", null);
			LIcalenderfilterDAO dao = new LIcalenderfilterDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIcalenderfilterRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIcalenderfilterRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIcalenderfilterRecord loadLIcalenderfilterRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIcalenderfilterRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIcalenderfilterRecord", null);
			LIcalenderfilterDAO dao = new LIcalenderfilterDAO();
			LIcalenderfilterRecord result = dao.loadLIcalenderfilterRecord(key);
			logger.trace("loadLIcalenderfilterRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIcalenderfilterRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIcalenderfilterRecordSearchResultByPage(LIcalenderfilterRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIcalenderfilterRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIcalenderfilterRecordSearchResultByPage(LIcalenderfilterRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIcalenderfilterRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIcalenderfilterRecordSearchResult", null);
			LIcalenderfilterDAO dao = new LIcalenderfilterDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIcalenderfilterRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIcalenderfilterRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIcalenderfilterRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIcalenderfilterRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIcalenderfilterRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIcalenderfilterRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIcalenderfilterRecordSearchResultByPageQuery", null);
			LIcalenderfilterDAO dao = new LIcalenderfilterDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIcalenderfilterRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIcalenderfilterRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIcalenderfilterRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIcalenderfilterRecord(LIcalenderfilterRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIcalenderfilterRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIcalenderfilterRecord", null);
			LIcalenderfilterDAO dao = new LIcalenderfilterDAO();
			int result = dao.insertLIcalenderfilterRecord(record);
			logger.trace("insertLIcalenderfilterRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIcalenderfilterRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIcalenderfilterRecord(LIcalenderfilterRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIcalenderfilterRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIcalenderfilterRecord", null);
			LIcalenderfilterDAO dao = new LIcalenderfilterDAO();
			boolean result = dao.updateLIcalenderfilterRecord(record);
			logger.trace("updateLIcalenderfilterRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIcalenderfilterRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIcalenderfilterRecordNonNull(LIcalenderfilterRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIcalenderfilterRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIcalenderfilterRecordNonNull", null);
			LIcalenderfilterDAO dao = new LIcalenderfilterDAO();
			LIcalenderfilterRecord dbRecord = dao.loadLIcalenderfilterRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIcalenderfilterRecord(dbRecord);
			logger.trace("updateLIcalenderfilterRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIcalenderfilterRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIcalenderfilterRecord(LIcalenderfilterRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIcalenderfilterRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIcalenderfilterRecord", null);
			LIcalenderfilterDAO dao = new LIcalenderfilterDAO();
			boolean result = dao.deleteLIcalenderfilterRecord(record);
			logger.trace("deleteLIcalenderfilterRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIcalenderfilterRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
