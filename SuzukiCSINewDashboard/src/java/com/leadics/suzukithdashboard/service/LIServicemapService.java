
/*
 * LIServicemapService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LIServicemapDAO;
import com.leadics.suzukithdashboard.to.LIServicemapRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIServicemapService extends LIService
{
	static LogUtils logger = new LogUtils(LIServicemapService.class.getName());


	public LIServicemapRecord[] loadLIServicemapRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIServicemapRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIServicemapRecords", null);
			LIServicemapDAO dao = new LIServicemapDAO();
			LIServicemapRecord[] results = dao.loadLIServicemapRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIServicemapRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIServicemapRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIServicemapRecord loadFirstLIServicemapRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIServicemapRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIServicemapRecord", null);
			LIServicemapDAO dao = new LIServicemapDAO();
			LIServicemapRecord result = dao.loadFirstLIServicemapRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIServicemapRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIServicemapRecord searchFirstLIServicemapRecord(LIServicemapRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIServicemapRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIServicemapRecord", null);
			LIServicemapDAO dao = new LIServicemapDAO();
			LIServicemapRecord[] records = dao.searchLIServicemapRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIServicemapRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIServicemapRecord searchFirstLIServicemapRecordExactUpper(LIServicemapRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIServicemapRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIServicemapRecordsExactUpper", null);
			LIServicemapDAO dao = new LIServicemapDAO();
			LIServicemapRecord[] records = dao.searchLIServicemapRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIServicemapRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIServicemapRecord[] searchLIServicemapRecords(LIServicemapRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIServicemapRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIServicemapRecords", null);
			LIServicemapDAO dao = new LIServicemapDAO();
			LIServicemapRecord[] records = dao.searchLIServicemapRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIServicemapRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIServicemapRecordCount(LIServicemapRecord record)
	throws Exception
	{
		return loadLIServicemapRecordCount(record, null);
	}


	public int loadLIServicemapRecordCount(LIServicemapRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIServicemapRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIServicemapRecordCount", null);
			LIServicemapDAO dao = new LIServicemapDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIServicemapRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIServicemapRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIServicemapRecord loadLIServicemapRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIServicemapRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIServicemapRecord", null);
			LIServicemapDAO dao = new LIServicemapDAO();
			LIServicemapRecord result = dao.loadLIServicemapRecord(key);
			logger.trace("loadLIServicemapRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIServicemapRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIServicemapRecordSearchResultByPage(LIServicemapRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIServicemapRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIServicemapRecordSearchResultByPage(LIServicemapRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIServicemapRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIServicemapRecordSearchResult", null);
			LIServicemapDAO dao = new LIServicemapDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIServicemapRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIServicemapRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIServicemapRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIServicemapRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIServicemapRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIServicemapRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIServicemapRecordSearchResultByPageQuery", null);
			LIServicemapDAO dao = new LIServicemapDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIServicemapRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIServicemapRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIServicemapRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIServicemapRecord(LIServicemapRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIServicemapRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIServicemapRecord", null);
			LIServicemapDAO dao = new LIServicemapDAO();
			int result = dao.insertLIServicemapRecord(record);
			logger.trace("insertLIServicemapRecord:Result:" + result);			
			createMakerCheckerAuditEntry("service_map",result + "", "Create",record.getCreatedby(),"Created");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIServicemapRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIServicemapRecord(LIServicemapRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIServicemapRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIServicemapRecord", null);
			LIServicemapDAO dao = new LIServicemapDAO();
			boolean result = dao.updateLIServicemapRecord(record);
			createMakerCheckerAuditEntry("service_map",record.getId(), "Update",record.getModifiedby(),"Update");
			logger.trace("updateLIServicemapRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIServicemapRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIServicemapRecordNonNull(LIServicemapRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIServicemapRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIServicemapRecordNonNull", null);
			LIServicemapDAO dao = new LIServicemapDAO();
			LIServicemapRecord dbRecord = dao.loadLIServicemapRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIServicemapRecord(dbRecord);
			createMakerCheckerAuditEntry("service_map",inputRecord.getId(), "Update",dbRecord.getModifiedby(),"Update");
			logger.trace("updateLIServicemapRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIServicemapRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIServicemapRecord(LIServicemapRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIServicemapRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIServicemapRecord", null);
			LIServicemapDAO dao = new LIServicemapDAO();
			boolean result = dao.deleteLIServicemapRecord(record);
			logger.trace("deleteLIServicemapRecord:Result:" + result);			
			createMakerCheckerAuditEntry("service_map",record.getId(), "Delete",null,"Deleted");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIServicemapRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean approveLIServicemapRecord(LIServicemapRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("approveLIServicemapRecord:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("approveLIServicemapRecord", null);
			LIServicemapDAO dao = new LIServicemapDAO();
			LIServicemapRecord updateRecord = dao.loadLIServicemapRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CA-RNFA104:Record not found");
			}
			if (!isValidStatusForApproval(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CA-IS101:Cannot approve - Invalid Status for Approval");
			}
			if (StringUtils.isSame(updateRecord.getMadeby(), updateId))
			{
				throw new Exception("MF-ERR-CA-MCS102:Cannot Approve. Maker Checker cannot be same");
			}
			updateRecord.setRstatus("1");
			updateRecord.setCheckerlastcmt(comment);
			updateRecord.setCurrappstatus("1");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setCheckedat(DateUtils.getCurrentDateTime());
			updateRecord.setCheckedby(updateId);
			boolean result = dao.updateLIServicemapRecord(updateRecord);
			logger.trace("approveLIServicemapRecord:Result:" + result);			
			createMakerCheckerAuditEntry("service_map",updateRecord.getId(), "Approve",updateId,comment);
			HashMap approveEventMap = callActionEvent("Event_OnApprovalOf_LIServicemapRecord", updateRecord.getId());
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("approveLIServicemapRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean submitLIServicemapRecord(LIServicemapRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("submitLIServicemapRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("submitLIServicemapRecordNonNull", null);
			LIServicemapDAO dao = new LIServicemapDAO();
			LIServicemapRecord updateRecord = dao.loadLIServicemapRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CS-RNFA104:Record not found");
			}
			if (!isValidStatusForSubmission(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CS-IS101:Cannot submit - Invalid Status for Submission");
			}
			updateRecord.setRstatus("1");
			updateRecord.setMakerlastcmt(comment);
			updateRecord.setCurrappstatus("4");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setMadeat(DateUtils.getCurrentDateTime());
			updateRecord.setMadeby(updateId);
			boolean result = dao.updateLIServicemapRecord(updateRecord);
			createMakerCheckerAuditEntry("service_map",updateRecord.getId(),"Submit",updateId,comment);
			HashMap submitEventMap = callActionEvent("Event_OnSubmitOf_LIServicemapRecord", updateRecord.getId());
			logger.trace("submitLIServicemapRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("submitLIServicemapRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean denyLIServicemapRecord(LIServicemapRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("denyLIServicemapRecord:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("denyLIServicemapRecord", null);
			LIServicemapDAO dao = new LIServicemapDAO();
			LIServicemapRecord updateRecord = dao.loadLIServicemapRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CD-RNFA104:Record not found");
			}
			if (!isValidStatusForDeny(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CD-IS101:Cannot deny - Invalid Status for Deny");
			}
			if (StringUtils.isSame(updateRecord.getMadeby(), updateId))
			{
				throw new Exception("MF-ERR-CD-MCS101:Cannot Deny. Maker Checker cannot be same");
			}
			updateRecord.setCheckerlastcmt(comment);
			updateRecord.setCurrappstatus("5");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setCheckedat(DateUtils.getCurrentDateTime());
			updateRecord.setCheckedby(updateId);
			boolean result = dao.updateLIServicemapRecord(updateRecord);
			createMakerCheckerAuditEntry("service_map",updateRecord.getId(),"Deny",updateId,comment);
			HashMap denyEventMap = callActionEvent("Event_OnDenyOf_LIServicemapRecord", updateRecord.getId());
			logger.trace("denyLIServicemapRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("denyLIServicemapRecord" + getStackTrace(exception));
			throw exception;
		}
	}
	public boolean denyPermanantlyLIServicemapRecord(LIServicemapRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("denyPermanantlyLIServicemapRecord:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("denyPermanantlyLIServicemapRecord", null);
			LIServicemapDAO dao = new LIServicemapDAO();
			LIServicemapRecord updateRecord = dao.loadLIServicemapRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CD-RNFADP104:Record not found");
			}
			if (!isValidStatusForDeny(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CD-ISDP101:Cannot deny - Invalid Status for Deny");
			}
			if (StringUtils.isSame(updateRecord.getMadeby(), updateId))
			{
				throw new Exception("MF-ERR-CD-MCS101DP:Cannot Deny. Maker Checker cannot be same");
			}
			updateRecord.setCheckerlastcmt(comment);
			updateRecord.setCurrappstatus("58");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setCheckedat(DateUtils.getCurrentDateTime());
			updateRecord.setCheckedby(updateId);
			boolean result = dao.updateLIServicemapRecord(updateRecord);
			createMakerCheckerAuditEntry("service_map",updateRecord.getId(),"DenyP",updateId,comment);
			HashMap denyEventMap = callActionEvent("Event_OnDenyPOf_LIServicemapRecord", updateRecord.getId());
			logger.trace("denyPLIServicemapRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("denyPLIServicemapRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean remindApprovalLIServicemapRecord(LIServicemapRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("remindApprovalLIServicemapRecord:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("remindApprovalLIServicemapRecord", null);
			LIServicemapDAO dao = new LIServicemapDAO();
			LIServicemapRecord updateRecord = dao.loadLIServicemapRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CR-RNFA104:Record not found");
			}
			if (!isValidStatusForApproval(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CR-IS101:Cannot Remind - Invalid Status for Reminder");
			}
			createMakerCheckerAuditEntry("service_map",updateRecord.getId(), "Approval Reminder",updateId,comment);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return true;
		}
		catch(Exception exception)
		{
			logger.error("remindApprovalLIServicemapRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean resetApprovalLIServicemapRecord(LIServicemapRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("resetApprovalLIServicemapRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("resetApprovalLIServicemapRecordNonNull", null);
			LIServicemapDAO dao = new LIServicemapDAO();
			LIServicemapRecord updateRecord = dao.loadLIServicemapRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-RA-RNFA104:Record not found");
			}
			if (!isValidStatusForReset(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-RA-IS101:Cannot submit - Invalid Status for Reset");
			}
			updateRecord.setRstatus("0");
			updateRecord.setAdminlastcmt(comment);
			updateRecord.setCurrappstatus("0");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setMadeat(DateUtils.getCurrentDateTime());
			updateRecord.setMadeby(updateId);
			boolean result = dao.updateLIServicemapRecord(updateRecord);
			createMakerCheckerAuditEntry("service_map",updateRecord.getId(),"Reset Approval",updateId,comment);
			logger.trace("resetApprovalLIServicemapRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("resetApprovalLIServicemapRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
