
/*
 * LICsisopaggregateService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LICsisopaggregateDAO;
import com.leadics.suzukithdashboard.to.LICsisopaggregateRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LICsisopaggregateService extends LIService
{
	static LogUtils logger = new LogUtils(LICsisopaggregateService.class.getName());


	public LICsisopaggregateRecord[] loadLICsisopaggregateRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLICsisopaggregateRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsisopaggregateRecords", null);
			LICsisopaggregateDAO dao = new LICsisopaggregateDAO();
			LICsisopaggregateRecord[] results = dao.loadLICsisopaggregateRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLICsisopaggregateRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLICsisopaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LICsisopaggregateRecord loadFirstLICsisopaggregateRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLICsisopaggregateRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLICsisopaggregateRecord", null);
			LICsisopaggregateDAO dao = new LICsisopaggregateDAO();
			LICsisopaggregateRecord result = dao.loadFirstLICsisopaggregateRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLICsisopaggregateRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICsisopaggregateRecord searchFirstLICsisopaggregateRecord(LICsisopaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLICsisopaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLICsisopaggregateRecord", null);
			LICsisopaggregateDAO dao = new LICsisopaggregateDAO();
			LICsisopaggregateRecord[] records = dao.searchLICsisopaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLICsisopaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LICsisopaggregateRecord searchFirstLICsisopaggregateRecordExactUpper(LICsisopaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLICsisopaggregateRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLICsisopaggregateRecordsExactUpper", null);
			LICsisopaggregateDAO dao = new LICsisopaggregateDAO();
			LICsisopaggregateRecord[] records = dao.searchLICsisopaggregateRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLICsisopaggregateRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICsisopaggregateRecord[] searchLICsisopaggregateRecords(LICsisopaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLICsisopaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsisopaggregateRecords", null);
			LICsisopaggregateDAO dao = new LICsisopaggregateDAO();
			LICsisopaggregateRecord[] records = dao.searchLICsisopaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLICsisopaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLICsisopaggregateRecordCount(LICsisopaggregateRecord record)
	throws Exception
	{
		return loadLICsisopaggregateRecordCount(record, null);
	}


	public int loadLICsisopaggregateRecordCount(LICsisopaggregateRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLICsisopaggregateRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsisopaggregateRecordCount", null);
			LICsisopaggregateDAO dao = new LICsisopaggregateDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLICsisopaggregateRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLICsisopaggregateRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICsisopaggregateRecord loadLICsisopaggregateRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLICsisopaggregateRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsisopaggregateRecord", null);
			LICsisopaggregateDAO dao = new LICsisopaggregateDAO();
			LICsisopaggregateRecord result = dao.loadLICsisopaggregateRecord(key);
			logger.trace("loadLICsisopaggregateRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLICsisopaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLICsisopaggregateRecordSearchResultByPage(LICsisopaggregateRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLICsisopaggregateRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLICsisopaggregateRecordSearchResultByPage(LICsisopaggregateRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLICsisopaggregateRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLICsisopaggregateRecordSearchResult", null);
			LICsisopaggregateDAO dao = new LICsisopaggregateDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLICsisopaggregateRecordCount(record);
			dao.setLimits(offset, maxrows);
			LICsisopaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLICsisopaggregateRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLICsisopaggregateRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLICsisopaggregateRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLICsisopaggregateRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLICsisopaggregateRecordSearchResultByPageQuery", null);
			LICsisopaggregateDAO dao = new LICsisopaggregateDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LICsisopaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLICsisopaggregateRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLICsisopaggregateRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLICsisopaggregateRecord(LICsisopaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLICsisopaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLICsisopaggregateRecord", null);
			LICsisopaggregateDAO dao = new LICsisopaggregateDAO();
			int result = dao.insertLICsisopaggregateRecord(record);
			logger.trace("insertLICsisopaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLICsisopaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLICsisopaggregateRecord(LICsisopaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLICsisopaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLICsisopaggregateRecord", null);
			LICsisopaggregateDAO dao = new LICsisopaggregateDAO();
			boolean result = dao.updateLICsisopaggregateRecord(record);
			logger.trace("updateLICsisopaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLICsisopaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLICsisopaggregateRecordNonNull(LICsisopaggregateRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLICsisopaggregateRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLICsisopaggregateRecordNonNull", null);
			LICsisopaggregateDAO dao = new LICsisopaggregateDAO();
			LICsisopaggregateRecord dbRecord = dao.loadLICsisopaggregateRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLICsisopaggregateRecord(dbRecord);
			logger.trace("updateLICsisopaggregateRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLICsisopaggregateRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLICsisopaggregateRecord(LICsisopaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLICsisopaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLICsisopaggregateRecord", null);
			LICsisopaggregateDAO dao = new LICsisopaggregateDAO();
			boolean result = dao.deleteLICsisopaggregateRecord(record);
			logger.trace("deleteLICsisopaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLICsisopaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
