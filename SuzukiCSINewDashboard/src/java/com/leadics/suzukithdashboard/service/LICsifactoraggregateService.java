
/*
 * LICsifactoraggregateService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LICsifactoraggregateDAO;
import com.leadics.suzukithdashboard.to.LICsifactoraggregateRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LICsifactoraggregateService extends LIService
{
	static LogUtils logger = new LogUtils(LICsifactoraggregateService.class.getName());


	public LICsifactoraggregateRecord[] loadLICsifactoraggregateRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLICsifactoraggregateRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsifactoraggregateRecords", null);
			LICsifactoraggregateDAO dao = new LICsifactoraggregateDAO();
			LICsifactoraggregateRecord[] results = dao.loadLICsifactoraggregateRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLICsifactoraggregateRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLICsifactoraggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LICsifactoraggregateRecord loadFirstLICsifactoraggregateRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLICsifactoraggregateRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLICsifactoraggregateRecord", null);
			LICsifactoraggregateDAO dao = new LICsifactoraggregateDAO();
			LICsifactoraggregateRecord result = dao.loadFirstLICsifactoraggregateRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLICsifactoraggregateRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICsifactoraggregateRecord searchFirstLICsifactoraggregateRecord(LICsifactoraggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLICsifactoraggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLICsifactoraggregateRecord", null);
			LICsifactoraggregateDAO dao = new LICsifactoraggregateDAO();
			LICsifactoraggregateRecord[] records = dao.searchLICsifactoraggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLICsifactoraggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LICsifactoraggregateRecord searchFirstLICsifactoraggregateRecordExactUpper(LICsifactoraggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLICsifactoraggregateRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLICsifactoraggregateRecordsExactUpper", null);
			LICsifactoraggregateDAO dao = new LICsifactoraggregateDAO();
			LICsifactoraggregateRecord[] records = dao.searchLICsifactoraggregateRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLICsifactoraggregateRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICsifactoraggregateRecord[] searchLICsifactoraggregateRecords(LICsifactoraggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLICsifactoraggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsifactoraggregateRecords", null);
			LICsifactoraggregateDAO dao = new LICsifactoraggregateDAO();
			LICsifactoraggregateRecord[] records = dao.searchLICsifactoraggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLICsifactoraggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLICsifactoraggregateRecordCount(LICsifactoraggregateRecord record)
	throws Exception
	{
		return loadLICsifactoraggregateRecordCount(record, null);
	}


	public int loadLICsifactoraggregateRecordCount(LICsifactoraggregateRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLICsifactoraggregateRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsifactoraggregateRecordCount", null);
			LICsifactoraggregateDAO dao = new LICsifactoraggregateDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLICsifactoraggregateRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLICsifactoraggregateRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICsifactoraggregateRecord loadLICsifactoraggregateRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLICsifactoraggregateRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsifactoraggregateRecord", null);
			LICsifactoraggregateDAO dao = new LICsifactoraggregateDAO();
			LICsifactoraggregateRecord result = dao.loadLICsifactoraggregateRecord(key);
			logger.trace("loadLICsifactoraggregateRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLICsifactoraggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLICsifactoraggregateRecordSearchResultByPage(LICsifactoraggregateRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLICsifactoraggregateRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLICsifactoraggregateRecordSearchResultByPage(LICsifactoraggregateRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLICsifactoraggregateRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLICsifactoraggregateRecordSearchResult", null);
			LICsifactoraggregateDAO dao = new LICsifactoraggregateDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLICsifactoraggregateRecordCount(record);
			dao.setLimits(offset, maxrows);
			LICsifactoraggregateRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLICsifactoraggregateRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLICsifactoraggregateRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLICsifactoraggregateRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLICsifactoraggregateRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLICsifactoraggregateRecordSearchResultByPageQuery", null);
			LICsifactoraggregateDAO dao = new LICsifactoraggregateDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LICsifactoraggregateRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLICsifactoraggregateRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLICsifactoraggregateRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLICsifactoraggregateRecord(LICsifactoraggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLICsifactoraggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLICsifactoraggregateRecord", null);
			LICsifactoraggregateDAO dao = new LICsifactoraggregateDAO();
			int result = dao.insertLICsifactoraggregateRecord(record);
			logger.trace("insertLICsifactoraggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLICsifactoraggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLICsifactoraggregateRecord(LICsifactoraggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLICsifactoraggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLICsifactoraggregateRecord", null);
			LICsifactoraggregateDAO dao = new LICsifactoraggregateDAO();
			boolean result = dao.updateLICsifactoraggregateRecord(record);
			logger.trace("updateLICsifactoraggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLICsifactoraggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLICsifactoraggregateRecordNonNull(LICsifactoraggregateRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLICsifactoraggregateRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLICsifactoraggregateRecordNonNull", null);
			LICsifactoraggregateDAO dao = new LICsifactoraggregateDAO();
			LICsifactoraggregateRecord dbRecord = dao.loadLICsifactoraggregateRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLICsifactoraggregateRecord(dbRecord);
			logger.trace("updateLICsifactoraggregateRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLICsifactoraggregateRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLICsifactoraggregateRecord(LICsifactoraggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLICsifactoraggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLICsifactoraggregateRecord", null);
			LICsifactoraggregateDAO dao = new LICsifactoraggregateDAO();
			boolean result = dao.deleteLICsifactoraggregateRecord(record);
			logger.trace("deleteLICsifactoraggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLICsifactoraggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
