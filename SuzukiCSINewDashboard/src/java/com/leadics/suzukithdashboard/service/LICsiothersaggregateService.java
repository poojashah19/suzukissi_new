
/*
 * LICsiothersaggregateService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LICsiothersaggregateDAO;
import com.leadics.suzukithdashboard.to.LICsiothersaggregateRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LICsiothersaggregateService extends LIService
{
	static LogUtils logger = new LogUtils(LICsiothersaggregateService.class.getName());


	public LICsiothersaggregateRecord[] loadLICsiothersaggregateRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLICsiothersaggregateRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsiothersaggregateRecords", null);
			LICsiothersaggregateDAO dao = new LICsiothersaggregateDAO();
			LICsiothersaggregateRecord[] results = dao.loadLICsiothersaggregateRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLICsiothersaggregateRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLICsiothersaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LICsiothersaggregateRecord loadFirstLICsiothersaggregateRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLICsiothersaggregateRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLICsiothersaggregateRecord", null);
			LICsiothersaggregateDAO dao = new LICsiothersaggregateDAO();
			LICsiothersaggregateRecord result = dao.loadFirstLICsiothersaggregateRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLICsiothersaggregateRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICsiothersaggregateRecord searchFirstLICsiothersaggregateRecord(LICsiothersaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLICsiothersaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLICsiothersaggregateRecord", null);
			LICsiothersaggregateDAO dao = new LICsiothersaggregateDAO();
			LICsiothersaggregateRecord[] records = dao.searchLICsiothersaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLICsiothersaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LICsiothersaggregateRecord searchFirstLICsiothersaggregateRecordExactUpper(LICsiothersaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLICsiothersaggregateRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLICsiothersaggregateRecordsExactUpper", null);
			LICsiothersaggregateDAO dao = new LICsiothersaggregateDAO();
			LICsiothersaggregateRecord[] records = dao.searchLICsiothersaggregateRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLICsiothersaggregateRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICsiothersaggregateRecord[] searchLICsiothersaggregateRecords(LICsiothersaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLICsiothersaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsiothersaggregateRecords", null);
			LICsiothersaggregateDAO dao = new LICsiothersaggregateDAO();
			LICsiothersaggregateRecord[] records = dao.searchLICsiothersaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLICsiothersaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLICsiothersaggregateRecordCount(LICsiothersaggregateRecord record)
	throws Exception
	{
		return loadLICsiothersaggregateRecordCount(record, null);
	}


	public int loadLICsiothersaggregateRecordCount(LICsiothersaggregateRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLICsiothersaggregateRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsiothersaggregateRecordCount", null);
			LICsiothersaggregateDAO dao = new LICsiothersaggregateDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLICsiothersaggregateRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLICsiothersaggregateRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICsiothersaggregateRecord loadLICsiothersaggregateRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLICsiothersaggregateRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsiothersaggregateRecord", null);
			LICsiothersaggregateDAO dao = new LICsiothersaggregateDAO();
			LICsiothersaggregateRecord result = dao.loadLICsiothersaggregateRecord(key);
			logger.trace("loadLICsiothersaggregateRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLICsiothersaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLICsiothersaggregateRecordSearchResultByPage(LICsiothersaggregateRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLICsiothersaggregateRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLICsiothersaggregateRecordSearchResultByPage(LICsiothersaggregateRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLICsiothersaggregateRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLICsiothersaggregateRecordSearchResult", null);
			LICsiothersaggregateDAO dao = new LICsiothersaggregateDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLICsiothersaggregateRecordCount(record);
			dao.setLimits(offset, maxrows);
			LICsiothersaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLICsiothersaggregateRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLICsiothersaggregateRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLICsiothersaggregateRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLICsiothersaggregateRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLICsiothersaggregateRecordSearchResultByPageQuery", null);
			LICsiothersaggregateDAO dao = new LICsiothersaggregateDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LICsiothersaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLICsiothersaggregateRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLICsiothersaggregateRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLICsiothersaggregateRecord(LICsiothersaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLICsiothersaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLICsiothersaggregateRecord", null);
			LICsiothersaggregateDAO dao = new LICsiothersaggregateDAO();
			int result = dao.insertLICsiothersaggregateRecord(record);
			logger.trace("insertLICsiothersaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLICsiothersaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLICsiothersaggregateRecord(LICsiothersaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLICsiothersaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLICsiothersaggregateRecord", null);
			LICsiothersaggregateDAO dao = new LICsiothersaggregateDAO();
			boolean result = dao.updateLICsiothersaggregateRecord(record);
			logger.trace("updateLICsiothersaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLICsiothersaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLICsiothersaggregateRecordNonNull(LICsiothersaggregateRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLICsiothersaggregateRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLICsiothersaggregateRecordNonNull", null);
			LICsiothersaggregateDAO dao = new LICsiothersaggregateDAO();
			LICsiothersaggregateRecord dbRecord = dao.loadLICsiothersaggregateRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLICsiothersaggregateRecord(dbRecord);
			logger.trace("updateLICsiothersaggregateRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLICsiothersaggregateRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLICsiothersaggregateRecord(LICsiothersaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLICsiothersaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLICsiothersaggregateRecord", null);
			LICsiothersaggregateDAO dao = new LICsiothersaggregateDAO();
			boolean result = dao.deleteLICsiothersaggregateRecord(record);
			logger.trace("deleteLICsiothersaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLICsiothersaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
