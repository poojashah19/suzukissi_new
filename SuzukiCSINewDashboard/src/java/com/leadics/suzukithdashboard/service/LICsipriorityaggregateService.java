
/*
 * LICsipriorityaggregateService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LICsipriorityaggregateDAO;
import com.leadics.suzukithdashboard.to.LICsipriorityaggregateRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LICsipriorityaggregateService extends LIService
{
	static LogUtils logger = new LogUtils(LICsipriorityaggregateService.class.getName());


	public LICsipriorityaggregateRecord[] loadLICsipriorityaggregateRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLICsipriorityaggregateRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsipriorityaggregateRecords", null);
			LICsipriorityaggregateDAO dao = new LICsipriorityaggregateDAO();
			LICsipriorityaggregateRecord[] results = dao.loadLICsipriorityaggregateRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLICsipriorityaggregateRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLICsipriorityaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LICsipriorityaggregateRecord loadFirstLICsipriorityaggregateRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLICsipriorityaggregateRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLICsipriorityaggregateRecord", null);
			LICsipriorityaggregateDAO dao = new LICsipriorityaggregateDAO();
			LICsipriorityaggregateRecord result = dao.loadFirstLICsipriorityaggregateRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLICsipriorityaggregateRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICsipriorityaggregateRecord searchFirstLICsipriorityaggregateRecord(LICsipriorityaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLICsipriorityaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLICsipriorityaggregateRecord", null);
			LICsipriorityaggregateDAO dao = new LICsipriorityaggregateDAO();
			LICsipriorityaggregateRecord[] records = dao.searchLICsipriorityaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLICsipriorityaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LICsipriorityaggregateRecord searchFirstLICsipriorityaggregateRecordExactUpper(LICsipriorityaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLICsipriorityaggregateRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLICsipriorityaggregateRecordsExactUpper", null);
			LICsipriorityaggregateDAO dao = new LICsipriorityaggregateDAO();
			LICsipriorityaggregateRecord[] records = dao.searchLICsipriorityaggregateRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLICsipriorityaggregateRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICsipriorityaggregateRecord[] searchLICsipriorityaggregateRecords(LICsipriorityaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLICsipriorityaggregateRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsipriorityaggregateRecords", null);
			LICsipriorityaggregateDAO dao = new LICsipriorityaggregateDAO();
			LICsipriorityaggregateRecord[] records = dao.searchLICsipriorityaggregateRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLICsipriorityaggregateRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLICsipriorityaggregateRecordCount(LICsipriorityaggregateRecord record)
	throws Exception
	{
		return loadLICsipriorityaggregateRecordCount(record, null);
	}


	public int loadLICsipriorityaggregateRecordCount(LICsipriorityaggregateRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLICsipriorityaggregateRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsipriorityaggregateRecordCount", null);
			LICsipriorityaggregateDAO dao = new LICsipriorityaggregateDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLICsipriorityaggregateRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLICsipriorityaggregateRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LICsipriorityaggregateRecord loadLICsipriorityaggregateRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLICsipriorityaggregateRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLICsipriorityaggregateRecord", null);
			LICsipriorityaggregateDAO dao = new LICsipriorityaggregateDAO();
			LICsipriorityaggregateRecord result = dao.loadLICsipriorityaggregateRecord(key);
			logger.trace("loadLICsipriorityaggregateRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLICsipriorityaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLICsipriorityaggregateRecordSearchResultByPage(LICsipriorityaggregateRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLICsipriorityaggregateRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLICsipriorityaggregateRecordSearchResultByPage(LICsipriorityaggregateRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLICsipriorityaggregateRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLICsipriorityaggregateRecordSearchResult", null);
			LICsipriorityaggregateDAO dao = new LICsipriorityaggregateDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLICsipriorityaggregateRecordCount(record);
			dao.setLimits(offset, maxrows);
			LICsipriorityaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLICsipriorityaggregateRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLICsipriorityaggregateRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLICsipriorityaggregateRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLICsipriorityaggregateRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLICsipriorityaggregateRecordSearchResultByPageQuery", null);
			LICsipriorityaggregateDAO dao = new LICsipriorityaggregateDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LICsipriorityaggregateRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLICsipriorityaggregateRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLICsipriorityaggregateRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLICsipriorityaggregateRecord(LICsipriorityaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLICsipriorityaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLICsipriorityaggregateRecord", null);
			LICsipriorityaggregateDAO dao = new LICsipriorityaggregateDAO();
			int result = dao.insertLICsipriorityaggregateRecord(record);
			logger.trace("insertLICsipriorityaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLICsipriorityaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLICsipriorityaggregateRecord(LICsipriorityaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLICsipriorityaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLICsipriorityaggregateRecord", null);
			LICsipriorityaggregateDAO dao = new LICsipriorityaggregateDAO();
			boolean result = dao.updateLICsipriorityaggregateRecord(record);
			logger.trace("updateLICsipriorityaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLICsipriorityaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLICsipriorityaggregateRecordNonNull(LICsipriorityaggregateRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLICsipriorityaggregateRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLICsipriorityaggregateRecordNonNull", null);
			LICsipriorityaggregateDAO dao = new LICsipriorityaggregateDAO();
			LICsipriorityaggregateRecord dbRecord = dao.loadLICsipriorityaggregateRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLICsipriorityaggregateRecord(dbRecord);
			logger.trace("updateLICsipriorityaggregateRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLICsipriorityaggregateRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLICsipriorityaggregateRecord(LICsipriorityaggregateRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLICsipriorityaggregateRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLICsipriorityaggregateRecord", null);
			LICsipriorityaggregateDAO dao = new LICsipriorityaggregateDAO();
			boolean result = dao.deleteLICsipriorityaggregateRecord(record);
			logger.trace("deleteLICsipriorityaggregateRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLICsipriorityaggregateRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
