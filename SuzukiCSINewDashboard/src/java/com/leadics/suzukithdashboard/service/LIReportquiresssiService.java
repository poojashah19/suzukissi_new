
/*
 * LIReportquiresssiService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LIReportquiresssiDAO;
import com.leadics.suzukithdashboard.to.LIReportquiresssiRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIReportquiresssiService extends LIService
{
	static LogUtils logger = new LogUtils(LIReportquiresssiService.class.getName());


	public LIReportquiresssiRecord[] loadLIReportquiresssiRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIReportquiresssiRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIReportquiresssiRecords", null);
			LIReportquiresssiDAO dao = new LIReportquiresssiDAO();
			LIReportquiresssiRecord[] results = dao.loadLIReportquiresssiRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIReportquiresssiRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIReportquiresssiRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIReportquiresssiRecord loadFirstLIReportquiresssiRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIReportquiresssiRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIReportquiresssiRecord", null);
			LIReportquiresssiDAO dao = new LIReportquiresssiDAO();
			LIReportquiresssiRecord result = dao.loadFirstLIReportquiresssiRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIReportquiresssiRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIReportquiresssiRecord searchFirstLIReportquiresssiRecord(LIReportquiresssiRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIReportquiresssiRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIReportquiresssiRecord", null);
			LIReportquiresssiDAO dao = new LIReportquiresssiDAO();
			LIReportquiresssiRecord[] records = dao.searchLIReportquiresssiRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIReportquiresssiRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIReportquiresssiRecord searchFirstLIReportquiresssiRecordExactUpper(LIReportquiresssiRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIReportquiresssiRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIReportquiresssiRecordsExactUpper", null);
			LIReportquiresssiDAO dao = new LIReportquiresssiDAO();
			LIReportquiresssiRecord[] records = dao.searchLIReportquiresssiRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIReportquiresssiRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIReportquiresssiRecord[] searchLIReportquiresssiRecords(LIReportquiresssiRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIReportquiresssiRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIReportquiresssiRecords", null);
			LIReportquiresssiDAO dao = new LIReportquiresssiDAO();
			LIReportquiresssiRecord[] records = dao.searchLIReportquiresssiRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIReportquiresssiRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIReportquiresssiRecordCount(LIReportquiresssiRecord record)
	throws Exception
	{
		return loadLIReportquiresssiRecordCount(record, null);
	}


	public int loadLIReportquiresssiRecordCount(LIReportquiresssiRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIReportquiresssiRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIReportquiresssiRecordCount", null);
			LIReportquiresssiDAO dao = new LIReportquiresssiDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIReportquiresssiRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIReportquiresssiRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIReportquiresssiRecord loadLIReportquiresssiRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIReportquiresssiRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIReportquiresssiRecord", null);
			LIReportquiresssiDAO dao = new LIReportquiresssiDAO();
			LIReportquiresssiRecord result = dao.loadLIReportquiresssiRecord(key);
			logger.trace("loadLIReportquiresssiRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIReportquiresssiRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIReportquiresssiRecordSearchResultByPage(LIReportquiresssiRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIReportquiresssiRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIReportquiresssiRecordSearchResultByPage(LIReportquiresssiRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIReportquiresssiRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIReportquiresssiRecordSearchResult", null);
			LIReportquiresssiDAO dao = new LIReportquiresssiDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIReportquiresssiRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIReportquiresssiRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIReportquiresssiRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIReportquiresssiRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIReportquiresssiRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIReportquiresssiRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIReportquiresssiRecordSearchResultByPageQuery", null);
			LIReportquiresssiDAO dao = new LIReportquiresssiDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIReportquiresssiRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIReportquiresssiRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIReportquiresssiRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIReportquiresssiRecord(LIReportquiresssiRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIReportquiresssiRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIReportquiresssiRecord", null);
			LIReportquiresssiDAO dao = new LIReportquiresssiDAO();
			int result = dao.insertLIReportquiresssiRecord(record);
			logger.trace("insertLIReportquiresssiRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIReportquiresssiRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIReportquiresssiRecord(LIReportquiresssiRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIReportquiresssiRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIReportquiresssiRecord", null);
			LIReportquiresssiDAO dao = new LIReportquiresssiDAO();
			boolean result = dao.updateLIReportquiresssiRecord(record);
			logger.trace("updateLIReportquiresssiRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIReportquiresssiRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIReportquiresssiRecordNonNull(LIReportquiresssiRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIReportquiresssiRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIReportquiresssiRecordNonNull", null);
			LIReportquiresssiDAO dao = new LIReportquiresssiDAO();
			LIReportquiresssiRecord dbRecord = dao.loadLIReportquiresssiRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIReportquiresssiRecord(dbRecord);
			logger.trace("updateLIReportquiresssiRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIReportquiresssiRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIReportquiresssiRecord(LIReportquiresssiRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIReportquiresssiRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIReportquiresssiRecord", null);
			LIReportquiresssiDAO dao = new LIReportquiresssiDAO();
			boolean result = dao.deleteLIReportquiresssiRecord(record);
			logger.trace("deleteLIReportquiresssiRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIReportquiresssiRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
