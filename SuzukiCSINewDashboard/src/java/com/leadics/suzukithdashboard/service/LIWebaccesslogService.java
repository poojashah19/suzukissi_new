
/*
 * LIWebaccesslogService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LIWebaccesslogDAO;
import com.leadics.suzukithdashboard.to.LIWebaccesslogRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIWebaccesslogService extends LIService
{
	static LogUtils logger = new LogUtils(LIWebaccesslogService.class.getName());


	public LIWebaccesslogRecord[] loadLIWebaccesslogRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIWebaccesslogRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIWebaccesslogRecords", null);
			LIWebaccesslogDAO dao = new LIWebaccesslogDAO();
			LIWebaccesslogRecord[] results = dao.loadLIWebaccesslogRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIWebaccesslogRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIWebaccesslogRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIWebaccesslogRecord loadFirstLIWebaccesslogRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIWebaccesslogRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIWebaccesslogRecord", null);
			LIWebaccesslogDAO dao = new LIWebaccesslogDAO();
			LIWebaccesslogRecord result = dao.loadFirstLIWebaccesslogRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIWebaccesslogRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIWebaccesslogRecord searchFirstLIWebaccesslogRecord(LIWebaccesslogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIWebaccesslogRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIWebaccesslogRecord", null);
			LIWebaccesslogDAO dao = new LIWebaccesslogDAO();
			LIWebaccesslogRecord[] records = dao.searchLIWebaccesslogRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIWebaccesslogRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIWebaccesslogRecord searchFirstLIWebaccesslogRecordExactUpper(LIWebaccesslogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIWebaccesslogRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIWebaccesslogRecordsExactUpper", null);
			LIWebaccesslogDAO dao = new LIWebaccesslogDAO();
			LIWebaccesslogRecord[] records = dao.searchLIWebaccesslogRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIWebaccesslogRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIWebaccesslogRecord[] searchLIWebaccesslogRecords(LIWebaccesslogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIWebaccesslogRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIWebaccesslogRecords", null);
			LIWebaccesslogDAO dao = new LIWebaccesslogDAO();
			LIWebaccesslogRecord[] records = dao.searchLIWebaccesslogRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIWebaccesslogRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIWebaccesslogRecordCount(LIWebaccesslogRecord record)
	throws Exception
	{
		return loadLIWebaccesslogRecordCount(record, null);
	}


	public int loadLIWebaccesslogRecordCount(LIWebaccesslogRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIWebaccesslogRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIWebaccesslogRecordCount", null);
			LIWebaccesslogDAO dao = new LIWebaccesslogDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIWebaccesslogRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIWebaccesslogRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIWebaccesslogRecord loadLIWebaccesslogRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIWebaccesslogRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIWebaccesslogRecord", null);
			LIWebaccesslogDAO dao = new LIWebaccesslogDAO();
			LIWebaccesslogRecord result = dao.loadLIWebaccesslogRecord(key);
			logger.trace("loadLIWebaccesslogRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIWebaccesslogRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIWebaccesslogRecordSearchResultByPage(LIWebaccesslogRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIWebaccesslogRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIWebaccesslogRecordSearchResultByPage(LIWebaccesslogRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIWebaccesslogRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIWebaccesslogRecordSearchResult", null);
			LIWebaccesslogDAO dao = new LIWebaccesslogDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIWebaccesslogRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIWebaccesslogRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIWebaccesslogRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIWebaccesslogRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIWebaccesslogRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIWebaccesslogRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIWebaccesslogRecordSearchResultByPageQuery", null);
			LIWebaccesslogDAO dao = new LIWebaccesslogDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIWebaccesslogRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIWebaccesslogRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIWebaccesslogRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIWebaccesslogRecord(LIWebaccesslogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIWebaccesslogRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIWebaccesslogRecord", null);
			LIWebaccesslogDAO dao = new LIWebaccesslogDAO();
			int result = dao.insertLIWebaccesslogRecord(record);
			logger.trace("insertLIWebaccesslogRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIWebaccesslogRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIWebaccesslogRecord(LIWebaccesslogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIWebaccesslogRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIWebaccesslogRecord", null);
			LIWebaccesslogDAO dao = new LIWebaccesslogDAO();
			boolean result = dao.updateLIWebaccesslogRecord(record);
			logger.trace("updateLIWebaccesslogRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIWebaccesslogRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIWebaccesslogRecordNonNull(LIWebaccesslogRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIWebaccesslogRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIWebaccesslogRecordNonNull", null);
			LIWebaccesslogDAO dao = new LIWebaccesslogDAO();
			LIWebaccesslogRecord dbRecord = dao.loadLIWebaccesslogRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIWebaccesslogRecord(dbRecord);
			logger.trace("updateLIWebaccesslogRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIWebaccesslogRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIWebaccesslogRecord(LIWebaccesslogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIWebaccesslogRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIWebaccesslogRecord", null);
			LIWebaccesslogDAO dao = new LIWebaccesslogDAO();
			boolean result = dao.deleteLIWebaccesslogRecord(record);
			logger.trace("deleteLIWebaccesslogRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIWebaccesslogRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
