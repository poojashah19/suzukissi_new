
/*
 * LIConsolefailedloginService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LIConsolefailedloginDAO;
import com.leadics.suzukithdashboard.to.LIConsolefailedloginRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIConsolefailedloginService extends LIService
{
	static LogUtils logger = new LogUtils(LIConsolefailedloginService.class.getName());


	public LIConsolefailedloginRecord[] loadLIConsolefailedloginRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIConsolefailedloginRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConsolefailedloginRecords", null);
			LIConsolefailedloginDAO dao = new LIConsolefailedloginDAO();
			LIConsolefailedloginRecord[] results = dao.loadLIConsolefailedloginRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIConsolefailedloginRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIConsolefailedloginRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIConsolefailedloginRecord loadFirstLIConsolefailedloginRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIConsolefailedloginRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIConsolefailedloginRecord", null);
			LIConsolefailedloginDAO dao = new LIConsolefailedloginDAO();
			LIConsolefailedloginRecord result = dao.loadFirstLIConsolefailedloginRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIConsolefailedloginRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIConsolefailedloginRecord searchFirstLIConsolefailedloginRecord(LIConsolefailedloginRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIConsolefailedloginRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIConsolefailedloginRecord", null);
			LIConsolefailedloginDAO dao = new LIConsolefailedloginDAO();
			LIConsolefailedloginRecord[] records = dao.searchLIConsolefailedloginRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIConsolefailedloginRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIConsolefailedloginRecord searchFirstLIConsolefailedloginRecordExactUpper(LIConsolefailedloginRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIConsolefailedloginRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIConsolefailedloginRecordsExactUpper", null);
			LIConsolefailedloginDAO dao = new LIConsolefailedloginDAO();
			LIConsolefailedloginRecord[] records = dao.searchLIConsolefailedloginRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIConsolefailedloginRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIConsolefailedloginRecord[] searchLIConsolefailedloginRecords(LIConsolefailedloginRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIConsolefailedloginRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConsolefailedloginRecords", null);
			LIConsolefailedloginDAO dao = new LIConsolefailedloginDAO();
			LIConsolefailedloginRecord[] records = dao.searchLIConsolefailedloginRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIConsolefailedloginRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIConsolefailedloginRecordCount(LIConsolefailedloginRecord record)
	throws Exception
	{
		return loadLIConsolefailedloginRecordCount(record, null);
	}


	public int loadLIConsolefailedloginRecordCount(LIConsolefailedloginRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIConsolefailedloginRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConsolefailedloginRecordCount", null);
			LIConsolefailedloginDAO dao = new LIConsolefailedloginDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIConsolefailedloginRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIConsolefailedloginRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIConsolefailedloginRecord loadLIConsolefailedloginRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIConsolefailedloginRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConsolefailedloginRecord", null);
			LIConsolefailedloginDAO dao = new LIConsolefailedloginDAO();
			LIConsolefailedloginRecord result = dao.loadLIConsolefailedloginRecord(key);
			logger.trace("loadLIConsolefailedloginRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIConsolefailedloginRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIConsolefailedloginRecordSearchResultByPage(LIConsolefailedloginRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIConsolefailedloginRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIConsolefailedloginRecordSearchResultByPage(LIConsolefailedloginRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIConsolefailedloginRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIConsolefailedloginRecordSearchResult", null);
			LIConsolefailedloginDAO dao = new LIConsolefailedloginDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIConsolefailedloginRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIConsolefailedloginRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIConsolefailedloginRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIConsolefailedloginRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIConsolefailedloginRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIConsolefailedloginRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIConsolefailedloginRecordSearchResultByPageQuery", null);
			LIConsolefailedloginDAO dao = new LIConsolefailedloginDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIConsolefailedloginRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIConsolefailedloginRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIConsolefailedloginRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIConsolefailedloginRecord(LIConsolefailedloginRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIConsolefailedloginRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIConsolefailedloginRecord", null);
			LIConsolefailedloginDAO dao = new LIConsolefailedloginDAO();
			int result = dao.insertLIConsolefailedloginRecord(record);
			logger.trace("insertLIConsolefailedloginRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIConsolefailedloginRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIConsolefailedloginRecord(LIConsolefailedloginRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIConsolefailedloginRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIConsolefailedloginRecord", null);
			LIConsolefailedloginDAO dao = new LIConsolefailedloginDAO();
			boolean result = dao.updateLIConsolefailedloginRecord(record);
			logger.trace("updateLIConsolefailedloginRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIConsolefailedloginRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIConsolefailedloginRecordNonNull(LIConsolefailedloginRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIConsolefailedloginRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIConsolefailedloginRecordNonNull", null);
			LIConsolefailedloginDAO dao = new LIConsolefailedloginDAO();
			LIConsolefailedloginRecord dbRecord = dao.loadLIConsolefailedloginRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIConsolefailedloginRecord(dbRecord);
			logger.trace("updateLIConsolefailedloginRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIConsolefailedloginRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIConsolefailedloginRecord(LIConsolefailedloginRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIConsolefailedloginRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIConsolefailedloginRecord", null);
			LIConsolefailedloginDAO dao = new LIConsolefailedloginDAO();
			boolean result = dao.deleteLIConsolefailedloginRecord(record);
			logger.trace("deleteLIConsolefailedloginRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIConsolefailedloginRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
