
/*
 * LIConsoleaccesslogService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LIConsoleaccesslogDAO;
import com.leadics.suzukithdashboard.to.LIConsoleaccesslogRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIConsoleaccesslogService extends LIService
{
	static LogUtils logger = new LogUtils(LIConsoleaccesslogService.class.getName());


	public LIConsoleaccesslogRecord[] loadLIConsoleaccesslogRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIConsoleaccesslogRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConsoleaccesslogRecords", null);
			LIConsoleaccesslogDAO dao = new LIConsoleaccesslogDAO();
			LIConsoleaccesslogRecord[] results = dao.loadLIConsoleaccesslogRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIConsoleaccesslogRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIConsoleaccesslogRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIConsoleaccesslogRecord loadFirstLIConsoleaccesslogRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIConsoleaccesslogRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIConsoleaccesslogRecord", null);
			LIConsoleaccesslogDAO dao = new LIConsoleaccesslogDAO();
			LIConsoleaccesslogRecord result = dao.loadFirstLIConsoleaccesslogRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIConsoleaccesslogRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIConsoleaccesslogRecord searchFirstLIConsoleaccesslogRecord(LIConsoleaccesslogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIConsoleaccesslogRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIConsoleaccesslogRecord", null);
			LIConsoleaccesslogDAO dao = new LIConsoleaccesslogDAO();
			LIConsoleaccesslogRecord[] records = dao.searchLIConsoleaccesslogRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIConsoleaccesslogRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIConsoleaccesslogRecord searchFirstLIConsoleaccesslogRecordExactUpper(LIConsoleaccesslogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIConsoleaccesslogRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIConsoleaccesslogRecordsExactUpper", null);
			LIConsoleaccesslogDAO dao = new LIConsoleaccesslogDAO();
			LIConsoleaccesslogRecord[] records = dao.searchLIConsoleaccesslogRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIConsoleaccesslogRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIConsoleaccesslogRecord[] searchLIConsoleaccesslogRecords(LIConsoleaccesslogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIConsoleaccesslogRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConsoleaccesslogRecords", null);
			LIConsoleaccesslogDAO dao = new LIConsoleaccesslogDAO();
			LIConsoleaccesslogRecord[] records = dao.searchLIConsoleaccesslogRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIConsoleaccesslogRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIConsoleaccesslogRecordCount(LIConsoleaccesslogRecord record)
	throws Exception
	{
		return loadLIConsoleaccesslogRecordCount(record, null);
	}


	public int loadLIConsoleaccesslogRecordCount(LIConsoleaccesslogRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIConsoleaccesslogRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConsoleaccesslogRecordCount", null);
			LIConsoleaccesslogDAO dao = new LIConsoleaccesslogDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIConsoleaccesslogRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIConsoleaccesslogRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIConsoleaccesslogRecord loadLIConsoleaccesslogRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIConsoleaccesslogRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIConsoleaccesslogRecord", null);
			LIConsoleaccesslogDAO dao = new LIConsoleaccesslogDAO();
			LIConsoleaccesslogRecord result = dao.loadLIConsoleaccesslogRecord(key);
			logger.trace("loadLIConsoleaccesslogRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIConsoleaccesslogRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIConsoleaccesslogRecordSearchResultByPage(LIConsoleaccesslogRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIConsoleaccesslogRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIConsoleaccesslogRecordSearchResultByPage(LIConsoleaccesslogRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIConsoleaccesslogRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIConsoleaccesslogRecordSearchResult", null);
			LIConsoleaccesslogDAO dao = new LIConsoleaccesslogDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIConsoleaccesslogRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIConsoleaccesslogRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIConsoleaccesslogRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIConsoleaccesslogRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIConsoleaccesslogRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIConsoleaccesslogRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIConsoleaccesslogRecordSearchResultByPageQuery", null);
			LIConsoleaccesslogDAO dao = new LIConsoleaccesslogDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIConsoleaccesslogRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIConsoleaccesslogRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIConsoleaccesslogRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIConsoleaccesslogRecord(LIConsoleaccesslogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIConsoleaccesslogRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIConsoleaccesslogRecord", null);
			LIConsoleaccesslogDAO dao = new LIConsoleaccesslogDAO();
			int result = dao.insertLIConsoleaccesslogRecord(record);
			logger.trace("insertLIConsoleaccesslogRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIConsoleaccesslogRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIConsoleaccesslogRecord(LIConsoleaccesslogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIConsoleaccesslogRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIConsoleaccesslogRecord", null);
			LIConsoleaccesslogDAO dao = new LIConsoleaccesslogDAO();
			boolean result = dao.updateLIConsoleaccesslogRecord(record);
			logger.trace("updateLIConsoleaccesslogRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIConsoleaccesslogRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIConsoleaccesslogRecordNonNull(LIConsoleaccesslogRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIConsoleaccesslogRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIConsoleaccesslogRecordNonNull", null);
			LIConsoleaccesslogDAO dao = new LIConsoleaccesslogDAO();
			LIConsoleaccesslogRecord dbRecord = dao.loadLIConsoleaccesslogRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIConsoleaccesslogRecord(dbRecord);
			logger.trace("updateLIConsoleaccesslogRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIConsoleaccesslogRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIConsoleaccesslogRecord(LIConsoleaccesslogRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIConsoleaccesslogRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIConsoleaccesslogRecord", null);
			LIConsoleaccesslogDAO dao = new LIConsoleaccesslogDAO();
			boolean result = dao.deleteLIConsoleaccesslogRecord(record);
			logger.trace("deleteLIConsoleaccesslogRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIConsoleaccesslogRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
