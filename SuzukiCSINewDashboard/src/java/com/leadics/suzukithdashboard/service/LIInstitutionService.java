
/*
 * LIInstitutionService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LIInstitutionDAO;
import com.leadics.suzukithdashboard.to.LIInstitutionRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LIInstitutionService extends LIService
{
	static LogUtils logger = new LogUtils(LIInstitutionService.class.getName());


	public LIInstitutionRecord[] loadLIInstitutionRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLIInstitutionRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIInstitutionRecords", null);
			LIInstitutionDAO dao = new LIInstitutionDAO();
			LIInstitutionRecord[] results = dao.loadLIInstitutionRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLIInstitutionRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLIInstitutionRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIInstitutionRecord loadFirstLIInstitutionRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLIInstitutionRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLIInstitutionRecord", null);
			LIInstitutionDAO dao = new LIInstitutionDAO();
			LIInstitutionRecord result = dao.loadFirstLIInstitutionRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLIInstitutionRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIInstitutionRecord searchFirstLIInstitutionRecord(LIInstitutionRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIInstitutionRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIInstitutionRecord", null);
			LIInstitutionDAO dao = new LIInstitutionDAO();
			LIInstitutionRecord[] records = dao.searchLIInstitutionRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIInstitutionRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LIInstitutionRecord searchFirstLIInstitutionRecordExactUpper(LIInstitutionRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLIInstitutionRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLIInstitutionRecordsExactUpper", null);
			LIInstitutionDAO dao = new LIInstitutionDAO();
			LIInstitutionRecord[] records = dao.searchLIInstitutionRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLIInstitutionRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIInstitutionRecord[] searchLIInstitutionRecords(LIInstitutionRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLIInstitutionRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIInstitutionRecords", null);
			LIInstitutionDAO dao = new LIInstitutionDAO();
			LIInstitutionRecord[] records = dao.searchLIInstitutionRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLIInstitutionRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLIInstitutionRecordCount(LIInstitutionRecord record)
	throws Exception
	{
		return loadLIInstitutionRecordCount(record, null);
	}


	public int loadLIInstitutionRecordCount(LIInstitutionRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLIInstitutionRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIInstitutionRecordCount", null);
			LIInstitutionDAO dao = new LIInstitutionDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLIInstitutionRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLIInstitutionRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LIInstitutionRecord loadLIInstitutionRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLIInstitutionRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLIInstitutionRecord", null);
			LIInstitutionDAO dao = new LIInstitutionDAO();
			LIInstitutionRecord result = dao.loadLIInstitutionRecord(key);
			logger.trace("loadLIInstitutionRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLIInstitutionRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLIInstitutionRecordSearchResultByPage(LIInstitutionRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLIInstitutionRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLIInstitutionRecordSearchResultByPage(LIInstitutionRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIInstitutionRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIInstitutionRecordSearchResult", null);
			LIInstitutionDAO dao = new LIInstitutionDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLIInstitutionRecordCount(record);
			dao.setLimits(offset, maxrows);
			LIInstitutionRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLIInstitutionRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIInstitutionRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLIInstitutionRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLIInstitutionRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLIInstitutionRecordSearchResultByPageQuery", null);
			LIInstitutionDAO dao = new LIInstitutionDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LIInstitutionRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLIInstitutionRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLIInstitutionRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int insertLIInstitutionRecord(LIInstitutionRecord record)
	throws Exception
	{
		try
		{
			logger.trace("insertLIInstitutionRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("insertLIInstitutionRecord", null);
			LIInstitutionDAO dao = new LIInstitutionDAO();
			int result = dao.insertLIInstitutionRecord(record);
			logger.trace("insertLIInstitutionRecord:Result:" + result);			
			createMakerCheckerAuditEntry("institution",result + "", "Create",record.getCreatedby(),"Created");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("insertLIInstitutionRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIInstitutionRecord(LIInstitutionRecord record)
	throws Exception
	{
		try
		{
			logger.trace("updateLIInstitutionRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIInstitutionRecord", null);
			LIInstitutionDAO dao = new LIInstitutionDAO();
			boolean result = dao.updateLIInstitutionRecord(record);
			createMakerCheckerAuditEntry("institution",record.getId(), "Update",record.getModifiedby(),"Update");
			logger.trace("updateLIInstitutionRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIInstitutionRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean updateLIInstitutionRecordNonNull(LIInstitutionRecord inputRecord)
	throws Exception
	{
		try
		{
			logger.trace("updateLIInstitutionRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("updateLIInstitutionRecordNonNull", null);
			LIInstitutionDAO dao = new LIInstitutionDAO();
			LIInstitutionRecord dbRecord = dao.loadLIInstitutionRecord(inputRecord.getId());
			if (dbRecord == null)
			{
				throw new Exception("Record not found");
			}
			dbRecord.loadNonNullContent(inputRecord);
			dbRecord.setActionSource(inputRecord.getActionSource());
			boolean result = dao.updateLIInstitutionRecord(dbRecord);
			createMakerCheckerAuditEntry("institution",inputRecord.getId(), "Update",dbRecord.getModifiedby(),"Update");
			logger.trace("updateLIInstitutionRecordNonNull:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("updateLIInstitutionRecordNonNull" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean deleteLIInstitutionRecord(LIInstitutionRecord record)
	throws Exception
	{
		try
		{
			logger.trace("deleteLIInstitutionRecord:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("deleteLIInstitutionRecord", null);
			LIInstitutionDAO dao = new LIInstitutionDAO();
			boolean result = dao.deleteLIInstitutionRecord(record);
			logger.trace("deleteLIInstitutionRecord:Result:" + result);			
			createMakerCheckerAuditEntry("institution",record.getId(), "Delete",null,"Deleted");
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("deleteLIInstitutionRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean approveLIInstitutionRecord(LIInstitutionRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("approveLIInstitutionRecord:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("approveLIInstitutionRecord", null);
			LIInstitutionDAO dao = new LIInstitutionDAO();
			LIInstitutionRecord updateRecord = dao.loadLIInstitutionRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CA-RNFA104:Record not found");
			}
			if (!isValidStatusForApproval(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CA-IS101:Cannot approve - Invalid Status for Approval");
			}
			if (StringUtils.isSame(updateRecord.getMadeby(), updateId))
			{
				throw new Exception("MF-ERR-CA-MCS102:Cannot Approve. Maker Checker cannot be same");
			}
			updateRecord.setRstatus("1");
			updateRecord.setCheckerlastcmt(comment);
			updateRecord.setCurrappstatus("1");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setCheckedat(DateUtils.getCurrentDateTime());
			updateRecord.setCheckedby(updateId);
			boolean result = dao.updateLIInstitutionRecord(updateRecord);
			logger.trace("approveLIInstitutionRecord:Result:" + result);			
			createMakerCheckerAuditEntry("institution",updateRecord.getId(), "Approve",updateId,comment);
			HashMap approveEventMap = callActionEvent("Event_OnApprovalOf_LIInstitutionRecord", updateRecord.getId());
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("approveLIInstitutionRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean submitLIInstitutionRecord(LIInstitutionRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("submitLIInstitutionRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("submitLIInstitutionRecordNonNull", null);
			LIInstitutionDAO dao = new LIInstitutionDAO();
			LIInstitutionRecord updateRecord = dao.loadLIInstitutionRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CS-RNFA104:Record not found");
			}
			if (!isValidStatusForSubmission(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CS-IS101:Cannot submit - Invalid Status for Submission");
			}
			updateRecord.setRstatus("1");
			updateRecord.setMakerlastcmt(comment);
			updateRecord.setCurrappstatus("4");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setMadeat(DateUtils.getCurrentDateTime());
			updateRecord.setMadeby(updateId);
			boolean result = dao.updateLIInstitutionRecord(updateRecord);
			createMakerCheckerAuditEntry("institution",updateRecord.getId(),"Submit",updateId,comment);
			HashMap submitEventMap = callActionEvent("Event_OnSubmitOf_LIInstitutionRecord", updateRecord.getId());
			logger.trace("submitLIInstitutionRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("submitLIInstitutionRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean denyLIInstitutionRecord(LIInstitutionRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("denyLIInstitutionRecord:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("denyLIInstitutionRecord", null);
			LIInstitutionDAO dao = new LIInstitutionDAO();
			LIInstitutionRecord updateRecord = dao.loadLIInstitutionRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CD-RNFA104:Record not found");
			}
			if (!isValidStatusForDeny(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CD-IS101:Cannot deny - Invalid Status for Deny");
			}
			if (StringUtils.isSame(updateRecord.getMadeby(), updateId))
			{
				throw new Exception("MF-ERR-CD-MCS101:Cannot Deny. Maker Checker cannot be same");
			}
			updateRecord.setCheckerlastcmt(comment);
			updateRecord.setCurrappstatus("5");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setCheckedat(DateUtils.getCurrentDateTime());
			updateRecord.setCheckedby(updateId);
			boolean result = dao.updateLIInstitutionRecord(updateRecord);
			createMakerCheckerAuditEntry("institution",updateRecord.getId(),"Deny",updateId,comment);
			HashMap denyEventMap = callActionEvent("Event_OnDenyOf_LIInstitutionRecord", updateRecord.getId());
			logger.trace("denyLIInstitutionRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("denyLIInstitutionRecord" + getStackTrace(exception));
			throw exception;
		}
	}
	public boolean denyPermanantlyLIInstitutionRecord(LIInstitutionRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("denyPermanantlyLIInstitutionRecord:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("denyPermanantlyLIInstitutionRecord", null);
			LIInstitutionDAO dao = new LIInstitutionDAO();
			LIInstitutionRecord updateRecord = dao.loadLIInstitutionRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CD-RNFADP104:Record not found");
			}
			if (!isValidStatusForDeny(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CD-ISDP101:Cannot deny - Invalid Status for Deny");
			}
			if (StringUtils.isSame(updateRecord.getMadeby(), updateId))
			{
				throw new Exception("MF-ERR-CD-MCS101DP:Cannot Deny. Maker Checker cannot be same");
			}
			updateRecord.setCheckerlastcmt(comment);
			updateRecord.setCurrappstatus("58");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setCheckedat(DateUtils.getCurrentDateTime());
			updateRecord.setCheckedby(updateId);
			boolean result = dao.updateLIInstitutionRecord(updateRecord);
			createMakerCheckerAuditEntry("institution",updateRecord.getId(),"DenyP",updateId,comment);
			HashMap denyEventMap = callActionEvent("Event_OnDenyPOf_LIInstitutionRecord", updateRecord.getId());
			logger.trace("denyPLIInstitutionRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("denyPLIInstitutionRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean remindApprovalLIInstitutionRecord(LIInstitutionRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("remindApprovalLIInstitutionRecord:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("remindApprovalLIInstitutionRecord", null);
			LIInstitutionDAO dao = new LIInstitutionDAO();
			LIInstitutionRecord updateRecord = dao.loadLIInstitutionRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-CR-RNFA104:Record not found");
			}
			if (!isValidStatusForApproval(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-CR-IS101:Cannot Remind - Invalid Status for Reminder");
			}
			createMakerCheckerAuditEntry("institution",updateRecord.getId(), "Approval Reminder",updateId,comment);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return true;
		}
		catch(Exception exception)
		{
			logger.error("remindApprovalLIInstitutionRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public boolean resetApprovalLIInstitutionRecord(LIInstitutionRecord inputRecord, String updateId, String comment)
	throws Exception
	{
		try
		{
			logger.trace("resetApprovalLIInstitutionRecordNonNull:" + inputRecord);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("resetApprovalLIInstitutionRecordNonNull", null);
			LIInstitutionDAO dao = new LIInstitutionDAO();
			LIInstitutionRecord updateRecord = dao.loadLIInstitutionRecord(inputRecord.getId());
			if (updateRecord == null)
			{
				throw new Exception("MF-ERR-RA-RNFA104:Record not found");
			}
			if (!isValidStatusForReset(updateRecord.getCurrappstatus()))
			{
				throw new Exception("MF-ERR-RA-IS101:Cannot submit - Invalid Status for Reset");
			}
			updateRecord.setRstatus("0");
			updateRecord.setAdminlastcmt(comment);
			updateRecord.setCurrappstatus("0");
			updateRecord.setModifiedat(DateUtils.getCurrentDateTime());
			updateRecord.setModifiedby(updateId);
			updateRecord.setMadeat(DateUtils.getCurrentDateTime());
			updateRecord.setMadeby(updateId);
			boolean result = dao.updateLIInstitutionRecord(updateRecord);
			createMakerCheckerAuditEntry("institution",updateRecord.getId(),"Reset Approval",updateId,comment);
			logger.trace("resetApprovalLIInstitutionRecord:Result:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("resetApprovalLIInstitutionRecord" + getStackTrace(exception));
			throw exception;
		}
	}
}
