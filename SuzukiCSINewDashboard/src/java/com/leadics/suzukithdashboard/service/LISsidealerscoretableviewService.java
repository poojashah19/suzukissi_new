
/*
 * LISsidealerscoretableviewService.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.service;
import com.leadics.suzukithdashboard.common.LIService;
import com.leadics.suzukithdashboard.dao.LISsidealerscoretableviewDAO;
import com.leadics.suzukithdashboard.to.LISsidealerscoretableviewRecord;
import java.util.*;
import org.slf4j.*;
import java.sql.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import com.leadics.utils.*;
public class LISsidealerscoretableviewService extends LIService
{
	static LogUtils logger = new LogUtils(LISsidealerscoretableviewService.class.getName());


	public LISsidealerscoretableviewRecord[] loadLISsidealerscoretableviewRecords(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadLISsidealerscoretableviewRecords:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsidealerscoretableviewRecords", null);
			LISsidealerscoretableviewDAO dao = new LISsidealerscoretableviewDAO();
			LISsidealerscoretableviewRecord[] results = dao.loadLISsidealerscoretableviewRecords(query);
			int resultRecordCount = 0;
			if (results != null) resultRecordCount = results.length;
			logger.trace("loadLISsidealerscoretableviewRecords:Fetched" + resultRecordCount);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return results;
		}
		catch(Exception exception)
		{
			logger.error("loadLISsidealerscoretableviewRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LISsidealerscoretableviewRecord loadFirstLISsidealerscoretableviewRecord(String query)
	throws Exception
	{
		try
		{
			logger.trace("loadFirstLISsidealerscoretableviewRecord:" + query);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadFirstLISsidealerscoretableviewRecord", null);
			LISsidealerscoretableviewDAO dao = new LISsidealerscoretableviewDAO();
			LISsidealerscoretableviewRecord result = dao.loadFirstLISsidealerscoretableviewRecord(query);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadFirstLISsidealerscoretableviewRecord" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISsidealerscoretableviewRecord searchFirstLISsidealerscoretableviewRecord(LISsidealerscoretableviewRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLISsidealerscoretableviewRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLISsidealerscoretableviewRecord", null);
			LISsidealerscoretableviewDAO dao = new LISsidealerscoretableviewDAO();
			LISsidealerscoretableviewRecord[] records = dao.searchLISsidealerscoretableviewRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLISsidealerscoretableviewRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public LISsidealerscoretableviewRecord searchFirstLISsidealerscoretableviewRecordExactUpper(LISsidealerscoretableviewRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchFirstLISsidealerscoretableviewRecordsExactUpper:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("searchFirstLISsidealerscoretableviewRecordsExactUpper", null);
			LISsidealerscoretableviewDAO dao = new LISsidealerscoretableviewDAO();
			LISsidealerscoretableviewRecord[] records = dao.searchLISsidealerscoretableviewRecordsExactUpper(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			if(records == null) return null;
			if(records.length < 1) return null;
			return records[0];
		}
		catch(Exception exception)
		{
			logger.error("searchFirstLISsidealerscoretableviewRecordsExactUpper" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISsidealerscoretableviewRecord[] searchLISsidealerscoretableviewRecords(LISsidealerscoretableviewRecord record)
	throws Exception
	{
		try
		{
			logger.trace("searchLISsidealerscoretableviewRecords:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsidealerscoretableviewRecords", null);
			LISsidealerscoretableviewDAO dao = new LISsidealerscoretableviewDAO();
			LISsidealerscoretableviewRecord[] records = dao.searchLISsidealerscoretableviewRecords(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return records;
		}
		catch(Exception exception)
		{
			logger.error("searchLISsidealerscoretableviewRecords" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public int loadLISsidealerscoretableviewRecordCount(LISsidealerscoretableviewRecord record)
	throws Exception
	{
		return loadLISsidealerscoretableviewRecordCount(record, null);
	}


	public int loadLISsidealerscoretableviewRecordCount(LISsidealerscoretableviewRecord record, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("loadLISsidealerscoretableviewRecordCount:" + record);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsidealerscoretableviewRecordCount", null);
			LISsidealerscoretableviewDAO dao = new LISsidealerscoretableviewDAO();
			dao.setCustomCondition(customCondition);
			int resultcount = dao.loadLISsidealerscoretableviewRecordCount(record);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultcount;
		}
		catch(Exception exception)
		{
			logger.error("loadLISsidealerscoretableviewRecordCount" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}

	public LISsidealerscoretableviewRecord loadLISsidealerscoretableviewRecord(String key)
	throws Exception
	{
		try
		{
			logger.trace("loadLISsidealerscoretableviewRecord:" + key);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("loadLISsidealerscoretableviewRecord", null);
			LISsidealerscoretableviewDAO dao = new LISsidealerscoretableviewDAO();
			LISsidealerscoretableviewRecord result = dao.loadLISsidealerscoretableviewRecord(key);
			logger.trace("loadLISsidealerscoretableviewRecord:" + result);			
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return result;
		}
		catch(Exception exception)
		{
			logger.error("loadLISsidealerscoretableviewRecord" + getStackTrace(exception));
			throw exception;
		}
	}

	public JSONObject getJSONLISsidealerscoretableviewRecordSearchResultByPage(LISsidealerscoretableviewRecord record, String offset, String maxrows, String orderBy)
	throws Exception
	{
		return getJSONLISsidealerscoretableviewRecordSearchResultByPage(record, offset, maxrows, orderBy, null);
	}


	public JSONObject getJSONLISsidealerscoretableviewRecordSearchResultByPage(LISsidealerscoretableviewRecord record, String offset, String maxrows, String orderBy, String customCondition)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLISsidealerscoretableviewRecordSearchResultByPage:" + record + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLISsidealerscoretableviewRecordSearchResult", null);
			LISsidealerscoretableviewDAO dao = new LISsidealerscoretableviewDAO();
			dao.setCustomCondition(customCondition);
			int totalCount = dao.loadLISsidealerscoretableviewRecordCount(record);
			dao.setLimits(offset, maxrows);
			LISsidealerscoretableviewRecord[] records = null;
			if (totalCount > 0)
			{
				dao.setOrderBy(orderBy);
				records = dao.searchLISsidealerscoretableviewRecords(record);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLISsidealerscoretableviewRecordSearchResult" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
	public JSONObject getJSONLISsidealerscoretableviewRecordSearchResultByPageQuery(Connection con, String query, String countQuery, String offset, String maxrows)
	throws Exception
	{
		try
		{
			logger.trace("getJSONLISsidealerscoretableviewRecordSearchResultByPageQuery:" + query + " Offset:" + offset + " Maxrows:" + maxrows);
			String beginMesssage = logger.generateFunctionBeginTimerMessage("getJSONLISsidealerscoretableviewRecordSearchResultByPageQuery", null);
			LISsidealerscoretableviewDAO dao = new LISsidealerscoretableviewDAO();
			int totalCount = dao.loadRecordCount(con, countQuery);
			dao.setLimits(offset, maxrows);
			LISsidealerscoretableviewRecord[] records = null;
			if (totalCount > 0)
			{
				records = dao.loadLISsidealerscoretableviewRecords(query, con, true);
			}
			JSONObject resultObject = new JSONObject();
			resultObject.put("total",totalCount + "");
			JSONArray dataArray = new JSONArray();
			int recordCount = 0;
			if (records != null)
			{
				recordCount = records.length;
			}
			for (int index = 0; index < recordCount; index++)
			{
				dataArray.add(records[index].getJSONObjectUI());
			}
			resultObject.put("rows",dataArray);
			logger.generateFunctionEndTimerMessage(beginMesssage);
			return resultObject;
		}
		catch(Exception exception)
		{
			logger.error("getJSONLISsidealerscoretableviewRecordSearchResultByPageQuery" + getStackTrace(exception));
			throw new Exception("MFException:" + getErrorMessage(exception));
		}
	}
}
