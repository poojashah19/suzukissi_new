
/*
 * LIOtherstableviewRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.to;
import com.leadics.suzukithdashboard.common.LIRecord;
import com.leadics.utils.*;
import org.json.simple.*;
import java.util.*;
public class LIOtherstableviewRecord extends LIRecord
{
	static LogUtils logger = new LogUtils(LIOtherstableviewRecord.class.getName());

	private String attributename;
	private String dealeraverage;
	private String regionaverage;
	private String regionbest;
	private String studybest;

	public String getAttributename()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(attributename);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(attributename);
		}
		else
		{
			return attributename;
		}
	}

	public String getDealeraverage()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dealeraverage);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dealeraverage);
		}
		else
		{
			return dealeraverage;
		}
	}

	public String getRegionaverage()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(regionaverage);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(regionaverage);
		}
		else
		{
			return regionaverage;
		}
	}

	public String getRegionbest()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(regionbest);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(regionbest);
		}
		else
		{
			return regionbest;
		}
	}

	public String getStudybest()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(studybest);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(studybest);
		}
		else
		{
			return studybest;
		}
	}


	public void setAttributename(String value)
	{
		attributename = value;
	}

	public void setDealeraverage(String value)
	{
		dealeraverage = value;
	}

	public void setRegionaverage(String value)
	{
		regionaverage = value;
	}

	public void setRegionbest(String value)
	{
		regionbest = value;
	}

	public void setStudybest(String value)
	{
		studybest = value;
	}


	public String toString()
	{
		return "\nattributename:" + attributename +
				"\ndealeraverage:" + dealeraverage +
				"\nregionaverage:" + regionaverage +
				"\nregionbest:" + regionbest +
				"\nstudybest:" + studybest +
				"\n";
	}

	public void loadContent(LIOtherstableviewRecord inputRecord)
	{
		setAttributename(inputRecord.getAttributename());
		setDealeraverage(inputRecord.getDealeraverage());
		setRegionaverage(inputRecord.getRegionaverage());
		setRegionbest(inputRecord.getRegionbest());
		setStudybest(inputRecord.getStudybest());
	}

	public void loadNonNullContent(LIOtherstableviewRecord inputRecord)
	{
		if (StringUtils.hasChanged(getAttributename(), inputRecord.getAttributename()))
		{
			setAttributename(StringUtils.noNull(inputRecord.getAttributename()));
		}
		if (StringUtils.hasChanged(getDealeraverage(), inputRecord.getDealeraverage()))
		{
			setDealeraverage(StringUtils.noNull(inputRecord.getDealeraverage()));
		}
		if (StringUtils.hasChanged(getRegionaverage(), inputRecord.getRegionaverage()))
		{
			setRegionaverage(StringUtils.noNull(inputRecord.getRegionaverage()));
		}
		if (StringUtils.hasChanged(getRegionbest(), inputRecord.getRegionbest()))
		{
			setRegionbest(StringUtils.noNull(inputRecord.getRegionbest()));
		}
		if (StringUtils.hasChanged(getStudybest(), inputRecord.getStudybest()))
		{
			setStudybest(StringUtils.noNull(inputRecord.getStudybest()));
		}
	}

	public JSONObject getJSONObject()
	{
		JSONObject obj = new JSONObject();

		obj.put("attributename",StringUtils.noNull(attributename));				
		obj.put("dealeraverage",StringUtils.noNull(dealeraverage));				
		obj.put("regionaverage",StringUtils.noNull(regionaverage));				
		obj.put("regionbest",StringUtils.noNull(regionbest));				
		obj.put("studybest",StringUtils.noNull(studybest));
		return obj;
	}

	public void loadJSONObject(JSONObject obj)
	throws Exception
	{
		if (obj == null) return;

		attributename = StringUtils.getValueFromJSONObject(obj, "attributename");				
		dealeraverage = StringUtils.getValueFromJSONObject(obj, "dealeraverage");				
		regionaverage = StringUtils.getValueFromJSONObject(obj, "regionaverage");				
		regionbest = StringUtils.getValueFromJSONObject(obj, "regionbest");				
		studybest = StringUtils.getValueFromJSONObject(obj, "studybest");
		return;
	}

	public JSONObject getJSONObjectUI()
	{
		JSONObject obj = new JSONObject();

		obj.put("attributename",StringUtils.noNull(attributename));				
		obj.put("dealeraverage",StringUtils.noNull(dealeraverage));				
		obj.put("regionaverage",StringUtils.noNull(regionaverage));				
		obj.put("regionbest",StringUtils.noNull(regionbest));				
		obj.put("studybest",StringUtils.noNull(studybest));
		return obj;
	}

	public void log()
	{
		logger.trace(this.toString());
	}

	public HashMap getTableMap()
	{
		HashMap resultMap = new HashMap();
		ArrayList columnList = new ArrayList();
		resultMap.put("table", "otherstable_view");

		columnList.add("attributename");				
		columnList.add("dealeraverage");				
		columnList.add("regionaverage");				
		columnList.add("regionbest");				
		columnList.add("studybest");
		resultMap.put("ColumnList", columnList);

		return resultMap;
	}

}
