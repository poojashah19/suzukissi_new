
/*
 * LIReportquiresRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.to;
import com.leadics.suzukithdashboard.common.LIRecord;
import com.leadics.utils.*;
import org.json.simple.*;
import java.util.*;
public class LIReportquiresRecord extends LIRecord
{
	static LogUtils logger = new LogUtils(LIReportquiresRecord.class.getName());

	private String id;
	private String reporttype;
	private String reportname;
	private String query;
	private String countquery;
	private String jsontemplate;
	private String rstatus;
	private String createdby;
	private String createdat;
	private String modifiedat;
	private String modifiedby;
	private String institutionid;

	public String getId()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(id);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(id);
		}
		else
		{
			return id;
		}
	}

	public String getReporttype()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(reporttype);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(reporttype);
		}
		else
		{
			return reporttype;
		}
	}

	public String getReportname()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(reportname);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(reportname);
		}
		else
		{
			return reportname;
		}
	}

	public String getQuery()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(query);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(query);
		}
		else
		{
			return query;
		}
	}

	public String getCountquery()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(countquery);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(countquery);
		}
		else
		{
			return countquery;
		}
	}

	public String getJsontemplate()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(jsontemplate);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(jsontemplate);
		}
		else
		{
			return jsontemplate;
		}
	}

	public String getRstatus()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(rstatus);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(rstatus);
		}
		else
		{
			return rstatus;
		}
	}

	public String getCreatedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdby);
		}
		else
		{
			return createdby;
		}
	}

	public String getCreatedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdat);
		}
		else
		{
			return createdat;
		}
	}

	public String getModifiedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedat);
		}
		else
		{
			return modifiedat;
		}
	}

	public String getModifiedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedby);
		}
		else
		{
			return modifiedby;
		}
	}

	public String getInstitutionid()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(institutionid);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(institutionid);
		}
		else
		{
			return institutionid;
		}
	}


	public void setId(String value)
	{
		id = value;
	}

	public void setReporttype(String value)
	{
		reporttype = value;
	}

	public void setReportname(String value)
	{
		reportname = value;
	}

	public void setQuery(String value)
	{
		query = value;
	}

	public void setCountquery(String value)
	{
		countquery = value;
	}

	public void setJsontemplate(String value)
	{
		jsontemplate = value;
	}

	public void setRstatus(String value)
	{
		rstatus = value;
	}

	public void setCreatedby(String value)
	{
		createdby = value;
	}

	public void setCreatedat(String value)
	{
		createdat = value;
	}

	public void setModifiedat(String value)
	{
		modifiedat = value;
	}

	public void setModifiedby(String value)
	{
		modifiedby = value;
	}

	public void setInstitutionid(String value)
	{
		institutionid = value;
	}


	public String toString()
	{
		return "\nid:" + id +
				"\nreporttype:" + reporttype +
				"\nreportname:" + reportname +
				"\nquery:" + query +
				"\ncountquery:" + countquery +
				"\njsontemplate:" + jsontemplate +
				"\nrstatus:" + rstatus +
				"\ncreatedby:" + createdby +
				"\ncreatedat:" + createdat +
				"\nmodifiedat:" + modifiedat +
				"\nmodifiedby:" + modifiedby +
				"\ninstitutionid:" + institutionid +
				"\n";
	}

	public void loadContent(LIReportquiresRecord inputRecord)
	{
		setId(inputRecord.getId());
		setReporttype(inputRecord.getReporttype());
		setReportname(inputRecord.getReportname());
		setQuery(inputRecord.getQuery());
		setCountquery(inputRecord.getCountquery());
		setJsontemplate(inputRecord.getJsontemplate());
		setRstatus(inputRecord.getRstatus());
		setCreatedby(inputRecord.getCreatedby());
		setCreatedat(inputRecord.getCreatedat());
		setModifiedat(inputRecord.getModifiedat());
		setModifiedby(inputRecord.getModifiedby());
		setInstitutionid(inputRecord.getInstitutionid());
	}

	public void loadNonNullContent(LIReportquiresRecord inputRecord)
	{
		if (StringUtils.hasChanged(getId(), inputRecord.getId()))
		{
			setId(StringUtils.noNull(inputRecord.getId()));
		}
		if (StringUtils.hasChanged(getReporttype(), inputRecord.getReporttype()))
		{
			setReporttype(StringUtils.noNull(inputRecord.getReporttype()));
		}
		if (StringUtils.hasChanged(getReportname(), inputRecord.getReportname()))
		{
			setReportname(StringUtils.noNull(inputRecord.getReportname()));
		}
		if (StringUtils.hasChanged(getQuery(), inputRecord.getQuery()))
		{
			setQuery(StringUtils.noNull(inputRecord.getQuery()));
		}
		if (StringUtils.hasChanged(getCountquery(), inputRecord.getCountquery()))
		{
			setCountquery(StringUtils.noNull(inputRecord.getCountquery()));
		}
		if (StringUtils.hasChanged(getJsontemplate(), inputRecord.getJsontemplate()))
		{
			setJsontemplate(StringUtils.noNull(inputRecord.getJsontemplate()));
		}
		if (StringUtils.hasChanged(getRstatus(), inputRecord.getRstatus()))
		{
			setRstatus(StringUtils.noNull(inputRecord.getRstatus()));
		}
		if (StringUtils.hasChanged(getCreatedby(), inputRecord.getCreatedby()))
		{
			setCreatedby(StringUtils.noNull(inputRecord.getCreatedby()));
		}
		if (StringUtils.hasChanged(getCreatedat(), inputRecord.getCreatedat()))
		{
			setCreatedat(StringUtils.noNull(inputRecord.getCreatedat()));
		}
		if (StringUtils.hasChanged(getModifiedat(), inputRecord.getModifiedat()))
		{
			setModifiedat(StringUtils.noNull(inputRecord.getModifiedat()));
		}
		if (StringUtils.hasChanged(getModifiedby(), inputRecord.getModifiedby()))
		{
			setModifiedby(StringUtils.noNull(inputRecord.getModifiedby()));
		}
		if (StringUtils.hasChanged(getInstitutionid(), inputRecord.getInstitutionid()))
		{
			setInstitutionid(StringUtils.noNull(inputRecord.getInstitutionid()));
		}
	}

	public JSONObject getJSONObject()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("reporttype",StringUtils.noNull(reporttype));				
		obj.put("reportname",StringUtils.noNull(reportname));				
		obj.put("query",StringUtils.noNull(query));				
		obj.put("countquery",StringUtils.noNull(countquery));				
		obj.put("jsontemplate",StringUtils.noNull(jsontemplate));				
		obj.put("rstatus",StringUtils.noNull(rstatus));				
		obj.put("createdby",StringUtils.noNull(createdby));				
		obj.put("createdat",StringUtils.noNull(createdat));				
		obj.put("modifiedat",StringUtils.noNull(modifiedat));				
		obj.put("modifiedby",StringUtils.noNull(modifiedby));				
		obj.put("institutionid",StringUtils.noNull(institutionid));
		return obj;
	}

	public void loadJSONObject(JSONObject obj)
	throws Exception
	{
		if (obj == null) return;

		id = StringUtils.getValueFromJSONObject(obj, "id");				
		reporttype = StringUtils.getValueFromJSONObject(obj, "reporttype");				
		reportname = StringUtils.getValueFromJSONObject(obj, "reportname");				
		query = StringUtils.getValueFromJSONObject(obj, "query");				
		countquery = StringUtils.getValueFromJSONObject(obj, "countquery");				
		jsontemplate = StringUtils.getValueFromJSONObject(obj, "jsontemplate");				
		rstatus = StringUtils.getValueFromJSONObject(obj, "rstatus");				
		createdby = StringUtils.getValueFromJSONObject(obj, "createdby");				
		createdat = StringUtils.getValueFromJSONObject(obj, "createdat");				
		modifiedat = StringUtils.getValueFromJSONObject(obj, "modifiedat");				
		modifiedby = StringUtils.getValueFromJSONObject(obj, "modifiedby");				
		institutionid = StringUtils.getValueFromJSONObject(obj, "institutionid");
		return;
	}

	public JSONObject getJSONObjectUI()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("reporttype",StringUtils.noNull(reporttype));				
		obj.put("reportname",StringUtils.noNull(reportname));				
		obj.put("query",StringUtils.noNull(query));				
		obj.put("countquery",StringUtils.noNull(countquery));				
		obj.put("jsontemplate",StringUtils.noNull(jsontemplate));				
		obj.put("rstatus",StringUtils.noNull(rstatus));				
		obj.put("created_by",StringUtils.noNull(createdby));				
		obj.put("created_at",StringUtils.noNull(createdat));				
		obj.put("modified_at",StringUtils.noNull(modifiedat));				
		obj.put("modified_by",StringUtils.noNull(modifiedby));				
		obj.put("institution_id",StringUtils.noNull(institutionid));
		return obj;
	}

	public void log()
	{
		logger.trace(this.toString());
	}

	public HashMap getTableMap()
	{
		HashMap resultMap = new HashMap();
		ArrayList columnList = new ArrayList();
		resultMap.put("table", "reportquires_csi");

		columnList.add("id");				
		columnList.add("reporttype");				
		columnList.add("reportname");				
		columnList.add("query");				
		columnList.add("countquery");				
		columnList.add("jsontemplate");				
		columnList.add("rstatus");				
		columnList.add("created_by");				
		columnList.add("created_at");				
		columnList.add("modified_at");				
		columnList.add("modified_by");				
		columnList.add("institution_id");
		resultMap.put("ColumnList", columnList);

		return resultMap;
	}

	public void toggleStatus()
	{
		String currentStatus = StringUtils.noNull(getRstatus());
		setRstatus("1");
		if (currentStatus.equals("1"))
		{
			setRstatus("0");
		}
	}

}
