
/*
 * LIConsolefailedloginRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.to;
import com.leadics.suzukithdashboard.common.LIRecord;
import com.leadics.utils.*;
import org.json.simple.*;
import java.util.*;
public class LIConsolefailedloginRecord extends LIRecord
{
	static LogUtils logger = new LogUtils(LIConsolefailedloginRecord.class.getName());

	private String id;
	private String username;
	private String channel;
	private String source;
	private String reason;
	private String createdat;
	private String createdby;
	private String modifiedat;
	private String modifiedby;

	public String getId()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(id);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(id);
		}
		else
		{
			return id;
		}
	}

	public String getUsername()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(username);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(username);
		}
		else
		{
			return username;
		}
	}

	public String getChannel()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(channel);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(channel);
		}
		else
		{
			return channel;
		}
	}

	public String getSource()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(source);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(source);
		}
		else
		{
			return source;
		}
	}

	public String getReason()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(reason);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(reason);
		}
		else
		{
			return reason;
		}
	}

	public String getCreatedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdat);
		}
		else
		{
			return createdat;
		}
	}

	public String getCreatedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdby);
		}
		else
		{
			return createdby;
		}
	}

	public String getModifiedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedat);
		}
		else
		{
			return modifiedat;
		}
	}

	public String getModifiedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedby);
		}
		else
		{
			return modifiedby;
		}
	}


	public void setId(String value)
	{
		id = value;
	}

	public void setUsername(String value)
	{
		username = value;
	}

	public void setChannel(String value)
	{
		channel = value;
	}

	public void setSource(String value)
	{
		source = value;
	}

	public void setReason(String value)
	{
		reason = value;
	}

	public void setCreatedat(String value)
	{
		createdat = value;
	}

	public void setCreatedby(String value)
	{
		createdby = value;
	}

	public void setModifiedat(String value)
	{
		modifiedat = value;
	}

	public void setModifiedby(String value)
	{
		modifiedby = value;
	}


	public String toString()
	{
		return "\nid:" + id +
				"\nusername:" + username +
				"\nchannel:" + channel +
				"\nsource:" + source +
				"\nreason:" + reason +
				"\ncreatedat:" + createdat +
				"\ncreatedby:" + createdby +
				"\nmodifiedat:" + modifiedat +
				"\nmodifiedby:" + modifiedby +
				"\n";
	}

	public void loadContent(LIConsolefailedloginRecord inputRecord)
	{
		setId(inputRecord.getId());
		setUsername(inputRecord.getUsername());
		setChannel(inputRecord.getChannel());
		setSource(inputRecord.getSource());
		setReason(inputRecord.getReason());
		setCreatedat(inputRecord.getCreatedat());
		setCreatedby(inputRecord.getCreatedby());
		setModifiedat(inputRecord.getModifiedat());
		setModifiedby(inputRecord.getModifiedby());
	}

	public void loadNonNullContent(LIConsolefailedloginRecord inputRecord)
	{
		if (StringUtils.hasChanged(getId(), inputRecord.getId()))
		{
			setId(StringUtils.noNull(inputRecord.getId()));
		}
		if (StringUtils.hasChanged(getUsername(), inputRecord.getUsername()))
		{
			setUsername(StringUtils.noNull(inputRecord.getUsername()));
		}
		if (StringUtils.hasChanged(getChannel(), inputRecord.getChannel()))
		{
			setChannel(StringUtils.noNull(inputRecord.getChannel()));
		}
		if (StringUtils.hasChanged(getSource(), inputRecord.getSource()))
		{
			setSource(StringUtils.noNull(inputRecord.getSource()));
		}
		if (StringUtils.hasChanged(getReason(), inputRecord.getReason()))
		{
			setReason(StringUtils.noNull(inputRecord.getReason()));
		}
		if (StringUtils.hasChanged(getCreatedat(), inputRecord.getCreatedat()))
		{
			setCreatedat(StringUtils.noNull(inputRecord.getCreatedat()));
		}
		if (StringUtils.hasChanged(getCreatedby(), inputRecord.getCreatedby()))
		{
			setCreatedby(StringUtils.noNull(inputRecord.getCreatedby()));
		}
		if (StringUtils.hasChanged(getModifiedat(), inputRecord.getModifiedat()))
		{
			setModifiedat(StringUtils.noNull(inputRecord.getModifiedat()));
		}
		if (StringUtils.hasChanged(getModifiedby(), inputRecord.getModifiedby()))
		{
			setModifiedby(StringUtils.noNull(inputRecord.getModifiedby()));
		}
	}

	public JSONObject getJSONObject()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("username",StringUtils.noNull(username));				
		obj.put("channel",StringUtils.noNull(channel));				
		obj.put("source",StringUtils.noNull(source));				
		obj.put("reason",StringUtils.noNull(reason));				
		obj.put("createdat",StringUtils.noNull(createdat));				
		obj.put("createdby",StringUtils.noNull(createdby));				
		obj.put("modifiedat",StringUtils.noNull(modifiedat));				
		obj.put("modifiedby",StringUtils.noNull(modifiedby));
		return obj;
	}

	public void loadJSONObject(JSONObject obj)
	throws Exception
	{
		if (obj == null) return;

		id = StringUtils.getValueFromJSONObject(obj, "id");				
		username = StringUtils.getValueFromJSONObject(obj, "username");				
		channel = StringUtils.getValueFromJSONObject(obj, "channel");				
		source = StringUtils.getValueFromJSONObject(obj, "source");				
		reason = StringUtils.getValueFromJSONObject(obj, "reason");				
		createdat = StringUtils.getValueFromJSONObject(obj, "createdat");				
		createdby = StringUtils.getValueFromJSONObject(obj, "createdby");				
		modifiedat = StringUtils.getValueFromJSONObject(obj, "modifiedat");				
		modifiedby = StringUtils.getValueFromJSONObject(obj, "modifiedby");
		return;
	}

	public JSONObject getJSONObjectUI()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("username",StringUtils.noNull(username));				
		obj.put("channel",StringUtils.noNull(channel));				
		obj.put("source",StringUtils.noNull(source));				
		obj.put("reason",StringUtils.noNull(reason));				
		obj.put("created_at",StringUtils.noNull(createdat));				
		obj.put("created_by",StringUtils.noNull(createdby));				
		obj.put("modified_at",StringUtils.noNull(modifiedat));				
		obj.put("modified_by",StringUtils.noNull(modifiedby));
		return obj;
	}

	public void log()
	{
		logger.trace(this.toString());
	}

	public HashMap getTableMap()
	{
		HashMap resultMap = new HashMap();
		ArrayList columnList = new ArrayList();
		resultMap.put("table", "console_failed_login");

		columnList.add("id");				
		columnList.add("username");				
		columnList.add("channel");				
		columnList.add("source");				
		columnList.add("reason");				
		columnList.add("created_at");				
		columnList.add("created_by");				
		columnList.add("modified_at");				
		columnList.add("modified_by");
		resultMap.put("ColumnList", columnList);

		return resultMap;
	}

}
