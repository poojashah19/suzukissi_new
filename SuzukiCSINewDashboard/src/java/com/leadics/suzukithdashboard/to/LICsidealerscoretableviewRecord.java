
/*
 * LICsidealerscoretableviewRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.to;

import com.leadics.suzukithdashboard.common.LIRecord;
import com.leadics.utils.*;
import org.json.simple.*;
import java.util.*;

public class LICsidealerscoretableviewRecord extends LIRecord {

    static LogUtils logger = new LogUtils(LICsidealerscoretableviewRecord.class.getName());

   

    private String rank;
    private String model;
    private String region;
    private String dealer;
    private String csi;
    private String csicolor;
    private String serviceadvisor;
    private String serviceadvisorcolor;
    private String servicefacility;
    private String servicefacilitycolor;
    private String serviceinitiation;
    private String serviceinitiationcolor;
    private String servicequality;
    private String servicequalitycolor;
    private String vehiclepickup;
    private String vehiclepickupcolor;
    private String sop;
    private String sopcolor;

    public String getRank() {
        return StringUtils.noNull(rank);
    }

    public void setRank(String rank) {
        this.rank = rank;
    }
     public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
    public String getRegion() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(region);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(region);
        } else {
            return region;
        }
    }

    public String getDealer() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(dealer);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(dealer);
        } else {
            return dealer;
        }
    }

    public String getCsi() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(csi);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(csi);
        } else {
            return csi;
        }
    }

    public String getCsicolor() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(csicolor);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(csicolor);
        } else {
            return csicolor;
        }
    }

    public String getServiceadvisor() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(serviceadvisor);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(serviceadvisor);
        } else {
            return serviceadvisor;
        }
    }

    public String getServiceadvisorcolor() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(serviceadvisorcolor);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(serviceadvisorcolor);
        } else {
            return serviceadvisorcolor;
        }
    }

    public String getServicefacility() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(servicefacility);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(servicefacility);
        } else {
            return servicefacility;
        }
    }

    public String getServicefacilitycolor() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(servicefacilitycolor);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(servicefacilitycolor);
        } else {
            return servicefacilitycolor;
        }
    }

    public String getServiceinitiation() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(serviceinitiation);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(serviceinitiation);
        } else {
            return serviceinitiation;
        }
    }

    public String getServiceinitiationcolor() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(serviceinitiationcolor);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(serviceinitiationcolor);
        } else {
            return serviceinitiationcolor;
        }
    }

    public String getServicequality() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(servicequality);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(servicequality);
        } else {
            return servicequality;
        }
    }

    public String getServicequalitycolor() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(servicequalitycolor);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(servicequalitycolor);
        } else {
            return servicequalitycolor;
        }
    }

    public String getVehiclepickup() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(vehiclepickup);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(vehiclepickup);
        } else {
            return vehiclepickup;
        }
    }

    public String getVehiclepickupcolor() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(vehiclepickupcolor);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(vehiclepickupcolor);
        } else {
            return vehiclepickupcolor;
        }
    }

    public String getSop() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(sop);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(sop);
        } else {
            return sop;
        }
    }

    public String getSopcolor() {
        if (isNoNullForHTMLEnabled()) {
            return StringUtils.noNullForHTML(sopcolor);
        } else if (isNoNullEnabled()) {
            return StringUtils.noNull(sopcolor);
        } else {
            return sopcolor;
        }
    }

    public void setRegion(String value) {
        region = value;
    }

    public void setDealer(String value) {
        dealer = value;
    }

    public void setCsi(String value) {
        csi = value;
    }

    public void setCsicolor(String value) {
        csicolor = value;
    }

    public void setServiceadvisor(String value) {
        serviceadvisor = value;
    }

    public void setServiceadvisorcolor(String value) {
        serviceadvisorcolor = value;
    }

    public void setServicefacility(String value) {
        servicefacility = value;
    }

    public void setServicefacilitycolor(String value) {
        servicefacilitycolor = value;
    }

    public void setServiceinitiation(String value) {
        serviceinitiation = value;
    }

    public void setServiceinitiationcolor(String value) {
        serviceinitiationcolor = value;
    }

    public void setServicequality(String value) {
        servicequality = value;
    }

    public void setServicequalitycolor(String value) {
        servicequalitycolor = value;
    }

    public void setVehiclepickup(String value) {
        vehiclepickup = value;
    }

    public void setVehiclepickupcolor(String value) {
        vehiclepickupcolor = value;
    }

    public void setSop(String value) {
        sop = value;
    }

    public void setSopcolor(String value) {
        sopcolor = value;
    }

    public String toString() {
        return "\nregion:" + region
                + "\ndealer:" + dealer
                + "\ncsi:" + csi
                + "\ncsicolor:" + csicolor
                + "\nserviceadvisor:" + serviceadvisor
                + "\nserviceadvisorcolor:" + serviceadvisorcolor
                + "\nservicefacility:" + servicefacility
                + "\nservicefacilitycolor:" + servicefacilitycolor
                + "\nserviceinitiation:" + serviceinitiation
                + "\nserviceinitiationcolor:" + serviceinitiationcolor
                + "\nservicequality:" + servicequality
                + "\nservicequalitycolor:" + servicequalitycolor
                + "\nvehiclepickup:" + vehiclepickup
                + "\nvehiclepickupcolor:" + vehiclepickupcolor
                + "\nsop:" + sop
                + "\nsopcolor:" + sopcolor
                + "\n";
    }

    public void loadContent(LICsidealerscoretableviewRecord inputRecord) {
        setRegion(inputRecord.getRegion());
        setDealer(inputRecord.getDealer());
        setCsi(inputRecord.getCsi());
        setCsicolor(inputRecord.getCsicolor());
        setServiceadvisor(inputRecord.getServiceadvisor());
        setServiceadvisorcolor(inputRecord.getServiceadvisorcolor());
        setServicefacility(inputRecord.getServicefacility());
        setServicefacilitycolor(inputRecord.getServicefacilitycolor());
        setServiceinitiation(inputRecord.getServiceinitiation());
        setServiceinitiationcolor(inputRecord.getServiceinitiationcolor());
        setServicequality(inputRecord.getServicequality());
        setServicequalitycolor(inputRecord.getServicequalitycolor());
        setVehiclepickup(inputRecord.getVehiclepickup());
        setVehiclepickupcolor(inputRecord.getVehiclepickupcolor());
        setSop(inputRecord.getSop());
        setSopcolor(inputRecord.getSopcolor());
    }

    public void loadNonNullContent(LICsidealerscoretableviewRecord inputRecord) {
        if (StringUtils.hasChanged(getRegion(), inputRecord.getRegion())) {
            setRegion(StringUtils.noNull(inputRecord.getRegion()));
        }
        if (StringUtils.hasChanged(getDealer(), inputRecord.getDealer())) {
            setDealer(StringUtils.noNull(inputRecord.getDealer()));
        }
        if (StringUtils.hasChanged(getCsi(), inputRecord.getCsi())) {
            setCsi(StringUtils.noNull(inputRecord.getCsi()));
        }
        if (StringUtils.hasChanged(getCsicolor(), inputRecord.getCsicolor())) {
            setCsicolor(StringUtils.noNull(inputRecord.getCsicolor()));
        }
        if (StringUtils.hasChanged(getServiceadvisor(), inputRecord.getServiceadvisor())) {
            setServiceadvisor(StringUtils.noNull(inputRecord.getServiceadvisor()));
        }
        if (StringUtils.hasChanged(getServiceadvisorcolor(), inputRecord.getServiceadvisorcolor())) {
            setServiceadvisorcolor(StringUtils.noNull(inputRecord.getServiceadvisorcolor()));
        }
        if (StringUtils.hasChanged(getServicefacility(), inputRecord.getServicefacility())) {
            setServicefacility(StringUtils.noNull(inputRecord.getServicefacility()));
        }
        if (StringUtils.hasChanged(getServicefacilitycolor(), inputRecord.getServicefacilitycolor())) {
            setServicefacilitycolor(StringUtils.noNull(inputRecord.getServicefacilitycolor()));
        }
        if (StringUtils.hasChanged(getServiceinitiation(), inputRecord.getServiceinitiation())) {
            setServiceinitiation(StringUtils.noNull(inputRecord.getServiceinitiation()));
        }
        if (StringUtils.hasChanged(getServiceinitiationcolor(), inputRecord.getServiceinitiationcolor())) {
            setServiceinitiationcolor(StringUtils.noNull(inputRecord.getServiceinitiationcolor()));
        }
        if (StringUtils.hasChanged(getServicequality(), inputRecord.getServicequality())) {
            setServicequality(StringUtils.noNull(inputRecord.getServicequality()));
        }
        if (StringUtils.hasChanged(getServicequalitycolor(), inputRecord.getServicequalitycolor())) {
            setServicequalitycolor(StringUtils.noNull(inputRecord.getServicequalitycolor()));
        }
        if (StringUtils.hasChanged(getVehiclepickup(), inputRecord.getVehiclepickup())) {
            setVehiclepickup(StringUtils.noNull(inputRecord.getVehiclepickup()));
        }
        if (StringUtils.hasChanged(getVehiclepickupcolor(), inputRecord.getVehiclepickupcolor())) {
            setVehiclepickupcolor(StringUtils.noNull(inputRecord.getVehiclepickupcolor()));
        }
        if (StringUtils.hasChanged(getSop(), inputRecord.getSop())) {
            setSop(StringUtils.noNull(inputRecord.getSop()));
        }
        if (StringUtils.hasChanged(getSopcolor(), inputRecord.getSopcolor())) {
            setSopcolor(StringUtils.noNull(inputRecord.getSopcolor()));
        }
    }

    public JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        obj.put("rank", StringUtils.noNull(rank));
        obj.put("region", StringUtils.noNull(region));
        obj.put("dealer", StringUtils.noNull(dealer));
        obj.put("csi", StringUtils.noNull(csi));
        obj.put("csicolor", StringUtils.noNull(csicolor));
        obj.put("serviceadvisor", StringUtils.noNull(serviceadvisor));
        obj.put("serviceadvisorcolor", StringUtils.noNull(serviceadvisorcolor));
        obj.put("servicefacility", StringUtils.noNull(servicefacility));
        obj.put("servicefacilitycolor", StringUtils.noNull(servicefacilitycolor));
        obj.put("serviceinitiation", StringUtils.noNull(serviceinitiation));
        obj.put("serviceinitiationcolor", StringUtils.noNull(serviceinitiationcolor));
        obj.put("servicequality", StringUtils.noNull(servicequality));
        obj.put("servicequalitycolor", StringUtils.noNull(servicequalitycolor));
        obj.put("vehiclepickup", StringUtils.noNull(vehiclepickup));
        obj.put("vehiclepickupcolor", StringUtils.noNull(vehiclepickupcolor));
        obj.put("sop", StringUtils.noNull(sop));
        obj.put("sopcolor", StringUtils.noNull(sopcolor));
        return obj;
    }

    public void loadJSONObject(JSONObject obj)
            throws Exception {
        if (obj == null) {
            return;
        }

        region = StringUtils.getValueFromJSONObject(obj, "region");
        dealer = StringUtils.getValueFromJSONObject(obj, "dealer");
        csi = StringUtils.getValueFromJSONObject(obj, "csi");
        csicolor = StringUtils.getValueFromJSONObject(obj, "csicolor");
        serviceadvisor = StringUtils.getValueFromJSONObject(obj, "serviceadvisor");
        serviceadvisorcolor = StringUtils.getValueFromJSONObject(obj, "serviceadvisorcolor");
        servicefacility = StringUtils.getValueFromJSONObject(obj, "servicefacility");
        servicefacilitycolor = StringUtils.getValueFromJSONObject(obj, "servicefacilitycolor");
        serviceinitiation = StringUtils.getValueFromJSONObject(obj, "serviceinitiation");
        serviceinitiationcolor = StringUtils.getValueFromJSONObject(obj, "serviceinitiationcolor");
        servicequality = StringUtils.getValueFromJSONObject(obj, "servicequality");
        servicequalitycolor = StringUtils.getValueFromJSONObject(obj, "servicequalitycolor");
        vehiclepickup = StringUtils.getValueFromJSONObject(obj, "vehiclepickup");
        vehiclepickupcolor = StringUtils.getValueFromJSONObject(obj, "vehiclepickupcolor");
        sop = StringUtils.getValueFromJSONObject(obj, "sop");
        sopcolor = StringUtils.getValueFromJSONObject(obj, "sopcolor");
        return;
    }

    public JSONObject getJSONObjectUI() {
        JSONObject obj = new JSONObject();

        obj.put("region", StringUtils.noNull(region));
        obj.put("dealer", StringUtils.noNull(dealer));
        obj.put("csi", StringUtils.noNull(csi));
        obj.put("csicolor", StringUtils.noNull(csicolor));
        obj.put("serviceadvisor", StringUtils.noNull(serviceadvisor));
        obj.put("serviceadvisorcolor", StringUtils.noNull(serviceadvisorcolor));
        obj.put("servicefacility", StringUtils.noNull(servicefacility));
        obj.put("servicefacilitycolor", StringUtils.noNull(servicefacilitycolor));
        obj.put("serviceinitiation", StringUtils.noNull(serviceinitiation));
        obj.put("serviceinitiationcolor", StringUtils.noNull(serviceinitiationcolor));
        obj.put("servicequality", StringUtils.noNull(servicequality));
        obj.put("servicequalitycolor", StringUtils.noNull(servicequalitycolor));
        obj.put("vehiclepickup", StringUtils.noNull(vehiclepickup));
        obj.put("vehiclepickupcolor", StringUtils.noNull(vehiclepickupcolor));
        obj.put("sop", StringUtils.noNull(sop));
        obj.put("sopcolor", StringUtils.noNull(sopcolor));
        return obj;
    }

    public void log() {
        logger.trace(this.toString());
    }

    public HashMap getTableMap() {
        HashMap resultMap = new HashMap();
        ArrayList columnList = new ArrayList();
        resultMap.put("table", "csidealerscoretable_view");

        columnList.add("region");
        columnList.add("dealer");
        columnList.add("csi");
        columnList.add("csicolor");
        columnList.add("serviceadvisor");
        columnList.add("serviceadvisorcolor");
        columnList.add("servicefacility");
        columnList.add("servicefacilitycolor");
        columnList.add("serviceinitiation");
        columnList.add("serviceinitiationcolor");
        columnList.add("servicequality");
        columnList.add("servicequalitycolor");
        columnList.add("vehiclepickup");
        columnList.add("vehiclepickupcolor");
        columnList.add("sop");
        columnList.add("sopcolor");
        resultMap.put("ColumnList", columnList);

        return resultMap;
    }

}
