
/*
 * LISsiattributefactoraggregateRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.to;
import com.leadics.suzukithdashboard.common.LIRecord;
import com.leadics.utils.*;
import org.json.simple.*;
import java.util.*;
public class LISsiattributefactoraggregateRecord extends LIRecord
{
	static LogUtils logger = new LogUtils(LISsiattributefactoraggregateRecord.class.getName());

	private String id;
	private String attribute;
	private String attributename;
	private String attributeindex;
	private String factor;
	private String region;
	private String state;
	private String city;
	private String dealer;
	private String model;
	private String biweekly;
	private String month;
	private String quarter;
	private String dealerattributesum;
	private String dealerattributecount;
	private String totalcount;
	private String dealerdissatisfiedcount;
	private String rstatus;
	private String lastacc;
	private String createdby;
	private String createdat;
	private String modifiedat;
	private String modifiedby;
	private String closetype;
	private String closedat;

	public String getId()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(id);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(id);
		}
		else
		{
			return id;
		}
	}

	public String getAttribute()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(attribute);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(attribute);
		}
		else
		{
			return attribute;
		}
	}

	public String getAttributename()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(attributename);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(attributename);
		}
		else
		{
			return attributename;
		}
	}

	public String getAttributeindex()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(attributeindex);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(attributeindex);
		}
		else
		{
			return attributeindex;
		}
	}

	public String getFactor()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(factor);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(factor);
		}
		else
		{
			return factor;
		}
	}

	public String getRegion()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(region);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(region);
		}
		else
		{
			return region;
		}
	}

	public String getState()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(state);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(state);
		}
		else
		{
			return state;
		}
	}

	public String getCity()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(city);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(city);
		}
		else
		{
			return city;
		}
	}

	public String getDealer()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dealer);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dealer);
		}
		else
		{
			return dealer;
		}
	}

	public String getModel()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(model);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(model);
		}
		else
		{
			return model;
		}
	}

	public String getBiweekly()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(biweekly);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(biweekly);
		}
		else
		{
			return biweekly;
		}
	}

	public String getMonth()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(month);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(month);
		}
		else
		{
			return month;
		}
	}

	public String getQuarter()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(quarter);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(quarter);
		}
		else
		{
			return quarter;
		}
	}

	public String getDealerattributesum()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dealerattributesum);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dealerattributesum);
		}
		else
		{
			return dealerattributesum;
		}
	}

	public String getDealerattributecount()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dealerattributecount);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dealerattributecount);
		}
		else
		{
			return dealerattributecount;
		}
	}

	public String getTotalcount()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(totalcount);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(totalcount);
		}
		else
		{
			return totalcount;
		}
	}

	public String getDealerdissatisfiedcount()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dealerdissatisfiedcount);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dealerdissatisfiedcount);
		}
		else
		{
			return dealerdissatisfiedcount;
		}
	}

	public String getRstatus()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(rstatus);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(rstatus);
		}
		else
		{
			return rstatus;
		}
	}

	public String getLastacc()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(lastacc);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(lastacc);
		}
		else
		{
			return lastacc;
		}
	}

	public String getCreatedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdby);
		}
		else
		{
			return createdby;
		}
	}

	public String getCreatedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdat);
		}
		else
		{
			return createdat;
		}
	}

	public String getModifiedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedat);
		}
		else
		{
			return modifiedat;
		}
	}

	public String getModifiedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedby);
		}
		else
		{
			return modifiedby;
		}
	}

	public String getClosetype()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(closetype);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(closetype);
		}
		else
		{
			return closetype;
		}
	}

	public String getClosedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(closedat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(closedat);
		}
		else
		{
			return closedat;
		}
	}


	public void setId(String value)
	{
		id = value;
	}

	public void setAttribute(String value)
	{
		attribute = value;
	}

	public void setAttributename(String value)
	{
		attributename = value;
	}

	public void setAttributeindex(String value)
	{
		attributeindex = value;
	}

	public void setFactor(String value)
	{
		factor = value;
	}

	public void setRegion(String value)
	{
		region = value;
	}

	public void setState(String value)
	{
		state = value;
	}

	public void setCity(String value)
	{
		city = value;
	}

	public void setDealer(String value)
	{
		dealer = value;
	}

	public void setModel(String value)
	{
		model = value;
	}

	public void setBiweekly(String value)
	{
		biweekly = value;
	}

	public void setMonth(String value)
	{
		month = value;
	}

	public void setQuarter(String value)
	{
		quarter = value;
	}

	public void setDealerattributesum(String value)
	{
		dealerattributesum = value;
	}

	public void setDealerattributecount(String value)
	{
		dealerattributecount = value;
	}

	public void setTotalcount(String value)
	{
		totalcount = value;
	}

	public void setDealerdissatisfiedcount(String value)
	{
		dealerdissatisfiedcount = value;
	}

	public void setRstatus(String value)
	{
		rstatus = value;
	}

	public void setLastacc(String value)
	{
		lastacc = value;
	}

	public void setCreatedby(String value)
	{
		createdby = value;
	}

	public void setCreatedat(String value)
	{
		createdat = value;
	}

	public void setModifiedat(String value)
	{
		modifiedat = value;
	}

	public void setModifiedby(String value)
	{
		modifiedby = value;
	}

	public void setClosetype(String value)
	{
		closetype = value;
	}

	public void setClosedat(String value)
	{
		closedat = value;
	}


	public String toString()
	{
		return "\nid:" + id +
				"\nattribute:" + attribute +
				"\nattributename:" + attributename +
				"\nattributeindex:" + attributeindex +
				"\nfactor:" + factor +
				"\nregion:" + region +
				"\nstate:" + state +
				"\ncity:" + city +
				"\ndealer:" + dealer +
				"\nmodel:" + model +
				"\nbiweekly:" + biweekly +
				"\nmonth:" + month +
				"\nquarter:" + quarter +
				"\ndealerattributesum:" + dealerattributesum +
				"\ndealerattributecount:" + dealerattributecount +
				"\ntotalcount:" + totalcount +
				"\ndealerdissatisfiedcount:" + dealerdissatisfiedcount +
				"\nrstatus:" + rstatus +
				"\nlastacc:" + lastacc +
				"\ncreatedby:" + createdby +
				"\ncreatedat:" + createdat +
				"\nmodifiedat:" + modifiedat +
				"\nmodifiedby:" + modifiedby +
				"\nclosetype:" + closetype +
				"\nclosedat:" + closedat +
				"\n";
	}

	public void loadContent(LISsiattributefactoraggregateRecord inputRecord)
	{
		setId(inputRecord.getId());
		setAttribute(inputRecord.getAttribute());
		setAttributename(inputRecord.getAttributename());
		setAttributeindex(inputRecord.getAttributeindex());
		setFactor(inputRecord.getFactor());
		setRegion(inputRecord.getRegion());
		setState(inputRecord.getState());
		setCity(inputRecord.getCity());
		setDealer(inputRecord.getDealer());
		setModel(inputRecord.getModel());
		setBiweekly(inputRecord.getBiweekly());
		setMonth(inputRecord.getMonth());
		setQuarter(inputRecord.getQuarter());
		setDealerattributesum(inputRecord.getDealerattributesum());
		setDealerattributecount(inputRecord.getDealerattributecount());
		setTotalcount(inputRecord.getTotalcount());
		setDealerdissatisfiedcount(inputRecord.getDealerdissatisfiedcount());
		setRstatus(inputRecord.getRstatus());
		setLastacc(inputRecord.getLastacc());
		setCreatedby(inputRecord.getCreatedby());
		setCreatedat(inputRecord.getCreatedat());
		setModifiedat(inputRecord.getModifiedat());
		setModifiedby(inputRecord.getModifiedby());
		setClosetype(inputRecord.getClosetype());
		setClosedat(inputRecord.getClosedat());
	}

	public void loadNonNullContent(LISsiattributefactoraggregateRecord inputRecord)
	{
		if (StringUtils.hasChanged(getId(), inputRecord.getId()))
		{
			setId(StringUtils.noNull(inputRecord.getId()));
		}
		if (StringUtils.hasChanged(getAttribute(), inputRecord.getAttribute()))
		{
			setAttribute(StringUtils.noNull(inputRecord.getAttribute()));
		}
		if (StringUtils.hasChanged(getAttributename(), inputRecord.getAttributename()))
		{
			setAttributename(StringUtils.noNull(inputRecord.getAttributename()));
		}
		if (StringUtils.hasChanged(getAttributeindex(), inputRecord.getAttributeindex()))
		{
			setAttributeindex(StringUtils.noNull(inputRecord.getAttributeindex()));
		}
		if (StringUtils.hasChanged(getFactor(), inputRecord.getFactor()))
		{
			setFactor(StringUtils.noNull(inputRecord.getFactor()));
		}
		if (StringUtils.hasChanged(getRegion(), inputRecord.getRegion()))
		{
			setRegion(StringUtils.noNull(inputRecord.getRegion()));
		}
		if (StringUtils.hasChanged(getState(), inputRecord.getState()))
		{
			setState(StringUtils.noNull(inputRecord.getState()));
		}
		if (StringUtils.hasChanged(getCity(), inputRecord.getCity()))
		{
			setCity(StringUtils.noNull(inputRecord.getCity()));
		}
		if (StringUtils.hasChanged(getDealer(), inputRecord.getDealer()))
		{
			setDealer(StringUtils.noNull(inputRecord.getDealer()));
		}
		if (StringUtils.hasChanged(getModel(), inputRecord.getModel()))
		{
			setModel(StringUtils.noNull(inputRecord.getModel()));
		}
		if (StringUtils.hasChanged(getBiweekly(), inputRecord.getBiweekly()))
		{
			setBiweekly(StringUtils.noNull(inputRecord.getBiweekly()));
		}
		if (StringUtils.hasChanged(getMonth(), inputRecord.getMonth()))
		{
			setMonth(StringUtils.noNull(inputRecord.getMonth()));
		}
		if (StringUtils.hasChanged(getQuarter(), inputRecord.getQuarter()))
		{
			setQuarter(StringUtils.noNull(inputRecord.getQuarter()));
		}
		if (StringUtils.hasChanged(getDealerattributesum(), inputRecord.getDealerattributesum()))
		{
			setDealerattributesum(StringUtils.noNull(inputRecord.getDealerattributesum()));
		}
		if (StringUtils.hasChanged(getDealerattributecount(), inputRecord.getDealerattributecount()))
		{
			setDealerattributecount(StringUtils.noNull(inputRecord.getDealerattributecount()));
		}
		if (StringUtils.hasChanged(getTotalcount(), inputRecord.getTotalcount()))
		{
			setTotalcount(StringUtils.noNull(inputRecord.getTotalcount()));
		}
		if (StringUtils.hasChanged(getDealerdissatisfiedcount(), inputRecord.getDealerdissatisfiedcount()))
		{
			setDealerdissatisfiedcount(StringUtils.noNull(inputRecord.getDealerdissatisfiedcount()));
		}
		if (StringUtils.hasChanged(getRstatus(), inputRecord.getRstatus()))
		{
			setRstatus(StringUtils.noNull(inputRecord.getRstatus()));
		}
		if (StringUtils.hasChanged(getLastacc(), inputRecord.getLastacc()))
		{
			setLastacc(StringUtils.noNull(inputRecord.getLastacc()));
		}
		if (StringUtils.hasChanged(getCreatedby(), inputRecord.getCreatedby()))
		{
			setCreatedby(StringUtils.noNull(inputRecord.getCreatedby()));
		}
		if (StringUtils.hasChanged(getCreatedat(), inputRecord.getCreatedat()))
		{
			setCreatedat(StringUtils.noNull(inputRecord.getCreatedat()));
		}
		if (StringUtils.hasChanged(getModifiedat(), inputRecord.getModifiedat()))
		{
			setModifiedat(StringUtils.noNull(inputRecord.getModifiedat()));
		}
		if (StringUtils.hasChanged(getModifiedby(), inputRecord.getModifiedby()))
		{
			setModifiedby(StringUtils.noNull(inputRecord.getModifiedby()));
		}
		if (StringUtils.hasChanged(getClosetype(), inputRecord.getClosetype()))
		{
			setClosetype(StringUtils.noNull(inputRecord.getClosetype()));
		}
		if (StringUtils.hasChanged(getClosedat(), inputRecord.getClosedat()))
		{
			setClosedat(StringUtils.noNull(inputRecord.getClosedat()));
		}
	}

	public JSONObject getJSONObject()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("attribute",StringUtils.noNull(attribute));				
		obj.put("attributename",StringUtils.noNull(attributename));				
		obj.put("attributeindex",StringUtils.noNull(attributeindex));				
		obj.put("factor",StringUtils.noNull(factor));				
		obj.put("region",StringUtils.noNull(region));				
		obj.put("state",StringUtils.noNull(state));				
		obj.put("city",StringUtils.noNull(city));				
		obj.put("dealer",StringUtils.noNull(dealer));				
		obj.put("model",StringUtils.noNull(model));				
		obj.put("biweekly",StringUtils.noNull(biweekly));				
		obj.put("month",StringUtils.noNull(month));				
		obj.put("quarter",StringUtils.noNull(quarter));				
		obj.put("dealerattributesum",StringUtils.noNull(dealerattributesum));				
		obj.put("dealerattributecount",StringUtils.noNull(dealerattributecount));				
		obj.put("totalcount",StringUtils.noNull(totalcount));				
		obj.put("dealerdissatisfiedcount",StringUtils.noNull(dealerdissatisfiedcount));				
		obj.put("rstatus",StringUtils.noNull(rstatus));				
		obj.put("lastacc",StringUtils.noNull(lastacc));				
		obj.put("createdby",StringUtils.noNull(createdby));				
		obj.put("createdat",StringUtils.noNull(createdat));				
		obj.put("modifiedat",StringUtils.noNull(modifiedat));				
		obj.put("modifiedby",StringUtils.noNull(modifiedby));				
		obj.put("closetype",StringUtils.noNull(closetype));				
		obj.put("closedat",StringUtils.noNull(closedat));
		return obj;
	}

	public void loadJSONObject(JSONObject obj)
	throws Exception
	{
		if (obj == null) return;

		id = StringUtils.getValueFromJSONObject(obj, "id");				
		attribute = StringUtils.getValueFromJSONObject(obj, "attribute");				
		attributename = StringUtils.getValueFromJSONObject(obj, "attributename");				
		attributeindex = StringUtils.getValueFromJSONObject(obj, "attributeindex");				
		factor = StringUtils.getValueFromJSONObject(obj, "factor");				
		region = StringUtils.getValueFromJSONObject(obj, "region");				
		state = StringUtils.getValueFromJSONObject(obj, "state");				
		city = StringUtils.getValueFromJSONObject(obj, "city");				
		dealer = StringUtils.getValueFromJSONObject(obj, "dealer");				
		model = StringUtils.getValueFromJSONObject(obj, "model");				
		biweekly = StringUtils.getValueFromJSONObject(obj, "biweekly");				
		month = StringUtils.getValueFromJSONObject(obj, "month");				
		quarter = StringUtils.getValueFromJSONObject(obj, "quarter");				
		dealerattributesum = StringUtils.getValueFromJSONObject(obj, "dealerattributesum");				
		dealerattributecount = StringUtils.getValueFromJSONObject(obj, "dealerattributecount");				
		totalcount = StringUtils.getValueFromJSONObject(obj, "totalcount");				
		dealerdissatisfiedcount = StringUtils.getValueFromJSONObject(obj, "dealerdissatisfiedcount");				
		rstatus = StringUtils.getValueFromJSONObject(obj, "rstatus");				
		lastacc = StringUtils.getValueFromJSONObject(obj, "lastacc");				
		createdby = StringUtils.getValueFromJSONObject(obj, "createdby");				
		createdat = StringUtils.getValueFromJSONObject(obj, "createdat");				
		modifiedat = StringUtils.getValueFromJSONObject(obj, "modifiedat");				
		modifiedby = StringUtils.getValueFromJSONObject(obj, "modifiedby");				
		closetype = StringUtils.getValueFromJSONObject(obj, "closetype");				
		closedat = StringUtils.getValueFromJSONObject(obj, "closedat");
		return;
	}

	public JSONObject getJSONObjectUI()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("attribute",StringUtils.noNull(attribute));				
		obj.put("attributename",StringUtils.noNull(attributename));				
		obj.put("attributeindex",StringUtils.noNull(attributeindex));				
		obj.put("factor",StringUtils.noNull(factor));				
		obj.put("region",StringUtils.noNull(region));				
		obj.put("state",StringUtils.noNull(state));				
		obj.put("city",StringUtils.noNull(city));				
		obj.put("dealer",StringUtils.noNull(dealer));				
		obj.put("model",StringUtils.noNull(model));				
		obj.put("bi_weekly",StringUtils.noNull(biweekly));				
		obj.put("month",StringUtils.noNull(month));				
		obj.put("quarter",StringUtils.noNull(quarter));				
		obj.put("dealer_attribute_sum",StringUtils.noNull(dealerattributesum));				
		obj.put("dealer_attribute_count",StringUtils.noNull(dealerattributecount));				
		obj.put("totalcount",StringUtils.noNull(totalcount));				
		obj.put("dealer_dissatisfied_count",StringUtils.noNull(dealerdissatisfiedcount));				
		obj.put("rstatus",StringUtils.noNull(rstatus));				
		obj.put("lastacc",StringUtils.noNull(lastacc));				
		obj.put("created_by",StringUtils.noNull(createdby));				
		obj.put("created_at",StringUtils.noNull(createdat));				
		obj.put("modified_at",StringUtils.noNull(modifiedat));				
		obj.put("modified_by",StringUtils.noNull(modifiedby));				
		obj.put("close_type",StringUtils.noNull(closetype));				
		obj.put("closed_at",StringUtils.noNull(closedat));
		return obj;
	}

	public void log()
	{
		logger.trace(this.toString());
	}

	public HashMap getTableMap()
	{
		HashMap resultMap = new HashMap();
		ArrayList columnList = new ArrayList();
		resultMap.put("table", "ssi_attribute_factor_aggregate");

		columnList.add("id");				
		columnList.add("attribute");				
		columnList.add("attributename");				
		columnList.add("attributeindex");				
		columnList.add("factor");				
		columnList.add("region");				
		columnList.add("state");				
		columnList.add("city");				
		columnList.add("dealer");				
		columnList.add("model");				
		columnList.add("bi_weekly");				
		columnList.add("month");				
		columnList.add("quarter");				
		columnList.add("dealer_attribute_sum");				
		columnList.add("dealer_attribute_count");				
		columnList.add("totalcount");				
		columnList.add("dealer_dissatisfied_count");				
		columnList.add("rstatus");				
		columnList.add("lastacc");				
		columnList.add("created_by");				
		columnList.add("created_at");				
		columnList.add("modified_at");				
		columnList.add("modified_by");				
		columnList.add("close_type");				
		columnList.add("closed_at");
		resultMap.put("ColumnList", columnList);

		return resultMap;
	}

	public void toggleStatus()
	{
		String currentStatus = StringUtils.noNull(getRstatus());
		setRstatus("1");
		if (currentStatus.equals("1"))
		{
			setRstatus("0");
		}
	}

}
