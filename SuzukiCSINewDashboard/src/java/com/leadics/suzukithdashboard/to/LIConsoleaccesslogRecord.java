
/*
 * LIConsoleaccesslogRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.to;
import com.leadics.suzukithdashboard.common.LIRecord;
import com.leadics.utils.*;
import org.json.simple.*;
import java.util.*;
public class LIConsoleaccesslogRecord extends LIRecord
{
	static LogUtils logger = new LogUtils(LIConsoleaccesslogRecord.class.getName());

	private String id;
	private String userid;
	private String channel;
	private String source;
	private String pagepath;
	private String sessionid;
	private String createdby;
	private String createdat;
	private String modifiedat;
	private String modifiedby;

	public String getId()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(id);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(id);
		}
		else
		{
			return id;
		}
	}

	public String getUserid()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(userid);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(userid);
		}
		else
		{
			return userid;
		}
	}

	public String getChannel()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(channel);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(channel);
		}
		else
		{
			return channel;
		}
	}

	public String getSource()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(source);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(source);
		}
		else
		{
			return source;
		}
	}

	public String getPagepath()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(pagepath);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(pagepath);
		}
		else
		{
			return pagepath;
		}
	}

	public String getSessionid()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sessionid);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sessionid);
		}
		else
		{
			return sessionid;
		}
	}

	public String getCreatedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdby);
		}
		else
		{
			return createdby;
		}
	}

	public String getCreatedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdat);
		}
		else
		{
			return createdat;
		}
	}

	public String getModifiedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedat);
		}
		else
		{
			return modifiedat;
		}
	}

	public String getModifiedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedby);
		}
		else
		{
			return modifiedby;
		}
	}


	public void setId(String value)
	{
		id = value;
	}

	public void setUserid(String value)
	{
		userid = value;
	}

	public void setChannel(String value)
	{
		channel = value;
	}

	public void setSource(String value)
	{
		source = value;
	}

	public void setPagepath(String value)
	{
		pagepath = value;
	}

	public void setSessionid(String value)
	{
		sessionid = value;
	}

	public void setCreatedby(String value)
	{
		createdby = value;
	}

	public void setCreatedat(String value)
	{
		createdat = value;
	}

	public void setModifiedat(String value)
	{
		modifiedat = value;
	}

	public void setModifiedby(String value)
	{
		modifiedby = value;
	}


	public String toString()
	{
		return "\nid:" + id +
				"\nuserid:" + userid +
				"\nchannel:" + channel +
				"\nsource:" + source +
				"\npagepath:" + pagepath +
				"\nsessionid:" + sessionid +
				"\ncreatedby:" + createdby +
				"\ncreatedat:" + createdat +
				"\nmodifiedat:" + modifiedat +
				"\nmodifiedby:" + modifiedby +
				"\n";
	}

	public void loadContent(LIConsoleaccesslogRecord inputRecord)
	{
		setId(inputRecord.getId());
		setUserid(inputRecord.getUserid());
		setChannel(inputRecord.getChannel());
		setSource(inputRecord.getSource());
		setPagepath(inputRecord.getPagepath());
		setSessionid(inputRecord.getSessionid());
		setCreatedby(inputRecord.getCreatedby());
		setCreatedat(inputRecord.getCreatedat());
		setModifiedat(inputRecord.getModifiedat());
		setModifiedby(inputRecord.getModifiedby());
	}

	public void loadNonNullContent(LIConsoleaccesslogRecord inputRecord)
	{
		if (StringUtils.hasChanged(getId(), inputRecord.getId()))
		{
			setId(StringUtils.noNull(inputRecord.getId()));
		}
		if (StringUtils.hasChanged(getUserid(), inputRecord.getUserid()))
		{
			setUserid(StringUtils.noNull(inputRecord.getUserid()));
		}
		if (StringUtils.hasChanged(getChannel(), inputRecord.getChannel()))
		{
			setChannel(StringUtils.noNull(inputRecord.getChannel()));
		}
		if (StringUtils.hasChanged(getSource(), inputRecord.getSource()))
		{
			setSource(StringUtils.noNull(inputRecord.getSource()));
		}
		if (StringUtils.hasChanged(getPagepath(), inputRecord.getPagepath()))
		{
			setPagepath(StringUtils.noNull(inputRecord.getPagepath()));
		}
		if (StringUtils.hasChanged(getSessionid(), inputRecord.getSessionid()))
		{
			setSessionid(StringUtils.noNull(inputRecord.getSessionid()));
		}
		if (StringUtils.hasChanged(getCreatedby(), inputRecord.getCreatedby()))
		{
			setCreatedby(StringUtils.noNull(inputRecord.getCreatedby()));
		}
		if (StringUtils.hasChanged(getCreatedat(), inputRecord.getCreatedat()))
		{
			setCreatedat(StringUtils.noNull(inputRecord.getCreatedat()));
		}
		if (StringUtils.hasChanged(getModifiedat(), inputRecord.getModifiedat()))
		{
			setModifiedat(StringUtils.noNull(inputRecord.getModifiedat()));
		}
		if (StringUtils.hasChanged(getModifiedby(), inputRecord.getModifiedby()))
		{
			setModifiedby(StringUtils.noNull(inputRecord.getModifiedby()));
		}
	}

	public JSONObject getJSONObject()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("userid",StringUtils.noNull(userid));				
		obj.put("channel",StringUtils.noNull(channel));				
		obj.put("source",StringUtils.noNull(source));				
		obj.put("pagepath",StringUtils.noNull(pagepath));				
		obj.put("sessionid",StringUtils.noNull(sessionid));				
		obj.put("createdby",StringUtils.noNull(createdby));				
		obj.put("createdat",StringUtils.noNull(createdat));				
		obj.put("modifiedat",StringUtils.noNull(modifiedat));				
		obj.put("modifiedby",StringUtils.noNull(modifiedby));
		return obj;
	}

	public void loadJSONObject(JSONObject obj)
	throws Exception
	{
		if (obj == null) return;

		id = StringUtils.getValueFromJSONObject(obj, "id");				
		userid = StringUtils.getValueFromJSONObject(obj, "userid");				
		channel = StringUtils.getValueFromJSONObject(obj, "channel");				
		source = StringUtils.getValueFromJSONObject(obj, "source");				
		pagepath = StringUtils.getValueFromJSONObject(obj, "pagepath");				
		sessionid = StringUtils.getValueFromJSONObject(obj, "sessionid");				
		createdby = StringUtils.getValueFromJSONObject(obj, "createdby");				
		createdat = StringUtils.getValueFromJSONObject(obj, "createdat");				
		modifiedat = StringUtils.getValueFromJSONObject(obj, "modifiedat");				
		modifiedby = StringUtils.getValueFromJSONObject(obj, "modifiedby");
		return;
	}

	public JSONObject getJSONObjectUI()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("user_id",StringUtils.noNull(userid));				
		obj.put("channel",StringUtils.noNull(channel));				
		obj.put("source",StringUtils.noNull(source));				
		obj.put("pagepath",StringUtils.noNull(pagepath));				
		obj.put("sessionid",StringUtils.noNull(sessionid));				
		obj.put("created_by",StringUtils.noNull(createdby));				
		obj.put("created_at",StringUtils.noNull(createdat));				
		obj.put("modified_at",StringUtils.noNull(modifiedat));				
		obj.put("modified_by",StringUtils.noNull(modifiedby));
		return obj;
	}

	public void log()
	{
		logger.trace(this.toString());
	}

	public HashMap getTableMap()
	{
		HashMap resultMap = new HashMap();
		ArrayList columnList = new ArrayList();
		resultMap.put("table", "console_access_log");

		columnList.add("id");				
		columnList.add("user_id");				
		columnList.add("channel");				
		columnList.add("source");				
		columnList.add("pagepath");				
		columnList.add("sessionid");				
		columnList.add("created_by");				
		columnList.add("created_at");				
		columnList.add("modified_at");				
		columnList.add("modified_by");
		resultMap.put("ColumnList", columnList);

		return resultMap;
	}

}
