
/*
 * LISsitranslatedRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.to;
import com.leadics.suzukithdashboard.common.LIRecord;
import com.leadics.utils.*;
import org.json.simple.*;
import java.util.*;
public class LISsitranslatedRecord extends LIRecord
{
	static LogUtils logger = new LogUtils(LISsitranslatedRecord.class.getName());

	private String srno;
	private String dbsrno;
	private String track;
	private String lstned;
	private String qcdone;
	private String tapeadt;
	private String extaudi;
	private String pdatey;
	private String pdatem;
	private String work;
	private String cmak1;
	private String percon;
	private String condate;
	private String ddatecon;
	private String confdeal;
	private String dealcon;
	private String mainuser;
	private String pdcmaker;
	private String vstford;
	private String qrot;
	private String waitdlr;
	private String effund;
	private String ictstdr;
	private String tdrdlr;
	private String tstrout;
	private String tstcar;
	private String tdover;
	private String ftsafety;
	private String ftconv;
	private String ftperf;
	private String ftcost;
	private String spprovd;
	private String spcall;
	private String sigreet;
	private String siexpln;
	private String sidemo;
	private String sifllow;
	private String siiover;
	private String dfseat;
	private String dfpark;
	private String dflit;
	private String dfclean;
	private String dftemp;
	private String dfref;
	private String dfsmell;
	private String dfsignge;
	private String dfrtrm;
	private String dfavshw;
	private String dlrlyot;
	private String dlrcfof;
	private String dlreslk;
	private String dlrsov;
	private String dlrloc;
	private String dlrover;
	private String vehfncd;
	private String dlrfnc;
	private String condeal;
	private String dlease;
	private String dlpexp;
	private String dlqkcom;
	private String dlprcex;
	private String dldover;
	private String icgroom;
	private String icneatly;
	private String icspent;
	private String icaskuse;
	private String icinform;
	private String icstatus;
	private String spkldge;
	private String spneed;
	private String spcrtsy;
	private String spprssr;
	private String spcommt;
	private String spover;
	private String icsame;
	private String icday;
	private String dtvsexp;
	private String dldelct;
	private String dtlen;
	private String dtstatus;
	private String dttime;
	private String dtover;
	private String afvehft;
	private String afope;
	private String afwarr;
	private String afmaint;
	private String dlhelp;
	private String dlintro;
	private String dlenftme;
	private String dlthank;
	private String dlramtf;
	private String dlspcere;
	private String dlvisit;
	private String dlext;
	private String dlfeat;
	private String dlans;
	private String dlfeebk;
	private String dlconta;
	private String dlappt;
	private String dldelpt;
	private String delhr;
	private String delmin;
	private String delcar;
	private String dpconvh;
	private String dpenthu;
	private String dptime;
	private String dpans;
	private String dpthor;
	private String dpover;
	private String ovover;
	private String prbselct;
	private String prbstaf;
	private String prbchrge;
	private String prbanswr;
	private String prbshff;
	private String prbphony;
	private String recodlr;
	private String psamedlr;
	private String recomake;
	private String reprmake;
	private String ecdlr;
	private String ecveh;
	private String firstveh;
	private String newveh;
	private String jdpres;
	private String jdpinf;
	private String fc;
	private String reveal;
	private String provfb;
	private String gender;
	private String dtmn;
	private String intid;
	private String intgen;
	private String lmn;
	private String intme;
	private String txtoestart;
	private String txtintid;
	private String txtoeend;
	private String scity;
	private String intday;
	private String intmth;
	private String intdate;
	private String dbstart;
	private String dbcustomername;
	private String dbcustomercity;
	private String dbcustomerstate;
	private String dbmobile;
	private String dbtel1;
	private String dbtel2;
	private String dbmodel;
	private String dbmodelvarient;
	private String dbfueltype;
	private String dbvin;
	private String dbdealercode;
	private String dbdealername;
	private String dbdealercity;
	private String dbdealerstate;
	private String dbdeliverydate;
	private String dbsalesconsultantcode;
	private String dbsalesconsultantname;
	private String dbpandacode;
	private String dbdealer2;
	private String dbdealer2labels;
	private String dbquarternum;
	private String dbbatchnum;
	private String dbend;
	private String fdstart;
	private String dealer;
	private String dealer2;
	private String city2;
	private String regiona;
	private String state2;
	private String model;
	private String model2;
	private String city3;
	private String fdend;
	private String indexoutlierfilter;
	private String irecodestart;
	private String zdelhr;
	private String zdelmin;
	private String cdelmin;
	private String ncicday;
	private String ncdelmin;
	private String irecodeend;
	private String recodestart;
	private String ntdover;
	private String nsigreet;
	private String nsiexpln;
	private String nsidemo;
	private String nsifllow;
	private String nsiiover;
	private String ndlrlyot;
	private String ndlrcfof;
	private String ndlreslk;
	private String ndlrsov;
	private String ndlrloc;
	private String ndlrover;
	private String ndlease;
	private String ndlpexp;
	private String ndlqkcom;
	private String ndlprcex;
	private String ndldover;
	private String nspkldge;
	private String nspneed;
	private String nspcrtsy;
	private String nspprssr;
	private String nspcommt;
	private String nspover;
	private String ndtlen;
	private String ndtstatus;
	private String ndttime;
	private String ndtover;
	private String ndpconvh;
	private String ndpenthu;
	private String ndptime;
	private String ndpans;
	private String ndpthor;
	private String ndpover;
	private String novover;
	private String ttdover;
	private String tsigreet;
	private String tsiexpln;
	private String tsidemo;
	private String tsifllow;
	private String tsiiover;
	private String tdlrlyot;
	private String tdlrcfof;
	private String tdlreslk;
	private String tdlrsov;
	private String tdlrloc;
	private String tdlrover;
	private String tdlease;
	private String tdlpexp;
	private String tdlqkcom;
	private String tdlprcex;
	private String tdldover;
	private String tspkldge;
	private String tspneed;
	private String tspcrtsy;
	private String tspprssr;
	private String tspcommt;
	private String tspover;
	private String tdtlen;
	private String tdtstatus;
	private String tdttime;
	private String tdtover;
	private String tdpconvh;
	private String tdpenthu;
	private String tdptime;
	private String tdpans;
	private String tdpthor;
	private String tdpover;
	private String tovover;
	private String atdover;
	private String asigreet;
	private String asiexpln;
	private String asidemo;
	private String asifllow;
	private String asiiover;
	private String adlrlyot;
	private String adlrcfof;
	private String adlreslk;
	private String adlrsov;
	private String adlrloc;
	private String adlrover;
	private String adlease;
	private String adlpexp;
	private String adlqkcom;
	private String adlprcex;
	private String adldover;
	private String aspkldge;
	private String aspneed;
	private String aspcrtsy;
	private String aspprssr;
	private String aspcommt;
	private String aspover;
	private String adtlen;
	private String adtstatus;
	private String adttime;
	private String adtover;
	private String adpconvh;
	private String adpenthu;
	private String adptime;
	private String adpans;
	private String adpthor;
	private String adpover;
	private String aovover;
	private String prb;
	private String nprb;
	private String nnprb;
	private String cntft;
	private String ncntft;
	private String cntexp;
	private String ncntexp;
	private String sop;
	private String nsop;
	private String comsop;
	private String ncomsop;
	private String prosop;
	private String nprosop;
	private String recodeend;
	private String index;
	private String si;
	private String df;
	private String deal;
	private String sp;
	private String dt;
	private String dp;
	private String nindex;
	private String biweekly;
	private String month;
	private String quarter;
	private String year;

	public String getSrno()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(srno);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(srno);
		}
		else
		{
			return srno;
		}
	}

	public String getDbsrno()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbsrno);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbsrno);
		}
		else
		{
			return dbsrno;
		}
	}

	public String getTrack()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(track);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(track);
		}
		else
		{
			return track;
		}
	}

	public String getLstned()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(lstned);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(lstned);
		}
		else
		{
			return lstned;
		}
	}

	public String getQcdone()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(qcdone);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(qcdone);
		}
		else
		{
			return qcdone;
		}
	}

	public String getTapeadt()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tapeadt);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tapeadt);
		}
		else
		{
			return tapeadt;
		}
	}

	public String getExtaudi()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(extaudi);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(extaudi);
		}
		else
		{
			return extaudi;
		}
	}

	public String getPdatey()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(pdatey);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(pdatey);
		}
		else
		{
			return pdatey;
		}
	}

	public String getPdatem()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(pdatem);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(pdatem);
		}
		else
		{
			return pdatem;
		}
	}

	public String getWork()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(work);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(work);
		}
		else
		{
			return work;
		}
	}

	public String getCmak1()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(cmak1);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(cmak1);
		}
		else
		{
			return cmak1;
		}
	}

	public String getPercon()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(percon);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(percon);
		}
		else
		{
			return percon;
		}
	}

	public String getCondate()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(condate);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(condate);
		}
		else
		{
			return condate;
		}
	}

	public String getDdatecon()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ddatecon);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ddatecon);
		}
		else
		{
			return ddatecon;
		}
	}

	public String getConfdeal()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(confdeal);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(confdeal);
		}
		else
		{
			return confdeal;
		}
	}

	public String getDealcon()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dealcon);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dealcon);
		}
		else
		{
			return dealcon;
		}
	}

	public String getMainuser()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(mainuser);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(mainuser);
		}
		else
		{
			return mainuser;
		}
	}

	public String getPdcmaker()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(pdcmaker);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(pdcmaker);
		}
		else
		{
			return pdcmaker;
		}
	}

	public String getVstford()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(vstford);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(vstford);
		}
		else
		{
			return vstford;
		}
	}

	public String getQrot()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(qrot);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(qrot);
		}
		else
		{
			return qrot;
		}
	}

	public String getWaitdlr()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(waitdlr);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(waitdlr);
		}
		else
		{
			return waitdlr;
		}
	}

	public String getEffund()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(effund);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(effund);
		}
		else
		{
			return effund;
		}
	}

	public String getIctstdr()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ictstdr);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ictstdr);
		}
		else
		{
			return ictstdr;
		}
	}

	public String getTdrdlr()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tdrdlr);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tdrdlr);
		}
		else
		{
			return tdrdlr;
		}
	}

	public String getTstrout()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tstrout);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tstrout);
		}
		else
		{
			return tstrout;
		}
	}

	public String getTstcar()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tstcar);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tstcar);
		}
		else
		{
			return tstcar;
		}
	}

	public String getTdover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tdover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tdover);
		}
		else
		{
			return tdover;
		}
	}

	public String getFtsafety()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ftsafety);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ftsafety);
		}
		else
		{
			return ftsafety;
		}
	}

	public String getFtconv()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ftconv);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ftconv);
		}
		else
		{
			return ftconv;
		}
	}

	public String getFtperf()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ftperf);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ftperf);
		}
		else
		{
			return ftperf;
		}
	}

	public String getFtcost()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ftcost);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ftcost);
		}
		else
		{
			return ftcost;
		}
	}

	public String getSpprovd()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(spprovd);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(spprovd);
		}
		else
		{
			return spprovd;
		}
	}

	public String getSpcall()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(spcall);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(spcall);
		}
		else
		{
			return spcall;
		}
	}

	public String getSigreet()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sigreet);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sigreet);
		}
		else
		{
			return sigreet;
		}
	}

	public String getSiexpln()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(siexpln);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(siexpln);
		}
		else
		{
			return siexpln;
		}
	}

	public String getSidemo()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sidemo);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sidemo);
		}
		else
		{
			return sidemo;
		}
	}

	public String getSifllow()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sifllow);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sifllow);
		}
		else
		{
			return sifllow;
		}
	}

	public String getSiiover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(siiover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(siiover);
		}
		else
		{
			return siiover;
		}
	}

	public String getDfseat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dfseat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dfseat);
		}
		else
		{
			return dfseat;
		}
	}

	public String getDfpark()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dfpark);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dfpark);
		}
		else
		{
			return dfpark;
		}
	}

	public String getDflit()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dflit);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dflit);
		}
		else
		{
			return dflit;
		}
	}

	public String getDfclean()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dfclean);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dfclean);
		}
		else
		{
			return dfclean;
		}
	}

	public String getDftemp()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dftemp);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dftemp);
		}
		else
		{
			return dftemp;
		}
	}

	public String getDfref()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dfref);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dfref);
		}
		else
		{
			return dfref;
		}
	}

	public String getDfsmell()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dfsmell);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dfsmell);
		}
		else
		{
			return dfsmell;
		}
	}

	public String getDfsignge()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dfsignge);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dfsignge);
		}
		else
		{
			return dfsignge;
		}
	}

	public String getDfrtrm()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dfrtrm);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dfrtrm);
		}
		else
		{
			return dfrtrm;
		}
	}

	public String getDfavshw()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dfavshw);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dfavshw);
		}
		else
		{
			return dfavshw;
		}
	}

	public String getDlrlyot()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dlrlyot);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dlrlyot);
		}
		else
		{
			return dlrlyot;
		}
	}

	public String getDlrcfof()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dlrcfof);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dlrcfof);
		}
		else
		{
			return dlrcfof;
		}
	}

	public String getDlreslk()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dlreslk);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dlreslk);
		}
		else
		{
			return dlreslk;
		}
	}

	public String getDlrsov()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dlrsov);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dlrsov);
		}
		else
		{
			return dlrsov;
		}
	}

	public String getDlrloc()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dlrloc);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dlrloc);
		}
		else
		{
			return dlrloc;
		}
	}

	public String getDlrover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dlrover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dlrover);
		}
		else
		{
			return dlrover;
		}
	}

	public String getVehfncd()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(vehfncd);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(vehfncd);
		}
		else
		{
			return vehfncd;
		}
	}

	public String getDlrfnc()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dlrfnc);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dlrfnc);
		}
		else
		{
			return dlrfnc;
		}
	}

	public String getCondeal()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(condeal);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(condeal);
		}
		else
		{
			return condeal;
		}
	}

	public String getDlease()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dlease);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dlease);
		}
		else
		{
			return dlease;
		}
	}

	public String getDlpexp()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dlpexp);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dlpexp);
		}
		else
		{
			return dlpexp;
		}
	}

	public String getDlqkcom()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dlqkcom);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dlqkcom);
		}
		else
		{
			return dlqkcom;
		}
	}

	public String getDlprcex()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dlprcex);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dlprcex);
		}
		else
		{
			return dlprcex;
		}
	}

	public String getDldover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dldover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dldover);
		}
		else
		{
			return dldover;
		}
	}

	public String getIcgroom()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(icgroom);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(icgroom);
		}
		else
		{
			return icgroom;
		}
	}

	public String getIcneatly()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(icneatly);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(icneatly);
		}
		else
		{
			return icneatly;
		}
	}

	public String getIcspent()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(icspent);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(icspent);
		}
		else
		{
			return icspent;
		}
	}

	public String getIcaskuse()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(icaskuse);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(icaskuse);
		}
		else
		{
			return icaskuse;
		}
	}

	public String getIcinform()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(icinform);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(icinform);
		}
		else
		{
			return icinform;
		}
	}

	public String getIcstatus()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(icstatus);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(icstatus);
		}
		else
		{
			return icstatus;
		}
	}

	public String getSpkldge()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(spkldge);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(spkldge);
		}
		else
		{
			return spkldge;
		}
	}

	public String getSpneed()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(spneed);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(spneed);
		}
		else
		{
			return spneed;
		}
	}

	public String getSpcrtsy()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(spcrtsy);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(spcrtsy);
		}
		else
		{
			return spcrtsy;
		}
	}

	public String getSpprssr()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(spprssr);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(spprssr);
		}
		else
		{
			return spprssr;
		}
	}

	public String getSpcommt()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(spcommt);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(spcommt);
		}
		else
		{
			return spcommt;
		}
	}

	public String getSpover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(spover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(spover);
		}
		else
		{
			return spover;
		}
	}

	public String getIcsame()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(icsame);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(icsame);
		}
		else
		{
			return icsame;
		}
	}

	public String getIcday()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(icday);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(icday);
		}
		else
		{
			return icday;
		}
	}

	public String getDtvsexp()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dtvsexp);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dtvsexp);
		}
		else
		{
			return dtvsexp;
		}
	}

	public String getDldelct()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dldelct);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dldelct);
		}
		else
		{
			return dldelct;
		}
	}

	public String getDtlen()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dtlen);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dtlen);
		}
		else
		{
			return dtlen;
		}
	}

	public String getDtstatus()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dtstatus);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dtstatus);
		}
		else
		{
			return dtstatus;
		}
	}

	public String getDttime()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dttime);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dttime);
		}
		else
		{
			return dttime;
		}
	}

	public String getDtover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dtover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dtover);
		}
		else
		{
			return dtover;
		}
	}

	public String getAfvehft()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(afvehft);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(afvehft);
		}
		else
		{
			return afvehft;
		}
	}

	public String getAfope()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(afope);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(afope);
		}
		else
		{
			return afope;
		}
	}

	public String getAfwarr()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(afwarr);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(afwarr);
		}
		else
		{
			return afwarr;
		}
	}

	public String getAfmaint()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(afmaint);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(afmaint);
		}
		else
		{
			return afmaint;
		}
	}

	public String getDlhelp()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dlhelp);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dlhelp);
		}
		else
		{
			return dlhelp;
		}
	}

	public String getDlintro()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dlintro);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dlintro);
		}
		else
		{
			return dlintro;
		}
	}

	public String getDlenftme()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dlenftme);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dlenftme);
		}
		else
		{
			return dlenftme;
		}
	}

	public String getDlthank()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dlthank);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dlthank);
		}
		else
		{
			return dlthank;
		}
	}

	public String getDlramtf()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dlramtf);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dlramtf);
		}
		else
		{
			return dlramtf;
		}
	}

	public String getDlspcere()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dlspcere);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dlspcere);
		}
		else
		{
			return dlspcere;
		}
	}

	public String getDlvisit()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dlvisit);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dlvisit);
		}
		else
		{
			return dlvisit;
		}
	}

	public String getDlext()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dlext);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dlext);
		}
		else
		{
			return dlext;
		}
	}

	public String getDlfeat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dlfeat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dlfeat);
		}
		else
		{
			return dlfeat;
		}
	}

	public String getDlans()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dlans);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dlans);
		}
		else
		{
			return dlans;
		}
	}

	public String getDlfeebk()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dlfeebk);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dlfeebk);
		}
		else
		{
			return dlfeebk;
		}
	}

	public String getDlconta()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dlconta);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dlconta);
		}
		else
		{
			return dlconta;
		}
	}

	public String getDlappt()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dlappt);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dlappt);
		}
		else
		{
			return dlappt;
		}
	}

	public String getDldelpt()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dldelpt);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dldelpt);
		}
		else
		{
			return dldelpt;
		}
	}

	public String getDelhr()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(delhr);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(delhr);
		}
		else
		{
			return delhr;
		}
	}

	public String getDelmin()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(delmin);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(delmin);
		}
		else
		{
			return delmin;
		}
	}

	public String getDelcar()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(delcar);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(delcar);
		}
		else
		{
			return delcar;
		}
	}

	public String getDpconvh()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dpconvh);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dpconvh);
		}
		else
		{
			return dpconvh;
		}
	}

	public String getDpenthu()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dpenthu);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dpenthu);
		}
		else
		{
			return dpenthu;
		}
	}

	public String getDptime()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dptime);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dptime);
		}
		else
		{
			return dptime;
		}
	}

	public String getDpans()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dpans);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dpans);
		}
		else
		{
			return dpans;
		}
	}

	public String getDpthor()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dpthor);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dpthor);
		}
		else
		{
			return dpthor;
		}
	}

	public String getDpover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dpover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dpover);
		}
		else
		{
			return dpover;
		}
	}

	public String getOvover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ovover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ovover);
		}
		else
		{
			return ovover;
		}
	}

	public String getPrbselct()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(prbselct);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(prbselct);
		}
		else
		{
			return prbselct;
		}
	}

	public String getPrbstaf()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(prbstaf);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(prbstaf);
		}
		else
		{
			return prbstaf;
		}
	}

	public String getPrbchrge()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(prbchrge);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(prbchrge);
		}
		else
		{
			return prbchrge;
		}
	}

	public String getPrbanswr()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(prbanswr);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(prbanswr);
		}
		else
		{
			return prbanswr;
		}
	}

	public String getPrbshff()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(prbshff);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(prbshff);
		}
		else
		{
			return prbshff;
		}
	}

	public String getPrbphony()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(prbphony);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(prbphony);
		}
		else
		{
			return prbphony;
		}
	}

	public String getRecodlr()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(recodlr);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(recodlr);
		}
		else
		{
			return recodlr;
		}
	}

	public String getPsamedlr()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(psamedlr);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(psamedlr);
		}
		else
		{
			return psamedlr;
		}
	}

	public String getRecomake()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(recomake);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(recomake);
		}
		else
		{
			return recomake;
		}
	}

	public String getReprmake()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(reprmake);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(reprmake);
		}
		else
		{
			return reprmake;
		}
	}

	public String getEcdlr()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ecdlr);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ecdlr);
		}
		else
		{
			return ecdlr;
		}
	}

	public String getEcveh()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ecveh);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ecveh);
		}
		else
		{
			return ecveh;
		}
	}

	public String getFirstveh()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(firstveh);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(firstveh);
		}
		else
		{
			return firstveh;
		}
	}

	public String getNewveh()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(newveh);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(newveh);
		}
		else
		{
			return newveh;
		}
	}

	public String getJdpres()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(jdpres);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(jdpres);
		}
		else
		{
			return jdpres;
		}
	}

	public String getJdpinf()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(jdpinf);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(jdpinf);
		}
		else
		{
			return jdpinf;
		}
	}

	public String getFc()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(fc);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(fc);
		}
		else
		{
			return fc;
		}
	}

	public String getReveal()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(reveal);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(reveal);
		}
		else
		{
			return reveal;
		}
	}

	public String getProvfb()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(provfb);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(provfb);
		}
		else
		{
			return provfb;
		}
	}

	public String getGender()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(gender);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(gender);
		}
		else
		{
			return gender;
		}
	}

	public String getDtmn()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dtmn);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dtmn);
		}
		else
		{
			return dtmn;
		}
	}

	public String getIntid()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(intid);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(intid);
		}
		else
		{
			return intid;
		}
	}

	public String getIntgen()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(intgen);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(intgen);
		}
		else
		{
			return intgen;
		}
	}

	public String getLmn()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(lmn);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(lmn);
		}
		else
		{
			return lmn;
		}
	}

	public String getIntme()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(intme);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(intme);
		}
		else
		{
			return intme;
		}
	}

	public String getTxtoestart()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(txtoestart);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(txtoestart);
		}
		else
		{
			return txtoestart;
		}
	}

	public String getTxtintid()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(txtintid);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(txtintid);
		}
		else
		{
			return txtintid;
		}
	}

	public String getTxtoeend()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(txtoeend);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(txtoeend);
		}
		else
		{
			return txtoeend;
		}
	}

	public String getScity()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(scity);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(scity);
		}
		else
		{
			return scity;
		}
	}

	public String getIntday()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(intday);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(intday);
		}
		else
		{
			return intday;
		}
	}

	public String getIntmth()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(intmth);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(intmth);
		}
		else
		{
			return intmth;
		}
	}

	public String getIntdate()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(intdate);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(intdate);
		}
		else
		{
			return intdate;
		}
	}

	public String getDbstart()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbstart);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbstart);
		}
		else
		{
			return dbstart;
		}
	}

	public String getDbcustomername()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbcustomername);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbcustomername);
		}
		else
		{
			return dbcustomername;
		}
	}

	public String getDbcustomercity()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbcustomercity);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbcustomercity);
		}
		else
		{
			return dbcustomercity;
		}
	}

	public String getDbcustomerstate()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbcustomerstate);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbcustomerstate);
		}
		else
		{
			return dbcustomerstate;
		}
	}

	public String getDbmobile()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbmobile);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbmobile);
		}
		else
		{
			return dbmobile;
		}
	}

	public String getDbtel1()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbtel1);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbtel1);
		}
		else
		{
			return dbtel1;
		}
	}

	public String getDbtel2()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbtel2);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbtel2);
		}
		else
		{
			return dbtel2;
		}
	}

	public String getDbmodel()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbmodel);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbmodel);
		}
		else
		{
			return dbmodel;
		}
	}

	public String getDbmodelvarient()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbmodelvarient);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbmodelvarient);
		}
		else
		{
			return dbmodelvarient;
		}
	}

	public String getDbfueltype()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbfueltype);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbfueltype);
		}
		else
		{
			return dbfueltype;
		}
	}

	public String getDbvin()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbvin);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbvin);
		}
		else
		{
			return dbvin;
		}
	}

	public String getDbdealercode()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbdealercode);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbdealercode);
		}
		else
		{
			return dbdealercode;
		}
	}

	public String getDbdealername()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbdealername);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbdealername);
		}
		else
		{
			return dbdealername;
		}
	}

	public String getDbdealercity()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbdealercity);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbdealercity);
		}
		else
		{
			return dbdealercity;
		}
	}

	public String getDbdealerstate()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbdealerstate);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbdealerstate);
		}
		else
		{
			return dbdealerstate;
		}
	}

	public String getDbdeliverydate()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbdeliverydate);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbdeliverydate);
		}
		else
		{
			return dbdeliverydate;
		}
	}

	public String getDbsalesconsultantcode()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbsalesconsultantcode);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbsalesconsultantcode);
		}
		else
		{
			return dbsalesconsultantcode;
		}
	}

	public String getDbsalesconsultantname()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbsalesconsultantname);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbsalesconsultantname);
		}
		else
		{
			return dbsalesconsultantname;
		}
	}

	public String getDbpandacode()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbpandacode);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbpandacode);
		}
		else
		{
			return dbpandacode;
		}
	}

	public String getDbdealer2()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbdealer2);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbdealer2);
		}
		else
		{
			return dbdealer2;
		}
	}

	public String getDbdealer2labels()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbdealer2labels);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbdealer2labels);
		}
		else
		{
			return dbdealer2labels;
		}
	}

	public String getDbquarternum()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbquarternum);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbquarternum);
		}
		else
		{
			return dbquarternum;
		}
	}

	public String getDbbatchnum()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbbatchnum);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbbatchnum);
		}
		else
		{
			return dbbatchnum;
		}
	}

	public String getDbend()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbend);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbend);
		}
		else
		{
			return dbend;
		}
	}

	public String getFdstart()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(fdstart);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(fdstart);
		}
		else
		{
			return fdstart;
		}
	}

	public String getDealer()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dealer);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dealer);
		}
		else
		{
			return dealer;
		}
	}

	public String getDealer2()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dealer2);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dealer2);
		}
		else
		{
			return dealer2;
		}
	}

	public String getCity2()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(city2);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(city2);
		}
		else
		{
			return city2;
		}
	}

	public String getRegiona()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(regiona);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(regiona);
		}
		else
		{
			return regiona;
		}
	}

	public String getState2()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(state2);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(state2);
		}
		else
		{
			return state2;
		}
	}

	public String getModel()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(model);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(model);
		}
		else
		{
			return model;
		}
	}

	public String getModel2()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(model2);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(model2);
		}
		else
		{
			return model2;
		}
	}

	public String getCity3()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(city3);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(city3);
		}
		else
		{
			return city3;
		}
	}

	public String getFdend()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(fdend);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(fdend);
		}
		else
		{
			return fdend;
		}
	}

	public String getIndexoutlierfilter()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(indexoutlierfilter);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(indexoutlierfilter);
		}
		else
		{
			return indexoutlierfilter;
		}
	}

	public String getIrecodestart()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(irecodestart);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(irecodestart);
		}
		else
		{
			return irecodestart;
		}
	}

	public String getZdelhr()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(zdelhr);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(zdelhr);
		}
		else
		{
			return zdelhr;
		}
	}

	public String getZdelmin()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(zdelmin);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(zdelmin);
		}
		else
		{
			return zdelmin;
		}
	}

	public String getCdelmin()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(cdelmin);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(cdelmin);
		}
		else
		{
			return cdelmin;
		}
	}

	public String getNcicday()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ncicday);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ncicday);
		}
		else
		{
			return ncicday;
		}
	}

	public String getNcdelmin()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ncdelmin);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ncdelmin);
		}
		else
		{
			return ncdelmin;
		}
	}

	public String getIrecodeend()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(irecodeend);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(irecodeend);
		}
		else
		{
			return irecodeend;
		}
	}

	public String getRecodestart()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(recodestart);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(recodestart);
		}
		else
		{
			return recodestart;
		}
	}

	public String getNtdover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ntdover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ntdover);
		}
		else
		{
			return ntdover;
		}
	}

	public String getNsigreet()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsigreet);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsigreet);
		}
		else
		{
			return nsigreet;
		}
	}

	public String getNsiexpln()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsiexpln);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsiexpln);
		}
		else
		{
			return nsiexpln;
		}
	}

	public String getNsidemo()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsidemo);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsidemo);
		}
		else
		{
			return nsidemo;
		}
	}

	public String getNsifllow()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsifllow);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsifllow);
		}
		else
		{
			return nsifllow;
		}
	}

	public String getNsiiover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsiiover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsiiover);
		}
		else
		{
			return nsiiover;
		}
	}

	public String getNdlrlyot()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ndlrlyot);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ndlrlyot);
		}
		else
		{
			return ndlrlyot;
		}
	}

	public String getNdlrcfof()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ndlrcfof);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ndlrcfof);
		}
		else
		{
			return ndlrcfof;
		}
	}

	public String getNdlreslk()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ndlreslk);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ndlreslk);
		}
		else
		{
			return ndlreslk;
		}
	}

	public String getNdlrsov()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ndlrsov);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ndlrsov);
		}
		else
		{
			return ndlrsov;
		}
	}

	public String getNdlrloc()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ndlrloc);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ndlrloc);
		}
		else
		{
			return ndlrloc;
		}
	}

	public String getNdlrover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ndlrover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ndlrover);
		}
		else
		{
			return ndlrover;
		}
	}

	public String getNdlease()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ndlease);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ndlease);
		}
		else
		{
			return ndlease;
		}
	}

	public String getNdlpexp()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ndlpexp);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ndlpexp);
		}
		else
		{
			return ndlpexp;
		}
	}

	public String getNdlqkcom()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ndlqkcom);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ndlqkcom);
		}
		else
		{
			return ndlqkcom;
		}
	}

	public String getNdlprcex()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ndlprcex);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ndlprcex);
		}
		else
		{
			return ndlprcex;
		}
	}

	public String getNdldover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ndldover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ndldover);
		}
		else
		{
			return ndldover;
		}
	}

	public String getNspkldge()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nspkldge);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nspkldge);
		}
		else
		{
			return nspkldge;
		}
	}

	public String getNspneed()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nspneed);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nspneed);
		}
		else
		{
			return nspneed;
		}
	}

	public String getNspcrtsy()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nspcrtsy);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nspcrtsy);
		}
		else
		{
			return nspcrtsy;
		}
	}

	public String getNspprssr()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nspprssr);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nspprssr);
		}
		else
		{
			return nspprssr;
		}
	}

	public String getNspcommt()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nspcommt);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nspcommt);
		}
		else
		{
			return nspcommt;
		}
	}

	public String getNspover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nspover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nspover);
		}
		else
		{
			return nspover;
		}
	}

	public String getNdtlen()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ndtlen);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ndtlen);
		}
		else
		{
			return ndtlen;
		}
	}

	public String getNdtstatus()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ndtstatus);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ndtstatus);
		}
		else
		{
			return ndtstatus;
		}
	}

	public String getNdttime()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ndttime);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ndttime);
		}
		else
		{
			return ndttime;
		}
	}

	public String getNdtover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ndtover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ndtover);
		}
		else
		{
			return ndtover;
		}
	}

	public String getNdpconvh()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ndpconvh);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ndpconvh);
		}
		else
		{
			return ndpconvh;
		}
	}

	public String getNdpenthu()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ndpenthu);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ndpenthu);
		}
		else
		{
			return ndpenthu;
		}
	}

	public String getNdptime()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ndptime);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ndptime);
		}
		else
		{
			return ndptime;
		}
	}

	public String getNdpans()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ndpans);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ndpans);
		}
		else
		{
			return ndpans;
		}
	}

	public String getNdpthor()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ndpthor);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ndpthor);
		}
		else
		{
			return ndpthor;
		}
	}

	public String getNdpover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ndpover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ndpover);
		}
		else
		{
			return ndpover;
		}
	}

	public String getNovover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(novover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(novover);
		}
		else
		{
			return novover;
		}
	}

	public String getTtdover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ttdover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ttdover);
		}
		else
		{
			return ttdover;
		}
	}

	public String getTsigreet()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tsigreet);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tsigreet);
		}
		else
		{
			return tsigreet;
		}
	}

	public String getTsiexpln()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tsiexpln);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tsiexpln);
		}
		else
		{
			return tsiexpln;
		}
	}

	public String getTsidemo()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tsidemo);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tsidemo);
		}
		else
		{
			return tsidemo;
		}
	}

	public String getTsifllow()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tsifllow);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tsifllow);
		}
		else
		{
			return tsifllow;
		}
	}

	public String getTsiiover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tsiiover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tsiiover);
		}
		else
		{
			return tsiiover;
		}
	}

	public String getTdlrlyot()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tdlrlyot);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tdlrlyot);
		}
		else
		{
			return tdlrlyot;
		}
	}

	public String getTdlrcfof()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tdlrcfof);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tdlrcfof);
		}
		else
		{
			return tdlrcfof;
		}
	}

	public String getTdlreslk()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tdlreslk);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tdlreslk);
		}
		else
		{
			return tdlreslk;
		}
	}

	public String getTdlrsov()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tdlrsov);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tdlrsov);
		}
		else
		{
			return tdlrsov;
		}
	}

	public String getTdlrloc()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tdlrloc);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tdlrloc);
		}
		else
		{
			return tdlrloc;
		}
	}

	public String getTdlrover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tdlrover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tdlrover);
		}
		else
		{
			return tdlrover;
		}
	}

	public String getTdlease()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tdlease);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tdlease);
		}
		else
		{
			return tdlease;
		}
	}

	public String getTdlpexp()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tdlpexp);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tdlpexp);
		}
		else
		{
			return tdlpexp;
		}
	}

	public String getTdlqkcom()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tdlqkcom);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tdlqkcom);
		}
		else
		{
			return tdlqkcom;
		}
	}

	public String getTdlprcex()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tdlprcex);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tdlprcex);
		}
		else
		{
			return tdlprcex;
		}
	}

	public String getTdldover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tdldover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tdldover);
		}
		else
		{
			return tdldover;
		}
	}

	public String getTspkldge()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tspkldge);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tspkldge);
		}
		else
		{
			return tspkldge;
		}
	}

	public String getTspneed()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tspneed);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tspneed);
		}
		else
		{
			return tspneed;
		}
	}

	public String getTspcrtsy()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tspcrtsy);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tspcrtsy);
		}
		else
		{
			return tspcrtsy;
		}
	}

	public String getTspprssr()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tspprssr);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tspprssr);
		}
		else
		{
			return tspprssr;
		}
	}

	public String getTspcommt()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tspcommt);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tspcommt);
		}
		else
		{
			return tspcommt;
		}
	}

	public String getTspover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tspover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tspover);
		}
		else
		{
			return tspover;
		}
	}

	public String getTdtlen()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tdtlen);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tdtlen);
		}
		else
		{
			return tdtlen;
		}
	}

	public String getTdtstatus()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tdtstatus);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tdtstatus);
		}
		else
		{
			return tdtstatus;
		}
	}

	public String getTdttime()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tdttime);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tdttime);
		}
		else
		{
			return tdttime;
		}
	}

	public String getTdtover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tdtover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tdtover);
		}
		else
		{
			return tdtover;
		}
	}

	public String getTdpconvh()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tdpconvh);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tdpconvh);
		}
		else
		{
			return tdpconvh;
		}
	}

	public String getTdpenthu()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tdpenthu);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tdpenthu);
		}
		else
		{
			return tdpenthu;
		}
	}

	public String getTdptime()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tdptime);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tdptime);
		}
		else
		{
			return tdptime;
		}
	}

	public String getTdpans()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tdpans);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tdpans);
		}
		else
		{
			return tdpans;
		}
	}

	public String getTdpthor()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tdpthor);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tdpthor);
		}
		else
		{
			return tdpthor;
		}
	}

	public String getTdpover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tdpover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tdpover);
		}
		else
		{
			return tdpover;
		}
	}

	public String getTovover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tovover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tovover);
		}
		else
		{
			return tovover;
		}
	}

	public String getAtdover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(atdover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(atdover);
		}
		else
		{
			return atdover;
		}
	}

	public String getAsigreet()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(asigreet);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(asigreet);
		}
		else
		{
			return asigreet;
		}
	}

	public String getAsiexpln()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(asiexpln);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(asiexpln);
		}
		else
		{
			return asiexpln;
		}
	}

	public String getAsidemo()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(asidemo);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(asidemo);
		}
		else
		{
			return asidemo;
		}
	}

	public String getAsifllow()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(asifllow);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(asifllow);
		}
		else
		{
			return asifllow;
		}
	}

	public String getAsiiover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(asiiover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(asiiover);
		}
		else
		{
			return asiiover;
		}
	}

	public String getAdlrlyot()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(adlrlyot);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(adlrlyot);
		}
		else
		{
			return adlrlyot;
		}
	}

	public String getAdlrcfof()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(adlrcfof);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(adlrcfof);
		}
		else
		{
			return adlrcfof;
		}
	}

	public String getAdlreslk()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(adlreslk);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(adlreslk);
		}
		else
		{
			return adlreslk;
		}
	}

	public String getAdlrsov()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(adlrsov);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(adlrsov);
		}
		else
		{
			return adlrsov;
		}
	}

	public String getAdlrloc()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(adlrloc);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(adlrloc);
		}
		else
		{
			return adlrloc;
		}
	}

	public String getAdlrover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(adlrover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(adlrover);
		}
		else
		{
			return adlrover;
		}
	}

	public String getAdlease()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(adlease);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(adlease);
		}
		else
		{
			return adlease;
		}
	}

	public String getAdlpexp()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(adlpexp);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(adlpexp);
		}
		else
		{
			return adlpexp;
		}
	}

	public String getAdlqkcom()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(adlqkcom);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(adlqkcom);
		}
		else
		{
			return adlqkcom;
		}
	}

	public String getAdlprcex()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(adlprcex);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(adlprcex);
		}
		else
		{
			return adlprcex;
		}
	}

	public String getAdldover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(adldover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(adldover);
		}
		else
		{
			return adldover;
		}
	}

	public String getAspkldge()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(aspkldge);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(aspkldge);
		}
		else
		{
			return aspkldge;
		}
	}

	public String getAspneed()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(aspneed);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(aspneed);
		}
		else
		{
			return aspneed;
		}
	}

	public String getAspcrtsy()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(aspcrtsy);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(aspcrtsy);
		}
		else
		{
			return aspcrtsy;
		}
	}

	public String getAspprssr()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(aspprssr);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(aspprssr);
		}
		else
		{
			return aspprssr;
		}
	}

	public String getAspcommt()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(aspcommt);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(aspcommt);
		}
		else
		{
			return aspcommt;
		}
	}

	public String getAspover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(aspover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(aspover);
		}
		else
		{
			return aspover;
		}
	}

	public String getAdtlen()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(adtlen);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(adtlen);
		}
		else
		{
			return adtlen;
		}
	}

	public String getAdtstatus()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(adtstatus);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(adtstatus);
		}
		else
		{
			return adtstatus;
		}
	}

	public String getAdttime()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(adttime);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(adttime);
		}
		else
		{
			return adttime;
		}
	}

	public String getAdtover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(adtover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(adtover);
		}
		else
		{
			return adtover;
		}
	}

	public String getAdpconvh()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(adpconvh);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(adpconvh);
		}
		else
		{
			return adpconvh;
		}
	}

	public String getAdpenthu()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(adpenthu);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(adpenthu);
		}
		else
		{
			return adpenthu;
		}
	}

	public String getAdptime()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(adptime);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(adptime);
		}
		else
		{
			return adptime;
		}
	}

	public String getAdpans()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(adpans);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(adpans);
		}
		else
		{
			return adpans;
		}
	}

	public String getAdpthor()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(adpthor);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(adpthor);
		}
		else
		{
			return adpthor;
		}
	}

	public String getAdpover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(adpover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(adpover);
		}
		else
		{
			return adpover;
		}
	}

	public String getAovover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(aovover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(aovover);
		}
		else
		{
			return aovover;
		}
	}

	public String getPrb()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(prb);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(prb);
		}
		else
		{
			return prb;
		}
	}

	public String getNprb()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nprb);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nprb);
		}
		else
		{
			return nprb;
		}
	}

	public String getNnprb()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nnprb);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nnprb);
		}
		else
		{
			return nnprb;
		}
	}

	public String getCntft()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(cntft);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(cntft);
		}
		else
		{
			return cntft;
		}
	}

	public String getNcntft()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ncntft);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ncntft);
		}
		else
		{
			return ncntft;
		}
	}

	public String getCntexp()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(cntexp);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(cntexp);
		}
		else
		{
			return cntexp;
		}
	}

	public String getNcntexp()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ncntexp);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ncntexp);
		}
		else
		{
			return ncntexp;
		}
	}

	public String getSop()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sop);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sop);
		}
		else
		{
			return sop;
		}
	}

	public String getNsop()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsop);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsop);
		}
		else
		{
			return nsop;
		}
	}

	public String getComsop()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(comsop);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(comsop);
		}
		else
		{
			return comsop;
		}
	}

	public String getNcomsop()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ncomsop);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ncomsop);
		}
		else
		{
			return ncomsop;
		}
	}

	public String getProsop()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(prosop);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(prosop);
		}
		else
		{
			return prosop;
		}
	}

	public String getNprosop()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nprosop);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nprosop);
		}
		else
		{
			return nprosop;
		}
	}

	public String getRecodeend()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(recodeend);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(recodeend);
		}
		else
		{
			return recodeend;
		}
	}

	public String getIndex()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(index);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(index);
		}
		else
		{
			return index;
		}
	}

	public String getSi()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(si);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(si);
		}
		else
		{
			return si;
		}
	}

	public String getDf()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(df);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(df);
		}
		else
		{
			return df;
		}
	}

	public String getDeal()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(deal);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(deal);
		}
		else
		{
			return deal;
		}
	}

	public String getSp()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sp);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sp);
		}
		else
		{
			return sp;
		}
	}

	public String getDt()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dt);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dt);
		}
		else
		{
			return dt;
		}
	}

	public String getDp()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dp);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dp);
		}
		else
		{
			return dp;
		}
	}

	public String getNindex()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nindex);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nindex);
		}
		else
		{
			return nindex;
		}
	}

	public String getBiweekly()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(biweekly);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(biweekly);
		}
		else
		{
			return biweekly;
		}
	}

	public String getMonth()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(month);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(month);
		}
		else
		{
			return month;
		}
	}

	public String getQuarter()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(quarter);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(quarter);
		}
		else
		{
			return quarter;
		}
	}

	public String getYear()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(year);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(year);
		}
		else
		{
			return year;
		}
	}


	public void setSrno(String value)
	{
		srno = value;
	}

	public void setDbsrno(String value)
	{
		dbsrno = value;
	}

	public void setTrack(String value)
	{
		track = value;
	}

	public void setLstned(String value)
	{
		lstned = value;
	}

	public void setQcdone(String value)
	{
		qcdone = value;
	}

	public void setTapeadt(String value)
	{
		tapeadt = value;
	}

	public void setExtaudi(String value)
	{
		extaudi = value;
	}

	public void setPdatey(String value)
	{
		pdatey = value;
	}

	public void setPdatem(String value)
	{
		pdatem = value;
	}

	public void setWork(String value)
	{
		work = value;
	}

	public void setCmak1(String value)
	{
		cmak1 = value;
	}

	public void setPercon(String value)
	{
		percon = value;
	}

	public void setCondate(String value)
	{
		condate = value;
	}

	public void setDdatecon(String value)
	{
		ddatecon = value;
	}

	public void setConfdeal(String value)
	{
		confdeal = value;
	}

	public void setDealcon(String value)
	{
		dealcon = value;
	}

	public void setMainuser(String value)
	{
		mainuser = value;
	}

	public void setPdcmaker(String value)
	{
		pdcmaker = value;
	}

	public void setVstford(String value)
	{
		vstford = value;
	}

	public void setQrot(String value)
	{
		qrot = value;
	}

	public void setWaitdlr(String value)
	{
		waitdlr = value;
	}

	public void setEffund(String value)
	{
		effund = value;
	}

	public void setIctstdr(String value)
	{
		ictstdr = value;
	}

	public void setTdrdlr(String value)
	{
		tdrdlr = value;
	}

	public void setTstrout(String value)
	{
		tstrout = value;
	}

	public void setTstcar(String value)
	{
		tstcar = value;
	}

	public void setTdover(String value)
	{
		tdover = value;
	}

	public void setFtsafety(String value)
	{
		ftsafety = value;
	}

	public void setFtconv(String value)
	{
		ftconv = value;
	}

	public void setFtperf(String value)
	{
		ftperf = value;
	}

	public void setFtcost(String value)
	{
		ftcost = value;
	}

	public void setSpprovd(String value)
	{
		spprovd = value;
	}

	public void setSpcall(String value)
	{
		spcall = value;
	}

	public void setSigreet(String value)
	{
		sigreet = value;
	}

	public void setSiexpln(String value)
	{
		siexpln = value;
	}

	public void setSidemo(String value)
	{
		sidemo = value;
	}

	public void setSifllow(String value)
	{
		sifllow = value;
	}

	public void setSiiover(String value)
	{
		siiover = value;
	}

	public void setDfseat(String value)
	{
		dfseat = value;
	}

	public void setDfpark(String value)
	{
		dfpark = value;
	}

	public void setDflit(String value)
	{
		dflit = value;
	}

	public void setDfclean(String value)
	{
		dfclean = value;
	}

	public void setDftemp(String value)
	{
		dftemp = value;
	}

	public void setDfref(String value)
	{
		dfref = value;
	}

	public void setDfsmell(String value)
	{
		dfsmell = value;
	}

	public void setDfsignge(String value)
	{
		dfsignge = value;
	}

	public void setDfrtrm(String value)
	{
		dfrtrm = value;
	}

	public void setDfavshw(String value)
	{
		dfavshw = value;
	}

	public void setDlrlyot(String value)
	{
		dlrlyot = value;
	}

	public void setDlrcfof(String value)
	{
		dlrcfof = value;
	}

	public void setDlreslk(String value)
	{
		dlreslk = value;
	}

	public void setDlrsov(String value)
	{
		dlrsov = value;
	}

	public void setDlrloc(String value)
	{
		dlrloc = value;
	}

	public void setDlrover(String value)
	{
		dlrover = value;
	}

	public void setVehfncd(String value)
	{
		vehfncd = value;
	}

	public void setDlrfnc(String value)
	{
		dlrfnc = value;
	}

	public void setCondeal(String value)
	{
		condeal = value;
	}

	public void setDlease(String value)
	{
		dlease = value;
	}

	public void setDlpexp(String value)
	{
		dlpexp = value;
	}

	public void setDlqkcom(String value)
	{
		dlqkcom = value;
	}

	public void setDlprcex(String value)
	{
		dlprcex = value;
	}

	public void setDldover(String value)
	{
		dldover = value;
	}

	public void setIcgroom(String value)
	{
		icgroom = value;
	}

	public void setIcneatly(String value)
	{
		icneatly = value;
	}

	public void setIcspent(String value)
	{
		icspent = value;
	}

	public void setIcaskuse(String value)
	{
		icaskuse = value;
	}

	public void setIcinform(String value)
	{
		icinform = value;
	}

	public void setIcstatus(String value)
	{
		icstatus = value;
	}

	public void setSpkldge(String value)
	{
		spkldge = value;
	}

	public void setSpneed(String value)
	{
		spneed = value;
	}

	public void setSpcrtsy(String value)
	{
		spcrtsy = value;
	}

	public void setSpprssr(String value)
	{
		spprssr = value;
	}

	public void setSpcommt(String value)
	{
		spcommt = value;
	}

	public void setSpover(String value)
	{
		spover = value;
	}

	public void setIcsame(String value)
	{
		icsame = value;
	}

	public void setIcday(String value)
	{
		icday = value;
	}

	public void setDtvsexp(String value)
	{
		dtvsexp = value;
	}

	public void setDldelct(String value)
	{
		dldelct = value;
	}

	public void setDtlen(String value)
	{
		dtlen = value;
	}

	public void setDtstatus(String value)
	{
		dtstatus = value;
	}

	public void setDttime(String value)
	{
		dttime = value;
	}

	public void setDtover(String value)
	{
		dtover = value;
	}

	public void setAfvehft(String value)
	{
		afvehft = value;
	}

	public void setAfope(String value)
	{
		afope = value;
	}

	public void setAfwarr(String value)
	{
		afwarr = value;
	}

	public void setAfmaint(String value)
	{
		afmaint = value;
	}

	public void setDlhelp(String value)
	{
		dlhelp = value;
	}

	public void setDlintro(String value)
	{
		dlintro = value;
	}

	public void setDlenftme(String value)
	{
		dlenftme = value;
	}

	public void setDlthank(String value)
	{
		dlthank = value;
	}

	public void setDlramtf(String value)
	{
		dlramtf = value;
	}

	public void setDlspcere(String value)
	{
		dlspcere = value;
	}

	public void setDlvisit(String value)
	{
		dlvisit = value;
	}

	public void setDlext(String value)
	{
		dlext = value;
	}

	public void setDlfeat(String value)
	{
		dlfeat = value;
	}

	public void setDlans(String value)
	{
		dlans = value;
	}

	public void setDlfeebk(String value)
	{
		dlfeebk = value;
	}

	public void setDlconta(String value)
	{
		dlconta = value;
	}

	public void setDlappt(String value)
	{
		dlappt = value;
	}

	public void setDldelpt(String value)
	{
		dldelpt = value;
	}

	public void setDelhr(String value)
	{
		delhr = value;
	}

	public void setDelmin(String value)
	{
		delmin = value;
	}

	public void setDelcar(String value)
	{
		delcar = value;
	}

	public void setDpconvh(String value)
	{
		dpconvh = value;
	}

	public void setDpenthu(String value)
	{
		dpenthu = value;
	}

	public void setDptime(String value)
	{
		dptime = value;
	}

	public void setDpans(String value)
	{
		dpans = value;
	}

	public void setDpthor(String value)
	{
		dpthor = value;
	}

	public void setDpover(String value)
	{
		dpover = value;
	}

	public void setOvover(String value)
	{
		ovover = value;
	}

	public void setPrbselct(String value)
	{
		prbselct = value;
	}

	public void setPrbstaf(String value)
	{
		prbstaf = value;
	}

	public void setPrbchrge(String value)
	{
		prbchrge = value;
	}

	public void setPrbanswr(String value)
	{
		prbanswr = value;
	}

	public void setPrbshff(String value)
	{
		prbshff = value;
	}

	public void setPrbphony(String value)
	{
		prbphony = value;
	}

	public void setRecodlr(String value)
	{
		recodlr = value;
	}

	public void setPsamedlr(String value)
	{
		psamedlr = value;
	}

	public void setRecomake(String value)
	{
		recomake = value;
	}

	public void setReprmake(String value)
	{
		reprmake = value;
	}

	public void setEcdlr(String value)
	{
		ecdlr = value;
	}

	public void setEcveh(String value)
	{
		ecveh = value;
	}

	public void setFirstveh(String value)
	{
		firstveh = value;
	}

	public void setNewveh(String value)
	{
		newveh = value;
	}

	public void setJdpres(String value)
	{
		jdpres = value;
	}

	public void setJdpinf(String value)
	{
		jdpinf = value;
	}

	public void setFc(String value)
	{
		fc = value;
	}

	public void setReveal(String value)
	{
		reveal = value;
	}

	public void setProvfb(String value)
	{
		provfb = value;
	}

	public void setGender(String value)
	{
		gender = value;
	}

	public void setDtmn(String value)
	{
		dtmn = value;
	}

	public void setIntid(String value)
	{
		intid = value;
	}

	public void setIntgen(String value)
	{
		intgen = value;
	}

	public void setLmn(String value)
	{
		lmn = value;
	}

	public void setIntme(String value)
	{
		intme = value;
	}

	public void setTxtoestart(String value)
	{
		txtoestart = value;
	}

	public void setTxtintid(String value)
	{
		txtintid = value;
	}

	public void setTxtoeend(String value)
	{
		txtoeend = value;
	}

	public void setScity(String value)
	{
		scity = value;
	}

	public void setIntday(String value)
	{
		intday = value;
	}

	public void setIntmth(String value)
	{
		intmth = value;
	}

	public void setIntdate(String value)
	{
		intdate = value;
	}

	public void setDbstart(String value)
	{
		dbstart = value;
	}

	public void setDbcustomername(String value)
	{
		dbcustomername = value;
	}

	public void setDbcustomercity(String value)
	{
		dbcustomercity = value;
	}

	public void setDbcustomerstate(String value)
	{
		dbcustomerstate = value;
	}

	public void setDbmobile(String value)
	{
		dbmobile = value;
	}

	public void setDbtel1(String value)
	{
		dbtel1 = value;
	}

	public void setDbtel2(String value)
	{
		dbtel2 = value;
	}

	public void setDbmodel(String value)
	{
		dbmodel = value;
	}

	public void setDbmodelvarient(String value)
	{
		dbmodelvarient = value;
	}

	public void setDbfueltype(String value)
	{
		dbfueltype = value;
	}

	public void setDbvin(String value)
	{
		dbvin = value;
	}

	public void setDbdealercode(String value)
	{
		dbdealercode = value;
	}

	public void setDbdealername(String value)
	{
		dbdealername = value;
	}

	public void setDbdealercity(String value)
	{
		dbdealercity = value;
	}

	public void setDbdealerstate(String value)
	{
		dbdealerstate = value;
	}

	public void setDbdeliverydate(String value)
	{
		dbdeliverydate = value;
	}

	public void setDbsalesconsultantcode(String value)
	{
		dbsalesconsultantcode = value;
	}

	public void setDbsalesconsultantname(String value)
	{
		dbsalesconsultantname = value;
	}

	public void setDbpandacode(String value)
	{
		dbpandacode = value;
	}

	public void setDbdealer2(String value)
	{
		dbdealer2 = value;
	}

	public void setDbdealer2labels(String value)
	{
		dbdealer2labels = value;
	}

	public void setDbquarternum(String value)
	{
		dbquarternum = value;
	}

	public void setDbbatchnum(String value)
	{
		dbbatchnum = value;
	}

	public void setDbend(String value)
	{
		dbend = value;
	}

	public void setFdstart(String value)
	{
		fdstart = value;
	}

	public void setDealer(String value)
	{
		dealer = value;
	}

	public void setDealer2(String value)
	{
		dealer2 = value;
	}

	public void setCity2(String value)
	{
		city2 = value;
	}

	public void setRegiona(String value)
	{
		regiona = value;
	}

	public void setState2(String value)
	{
		state2 = value;
	}

	public void setModel(String value)
	{
		model = value;
	}

	public void setModel2(String value)
	{
		model2 = value;
	}

	public void setCity3(String value)
	{
		city3 = value;
	}

	public void setFdend(String value)
	{
		fdend = value;
	}

	public void setIndexoutlierfilter(String value)
	{
		indexoutlierfilter = value;
	}

	public void setIrecodestart(String value)
	{
		irecodestart = value;
	}

	public void setZdelhr(String value)
	{
		zdelhr = value;
	}

	public void setZdelmin(String value)
	{
		zdelmin = value;
	}

	public void setCdelmin(String value)
	{
		cdelmin = value;
	}

	public void setNcicday(String value)
	{
		ncicday = value;
	}

	public void setNcdelmin(String value)
	{
		ncdelmin = value;
	}

	public void setIrecodeend(String value)
	{
		irecodeend = value;
	}

	public void setRecodestart(String value)
	{
		recodestart = value;
	}

	public void setNtdover(String value)
	{
		ntdover = value;
	}

	public void setNsigreet(String value)
	{
		nsigreet = value;
	}

	public void setNsiexpln(String value)
	{
		nsiexpln = value;
	}

	public void setNsidemo(String value)
	{
		nsidemo = value;
	}

	public void setNsifllow(String value)
	{
		nsifllow = value;
	}

	public void setNsiiover(String value)
	{
		nsiiover = value;
	}

	public void setNdlrlyot(String value)
	{
		ndlrlyot = value;
	}

	public void setNdlrcfof(String value)
	{
		ndlrcfof = value;
	}

	public void setNdlreslk(String value)
	{
		ndlreslk = value;
	}

	public void setNdlrsov(String value)
	{
		ndlrsov = value;
	}

	public void setNdlrloc(String value)
	{
		ndlrloc = value;
	}

	public void setNdlrover(String value)
	{
		ndlrover = value;
	}

	public void setNdlease(String value)
	{
		ndlease = value;
	}

	public void setNdlpexp(String value)
	{
		ndlpexp = value;
	}

	public void setNdlqkcom(String value)
	{
		ndlqkcom = value;
	}

	public void setNdlprcex(String value)
	{
		ndlprcex = value;
	}

	public void setNdldover(String value)
	{
		ndldover = value;
	}

	public void setNspkldge(String value)
	{
		nspkldge = value;
	}

	public void setNspneed(String value)
	{
		nspneed = value;
	}

	public void setNspcrtsy(String value)
	{
		nspcrtsy = value;
	}

	public void setNspprssr(String value)
	{
		nspprssr = value;
	}

	public void setNspcommt(String value)
	{
		nspcommt = value;
	}

	public void setNspover(String value)
	{
		nspover = value;
	}

	public void setNdtlen(String value)
	{
		ndtlen = value;
	}

	public void setNdtstatus(String value)
	{
		ndtstatus = value;
	}

	public void setNdttime(String value)
	{
		ndttime = value;
	}

	public void setNdtover(String value)
	{
		ndtover = value;
	}

	public void setNdpconvh(String value)
	{
		ndpconvh = value;
	}

	public void setNdpenthu(String value)
	{
		ndpenthu = value;
	}

	public void setNdptime(String value)
	{
		ndptime = value;
	}

	public void setNdpans(String value)
	{
		ndpans = value;
	}

	public void setNdpthor(String value)
	{
		ndpthor = value;
	}

	public void setNdpover(String value)
	{
		ndpover = value;
	}

	public void setNovover(String value)
	{
		novover = value;
	}

	public void setTtdover(String value)
	{
		ttdover = value;
	}

	public void setTsigreet(String value)
	{
		tsigreet = value;
	}

	public void setTsiexpln(String value)
	{
		tsiexpln = value;
	}

	public void setTsidemo(String value)
	{
		tsidemo = value;
	}

	public void setTsifllow(String value)
	{
		tsifllow = value;
	}

	public void setTsiiover(String value)
	{
		tsiiover = value;
	}

	public void setTdlrlyot(String value)
	{
		tdlrlyot = value;
	}

	public void setTdlrcfof(String value)
	{
		tdlrcfof = value;
	}

	public void setTdlreslk(String value)
	{
		tdlreslk = value;
	}

	public void setTdlrsov(String value)
	{
		tdlrsov = value;
	}

	public void setTdlrloc(String value)
	{
		tdlrloc = value;
	}

	public void setTdlrover(String value)
	{
		tdlrover = value;
	}

	public void setTdlease(String value)
	{
		tdlease = value;
	}

	public void setTdlpexp(String value)
	{
		tdlpexp = value;
	}

	public void setTdlqkcom(String value)
	{
		tdlqkcom = value;
	}

	public void setTdlprcex(String value)
	{
		tdlprcex = value;
	}

	public void setTdldover(String value)
	{
		tdldover = value;
	}

	public void setTspkldge(String value)
	{
		tspkldge = value;
	}

	public void setTspneed(String value)
	{
		tspneed = value;
	}

	public void setTspcrtsy(String value)
	{
		tspcrtsy = value;
	}

	public void setTspprssr(String value)
	{
		tspprssr = value;
	}

	public void setTspcommt(String value)
	{
		tspcommt = value;
	}

	public void setTspover(String value)
	{
		tspover = value;
	}

	public void setTdtlen(String value)
	{
		tdtlen = value;
	}

	public void setTdtstatus(String value)
	{
		tdtstatus = value;
	}

	public void setTdttime(String value)
	{
		tdttime = value;
	}

	public void setTdtover(String value)
	{
		tdtover = value;
	}

	public void setTdpconvh(String value)
	{
		tdpconvh = value;
	}

	public void setTdpenthu(String value)
	{
		tdpenthu = value;
	}

	public void setTdptime(String value)
	{
		tdptime = value;
	}

	public void setTdpans(String value)
	{
		tdpans = value;
	}

	public void setTdpthor(String value)
	{
		tdpthor = value;
	}

	public void setTdpover(String value)
	{
		tdpover = value;
	}

	public void setTovover(String value)
	{
		tovover = value;
	}

	public void setAtdover(String value)
	{
		atdover = value;
	}

	public void setAsigreet(String value)
	{
		asigreet = value;
	}

	public void setAsiexpln(String value)
	{
		asiexpln = value;
	}

	public void setAsidemo(String value)
	{
		asidemo = value;
	}

	public void setAsifllow(String value)
	{
		asifllow = value;
	}

	public void setAsiiover(String value)
	{
		asiiover = value;
	}

	public void setAdlrlyot(String value)
	{
		adlrlyot = value;
	}

	public void setAdlrcfof(String value)
	{
		adlrcfof = value;
	}

	public void setAdlreslk(String value)
	{
		adlreslk = value;
	}

	public void setAdlrsov(String value)
	{
		adlrsov = value;
	}

	public void setAdlrloc(String value)
	{
		adlrloc = value;
	}

	public void setAdlrover(String value)
	{
		adlrover = value;
	}

	public void setAdlease(String value)
	{
		adlease = value;
	}

	public void setAdlpexp(String value)
	{
		adlpexp = value;
	}

	public void setAdlqkcom(String value)
	{
		adlqkcom = value;
	}

	public void setAdlprcex(String value)
	{
		adlprcex = value;
	}

	public void setAdldover(String value)
	{
		adldover = value;
	}

	public void setAspkldge(String value)
	{
		aspkldge = value;
	}

	public void setAspneed(String value)
	{
		aspneed = value;
	}

	public void setAspcrtsy(String value)
	{
		aspcrtsy = value;
	}

	public void setAspprssr(String value)
	{
		aspprssr = value;
	}

	public void setAspcommt(String value)
	{
		aspcommt = value;
	}

	public void setAspover(String value)
	{
		aspover = value;
	}

	public void setAdtlen(String value)
	{
		adtlen = value;
	}

	public void setAdtstatus(String value)
	{
		adtstatus = value;
	}

	public void setAdttime(String value)
	{
		adttime = value;
	}

	public void setAdtover(String value)
	{
		adtover = value;
	}

	public void setAdpconvh(String value)
	{
		adpconvh = value;
	}

	public void setAdpenthu(String value)
	{
		adpenthu = value;
	}

	public void setAdptime(String value)
	{
		adptime = value;
	}

	public void setAdpans(String value)
	{
		adpans = value;
	}

	public void setAdpthor(String value)
	{
		adpthor = value;
	}

	public void setAdpover(String value)
	{
		adpover = value;
	}

	public void setAovover(String value)
	{
		aovover = value;
	}

	public void setPrb(String value)
	{
		prb = value;
	}

	public void setNprb(String value)
	{
		nprb = value;
	}

	public void setNnprb(String value)
	{
		nnprb = value;
	}

	public void setCntft(String value)
	{
		cntft = value;
	}

	public void setNcntft(String value)
	{
		ncntft = value;
	}

	public void setCntexp(String value)
	{
		cntexp = value;
	}

	public void setNcntexp(String value)
	{
		ncntexp = value;
	}

	public void setSop(String value)
	{
		sop = value;
	}

	public void setNsop(String value)
	{
		nsop = value;
	}

	public void setComsop(String value)
	{
		comsop = value;
	}

	public void setNcomsop(String value)
	{
		ncomsop = value;
	}

	public void setProsop(String value)
	{
		prosop = value;
	}

	public void setNprosop(String value)
	{
		nprosop = value;
	}

	public void setRecodeend(String value)
	{
		recodeend = value;
	}

	public void setIndex(String value)
	{
		index = value;
	}

	public void setSi(String value)
	{
		si = value;
	}

	public void setDf(String value)
	{
		df = value;
	}

	public void setDeal(String value)
	{
		deal = value;
	}

	public void setSp(String value)
	{
		sp = value;
	}

	public void setDt(String value)
	{
		dt = value;
	}

	public void setDp(String value)
	{
		dp = value;
	}

	public void setNindex(String value)
	{
		nindex = value;
	}

	public void setBiweekly(String value)
	{
		biweekly = value;
	}

	public void setMonth(String value)
	{
		month = value;
	}

	public void setQuarter(String value)
	{
		quarter = value;
	}

	public void setYear(String value)
	{
		year = value;
	}


	public String toString()
	{
		return "\nsrno:" + srno +
				"\ndbsrno:" + dbsrno +
				"\ntrack:" + track +
				"\nlstned:" + lstned +
				"\nqcdone:" + qcdone +
				"\ntapeadt:" + tapeadt +
				"\nextaudi:" + extaudi +
				"\npdatey:" + pdatey +
				"\npdatem:" + pdatem +
				"\nwork:" + work +
				"\ncmak1:" + cmak1 +
				"\npercon:" + percon +
				"\ncondate:" + condate +
				"\nddatecon:" + ddatecon +
				"\nconfdeal:" + confdeal +
				"\ndealcon:" + dealcon +
				"\nmainuser:" + mainuser +
				"\npdcmaker:" + pdcmaker +
				"\nvstford:" + vstford +
				"\nqrot:" + qrot +
				"\nwaitdlr:" + waitdlr +
				"\neffund:" + effund +
				"\nictstdr:" + ictstdr +
				"\ntdrdlr:" + tdrdlr +
				"\ntstrout:" + tstrout +
				"\ntstcar:" + tstcar +
				"\ntdover:" + tdover +
				"\nftsafety:" + ftsafety +
				"\nftconv:" + ftconv +
				"\nftperf:" + ftperf +
				"\nftcost:" + ftcost +
				"\nspprovd:" + spprovd +
				"\nspcall:" + spcall +
				"\nsigreet:" + sigreet +
				"\nsiexpln:" + siexpln +
				"\nsidemo:" + sidemo +
				"\nsifllow:" + sifllow +
				"\nsiiover:" + siiover +
				"\ndfseat:" + dfseat +
				"\ndfpark:" + dfpark +
				"\ndflit:" + dflit +
				"\ndfclean:" + dfclean +
				"\ndftemp:" + dftemp +
				"\ndfref:" + dfref +
				"\ndfsmell:" + dfsmell +
				"\ndfsignge:" + dfsignge +
				"\ndfrtrm:" + dfrtrm +
				"\ndfavshw:" + dfavshw +
				"\ndlrlyot:" + dlrlyot +
				"\ndlrcfof:" + dlrcfof +
				"\ndlreslk:" + dlreslk +
				"\ndlrsov:" + dlrsov +
				"\ndlrloc:" + dlrloc +
				"\ndlrover:" + dlrover +
				"\nvehfncd:" + vehfncd +
				"\ndlrfnc:" + dlrfnc +
				"\ncondeal:" + condeal +
				"\ndlease:" + dlease +
				"\ndlpexp:" + dlpexp +
				"\ndlqkcom:" + dlqkcom +
				"\ndlprcex:" + dlprcex +
				"\ndldover:" + dldover +
				"\nicgroom:" + icgroom +
				"\nicneatly:" + icneatly +
				"\nicspent:" + icspent +
				"\nicaskuse:" + icaskuse +
				"\nicinform:" + icinform +
				"\nicstatus:" + icstatus +
				"\nspkldge:" + spkldge +
				"\nspneed:" + spneed +
				"\nspcrtsy:" + spcrtsy +
				"\nspprssr:" + spprssr +
				"\nspcommt:" + spcommt +
				"\nspover:" + spover +
				"\nicsame:" + icsame +
				"\nicday:" + icday +
				"\ndtvsexp:" + dtvsexp +
				"\ndldelct:" + dldelct +
				"\ndtlen:" + dtlen +
				"\ndtstatus:" + dtstatus +
				"\ndttime:" + dttime +
				"\ndtover:" + dtover +
				"\nafvehft:" + afvehft +
				"\nafope:" + afope +
				"\nafwarr:" + afwarr +
				"\nafmaint:" + afmaint +
				"\ndlhelp:" + dlhelp +
				"\ndlintro:" + dlintro +
				"\ndlenftme:" + dlenftme +
				"\ndlthank:" + dlthank +
				"\ndlramtf:" + dlramtf +
				"\ndlspcere:" + dlspcere +
				"\ndlvisit:" + dlvisit +
				"\ndlext:" + dlext +
				"\ndlfeat:" + dlfeat +
				"\ndlans:" + dlans +
				"\ndlfeebk:" + dlfeebk +
				"\ndlconta:" + dlconta +
				"\ndlappt:" + dlappt +
				"\ndldelpt:" + dldelpt +
				"\ndelhr:" + delhr +
				"\ndelmin:" + delmin +
				"\ndelcar:" + delcar +
				"\ndpconvh:" + dpconvh +
				"\ndpenthu:" + dpenthu +
				"\ndptime:" + dptime +
				"\ndpans:" + dpans +
				"\ndpthor:" + dpthor +
				"\ndpover:" + dpover +
				"\novover:" + ovover +
				"\nprbselct:" + prbselct +
				"\nprbstaf:" + prbstaf +
				"\nprbchrge:" + prbchrge +
				"\nprbanswr:" + prbanswr +
				"\nprbshff:" + prbshff +
				"\nprbphony:" + prbphony +
				"\nrecodlr:" + recodlr +
				"\npsamedlr:" + psamedlr +
				"\nrecomake:" + recomake +
				"\nreprmake:" + reprmake +
				"\necdlr:" + ecdlr +
				"\necveh:" + ecveh +
				"\nfirstveh:" + firstveh +
				"\nnewveh:" + newveh +
				"\njdpres:" + jdpres +
				"\njdpinf:" + jdpinf +
				"\nfc:" + fc +
				"\nreveal:" + reveal +
				"\nprovfb:" + provfb +
				"\ngender:" + gender +
				"\ndtmn:" + dtmn +
				"\nintid:" + intid +
				"\nintgen:" + intgen +
				"\nlmn:" + lmn +
				"\nintme:" + intme +
				"\ntxtoestart:" + txtoestart +
				"\ntxtintid:" + txtintid +
				"\ntxtoeend:" + txtoeend +
				"\nscity:" + scity +
				"\nintday:" + intday +
				"\nintmth:" + intmth +
				"\nintdate:" + intdate +
				"\ndbstart:" + dbstart +
				"\ndbcustomername:" + dbcustomername +
				"\ndbcustomercity:" + dbcustomercity +
				"\ndbcustomerstate:" + dbcustomerstate +
				"\ndbmobile:" + dbmobile +
				"\ndbtel1:" + dbtel1 +
				"\ndbtel2:" + dbtel2 +
				"\ndbmodel:" + dbmodel +
				"\ndbmodelvarient:" + dbmodelvarient +
				"\ndbfueltype:" + dbfueltype +
				"\ndbvin:" + dbvin +
				"\ndbdealercode:" + dbdealercode +
				"\ndbdealername:" + dbdealername +
				"\ndbdealercity:" + dbdealercity +
				"\ndbdealerstate:" + dbdealerstate +
				"\ndbdeliverydate:" + dbdeliverydate +
				"\ndbsalesconsultantcode:" + dbsalesconsultantcode +
				"\ndbsalesconsultantname:" + dbsalesconsultantname +
				"\ndbpandacode:" + dbpandacode +
				"\ndbdealer2:" + dbdealer2 +
				"\ndbdealer2labels:" + dbdealer2labels +
				"\ndbquarternum:" + dbquarternum +
				"\ndbbatchnum:" + dbbatchnum +
				"\ndbend:" + dbend +
				"\nfdstart:" + fdstart +
				"\ndealer:" + dealer +
				"\ndealer2:" + dealer2 +
				"\ncity2:" + city2 +
				"\nregiona:" + regiona +
				"\nstate2:" + state2 +
				"\nmodel:" + model +
				"\nmodel2:" + model2 +
				"\ncity3:" + city3 +
				"\nfdend:" + fdend +
				"\nindexoutlierfilter:" + indexoutlierfilter +
				"\nirecodestart:" + irecodestart +
				"\nzdelhr:" + zdelhr +
				"\nzdelmin:" + zdelmin +
				"\ncdelmin:" + cdelmin +
				"\nncicday:" + ncicday +
				"\nncdelmin:" + ncdelmin +
				"\nirecodeend:" + irecodeend +
				"\nrecodestart:" + recodestart +
				"\nntdover:" + ntdover +
				"\nnsigreet:" + nsigreet +
				"\nnsiexpln:" + nsiexpln +
				"\nnsidemo:" + nsidemo +
				"\nnsifllow:" + nsifllow +
				"\nnsiiover:" + nsiiover +
				"\nndlrlyot:" + ndlrlyot +
				"\nndlrcfof:" + ndlrcfof +
				"\nndlreslk:" + ndlreslk +
				"\nndlrsov:" + ndlrsov +
				"\nndlrloc:" + ndlrloc +
				"\nndlrover:" + ndlrover +
				"\nndlease:" + ndlease +
				"\nndlpexp:" + ndlpexp +
				"\nndlqkcom:" + ndlqkcom +
				"\nndlprcex:" + ndlprcex +
				"\nndldover:" + ndldover +
				"\nnspkldge:" + nspkldge +
				"\nnspneed:" + nspneed +
				"\nnspcrtsy:" + nspcrtsy +
				"\nnspprssr:" + nspprssr +
				"\nnspcommt:" + nspcommt +
				"\nnspover:" + nspover +
				"\nndtlen:" + ndtlen +
				"\nndtstatus:" + ndtstatus +
				"\nndttime:" + ndttime +
				"\nndtover:" + ndtover +
				"\nndpconvh:" + ndpconvh +
				"\nndpenthu:" + ndpenthu +
				"\nndptime:" + ndptime +
				"\nndpans:" + ndpans +
				"\nndpthor:" + ndpthor +
				"\nndpover:" + ndpover +
				"\nnovover:" + novover +
				"\nttdover:" + ttdover +
				"\ntsigreet:" + tsigreet +
				"\ntsiexpln:" + tsiexpln +
				"\ntsidemo:" + tsidemo +
				"\ntsifllow:" + tsifllow +
				"\ntsiiover:" + tsiiover +
				"\ntdlrlyot:" + tdlrlyot +
				"\ntdlrcfof:" + tdlrcfof +
				"\ntdlreslk:" + tdlreslk +
				"\ntdlrsov:" + tdlrsov +
				"\ntdlrloc:" + tdlrloc +
				"\ntdlrover:" + tdlrover +
				"\ntdlease:" + tdlease +
				"\ntdlpexp:" + tdlpexp +
				"\ntdlqkcom:" + tdlqkcom +
				"\ntdlprcex:" + tdlprcex +
				"\ntdldover:" + tdldover +
				"\ntspkldge:" + tspkldge +
				"\ntspneed:" + tspneed +
				"\ntspcrtsy:" + tspcrtsy +
				"\ntspprssr:" + tspprssr +
				"\ntspcommt:" + tspcommt +
				"\ntspover:" + tspover +
				"\ntdtlen:" + tdtlen +
				"\ntdtstatus:" + tdtstatus +
				"\ntdttime:" + tdttime +
				"\ntdtover:" + tdtover +
				"\ntdpconvh:" + tdpconvh +
				"\ntdpenthu:" + tdpenthu +
				"\ntdptime:" + tdptime +
				"\ntdpans:" + tdpans +
				"\ntdpthor:" + tdpthor +
				"\ntdpover:" + tdpover +
				"\ntovover:" + tovover +
				"\natdover:" + atdover +
				"\nasigreet:" + asigreet +
				"\nasiexpln:" + asiexpln +
				"\nasidemo:" + asidemo +
				"\nasifllow:" + asifllow +
				"\nasiiover:" + asiiover +
				"\nadlrlyot:" + adlrlyot +
				"\nadlrcfof:" + adlrcfof +
				"\nadlreslk:" + adlreslk +
				"\nadlrsov:" + adlrsov +
				"\nadlrloc:" + adlrloc +
				"\nadlrover:" + adlrover +
				"\nadlease:" + adlease +
				"\nadlpexp:" + adlpexp +
				"\nadlqkcom:" + adlqkcom +
				"\nadlprcex:" + adlprcex +
				"\nadldover:" + adldover +
				"\naspkldge:" + aspkldge +
				"\naspneed:" + aspneed +
				"\naspcrtsy:" + aspcrtsy +
				"\naspprssr:" + aspprssr +
				"\naspcommt:" + aspcommt +
				"\naspover:" + aspover +
				"\nadtlen:" + adtlen +
				"\nadtstatus:" + adtstatus +
				"\nadttime:" + adttime +
				"\nadtover:" + adtover +
				"\nadpconvh:" + adpconvh +
				"\nadpenthu:" + adpenthu +
				"\nadptime:" + adptime +
				"\nadpans:" + adpans +
				"\nadpthor:" + adpthor +
				"\nadpover:" + adpover +
				"\naovover:" + aovover +
				"\nprb:" + prb +
				"\nnprb:" + nprb +
				"\nnnprb:" + nnprb +
				"\ncntft:" + cntft +
				"\nncntft:" + ncntft +
				"\ncntexp:" + cntexp +
				"\nncntexp:" + ncntexp +
				"\nsop:" + sop +
				"\nnsop:" + nsop +
				"\ncomsop:" + comsop +
				"\nncomsop:" + ncomsop +
				"\nprosop:" + prosop +
				"\nnprosop:" + nprosop +
				"\nrecodeend:" + recodeend +
				"\nindex:" + index +
				"\nsi:" + si +
				"\ndf:" + df +
				"\ndeal:" + deal +
				"\nsp:" + sp +
				"\ndt:" + dt +
				"\ndp:" + dp +
				"\nnindex:" + nindex +
				"\nbiweekly:" + biweekly +
				"\nmonth:" + month +
				"\nquarter:" + quarter +
				"\nyear:" + year +
				"\n";
	}

	public void loadContent(LISsitranslatedRecord inputRecord)
	{
		setSrno(inputRecord.getSrno());
		setDbsrno(inputRecord.getDbsrno());
		setTrack(inputRecord.getTrack());
		setLstned(inputRecord.getLstned());
		setQcdone(inputRecord.getQcdone());
		setTapeadt(inputRecord.getTapeadt());
		setExtaudi(inputRecord.getExtaudi());
		setPdatey(inputRecord.getPdatey());
		setPdatem(inputRecord.getPdatem());
		setWork(inputRecord.getWork());
		setCmak1(inputRecord.getCmak1());
		setPercon(inputRecord.getPercon());
		setCondate(inputRecord.getCondate());
		setDdatecon(inputRecord.getDdatecon());
		setConfdeal(inputRecord.getConfdeal());
		setDealcon(inputRecord.getDealcon());
		setMainuser(inputRecord.getMainuser());
		setPdcmaker(inputRecord.getPdcmaker());
		setVstford(inputRecord.getVstford());
		setQrot(inputRecord.getQrot());
		setWaitdlr(inputRecord.getWaitdlr());
		setEffund(inputRecord.getEffund());
		setIctstdr(inputRecord.getIctstdr());
		setTdrdlr(inputRecord.getTdrdlr());
		setTstrout(inputRecord.getTstrout());
		setTstcar(inputRecord.getTstcar());
		setTdover(inputRecord.getTdover());
		setFtsafety(inputRecord.getFtsafety());
		setFtconv(inputRecord.getFtconv());
		setFtperf(inputRecord.getFtperf());
		setFtcost(inputRecord.getFtcost());
		setSpprovd(inputRecord.getSpprovd());
		setSpcall(inputRecord.getSpcall());
		setSigreet(inputRecord.getSigreet());
		setSiexpln(inputRecord.getSiexpln());
		setSidemo(inputRecord.getSidemo());
		setSifllow(inputRecord.getSifllow());
		setSiiover(inputRecord.getSiiover());
		setDfseat(inputRecord.getDfseat());
		setDfpark(inputRecord.getDfpark());
		setDflit(inputRecord.getDflit());
		setDfclean(inputRecord.getDfclean());
		setDftemp(inputRecord.getDftemp());
		setDfref(inputRecord.getDfref());
		setDfsmell(inputRecord.getDfsmell());
		setDfsignge(inputRecord.getDfsignge());
		setDfrtrm(inputRecord.getDfrtrm());
		setDfavshw(inputRecord.getDfavshw());
		setDlrlyot(inputRecord.getDlrlyot());
		setDlrcfof(inputRecord.getDlrcfof());
		setDlreslk(inputRecord.getDlreslk());
		setDlrsov(inputRecord.getDlrsov());
		setDlrloc(inputRecord.getDlrloc());
		setDlrover(inputRecord.getDlrover());
		setVehfncd(inputRecord.getVehfncd());
		setDlrfnc(inputRecord.getDlrfnc());
		setCondeal(inputRecord.getCondeal());
		setDlease(inputRecord.getDlease());
		setDlpexp(inputRecord.getDlpexp());
		setDlqkcom(inputRecord.getDlqkcom());
		setDlprcex(inputRecord.getDlprcex());
		setDldover(inputRecord.getDldover());
		setIcgroom(inputRecord.getIcgroom());
		setIcneatly(inputRecord.getIcneatly());
		setIcspent(inputRecord.getIcspent());
		setIcaskuse(inputRecord.getIcaskuse());
		setIcinform(inputRecord.getIcinform());
		setIcstatus(inputRecord.getIcstatus());
		setSpkldge(inputRecord.getSpkldge());
		setSpneed(inputRecord.getSpneed());
		setSpcrtsy(inputRecord.getSpcrtsy());
		setSpprssr(inputRecord.getSpprssr());
		setSpcommt(inputRecord.getSpcommt());
		setSpover(inputRecord.getSpover());
		setIcsame(inputRecord.getIcsame());
		setIcday(inputRecord.getIcday());
		setDtvsexp(inputRecord.getDtvsexp());
		setDldelct(inputRecord.getDldelct());
		setDtlen(inputRecord.getDtlen());
		setDtstatus(inputRecord.getDtstatus());
		setDttime(inputRecord.getDttime());
		setDtover(inputRecord.getDtover());
		setAfvehft(inputRecord.getAfvehft());
		setAfope(inputRecord.getAfope());
		setAfwarr(inputRecord.getAfwarr());
		setAfmaint(inputRecord.getAfmaint());
		setDlhelp(inputRecord.getDlhelp());
		setDlintro(inputRecord.getDlintro());
		setDlenftme(inputRecord.getDlenftme());
		setDlthank(inputRecord.getDlthank());
		setDlramtf(inputRecord.getDlramtf());
		setDlspcere(inputRecord.getDlspcere());
		setDlvisit(inputRecord.getDlvisit());
		setDlext(inputRecord.getDlext());
		setDlfeat(inputRecord.getDlfeat());
		setDlans(inputRecord.getDlans());
		setDlfeebk(inputRecord.getDlfeebk());
		setDlconta(inputRecord.getDlconta());
		setDlappt(inputRecord.getDlappt());
		setDldelpt(inputRecord.getDldelpt());
		setDelhr(inputRecord.getDelhr());
		setDelmin(inputRecord.getDelmin());
		setDelcar(inputRecord.getDelcar());
		setDpconvh(inputRecord.getDpconvh());
		setDpenthu(inputRecord.getDpenthu());
		setDptime(inputRecord.getDptime());
		setDpans(inputRecord.getDpans());
		setDpthor(inputRecord.getDpthor());
		setDpover(inputRecord.getDpover());
		setOvover(inputRecord.getOvover());
		setPrbselct(inputRecord.getPrbselct());
		setPrbstaf(inputRecord.getPrbstaf());
		setPrbchrge(inputRecord.getPrbchrge());
		setPrbanswr(inputRecord.getPrbanswr());
		setPrbshff(inputRecord.getPrbshff());
		setPrbphony(inputRecord.getPrbphony());
		setRecodlr(inputRecord.getRecodlr());
		setPsamedlr(inputRecord.getPsamedlr());
		setRecomake(inputRecord.getRecomake());
		setReprmake(inputRecord.getReprmake());
		setEcdlr(inputRecord.getEcdlr());
		setEcveh(inputRecord.getEcveh());
		setFirstveh(inputRecord.getFirstveh());
		setNewveh(inputRecord.getNewveh());
		setJdpres(inputRecord.getJdpres());
		setJdpinf(inputRecord.getJdpinf());
		setFc(inputRecord.getFc());
		setReveal(inputRecord.getReveal());
		setProvfb(inputRecord.getProvfb());
		setGender(inputRecord.getGender());
		setDtmn(inputRecord.getDtmn());
		setIntid(inputRecord.getIntid());
		setIntgen(inputRecord.getIntgen());
		setLmn(inputRecord.getLmn());
		setIntme(inputRecord.getIntme());
		setTxtoestart(inputRecord.getTxtoestart());
		setTxtintid(inputRecord.getTxtintid());
		setTxtoeend(inputRecord.getTxtoeend());
		setScity(inputRecord.getScity());
		setIntday(inputRecord.getIntday());
		setIntmth(inputRecord.getIntmth());
		setIntdate(inputRecord.getIntdate());
		setDbstart(inputRecord.getDbstart());
		setDbcustomername(inputRecord.getDbcustomername());
		setDbcustomercity(inputRecord.getDbcustomercity());
		setDbcustomerstate(inputRecord.getDbcustomerstate());
		setDbmobile(inputRecord.getDbmobile());
		setDbtel1(inputRecord.getDbtel1());
		setDbtel2(inputRecord.getDbtel2());
		setDbmodel(inputRecord.getDbmodel());
		setDbmodelvarient(inputRecord.getDbmodelvarient());
		setDbfueltype(inputRecord.getDbfueltype());
		setDbvin(inputRecord.getDbvin());
		setDbdealercode(inputRecord.getDbdealercode());
		setDbdealername(inputRecord.getDbdealername());
		setDbdealercity(inputRecord.getDbdealercity());
		setDbdealerstate(inputRecord.getDbdealerstate());
		setDbdeliverydate(inputRecord.getDbdeliverydate());
		setDbsalesconsultantcode(inputRecord.getDbsalesconsultantcode());
		setDbsalesconsultantname(inputRecord.getDbsalesconsultantname());
		setDbpandacode(inputRecord.getDbpandacode());
		setDbdealer2(inputRecord.getDbdealer2());
		setDbdealer2labels(inputRecord.getDbdealer2labels());
		setDbquarternum(inputRecord.getDbquarternum());
		setDbbatchnum(inputRecord.getDbbatchnum());
		setDbend(inputRecord.getDbend());
		setFdstart(inputRecord.getFdstart());
		setDealer(inputRecord.getDealer());
		setDealer2(inputRecord.getDealer2());
		setCity2(inputRecord.getCity2());
		setRegiona(inputRecord.getRegiona());
		setState2(inputRecord.getState2());
		setModel(inputRecord.getModel());
		setModel2(inputRecord.getModel2());
		setCity3(inputRecord.getCity3());
		setFdend(inputRecord.getFdend());
		setIndexoutlierfilter(inputRecord.getIndexoutlierfilter());
		setIrecodestart(inputRecord.getIrecodestart());
		setZdelhr(inputRecord.getZdelhr());
		setZdelmin(inputRecord.getZdelmin());
		setCdelmin(inputRecord.getCdelmin());
		setNcicday(inputRecord.getNcicday());
		setNcdelmin(inputRecord.getNcdelmin());
		setIrecodeend(inputRecord.getIrecodeend());
		setRecodestart(inputRecord.getRecodestart());
		setNtdover(inputRecord.getNtdover());
		setNsigreet(inputRecord.getNsigreet());
		setNsiexpln(inputRecord.getNsiexpln());
		setNsidemo(inputRecord.getNsidemo());
		setNsifllow(inputRecord.getNsifllow());
		setNsiiover(inputRecord.getNsiiover());
		setNdlrlyot(inputRecord.getNdlrlyot());
		setNdlrcfof(inputRecord.getNdlrcfof());
		setNdlreslk(inputRecord.getNdlreslk());
		setNdlrsov(inputRecord.getNdlrsov());
		setNdlrloc(inputRecord.getNdlrloc());
		setNdlrover(inputRecord.getNdlrover());
		setNdlease(inputRecord.getNdlease());
		setNdlpexp(inputRecord.getNdlpexp());
		setNdlqkcom(inputRecord.getNdlqkcom());
		setNdlprcex(inputRecord.getNdlprcex());
		setNdldover(inputRecord.getNdldover());
		setNspkldge(inputRecord.getNspkldge());
		setNspneed(inputRecord.getNspneed());
		setNspcrtsy(inputRecord.getNspcrtsy());
		setNspprssr(inputRecord.getNspprssr());
		setNspcommt(inputRecord.getNspcommt());
		setNspover(inputRecord.getNspover());
		setNdtlen(inputRecord.getNdtlen());
		setNdtstatus(inputRecord.getNdtstatus());
		setNdttime(inputRecord.getNdttime());
		setNdtover(inputRecord.getNdtover());
		setNdpconvh(inputRecord.getNdpconvh());
		setNdpenthu(inputRecord.getNdpenthu());
		setNdptime(inputRecord.getNdptime());
		setNdpans(inputRecord.getNdpans());
		setNdpthor(inputRecord.getNdpthor());
		setNdpover(inputRecord.getNdpover());
		setNovover(inputRecord.getNovover());
		setTtdover(inputRecord.getTtdover());
		setTsigreet(inputRecord.getTsigreet());
		setTsiexpln(inputRecord.getTsiexpln());
		setTsidemo(inputRecord.getTsidemo());
		setTsifllow(inputRecord.getTsifllow());
		setTsiiover(inputRecord.getTsiiover());
		setTdlrlyot(inputRecord.getTdlrlyot());
		setTdlrcfof(inputRecord.getTdlrcfof());
		setTdlreslk(inputRecord.getTdlreslk());
		setTdlrsov(inputRecord.getTdlrsov());
		setTdlrloc(inputRecord.getTdlrloc());
		setTdlrover(inputRecord.getTdlrover());
		setTdlease(inputRecord.getTdlease());
		setTdlpexp(inputRecord.getTdlpexp());
		setTdlqkcom(inputRecord.getTdlqkcom());
		setTdlprcex(inputRecord.getTdlprcex());
		setTdldover(inputRecord.getTdldover());
		setTspkldge(inputRecord.getTspkldge());
		setTspneed(inputRecord.getTspneed());
		setTspcrtsy(inputRecord.getTspcrtsy());
		setTspprssr(inputRecord.getTspprssr());
		setTspcommt(inputRecord.getTspcommt());
		setTspover(inputRecord.getTspover());
		setTdtlen(inputRecord.getTdtlen());
		setTdtstatus(inputRecord.getTdtstatus());
		setTdttime(inputRecord.getTdttime());
		setTdtover(inputRecord.getTdtover());
		setTdpconvh(inputRecord.getTdpconvh());
		setTdpenthu(inputRecord.getTdpenthu());
		setTdptime(inputRecord.getTdptime());
		setTdpans(inputRecord.getTdpans());
		setTdpthor(inputRecord.getTdpthor());
		setTdpover(inputRecord.getTdpover());
		setTovover(inputRecord.getTovover());
		setAtdover(inputRecord.getAtdover());
		setAsigreet(inputRecord.getAsigreet());
		setAsiexpln(inputRecord.getAsiexpln());
		setAsidemo(inputRecord.getAsidemo());
		setAsifllow(inputRecord.getAsifllow());
		setAsiiover(inputRecord.getAsiiover());
		setAdlrlyot(inputRecord.getAdlrlyot());
		setAdlrcfof(inputRecord.getAdlrcfof());
		setAdlreslk(inputRecord.getAdlreslk());
		setAdlrsov(inputRecord.getAdlrsov());
		setAdlrloc(inputRecord.getAdlrloc());
		setAdlrover(inputRecord.getAdlrover());
		setAdlease(inputRecord.getAdlease());
		setAdlpexp(inputRecord.getAdlpexp());
		setAdlqkcom(inputRecord.getAdlqkcom());
		setAdlprcex(inputRecord.getAdlprcex());
		setAdldover(inputRecord.getAdldover());
		setAspkldge(inputRecord.getAspkldge());
		setAspneed(inputRecord.getAspneed());
		setAspcrtsy(inputRecord.getAspcrtsy());
		setAspprssr(inputRecord.getAspprssr());
		setAspcommt(inputRecord.getAspcommt());
		setAspover(inputRecord.getAspover());
		setAdtlen(inputRecord.getAdtlen());
		setAdtstatus(inputRecord.getAdtstatus());
		setAdttime(inputRecord.getAdttime());
		setAdtover(inputRecord.getAdtover());
		setAdpconvh(inputRecord.getAdpconvh());
		setAdpenthu(inputRecord.getAdpenthu());
		setAdptime(inputRecord.getAdptime());
		setAdpans(inputRecord.getAdpans());
		setAdpthor(inputRecord.getAdpthor());
		setAdpover(inputRecord.getAdpover());
		setAovover(inputRecord.getAovover());
		setPrb(inputRecord.getPrb());
		setNprb(inputRecord.getNprb());
		setNnprb(inputRecord.getNnprb());
		setCntft(inputRecord.getCntft());
		setNcntft(inputRecord.getNcntft());
		setCntexp(inputRecord.getCntexp());
		setNcntexp(inputRecord.getNcntexp());
		setSop(inputRecord.getSop());
		setNsop(inputRecord.getNsop());
		setComsop(inputRecord.getComsop());
		setNcomsop(inputRecord.getNcomsop());
		setProsop(inputRecord.getProsop());
		setNprosop(inputRecord.getNprosop());
		setRecodeend(inputRecord.getRecodeend());
		setIndex(inputRecord.getIndex());
		setSi(inputRecord.getSi());
		setDf(inputRecord.getDf());
		setDeal(inputRecord.getDeal());
		setSp(inputRecord.getSp());
		setDt(inputRecord.getDt());
		setDp(inputRecord.getDp());
		setNindex(inputRecord.getNindex());
		setBiweekly(inputRecord.getBiweekly());
		setMonth(inputRecord.getMonth());
		setQuarter(inputRecord.getQuarter());
		setYear(inputRecord.getYear());
	}

	public void loadNonNullContent(LISsitranslatedRecord inputRecord)
	{
		if (StringUtils.hasChanged(getSrno(), inputRecord.getSrno()))
		{
			setSrno(StringUtils.noNull(inputRecord.getSrno()));
		}
		if (StringUtils.hasChanged(getDbsrno(), inputRecord.getDbsrno()))
		{
			setDbsrno(StringUtils.noNull(inputRecord.getDbsrno()));
		}
		if (StringUtils.hasChanged(getTrack(), inputRecord.getTrack()))
		{
			setTrack(StringUtils.noNull(inputRecord.getTrack()));
		}
		if (StringUtils.hasChanged(getLstned(), inputRecord.getLstned()))
		{
			setLstned(StringUtils.noNull(inputRecord.getLstned()));
		}
		if (StringUtils.hasChanged(getQcdone(), inputRecord.getQcdone()))
		{
			setQcdone(StringUtils.noNull(inputRecord.getQcdone()));
		}
		if (StringUtils.hasChanged(getTapeadt(), inputRecord.getTapeadt()))
		{
			setTapeadt(StringUtils.noNull(inputRecord.getTapeadt()));
		}
		if (StringUtils.hasChanged(getExtaudi(), inputRecord.getExtaudi()))
		{
			setExtaudi(StringUtils.noNull(inputRecord.getExtaudi()));
		}
		if (StringUtils.hasChanged(getPdatey(), inputRecord.getPdatey()))
		{
			setPdatey(StringUtils.noNull(inputRecord.getPdatey()));
		}
		if (StringUtils.hasChanged(getPdatem(), inputRecord.getPdatem()))
		{
			setPdatem(StringUtils.noNull(inputRecord.getPdatem()));
		}
		if (StringUtils.hasChanged(getWork(), inputRecord.getWork()))
		{
			setWork(StringUtils.noNull(inputRecord.getWork()));
		}
		if (StringUtils.hasChanged(getCmak1(), inputRecord.getCmak1()))
		{
			setCmak1(StringUtils.noNull(inputRecord.getCmak1()));
		}
		if (StringUtils.hasChanged(getPercon(), inputRecord.getPercon()))
		{
			setPercon(StringUtils.noNull(inputRecord.getPercon()));
		}
		if (StringUtils.hasChanged(getCondate(), inputRecord.getCondate()))
		{
			setCondate(StringUtils.noNull(inputRecord.getCondate()));
		}
		if (StringUtils.hasChanged(getDdatecon(), inputRecord.getDdatecon()))
		{
			setDdatecon(StringUtils.noNull(inputRecord.getDdatecon()));
		}
		if (StringUtils.hasChanged(getConfdeal(), inputRecord.getConfdeal()))
		{
			setConfdeal(StringUtils.noNull(inputRecord.getConfdeal()));
		}
		if (StringUtils.hasChanged(getDealcon(), inputRecord.getDealcon()))
		{
			setDealcon(StringUtils.noNull(inputRecord.getDealcon()));
		}
		if (StringUtils.hasChanged(getMainuser(), inputRecord.getMainuser()))
		{
			setMainuser(StringUtils.noNull(inputRecord.getMainuser()));
		}
		if (StringUtils.hasChanged(getPdcmaker(), inputRecord.getPdcmaker()))
		{
			setPdcmaker(StringUtils.noNull(inputRecord.getPdcmaker()));
		}
		if (StringUtils.hasChanged(getVstford(), inputRecord.getVstford()))
		{
			setVstford(StringUtils.noNull(inputRecord.getVstford()));
		}
		if (StringUtils.hasChanged(getQrot(), inputRecord.getQrot()))
		{
			setQrot(StringUtils.noNull(inputRecord.getQrot()));
		}
		if (StringUtils.hasChanged(getWaitdlr(), inputRecord.getWaitdlr()))
		{
			setWaitdlr(StringUtils.noNull(inputRecord.getWaitdlr()));
		}
		if (StringUtils.hasChanged(getEffund(), inputRecord.getEffund()))
		{
			setEffund(StringUtils.noNull(inputRecord.getEffund()));
		}
		if (StringUtils.hasChanged(getIctstdr(), inputRecord.getIctstdr()))
		{
			setIctstdr(StringUtils.noNull(inputRecord.getIctstdr()));
		}
		if (StringUtils.hasChanged(getTdrdlr(), inputRecord.getTdrdlr()))
		{
			setTdrdlr(StringUtils.noNull(inputRecord.getTdrdlr()));
		}
		if (StringUtils.hasChanged(getTstrout(), inputRecord.getTstrout()))
		{
			setTstrout(StringUtils.noNull(inputRecord.getTstrout()));
		}
		if (StringUtils.hasChanged(getTstcar(), inputRecord.getTstcar()))
		{
			setTstcar(StringUtils.noNull(inputRecord.getTstcar()));
		}
		if (StringUtils.hasChanged(getTdover(), inputRecord.getTdover()))
		{
			setTdover(StringUtils.noNull(inputRecord.getTdover()));
		}
		if (StringUtils.hasChanged(getFtsafety(), inputRecord.getFtsafety()))
		{
			setFtsafety(StringUtils.noNull(inputRecord.getFtsafety()));
		}
		if (StringUtils.hasChanged(getFtconv(), inputRecord.getFtconv()))
		{
			setFtconv(StringUtils.noNull(inputRecord.getFtconv()));
		}
		if (StringUtils.hasChanged(getFtperf(), inputRecord.getFtperf()))
		{
			setFtperf(StringUtils.noNull(inputRecord.getFtperf()));
		}
		if (StringUtils.hasChanged(getFtcost(), inputRecord.getFtcost()))
		{
			setFtcost(StringUtils.noNull(inputRecord.getFtcost()));
		}
		if (StringUtils.hasChanged(getSpprovd(), inputRecord.getSpprovd()))
		{
			setSpprovd(StringUtils.noNull(inputRecord.getSpprovd()));
		}
		if (StringUtils.hasChanged(getSpcall(), inputRecord.getSpcall()))
		{
			setSpcall(StringUtils.noNull(inputRecord.getSpcall()));
		}
		if (StringUtils.hasChanged(getSigreet(), inputRecord.getSigreet()))
		{
			setSigreet(StringUtils.noNull(inputRecord.getSigreet()));
		}
		if (StringUtils.hasChanged(getSiexpln(), inputRecord.getSiexpln()))
		{
			setSiexpln(StringUtils.noNull(inputRecord.getSiexpln()));
		}
		if (StringUtils.hasChanged(getSidemo(), inputRecord.getSidemo()))
		{
			setSidemo(StringUtils.noNull(inputRecord.getSidemo()));
		}
		if (StringUtils.hasChanged(getSifllow(), inputRecord.getSifllow()))
		{
			setSifllow(StringUtils.noNull(inputRecord.getSifllow()));
		}
		if (StringUtils.hasChanged(getSiiover(), inputRecord.getSiiover()))
		{
			setSiiover(StringUtils.noNull(inputRecord.getSiiover()));
		}
		if (StringUtils.hasChanged(getDfseat(), inputRecord.getDfseat()))
		{
			setDfseat(StringUtils.noNull(inputRecord.getDfseat()));
		}
		if (StringUtils.hasChanged(getDfpark(), inputRecord.getDfpark()))
		{
			setDfpark(StringUtils.noNull(inputRecord.getDfpark()));
		}
		if (StringUtils.hasChanged(getDflit(), inputRecord.getDflit()))
		{
			setDflit(StringUtils.noNull(inputRecord.getDflit()));
		}
		if (StringUtils.hasChanged(getDfclean(), inputRecord.getDfclean()))
		{
			setDfclean(StringUtils.noNull(inputRecord.getDfclean()));
		}
		if (StringUtils.hasChanged(getDftemp(), inputRecord.getDftemp()))
		{
			setDftemp(StringUtils.noNull(inputRecord.getDftemp()));
		}
		if (StringUtils.hasChanged(getDfref(), inputRecord.getDfref()))
		{
			setDfref(StringUtils.noNull(inputRecord.getDfref()));
		}
		if (StringUtils.hasChanged(getDfsmell(), inputRecord.getDfsmell()))
		{
			setDfsmell(StringUtils.noNull(inputRecord.getDfsmell()));
		}
		if (StringUtils.hasChanged(getDfsignge(), inputRecord.getDfsignge()))
		{
			setDfsignge(StringUtils.noNull(inputRecord.getDfsignge()));
		}
		if (StringUtils.hasChanged(getDfrtrm(), inputRecord.getDfrtrm()))
		{
			setDfrtrm(StringUtils.noNull(inputRecord.getDfrtrm()));
		}
		if (StringUtils.hasChanged(getDfavshw(), inputRecord.getDfavshw()))
		{
			setDfavshw(StringUtils.noNull(inputRecord.getDfavshw()));
		}
		if (StringUtils.hasChanged(getDlrlyot(), inputRecord.getDlrlyot()))
		{
			setDlrlyot(StringUtils.noNull(inputRecord.getDlrlyot()));
		}
		if (StringUtils.hasChanged(getDlrcfof(), inputRecord.getDlrcfof()))
		{
			setDlrcfof(StringUtils.noNull(inputRecord.getDlrcfof()));
		}
		if (StringUtils.hasChanged(getDlreslk(), inputRecord.getDlreslk()))
		{
			setDlreslk(StringUtils.noNull(inputRecord.getDlreslk()));
		}
		if (StringUtils.hasChanged(getDlrsov(), inputRecord.getDlrsov()))
		{
			setDlrsov(StringUtils.noNull(inputRecord.getDlrsov()));
		}
		if (StringUtils.hasChanged(getDlrloc(), inputRecord.getDlrloc()))
		{
			setDlrloc(StringUtils.noNull(inputRecord.getDlrloc()));
		}
		if (StringUtils.hasChanged(getDlrover(), inputRecord.getDlrover()))
		{
			setDlrover(StringUtils.noNull(inputRecord.getDlrover()));
		}
		if (StringUtils.hasChanged(getVehfncd(), inputRecord.getVehfncd()))
		{
			setVehfncd(StringUtils.noNull(inputRecord.getVehfncd()));
		}
		if (StringUtils.hasChanged(getDlrfnc(), inputRecord.getDlrfnc()))
		{
			setDlrfnc(StringUtils.noNull(inputRecord.getDlrfnc()));
		}
		if (StringUtils.hasChanged(getCondeal(), inputRecord.getCondeal()))
		{
			setCondeal(StringUtils.noNull(inputRecord.getCondeal()));
		}
		if (StringUtils.hasChanged(getDlease(), inputRecord.getDlease()))
		{
			setDlease(StringUtils.noNull(inputRecord.getDlease()));
		}
		if (StringUtils.hasChanged(getDlpexp(), inputRecord.getDlpexp()))
		{
			setDlpexp(StringUtils.noNull(inputRecord.getDlpexp()));
		}
		if (StringUtils.hasChanged(getDlqkcom(), inputRecord.getDlqkcom()))
		{
			setDlqkcom(StringUtils.noNull(inputRecord.getDlqkcom()));
		}
		if (StringUtils.hasChanged(getDlprcex(), inputRecord.getDlprcex()))
		{
			setDlprcex(StringUtils.noNull(inputRecord.getDlprcex()));
		}
		if (StringUtils.hasChanged(getDldover(), inputRecord.getDldover()))
		{
			setDldover(StringUtils.noNull(inputRecord.getDldover()));
		}
		if (StringUtils.hasChanged(getIcgroom(), inputRecord.getIcgroom()))
		{
			setIcgroom(StringUtils.noNull(inputRecord.getIcgroom()));
		}
		if (StringUtils.hasChanged(getIcneatly(), inputRecord.getIcneatly()))
		{
			setIcneatly(StringUtils.noNull(inputRecord.getIcneatly()));
		}
		if (StringUtils.hasChanged(getIcspent(), inputRecord.getIcspent()))
		{
			setIcspent(StringUtils.noNull(inputRecord.getIcspent()));
		}
		if (StringUtils.hasChanged(getIcaskuse(), inputRecord.getIcaskuse()))
		{
			setIcaskuse(StringUtils.noNull(inputRecord.getIcaskuse()));
		}
		if (StringUtils.hasChanged(getIcinform(), inputRecord.getIcinform()))
		{
			setIcinform(StringUtils.noNull(inputRecord.getIcinform()));
		}
		if (StringUtils.hasChanged(getIcstatus(), inputRecord.getIcstatus()))
		{
			setIcstatus(StringUtils.noNull(inputRecord.getIcstatus()));
		}
		if (StringUtils.hasChanged(getSpkldge(), inputRecord.getSpkldge()))
		{
			setSpkldge(StringUtils.noNull(inputRecord.getSpkldge()));
		}
		if (StringUtils.hasChanged(getSpneed(), inputRecord.getSpneed()))
		{
			setSpneed(StringUtils.noNull(inputRecord.getSpneed()));
		}
		if (StringUtils.hasChanged(getSpcrtsy(), inputRecord.getSpcrtsy()))
		{
			setSpcrtsy(StringUtils.noNull(inputRecord.getSpcrtsy()));
		}
		if (StringUtils.hasChanged(getSpprssr(), inputRecord.getSpprssr()))
		{
			setSpprssr(StringUtils.noNull(inputRecord.getSpprssr()));
		}
		if (StringUtils.hasChanged(getSpcommt(), inputRecord.getSpcommt()))
		{
			setSpcommt(StringUtils.noNull(inputRecord.getSpcommt()));
		}
		if (StringUtils.hasChanged(getSpover(), inputRecord.getSpover()))
		{
			setSpover(StringUtils.noNull(inputRecord.getSpover()));
		}
		if (StringUtils.hasChanged(getIcsame(), inputRecord.getIcsame()))
		{
			setIcsame(StringUtils.noNull(inputRecord.getIcsame()));
		}
		if (StringUtils.hasChanged(getIcday(), inputRecord.getIcday()))
		{
			setIcday(StringUtils.noNull(inputRecord.getIcday()));
		}
		if (StringUtils.hasChanged(getDtvsexp(), inputRecord.getDtvsexp()))
		{
			setDtvsexp(StringUtils.noNull(inputRecord.getDtvsexp()));
		}
		if (StringUtils.hasChanged(getDldelct(), inputRecord.getDldelct()))
		{
			setDldelct(StringUtils.noNull(inputRecord.getDldelct()));
		}
		if (StringUtils.hasChanged(getDtlen(), inputRecord.getDtlen()))
		{
			setDtlen(StringUtils.noNull(inputRecord.getDtlen()));
		}
		if (StringUtils.hasChanged(getDtstatus(), inputRecord.getDtstatus()))
		{
			setDtstatus(StringUtils.noNull(inputRecord.getDtstatus()));
		}
		if (StringUtils.hasChanged(getDttime(), inputRecord.getDttime()))
		{
			setDttime(StringUtils.noNull(inputRecord.getDttime()));
		}
		if (StringUtils.hasChanged(getDtover(), inputRecord.getDtover()))
		{
			setDtover(StringUtils.noNull(inputRecord.getDtover()));
		}
		if (StringUtils.hasChanged(getAfvehft(), inputRecord.getAfvehft()))
		{
			setAfvehft(StringUtils.noNull(inputRecord.getAfvehft()));
		}
		if (StringUtils.hasChanged(getAfope(), inputRecord.getAfope()))
		{
			setAfope(StringUtils.noNull(inputRecord.getAfope()));
		}
		if (StringUtils.hasChanged(getAfwarr(), inputRecord.getAfwarr()))
		{
			setAfwarr(StringUtils.noNull(inputRecord.getAfwarr()));
		}
		if (StringUtils.hasChanged(getAfmaint(), inputRecord.getAfmaint()))
		{
			setAfmaint(StringUtils.noNull(inputRecord.getAfmaint()));
		}
		if (StringUtils.hasChanged(getDlhelp(), inputRecord.getDlhelp()))
		{
			setDlhelp(StringUtils.noNull(inputRecord.getDlhelp()));
		}
		if (StringUtils.hasChanged(getDlintro(), inputRecord.getDlintro()))
		{
			setDlintro(StringUtils.noNull(inputRecord.getDlintro()));
		}
		if (StringUtils.hasChanged(getDlenftme(), inputRecord.getDlenftme()))
		{
			setDlenftme(StringUtils.noNull(inputRecord.getDlenftme()));
		}
		if (StringUtils.hasChanged(getDlthank(), inputRecord.getDlthank()))
		{
			setDlthank(StringUtils.noNull(inputRecord.getDlthank()));
		}
		if (StringUtils.hasChanged(getDlramtf(), inputRecord.getDlramtf()))
		{
			setDlramtf(StringUtils.noNull(inputRecord.getDlramtf()));
		}
		if (StringUtils.hasChanged(getDlspcere(), inputRecord.getDlspcere()))
		{
			setDlspcere(StringUtils.noNull(inputRecord.getDlspcere()));
		}
		if (StringUtils.hasChanged(getDlvisit(), inputRecord.getDlvisit()))
		{
			setDlvisit(StringUtils.noNull(inputRecord.getDlvisit()));
		}
		if (StringUtils.hasChanged(getDlext(), inputRecord.getDlext()))
		{
			setDlext(StringUtils.noNull(inputRecord.getDlext()));
		}
		if (StringUtils.hasChanged(getDlfeat(), inputRecord.getDlfeat()))
		{
			setDlfeat(StringUtils.noNull(inputRecord.getDlfeat()));
		}
		if (StringUtils.hasChanged(getDlans(), inputRecord.getDlans()))
		{
			setDlans(StringUtils.noNull(inputRecord.getDlans()));
		}
		if (StringUtils.hasChanged(getDlfeebk(), inputRecord.getDlfeebk()))
		{
			setDlfeebk(StringUtils.noNull(inputRecord.getDlfeebk()));
		}
		if (StringUtils.hasChanged(getDlconta(), inputRecord.getDlconta()))
		{
			setDlconta(StringUtils.noNull(inputRecord.getDlconta()));
		}
		if (StringUtils.hasChanged(getDlappt(), inputRecord.getDlappt()))
		{
			setDlappt(StringUtils.noNull(inputRecord.getDlappt()));
		}
		if (StringUtils.hasChanged(getDldelpt(), inputRecord.getDldelpt()))
		{
			setDldelpt(StringUtils.noNull(inputRecord.getDldelpt()));
		}
		if (StringUtils.hasChanged(getDelhr(), inputRecord.getDelhr()))
		{
			setDelhr(StringUtils.noNull(inputRecord.getDelhr()));
		}
		if (StringUtils.hasChanged(getDelmin(), inputRecord.getDelmin()))
		{
			setDelmin(StringUtils.noNull(inputRecord.getDelmin()));
		}
		if (StringUtils.hasChanged(getDelcar(), inputRecord.getDelcar()))
		{
			setDelcar(StringUtils.noNull(inputRecord.getDelcar()));
		}
		if (StringUtils.hasChanged(getDpconvh(), inputRecord.getDpconvh()))
		{
			setDpconvh(StringUtils.noNull(inputRecord.getDpconvh()));
		}
		if (StringUtils.hasChanged(getDpenthu(), inputRecord.getDpenthu()))
		{
			setDpenthu(StringUtils.noNull(inputRecord.getDpenthu()));
		}
		if (StringUtils.hasChanged(getDptime(), inputRecord.getDptime()))
		{
			setDptime(StringUtils.noNull(inputRecord.getDptime()));
		}
		if (StringUtils.hasChanged(getDpans(), inputRecord.getDpans()))
		{
			setDpans(StringUtils.noNull(inputRecord.getDpans()));
		}
		if (StringUtils.hasChanged(getDpthor(), inputRecord.getDpthor()))
		{
			setDpthor(StringUtils.noNull(inputRecord.getDpthor()));
		}
		if (StringUtils.hasChanged(getDpover(), inputRecord.getDpover()))
		{
			setDpover(StringUtils.noNull(inputRecord.getDpover()));
		}
		if (StringUtils.hasChanged(getOvover(), inputRecord.getOvover()))
		{
			setOvover(StringUtils.noNull(inputRecord.getOvover()));
		}
		if (StringUtils.hasChanged(getPrbselct(), inputRecord.getPrbselct()))
		{
			setPrbselct(StringUtils.noNull(inputRecord.getPrbselct()));
		}
		if (StringUtils.hasChanged(getPrbstaf(), inputRecord.getPrbstaf()))
		{
			setPrbstaf(StringUtils.noNull(inputRecord.getPrbstaf()));
		}
		if (StringUtils.hasChanged(getPrbchrge(), inputRecord.getPrbchrge()))
		{
			setPrbchrge(StringUtils.noNull(inputRecord.getPrbchrge()));
		}
		if (StringUtils.hasChanged(getPrbanswr(), inputRecord.getPrbanswr()))
		{
			setPrbanswr(StringUtils.noNull(inputRecord.getPrbanswr()));
		}
		if (StringUtils.hasChanged(getPrbshff(), inputRecord.getPrbshff()))
		{
			setPrbshff(StringUtils.noNull(inputRecord.getPrbshff()));
		}
		if (StringUtils.hasChanged(getPrbphony(), inputRecord.getPrbphony()))
		{
			setPrbphony(StringUtils.noNull(inputRecord.getPrbphony()));
		}
		if (StringUtils.hasChanged(getRecodlr(), inputRecord.getRecodlr()))
		{
			setRecodlr(StringUtils.noNull(inputRecord.getRecodlr()));
		}
		if (StringUtils.hasChanged(getPsamedlr(), inputRecord.getPsamedlr()))
		{
			setPsamedlr(StringUtils.noNull(inputRecord.getPsamedlr()));
		}
		if (StringUtils.hasChanged(getRecomake(), inputRecord.getRecomake()))
		{
			setRecomake(StringUtils.noNull(inputRecord.getRecomake()));
		}
		if (StringUtils.hasChanged(getReprmake(), inputRecord.getReprmake()))
		{
			setReprmake(StringUtils.noNull(inputRecord.getReprmake()));
		}
		if (StringUtils.hasChanged(getEcdlr(), inputRecord.getEcdlr()))
		{
			setEcdlr(StringUtils.noNull(inputRecord.getEcdlr()));
		}
		if (StringUtils.hasChanged(getEcveh(), inputRecord.getEcveh()))
		{
			setEcveh(StringUtils.noNull(inputRecord.getEcveh()));
		}
		if (StringUtils.hasChanged(getFirstveh(), inputRecord.getFirstveh()))
		{
			setFirstveh(StringUtils.noNull(inputRecord.getFirstveh()));
		}
		if (StringUtils.hasChanged(getNewveh(), inputRecord.getNewveh()))
		{
			setNewveh(StringUtils.noNull(inputRecord.getNewveh()));
		}
		if (StringUtils.hasChanged(getJdpres(), inputRecord.getJdpres()))
		{
			setJdpres(StringUtils.noNull(inputRecord.getJdpres()));
		}
		if (StringUtils.hasChanged(getJdpinf(), inputRecord.getJdpinf()))
		{
			setJdpinf(StringUtils.noNull(inputRecord.getJdpinf()));
		}
		if (StringUtils.hasChanged(getFc(), inputRecord.getFc()))
		{
			setFc(StringUtils.noNull(inputRecord.getFc()));
		}
		if (StringUtils.hasChanged(getReveal(), inputRecord.getReveal()))
		{
			setReveal(StringUtils.noNull(inputRecord.getReveal()));
		}
		if (StringUtils.hasChanged(getProvfb(), inputRecord.getProvfb()))
		{
			setProvfb(StringUtils.noNull(inputRecord.getProvfb()));
		}
		if (StringUtils.hasChanged(getGender(), inputRecord.getGender()))
		{
			setGender(StringUtils.noNull(inputRecord.getGender()));
		}
		if (StringUtils.hasChanged(getDtmn(), inputRecord.getDtmn()))
		{
			setDtmn(StringUtils.noNull(inputRecord.getDtmn()));
		}
		if (StringUtils.hasChanged(getIntid(), inputRecord.getIntid()))
		{
			setIntid(StringUtils.noNull(inputRecord.getIntid()));
		}
		if (StringUtils.hasChanged(getIntgen(), inputRecord.getIntgen()))
		{
			setIntgen(StringUtils.noNull(inputRecord.getIntgen()));
		}
		if (StringUtils.hasChanged(getLmn(), inputRecord.getLmn()))
		{
			setLmn(StringUtils.noNull(inputRecord.getLmn()));
		}
		if (StringUtils.hasChanged(getIntme(), inputRecord.getIntme()))
		{
			setIntme(StringUtils.noNull(inputRecord.getIntme()));
		}
		if (StringUtils.hasChanged(getTxtoestart(), inputRecord.getTxtoestart()))
		{
			setTxtoestart(StringUtils.noNull(inputRecord.getTxtoestart()));
		}
		if (StringUtils.hasChanged(getTxtintid(), inputRecord.getTxtintid()))
		{
			setTxtintid(StringUtils.noNull(inputRecord.getTxtintid()));
		}
		if (StringUtils.hasChanged(getTxtoeend(), inputRecord.getTxtoeend()))
		{
			setTxtoeend(StringUtils.noNull(inputRecord.getTxtoeend()));
		}
		if (StringUtils.hasChanged(getScity(), inputRecord.getScity()))
		{
			setScity(StringUtils.noNull(inputRecord.getScity()));
		}
		if (StringUtils.hasChanged(getIntday(), inputRecord.getIntday()))
		{
			setIntday(StringUtils.noNull(inputRecord.getIntday()));
		}
		if (StringUtils.hasChanged(getIntmth(), inputRecord.getIntmth()))
		{
			setIntmth(StringUtils.noNull(inputRecord.getIntmth()));
		}
		if (StringUtils.hasChanged(getIntdate(), inputRecord.getIntdate()))
		{
			setIntdate(StringUtils.noNull(inputRecord.getIntdate()));
		}
		if (StringUtils.hasChanged(getDbstart(), inputRecord.getDbstart()))
		{
			setDbstart(StringUtils.noNull(inputRecord.getDbstart()));
		}
		if (StringUtils.hasChanged(getDbcustomername(), inputRecord.getDbcustomername()))
		{
			setDbcustomername(StringUtils.noNull(inputRecord.getDbcustomername()));
		}
		if (StringUtils.hasChanged(getDbcustomercity(), inputRecord.getDbcustomercity()))
		{
			setDbcustomercity(StringUtils.noNull(inputRecord.getDbcustomercity()));
		}
		if (StringUtils.hasChanged(getDbcustomerstate(), inputRecord.getDbcustomerstate()))
		{
			setDbcustomerstate(StringUtils.noNull(inputRecord.getDbcustomerstate()));
		}
		if (StringUtils.hasChanged(getDbmobile(), inputRecord.getDbmobile()))
		{
			setDbmobile(StringUtils.noNull(inputRecord.getDbmobile()));
		}
		if (StringUtils.hasChanged(getDbtel1(), inputRecord.getDbtel1()))
		{
			setDbtel1(StringUtils.noNull(inputRecord.getDbtel1()));
		}
		if (StringUtils.hasChanged(getDbtel2(), inputRecord.getDbtel2()))
		{
			setDbtel2(StringUtils.noNull(inputRecord.getDbtel2()));
		}
		if (StringUtils.hasChanged(getDbmodel(), inputRecord.getDbmodel()))
		{
			setDbmodel(StringUtils.noNull(inputRecord.getDbmodel()));
		}
		if (StringUtils.hasChanged(getDbmodelvarient(), inputRecord.getDbmodelvarient()))
		{
			setDbmodelvarient(StringUtils.noNull(inputRecord.getDbmodelvarient()));
		}
		if (StringUtils.hasChanged(getDbfueltype(), inputRecord.getDbfueltype()))
		{
			setDbfueltype(StringUtils.noNull(inputRecord.getDbfueltype()));
		}
		if (StringUtils.hasChanged(getDbvin(), inputRecord.getDbvin()))
		{
			setDbvin(StringUtils.noNull(inputRecord.getDbvin()));
		}
		if (StringUtils.hasChanged(getDbdealercode(), inputRecord.getDbdealercode()))
		{
			setDbdealercode(StringUtils.noNull(inputRecord.getDbdealercode()));
		}
		if (StringUtils.hasChanged(getDbdealername(), inputRecord.getDbdealername()))
		{
			setDbdealername(StringUtils.noNull(inputRecord.getDbdealername()));
		}
		if (StringUtils.hasChanged(getDbdealercity(), inputRecord.getDbdealercity()))
		{
			setDbdealercity(StringUtils.noNull(inputRecord.getDbdealercity()));
		}
		if (StringUtils.hasChanged(getDbdealerstate(), inputRecord.getDbdealerstate()))
		{
			setDbdealerstate(StringUtils.noNull(inputRecord.getDbdealerstate()));
		}
		if (StringUtils.hasChanged(getDbdeliverydate(), inputRecord.getDbdeliverydate()))
		{
			setDbdeliverydate(StringUtils.noNull(inputRecord.getDbdeliverydate()));
		}
		if (StringUtils.hasChanged(getDbsalesconsultantcode(), inputRecord.getDbsalesconsultantcode()))
		{
			setDbsalesconsultantcode(StringUtils.noNull(inputRecord.getDbsalesconsultantcode()));
		}
		if (StringUtils.hasChanged(getDbsalesconsultantname(), inputRecord.getDbsalesconsultantname()))
		{
			setDbsalesconsultantname(StringUtils.noNull(inputRecord.getDbsalesconsultantname()));
		}
		if (StringUtils.hasChanged(getDbpandacode(), inputRecord.getDbpandacode()))
		{
			setDbpandacode(StringUtils.noNull(inputRecord.getDbpandacode()));
		}
		if (StringUtils.hasChanged(getDbdealer2(), inputRecord.getDbdealer2()))
		{
			setDbdealer2(StringUtils.noNull(inputRecord.getDbdealer2()));
		}
		if (StringUtils.hasChanged(getDbdealer2labels(), inputRecord.getDbdealer2labels()))
		{
			setDbdealer2labels(StringUtils.noNull(inputRecord.getDbdealer2labels()));
		}
		if (StringUtils.hasChanged(getDbquarternum(), inputRecord.getDbquarternum()))
		{
			setDbquarternum(StringUtils.noNull(inputRecord.getDbquarternum()));
		}
		if (StringUtils.hasChanged(getDbbatchnum(), inputRecord.getDbbatchnum()))
		{
			setDbbatchnum(StringUtils.noNull(inputRecord.getDbbatchnum()));
		}
		if (StringUtils.hasChanged(getDbend(), inputRecord.getDbend()))
		{
			setDbend(StringUtils.noNull(inputRecord.getDbend()));
		}
		if (StringUtils.hasChanged(getFdstart(), inputRecord.getFdstart()))
		{
			setFdstart(StringUtils.noNull(inputRecord.getFdstart()));
		}
		if (StringUtils.hasChanged(getDealer(), inputRecord.getDealer()))
		{
			setDealer(StringUtils.noNull(inputRecord.getDealer()));
		}
		if (StringUtils.hasChanged(getDealer2(), inputRecord.getDealer2()))
		{
			setDealer2(StringUtils.noNull(inputRecord.getDealer2()));
		}
		if (StringUtils.hasChanged(getCity2(), inputRecord.getCity2()))
		{
			setCity2(StringUtils.noNull(inputRecord.getCity2()));
		}
		if (StringUtils.hasChanged(getRegiona(), inputRecord.getRegiona()))
		{
			setRegiona(StringUtils.noNull(inputRecord.getRegiona()));
		}
		if (StringUtils.hasChanged(getState2(), inputRecord.getState2()))
		{
			setState2(StringUtils.noNull(inputRecord.getState2()));
		}
		if (StringUtils.hasChanged(getModel(), inputRecord.getModel()))
		{
			setModel(StringUtils.noNull(inputRecord.getModel()));
		}
		if (StringUtils.hasChanged(getModel2(), inputRecord.getModel2()))
		{
			setModel2(StringUtils.noNull(inputRecord.getModel2()));
		}
		if (StringUtils.hasChanged(getCity3(), inputRecord.getCity3()))
		{
			setCity3(StringUtils.noNull(inputRecord.getCity3()));
		}
		if (StringUtils.hasChanged(getFdend(), inputRecord.getFdend()))
		{
			setFdend(StringUtils.noNull(inputRecord.getFdend()));
		}
		if (StringUtils.hasChanged(getIndexoutlierfilter(), inputRecord.getIndexoutlierfilter()))
		{
			setIndexoutlierfilter(StringUtils.noNull(inputRecord.getIndexoutlierfilter()));
		}
		if (StringUtils.hasChanged(getIrecodestart(), inputRecord.getIrecodestart()))
		{
			setIrecodestart(StringUtils.noNull(inputRecord.getIrecodestart()));
		}
		if (StringUtils.hasChanged(getZdelhr(), inputRecord.getZdelhr()))
		{
			setZdelhr(StringUtils.noNull(inputRecord.getZdelhr()));
		}
		if (StringUtils.hasChanged(getZdelmin(), inputRecord.getZdelmin()))
		{
			setZdelmin(StringUtils.noNull(inputRecord.getZdelmin()));
		}
		if (StringUtils.hasChanged(getCdelmin(), inputRecord.getCdelmin()))
		{
			setCdelmin(StringUtils.noNull(inputRecord.getCdelmin()));
		}
		if (StringUtils.hasChanged(getNcicday(), inputRecord.getNcicday()))
		{
			setNcicday(StringUtils.noNull(inputRecord.getNcicday()));
		}
		if (StringUtils.hasChanged(getNcdelmin(), inputRecord.getNcdelmin()))
		{
			setNcdelmin(StringUtils.noNull(inputRecord.getNcdelmin()));
		}
		if (StringUtils.hasChanged(getIrecodeend(), inputRecord.getIrecodeend()))
		{
			setIrecodeend(StringUtils.noNull(inputRecord.getIrecodeend()));
		}
		if (StringUtils.hasChanged(getRecodestart(), inputRecord.getRecodestart()))
		{
			setRecodestart(StringUtils.noNull(inputRecord.getRecodestart()));
		}
		if (StringUtils.hasChanged(getNtdover(), inputRecord.getNtdover()))
		{
			setNtdover(StringUtils.noNull(inputRecord.getNtdover()));
		}
		if (StringUtils.hasChanged(getNsigreet(), inputRecord.getNsigreet()))
		{
			setNsigreet(StringUtils.noNull(inputRecord.getNsigreet()));
		}
		if (StringUtils.hasChanged(getNsiexpln(), inputRecord.getNsiexpln()))
		{
			setNsiexpln(StringUtils.noNull(inputRecord.getNsiexpln()));
		}
		if (StringUtils.hasChanged(getNsidemo(), inputRecord.getNsidemo()))
		{
			setNsidemo(StringUtils.noNull(inputRecord.getNsidemo()));
		}
		if (StringUtils.hasChanged(getNsifllow(), inputRecord.getNsifllow()))
		{
			setNsifllow(StringUtils.noNull(inputRecord.getNsifllow()));
		}
		if (StringUtils.hasChanged(getNsiiover(), inputRecord.getNsiiover()))
		{
			setNsiiover(StringUtils.noNull(inputRecord.getNsiiover()));
		}
		if (StringUtils.hasChanged(getNdlrlyot(), inputRecord.getNdlrlyot()))
		{
			setNdlrlyot(StringUtils.noNull(inputRecord.getNdlrlyot()));
		}
		if (StringUtils.hasChanged(getNdlrcfof(), inputRecord.getNdlrcfof()))
		{
			setNdlrcfof(StringUtils.noNull(inputRecord.getNdlrcfof()));
		}
		if (StringUtils.hasChanged(getNdlreslk(), inputRecord.getNdlreslk()))
		{
			setNdlreslk(StringUtils.noNull(inputRecord.getNdlreslk()));
		}
		if (StringUtils.hasChanged(getNdlrsov(), inputRecord.getNdlrsov()))
		{
			setNdlrsov(StringUtils.noNull(inputRecord.getNdlrsov()));
		}
		if (StringUtils.hasChanged(getNdlrloc(), inputRecord.getNdlrloc()))
		{
			setNdlrloc(StringUtils.noNull(inputRecord.getNdlrloc()));
		}
		if (StringUtils.hasChanged(getNdlrover(), inputRecord.getNdlrover()))
		{
			setNdlrover(StringUtils.noNull(inputRecord.getNdlrover()));
		}
		if (StringUtils.hasChanged(getNdlease(), inputRecord.getNdlease()))
		{
			setNdlease(StringUtils.noNull(inputRecord.getNdlease()));
		}
		if (StringUtils.hasChanged(getNdlpexp(), inputRecord.getNdlpexp()))
		{
			setNdlpexp(StringUtils.noNull(inputRecord.getNdlpexp()));
		}
		if (StringUtils.hasChanged(getNdlqkcom(), inputRecord.getNdlqkcom()))
		{
			setNdlqkcom(StringUtils.noNull(inputRecord.getNdlqkcom()));
		}
		if (StringUtils.hasChanged(getNdlprcex(), inputRecord.getNdlprcex()))
		{
			setNdlprcex(StringUtils.noNull(inputRecord.getNdlprcex()));
		}
		if (StringUtils.hasChanged(getNdldover(), inputRecord.getNdldover()))
		{
			setNdldover(StringUtils.noNull(inputRecord.getNdldover()));
		}
		if (StringUtils.hasChanged(getNspkldge(), inputRecord.getNspkldge()))
		{
			setNspkldge(StringUtils.noNull(inputRecord.getNspkldge()));
		}
		if (StringUtils.hasChanged(getNspneed(), inputRecord.getNspneed()))
		{
			setNspneed(StringUtils.noNull(inputRecord.getNspneed()));
		}
		if (StringUtils.hasChanged(getNspcrtsy(), inputRecord.getNspcrtsy()))
		{
			setNspcrtsy(StringUtils.noNull(inputRecord.getNspcrtsy()));
		}
		if (StringUtils.hasChanged(getNspprssr(), inputRecord.getNspprssr()))
		{
			setNspprssr(StringUtils.noNull(inputRecord.getNspprssr()));
		}
		if (StringUtils.hasChanged(getNspcommt(), inputRecord.getNspcommt()))
		{
			setNspcommt(StringUtils.noNull(inputRecord.getNspcommt()));
		}
		if (StringUtils.hasChanged(getNspover(), inputRecord.getNspover()))
		{
			setNspover(StringUtils.noNull(inputRecord.getNspover()));
		}
		if (StringUtils.hasChanged(getNdtlen(), inputRecord.getNdtlen()))
		{
			setNdtlen(StringUtils.noNull(inputRecord.getNdtlen()));
		}
		if (StringUtils.hasChanged(getNdtstatus(), inputRecord.getNdtstatus()))
		{
			setNdtstatus(StringUtils.noNull(inputRecord.getNdtstatus()));
		}
		if (StringUtils.hasChanged(getNdttime(), inputRecord.getNdttime()))
		{
			setNdttime(StringUtils.noNull(inputRecord.getNdttime()));
		}
		if (StringUtils.hasChanged(getNdtover(), inputRecord.getNdtover()))
		{
			setNdtover(StringUtils.noNull(inputRecord.getNdtover()));
		}
		if (StringUtils.hasChanged(getNdpconvh(), inputRecord.getNdpconvh()))
		{
			setNdpconvh(StringUtils.noNull(inputRecord.getNdpconvh()));
		}
		if (StringUtils.hasChanged(getNdpenthu(), inputRecord.getNdpenthu()))
		{
			setNdpenthu(StringUtils.noNull(inputRecord.getNdpenthu()));
		}
		if (StringUtils.hasChanged(getNdptime(), inputRecord.getNdptime()))
		{
			setNdptime(StringUtils.noNull(inputRecord.getNdptime()));
		}
		if (StringUtils.hasChanged(getNdpans(), inputRecord.getNdpans()))
		{
			setNdpans(StringUtils.noNull(inputRecord.getNdpans()));
		}
		if (StringUtils.hasChanged(getNdpthor(), inputRecord.getNdpthor()))
		{
			setNdpthor(StringUtils.noNull(inputRecord.getNdpthor()));
		}
		if (StringUtils.hasChanged(getNdpover(), inputRecord.getNdpover()))
		{
			setNdpover(StringUtils.noNull(inputRecord.getNdpover()));
		}
		if (StringUtils.hasChanged(getNovover(), inputRecord.getNovover()))
		{
			setNovover(StringUtils.noNull(inputRecord.getNovover()));
		}
		if (StringUtils.hasChanged(getTtdover(), inputRecord.getTtdover()))
		{
			setTtdover(StringUtils.noNull(inputRecord.getTtdover()));
		}
		if (StringUtils.hasChanged(getTsigreet(), inputRecord.getTsigreet()))
		{
			setTsigreet(StringUtils.noNull(inputRecord.getTsigreet()));
		}
		if (StringUtils.hasChanged(getTsiexpln(), inputRecord.getTsiexpln()))
		{
			setTsiexpln(StringUtils.noNull(inputRecord.getTsiexpln()));
		}
		if (StringUtils.hasChanged(getTsidemo(), inputRecord.getTsidemo()))
		{
			setTsidemo(StringUtils.noNull(inputRecord.getTsidemo()));
		}
		if (StringUtils.hasChanged(getTsifllow(), inputRecord.getTsifllow()))
		{
			setTsifllow(StringUtils.noNull(inputRecord.getTsifllow()));
		}
		if (StringUtils.hasChanged(getTsiiover(), inputRecord.getTsiiover()))
		{
			setTsiiover(StringUtils.noNull(inputRecord.getTsiiover()));
		}
		if (StringUtils.hasChanged(getTdlrlyot(), inputRecord.getTdlrlyot()))
		{
			setTdlrlyot(StringUtils.noNull(inputRecord.getTdlrlyot()));
		}
		if (StringUtils.hasChanged(getTdlrcfof(), inputRecord.getTdlrcfof()))
		{
			setTdlrcfof(StringUtils.noNull(inputRecord.getTdlrcfof()));
		}
		if (StringUtils.hasChanged(getTdlreslk(), inputRecord.getTdlreslk()))
		{
			setTdlreslk(StringUtils.noNull(inputRecord.getTdlreslk()));
		}
		if (StringUtils.hasChanged(getTdlrsov(), inputRecord.getTdlrsov()))
		{
			setTdlrsov(StringUtils.noNull(inputRecord.getTdlrsov()));
		}
		if (StringUtils.hasChanged(getTdlrloc(), inputRecord.getTdlrloc()))
		{
			setTdlrloc(StringUtils.noNull(inputRecord.getTdlrloc()));
		}
		if (StringUtils.hasChanged(getTdlrover(), inputRecord.getTdlrover()))
		{
			setTdlrover(StringUtils.noNull(inputRecord.getTdlrover()));
		}
		if (StringUtils.hasChanged(getTdlease(), inputRecord.getTdlease()))
		{
			setTdlease(StringUtils.noNull(inputRecord.getTdlease()));
		}
		if (StringUtils.hasChanged(getTdlpexp(), inputRecord.getTdlpexp()))
		{
			setTdlpexp(StringUtils.noNull(inputRecord.getTdlpexp()));
		}
		if (StringUtils.hasChanged(getTdlqkcom(), inputRecord.getTdlqkcom()))
		{
			setTdlqkcom(StringUtils.noNull(inputRecord.getTdlqkcom()));
		}
		if (StringUtils.hasChanged(getTdlprcex(), inputRecord.getTdlprcex()))
		{
			setTdlprcex(StringUtils.noNull(inputRecord.getTdlprcex()));
		}
		if (StringUtils.hasChanged(getTdldover(), inputRecord.getTdldover()))
		{
			setTdldover(StringUtils.noNull(inputRecord.getTdldover()));
		}
		if (StringUtils.hasChanged(getTspkldge(), inputRecord.getTspkldge()))
		{
			setTspkldge(StringUtils.noNull(inputRecord.getTspkldge()));
		}
		if (StringUtils.hasChanged(getTspneed(), inputRecord.getTspneed()))
		{
			setTspneed(StringUtils.noNull(inputRecord.getTspneed()));
		}
		if (StringUtils.hasChanged(getTspcrtsy(), inputRecord.getTspcrtsy()))
		{
			setTspcrtsy(StringUtils.noNull(inputRecord.getTspcrtsy()));
		}
		if (StringUtils.hasChanged(getTspprssr(), inputRecord.getTspprssr()))
		{
			setTspprssr(StringUtils.noNull(inputRecord.getTspprssr()));
		}
		if (StringUtils.hasChanged(getTspcommt(), inputRecord.getTspcommt()))
		{
			setTspcommt(StringUtils.noNull(inputRecord.getTspcommt()));
		}
		if (StringUtils.hasChanged(getTspover(), inputRecord.getTspover()))
		{
			setTspover(StringUtils.noNull(inputRecord.getTspover()));
		}
		if (StringUtils.hasChanged(getTdtlen(), inputRecord.getTdtlen()))
		{
			setTdtlen(StringUtils.noNull(inputRecord.getTdtlen()));
		}
		if (StringUtils.hasChanged(getTdtstatus(), inputRecord.getTdtstatus()))
		{
			setTdtstatus(StringUtils.noNull(inputRecord.getTdtstatus()));
		}
		if (StringUtils.hasChanged(getTdttime(), inputRecord.getTdttime()))
		{
			setTdttime(StringUtils.noNull(inputRecord.getTdttime()));
		}
		if (StringUtils.hasChanged(getTdtover(), inputRecord.getTdtover()))
		{
			setTdtover(StringUtils.noNull(inputRecord.getTdtover()));
		}
		if (StringUtils.hasChanged(getTdpconvh(), inputRecord.getTdpconvh()))
		{
			setTdpconvh(StringUtils.noNull(inputRecord.getTdpconvh()));
		}
		if (StringUtils.hasChanged(getTdpenthu(), inputRecord.getTdpenthu()))
		{
			setTdpenthu(StringUtils.noNull(inputRecord.getTdpenthu()));
		}
		if (StringUtils.hasChanged(getTdptime(), inputRecord.getTdptime()))
		{
			setTdptime(StringUtils.noNull(inputRecord.getTdptime()));
		}
		if (StringUtils.hasChanged(getTdpans(), inputRecord.getTdpans()))
		{
			setTdpans(StringUtils.noNull(inputRecord.getTdpans()));
		}
		if (StringUtils.hasChanged(getTdpthor(), inputRecord.getTdpthor()))
		{
			setTdpthor(StringUtils.noNull(inputRecord.getTdpthor()));
		}
		if (StringUtils.hasChanged(getTdpover(), inputRecord.getTdpover()))
		{
			setTdpover(StringUtils.noNull(inputRecord.getTdpover()));
		}
		if (StringUtils.hasChanged(getTovover(), inputRecord.getTovover()))
		{
			setTovover(StringUtils.noNull(inputRecord.getTovover()));
		}
		if (StringUtils.hasChanged(getAtdover(), inputRecord.getAtdover()))
		{
			setAtdover(StringUtils.noNull(inputRecord.getAtdover()));
		}
		if (StringUtils.hasChanged(getAsigreet(), inputRecord.getAsigreet()))
		{
			setAsigreet(StringUtils.noNull(inputRecord.getAsigreet()));
		}
		if (StringUtils.hasChanged(getAsiexpln(), inputRecord.getAsiexpln()))
		{
			setAsiexpln(StringUtils.noNull(inputRecord.getAsiexpln()));
		}
		if (StringUtils.hasChanged(getAsidemo(), inputRecord.getAsidemo()))
		{
			setAsidemo(StringUtils.noNull(inputRecord.getAsidemo()));
		}
		if (StringUtils.hasChanged(getAsifllow(), inputRecord.getAsifllow()))
		{
			setAsifllow(StringUtils.noNull(inputRecord.getAsifllow()));
		}
		if (StringUtils.hasChanged(getAsiiover(), inputRecord.getAsiiover()))
		{
			setAsiiover(StringUtils.noNull(inputRecord.getAsiiover()));
		}
		if (StringUtils.hasChanged(getAdlrlyot(), inputRecord.getAdlrlyot()))
		{
			setAdlrlyot(StringUtils.noNull(inputRecord.getAdlrlyot()));
		}
		if (StringUtils.hasChanged(getAdlrcfof(), inputRecord.getAdlrcfof()))
		{
			setAdlrcfof(StringUtils.noNull(inputRecord.getAdlrcfof()));
		}
		if (StringUtils.hasChanged(getAdlreslk(), inputRecord.getAdlreslk()))
		{
			setAdlreslk(StringUtils.noNull(inputRecord.getAdlreslk()));
		}
		if (StringUtils.hasChanged(getAdlrsov(), inputRecord.getAdlrsov()))
		{
			setAdlrsov(StringUtils.noNull(inputRecord.getAdlrsov()));
		}
		if (StringUtils.hasChanged(getAdlrloc(), inputRecord.getAdlrloc()))
		{
			setAdlrloc(StringUtils.noNull(inputRecord.getAdlrloc()));
		}
		if (StringUtils.hasChanged(getAdlrover(), inputRecord.getAdlrover()))
		{
			setAdlrover(StringUtils.noNull(inputRecord.getAdlrover()));
		}
		if (StringUtils.hasChanged(getAdlease(), inputRecord.getAdlease()))
		{
			setAdlease(StringUtils.noNull(inputRecord.getAdlease()));
		}
		if (StringUtils.hasChanged(getAdlpexp(), inputRecord.getAdlpexp()))
		{
			setAdlpexp(StringUtils.noNull(inputRecord.getAdlpexp()));
		}
		if (StringUtils.hasChanged(getAdlqkcom(), inputRecord.getAdlqkcom()))
		{
			setAdlqkcom(StringUtils.noNull(inputRecord.getAdlqkcom()));
		}
		if (StringUtils.hasChanged(getAdlprcex(), inputRecord.getAdlprcex()))
		{
			setAdlprcex(StringUtils.noNull(inputRecord.getAdlprcex()));
		}
		if (StringUtils.hasChanged(getAdldover(), inputRecord.getAdldover()))
		{
			setAdldover(StringUtils.noNull(inputRecord.getAdldover()));
		}
		if (StringUtils.hasChanged(getAspkldge(), inputRecord.getAspkldge()))
		{
			setAspkldge(StringUtils.noNull(inputRecord.getAspkldge()));
		}
		if (StringUtils.hasChanged(getAspneed(), inputRecord.getAspneed()))
		{
			setAspneed(StringUtils.noNull(inputRecord.getAspneed()));
		}
		if (StringUtils.hasChanged(getAspcrtsy(), inputRecord.getAspcrtsy()))
		{
			setAspcrtsy(StringUtils.noNull(inputRecord.getAspcrtsy()));
		}
		if (StringUtils.hasChanged(getAspprssr(), inputRecord.getAspprssr()))
		{
			setAspprssr(StringUtils.noNull(inputRecord.getAspprssr()));
		}
		if (StringUtils.hasChanged(getAspcommt(), inputRecord.getAspcommt()))
		{
			setAspcommt(StringUtils.noNull(inputRecord.getAspcommt()));
		}
		if (StringUtils.hasChanged(getAspover(), inputRecord.getAspover()))
		{
			setAspover(StringUtils.noNull(inputRecord.getAspover()));
		}
		if (StringUtils.hasChanged(getAdtlen(), inputRecord.getAdtlen()))
		{
			setAdtlen(StringUtils.noNull(inputRecord.getAdtlen()));
		}
		if (StringUtils.hasChanged(getAdtstatus(), inputRecord.getAdtstatus()))
		{
			setAdtstatus(StringUtils.noNull(inputRecord.getAdtstatus()));
		}
		if (StringUtils.hasChanged(getAdttime(), inputRecord.getAdttime()))
		{
			setAdttime(StringUtils.noNull(inputRecord.getAdttime()));
		}
		if (StringUtils.hasChanged(getAdtover(), inputRecord.getAdtover()))
		{
			setAdtover(StringUtils.noNull(inputRecord.getAdtover()));
		}
		if (StringUtils.hasChanged(getAdpconvh(), inputRecord.getAdpconvh()))
		{
			setAdpconvh(StringUtils.noNull(inputRecord.getAdpconvh()));
		}
		if (StringUtils.hasChanged(getAdpenthu(), inputRecord.getAdpenthu()))
		{
			setAdpenthu(StringUtils.noNull(inputRecord.getAdpenthu()));
		}
		if (StringUtils.hasChanged(getAdptime(), inputRecord.getAdptime()))
		{
			setAdptime(StringUtils.noNull(inputRecord.getAdptime()));
		}
		if (StringUtils.hasChanged(getAdpans(), inputRecord.getAdpans()))
		{
			setAdpans(StringUtils.noNull(inputRecord.getAdpans()));
		}
		if (StringUtils.hasChanged(getAdpthor(), inputRecord.getAdpthor()))
		{
			setAdpthor(StringUtils.noNull(inputRecord.getAdpthor()));
		}
		if (StringUtils.hasChanged(getAdpover(), inputRecord.getAdpover()))
		{
			setAdpover(StringUtils.noNull(inputRecord.getAdpover()));
		}
		if (StringUtils.hasChanged(getAovover(), inputRecord.getAovover()))
		{
			setAovover(StringUtils.noNull(inputRecord.getAovover()));
		}
		if (StringUtils.hasChanged(getPrb(), inputRecord.getPrb()))
		{
			setPrb(StringUtils.noNull(inputRecord.getPrb()));
		}
		if (StringUtils.hasChanged(getNprb(), inputRecord.getNprb()))
		{
			setNprb(StringUtils.noNull(inputRecord.getNprb()));
		}
		if (StringUtils.hasChanged(getNnprb(), inputRecord.getNnprb()))
		{
			setNnprb(StringUtils.noNull(inputRecord.getNnprb()));
		}
		if (StringUtils.hasChanged(getCntft(), inputRecord.getCntft()))
		{
			setCntft(StringUtils.noNull(inputRecord.getCntft()));
		}
		if (StringUtils.hasChanged(getNcntft(), inputRecord.getNcntft()))
		{
			setNcntft(StringUtils.noNull(inputRecord.getNcntft()));
		}
		if (StringUtils.hasChanged(getCntexp(), inputRecord.getCntexp()))
		{
			setCntexp(StringUtils.noNull(inputRecord.getCntexp()));
		}
		if (StringUtils.hasChanged(getNcntexp(), inputRecord.getNcntexp()))
		{
			setNcntexp(StringUtils.noNull(inputRecord.getNcntexp()));
		}
		if (StringUtils.hasChanged(getSop(), inputRecord.getSop()))
		{
			setSop(StringUtils.noNull(inputRecord.getSop()));
		}
		if (StringUtils.hasChanged(getNsop(), inputRecord.getNsop()))
		{
			setNsop(StringUtils.noNull(inputRecord.getNsop()));
		}
		if (StringUtils.hasChanged(getComsop(), inputRecord.getComsop()))
		{
			setComsop(StringUtils.noNull(inputRecord.getComsop()));
		}
		if (StringUtils.hasChanged(getNcomsop(), inputRecord.getNcomsop()))
		{
			setNcomsop(StringUtils.noNull(inputRecord.getNcomsop()));
		}
		if (StringUtils.hasChanged(getProsop(), inputRecord.getProsop()))
		{
			setProsop(StringUtils.noNull(inputRecord.getProsop()));
		}
		if (StringUtils.hasChanged(getNprosop(), inputRecord.getNprosop()))
		{
			setNprosop(StringUtils.noNull(inputRecord.getNprosop()));
		}
		if (StringUtils.hasChanged(getRecodeend(), inputRecord.getRecodeend()))
		{
			setRecodeend(StringUtils.noNull(inputRecord.getRecodeend()));
		}
		if (StringUtils.hasChanged(getIndex(), inputRecord.getIndex()))
		{
			setIndex(StringUtils.noNull(inputRecord.getIndex()));
		}
		if (StringUtils.hasChanged(getSi(), inputRecord.getSi()))
		{
			setSi(StringUtils.noNull(inputRecord.getSi()));
		}
		if (StringUtils.hasChanged(getDf(), inputRecord.getDf()))
		{
			setDf(StringUtils.noNull(inputRecord.getDf()));
		}
		if (StringUtils.hasChanged(getDeal(), inputRecord.getDeal()))
		{
			setDeal(StringUtils.noNull(inputRecord.getDeal()));
		}
		if (StringUtils.hasChanged(getSp(), inputRecord.getSp()))
		{
			setSp(StringUtils.noNull(inputRecord.getSp()));
		}
		if (StringUtils.hasChanged(getDt(), inputRecord.getDt()))
		{
			setDt(StringUtils.noNull(inputRecord.getDt()));
		}
		if (StringUtils.hasChanged(getDp(), inputRecord.getDp()))
		{
			setDp(StringUtils.noNull(inputRecord.getDp()));
		}
		if (StringUtils.hasChanged(getNindex(), inputRecord.getNindex()))
		{
			setNindex(StringUtils.noNull(inputRecord.getNindex()));
		}
		if (StringUtils.hasChanged(getBiweekly(), inputRecord.getBiweekly()))
		{
			setBiweekly(StringUtils.noNull(inputRecord.getBiweekly()));
		}
		if (StringUtils.hasChanged(getMonth(), inputRecord.getMonth()))
		{
			setMonth(StringUtils.noNull(inputRecord.getMonth()));
		}
		if (StringUtils.hasChanged(getQuarter(), inputRecord.getQuarter()))
		{
			setQuarter(StringUtils.noNull(inputRecord.getQuarter()));
		}
		if (StringUtils.hasChanged(getYear(), inputRecord.getYear()))
		{
			setYear(StringUtils.noNull(inputRecord.getYear()));
		}
	}

	public JSONObject getJSONObject()
	{
		JSONObject obj = new JSONObject();

		obj.put("srno",StringUtils.noNull(srno));				
		obj.put("dbsrno",StringUtils.noNull(dbsrno));				
		obj.put("track",StringUtils.noNull(track));				
		obj.put("lstned",StringUtils.noNull(lstned));				
		obj.put("qcdone",StringUtils.noNull(qcdone));				
		obj.put("tapeadt",StringUtils.noNull(tapeadt));				
		obj.put("extaudi",StringUtils.noNull(extaudi));				
		obj.put("pdatey",StringUtils.noNull(pdatey));				
		obj.put("pdatem",StringUtils.noNull(pdatem));				
		obj.put("work",StringUtils.noNull(work));				
		obj.put("cmak1",StringUtils.noNull(cmak1));				
		obj.put("percon",StringUtils.noNull(percon));				
		obj.put("condate",StringUtils.noNull(condate));				
		obj.put("ddatecon",StringUtils.noNull(ddatecon));				
		obj.put("confdeal",StringUtils.noNull(confdeal));				
		obj.put("dealcon",StringUtils.noNull(dealcon));				
		obj.put("mainuser",StringUtils.noNull(mainuser));				
		obj.put("pdcmaker",StringUtils.noNull(pdcmaker));				
		obj.put("vstford",StringUtils.noNull(vstford));				
		obj.put("qrot",StringUtils.noNull(qrot));				
		obj.put("waitdlr",StringUtils.noNull(waitdlr));				
		obj.put("effund",StringUtils.noNull(effund));				
		obj.put("ictstdr",StringUtils.noNull(ictstdr));				
		obj.put("tdrdlr",StringUtils.noNull(tdrdlr));				
		obj.put("tstrout",StringUtils.noNull(tstrout));				
		obj.put("tstcar",StringUtils.noNull(tstcar));				
		obj.put("tdover",StringUtils.noNull(tdover));				
		obj.put("ftsafety",StringUtils.noNull(ftsafety));				
		obj.put("ftconv",StringUtils.noNull(ftconv));				
		obj.put("ftperf",StringUtils.noNull(ftperf));				
		obj.put("ftcost",StringUtils.noNull(ftcost));				
		obj.put("spprovd",StringUtils.noNull(spprovd));				
		obj.put("spcall",StringUtils.noNull(spcall));				
		obj.put("sigreet",StringUtils.noNull(sigreet));				
		obj.put("siexpln",StringUtils.noNull(siexpln));				
		obj.put("sidemo",StringUtils.noNull(sidemo));				
		obj.put("sifllow",StringUtils.noNull(sifllow));				
		obj.put("siiover",StringUtils.noNull(siiover));				
		obj.put("dfseat",StringUtils.noNull(dfseat));				
		obj.put("dfpark",StringUtils.noNull(dfpark));				
		obj.put("dflit",StringUtils.noNull(dflit));				
		obj.put("dfclean",StringUtils.noNull(dfclean));				
		obj.put("dftemp",StringUtils.noNull(dftemp));				
		obj.put("dfref",StringUtils.noNull(dfref));				
		obj.put("dfsmell",StringUtils.noNull(dfsmell));				
		obj.put("dfsignge",StringUtils.noNull(dfsignge));				
		obj.put("dfrtrm",StringUtils.noNull(dfrtrm));				
		obj.put("dfavshw",StringUtils.noNull(dfavshw));				
		obj.put("dlrlyot",StringUtils.noNull(dlrlyot));				
		obj.put("dlrcfof",StringUtils.noNull(dlrcfof));				
		obj.put("dlreslk",StringUtils.noNull(dlreslk));				
		obj.put("dlrsov",StringUtils.noNull(dlrsov));				
		obj.put("dlrloc",StringUtils.noNull(dlrloc));				
		obj.put("dlrover",StringUtils.noNull(dlrover));				
		obj.put("vehfncd",StringUtils.noNull(vehfncd));				
		obj.put("dlrfnc",StringUtils.noNull(dlrfnc));				
		obj.put("condeal",StringUtils.noNull(condeal));				
		obj.put("dlease",StringUtils.noNull(dlease));				
		obj.put("dlpexp",StringUtils.noNull(dlpexp));				
		obj.put("dlqkcom",StringUtils.noNull(dlqkcom));				
		obj.put("dlprcex",StringUtils.noNull(dlprcex));				
		obj.put("dldover",StringUtils.noNull(dldover));				
		obj.put("icgroom",StringUtils.noNull(icgroom));				
		obj.put("icneatly",StringUtils.noNull(icneatly));				
		obj.put("icspent",StringUtils.noNull(icspent));				
		obj.put("icaskuse",StringUtils.noNull(icaskuse));				
		obj.put("icinform",StringUtils.noNull(icinform));				
		obj.put("icstatus",StringUtils.noNull(icstatus));				
		obj.put("spkldge",StringUtils.noNull(spkldge));				
		obj.put("spneed",StringUtils.noNull(spneed));				
		obj.put("spcrtsy",StringUtils.noNull(spcrtsy));				
		obj.put("spprssr",StringUtils.noNull(spprssr));				
		obj.put("spcommt",StringUtils.noNull(spcommt));				
		obj.put("spover",StringUtils.noNull(spover));				
		obj.put("icsame",StringUtils.noNull(icsame));				
		obj.put("icday",StringUtils.noNull(icday));				
		obj.put("dtvsexp",StringUtils.noNull(dtvsexp));				
		obj.put("dldelct",StringUtils.noNull(dldelct));				
		obj.put("dtlen",StringUtils.noNull(dtlen));				
		obj.put("dtstatus",StringUtils.noNull(dtstatus));				
		obj.put("dttime",StringUtils.noNull(dttime));				
		obj.put("dtover",StringUtils.noNull(dtover));				
		obj.put("afvehft",StringUtils.noNull(afvehft));				
		obj.put("afope",StringUtils.noNull(afope));				
		obj.put("afwarr",StringUtils.noNull(afwarr));				
		obj.put("afmaint",StringUtils.noNull(afmaint));				
		obj.put("dlhelp",StringUtils.noNull(dlhelp));				
		obj.put("dlintro",StringUtils.noNull(dlintro));				
		obj.put("dlenftme",StringUtils.noNull(dlenftme));				
		obj.put("dlthank",StringUtils.noNull(dlthank));				
		obj.put("dlramtf",StringUtils.noNull(dlramtf));				
		obj.put("dlspcere",StringUtils.noNull(dlspcere));				
		obj.put("dlvisit",StringUtils.noNull(dlvisit));				
		obj.put("dlext",StringUtils.noNull(dlext));				
		obj.put("dlfeat",StringUtils.noNull(dlfeat));				
		obj.put("dlans",StringUtils.noNull(dlans));				
		obj.put("dlfeebk",StringUtils.noNull(dlfeebk));				
		obj.put("dlconta",StringUtils.noNull(dlconta));				
		obj.put("dlappt",StringUtils.noNull(dlappt));				
		obj.put("dldelpt",StringUtils.noNull(dldelpt));				
		obj.put("delhr",StringUtils.noNull(delhr));				
		obj.put("delmin",StringUtils.noNull(delmin));				
		obj.put("delcar",StringUtils.noNull(delcar));				
		obj.put("dpconvh",StringUtils.noNull(dpconvh));				
		obj.put("dpenthu",StringUtils.noNull(dpenthu));				
		obj.put("dptime",StringUtils.noNull(dptime));				
		obj.put("dpans",StringUtils.noNull(dpans));				
		obj.put("dpthor",StringUtils.noNull(dpthor));				
		obj.put("dpover",StringUtils.noNull(dpover));				
		obj.put("ovover",StringUtils.noNull(ovover));				
		obj.put("prbselct",StringUtils.noNull(prbselct));				
		obj.put("prbstaf",StringUtils.noNull(prbstaf));				
		obj.put("prbchrge",StringUtils.noNull(prbchrge));				
		obj.put("prbanswr",StringUtils.noNull(prbanswr));				
		obj.put("prbshff",StringUtils.noNull(prbshff));				
		obj.put("prbphony",StringUtils.noNull(prbphony));				
		obj.put("recodlr",StringUtils.noNull(recodlr));				
		obj.put("psamedlr",StringUtils.noNull(psamedlr));				
		obj.put("recomake",StringUtils.noNull(recomake));				
		obj.put("reprmake",StringUtils.noNull(reprmake));				
		obj.put("ecdlr",StringUtils.noNull(ecdlr));				
		obj.put("ecveh",StringUtils.noNull(ecveh));				
		obj.put("firstveh",StringUtils.noNull(firstveh));				
		obj.put("newveh",StringUtils.noNull(newveh));				
		obj.put("jdpres",StringUtils.noNull(jdpres));				
		obj.put("jdpinf",StringUtils.noNull(jdpinf));				
		obj.put("fc",StringUtils.noNull(fc));				
		obj.put("reveal",StringUtils.noNull(reveal));				
		obj.put("provfb",StringUtils.noNull(provfb));				
		obj.put("gender",StringUtils.noNull(gender));				
		obj.put("dtmn",StringUtils.noNull(dtmn));				
		obj.put("intid",StringUtils.noNull(intid));				
		obj.put("intgen",StringUtils.noNull(intgen));				
		obj.put("lmn",StringUtils.noNull(lmn));				
		obj.put("intme",StringUtils.noNull(intme));				
		obj.put("txtoestart",StringUtils.noNull(txtoestart));				
		obj.put("txtintid",StringUtils.noNull(txtintid));				
		obj.put("txtoeend",StringUtils.noNull(txtoeend));				
		obj.put("scity",StringUtils.noNull(scity));				
		obj.put("intday",StringUtils.noNull(intday));				
		obj.put("intmth",StringUtils.noNull(intmth));				
		obj.put("intdate",StringUtils.noNull(intdate));				
		obj.put("dbstart",StringUtils.noNull(dbstart));				
		obj.put("dbcustomername",StringUtils.noNull(dbcustomername));				
		obj.put("dbcustomercity",StringUtils.noNull(dbcustomercity));				
		obj.put("dbcustomerstate",StringUtils.noNull(dbcustomerstate));				
		obj.put("dbmobile",StringUtils.noNull(dbmobile));				
		obj.put("dbtel1",StringUtils.noNull(dbtel1));				
		obj.put("dbtel2",StringUtils.noNull(dbtel2));				
		obj.put("dbmodel",StringUtils.noNull(dbmodel));				
		obj.put("dbmodelvarient",StringUtils.noNull(dbmodelvarient));				
		obj.put("dbfueltype",StringUtils.noNull(dbfueltype));				
		obj.put("dbvin",StringUtils.noNull(dbvin));				
		obj.put("dbdealercode",StringUtils.noNull(dbdealercode));				
		obj.put("dbdealername",StringUtils.noNull(dbdealername));				
		obj.put("dbdealercity",StringUtils.noNull(dbdealercity));				
		obj.put("dbdealerstate",StringUtils.noNull(dbdealerstate));				
		obj.put("dbdeliverydate",StringUtils.noNull(dbdeliverydate));				
		obj.put("dbsalesconsultantcode",StringUtils.noNull(dbsalesconsultantcode));				
		obj.put("dbsalesconsultantname",StringUtils.noNull(dbsalesconsultantname));				
		obj.put("dbpandacode",StringUtils.noNull(dbpandacode));				
		obj.put("dbdealer2",StringUtils.noNull(dbdealer2));				
		obj.put("dbdealer2labels",StringUtils.noNull(dbdealer2labels));				
		obj.put("dbquarternum",StringUtils.noNull(dbquarternum));				
		obj.put("dbbatchnum",StringUtils.noNull(dbbatchnum));				
		obj.put("dbend",StringUtils.noNull(dbend));				
		obj.put("fdstart",StringUtils.noNull(fdstart));				
		obj.put("dealer",StringUtils.noNull(dealer));				
		obj.put("dealer2",StringUtils.noNull(dealer2));				
		obj.put("city2",StringUtils.noNull(city2));				
		obj.put("regiona",StringUtils.noNull(regiona));				
		obj.put("state2",StringUtils.noNull(state2));				
		obj.put("model",StringUtils.noNull(model));				
		obj.put("model2",StringUtils.noNull(model2));				
		obj.put("city3",StringUtils.noNull(city3));				
		obj.put("fdend",StringUtils.noNull(fdend));				
		obj.put("indexoutlierfilter",StringUtils.noNull(indexoutlierfilter));				
		obj.put("irecodestart",StringUtils.noNull(irecodestart));				
		obj.put("zdelhr",StringUtils.noNull(zdelhr));				
		obj.put("zdelmin",StringUtils.noNull(zdelmin));				
		obj.put("cdelmin",StringUtils.noNull(cdelmin));				
		obj.put("ncicday",StringUtils.noNull(ncicday));				
		obj.put("ncdelmin",StringUtils.noNull(ncdelmin));				
		obj.put("irecodeend",StringUtils.noNull(irecodeend));				
		obj.put("recodestart",StringUtils.noNull(recodestart));				
		obj.put("ntdover",StringUtils.noNull(ntdover));				
		obj.put("nsigreet",StringUtils.noNull(nsigreet));				
		obj.put("nsiexpln",StringUtils.noNull(nsiexpln));				
		obj.put("nsidemo",StringUtils.noNull(nsidemo));				
		obj.put("nsifllow",StringUtils.noNull(nsifllow));				
		obj.put("nsiiover",StringUtils.noNull(nsiiover));				
		obj.put("ndlrlyot",StringUtils.noNull(ndlrlyot));				
		obj.put("ndlrcfof",StringUtils.noNull(ndlrcfof));				
		obj.put("ndlreslk",StringUtils.noNull(ndlreslk));				
		obj.put("ndlrsov",StringUtils.noNull(ndlrsov));				
		obj.put("ndlrloc",StringUtils.noNull(ndlrloc));				
		obj.put("ndlrover",StringUtils.noNull(ndlrover));				
		obj.put("ndlease",StringUtils.noNull(ndlease));				
		obj.put("ndlpexp",StringUtils.noNull(ndlpexp));				
		obj.put("ndlqkcom",StringUtils.noNull(ndlqkcom));				
		obj.put("ndlprcex",StringUtils.noNull(ndlprcex));				
		obj.put("ndldover",StringUtils.noNull(ndldover));				
		obj.put("nspkldge",StringUtils.noNull(nspkldge));				
		obj.put("nspneed",StringUtils.noNull(nspneed));				
		obj.put("nspcrtsy",StringUtils.noNull(nspcrtsy));				
		obj.put("nspprssr",StringUtils.noNull(nspprssr));				
		obj.put("nspcommt",StringUtils.noNull(nspcommt));				
		obj.put("nspover",StringUtils.noNull(nspover));				
		obj.put("ndtlen",StringUtils.noNull(ndtlen));				
		obj.put("ndtstatus",StringUtils.noNull(ndtstatus));				
		obj.put("ndttime",StringUtils.noNull(ndttime));				
		obj.put("ndtover",StringUtils.noNull(ndtover));				
		obj.put("ndpconvh",StringUtils.noNull(ndpconvh));				
		obj.put("ndpenthu",StringUtils.noNull(ndpenthu));				
		obj.put("ndptime",StringUtils.noNull(ndptime));				
		obj.put("ndpans",StringUtils.noNull(ndpans));				
		obj.put("ndpthor",StringUtils.noNull(ndpthor));				
		obj.put("ndpover",StringUtils.noNull(ndpover));				
		obj.put("novover",StringUtils.noNull(novover));				
		obj.put("ttdover",StringUtils.noNull(ttdover));				
		obj.put("tsigreet",StringUtils.noNull(tsigreet));				
		obj.put("tsiexpln",StringUtils.noNull(tsiexpln));				
		obj.put("tsidemo",StringUtils.noNull(tsidemo));				
		obj.put("tsifllow",StringUtils.noNull(tsifllow));				
		obj.put("tsiiover",StringUtils.noNull(tsiiover));				
		obj.put("tdlrlyot",StringUtils.noNull(tdlrlyot));				
		obj.put("tdlrcfof",StringUtils.noNull(tdlrcfof));				
		obj.put("tdlreslk",StringUtils.noNull(tdlreslk));				
		obj.put("tdlrsov",StringUtils.noNull(tdlrsov));				
		obj.put("tdlrloc",StringUtils.noNull(tdlrloc));				
		obj.put("tdlrover",StringUtils.noNull(tdlrover));				
		obj.put("tdlease",StringUtils.noNull(tdlease));				
		obj.put("tdlpexp",StringUtils.noNull(tdlpexp));				
		obj.put("tdlqkcom",StringUtils.noNull(tdlqkcom));				
		obj.put("tdlprcex",StringUtils.noNull(tdlprcex));				
		obj.put("tdldover",StringUtils.noNull(tdldover));				
		obj.put("tspkldge",StringUtils.noNull(tspkldge));				
		obj.put("tspneed",StringUtils.noNull(tspneed));				
		obj.put("tspcrtsy",StringUtils.noNull(tspcrtsy));				
		obj.put("tspprssr",StringUtils.noNull(tspprssr));				
		obj.put("tspcommt",StringUtils.noNull(tspcommt));				
		obj.put("tspover",StringUtils.noNull(tspover));				
		obj.put("tdtlen",StringUtils.noNull(tdtlen));				
		obj.put("tdtstatus",StringUtils.noNull(tdtstatus));				
		obj.put("tdttime",StringUtils.noNull(tdttime));				
		obj.put("tdtover",StringUtils.noNull(tdtover));				
		obj.put("tdpconvh",StringUtils.noNull(tdpconvh));				
		obj.put("tdpenthu",StringUtils.noNull(tdpenthu));				
		obj.put("tdptime",StringUtils.noNull(tdptime));				
		obj.put("tdpans",StringUtils.noNull(tdpans));				
		obj.put("tdpthor",StringUtils.noNull(tdpthor));				
		obj.put("tdpover",StringUtils.noNull(tdpover));				
		obj.put("tovover",StringUtils.noNull(tovover));				
		obj.put("atdover",StringUtils.noNull(atdover));				
		obj.put("asigreet",StringUtils.noNull(asigreet));				
		obj.put("asiexpln",StringUtils.noNull(asiexpln));				
		obj.put("asidemo",StringUtils.noNull(asidemo));				
		obj.put("asifllow",StringUtils.noNull(asifllow));				
		obj.put("asiiover",StringUtils.noNull(asiiover));				
		obj.put("adlrlyot",StringUtils.noNull(adlrlyot));				
		obj.put("adlrcfof",StringUtils.noNull(adlrcfof));				
		obj.put("adlreslk",StringUtils.noNull(adlreslk));				
		obj.put("adlrsov",StringUtils.noNull(adlrsov));				
		obj.put("adlrloc",StringUtils.noNull(adlrloc));				
		obj.put("adlrover",StringUtils.noNull(adlrover));				
		obj.put("adlease",StringUtils.noNull(adlease));				
		obj.put("adlpexp",StringUtils.noNull(adlpexp));				
		obj.put("adlqkcom",StringUtils.noNull(adlqkcom));				
		obj.put("adlprcex",StringUtils.noNull(adlprcex));				
		obj.put("adldover",StringUtils.noNull(adldover));				
		obj.put("aspkldge",StringUtils.noNull(aspkldge));				
		obj.put("aspneed",StringUtils.noNull(aspneed));				
		obj.put("aspcrtsy",StringUtils.noNull(aspcrtsy));				
		obj.put("aspprssr",StringUtils.noNull(aspprssr));				
		obj.put("aspcommt",StringUtils.noNull(aspcommt));				
		obj.put("aspover",StringUtils.noNull(aspover));				
		obj.put("adtlen",StringUtils.noNull(adtlen));				
		obj.put("adtstatus",StringUtils.noNull(adtstatus));				
		obj.put("adttime",StringUtils.noNull(adttime));				
		obj.put("adtover",StringUtils.noNull(adtover));				
		obj.put("adpconvh",StringUtils.noNull(adpconvh));				
		obj.put("adpenthu",StringUtils.noNull(adpenthu));				
		obj.put("adptime",StringUtils.noNull(adptime));				
		obj.put("adpans",StringUtils.noNull(adpans));				
		obj.put("adpthor",StringUtils.noNull(adpthor));				
		obj.put("adpover",StringUtils.noNull(adpover));				
		obj.put("aovover",StringUtils.noNull(aovover));				
		obj.put("prb",StringUtils.noNull(prb));				
		obj.put("nprb",StringUtils.noNull(nprb));				
		obj.put("nnprb",StringUtils.noNull(nnprb));				
		obj.put("cntft",StringUtils.noNull(cntft));				
		obj.put("ncntft",StringUtils.noNull(ncntft));				
		obj.put("cntexp",StringUtils.noNull(cntexp));				
		obj.put("ncntexp",StringUtils.noNull(ncntexp));				
		obj.put("sop",StringUtils.noNull(sop));				
		obj.put("nsop",StringUtils.noNull(nsop));				
		obj.put("comsop",StringUtils.noNull(comsop));				
		obj.put("ncomsop",StringUtils.noNull(ncomsop));				
		obj.put("prosop",StringUtils.noNull(prosop));				
		obj.put("nprosop",StringUtils.noNull(nprosop));				
		obj.put("recodeend",StringUtils.noNull(recodeend));				
		obj.put("index",StringUtils.noNull(index));				
		obj.put("si",StringUtils.noNull(si));				
		obj.put("df",StringUtils.noNull(df));				
		obj.put("deal",StringUtils.noNull(deal));				
		obj.put("sp",StringUtils.noNull(sp));				
		obj.put("dt",StringUtils.noNull(dt));				
		obj.put("dp",StringUtils.noNull(dp));				
		obj.put("nindex",StringUtils.noNull(nindex));				
		obj.put("biweekly",StringUtils.noNull(biweekly));				
		obj.put("month",StringUtils.noNull(month));				
		obj.put("quarter",StringUtils.noNull(quarter));				
		obj.put("year",StringUtils.noNull(year));
		return obj;
	}

	public void loadJSONObject(JSONObject obj)
	throws Exception
	{
		if (obj == null) return;

		srno = StringUtils.getValueFromJSONObject(obj, "srno");				
		dbsrno = StringUtils.getValueFromJSONObject(obj, "dbsrno");				
		track = StringUtils.getValueFromJSONObject(obj, "track");				
		lstned = StringUtils.getValueFromJSONObject(obj, "lstned");				
		qcdone = StringUtils.getValueFromJSONObject(obj, "qcdone");				
		tapeadt = StringUtils.getValueFromJSONObject(obj, "tapeadt");				
		extaudi = StringUtils.getValueFromJSONObject(obj, "extaudi");				
		pdatey = StringUtils.getValueFromJSONObject(obj, "pdatey");				
		pdatem = StringUtils.getValueFromJSONObject(obj, "pdatem");				
		work = StringUtils.getValueFromJSONObject(obj, "work");				
		cmak1 = StringUtils.getValueFromJSONObject(obj, "cmak1");				
		percon = StringUtils.getValueFromJSONObject(obj, "percon");				
		condate = StringUtils.getValueFromJSONObject(obj, "condate");				
		ddatecon = StringUtils.getValueFromJSONObject(obj, "ddatecon");				
		confdeal = StringUtils.getValueFromJSONObject(obj, "confdeal");				
		dealcon = StringUtils.getValueFromJSONObject(obj, "dealcon");				
		mainuser = StringUtils.getValueFromJSONObject(obj, "mainuser");				
		pdcmaker = StringUtils.getValueFromJSONObject(obj, "pdcmaker");				
		vstford = StringUtils.getValueFromJSONObject(obj, "vstford");				
		qrot = StringUtils.getValueFromJSONObject(obj, "qrot");				
		waitdlr = StringUtils.getValueFromJSONObject(obj, "waitdlr");				
		effund = StringUtils.getValueFromJSONObject(obj, "effund");				
		ictstdr = StringUtils.getValueFromJSONObject(obj, "ictstdr");				
		tdrdlr = StringUtils.getValueFromJSONObject(obj, "tdrdlr");				
		tstrout = StringUtils.getValueFromJSONObject(obj, "tstrout");				
		tstcar = StringUtils.getValueFromJSONObject(obj, "tstcar");				
		tdover = StringUtils.getValueFromJSONObject(obj, "tdover");				
		ftsafety = StringUtils.getValueFromJSONObject(obj, "ftsafety");				
		ftconv = StringUtils.getValueFromJSONObject(obj, "ftconv");				
		ftperf = StringUtils.getValueFromJSONObject(obj, "ftperf");				
		ftcost = StringUtils.getValueFromJSONObject(obj, "ftcost");				
		spprovd = StringUtils.getValueFromJSONObject(obj, "spprovd");				
		spcall = StringUtils.getValueFromJSONObject(obj, "spcall");				
		sigreet = StringUtils.getValueFromJSONObject(obj, "sigreet");				
		siexpln = StringUtils.getValueFromJSONObject(obj, "siexpln");				
		sidemo = StringUtils.getValueFromJSONObject(obj, "sidemo");				
		sifllow = StringUtils.getValueFromJSONObject(obj, "sifllow");				
		siiover = StringUtils.getValueFromJSONObject(obj, "siiover");				
		dfseat = StringUtils.getValueFromJSONObject(obj, "dfseat");				
		dfpark = StringUtils.getValueFromJSONObject(obj, "dfpark");				
		dflit = StringUtils.getValueFromJSONObject(obj, "dflit");				
		dfclean = StringUtils.getValueFromJSONObject(obj, "dfclean");				
		dftemp = StringUtils.getValueFromJSONObject(obj, "dftemp");				
		dfref = StringUtils.getValueFromJSONObject(obj, "dfref");				
		dfsmell = StringUtils.getValueFromJSONObject(obj, "dfsmell");				
		dfsignge = StringUtils.getValueFromJSONObject(obj, "dfsignge");				
		dfrtrm = StringUtils.getValueFromJSONObject(obj, "dfrtrm");				
		dfavshw = StringUtils.getValueFromJSONObject(obj, "dfavshw");				
		dlrlyot = StringUtils.getValueFromJSONObject(obj, "dlrlyot");				
		dlrcfof = StringUtils.getValueFromJSONObject(obj, "dlrcfof");				
		dlreslk = StringUtils.getValueFromJSONObject(obj, "dlreslk");				
		dlrsov = StringUtils.getValueFromJSONObject(obj, "dlrsov");				
		dlrloc = StringUtils.getValueFromJSONObject(obj, "dlrloc");				
		dlrover = StringUtils.getValueFromJSONObject(obj, "dlrover");				
		vehfncd = StringUtils.getValueFromJSONObject(obj, "vehfncd");				
		dlrfnc = StringUtils.getValueFromJSONObject(obj, "dlrfnc");				
		condeal = StringUtils.getValueFromJSONObject(obj, "condeal");				
		dlease = StringUtils.getValueFromJSONObject(obj, "dlease");				
		dlpexp = StringUtils.getValueFromJSONObject(obj, "dlpexp");				
		dlqkcom = StringUtils.getValueFromJSONObject(obj, "dlqkcom");				
		dlprcex = StringUtils.getValueFromJSONObject(obj, "dlprcex");				
		dldover = StringUtils.getValueFromJSONObject(obj, "dldover");				
		icgroom = StringUtils.getValueFromJSONObject(obj, "icgroom");				
		icneatly = StringUtils.getValueFromJSONObject(obj, "icneatly");				
		icspent = StringUtils.getValueFromJSONObject(obj, "icspent");				
		icaskuse = StringUtils.getValueFromJSONObject(obj, "icaskuse");				
		icinform = StringUtils.getValueFromJSONObject(obj, "icinform");				
		icstatus = StringUtils.getValueFromJSONObject(obj, "icstatus");				
		spkldge = StringUtils.getValueFromJSONObject(obj, "spkldge");				
		spneed = StringUtils.getValueFromJSONObject(obj, "spneed");				
		spcrtsy = StringUtils.getValueFromJSONObject(obj, "spcrtsy");				
		spprssr = StringUtils.getValueFromJSONObject(obj, "spprssr");				
		spcommt = StringUtils.getValueFromJSONObject(obj, "spcommt");				
		spover = StringUtils.getValueFromJSONObject(obj, "spover");				
		icsame = StringUtils.getValueFromJSONObject(obj, "icsame");				
		icday = StringUtils.getValueFromJSONObject(obj, "icday");				
		dtvsexp = StringUtils.getValueFromJSONObject(obj, "dtvsexp");				
		dldelct = StringUtils.getValueFromJSONObject(obj, "dldelct");				
		dtlen = StringUtils.getValueFromJSONObject(obj, "dtlen");				
		dtstatus = StringUtils.getValueFromJSONObject(obj, "dtstatus");				
		dttime = StringUtils.getValueFromJSONObject(obj, "dttime");				
		dtover = StringUtils.getValueFromJSONObject(obj, "dtover");				
		afvehft = StringUtils.getValueFromJSONObject(obj, "afvehft");				
		afope = StringUtils.getValueFromJSONObject(obj, "afope");				
		afwarr = StringUtils.getValueFromJSONObject(obj, "afwarr");				
		afmaint = StringUtils.getValueFromJSONObject(obj, "afmaint");				
		dlhelp = StringUtils.getValueFromJSONObject(obj, "dlhelp");				
		dlintro = StringUtils.getValueFromJSONObject(obj, "dlintro");				
		dlenftme = StringUtils.getValueFromJSONObject(obj, "dlenftme");				
		dlthank = StringUtils.getValueFromJSONObject(obj, "dlthank");				
		dlramtf = StringUtils.getValueFromJSONObject(obj, "dlramtf");				
		dlspcere = StringUtils.getValueFromJSONObject(obj, "dlspcere");				
		dlvisit = StringUtils.getValueFromJSONObject(obj, "dlvisit");				
		dlext = StringUtils.getValueFromJSONObject(obj, "dlext");				
		dlfeat = StringUtils.getValueFromJSONObject(obj, "dlfeat");				
		dlans = StringUtils.getValueFromJSONObject(obj, "dlans");				
		dlfeebk = StringUtils.getValueFromJSONObject(obj, "dlfeebk");				
		dlconta = StringUtils.getValueFromJSONObject(obj, "dlconta");				
		dlappt = StringUtils.getValueFromJSONObject(obj, "dlappt");				
		dldelpt = StringUtils.getValueFromJSONObject(obj, "dldelpt");				
		delhr = StringUtils.getValueFromJSONObject(obj, "delhr");				
		delmin = StringUtils.getValueFromJSONObject(obj, "delmin");				
		delcar = StringUtils.getValueFromJSONObject(obj, "delcar");				
		dpconvh = StringUtils.getValueFromJSONObject(obj, "dpconvh");				
		dpenthu = StringUtils.getValueFromJSONObject(obj, "dpenthu");				
		dptime = StringUtils.getValueFromJSONObject(obj, "dptime");				
		dpans = StringUtils.getValueFromJSONObject(obj, "dpans");				
		dpthor = StringUtils.getValueFromJSONObject(obj, "dpthor");				
		dpover = StringUtils.getValueFromJSONObject(obj, "dpover");				
		ovover = StringUtils.getValueFromJSONObject(obj, "ovover");				
		prbselct = StringUtils.getValueFromJSONObject(obj, "prbselct");				
		prbstaf = StringUtils.getValueFromJSONObject(obj, "prbstaf");				
		prbchrge = StringUtils.getValueFromJSONObject(obj, "prbchrge");				
		prbanswr = StringUtils.getValueFromJSONObject(obj, "prbanswr");				
		prbshff = StringUtils.getValueFromJSONObject(obj, "prbshff");				
		prbphony = StringUtils.getValueFromJSONObject(obj, "prbphony");				
		recodlr = StringUtils.getValueFromJSONObject(obj, "recodlr");				
		psamedlr = StringUtils.getValueFromJSONObject(obj, "psamedlr");				
		recomake = StringUtils.getValueFromJSONObject(obj, "recomake");				
		reprmake = StringUtils.getValueFromJSONObject(obj, "reprmake");				
		ecdlr = StringUtils.getValueFromJSONObject(obj, "ecdlr");				
		ecveh = StringUtils.getValueFromJSONObject(obj, "ecveh");				
		firstveh = StringUtils.getValueFromJSONObject(obj, "firstveh");				
		newveh = StringUtils.getValueFromJSONObject(obj, "newveh");				
		jdpres = StringUtils.getValueFromJSONObject(obj, "jdpres");				
		jdpinf = StringUtils.getValueFromJSONObject(obj, "jdpinf");				
		fc = StringUtils.getValueFromJSONObject(obj, "fc");				
		reveal = StringUtils.getValueFromJSONObject(obj, "reveal");				
		provfb = StringUtils.getValueFromJSONObject(obj, "provfb");				
		gender = StringUtils.getValueFromJSONObject(obj, "gender");				
		dtmn = StringUtils.getValueFromJSONObject(obj, "dtmn");				
		intid = StringUtils.getValueFromJSONObject(obj, "intid");				
		intgen = StringUtils.getValueFromJSONObject(obj, "intgen");				
		lmn = StringUtils.getValueFromJSONObject(obj, "lmn");				
		intme = StringUtils.getValueFromJSONObject(obj, "intme");				
		txtoestart = StringUtils.getValueFromJSONObject(obj, "txtoestart");				
		txtintid = StringUtils.getValueFromJSONObject(obj, "txtintid");				
		txtoeend = StringUtils.getValueFromJSONObject(obj, "txtoeend");				
		scity = StringUtils.getValueFromJSONObject(obj, "scity");				
		intday = StringUtils.getValueFromJSONObject(obj, "intday");				
		intmth = StringUtils.getValueFromJSONObject(obj, "intmth");				
		intdate = StringUtils.getValueFromJSONObject(obj, "intdate");				
		dbstart = StringUtils.getValueFromJSONObject(obj, "dbstart");				
		dbcustomername = StringUtils.getValueFromJSONObject(obj, "dbcustomername");				
		dbcustomercity = StringUtils.getValueFromJSONObject(obj, "dbcustomercity");				
		dbcustomerstate = StringUtils.getValueFromJSONObject(obj, "dbcustomerstate");				
		dbmobile = StringUtils.getValueFromJSONObject(obj, "dbmobile");				
		dbtel1 = StringUtils.getValueFromJSONObject(obj, "dbtel1");				
		dbtel2 = StringUtils.getValueFromJSONObject(obj, "dbtel2");				
		dbmodel = StringUtils.getValueFromJSONObject(obj, "dbmodel");				
		dbmodelvarient = StringUtils.getValueFromJSONObject(obj, "dbmodelvarient");				
		dbfueltype = StringUtils.getValueFromJSONObject(obj, "dbfueltype");				
		dbvin = StringUtils.getValueFromJSONObject(obj, "dbvin");				
		dbdealercode = StringUtils.getValueFromJSONObject(obj, "dbdealercode");				
		dbdealername = StringUtils.getValueFromJSONObject(obj, "dbdealername");				
		dbdealercity = StringUtils.getValueFromJSONObject(obj, "dbdealercity");				
		dbdealerstate = StringUtils.getValueFromJSONObject(obj, "dbdealerstate");				
		dbdeliverydate = StringUtils.getValueFromJSONObject(obj, "dbdeliverydate");				
		dbsalesconsultantcode = StringUtils.getValueFromJSONObject(obj, "dbsalesconsultantcode");				
		dbsalesconsultantname = StringUtils.getValueFromJSONObject(obj, "dbsalesconsultantname");				
		dbpandacode = StringUtils.getValueFromJSONObject(obj, "dbpandacode");				
		dbdealer2 = StringUtils.getValueFromJSONObject(obj, "dbdealer2");				
		dbdealer2labels = StringUtils.getValueFromJSONObject(obj, "dbdealer2labels");				
		dbquarternum = StringUtils.getValueFromJSONObject(obj, "dbquarternum");				
		dbbatchnum = StringUtils.getValueFromJSONObject(obj, "dbbatchnum");				
		dbend = StringUtils.getValueFromJSONObject(obj, "dbend");				
		fdstart = StringUtils.getValueFromJSONObject(obj, "fdstart");				
		dealer = StringUtils.getValueFromJSONObject(obj, "dealer");				
		dealer2 = StringUtils.getValueFromJSONObject(obj, "dealer2");				
		city2 = StringUtils.getValueFromJSONObject(obj, "city2");				
		regiona = StringUtils.getValueFromJSONObject(obj, "regiona");				
		state2 = StringUtils.getValueFromJSONObject(obj, "state2");				
		model = StringUtils.getValueFromJSONObject(obj, "model");				
		model2 = StringUtils.getValueFromJSONObject(obj, "model2");				
		city3 = StringUtils.getValueFromJSONObject(obj, "city3");				
		fdend = StringUtils.getValueFromJSONObject(obj, "fdend");				
		indexoutlierfilter = StringUtils.getValueFromJSONObject(obj, "indexoutlierfilter");				
		irecodestart = StringUtils.getValueFromJSONObject(obj, "irecodestart");				
		zdelhr = StringUtils.getValueFromJSONObject(obj, "zdelhr");				
		zdelmin = StringUtils.getValueFromJSONObject(obj, "zdelmin");				
		cdelmin = StringUtils.getValueFromJSONObject(obj, "cdelmin");				
		ncicday = StringUtils.getValueFromJSONObject(obj, "ncicday");				
		ncdelmin = StringUtils.getValueFromJSONObject(obj, "ncdelmin");				
		irecodeend = StringUtils.getValueFromJSONObject(obj, "irecodeend");				
		recodestart = StringUtils.getValueFromJSONObject(obj, "recodestart");				
		ntdover = StringUtils.getValueFromJSONObject(obj, "ntdover");				
		nsigreet = StringUtils.getValueFromJSONObject(obj, "nsigreet");				
		nsiexpln = StringUtils.getValueFromJSONObject(obj, "nsiexpln");				
		nsidemo = StringUtils.getValueFromJSONObject(obj, "nsidemo");				
		nsifllow = StringUtils.getValueFromJSONObject(obj, "nsifllow");				
		nsiiover = StringUtils.getValueFromJSONObject(obj, "nsiiover");				
		ndlrlyot = StringUtils.getValueFromJSONObject(obj, "ndlrlyot");				
		ndlrcfof = StringUtils.getValueFromJSONObject(obj, "ndlrcfof");				
		ndlreslk = StringUtils.getValueFromJSONObject(obj, "ndlreslk");				
		ndlrsov = StringUtils.getValueFromJSONObject(obj, "ndlrsov");				
		ndlrloc = StringUtils.getValueFromJSONObject(obj, "ndlrloc");				
		ndlrover = StringUtils.getValueFromJSONObject(obj, "ndlrover");				
		ndlease = StringUtils.getValueFromJSONObject(obj, "ndlease");				
		ndlpexp = StringUtils.getValueFromJSONObject(obj, "ndlpexp");				
		ndlqkcom = StringUtils.getValueFromJSONObject(obj, "ndlqkcom");				
		ndlprcex = StringUtils.getValueFromJSONObject(obj, "ndlprcex");				
		ndldover = StringUtils.getValueFromJSONObject(obj, "ndldover");				
		nspkldge = StringUtils.getValueFromJSONObject(obj, "nspkldge");				
		nspneed = StringUtils.getValueFromJSONObject(obj, "nspneed");				
		nspcrtsy = StringUtils.getValueFromJSONObject(obj, "nspcrtsy");				
		nspprssr = StringUtils.getValueFromJSONObject(obj, "nspprssr");				
		nspcommt = StringUtils.getValueFromJSONObject(obj, "nspcommt");				
		nspover = StringUtils.getValueFromJSONObject(obj, "nspover");				
		ndtlen = StringUtils.getValueFromJSONObject(obj, "ndtlen");				
		ndtstatus = StringUtils.getValueFromJSONObject(obj, "ndtstatus");				
		ndttime = StringUtils.getValueFromJSONObject(obj, "ndttime");				
		ndtover = StringUtils.getValueFromJSONObject(obj, "ndtover");				
		ndpconvh = StringUtils.getValueFromJSONObject(obj, "ndpconvh");				
		ndpenthu = StringUtils.getValueFromJSONObject(obj, "ndpenthu");				
		ndptime = StringUtils.getValueFromJSONObject(obj, "ndptime");				
		ndpans = StringUtils.getValueFromJSONObject(obj, "ndpans");				
		ndpthor = StringUtils.getValueFromJSONObject(obj, "ndpthor");				
		ndpover = StringUtils.getValueFromJSONObject(obj, "ndpover");				
		novover = StringUtils.getValueFromJSONObject(obj, "novover");				
		ttdover = StringUtils.getValueFromJSONObject(obj, "ttdover");				
		tsigreet = StringUtils.getValueFromJSONObject(obj, "tsigreet");				
		tsiexpln = StringUtils.getValueFromJSONObject(obj, "tsiexpln");				
		tsidemo = StringUtils.getValueFromJSONObject(obj, "tsidemo");				
		tsifllow = StringUtils.getValueFromJSONObject(obj, "tsifllow");				
		tsiiover = StringUtils.getValueFromJSONObject(obj, "tsiiover");				
		tdlrlyot = StringUtils.getValueFromJSONObject(obj, "tdlrlyot");				
		tdlrcfof = StringUtils.getValueFromJSONObject(obj, "tdlrcfof");				
		tdlreslk = StringUtils.getValueFromJSONObject(obj, "tdlreslk");				
		tdlrsov = StringUtils.getValueFromJSONObject(obj, "tdlrsov");				
		tdlrloc = StringUtils.getValueFromJSONObject(obj, "tdlrloc");				
		tdlrover = StringUtils.getValueFromJSONObject(obj, "tdlrover");				
		tdlease = StringUtils.getValueFromJSONObject(obj, "tdlease");				
		tdlpexp = StringUtils.getValueFromJSONObject(obj, "tdlpexp");				
		tdlqkcom = StringUtils.getValueFromJSONObject(obj, "tdlqkcom");				
		tdlprcex = StringUtils.getValueFromJSONObject(obj, "tdlprcex");				
		tdldover = StringUtils.getValueFromJSONObject(obj, "tdldover");				
		tspkldge = StringUtils.getValueFromJSONObject(obj, "tspkldge");				
		tspneed = StringUtils.getValueFromJSONObject(obj, "tspneed");				
		tspcrtsy = StringUtils.getValueFromJSONObject(obj, "tspcrtsy");				
		tspprssr = StringUtils.getValueFromJSONObject(obj, "tspprssr");				
		tspcommt = StringUtils.getValueFromJSONObject(obj, "tspcommt");				
		tspover = StringUtils.getValueFromJSONObject(obj, "tspover");				
		tdtlen = StringUtils.getValueFromJSONObject(obj, "tdtlen");				
		tdtstatus = StringUtils.getValueFromJSONObject(obj, "tdtstatus");				
		tdttime = StringUtils.getValueFromJSONObject(obj, "tdttime");				
		tdtover = StringUtils.getValueFromJSONObject(obj, "tdtover");				
		tdpconvh = StringUtils.getValueFromJSONObject(obj, "tdpconvh");				
		tdpenthu = StringUtils.getValueFromJSONObject(obj, "tdpenthu");				
		tdptime = StringUtils.getValueFromJSONObject(obj, "tdptime");				
		tdpans = StringUtils.getValueFromJSONObject(obj, "tdpans");				
		tdpthor = StringUtils.getValueFromJSONObject(obj, "tdpthor");				
		tdpover = StringUtils.getValueFromJSONObject(obj, "tdpover");				
		tovover = StringUtils.getValueFromJSONObject(obj, "tovover");				
		atdover = StringUtils.getValueFromJSONObject(obj, "atdover");				
		asigreet = StringUtils.getValueFromJSONObject(obj, "asigreet");				
		asiexpln = StringUtils.getValueFromJSONObject(obj, "asiexpln");				
		asidemo = StringUtils.getValueFromJSONObject(obj, "asidemo");				
		asifllow = StringUtils.getValueFromJSONObject(obj, "asifllow");				
		asiiover = StringUtils.getValueFromJSONObject(obj, "asiiover");				
		adlrlyot = StringUtils.getValueFromJSONObject(obj, "adlrlyot");				
		adlrcfof = StringUtils.getValueFromJSONObject(obj, "adlrcfof");				
		adlreslk = StringUtils.getValueFromJSONObject(obj, "adlreslk");				
		adlrsov = StringUtils.getValueFromJSONObject(obj, "adlrsov");				
		adlrloc = StringUtils.getValueFromJSONObject(obj, "adlrloc");				
		adlrover = StringUtils.getValueFromJSONObject(obj, "adlrover");				
		adlease = StringUtils.getValueFromJSONObject(obj, "adlease");				
		adlpexp = StringUtils.getValueFromJSONObject(obj, "adlpexp");				
		adlqkcom = StringUtils.getValueFromJSONObject(obj, "adlqkcom");				
		adlprcex = StringUtils.getValueFromJSONObject(obj, "adlprcex");				
		adldover = StringUtils.getValueFromJSONObject(obj, "adldover");				
		aspkldge = StringUtils.getValueFromJSONObject(obj, "aspkldge");				
		aspneed = StringUtils.getValueFromJSONObject(obj, "aspneed");				
		aspcrtsy = StringUtils.getValueFromJSONObject(obj, "aspcrtsy");				
		aspprssr = StringUtils.getValueFromJSONObject(obj, "aspprssr");				
		aspcommt = StringUtils.getValueFromJSONObject(obj, "aspcommt");				
		aspover = StringUtils.getValueFromJSONObject(obj, "aspover");				
		adtlen = StringUtils.getValueFromJSONObject(obj, "adtlen");				
		adtstatus = StringUtils.getValueFromJSONObject(obj, "adtstatus");				
		adttime = StringUtils.getValueFromJSONObject(obj, "adttime");				
		adtover = StringUtils.getValueFromJSONObject(obj, "adtover");				
		adpconvh = StringUtils.getValueFromJSONObject(obj, "adpconvh");				
		adpenthu = StringUtils.getValueFromJSONObject(obj, "adpenthu");				
		adptime = StringUtils.getValueFromJSONObject(obj, "adptime");				
		adpans = StringUtils.getValueFromJSONObject(obj, "adpans");				
		adpthor = StringUtils.getValueFromJSONObject(obj, "adpthor");				
		adpover = StringUtils.getValueFromJSONObject(obj, "adpover");				
		aovover = StringUtils.getValueFromJSONObject(obj, "aovover");				
		prb = StringUtils.getValueFromJSONObject(obj, "prb");				
		nprb = StringUtils.getValueFromJSONObject(obj, "nprb");				
		nnprb = StringUtils.getValueFromJSONObject(obj, "nnprb");				
		cntft = StringUtils.getValueFromJSONObject(obj, "cntft");				
		ncntft = StringUtils.getValueFromJSONObject(obj, "ncntft");				
		cntexp = StringUtils.getValueFromJSONObject(obj, "cntexp");				
		ncntexp = StringUtils.getValueFromJSONObject(obj, "ncntexp");				
		sop = StringUtils.getValueFromJSONObject(obj, "sop");				
		nsop = StringUtils.getValueFromJSONObject(obj, "nsop");				
		comsop = StringUtils.getValueFromJSONObject(obj, "comsop");				
		ncomsop = StringUtils.getValueFromJSONObject(obj, "ncomsop");				
		prosop = StringUtils.getValueFromJSONObject(obj, "prosop");				
		nprosop = StringUtils.getValueFromJSONObject(obj, "nprosop");				
		recodeend = StringUtils.getValueFromJSONObject(obj, "recodeend");				
		index = StringUtils.getValueFromJSONObject(obj, "index");				
		si = StringUtils.getValueFromJSONObject(obj, "si");				
		df = StringUtils.getValueFromJSONObject(obj, "df");				
		deal = StringUtils.getValueFromJSONObject(obj, "deal");				
		sp = StringUtils.getValueFromJSONObject(obj, "sp");				
		dt = StringUtils.getValueFromJSONObject(obj, "dt");				
		dp = StringUtils.getValueFromJSONObject(obj, "dp");				
		nindex = StringUtils.getValueFromJSONObject(obj, "nindex");				
		biweekly = StringUtils.getValueFromJSONObject(obj, "biweekly");				
		month = StringUtils.getValueFromJSONObject(obj, "month");				
		quarter = StringUtils.getValueFromJSONObject(obj, "quarter");				
		year = StringUtils.getValueFromJSONObject(obj, "year");
		return;
	}

	public JSONObject getJSONObjectUI()
	{
		JSONObject obj = new JSONObject();

		obj.put("srno",StringUtils.noNull(srno));				
		obj.put("dbsrno",StringUtils.noNull(dbsrno));				
		obj.put("track",StringUtils.noNull(track));				
		obj.put("lstned",StringUtils.noNull(lstned));				
		obj.put("qcdone",StringUtils.noNull(qcdone));				
		obj.put("tapeadt",StringUtils.noNull(tapeadt));				
		obj.put("extaudi",StringUtils.noNull(extaudi));				
		obj.put("pdatey",StringUtils.noNull(pdatey));				
		obj.put("pdatem",StringUtils.noNull(pdatem));				
		obj.put("work",StringUtils.noNull(work));				
		obj.put("cmak1",StringUtils.noNull(cmak1));				
		obj.put("percon",StringUtils.noNull(percon));				
		obj.put("condate",StringUtils.noNull(condate));				
		obj.put("ddatecon",StringUtils.noNull(ddatecon));				
		obj.put("confdeal",StringUtils.noNull(confdeal));				
		obj.put("dealcon",StringUtils.noNull(dealcon));				
		obj.put("mainuser",StringUtils.noNull(mainuser));				
		obj.put("pdcmaker",StringUtils.noNull(pdcmaker));				
		obj.put("vstford",StringUtils.noNull(vstford));				
		obj.put("qrot",StringUtils.noNull(qrot));				
		obj.put("waitdlr",StringUtils.noNull(waitdlr));				
		obj.put("effund",StringUtils.noNull(effund));				
		obj.put("ictstdr",StringUtils.noNull(ictstdr));				
		obj.put("tdrdlr",StringUtils.noNull(tdrdlr));				
		obj.put("tstrout",StringUtils.noNull(tstrout));				
		obj.put("tstcar",StringUtils.noNull(tstcar));				
		obj.put("tdover",StringUtils.noNull(tdover));				
		obj.put("ftsafety",StringUtils.noNull(ftsafety));				
		obj.put("ftconv",StringUtils.noNull(ftconv));				
		obj.put("ftperf",StringUtils.noNull(ftperf));				
		obj.put("ftcost",StringUtils.noNull(ftcost));				
		obj.put("spprovd",StringUtils.noNull(spprovd));				
		obj.put("spcall",StringUtils.noNull(spcall));				
		obj.put("sigreet",StringUtils.noNull(sigreet));				
		obj.put("siexpln",StringUtils.noNull(siexpln));				
		obj.put("sidemo",StringUtils.noNull(sidemo));				
		obj.put("sifllow",StringUtils.noNull(sifllow));				
		obj.put("siiover",StringUtils.noNull(siiover));				
		obj.put("dfseat",StringUtils.noNull(dfseat));				
		obj.put("dfpark",StringUtils.noNull(dfpark));				
		obj.put("dflit",StringUtils.noNull(dflit));				
		obj.put("dfclean",StringUtils.noNull(dfclean));				
		obj.put("dftemp",StringUtils.noNull(dftemp));				
		obj.put("dfref",StringUtils.noNull(dfref));				
		obj.put("dfsmell",StringUtils.noNull(dfsmell));				
		obj.put("dfsignge",StringUtils.noNull(dfsignge));				
		obj.put("dfrtrm",StringUtils.noNull(dfrtrm));				
		obj.put("dfavshw",StringUtils.noNull(dfavshw));				
		obj.put("dlrlyot",StringUtils.noNull(dlrlyot));				
		obj.put("dlrcfof",StringUtils.noNull(dlrcfof));				
		obj.put("dlreslk",StringUtils.noNull(dlreslk));				
		obj.put("dlrsov",StringUtils.noNull(dlrsov));				
		obj.put("dlrloc",StringUtils.noNull(dlrloc));				
		obj.put("dlrover",StringUtils.noNull(dlrover));				
		obj.put("vehfncd",StringUtils.noNull(vehfncd));				
		obj.put("dlrfnc",StringUtils.noNull(dlrfnc));				
		obj.put("condeal",StringUtils.noNull(condeal));				
		obj.put("dlease",StringUtils.noNull(dlease));				
		obj.put("dlpexp",StringUtils.noNull(dlpexp));				
		obj.put("dlqkcom",StringUtils.noNull(dlqkcom));				
		obj.put("dlprcex",StringUtils.noNull(dlprcex));				
		obj.put("dldover",StringUtils.noNull(dldover));				
		obj.put("icgroom",StringUtils.noNull(icgroom));				
		obj.put("icneatly",StringUtils.noNull(icneatly));				
		obj.put("icspent",StringUtils.noNull(icspent));				
		obj.put("icaskuse",StringUtils.noNull(icaskuse));				
		obj.put("icinform",StringUtils.noNull(icinform));				
		obj.put("icstatus",StringUtils.noNull(icstatus));				
		obj.put("spkldge",StringUtils.noNull(spkldge));				
		obj.put("spneed",StringUtils.noNull(spneed));				
		obj.put("spcrtsy",StringUtils.noNull(spcrtsy));				
		obj.put("spprssr",StringUtils.noNull(spprssr));				
		obj.put("spcommt",StringUtils.noNull(spcommt));				
		obj.put("spover",StringUtils.noNull(spover));				
		obj.put("icsame",StringUtils.noNull(icsame));				
		obj.put("icday",StringUtils.noNull(icday));				
		obj.put("dtvsexp",StringUtils.noNull(dtvsexp));				
		obj.put("dldelct",StringUtils.noNull(dldelct));				
		obj.put("dtlen",StringUtils.noNull(dtlen));				
		obj.put("dtstatus",StringUtils.noNull(dtstatus));				
		obj.put("dttime",StringUtils.noNull(dttime));				
		obj.put("dtover",StringUtils.noNull(dtover));				
		obj.put("afvehft",StringUtils.noNull(afvehft));				
		obj.put("afope",StringUtils.noNull(afope));				
		obj.put("afwarr",StringUtils.noNull(afwarr));				
		obj.put("afmaint",StringUtils.noNull(afmaint));				
		obj.put("dlhelp",StringUtils.noNull(dlhelp));				
		obj.put("dlintro",StringUtils.noNull(dlintro));				
		obj.put("dlenftme",StringUtils.noNull(dlenftme));				
		obj.put("dlthank",StringUtils.noNull(dlthank));				
		obj.put("dlramtf",StringUtils.noNull(dlramtf));				
		obj.put("dlspcere",StringUtils.noNull(dlspcere));				
		obj.put("dlvisit",StringUtils.noNull(dlvisit));				
		obj.put("dlext",StringUtils.noNull(dlext));				
		obj.put("dlfeat",StringUtils.noNull(dlfeat));				
		obj.put("dlans",StringUtils.noNull(dlans));				
		obj.put("dlfeebk",StringUtils.noNull(dlfeebk));				
		obj.put("dlconta",StringUtils.noNull(dlconta));				
		obj.put("dlappt",StringUtils.noNull(dlappt));				
		obj.put("dldelpt",StringUtils.noNull(dldelpt));				
		obj.put("delhr",StringUtils.noNull(delhr));				
		obj.put("delmin",StringUtils.noNull(delmin));				
		obj.put("delcar",StringUtils.noNull(delcar));				
		obj.put("dpconvh",StringUtils.noNull(dpconvh));				
		obj.put("dpenthu",StringUtils.noNull(dpenthu));				
		obj.put("dptime",StringUtils.noNull(dptime));				
		obj.put("dpans",StringUtils.noNull(dpans));				
		obj.put("dpthor",StringUtils.noNull(dpthor));				
		obj.put("dpover",StringUtils.noNull(dpover));				
		obj.put("ovover",StringUtils.noNull(ovover));				
		obj.put("prbselct",StringUtils.noNull(prbselct));				
		obj.put("prbstaf",StringUtils.noNull(prbstaf));				
		obj.put("prbchrge",StringUtils.noNull(prbchrge));				
		obj.put("prbanswr",StringUtils.noNull(prbanswr));				
		obj.put("prbshff",StringUtils.noNull(prbshff));				
		obj.put("prbphony",StringUtils.noNull(prbphony));				
		obj.put("recodlr",StringUtils.noNull(recodlr));				
		obj.put("psamedlr",StringUtils.noNull(psamedlr));				
		obj.put("recomake",StringUtils.noNull(recomake));				
		obj.put("reprmake",StringUtils.noNull(reprmake));				
		obj.put("ecdlr",StringUtils.noNull(ecdlr));				
		obj.put("ecveh",StringUtils.noNull(ecveh));				
		obj.put("firstveh",StringUtils.noNull(firstveh));				
		obj.put("newveh",StringUtils.noNull(newveh));				
		obj.put("jdpres",StringUtils.noNull(jdpres));				
		obj.put("jdpinf",StringUtils.noNull(jdpinf));				
		obj.put("fc",StringUtils.noNull(fc));				
		obj.put("reveal",StringUtils.noNull(reveal));				
		obj.put("provfb",StringUtils.noNull(provfb));				
		obj.put("gender",StringUtils.noNull(gender));				
		obj.put("dtmn",StringUtils.noNull(dtmn));				
		obj.put("intid",StringUtils.noNull(intid));				
		obj.put("intgen",StringUtils.noNull(intgen));				
		obj.put("lmn",StringUtils.noNull(lmn));				
		obj.put("intme",StringUtils.noNull(intme));				
		obj.put("txt_oe_start",StringUtils.noNull(txtoestart));				
		obj.put("txt_intid",StringUtils.noNull(txtintid));				
		obj.put("txt_oe_end",StringUtils.noNull(txtoeend));				
		obj.put("scity",StringUtils.noNull(scity));				
		obj.put("intday",StringUtils.noNull(intday));				
		obj.put("intmth",StringUtils.noNull(intmth));				
		obj.put("intdate",StringUtils.noNull(intdate));				
		obj.put("db_start",StringUtils.noNull(dbstart));				
		obj.put("dbcustomername",StringUtils.noNull(dbcustomername));				
		obj.put("dbcustomercity",StringUtils.noNull(dbcustomercity));				
		obj.put("dbcustomerstate",StringUtils.noNull(dbcustomerstate));				
		obj.put("dbmobile",StringUtils.noNull(dbmobile));				
		obj.put("dbtel1",StringUtils.noNull(dbtel1));				
		obj.put("dbtel2",StringUtils.noNull(dbtel2));				
		obj.put("dbmodel",StringUtils.noNull(dbmodel));				
		obj.put("dbmodelvarient",StringUtils.noNull(dbmodelvarient));				
		obj.put("dbfueltype",StringUtils.noNull(dbfueltype));				
		obj.put("dbvin",StringUtils.noNull(dbvin));				
		obj.put("dbdealercode",StringUtils.noNull(dbdealercode));				
		obj.put("dbdealername",StringUtils.noNull(dbdealername));				
		obj.put("dbdealercity",StringUtils.noNull(dbdealercity));				
		obj.put("dbdealerstate",StringUtils.noNull(dbdealerstate));				
		obj.put("dbdeliverydate",StringUtils.noNull(dbdeliverydate));				
		obj.put("dbsalesconsultantcode",StringUtils.noNull(dbsalesconsultantcode));				
		obj.put("dbsalesconsultantname",StringUtils.noNull(dbsalesconsultantname));				
		obj.put("dbpandacode",StringUtils.noNull(dbpandacode));				
		obj.put("dbdealer2",StringUtils.noNull(dbdealer2));				
		obj.put("dbdealer2_labels",StringUtils.noNull(dbdealer2labels));				
		obj.put("dbquarternum",StringUtils.noNull(dbquarternum));				
		obj.put("dbbatchnum",StringUtils.noNull(dbbatchnum));				
		obj.put("db_end",StringUtils.noNull(dbend));				
		obj.put("fdstart",StringUtils.noNull(fdstart));				
		obj.put("dealer",StringUtils.noNull(dealer));				
		obj.put("dealer2",StringUtils.noNull(dealer2));				
		obj.put("city2",StringUtils.noNull(city2));				
		obj.put("regiona",StringUtils.noNull(regiona));				
		obj.put("state2",StringUtils.noNull(state2));				
		obj.put("model",StringUtils.noNull(model));				
		obj.put("model2",StringUtils.noNull(model2));				
		obj.put("city3",StringUtils.noNull(city3));				
		obj.put("fdend",StringUtils.noNull(fdend));				
		obj.put("index_outlier_filter",StringUtils.noNull(indexoutlierfilter));				
		obj.put("irecodestart",StringUtils.noNull(irecodestart));				
		obj.put("zdelhr",StringUtils.noNull(zdelhr));				
		obj.put("zdelmin",StringUtils.noNull(zdelmin));				
		obj.put("cdelmin",StringUtils.noNull(cdelmin));				
		obj.put("ncicday",StringUtils.noNull(ncicday));				
		obj.put("ncdelmin",StringUtils.noNull(ncdelmin));				
		obj.put("irecodeend",StringUtils.noNull(irecodeend));				
		obj.put("recodestart",StringUtils.noNull(recodestart));				
		obj.put("ntdover",StringUtils.noNull(ntdover));				
		obj.put("nsigreet",StringUtils.noNull(nsigreet));				
		obj.put("nsiexpln",StringUtils.noNull(nsiexpln));				
		obj.put("nsidemo",StringUtils.noNull(nsidemo));				
		obj.put("nsifllow",StringUtils.noNull(nsifllow));				
		obj.put("nsiiover",StringUtils.noNull(nsiiover));				
		obj.put("ndlrlyot",StringUtils.noNull(ndlrlyot));				
		obj.put("ndlrcfof",StringUtils.noNull(ndlrcfof));				
		obj.put("ndlreslk",StringUtils.noNull(ndlreslk));				
		obj.put("ndlrsov",StringUtils.noNull(ndlrsov));				
		obj.put("ndlrloc",StringUtils.noNull(ndlrloc));				
		obj.put("ndlrover",StringUtils.noNull(ndlrover));				
		obj.put("ndlease",StringUtils.noNull(ndlease));				
		obj.put("ndlpexp",StringUtils.noNull(ndlpexp));				
		obj.put("ndlqkcom",StringUtils.noNull(ndlqkcom));				
		obj.put("ndlprcex",StringUtils.noNull(ndlprcex));				
		obj.put("ndldover",StringUtils.noNull(ndldover));				
		obj.put("nspkldge",StringUtils.noNull(nspkldge));				
		obj.put("nspneed",StringUtils.noNull(nspneed));				
		obj.put("nspcrtsy",StringUtils.noNull(nspcrtsy));				
		obj.put("nspprssr",StringUtils.noNull(nspprssr));				
		obj.put("nspcommt",StringUtils.noNull(nspcommt));				
		obj.put("nspover",StringUtils.noNull(nspover));				
		obj.put("ndtlen",StringUtils.noNull(ndtlen));				
		obj.put("ndtstatus",StringUtils.noNull(ndtstatus));				
		obj.put("ndttime",StringUtils.noNull(ndttime));				
		obj.put("ndtover",StringUtils.noNull(ndtover));				
		obj.put("ndpconvh",StringUtils.noNull(ndpconvh));				
		obj.put("ndpenthu",StringUtils.noNull(ndpenthu));				
		obj.put("ndptime",StringUtils.noNull(ndptime));				
		obj.put("ndpans",StringUtils.noNull(ndpans));				
		obj.put("ndpthor",StringUtils.noNull(ndpthor));				
		obj.put("ndpover",StringUtils.noNull(ndpover));				
		obj.put("novover",StringUtils.noNull(novover));				
		obj.put("ttdover",StringUtils.noNull(ttdover));				
		obj.put("tsigreet",StringUtils.noNull(tsigreet));				
		obj.put("tsiexpln",StringUtils.noNull(tsiexpln));				
		obj.put("tsidemo",StringUtils.noNull(tsidemo));				
		obj.put("tsifllow",StringUtils.noNull(tsifllow));				
		obj.put("tsiiover",StringUtils.noNull(tsiiover));				
		obj.put("tdlrlyot",StringUtils.noNull(tdlrlyot));				
		obj.put("tdlrcfof",StringUtils.noNull(tdlrcfof));				
		obj.put("tdlreslk",StringUtils.noNull(tdlreslk));				
		obj.put("tdlrsov",StringUtils.noNull(tdlrsov));				
		obj.put("tdlrloc",StringUtils.noNull(tdlrloc));				
		obj.put("tdlrover",StringUtils.noNull(tdlrover));				
		obj.put("tdlease",StringUtils.noNull(tdlease));				
		obj.put("tdlpexp",StringUtils.noNull(tdlpexp));				
		obj.put("tdlqkcom",StringUtils.noNull(tdlqkcom));				
		obj.put("tdlprcex",StringUtils.noNull(tdlprcex));				
		obj.put("tdldover",StringUtils.noNull(tdldover));				
		obj.put("tspkldge",StringUtils.noNull(tspkldge));				
		obj.put("tspneed",StringUtils.noNull(tspneed));				
		obj.put("tspcrtsy",StringUtils.noNull(tspcrtsy));				
		obj.put("tspprssr",StringUtils.noNull(tspprssr));				
		obj.put("tspcommt",StringUtils.noNull(tspcommt));				
		obj.put("tspover",StringUtils.noNull(tspover));				
		obj.put("tdtlen",StringUtils.noNull(tdtlen));				
		obj.put("tdtstatus",StringUtils.noNull(tdtstatus));				
		obj.put("tdttime",StringUtils.noNull(tdttime));				
		obj.put("tdtover",StringUtils.noNull(tdtover));				
		obj.put("tdpconvh",StringUtils.noNull(tdpconvh));				
		obj.put("tdpenthu",StringUtils.noNull(tdpenthu));				
		obj.put("tdptime",StringUtils.noNull(tdptime));				
		obj.put("tdpans",StringUtils.noNull(tdpans));				
		obj.put("tdpthor",StringUtils.noNull(tdpthor));				
		obj.put("tdpover",StringUtils.noNull(tdpover));				
		obj.put("tovover",StringUtils.noNull(tovover));				
		obj.put("atdover",StringUtils.noNull(atdover));				
		obj.put("asigreet",StringUtils.noNull(asigreet));				
		obj.put("asiexpln",StringUtils.noNull(asiexpln));				
		obj.put("asidemo",StringUtils.noNull(asidemo));				
		obj.put("asifllow",StringUtils.noNull(asifllow));				
		obj.put("asiiover",StringUtils.noNull(asiiover));				
		obj.put("adlrlyot",StringUtils.noNull(adlrlyot));				
		obj.put("adlrcfof",StringUtils.noNull(adlrcfof));				
		obj.put("adlreslk",StringUtils.noNull(adlreslk));				
		obj.put("adlrsov",StringUtils.noNull(adlrsov));				
		obj.put("adlrloc",StringUtils.noNull(adlrloc));				
		obj.put("adlrover",StringUtils.noNull(adlrover));				
		obj.put("adlease",StringUtils.noNull(adlease));				
		obj.put("adlpexp",StringUtils.noNull(adlpexp));				
		obj.put("adlqkcom",StringUtils.noNull(adlqkcom));				
		obj.put("adlprcex",StringUtils.noNull(adlprcex));				
		obj.put("adldover",StringUtils.noNull(adldover));				
		obj.put("aspkldge",StringUtils.noNull(aspkldge));				
		obj.put("aspneed",StringUtils.noNull(aspneed));				
		obj.put("aspcrtsy",StringUtils.noNull(aspcrtsy));				
		obj.put("aspprssr",StringUtils.noNull(aspprssr));				
		obj.put("aspcommt",StringUtils.noNull(aspcommt));				
		obj.put("aspover",StringUtils.noNull(aspover));				
		obj.put("adtlen",StringUtils.noNull(adtlen));				
		obj.put("adtstatus",StringUtils.noNull(adtstatus));				
		obj.put("adttime",StringUtils.noNull(adttime));				
		obj.put("adtover",StringUtils.noNull(adtover));				
		obj.put("adpconvh",StringUtils.noNull(adpconvh));				
		obj.put("adpenthu",StringUtils.noNull(adpenthu));				
		obj.put("adptime",StringUtils.noNull(adptime));				
		obj.put("adpans",StringUtils.noNull(adpans));				
		obj.put("adpthor",StringUtils.noNull(adpthor));				
		obj.put("adpover",StringUtils.noNull(adpover));				
		obj.put("aovover",StringUtils.noNull(aovover));				
		obj.put("prb",StringUtils.noNull(prb));				
		obj.put("nprb",StringUtils.noNull(nprb));				
		obj.put("nnprb",StringUtils.noNull(nnprb));				
		obj.put("cntft",StringUtils.noNull(cntft));				
		obj.put("ncntft",StringUtils.noNull(ncntft));				
		obj.put("cntexp",StringUtils.noNull(cntexp));				
		obj.put("ncntexp",StringUtils.noNull(ncntexp));				
		obj.put("sop",StringUtils.noNull(sop));				
		obj.put("nsop",StringUtils.noNull(nsop));				
		obj.put("comsop",StringUtils.noNull(comsop));				
		obj.put("ncomsop",StringUtils.noNull(ncomsop));				
		obj.put("prosop",StringUtils.noNull(prosop));				
		obj.put("nprosop",StringUtils.noNull(nprosop));				
		obj.put("recodeend",StringUtils.noNull(recodeend));				
		obj.put("index",StringUtils.noNull(index));				
		obj.put("si",StringUtils.noNull(si));				
		obj.put("df",StringUtils.noNull(df));				
		obj.put("deal",StringUtils.noNull(deal));				
		obj.put("sp",StringUtils.noNull(sp));				
		obj.put("dt",StringUtils.noNull(dt));				
		obj.put("dp",StringUtils.noNull(dp));				
		obj.put("nindex",StringUtils.noNull(nindex));				
		obj.put("bi_weekly",StringUtils.noNull(biweekly));				
		obj.put("month",StringUtils.noNull(month));				
		obj.put("quarter",StringUtils.noNull(quarter));				
		obj.put("year",StringUtils.noNull(year));
		return obj;
	}

	public void log()
	{
		logger.trace(this.toString());
	}

	public HashMap getTableMap()
	{
		HashMap resultMap = new HashMap();
		ArrayList columnList = new ArrayList();
		resultMap.put("table", "ssi_translated");

		columnList.add("srno");				
		columnList.add("dbsrno");				
		columnList.add("track");				
		columnList.add("lstned");				
		columnList.add("qcdone");				
		columnList.add("tapeadt");				
		columnList.add("extaudi");				
		columnList.add("pdatey");				
		columnList.add("pdatem");				
		columnList.add("work");				
		columnList.add("cmak1");				
		columnList.add("percon");				
		columnList.add("condate");				
		columnList.add("ddatecon");				
		columnList.add("confdeal");				
		columnList.add("dealcon");				
		columnList.add("mainuser");				
		columnList.add("pdcmaker");				
		columnList.add("vstford");				
		columnList.add("qrot");				
		columnList.add("waitdlr");				
		columnList.add("effund");				
		columnList.add("ictstdr");				
		columnList.add("tdrdlr");				
		columnList.add("tstrout");				
		columnList.add("tstcar");				
		columnList.add("tdover");				
		columnList.add("ftsafety");				
		columnList.add("ftconv");				
		columnList.add("ftperf");				
		columnList.add("ftcost");				
		columnList.add("spprovd");				
		columnList.add("spcall");				
		columnList.add("sigreet");				
		columnList.add("siexpln");				
		columnList.add("sidemo");				
		columnList.add("sifllow");				
		columnList.add("siiover");				
		columnList.add("dfseat");				
		columnList.add("dfpark");				
		columnList.add("dflit");				
		columnList.add("dfclean");				
		columnList.add("dftemp");				
		columnList.add("dfref");				
		columnList.add("dfsmell");				
		columnList.add("dfsignge");				
		columnList.add("dfrtrm");				
		columnList.add("dfavshw");				
		columnList.add("dlrlyot");				
		columnList.add("dlrcfof");				
		columnList.add("dlreslk");				
		columnList.add("dlrsov");				
		columnList.add("dlrloc");				
		columnList.add("dlrover");				
		columnList.add("vehfncd");				
		columnList.add("dlrfnc");				
		columnList.add("condeal");				
		columnList.add("dlease");				
		columnList.add("dlpexp");				
		columnList.add("dlqkcom");				
		columnList.add("dlprcex");				
		columnList.add("dldover");				
		columnList.add("icgroom");				
		columnList.add("icneatly");				
		columnList.add("icspent");				
		columnList.add("icaskuse");				
		columnList.add("icinform");				
		columnList.add("icstatus");				
		columnList.add("spkldge");				
		columnList.add("spneed");				
		columnList.add("spcrtsy");				
		columnList.add("spprssr");				
		columnList.add("spcommt");				
		columnList.add("spover");				
		columnList.add("icsame");				
		columnList.add("icday");				
		columnList.add("dtvsexp");				
		columnList.add("dldelct");				
		columnList.add("dtlen");				
		columnList.add("dtstatus");				
		columnList.add("dttime");				
		columnList.add("dtover");				
		columnList.add("afvehft");				
		columnList.add("afope");				
		columnList.add("afwarr");				
		columnList.add("afmaint");				
		columnList.add("dlhelp");				
		columnList.add("dlintro");				
		columnList.add("dlenftme");				
		columnList.add("dlthank");				
		columnList.add("dlramtf");				
		columnList.add("dlspcere");				
		columnList.add("dlvisit");				
		columnList.add("dlext");				
		columnList.add("dlfeat");				
		columnList.add("dlans");				
		columnList.add("dlfeebk");				
		columnList.add("dlconta");				
		columnList.add("dlappt");				
		columnList.add("dldelpt");				
		columnList.add("delhr");				
		columnList.add("delmin");				
		columnList.add("delcar");				
		columnList.add("dpconvh");				
		columnList.add("dpenthu");				
		columnList.add("dptime");				
		columnList.add("dpans");				
		columnList.add("dpthor");				
		columnList.add("dpover");				
		columnList.add("ovover");				
		columnList.add("prbselct");				
		columnList.add("prbstaf");				
		columnList.add("prbchrge");				
		columnList.add("prbanswr");				
		columnList.add("prbshff");				
		columnList.add("prbphony");				
		columnList.add("recodlr");				
		columnList.add("psamedlr");				
		columnList.add("recomake");				
		columnList.add("reprmake");				
		columnList.add("ecdlr");				
		columnList.add("ecveh");				
		columnList.add("firstveh");				
		columnList.add("newveh");				
		columnList.add("jdpres");				
		columnList.add("jdpinf");				
		columnList.add("fc");				
		columnList.add("reveal");				
		columnList.add("provfb");				
		columnList.add("gender");				
		columnList.add("dtmn");				
		columnList.add("intid");				
		columnList.add("intgen");				
		columnList.add("lmn");				
		columnList.add("intme");				
		columnList.add("txt_oe_start");				
		columnList.add("txt_intid");				
		columnList.add("txt_oe_end");				
		columnList.add("scity");				
		columnList.add("intday");				
		columnList.add("intmth");				
		columnList.add("intdate");				
		columnList.add("db_start");				
		columnList.add("dbcustomername");				
		columnList.add("dbcustomercity");				
		columnList.add("dbcustomerstate");				
		columnList.add("dbmobile");				
		columnList.add("dbtel1");				
		columnList.add("dbtel2");				
		columnList.add("dbmodel");				
		columnList.add("dbmodelvarient");				
		columnList.add("dbfueltype");				
		columnList.add("dbvin");				
		columnList.add("dbdealercode");				
		columnList.add("dbdealername");				
		columnList.add("dbdealercity");				
		columnList.add("dbdealerstate");				
		columnList.add("dbdeliverydate");				
		columnList.add("dbsalesconsultantcode");				
		columnList.add("dbsalesconsultantname");				
		columnList.add("dbpandacode");				
		columnList.add("dbdealer2");				
		columnList.add("dbdealer2_labels");				
		columnList.add("dbquarternum");				
		columnList.add("dbbatchnum");				
		columnList.add("db_end");				
		columnList.add("fdstart");				
		columnList.add("dealer");				
		columnList.add("dealer2");				
		columnList.add("city2");				
		columnList.add("regiona");				
		columnList.add("state2");				
		columnList.add("model");				
		columnList.add("model2");				
		columnList.add("city3");				
		columnList.add("fdend");				
		columnList.add("index_outlier_filter");				
		columnList.add("irecodestart");				
		columnList.add("zdelhr");				
		columnList.add("zdelmin");				
		columnList.add("cdelmin");				
		columnList.add("ncicday");				
		columnList.add("ncdelmin");				
		columnList.add("irecodeend");				
		columnList.add("recodestart");				
		columnList.add("ntdover");				
		columnList.add("nsigreet");				
		columnList.add("nsiexpln");				
		columnList.add("nsidemo");				
		columnList.add("nsifllow");				
		columnList.add("nsiiover");				
		columnList.add("ndlrlyot");				
		columnList.add("ndlrcfof");				
		columnList.add("ndlreslk");				
		columnList.add("ndlrsov");				
		columnList.add("ndlrloc");				
		columnList.add("ndlrover");				
		columnList.add("ndlease");				
		columnList.add("ndlpexp");				
		columnList.add("ndlqkcom");				
		columnList.add("ndlprcex");				
		columnList.add("ndldover");				
		columnList.add("nspkldge");				
		columnList.add("nspneed");				
		columnList.add("nspcrtsy");				
		columnList.add("nspprssr");				
		columnList.add("nspcommt");				
		columnList.add("nspover");				
		columnList.add("ndtlen");				
		columnList.add("ndtstatus");				
		columnList.add("ndttime");				
		columnList.add("ndtover");				
		columnList.add("ndpconvh");				
		columnList.add("ndpenthu");				
		columnList.add("ndptime");				
		columnList.add("ndpans");				
		columnList.add("ndpthor");				
		columnList.add("ndpover");				
		columnList.add("novover");				
		columnList.add("ttdover");				
		columnList.add("tsigreet");				
		columnList.add("tsiexpln");				
		columnList.add("tsidemo");				
		columnList.add("tsifllow");				
		columnList.add("tsiiover");				
		columnList.add("tdlrlyot");				
		columnList.add("tdlrcfof");				
		columnList.add("tdlreslk");				
		columnList.add("tdlrsov");				
		columnList.add("tdlrloc");				
		columnList.add("tdlrover");				
		columnList.add("tdlease");				
		columnList.add("tdlpexp");				
		columnList.add("tdlqkcom");				
		columnList.add("tdlprcex");				
		columnList.add("tdldover");				
		columnList.add("tspkldge");				
		columnList.add("tspneed");				
		columnList.add("tspcrtsy");				
		columnList.add("tspprssr");				
		columnList.add("tspcommt");				
		columnList.add("tspover");				
		columnList.add("tdtlen");				
		columnList.add("tdtstatus");				
		columnList.add("tdttime");				
		columnList.add("tdtover");				
		columnList.add("tdpconvh");				
		columnList.add("tdpenthu");				
		columnList.add("tdptime");				
		columnList.add("tdpans");				
		columnList.add("tdpthor");				
		columnList.add("tdpover");				
		columnList.add("tovover");				
		columnList.add("atdover");				
		columnList.add("asigreet");				
		columnList.add("asiexpln");				
		columnList.add("asidemo");				
		columnList.add("asifllow");				
		columnList.add("asiiover");				
		columnList.add("adlrlyot");				
		columnList.add("adlrcfof");				
		columnList.add("adlreslk");				
		columnList.add("adlrsov");				
		columnList.add("adlrloc");				
		columnList.add("adlrover");				
		columnList.add("adlease");				
		columnList.add("adlpexp");				
		columnList.add("adlqkcom");				
		columnList.add("adlprcex");				
		columnList.add("adldover");				
		columnList.add("aspkldge");				
		columnList.add("aspneed");				
		columnList.add("aspcrtsy");				
		columnList.add("aspprssr");				
		columnList.add("aspcommt");				
		columnList.add("aspover");				
		columnList.add("adtlen");				
		columnList.add("adtstatus");				
		columnList.add("adttime");				
		columnList.add("adtover");				
		columnList.add("adpconvh");				
		columnList.add("adpenthu");				
		columnList.add("adptime");				
		columnList.add("adpans");				
		columnList.add("adpthor");				
		columnList.add("adpover");				
		columnList.add("aovover");				
		columnList.add("prb");				
		columnList.add("nprb");				
		columnList.add("nnprb");				
		columnList.add("cntft");				
		columnList.add("ncntft");				
		columnList.add("cntexp");				
		columnList.add("ncntexp");				
		columnList.add("sop");				
		columnList.add("nsop");				
		columnList.add("comsop");				
		columnList.add("ncomsop");				
		columnList.add("prosop");				
		columnList.add("nprosop");				
		columnList.add("recodeend");				
		columnList.add("index");				
		columnList.add("si");				
		columnList.add("df");				
		columnList.add("deal");				
		columnList.add("sp");				
		columnList.add("dt");				
		columnList.add("dp");				
		columnList.add("nindex");				
		columnList.add("bi_weekly");				
		columnList.add("month");				
		columnList.add("quarter");				
		columnList.add("year");
		resultMap.put("ColumnList", columnList);

		return resultMap;
	}

}
