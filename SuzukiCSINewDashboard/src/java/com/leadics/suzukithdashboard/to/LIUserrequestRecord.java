
/*
 * LIUserrequestRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.to;
import com.leadics.suzukithdashboard.common.LIRecord;
import com.leadics.utils.*;
import org.json.simple.*;
import java.util.*;
public class LIUserrequestRecord extends LIRecord
{
	static LogUtils logger = new LogUtils(LIUserrequestRecord.class.getName());

	private String id;
	private String userrecid;
	private String userid;
	private String uname;
	private String requesttype;
	private String comments;
	private String institutionid;
	private String madeby;
	private String madeat;
	private String checkedby;
	private String checkedat;
	private String makerlastcmt;
	private String checkerlastcmt;
	private String currappstatus;
	private String adminlastcmt;
	private String rstatus;
	private String createdby;
	private String createdat;
	private String modifiedat;
	private String modifiedby;

	public String getId()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(id);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(id);
		}
		else
		{
			return id;
		}
	}

	public String getUserrecid()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(userrecid);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(userrecid);
		}
		else
		{
			return userrecid;
		}
	}

	public String getUserid()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(userid);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(userid);
		}
		else
		{
			return userid;
		}
	}

	public String getUname()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(uname);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(uname);
		}
		else
		{
			return uname;
		}
	}

	public String getRequesttype()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(requesttype);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(requesttype);
		}
		else
		{
			return requesttype;
		}
	}

	public String getComments()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(comments);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(comments);
		}
		else
		{
			return comments;
		}
	}

	public String getInstitutionid()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(institutionid);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(institutionid);
		}
		else
		{
			return institutionid;
		}
	}

	public String getMadeby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(madeby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(madeby);
		}
		else
		{
			return madeby;
		}
	}

	public String getMadeat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(madeat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(madeat);
		}
		else
		{
			return madeat;
		}
	}

	public String getCheckedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(checkedby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(checkedby);
		}
		else
		{
			return checkedby;
		}
	}

	public String getCheckedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(checkedat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(checkedat);
		}
		else
		{
			return checkedat;
		}
	}

	public String getMakerlastcmt()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(makerlastcmt);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(makerlastcmt);
		}
		else
		{
			return makerlastcmt;
		}
	}

	public String getCheckerlastcmt()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(checkerlastcmt);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(checkerlastcmt);
		}
		else
		{
			return checkerlastcmt;
		}
	}

	public String getCurrappstatus()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(currappstatus);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(currappstatus);
		}
		else
		{
			return currappstatus;
		}
	}

	public String getAdminlastcmt()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(adminlastcmt);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(adminlastcmt);
		}
		else
		{
			return adminlastcmt;
		}
	}

	public String getRstatus()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(rstatus);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(rstatus);
		}
		else
		{
			return rstatus;
		}
	}

	public String getCreatedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdby);
		}
		else
		{
			return createdby;
		}
	}

	public String getCreatedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdat);
		}
		else
		{
			return createdat;
		}
	}

	public String getModifiedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedat);
		}
		else
		{
			return modifiedat;
		}
	}

	public String getModifiedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedby);
		}
		else
		{
			return modifiedby;
		}
	}


	public void setId(String value)
	{
		id = value;
	}

	public void setUserrecid(String value)
	{
		userrecid = value;
	}

	public void setUserid(String value)
	{
		userid = value;
	}

	public void setUname(String value)
	{
		uname = value;
	}

	public void setRequesttype(String value)
	{
		requesttype = value;
	}

	public void setComments(String value)
	{
		comments = value;
	}

	public void setInstitutionid(String value)
	{
		institutionid = value;
	}

	public void setMadeby(String value)
	{
		madeby = value;
	}

	public void setMadeat(String value)
	{
		madeat = value;
	}

	public void setCheckedby(String value)
	{
		checkedby = value;
	}

	public void setCheckedat(String value)
	{
		checkedat = value;
	}

	public void setMakerlastcmt(String value)
	{
		makerlastcmt = value;
	}

	public void setCheckerlastcmt(String value)
	{
		checkerlastcmt = value;
	}

	public void setCurrappstatus(String value)
	{
		currappstatus = value;
	}

	public void setAdminlastcmt(String value)
	{
		adminlastcmt = value;
	}

	public void setRstatus(String value)
	{
		rstatus = value;
	}

	public void setCreatedby(String value)
	{
		createdby = value;
	}

	public void setCreatedat(String value)
	{
		createdat = value;
	}

	public void setModifiedat(String value)
	{
		modifiedat = value;
	}

	public void setModifiedby(String value)
	{
		modifiedby = value;
	}


	public String toString()
	{
		return "\nid:" + id +
				"\nuserrecid:" + userrecid +
				"\nuserid:" + userid +
				"\nuname:" + uname +
				"\nrequesttype:" + requesttype +
				"\ncomments:" + comments +
				"\ninstitutionid:" + institutionid +
				"\nmadeby:" + madeby +
				"\nmadeat:" + madeat +
				"\ncheckedby:" + checkedby +
				"\ncheckedat:" + checkedat +
				"\nmakerlastcmt:" + makerlastcmt +
				"\ncheckerlastcmt:" + checkerlastcmt +
				"\ncurrappstatus:" + currappstatus +
				"\nadminlastcmt:" + adminlastcmt +
				"\nrstatus:" + rstatus +
				"\ncreatedby:" + createdby +
				"\ncreatedat:" + createdat +
				"\nmodifiedat:" + modifiedat +
				"\nmodifiedby:" + modifiedby +
				"\n";
	}

	public void loadContent(LIUserrequestRecord inputRecord)
	{
		setId(inputRecord.getId());
		setUserrecid(inputRecord.getUserrecid());
		setUserid(inputRecord.getUserid());
		setUname(inputRecord.getUname());
		setRequesttype(inputRecord.getRequesttype());
		setComments(inputRecord.getComments());
		setInstitutionid(inputRecord.getInstitutionid());
		setMadeby(inputRecord.getMadeby());
		setMadeat(inputRecord.getMadeat());
		setCheckedby(inputRecord.getCheckedby());
		setCheckedat(inputRecord.getCheckedat());
		setMakerlastcmt(inputRecord.getMakerlastcmt());
		setCheckerlastcmt(inputRecord.getCheckerlastcmt());
		setCurrappstatus(inputRecord.getCurrappstatus());
		setAdminlastcmt(inputRecord.getAdminlastcmt());
		setRstatus(inputRecord.getRstatus());
		setCreatedby(inputRecord.getCreatedby());
		setCreatedat(inputRecord.getCreatedat());
		setModifiedat(inputRecord.getModifiedat());
		setModifiedby(inputRecord.getModifiedby());
	}

	public void loadNonNullContent(LIUserrequestRecord inputRecord)
	{
		if (StringUtils.hasChanged(getId(), inputRecord.getId()))
		{
			setId(StringUtils.noNull(inputRecord.getId()));
		}
		if (StringUtils.hasChanged(getUserrecid(), inputRecord.getUserrecid()))
		{
			setUserrecid(StringUtils.noNull(inputRecord.getUserrecid()));
		}
		if (StringUtils.hasChanged(getUserid(), inputRecord.getUserid()))
		{
			setUserid(StringUtils.noNull(inputRecord.getUserid()));
		}
		if (StringUtils.hasChanged(getUname(), inputRecord.getUname()))
		{
			setUname(StringUtils.noNull(inputRecord.getUname()));
		}
		if (StringUtils.hasChanged(getRequesttype(), inputRecord.getRequesttype()))
		{
			setRequesttype(StringUtils.noNull(inputRecord.getRequesttype()));
		}
		if (StringUtils.hasChanged(getComments(), inputRecord.getComments()))
		{
			setComments(StringUtils.noNull(inputRecord.getComments()));
		}
		if (StringUtils.hasChanged(getInstitutionid(), inputRecord.getInstitutionid()))
		{
			setInstitutionid(StringUtils.noNull(inputRecord.getInstitutionid()));
		}
		if (StringUtils.hasChanged(getMadeby(), inputRecord.getMadeby()))
		{
			setMadeby(StringUtils.noNull(inputRecord.getMadeby()));
		}
		if (StringUtils.hasChanged(getMadeat(), inputRecord.getMadeat()))
		{
			setMadeat(StringUtils.noNull(inputRecord.getMadeat()));
		}
		if (StringUtils.hasChanged(getCheckedby(), inputRecord.getCheckedby()))
		{
			setCheckedby(StringUtils.noNull(inputRecord.getCheckedby()));
		}
		if (StringUtils.hasChanged(getCheckedat(), inputRecord.getCheckedat()))
		{
			setCheckedat(StringUtils.noNull(inputRecord.getCheckedat()));
		}
		if (StringUtils.hasChanged(getMakerlastcmt(), inputRecord.getMakerlastcmt()))
		{
			setMakerlastcmt(StringUtils.noNull(inputRecord.getMakerlastcmt()));
		}
		if (StringUtils.hasChanged(getCheckerlastcmt(), inputRecord.getCheckerlastcmt()))
		{
			setCheckerlastcmt(StringUtils.noNull(inputRecord.getCheckerlastcmt()));
		}
		if (StringUtils.hasChanged(getCurrappstatus(), inputRecord.getCurrappstatus()))
		{
			setCurrappstatus(StringUtils.noNull(inputRecord.getCurrappstatus()));
		}
		if (StringUtils.hasChanged(getAdminlastcmt(), inputRecord.getAdminlastcmt()))
		{
			setAdminlastcmt(StringUtils.noNull(inputRecord.getAdminlastcmt()));
		}
		if (StringUtils.hasChanged(getRstatus(), inputRecord.getRstatus()))
		{
			setRstatus(StringUtils.noNull(inputRecord.getRstatus()));
		}
		if (StringUtils.hasChanged(getCreatedby(), inputRecord.getCreatedby()))
		{
			setCreatedby(StringUtils.noNull(inputRecord.getCreatedby()));
		}
		if (StringUtils.hasChanged(getCreatedat(), inputRecord.getCreatedat()))
		{
			setCreatedat(StringUtils.noNull(inputRecord.getCreatedat()));
		}
		if (StringUtils.hasChanged(getModifiedat(), inputRecord.getModifiedat()))
		{
			setModifiedat(StringUtils.noNull(inputRecord.getModifiedat()));
		}
		if (StringUtils.hasChanged(getModifiedby(), inputRecord.getModifiedby()))
		{
			setModifiedby(StringUtils.noNull(inputRecord.getModifiedby()));
		}
	}

	public JSONObject getJSONObject()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("userrecid",StringUtils.noNull(userrecid));				
		obj.put("userid",StringUtils.noNull(userid));				
		obj.put("uname",StringUtils.noNull(uname));				
		obj.put("requesttype",StringUtils.noNull(requesttype));				
		obj.put("comments",StringUtils.noNull(comments));				
		obj.put("institutionid",StringUtils.noNull(institutionid));				
		obj.put("madeby",StringUtils.noNull(madeby));				
		obj.put("madeat",StringUtils.noNull(madeat));				
		obj.put("checkedby",StringUtils.noNull(checkedby));				
		obj.put("checkedat",StringUtils.noNull(checkedat));				
		obj.put("makerlastcmt",StringUtils.noNull(makerlastcmt));				
		obj.put("checkerlastcmt",StringUtils.noNull(checkerlastcmt));				
		obj.put("currappstatus",StringUtils.noNull(currappstatus));				
		obj.put("adminlastcmt",StringUtils.noNull(adminlastcmt));				
		obj.put("rstatus",StringUtils.noNull(rstatus));				
		obj.put("createdby",StringUtils.noNull(createdby));				
		obj.put("createdat",StringUtils.noNull(createdat));				
		obj.put("modifiedat",StringUtils.noNull(modifiedat));				
		obj.put("modifiedby",StringUtils.noNull(modifiedby));
		return obj;
	}

	public void loadJSONObject(JSONObject obj)
	throws Exception
	{
		if (obj == null) return;

		id = StringUtils.getValueFromJSONObject(obj, "id");				
		userrecid = StringUtils.getValueFromJSONObject(obj, "userrecid");				
		userid = StringUtils.getValueFromJSONObject(obj, "userid");				
		uname = StringUtils.getValueFromJSONObject(obj, "uname");				
		requesttype = StringUtils.getValueFromJSONObject(obj, "requesttype");				
		comments = StringUtils.getValueFromJSONObject(obj, "comments");				
		institutionid = StringUtils.getValueFromJSONObject(obj, "institutionid");				
		madeby = StringUtils.getValueFromJSONObject(obj, "madeby");				
		madeat = StringUtils.getValueFromJSONObject(obj, "madeat");				
		checkedby = StringUtils.getValueFromJSONObject(obj, "checkedby");				
		checkedat = StringUtils.getValueFromJSONObject(obj, "checkedat");				
		makerlastcmt = StringUtils.getValueFromJSONObject(obj, "makerlastcmt");				
		checkerlastcmt = StringUtils.getValueFromJSONObject(obj, "checkerlastcmt");				
		currappstatus = StringUtils.getValueFromJSONObject(obj, "currappstatus");				
		adminlastcmt = StringUtils.getValueFromJSONObject(obj, "adminlastcmt");				
		rstatus = StringUtils.getValueFromJSONObject(obj, "rstatus");				
		createdby = StringUtils.getValueFromJSONObject(obj, "createdby");				
		createdat = StringUtils.getValueFromJSONObject(obj, "createdat");				
		modifiedat = StringUtils.getValueFromJSONObject(obj, "modifiedat");				
		modifiedby = StringUtils.getValueFromJSONObject(obj, "modifiedby");
		return;
	}

	public JSONObject getJSONObjectUI()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("userrecid",StringUtils.noNull(userrecid));				
		obj.put("userid",StringUtils.noNull(userid));				
		obj.put("uname",StringUtils.noNull(uname));				
		obj.put("request_type",StringUtils.noNull(requesttype));				
		obj.put("comments",StringUtils.noNull(comments));				
		obj.put("institution_id",StringUtils.noNull(institutionid));				
		obj.put("made_by",StringUtils.noNull(madeby));				
		obj.put("made_at",StringUtils.noNull(madeat));				
		obj.put("checked_by",StringUtils.noNull(checkedby));				
		obj.put("checked_at",StringUtils.noNull(checkedat));				
		obj.put("maker_last_cmt",StringUtils.noNull(makerlastcmt));				
		obj.put("checker_last_cmt",StringUtils.noNull(checkerlastcmt));				
		obj.put("curr_app_status",StringUtils.noNull(currappstatus));				
		obj.put("admin_last_cmt",StringUtils.noNull(adminlastcmt));				
		obj.put("rstatus",StringUtils.noNull(rstatus));				
		obj.put("created_by",StringUtils.noNull(createdby));				
		obj.put("created_at",StringUtils.noNull(createdat));				
		obj.put("modified_at",StringUtils.noNull(modifiedat));				
		obj.put("modified_by",StringUtils.noNull(modifiedby));
		return obj;
	}

	public void log()
	{
		logger.trace(this.toString());
	}

	public HashMap getTableMap()
	{
		HashMap resultMap = new HashMap();
		ArrayList columnList = new ArrayList();
		resultMap.put("table", "user_request");

		columnList.add("id");				
		columnList.add("userrecid");				
		columnList.add("userid");				
		columnList.add("uname");				
		columnList.add("request_type");				
		columnList.add("comments");				
		columnList.add("institution_id");				
		columnList.add("made_by");				
		columnList.add("made_at");				
		columnList.add("checked_by");				
		columnList.add("checked_at");				
		columnList.add("maker_last_cmt");				
		columnList.add("checker_last_cmt");				
		columnList.add("curr_app_status");				
		columnList.add("admin_last_cmt");				
		columnList.add("rstatus");				
		columnList.add("created_by");				
		columnList.add("created_at");				
		columnList.add("modified_at");				
		columnList.add("modified_by");
		resultMap.put("ColumnList", columnList);

		return resultMap;
	}

	public void toggleStatus()
	{
		String currentStatus = StringUtils.noNull(getRstatus());
		setRstatus("1");
		if (currentStatus.equals("1"))
		{
			setRstatus("0");
		}
	}

}
