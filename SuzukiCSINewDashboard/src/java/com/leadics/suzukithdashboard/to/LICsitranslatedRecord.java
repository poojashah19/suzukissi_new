
/*
 * LICsitranslatedRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.to;
import com.leadics.suzukithdashboard.common.LIRecord;
import com.leadics.utils.*;
import org.json.simple.*;
import java.util.*;
public class LICsitranslatedRecord extends LIRecord
{
	static LogUtils logger = new LogUtils(LICsitranslatedRecord.class.getName());

	private String id;
	private String srno;
	private String dbsrno;
	private String track;
	private String listenobs;
	private String intadt;
	private String tapeadt;
	private String thirdadt;
	private String cityres;
	private String dbsvdate;
	private String dbpdate;
	private String caruse;
	private String dbdealer;
	private String scdlr;
	private String svdate;
	private String rsvcdate;
	private String owner;
	private String dropveh;
	private String pickveh;
	private String oilchng;
	private String maintain;
	private String emergrep;
	private String nemerrep;
	private String bpaintrep;
	private String mrdk;
	private String svctime;
	private String cddate;
	private String scddate;
	private String qrot;
	private String notified;
	private String schedule;
	private String aptdesir;
	private String hndovr;
	private String startsi;
	private String sisched;
	private String siflex;
	private String sitime;
	private String siover;
	private String greetwk;
	private String reaswait;
	private String saease;
	private String saadd;
	private String saneat;
	private String saattn;
	private String sarepeat;
	private String safocus;
	private String sawalk;
	private String sastatus;
	private String sahist;
	private String priorexp;
	private String priorfm;
	private String estimate;
	private String givready;
	private String startsa;
	private String safren;
	private String saresp;
	private String sathor;
	private String saover;
	private String stayleft;
	private String offtran;
	private String waitcln;
	private String cleanlou;
	private String convop;
	private String facweb;
	private String facrefr;
	private String factv;
	private String facseat;
	private String facread;
	private String facvpark;
	private String facaircon;
	private String facfood;
	private String facoth;
	private String facdk;
	private String facothv;
	private String startsf;
	private String sfdrive;
	private String sflocat;
	private String sfclean;
	private String sfcomfy;
	private String sfover;
	private String delivery;
	private String payment;
	private String payamt;
	private String infready;
	private String explwrka;
	private String sareview;
	private String saadvice;
	private String explchrg;
	private String schdnext;
	private String reastime;
	private String svctimh;
	private String svctimd;
	private String rdyprom;
	private String startvp;
	private String vptime;
	private String vpfair;
	private String vpstaff;
	private String vpover;
	private String wdright;
	private String compreq;
	private String vehwash;
	private String contact;
	private String repprob;
	private String dlrresol;
	private String startsq;
	private String sqtime;
	private String sqthor;
	private String sqclean;
	private String sqover;
	private String osat;
	private String satford;
	private String svcexp;
	private String recodlr;
	private String revwar;
	private String revpwar;
	private String purchase;
	private String recomake;
	private String reprmake;
	private String dlrstand;
	private String reascost;
	private String emodeal;
	private String emoford;
	private String svcnauth;
	private String problems;
	private String numprob;
	private String toldds;
	private String toldcc;
	private String fc;
	private String revres;
	private String feedbk;
	private String gender;
	private String dtmn;
	private String intid;
	private String intgen;
	private String lmn;
	private String intme;
	private String ddate;
	private String svcdate;
	private String dpwaitcln;
	private String dpcleanlou;
	private String dpsfdrive;
	private String dpsflocat;
	private String dpsfclean;
	private String dpsfcomfy;
	private String dpsfover;
	private String irecodestart;
	private String npayamt;
	private String nnpayamt;
	private String nsvctimd;
	private String nnsvctimd;
	private String nsvctimh;
	private String nnsvctimh;
	private String nnumprob;
	private String nhndovr;
	private String irecodeend;
	private String intdate;
	private String recodestart;
	private String tsisched;
	private String tsiflex;
	private String tsitime;
	private String tsiover;
	private String tsafren;
	private String tsaresp;
	private String tsathor;
	private String tsaover;
	private String tsfdrive;
	private String tsflocat;
	private String tsfclean;
	private String tsfcomfy;
	private String tsfover;
	private String tvptime;
	private String tvpfair;
	private String tvpstaff;
	private String tvpover;
	private String tsqtime;
	private String tsqthor;
	private String tsqclean;
	private String tsqover;
	private String tosat;
	private String tsatford;
	private String nsisched;
	private String nsiflex;
	private String nsitime;
	private String nsiover;
	private String nsafren;
	private String nsaresp;
	private String nsathor;
	private String nsaover;
	private String nsfdrive;
	private String nsflocat;
	private String nsfclean;
	private String nsfcomfy;
	private String nsfover;
	private String nvptime;
	private String nvpfair;
	private String nvpstaff;
	private String nvpover;
	private String nsqtime;
	private String nsqthor;
	private String nsqclean;
	private String nsqover;
	private String nosat;
	private String nsatford;
	private String asisched;
	private String asiflex;
	private String asitime;
	private String asiover;
	private String asafren;
	private String asaresp;
	private String asathor;
	private String asaover;
	private String asfdrive;
	private String asflocat;
	private String asfclean;
	private String asfcomfy;
	private String asfover;
	private String avptime;
	private String avpfair;
	private String avpstaff;
	private String avpover;
	private String asqtime;
	private String asqthor;
	private String asqclean;
	private String asqover;
	private String aosat;
	private String asatford;
	private String mthown;
	private String timeoilh;
	private String timeoild;
	private String timermh;
	private String timermd;
	private String timeerh;
	private String timeerd;
	private String timenerh;
	private String timenerd;
	private String timebph;
	private String timebpd;
	private String nsahist;
	private String nrdyprom;
	private String nvehwash;
	private String npayment;
	private String rightoil;
	private String rightrm;
	private String righter;
	private String rightner;
	private String rightbp;
	private String nwdright;
	private String nreptype;
	private String sop;
	private String nsop;
	private String nnsop;
	private String rmsop;
	private String nrmsop;
	private String nnrmsop;
	private String relmeas;
	private String recodeend;
	private String fdstart;
	private String dealer2;
	private String city2;
	private String regiona;
	private String state2;
	private String city3;
	private String model2;
	private String index;
	private String si;
	private String sa;
	private String sf;
	private String vp;
	private String sq;
	private String nindex;
	private String biweekly;
	private String month;
	private String quarter;
	private String year;
	private String rstatus;
	private String lastacc;
	private String createdby;
	private String createdat;
	private String modifiedat;
	private String modifiedby;
	private String closetype;
	private String closedat;

	public String getId()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(id);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(id);
		}
		else
		{
			return id;
		}
	}

	public String getSrno()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(srno);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(srno);
		}
		else
		{
			return srno;
		}
	}

	public String getDbsrno()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbsrno);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbsrno);
		}
		else
		{
			return dbsrno;
		}
	}

	public String getTrack()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(track);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(track);
		}
		else
		{
			return track;
		}
	}

	public String getListenobs()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(listenobs);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(listenobs);
		}
		else
		{
			return listenobs;
		}
	}

	public String getIntadt()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(intadt);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(intadt);
		}
		else
		{
			return intadt;
		}
	}

	public String getTapeadt()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tapeadt);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tapeadt);
		}
		else
		{
			return tapeadt;
		}
	}

	public String getThirdadt()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(thirdadt);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(thirdadt);
		}
		else
		{
			return thirdadt;
		}
	}

	public String getCityres()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(cityres);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(cityres);
		}
		else
		{
			return cityres;
		}
	}

	public String getDbsvdate()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbsvdate);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbsvdate);
		}
		else
		{
			return dbsvdate;
		}
	}

	public String getDbpdate()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbpdate);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbpdate);
		}
		else
		{
			return dbpdate;
		}
	}

	public String getCaruse()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(caruse);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(caruse);
		}
		else
		{
			return caruse;
		}
	}

	public String getDbdealer()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dbdealer);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dbdealer);
		}
		else
		{
			return dbdealer;
		}
	}

	public String getScdlr()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(scdlr);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(scdlr);
		}
		else
		{
			return scdlr;
		}
	}

	public String getSvdate()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(svdate);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(svdate);
		}
		else
		{
			return svdate;
		}
	}

	public String getRsvcdate()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(rsvcdate);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(rsvcdate);
		}
		else
		{
			return rsvcdate;
		}
	}

	public String getOwner()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(owner);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(owner);
		}
		else
		{
			return owner;
		}
	}

	public String getDropveh()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dropveh);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dropveh);
		}
		else
		{
			return dropveh;
		}
	}

	public String getPickveh()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(pickveh);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(pickveh);
		}
		else
		{
			return pickveh;
		}
	}

	public String getOilchng()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(oilchng);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(oilchng);
		}
		else
		{
			return oilchng;
		}
	}

	public String getMaintain()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(maintain);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(maintain);
		}
		else
		{
			return maintain;
		}
	}

	public String getEmergrep()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(emergrep);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(emergrep);
		}
		else
		{
			return emergrep;
		}
	}

	public String getNemerrep()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nemerrep);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nemerrep);
		}
		else
		{
			return nemerrep;
		}
	}

	public String getBpaintrep()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(bpaintrep);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(bpaintrep);
		}
		else
		{
			return bpaintrep;
		}
	}

	public String getMrdk()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(mrdk);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(mrdk);
		}
		else
		{
			return mrdk;
		}
	}

	public String getSvctime()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(svctime);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(svctime);
		}
		else
		{
			return svctime;
		}
	}

	public String getCddate()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(cddate);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(cddate);
		}
		else
		{
			return cddate;
		}
	}

	public String getScddate()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(scddate);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(scddate);
		}
		else
		{
			return scddate;
		}
	}

	public String getQrot()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(qrot);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(qrot);
		}
		else
		{
			return qrot;
		}
	}

	public String getNotified()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(notified);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(notified);
		}
		else
		{
			return notified;
		}
	}

	public String getSchedule()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(schedule);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(schedule);
		}
		else
		{
			return schedule;
		}
	}

	public String getAptdesir()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(aptdesir);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(aptdesir);
		}
		else
		{
			return aptdesir;
		}
	}

	public String getHndovr()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(hndovr);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(hndovr);
		}
		else
		{
			return hndovr;
		}
	}

	public String getStartsi()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(startsi);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(startsi);
		}
		else
		{
			return startsi;
		}
	}

	public String getSisched()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sisched);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sisched);
		}
		else
		{
			return sisched;
		}
	}

	public String getSiflex()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(siflex);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(siflex);
		}
		else
		{
			return siflex;
		}
	}

	public String getSitime()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sitime);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sitime);
		}
		else
		{
			return sitime;
		}
	}

	public String getSiover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(siover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(siover);
		}
		else
		{
			return siover;
		}
	}

	public String getGreetwk()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(greetwk);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(greetwk);
		}
		else
		{
			return greetwk;
		}
	}

	public String getReaswait()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(reaswait);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(reaswait);
		}
		else
		{
			return reaswait;
		}
	}

	public String getSaease()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(saease);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(saease);
		}
		else
		{
			return saease;
		}
	}

	public String getSaadd()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(saadd);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(saadd);
		}
		else
		{
			return saadd;
		}
	}

	public String getSaneat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(saneat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(saneat);
		}
		else
		{
			return saneat;
		}
	}

	public String getSaattn()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(saattn);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(saattn);
		}
		else
		{
			return saattn;
		}
	}

	public String getSarepeat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sarepeat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sarepeat);
		}
		else
		{
			return sarepeat;
		}
	}

	public String getSafocus()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(safocus);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(safocus);
		}
		else
		{
			return safocus;
		}
	}

	public String getSawalk()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sawalk);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sawalk);
		}
		else
		{
			return sawalk;
		}
	}

	public String getSastatus()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sastatus);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sastatus);
		}
		else
		{
			return sastatus;
		}
	}

	public String getSahist()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sahist);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sahist);
		}
		else
		{
			return sahist;
		}
	}

	public String getPriorexp()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(priorexp);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(priorexp);
		}
		else
		{
			return priorexp;
		}
	}

	public String getPriorfm()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(priorfm);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(priorfm);
		}
		else
		{
			return priorfm;
		}
	}

	public String getEstimate()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(estimate);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(estimate);
		}
		else
		{
			return estimate;
		}
	}

	public String getGivready()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(givready);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(givready);
		}
		else
		{
			return givready;
		}
	}

	public String getStartsa()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(startsa);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(startsa);
		}
		else
		{
			return startsa;
		}
	}

	public String getSafren()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(safren);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(safren);
		}
		else
		{
			return safren;
		}
	}

	public String getSaresp()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(saresp);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(saresp);
		}
		else
		{
			return saresp;
		}
	}

	public String getSathor()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sathor);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sathor);
		}
		else
		{
			return sathor;
		}
	}

	public String getSaover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(saover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(saover);
		}
		else
		{
			return saover;
		}
	}

	public String getStayleft()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(stayleft);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(stayleft);
		}
		else
		{
			return stayleft;
		}
	}

	public String getOfftran()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(offtran);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(offtran);
		}
		else
		{
			return offtran;
		}
	}

	public String getWaitcln()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(waitcln);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(waitcln);
		}
		else
		{
			return waitcln;
		}
	}

	public String getCleanlou()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(cleanlou);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(cleanlou);
		}
		else
		{
			return cleanlou;
		}
	}

	public String getConvop()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(convop);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(convop);
		}
		else
		{
			return convop;
		}
	}

	public String getFacweb()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(facweb);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(facweb);
		}
		else
		{
			return facweb;
		}
	}

	public String getFacrefr()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(facrefr);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(facrefr);
		}
		else
		{
			return facrefr;
		}
	}

	public String getFactv()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(factv);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(factv);
		}
		else
		{
			return factv;
		}
	}

	public String getFacseat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(facseat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(facseat);
		}
		else
		{
			return facseat;
		}
	}

	public String getFacread()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(facread);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(facread);
		}
		else
		{
			return facread;
		}
	}

	public String getFacvpark()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(facvpark);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(facvpark);
		}
		else
		{
			return facvpark;
		}
	}

	public String getFacaircon()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(facaircon);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(facaircon);
		}
		else
		{
			return facaircon;
		}
	}

	public String getFacfood()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(facfood);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(facfood);
		}
		else
		{
			return facfood;
		}
	}

	public String getFacoth()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(facoth);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(facoth);
		}
		else
		{
			return facoth;
		}
	}

	public String getFacdk()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(facdk);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(facdk);
		}
		else
		{
			return facdk;
		}
	}

	public String getFacothv()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(facothv);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(facothv);
		}
		else
		{
			return facothv;
		}
	}

	public String getStartsf()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(startsf);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(startsf);
		}
		else
		{
			return startsf;
		}
	}

	public String getSfdrive()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sfdrive);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sfdrive);
		}
		else
		{
			return sfdrive;
		}
	}

	public String getSflocat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sflocat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sflocat);
		}
		else
		{
			return sflocat;
		}
	}

	public String getSfclean()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sfclean);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sfclean);
		}
		else
		{
			return sfclean;
		}
	}

	public String getSfcomfy()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sfcomfy);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sfcomfy);
		}
		else
		{
			return sfcomfy;
		}
	}

	public String getSfover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sfover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sfover);
		}
		else
		{
			return sfover;
		}
	}

	public String getDelivery()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(delivery);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(delivery);
		}
		else
		{
			return delivery;
		}
	}

	public String getPayment()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(payment);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(payment);
		}
		else
		{
			return payment;
		}
	}

	public String getPayamt()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(payamt);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(payamt);
		}
		else
		{
			return payamt;
		}
	}

	public String getInfready()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(infready);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(infready);
		}
		else
		{
			return infready;
		}
	}

	public String getExplwrka()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(explwrka);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(explwrka);
		}
		else
		{
			return explwrka;
		}
	}

	public String getSareview()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sareview);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sareview);
		}
		else
		{
			return sareview;
		}
	}

	public String getSaadvice()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(saadvice);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(saadvice);
		}
		else
		{
			return saadvice;
		}
	}

	public String getExplchrg()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(explchrg);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(explchrg);
		}
		else
		{
			return explchrg;
		}
	}

	public String getSchdnext()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(schdnext);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(schdnext);
		}
		else
		{
			return schdnext;
		}
	}

	public String getReastime()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(reastime);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(reastime);
		}
		else
		{
			return reastime;
		}
	}

	public String getSvctimh()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(svctimh);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(svctimh);
		}
		else
		{
			return svctimh;
		}
	}

	public String getSvctimd()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(svctimd);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(svctimd);
		}
		else
		{
			return svctimd;
		}
	}

	public String getRdyprom()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(rdyprom);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(rdyprom);
		}
		else
		{
			return rdyprom;
		}
	}

	public String getStartvp()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(startvp);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(startvp);
		}
		else
		{
			return startvp;
		}
	}

	public String getVptime()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(vptime);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(vptime);
		}
		else
		{
			return vptime;
		}
	}

	public String getVpfair()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(vpfair);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(vpfair);
		}
		else
		{
			return vpfair;
		}
	}

	public String getVpstaff()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(vpstaff);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(vpstaff);
		}
		else
		{
			return vpstaff;
		}
	}

	public String getVpover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(vpover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(vpover);
		}
		else
		{
			return vpover;
		}
	}

	public String getWdright()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(wdright);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(wdright);
		}
		else
		{
			return wdright;
		}
	}

	public String getCompreq()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(compreq);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(compreq);
		}
		else
		{
			return compreq;
		}
	}

	public String getVehwash()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(vehwash);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(vehwash);
		}
		else
		{
			return vehwash;
		}
	}

	public String getContact()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(contact);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(contact);
		}
		else
		{
			return contact;
		}
	}

	public String getRepprob()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(repprob);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(repprob);
		}
		else
		{
			return repprob;
		}
	}

	public String getDlrresol()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dlrresol);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dlrresol);
		}
		else
		{
			return dlrresol;
		}
	}

	public String getStartsq()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(startsq);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(startsq);
		}
		else
		{
			return startsq;
		}
	}

	public String getSqtime()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sqtime);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sqtime);
		}
		else
		{
			return sqtime;
		}
	}

	public String getSqthor()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sqthor);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sqthor);
		}
		else
		{
			return sqthor;
		}
	}

	public String getSqclean()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sqclean);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sqclean);
		}
		else
		{
			return sqclean;
		}
	}

	public String getSqover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sqover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sqover);
		}
		else
		{
			return sqover;
		}
	}

	public String getOsat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(osat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(osat);
		}
		else
		{
			return osat;
		}
	}

	public String getSatford()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(satford);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(satford);
		}
		else
		{
			return satford;
		}
	}

	public String getSvcexp()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(svcexp);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(svcexp);
		}
		else
		{
			return svcexp;
		}
	}

	public String getRecodlr()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(recodlr);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(recodlr);
		}
		else
		{
			return recodlr;
		}
	}

	public String getRevwar()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(revwar);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(revwar);
		}
		else
		{
			return revwar;
		}
	}

	public String getRevpwar()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(revpwar);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(revpwar);
		}
		else
		{
			return revpwar;
		}
	}

	public String getPurchase()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(purchase);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(purchase);
		}
		else
		{
			return purchase;
		}
	}

	public String getRecomake()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(recomake);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(recomake);
		}
		else
		{
			return recomake;
		}
	}

	public String getReprmake()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(reprmake);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(reprmake);
		}
		else
		{
			return reprmake;
		}
	}

	public String getDlrstand()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dlrstand);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dlrstand);
		}
		else
		{
			return dlrstand;
		}
	}

	public String getReascost()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(reascost);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(reascost);
		}
		else
		{
			return reascost;
		}
	}

	public String getEmodeal()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(emodeal);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(emodeal);
		}
		else
		{
			return emodeal;
		}
	}

	public String getEmoford()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(emoford);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(emoford);
		}
		else
		{
			return emoford;
		}
	}

	public String getSvcnauth()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(svcnauth);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(svcnauth);
		}
		else
		{
			return svcnauth;
		}
	}

	public String getProblems()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(problems);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(problems);
		}
		else
		{
			return problems;
		}
	}

	public String getNumprob()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(numprob);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(numprob);
		}
		else
		{
			return numprob;
		}
	}

	public String getToldds()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(toldds);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(toldds);
		}
		else
		{
			return toldds;
		}
	}

	public String getToldcc()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(toldcc);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(toldcc);
		}
		else
		{
			return toldcc;
		}
	}

	public String getFc()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(fc);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(fc);
		}
		else
		{
			return fc;
		}
	}

	public String getRevres()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(revres);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(revres);
		}
		else
		{
			return revres;
		}
	}

	public String getFeedbk()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(feedbk);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(feedbk);
		}
		else
		{
			return feedbk;
		}
	}

	public String getGender()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(gender);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(gender);
		}
		else
		{
			return gender;
		}
	}

	public String getDtmn()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dtmn);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dtmn);
		}
		else
		{
			return dtmn;
		}
	}

	public String getIntid()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(intid);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(intid);
		}
		else
		{
			return intid;
		}
	}

	public String getIntgen()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(intgen);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(intgen);
		}
		else
		{
			return intgen;
		}
	}

	public String getLmn()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(lmn);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(lmn);
		}
		else
		{
			return lmn;
		}
	}

	public String getIntme()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(intme);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(intme);
		}
		else
		{
			return intme;
		}
	}

	public String getDdate()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ddate);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ddate);
		}
		else
		{
			return ddate;
		}
	}

	public String getSvcdate()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(svcdate);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(svcdate);
		}
		else
		{
			return svcdate;
		}
	}

	public String getDpwaitcln()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dpwaitcln);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dpwaitcln);
		}
		else
		{
			return dpwaitcln;
		}
	}

	public String getDpcleanlou()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dpcleanlou);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dpcleanlou);
		}
		else
		{
			return dpcleanlou;
		}
	}

	public String getDpsfdrive()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dpsfdrive);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dpsfdrive);
		}
		else
		{
			return dpsfdrive;
		}
	}

	public String getDpsflocat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dpsflocat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dpsflocat);
		}
		else
		{
			return dpsflocat;
		}
	}

	public String getDpsfclean()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dpsfclean);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dpsfclean);
		}
		else
		{
			return dpsfclean;
		}
	}

	public String getDpsfcomfy()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dpsfcomfy);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dpsfcomfy);
		}
		else
		{
			return dpsfcomfy;
		}
	}

	public String getDpsfover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dpsfover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dpsfover);
		}
		else
		{
			return dpsfover;
		}
	}

	public String getIrecodestart()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(irecodestart);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(irecodestart);
		}
		else
		{
			return irecodestart;
		}
	}

	public String getNpayamt()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(npayamt);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(npayamt);
		}
		else
		{
			return npayamt;
		}
	}

	public String getNnpayamt()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nnpayamt);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nnpayamt);
		}
		else
		{
			return nnpayamt;
		}
	}

	public String getNsvctimd()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsvctimd);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsvctimd);
		}
		else
		{
			return nsvctimd;
		}
	}

	public String getNnsvctimd()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nnsvctimd);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nnsvctimd);
		}
		else
		{
			return nnsvctimd;
		}
	}

	public String getNsvctimh()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsvctimh);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsvctimh);
		}
		else
		{
			return nsvctimh;
		}
	}

	public String getNnsvctimh()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nnsvctimh);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nnsvctimh);
		}
		else
		{
			return nnsvctimh;
		}
	}

	public String getNnumprob()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nnumprob);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nnumprob);
		}
		else
		{
			return nnumprob;
		}
	}

	public String getNhndovr()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nhndovr);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nhndovr);
		}
		else
		{
			return nhndovr;
		}
	}

	public String getIrecodeend()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(irecodeend);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(irecodeend);
		}
		else
		{
			return irecodeend;
		}
	}

	public String getIntdate()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(intdate);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(intdate);
		}
		else
		{
			return intdate;
		}
	}

	public String getRecodestart()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(recodestart);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(recodestart);
		}
		else
		{
			return recodestart;
		}
	}

	public String getTsisched()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tsisched);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tsisched);
		}
		else
		{
			return tsisched;
		}
	}

	public String getTsiflex()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tsiflex);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tsiflex);
		}
		else
		{
			return tsiflex;
		}
	}

	public String getTsitime()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tsitime);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tsitime);
		}
		else
		{
			return tsitime;
		}
	}

	public String getTsiover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tsiover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tsiover);
		}
		else
		{
			return tsiover;
		}
	}

	public String getTsafren()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tsafren);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tsafren);
		}
		else
		{
			return tsafren;
		}
	}

	public String getTsaresp()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tsaresp);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tsaresp);
		}
		else
		{
			return tsaresp;
		}
	}

	public String getTsathor()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tsathor);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tsathor);
		}
		else
		{
			return tsathor;
		}
	}

	public String getTsaover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tsaover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tsaover);
		}
		else
		{
			return tsaover;
		}
	}

	public String getTsfdrive()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tsfdrive);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tsfdrive);
		}
		else
		{
			return tsfdrive;
		}
	}

	public String getTsflocat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tsflocat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tsflocat);
		}
		else
		{
			return tsflocat;
		}
	}

	public String getTsfclean()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tsfclean);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tsfclean);
		}
		else
		{
			return tsfclean;
		}
	}

	public String getTsfcomfy()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tsfcomfy);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tsfcomfy);
		}
		else
		{
			return tsfcomfy;
		}
	}

	public String getTsfover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tsfover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tsfover);
		}
		else
		{
			return tsfover;
		}
	}

	public String getTvptime()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tvptime);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tvptime);
		}
		else
		{
			return tvptime;
		}
	}

	public String getTvpfair()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tvpfair);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tvpfair);
		}
		else
		{
			return tvpfair;
		}
	}

	public String getTvpstaff()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tvpstaff);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tvpstaff);
		}
		else
		{
			return tvpstaff;
		}
	}

	public String getTvpover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tvpover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tvpover);
		}
		else
		{
			return tvpover;
		}
	}

	public String getTsqtime()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tsqtime);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tsqtime);
		}
		else
		{
			return tsqtime;
		}
	}

	public String getTsqthor()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tsqthor);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tsqthor);
		}
		else
		{
			return tsqthor;
		}
	}

	public String getTsqclean()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tsqclean);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tsqclean);
		}
		else
		{
			return tsqclean;
		}
	}

	public String getTsqover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tsqover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tsqover);
		}
		else
		{
			return tsqover;
		}
	}

	public String getTosat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tosat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tosat);
		}
		else
		{
			return tosat;
		}
	}

	public String getTsatford()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(tsatford);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(tsatford);
		}
		else
		{
			return tsatford;
		}
	}

	public String getNsisched()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsisched);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsisched);
		}
		else
		{
			return nsisched;
		}
	}

	public String getNsiflex()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsiflex);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsiflex);
		}
		else
		{
			return nsiflex;
		}
	}

	public String getNsitime()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsitime);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsitime);
		}
		else
		{
			return nsitime;
		}
	}

	public String getNsiover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsiover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsiover);
		}
		else
		{
			return nsiover;
		}
	}

	public String getNsafren()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsafren);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsafren);
		}
		else
		{
			return nsafren;
		}
	}

	public String getNsaresp()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsaresp);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsaresp);
		}
		else
		{
			return nsaresp;
		}
	}

	public String getNsathor()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsathor);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsathor);
		}
		else
		{
			return nsathor;
		}
	}

	public String getNsaover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsaover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsaover);
		}
		else
		{
			return nsaover;
		}
	}

	public String getNsfdrive()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsfdrive);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsfdrive);
		}
		else
		{
			return nsfdrive;
		}
	}

	public String getNsflocat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsflocat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsflocat);
		}
		else
		{
			return nsflocat;
		}
	}

	public String getNsfclean()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsfclean);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsfclean);
		}
		else
		{
			return nsfclean;
		}
	}

	public String getNsfcomfy()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsfcomfy);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsfcomfy);
		}
		else
		{
			return nsfcomfy;
		}
	}

	public String getNsfover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsfover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsfover);
		}
		else
		{
			return nsfover;
		}
	}

	public String getNvptime()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nvptime);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nvptime);
		}
		else
		{
			return nvptime;
		}
	}

	public String getNvpfair()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nvpfair);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nvpfair);
		}
		else
		{
			return nvpfair;
		}
	}

	public String getNvpstaff()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nvpstaff);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nvpstaff);
		}
		else
		{
			return nvpstaff;
		}
	}

	public String getNvpover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nvpover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nvpover);
		}
		else
		{
			return nvpover;
		}
	}

	public String getNsqtime()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsqtime);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsqtime);
		}
		else
		{
			return nsqtime;
		}
	}

	public String getNsqthor()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsqthor);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsqthor);
		}
		else
		{
			return nsqthor;
		}
	}

	public String getNsqclean()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsqclean);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsqclean);
		}
		else
		{
			return nsqclean;
		}
	}

	public String getNsqover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsqover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsqover);
		}
		else
		{
			return nsqover;
		}
	}

	public String getNosat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nosat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nosat);
		}
		else
		{
			return nosat;
		}
	}

	public String getNsatford()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsatford);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsatford);
		}
		else
		{
			return nsatford;
		}
	}

	public String getAsisched()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(asisched);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(asisched);
		}
		else
		{
			return asisched;
		}
	}

	public String getAsiflex()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(asiflex);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(asiflex);
		}
		else
		{
			return asiflex;
		}
	}

	public String getAsitime()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(asitime);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(asitime);
		}
		else
		{
			return asitime;
		}
	}

	public String getAsiover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(asiover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(asiover);
		}
		else
		{
			return asiover;
		}
	}

	public String getAsafren()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(asafren);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(asafren);
		}
		else
		{
			return asafren;
		}
	}

	public String getAsaresp()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(asaresp);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(asaresp);
		}
		else
		{
			return asaresp;
		}
	}

	public String getAsathor()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(asathor);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(asathor);
		}
		else
		{
			return asathor;
		}
	}

	public String getAsaover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(asaover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(asaover);
		}
		else
		{
			return asaover;
		}
	}

	public String getAsfdrive()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(asfdrive);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(asfdrive);
		}
		else
		{
			return asfdrive;
		}
	}

	public String getAsflocat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(asflocat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(asflocat);
		}
		else
		{
			return asflocat;
		}
	}

	public String getAsfclean()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(asfclean);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(asfclean);
		}
		else
		{
			return asfclean;
		}
	}

	public String getAsfcomfy()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(asfcomfy);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(asfcomfy);
		}
		else
		{
			return asfcomfy;
		}
	}

	public String getAsfover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(asfover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(asfover);
		}
		else
		{
			return asfover;
		}
	}

	public String getAvptime()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(avptime);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(avptime);
		}
		else
		{
			return avptime;
		}
	}

	public String getAvpfair()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(avpfair);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(avpfair);
		}
		else
		{
			return avpfair;
		}
	}

	public String getAvpstaff()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(avpstaff);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(avpstaff);
		}
		else
		{
			return avpstaff;
		}
	}

	public String getAvpover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(avpover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(avpover);
		}
		else
		{
			return avpover;
		}
	}

	public String getAsqtime()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(asqtime);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(asqtime);
		}
		else
		{
			return asqtime;
		}
	}

	public String getAsqthor()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(asqthor);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(asqthor);
		}
		else
		{
			return asqthor;
		}
	}

	public String getAsqclean()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(asqclean);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(asqclean);
		}
		else
		{
			return asqclean;
		}
	}

	public String getAsqover()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(asqover);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(asqover);
		}
		else
		{
			return asqover;
		}
	}

	public String getAosat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(aosat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(aosat);
		}
		else
		{
			return aosat;
		}
	}

	public String getAsatford()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(asatford);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(asatford);
		}
		else
		{
			return asatford;
		}
	}

	public String getMthown()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(mthown);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(mthown);
		}
		else
		{
			return mthown;
		}
	}

	public String getTimeoilh()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(timeoilh);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(timeoilh);
		}
		else
		{
			return timeoilh;
		}
	}

	public String getTimeoild()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(timeoild);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(timeoild);
		}
		else
		{
			return timeoild;
		}
	}

	public String getTimermh()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(timermh);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(timermh);
		}
		else
		{
			return timermh;
		}
	}

	public String getTimermd()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(timermd);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(timermd);
		}
		else
		{
			return timermd;
		}
	}

	public String getTimeerh()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(timeerh);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(timeerh);
		}
		else
		{
			return timeerh;
		}
	}

	public String getTimeerd()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(timeerd);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(timeerd);
		}
		else
		{
			return timeerd;
		}
	}

	public String getTimenerh()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(timenerh);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(timenerh);
		}
		else
		{
			return timenerh;
		}
	}

	public String getTimenerd()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(timenerd);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(timenerd);
		}
		else
		{
			return timenerd;
		}
	}

	public String getTimebph()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(timebph);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(timebph);
		}
		else
		{
			return timebph;
		}
	}

	public String getTimebpd()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(timebpd);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(timebpd);
		}
		else
		{
			return timebpd;
		}
	}

	public String getNsahist()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsahist);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsahist);
		}
		else
		{
			return nsahist;
		}
	}

	public String getNrdyprom()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nrdyprom);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nrdyprom);
		}
		else
		{
			return nrdyprom;
		}
	}

	public String getNvehwash()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nvehwash);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nvehwash);
		}
		else
		{
			return nvehwash;
		}
	}

	public String getNpayment()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(npayment);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(npayment);
		}
		else
		{
			return npayment;
		}
	}

	public String getRightoil()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(rightoil);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(rightoil);
		}
		else
		{
			return rightoil;
		}
	}

	public String getRightrm()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(rightrm);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(rightrm);
		}
		else
		{
			return rightrm;
		}
	}

	public String getRighter()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(righter);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(righter);
		}
		else
		{
			return righter;
		}
	}

	public String getRightner()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(rightner);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(rightner);
		}
		else
		{
			return rightner;
		}
	}

	public String getRightbp()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(rightbp);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(rightbp);
		}
		else
		{
			return rightbp;
		}
	}

	public String getNwdright()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nwdright);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nwdright);
		}
		else
		{
			return nwdright;
		}
	}

	public String getNreptype()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nreptype);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nreptype);
		}
		else
		{
			return nreptype;
		}
	}

	public String getSop()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sop);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sop);
		}
		else
		{
			return sop;
		}
	}

	public String getNsop()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nsop);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nsop);
		}
		else
		{
			return nsop;
		}
	}

	public String getNnsop()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nnsop);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nnsop);
		}
		else
		{
			return nnsop;
		}
	}

	public String getRmsop()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(rmsop);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(rmsop);
		}
		else
		{
			return rmsop;
		}
	}

	public String getNrmsop()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nrmsop);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nrmsop);
		}
		else
		{
			return nrmsop;
		}
	}

	public String getNnrmsop()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nnrmsop);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nnrmsop);
		}
		else
		{
			return nnrmsop;
		}
	}

	public String getRelmeas()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(relmeas);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(relmeas);
		}
		else
		{
			return relmeas;
		}
	}

	public String getRecodeend()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(recodeend);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(recodeend);
		}
		else
		{
			return recodeend;
		}
	}

	public String getFdstart()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(fdstart);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(fdstart);
		}
		else
		{
			return fdstart;
		}
	}

	public String getDealer2()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dealer2);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dealer2);
		}
		else
		{
			return dealer2;
		}
	}

	public String getCity2()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(city2);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(city2);
		}
		else
		{
			return city2;
		}
	}

	public String getRegiona()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(regiona);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(regiona);
		}
		else
		{
			return regiona;
		}
	}

	public String getState2()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(state2);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(state2);
		}
		else
		{
			return state2;
		}
	}

	public String getCity3()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(city3);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(city3);
		}
		else
		{
			return city3;
		}
	}

	public String getModel2()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(model2);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(model2);
		}
		else
		{
			return model2;
		}
	}

	public String getIndex()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(index);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(index);
		}
		else
		{
			return index;
		}
	}

	public String getSi()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(si);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(si);
		}
		else
		{
			return si;
		}
	}

	public String getSa()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sa);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sa);
		}
		else
		{
			return sa;
		}
	}

	public String getSf()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sf);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sf);
		}
		else
		{
			return sf;
		}
	}

	public String getVp()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(vp);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(vp);
		}
		else
		{
			return vp;
		}
	}

	public String getSq()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sq);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sq);
		}
		else
		{
			return sq;
		}
	}

	public String getNindex()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(nindex);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(nindex);
		}
		else
		{
			return nindex;
		}
	}

	public String getBiweekly()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(biweekly);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(biweekly);
		}
		else
		{
			return biweekly;
		}
	}

	public String getMonth()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(month);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(month);
		}
		else
		{
			return month;
		}
	}

	public String getQuarter()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(quarter);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(quarter);
		}
		else
		{
			return quarter;
		}
	}

	public String getYear()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(year);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(year);
		}
		else
		{
			return year;
		}
	}

	public String getRstatus()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(rstatus);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(rstatus);
		}
		else
		{
			return rstatus;
		}
	}

	public String getLastacc()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(lastacc);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(lastacc);
		}
		else
		{
			return lastacc;
		}
	}

	public String getCreatedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdby);
		}
		else
		{
			return createdby;
		}
	}

	public String getCreatedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdat);
		}
		else
		{
			return createdat;
		}
	}

	public String getModifiedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedat);
		}
		else
		{
			return modifiedat;
		}
	}

	public String getModifiedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedby);
		}
		else
		{
			return modifiedby;
		}
	}

	public String getClosetype()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(closetype);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(closetype);
		}
		else
		{
			return closetype;
		}
	}

	public String getClosedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(closedat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(closedat);
		}
		else
		{
			return closedat;
		}
	}


	public void setId(String value)
	{
		id = value;
	}

	public void setSrno(String value)
	{
		srno = value;
	}

	public void setDbsrno(String value)
	{
		dbsrno = value;
	}

	public void setTrack(String value)
	{
		track = value;
	}

	public void setListenobs(String value)
	{
		listenobs = value;
	}

	public void setIntadt(String value)
	{
		intadt = value;
	}

	public void setTapeadt(String value)
	{
		tapeadt = value;
	}

	public void setThirdadt(String value)
	{
		thirdadt = value;
	}

	public void setCityres(String value)
	{
		cityres = value;
	}

	public void setDbsvdate(String value)
	{
		dbsvdate = value;
	}

	public void setDbpdate(String value)
	{
		dbpdate = value;
	}

	public void setCaruse(String value)
	{
		caruse = value;
	}

	public void setDbdealer(String value)
	{
		dbdealer = value;
	}

	public void setScdlr(String value)
	{
		scdlr = value;
	}

	public void setSvdate(String value)
	{
		svdate = value;
	}

	public void setRsvcdate(String value)
	{
		rsvcdate = value;
	}

	public void setOwner(String value)
	{
		owner = value;
	}

	public void setDropveh(String value)
	{
		dropveh = value;
	}

	public void setPickveh(String value)
	{
		pickveh = value;
	}

	public void setOilchng(String value)
	{
		oilchng = value;
	}

	public void setMaintain(String value)
	{
		maintain = value;
	}

	public void setEmergrep(String value)
	{
		emergrep = value;
	}

	public void setNemerrep(String value)
	{
		nemerrep = value;
	}

	public void setBpaintrep(String value)
	{
		bpaintrep = value;
	}

	public void setMrdk(String value)
	{
		mrdk = value;
	}

	public void setSvctime(String value)
	{
		svctime = value;
	}

	public void setCddate(String value)
	{
		cddate = value;
	}

	public void setScddate(String value)
	{
		scddate = value;
	}

	public void setQrot(String value)
	{
		qrot = value;
	}

	public void setNotified(String value)
	{
		notified = value;
	}

	public void setSchedule(String value)
	{
		schedule = value;
	}

	public void setAptdesir(String value)
	{
		aptdesir = value;
	}

	public void setHndovr(String value)
	{
		hndovr = value;
	}

	public void setStartsi(String value)
	{
		startsi = value;
	}

	public void setSisched(String value)
	{
		sisched = value;
	}

	public void setSiflex(String value)
	{
		siflex = value;
	}

	public void setSitime(String value)
	{
		sitime = value;
	}

	public void setSiover(String value)
	{
		siover = value;
	}

	public void setGreetwk(String value)
	{
		greetwk = value;
	}

	public void setReaswait(String value)
	{
		reaswait = value;
	}

	public void setSaease(String value)
	{
		saease = value;
	}

	public void setSaadd(String value)
	{
		saadd = value;
	}

	public void setSaneat(String value)
	{
		saneat = value;
	}

	public void setSaattn(String value)
	{
		saattn = value;
	}

	public void setSarepeat(String value)
	{
		sarepeat = value;
	}

	public void setSafocus(String value)
	{
		safocus = value;
	}

	public void setSawalk(String value)
	{
		sawalk = value;
	}

	public void setSastatus(String value)
	{
		sastatus = value;
	}

	public void setSahist(String value)
	{
		sahist = value;
	}

	public void setPriorexp(String value)
	{
		priorexp = value;
	}

	public void setPriorfm(String value)
	{
		priorfm = value;
	}

	public void setEstimate(String value)
	{
		estimate = value;
	}

	public void setGivready(String value)
	{
		givready = value;
	}

	public void setStartsa(String value)
	{
		startsa = value;
	}

	public void setSafren(String value)
	{
		safren = value;
	}

	public void setSaresp(String value)
	{
		saresp = value;
	}

	public void setSathor(String value)
	{
		sathor = value;
	}

	public void setSaover(String value)
	{
		saover = value;
	}

	public void setStayleft(String value)
	{
		stayleft = value;
	}

	public void setOfftran(String value)
	{
		offtran = value;
	}

	public void setWaitcln(String value)
	{
		waitcln = value;
	}

	public void setCleanlou(String value)
	{
		cleanlou = value;
	}

	public void setConvop(String value)
	{
		convop = value;
	}

	public void setFacweb(String value)
	{
		facweb = value;
	}

	public void setFacrefr(String value)
	{
		facrefr = value;
	}

	public void setFactv(String value)
	{
		factv = value;
	}

	public void setFacseat(String value)
	{
		facseat = value;
	}

	public void setFacread(String value)
	{
		facread = value;
	}

	public void setFacvpark(String value)
	{
		facvpark = value;
	}

	public void setFacaircon(String value)
	{
		facaircon = value;
	}

	public void setFacfood(String value)
	{
		facfood = value;
	}

	public void setFacoth(String value)
	{
		facoth = value;
	}

	public void setFacdk(String value)
	{
		facdk = value;
	}

	public void setFacothv(String value)
	{
		facothv = value;
	}

	public void setStartsf(String value)
	{
		startsf = value;
	}

	public void setSfdrive(String value)
	{
		sfdrive = value;
	}

	public void setSflocat(String value)
	{
		sflocat = value;
	}

	public void setSfclean(String value)
	{
		sfclean = value;
	}

	public void setSfcomfy(String value)
	{
		sfcomfy = value;
	}

	public void setSfover(String value)
	{
		sfover = value;
	}

	public void setDelivery(String value)
	{
		delivery = value;
	}

	public void setPayment(String value)
	{
		payment = value;
	}

	public void setPayamt(String value)
	{
		payamt = value;
	}

	public void setInfready(String value)
	{
		infready = value;
	}

	public void setExplwrka(String value)
	{
		explwrka = value;
	}

	public void setSareview(String value)
	{
		sareview = value;
	}

	public void setSaadvice(String value)
	{
		saadvice = value;
	}

	public void setExplchrg(String value)
	{
		explchrg = value;
	}

	public void setSchdnext(String value)
	{
		schdnext = value;
	}

	public void setReastime(String value)
	{
		reastime = value;
	}

	public void setSvctimh(String value)
	{
		svctimh = value;
	}

	public void setSvctimd(String value)
	{
		svctimd = value;
	}

	public void setRdyprom(String value)
	{
		rdyprom = value;
	}

	public void setStartvp(String value)
	{
		startvp = value;
	}

	public void setVptime(String value)
	{
		vptime = value;
	}

	public void setVpfair(String value)
	{
		vpfair = value;
	}

	public void setVpstaff(String value)
	{
		vpstaff = value;
	}

	public void setVpover(String value)
	{
		vpover = value;
	}

	public void setWdright(String value)
	{
		wdright = value;
	}

	public void setCompreq(String value)
	{
		compreq = value;
	}

	public void setVehwash(String value)
	{
		vehwash = value;
	}

	public void setContact(String value)
	{
		contact = value;
	}

	public void setRepprob(String value)
	{
		repprob = value;
	}

	public void setDlrresol(String value)
	{
		dlrresol = value;
	}

	public void setStartsq(String value)
	{
		startsq = value;
	}

	public void setSqtime(String value)
	{
		sqtime = value;
	}

	public void setSqthor(String value)
	{
		sqthor = value;
	}

	public void setSqclean(String value)
	{
		sqclean = value;
	}

	public void setSqover(String value)
	{
		sqover = value;
	}

	public void setOsat(String value)
	{
		osat = value;
	}

	public void setSatford(String value)
	{
		satford = value;
	}

	public void setSvcexp(String value)
	{
		svcexp = value;
	}

	public void setRecodlr(String value)
	{
		recodlr = value;
	}

	public void setRevwar(String value)
	{
		revwar = value;
	}

	public void setRevpwar(String value)
	{
		revpwar = value;
	}

	public void setPurchase(String value)
	{
		purchase = value;
	}

	public void setRecomake(String value)
	{
		recomake = value;
	}

	public void setReprmake(String value)
	{
		reprmake = value;
	}

	public void setDlrstand(String value)
	{
		dlrstand = value;
	}

	public void setReascost(String value)
	{
		reascost = value;
	}

	public void setEmodeal(String value)
	{
		emodeal = value;
	}

	public void setEmoford(String value)
	{
		emoford = value;
	}

	public void setSvcnauth(String value)
	{
		svcnauth = value;
	}

	public void setProblems(String value)
	{
		problems = value;
	}

	public void setNumprob(String value)
	{
		numprob = value;
	}

	public void setToldds(String value)
	{
		toldds = value;
	}

	public void setToldcc(String value)
	{
		toldcc = value;
	}

	public void setFc(String value)
	{
		fc = value;
	}

	public void setRevres(String value)
	{
		revres = value;
	}

	public void setFeedbk(String value)
	{
		feedbk = value;
	}

	public void setGender(String value)
	{
		gender = value;
	}

	public void setDtmn(String value)
	{
		dtmn = value;
	}

	public void setIntid(String value)
	{
		intid = value;
	}

	public void setIntgen(String value)
	{
		intgen = value;
	}

	public void setLmn(String value)
	{
		lmn = value;
	}

	public void setIntme(String value)
	{
		intme = value;
	}

	public void setDdate(String value)
	{
		ddate = value;
	}

	public void setSvcdate(String value)
	{
		svcdate = value;
	}

	public void setDpwaitcln(String value)
	{
		dpwaitcln = value;
	}

	public void setDpcleanlou(String value)
	{
		dpcleanlou = value;
	}

	public void setDpsfdrive(String value)
	{
		dpsfdrive = value;
	}

	public void setDpsflocat(String value)
	{
		dpsflocat = value;
	}

	public void setDpsfclean(String value)
	{
		dpsfclean = value;
	}

	public void setDpsfcomfy(String value)
	{
		dpsfcomfy = value;
	}

	public void setDpsfover(String value)
	{
		dpsfover = value;
	}

	public void setIrecodestart(String value)
	{
		irecodestart = value;
	}

	public void setNpayamt(String value)
	{
		npayamt = value;
	}

	public void setNnpayamt(String value)
	{
		nnpayamt = value;
	}

	public void setNsvctimd(String value)
	{
		nsvctimd = value;
	}

	public void setNnsvctimd(String value)
	{
		nnsvctimd = value;
	}

	public void setNsvctimh(String value)
	{
		nsvctimh = value;
	}

	public void setNnsvctimh(String value)
	{
		nnsvctimh = value;
	}

	public void setNnumprob(String value)
	{
		nnumprob = value;
	}

	public void setNhndovr(String value)
	{
		nhndovr = value;
	}

	public void setIrecodeend(String value)
	{
		irecodeend = value;
	}

	public void setIntdate(String value)
	{
		intdate = value;
	}

	public void setRecodestart(String value)
	{
		recodestart = value;
	}

	public void setTsisched(String value)
	{
		tsisched = value;
	}

	public void setTsiflex(String value)
	{
		tsiflex = value;
	}

	public void setTsitime(String value)
	{
		tsitime = value;
	}

	public void setTsiover(String value)
	{
		tsiover = value;
	}

	public void setTsafren(String value)
	{
		tsafren = value;
	}

	public void setTsaresp(String value)
	{
		tsaresp = value;
	}

	public void setTsathor(String value)
	{
		tsathor = value;
	}

	public void setTsaover(String value)
	{
		tsaover = value;
	}

	public void setTsfdrive(String value)
	{
		tsfdrive = value;
	}

	public void setTsflocat(String value)
	{
		tsflocat = value;
	}

	public void setTsfclean(String value)
	{
		tsfclean = value;
	}

	public void setTsfcomfy(String value)
	{
		tsfcomfy = value;
	}

	public void setTsfover(String value)
	{
		tsfover = value;
	}

	public void setTvptime(String value)
	{
		tvptime = value;
	}

	public void setTvpfair(String value)
	{
		tvpfair = value;
	}

	public void setTvpstaff(String value)
	{
		tvpstaff = value;
	}

	public void setTvpover(String value)
	{
		tvpover = value;
	}

	public void setTsqtime(String value)
	{
		tsqtime = value;
	}

	public void setTsqthor(String value)
	{
		tsqthor = value;
	}

	public void setTsqclean(String value)
	{
		tsqclean = value;
	}

	public void setTsqover(String value)
	{
		tsqover = value;
	}

	public void setTosat(String value)
	{
		tosat = value;
	}

	public void setTsatford(String value)
	{
		tsatford = value;
	}

	public void setNsisched(String value)
	{
		nsisched = value;
	}

	public void setNsiflex(String value)
	{
		nsiflex = value;
	}

	public void setNsitime(String value)
	{
		nsitime = value;
	}

	public void setNsiover(String value)
	{
		nsiover = value;
	}

	public void setNsafren(String value)
	{
		nsafren = value;
	}

	public void setNsaresp(String value)
	{
		nsaresp = value;
	}

	public void setNsathor(String value)
	{
		nsathor = value;
	}

	public void setNsaover(String value)
	{
		nsaover = value;
	}

	public void setNsfdrive(String value)
	{
		nsfdrive = value;
	}

	public void setNsflocat(String value)
	{
		nsflocat = value;
	}

	public void setNsfclean(String value)
	{
		nsfclean = value;
	}

	public void setNsfcomfy(String value)
	{
		nsfcomfy = value;
	}

	public void setNsfover(String value)
	{
		nsfover = value;
	}

	public void setNvptime(String value)
	{
		nvptime = value;
	}

	public void setNvpfair(String value)
	{
		nvpfair = value;
	}

	public void setNvpstaff(String value)
	{
		nvpstaff = value;
	}

	public void setNvpover(String value)
	{
		nvpover = value;
	}

	public void setNsqtime(String value)
	{
		nsqtime = value;
	}

	public void setNsqthor(String value)
	{
		nsqthor = value;
	}

	public void setNsqclean(String value)
	{
		nsqclean = value;
	}

	public void setNsqover(String value)
	{
		nsqover = value;
	}

	public void setNosat(String value)
	{
		nosat = value;
	}

	public void setNsatford(String value)
	{
		nsatford = value;
	}

	public void setAsisched(String value)
	{
		asisched = value;
	}

	public void setAsiflex(String value)
	{
		asiflex = value;
	}

	public void setAsitime(String value)
	{
		asitime = value;
	}

	public void setAsiover(String value)
	{
		asiover = value;
	}

	public void setAsafren(String value)
	{
		asafren = value;
	}

	public void setAsaresp(String value)
	{
		asaresp = value;
	}

	public void setAsathor(String value)
	{
		asathor = value;
	}

	public void setAsaover(String value)
	{
		asaover = value;
	}

	public void setAsfdrive(String value)
	{
		asfdrive = value;
	}

	public void setAsflocat(String value)
	{
		asflocat = value;
	}

	public void setAsfclean(String value)
	{
		asfclean = value;
	}

	public void setAsfcomfy(String value)
	{
		asfcomfy = value;
	}

	public void setAsfover(String value)
	{
		asfover = value;
	}

	public void setAvptime(String value)
	{
		avptime = value;
	}

	public void setAvpfair(String value)
	{
		avpfair = value;
	}

	public void setAvpstaff(String value)
	{
		avpstaff = value;
	}

	public void setAvpover(String value)
	{
		avpover = value;
	}

	public void setAsqtime(String value)
	{
		asqtime = value;
	}

	public void setAsqthor(String value)
	{
		asqthor = value;
	}

	public void setAsqclean(String value)
	{
		asqclean = value;
	}

	public void setAsqover(String value)
	{
		asqover = value;
	}

	public void setAosat(String value)
	{
		aosat = value;
	}

	public void setAsatford(String value)
	{
		asatford = value;
	}

	public void setMthown(String value)
	{
		mthown = value;
	}

	public void setTimeoilh(String value)
	{
		timeoilh = value;
	}

	public void setTimeoild(String value)
	{
		timeoild = value;
	}

	public void setTimermh(String value)
	{
		timermh = value;
	}

	public void setTimermd(String value)
	{
		timermd = value;
	}

	public void setTimeerh(String value)
	{
		timeerh = value;
	}

	public void setTimeerd(String value)
	{
		timeerd = value;
	}

	public void setTimenerh(String value)
	{
		timenerh = value;
	}

	public void setTimenerd(String value)
	{
		timenerd = value;
	}

	public void setTimebph(String value)
	{
		timebph = value;
	}

	public void setTimebpd(String value)
	{
		timebpd = value;
	}

	public void setNsahist(String value)
	{
		nsahist = value;
	}

	public void setNrdyprom(String value)
	{
		nrdyprom = value;
	}

	public void setNvehwash(String value)
	{
		nvehwash = value;
	}

	public void setNpayment(String value)
	{
		npayment = value;
	}

	public void setRightoil(String value)
	{
		rightoil = value;
	}

	public void setRightrm(String value)
	{
		rightrm = value;
	}

	public void setRighter(String value)
	{
		righter = value;
	}

	public void setRightner(String value)
	{
		rightner = value;
	}

	public void setRightbp(String value)
	{
		rightbp = value;
	}

	public void setNwdright(String value)
	{
		nwdright = value;
	}

	public void setNreptype(String value)
	{
		nreptype = value;
	}

	public void setSop(String value)
	{
		sop = value;
	}

	public void setNsop(String value)
	{
		nsop = value;
	}

	public void setNnsop(String value)
	{
		nnsop = value;
	}

	public void setRmsop(String value)
	{
		rmsop = value;
	}

	public void setNrmsop(String value)
	{
		nrmsop = value;
	}

	public void setNnrmsop(String value)
	{
		nnrmsop = value;
	}

	public void setRelmeas(String value)
	{
		relmeas = value;
	}

	public void setRecodeend(String value)
	{
		recodeend = value;
	}

	public void setFdstart(String value)
	{
		fdstart = value;
	}

	public void setDealer2(String value)
	{
		dealer2 = value;
	}

	public void setCity2(String value)
	{
		city2 = value;
	}

	public void setRegiona(String value)
	{
		regiona = value;
	}

	public void setState2(String value)
	{
		state2 = value;
	}

	public void setCity3(String value)
	{
		city3 = value;
	}

	public void setModel2(String value)
	{
		model2 = value;
	}

	public void setIndex(String value)
	{
		index = value;
	}

	public void setSi(String value)
	{
		si = value;
	}

	public void setSa(String value)
	{
		sa = value;
	}

	public void setSf(String value)
	{
		sf = value;
	}

	public void setVp(String value)
	{
		vp = value;
	}

	public void setSq(String value)
	{
		sq = value;
	}

	public void setNindex(String value)
	{
		nindex = value;
	}

	public void setBiweekly(String value)
	{
		biweekly = value;
	}

	public void setMonth(String value)
	{
		month = value;
	}

	public void setQuarter(String value)
	{
		quarter = value;
	}

	public void setYear(String value)
	{
		year = value;
	}

	public void setRstatus(String value)
	{
		rstatus = value;
	}

	public void setLastacc(String value)
	{
		lastacc = value;
	}

	public void setCreatedby(String value)
	{
		createdby = value;
	}

	public void setCreatedat(String value)
	{
		createdat = value;
	}

	public void setModifiedat(String value)
	{
		modifiedat = value;
	}

	public void setModifiedby(String value)
	{
		modifiedby = value;
	}

	public void setClosetype(String value)
	{
		closetype = value;
	}

	public void setClosedat(String value)
	{
		closedat = value;
	}


	public String toString()
	{
		return "\nid:" + id +
				"\nsrno:" + srno +
				"\ndbsrno:" + dbsrno +
				"\ntrack:" + track +
				"\nlistenobs:" + listenobs +
				"\nintadt:" + intadt +
				"\ntapeadt:" + tapeadt +
				"\nthirdadt:" + thirdadt +
				"\ncityres:" + cityres +
				"\ndbsvdate:" + dbsvdate +
				"\ndbpdate:" + dbpdate +
				"\ncaruse:" + caruse +
				"\ndbdealer:" + dbdealer +
				"\nscdlr:" + scdlr +
				"\nsvdate:" + svdate +
				"\nrsvcdate:" + rsvcdate +
				"\nowner:" + owner +
				"\ndropveh:" + dropveh +
				"\npickveh:" + pickveh +
				"\noilchng:" + oilchng +
				"\nmaintain:" + maintain +
				"\nemergrep:" + emergrep +
				"\nnemerrep:" + nemerrep +
				"\nbpaintrep:" + bpaintrep +
				"\nmrdk:" + mrdk +
				"\nsvctime:" + svctime +
				"\ncddate:" + cddate +
				"\nscddate:" + scddate +
				"\nqrot:" + qrot +
				"\nnotified:" + notified +
				"\nschedule:" + schedule +
				"\naptdesir:" + aptdesir +
				"\nhndovr:" + hndovr +
				"\nstartsi:" + startsi +
				"\nsisched:" + sisched +
				"\nsiflex:" + siflex +
				"\nsitime:" + sitime +
				"\nsiover:" + siover +
				"\ngreetwk:" + greetwk +
				"\nreaswait:" + reaswait +
				"\nsaease:" + saease +
				"\nsaadd:" + saadd +
				"\nsaneat:" + saneat +
				"\nsaattn:" + saattn +
				"\nsarepeat:" + sarepeat +
				"\nsafocus:" + safocus +
				"\nsawalk:" + sawalk +
				"\nsastatus:" + sastatus +
				"\nsahist:" + sahist +
				"\npriorexp:" + priorexp +
				"\npriorfm:" + priorfm +
				"\nestimate:" + estimate +
				"\ngivready:" + givready +
				"\nstartsa:" + startsa +
				"\nsafren:" + safren +
				"\nsaresp:" + saresp +
				"\nsathor:" + sathor +
				"\nsaover:" + saover +
				"\nstayleft:" + stayleft +
				"\nofftran:" + offtran +
				"\nwaitcln:" + waitcln +
				"\ncleanlou:" + cleanlou +
				"\nconvop:" + convop +
				"\nfacweb:" + facweb +
				"\nfacrefr:" + facrefr +
				"\nfactv:" + factv +
				"\nfacseat:" + facseat +
				"\nfacread:" + facread +
				"\nfacvpark:" + facvpark +
				"\nfacaircon:" + facaircon +
				"\nfacfood:" + facfood +
				"\nfacoth:" + facoth +
				"\nfacdk:" + facdk +
				"\nfacothv:" + facothv +
				"\nstartsf:" + startsf +
				"\nsfdrive:" + sfdrive +
				"\nsflocat:" + sflocat +
				"\nsfclean:" + sfclean +
				"\nsfcomfy:" + sfcomfy +
				"\nsfover:" + sfover +
				"\ndelivery:" + delivery +
				"\npayment:" + payment +
				"\npayamt:" + payamt +
				"\ninfready:" + infready +
				"\nexplwrka:" + explwrka +
				"\nsareview:" + sareview +
				"\nsaadvice:" + saadvice +
				"\nexplchrg:" + explchrg +
				"\nschdnext:" + schdnext +
				"\nreastime:" + reastime +
				"\nsvctimh:" + svctimh +
				"\nsvctimd:" + svctimd +
				"\nrdyprom:" + rdyprom +
				"\nstartvp:" + startvp +
				"\nvptime:" + vptime +
				"\nvpfair:" + vpfair +
				"\nvpstaff:" + vpstaff +
				"\nvpover:" + vpover +
				"\nwdright:" + wdright +
				"\ncompreq:" + compreq +
				"\nvehwash:" + vehwash +
				"\ncontact:" + contact +
				"\nrepprob:" + repprob +
				"\ndlrresol:" + dlrresol +
				"\nstartsq:" + startsq +
				"\nsqtime:" + sqtime +
				"\nsqthor:" + sqthor +
				"\nsqclean:" + sqclean +
				"\nsqover:" + sqover +
				"\nosat:" + osat +
				"\nsatford:" + satford +
				"\nsvcexp:" + svcexp +
				"\nrecodlr:" + recodlr +
				"\nrevwar:" + revwar +
				"\nrevpwar:" + revpwar +
				"\npurchase:" + purchase +
				"\nrecomake:" + recomake +
				"\nreprmake:" + reprmake +
				"\ndlrstand:" + dlrstand +
				"\nreascost:" + reascost +
				"\nemodeal:" + emodeal +
				"\nemoford:" + emoford +
				"\nsvcnauth:" + svcnauth +
				"\nproblems:" + problems +
				"\nnumprob:" + numprob +
				"\ntoldds:" + toldds +
				"\ntoldcc:" + toldcc +
				"\nfc:" + fc +
				"\nrevres:" + revres +
				"\nfeedbk:" + feedbk +
				"\ngender:" + gender +
				"\ndtmn:" + dtmn +
				"\nintid:" + intid +
				"\nintgen:" + intgen +
				"\nlmn:" + lmn +
				"\nintme:" + intme +
				"\nddate:" + ddate +
				"\nsvcdate:" + svcdate +
				"\ndpwaitcln:" + dpwaitcln +
				"\ndpcleanlou:" + dpcleanlou +
				"\ndpsfdrive:" + dpsfdrive +
				"\ndpsflocat:" + dpsflocat +
				"\ndpsfclean:" + dpsfclean +
				"\ndpsfcomfy:" + dpsfcomfy +
				"\ndpsfover:" + dpsfover +
				"\nirecodestart:" + irecodestart +
				"\nnpayamt:" + npayamt +
				"\nnnpayamt:" + nnpayamt +
				"\nnsvctimd:" + nsvctimd +
				"\nnnsvctimd:" + nnsvctimd +
				"\nnsvctimh:" + nsvctimh +
				"\nnnsvctimh:" + nnsvctimh +
				"\nnnumprob:" + nnumprob +
				"\nnhndovr:" + nhndovr +
				"\nirecodeend:" + irecodeend +
				"\nintdate:" + intdate +
				"\nrecodestart:" + recodestart +
				"\ntsisched:" + tsisched +
				"\ntsiflex:" + tsiflex +
				"\ntsitime:" + tsitime +
				"\ntsiover:" + tsiover +
				"\ntsafren:" + tsafren +
				"\ntsaresp:" + tsaresp +
				"\ntsathor:" + tsathor +
				"\ntsaover:" + tsaover +
				"\ntsfdrive:" + tsfdrive +
				"\ntsflocat:" + tsflocat +
				"\ntsfclean:" + tsfclean +
				"\ntsfcomfy:" + tsfcomfy +
				"\ntsfover:" + tsfover +
				"\ntvptime:" + tvptime +
				"\ntvpfair:" + tvpfair +
				"\ntvpstaff:" + tvpstaff +
				"\ntvpover:" + tvpover +
				"\ntsqtime:" + tsqtime +
				"\ntsqthor:" + tsqthor +
				"\ntsqclean:" + tsqclean +
				"\ntsqover:" + tsqover +
				"\ntosat:" + tosat +
				"\ntsatford:" + tsatford +
				"\nnsisched:" + nsisched +
				"\nnsiflex:" + nsiflex +
				"\nnsitime:" + nsitime +
				"\nnsiover:" + nsiover +
				"\nnsafren:" + nsafren +
				"\nnsaresp:" + nsaresp +
				"\nnsathor:" + nsathor +
				"\nnsaover:" + nsaover +
				"\nnsfdrive:" + nsfdrive +
				"\nnsflocat:" + nsflocat +
				"\nnsfclean:" + nsfclean +
				"\nnsfcomfy:" + nsfcomfy +
				"\nnsfover:" + nsfover +
				"\nnvptime:" + nvptime +
				"\nnvpfair:" + nvpfair +
				"\nnvpstaff:" + nvpstaff +
				"\nnvpover:" + nvpover +
				"\nnsqtime:" + nsqtime +
				"\nnsqthor:" + nsqthor +
				"\nnsqclean:" + nsqclean +
				"\nnsqover:" + nsqover +
				"\nnosat:" + nosat +
				"\nnsatford:" + nsatford +
				"\nasisched:" + asisched +
				"\nasiflex:" + asiflex +
				"\nasitime:" + asitime +
				"\nasiover:" + asiover +
				"\nasafren:" + asafren +
				"\nasaresp:" + asaresp +
				"\nasathor:" + asathor +
				"\nasaover:" + asaover +
				"\nasfdrive:" + asfdrive +
				"\nasflocat:" + asflocat +
				"\nasfclean:" + asfclean +
				"\nasfcomfy:" + asfcomfy +
				"\nasfover:" + asfover +
				"\navptime:" + avptime +
				"\navpfair:" + avpfair +
				"\navpstaff:" + avpstaff +
				"\navpover:" + avpover +
				"\nasqtime:" + asqtime +
				"\nasqthor:" + asqthor +
				"\nasqclean:" + asqclean +
				"\nasqover:" + asqover +
				"\naosat:" + aosat +
				"\nasatford:" + asatford +
				"\nmthown:" + mthown +
				"\ntimeoilh:" + timeoilh +
				"\ntimeoild:" + timeoild +
				"\ntimermh:" + timermh +
				"\ntimermd:" + timermd +
				"\ntimeerh:" + timeerh +
				"\ntimeerd:" + timeerd +
				"\ntimenerh:" + timenerh +
				"\ntimenerd:" + timenerd +
				"\ntimebph:" + timebph +
				"\ntimebpd:" + timebpd +
				"\nnsahist:" + nsahist +
				"\nnrdyprom:" + nrdyprom +
				"\nnvehwash:" + nvehwash +
				"\nnpayment:" + npayment +
				"\nrightoil:" + rightoil +
				"\nrightrm:" + rightrm +
				"\nrighter:" + righter +
				"\nrightner:" + rightner +
				"\nrightbp:" + rightbp +
				"\nnwdright:" + nwdright +
				"\nnreptype:" + nreptype +
				"\nsop:" + sop +
				"\nnsop:" + nsop +
				"\nnnsop:" + nnsop +
				"\nrmsop:" + rmsop +
				"\nnrmsop:" + nrmsop +
				"\nnnrmsop:" + nnrmsop +
				"\nrelmeas:" + relmeas +
				"\nrecodeend:" + recodeend +
				"\nfdstart:" + fdstart +
				"\ndealer2:" + dealer2 +
				"\ncity2:" + city2 +
				"\nregiona:" + regiona +
				"\nstate2:" + state2 +
				"\ncity3:" + city3 +
				"\nmodel2:" + model2 +
				"\nindex:" + index +
				"\nsi:" + si +
				"\nsa:" + sa +
				"\nsf:" + sf +
				"\nvp:" + vp +
				"\nsq:" + sq +
				"\nnindex:" + nindex +
				"\nbiweekly:" + biweekly +
				"\nmonth:" + month +
				"\nquarter:" + quarter +
				"\nyear:" + year +
				"\nrstatus:" + rstatus +
				"\nlastacc:" + lastacc +
				"\ncreatedby:" + createdby +
				"\ncreatedat:" + createdat +
				"\nmodifiedat:" + modifiedat +
				"\nmodifiedby:" + modifiedby +
				"\nclosetype:" + closetype +
				"\nclosedat:" + closedat +
				"\n";
	}

	public void loadContent(LICsitranslatedRecord inputRecord)
	{
		setId(inputRecord.getId());
		setSrno(inputRecord.getSrno());
		setDbsrno(inputRecord.getDbsrno());
		setTrack(inputRecord.getTrack());
		setListenobs(inputRecord.getListenobs());
		setIntadt(inputRecord.getIntadt());
		setTapeadt(inputRecord.getTapeadt());
		setThirdadt(inputRecord.getThirdadt());
		setCityres(inputRecord.getCityres());
		setDbsvdate(inputRecord.getDbsvdate());
		setDbpdate(inputRecord.getDbpdate());
		setCaruse(inputRecord.getCaruse());
		setDbdealer(inputRecord.getDbdealer());
		setScdlr(inputRecord.getScdlr());
		setSvdate(inputRecord.getSvdate());
		setRsvcdate(inputRecord.getRsvcdate());
		setOwner(inputRecord.getOwner());
		setDropveh(inputRecord.getDropveh());
		setPickveh(inputRecord.getPickveh());
		setOilchng(inputRecord.getOilchng());
		setMaintain(inputRecord.getMaintain());
		setEmergrep(inputRecord.getEmergrep());
		setNemerrep(inputRecord.getNemerrep());
		setBpaintrep(inputRecord.getBpaintrep());
		setMrdk(inputRecord.getMrdk());
		setSvctime(inputRecord.getSvctime());
		setCddate(inputRecord.getCddate());
		setScddate(inputRecord.getScddate());
		setQrot(inputRecord.getQrot());
		setNotified(inputRecord.getNotified());
		setSchedule(inputRecord.getSchedule());
		setAptdesir(inputRecord.getAptdesir());
		setHndovr(inputRecord.getHndovr());
		setStartsi(inputRecord.getStartsi());
		setSisched(inputRecord.getSisched());
		setSiflex(inputRecord.getSiflex());
		setSitime(inputRecord.getSitime());
		setSiover(inputRecord.getSiover());
		setGreetwk(inputRecord.getGreetwk());
		setReaswait(inputRecord.getReaswait());
		setSaease(inputRecord.getSaease());
		setSaadd(inputRecord.getSaadd());
		setSaneat(inputRecord.getSaneat());
		setSaattn(inputRecord.getSaattn());
		setSarepeat(inputRecord.getSarepeat());
		setSafocus(inputRecord.getSafocus());
		setSawalk(inputRecord.getSawalk());
		setSastatus(inputRecord.getSastatus());
		setSahist(inputRecord.getSahist());
		setPriorexp(inputRecord.getPriorexp());
		setPriorfm(inputRecord.getPriorfm());
		setEstimate(inputRecord.getEstimate());
		setGivready(inputRecord.getGivready());
		setStartsa(inputRecord.getStartsa());
		setSafren(inputRecord.getSafren());
		setSaresp(inputRecord.getSaresp());
		setSathor(inputRecord.getSathor());
		setSaover(inputRecord.getSaover());
		setStayleft(inputRecord.getStayleft());
		setOfftran(inputRecord.getOfftran());
		setWaitcln(inputRecord.getWaitcln());
		setCleanlou(inputRecord.getCleanlou());
		setConvop(inputRecord.getConvop());
		setFacweb(inputRecord.getFacweb());
		setFacrefr(inputRecord.getFacrefr());
		setFactv(inputRecord.getFactv());
		setFacseat(inputRecord.getFacseat());
		setFacread(inputRecord.getFacread());
		setFacvpark(inputRecord.getFacvpark());
		setFacaircon(inputRecord.getFacaircon());
		setFacfood(inputRecord.getFacfood());
		setFacoth(inputRecord.getFacoth());
		setFacdk(inputRecord.getFacdk());
		setFacothv(inputRecord.getFacothv());
		setStartsf(inputRecord.getStartsf());
		setSfdrive(inputRecord.getSfdrive());
		setSflocat(inputRecord.getSflocat());
		setSfclean(inputRecord.getSfclean());
		setSfcomfy(inputRecord.getSfcomfy());
		setSfover(inputRecord.getSfover());
		setDelivery(inputRecord.getDelivery());
		setPayment(inputRecord.getPayment());
		setPayamt(inputRecord.getPayamt());
		setInfready(inputRecord.getInfready());
		setExplwrka(inputRecord.getExplwrka());
		setSareview(inputRecord.getSareview());
		setSaadvice(inputRecord.getSaadvice());
		setExplchrg(inputRecord.getExplchrg());
		setSchdnext(inputRecord.getSchdnext());
		setReastime(inputRecord.getReastime());
		setSvctimh(inputRecord.getSvctimh());
		setSvctimd(inputRecord.getSvctimd());
		setRdyprom(inputRecord.getRdyprom());
		setStartvp(inputRecord.getStartvp());
		setVptime(inputRecord.getVptime());
		setVpfair(inputRecord.getVpfair());
		setVpstaff(inputRecord.getVpstaff());
		setVpover(inputRecord.getVpover());
		setWdright(inputRecord.getWdright());
		setCompreq(inputRecord.getCompreq());
		setVehwash(inputRecord.getVehwash());
		setContact(inputRecord.getContact());
		setRepprob(inputRecord.getRepprob());
		setDlrresol(inputRecord.getDlrresol());
		setStartsq(inputRecord.getStartsq());
		setSqtime(inputRecord.getSqtime());
		setSqthor(inputRecord.getSqthor());
		setSqclean(inputRecord.getSqclean());
		setSqover(inputRecord.getSqover());
		setOsat(inputRecord.getOsat());
		setSatford(inputRecord.getSatford());
		setSvcexp(inputRecord.getSvcexp());
		setRecodlr(inputRecord.getRecodlr());
		setRevwar(inputRecord.getRevwar());
		setRevpwar(inputRecord.getRevpwar());
		setPurchase(inputRecord.getPurchase());
		setRecomake(inputRecord.getRecomake());
		setReprmake(inputRecord.getReprmake());
		setDlrstand(inputRecord.getDlrstand());
		setReascost(inputRecord.getReascost());
		setEmodeal(inputRecord.getEmodeal());
		setEmoford(inputRecord.getEmoford());
		setSvcnauth(inputRecord.getSvcnauth());
		setProblems(inputRecord.getProblems());
		setNumprob(inputRecord.getNumprob());
		setToldds(inputRecord.getToldds());
		setToldcc(inputRecord.getToldcc());
		setFc(inputRecord.getFc());
		setRevres(inputRecord.getRevres());
		setFeedbk(inputRecord.getFeedbk());
		setGender(inputRecord.getGender());
		setDtmn(inputRecord.getDtmn());
		setIntid(inputRecord.getIntid());
		setIntgen(inputRecord.getIntgen());
		setLmn(inputRecord.getLmn());
		setIntme(inputRecord.getIntme());
		setDdate(inputRecord.getDdate());
		setSvcdate(inputRecord.getSvcdate());
		setDpwaitcln(inputRecord.getDpwaitcln());
		setDpcleanlou(inputRecord.getDpcleanlou());
		setDpsfdrive(inputRecord.getDpsfdrive());
		setDpsflocat(inputRecord.getDpsflocat());
		setDpsfclean(inputRecord.getDpsfclean());
		setDpsfcomfy(inputRecord.getDpsfcomfy());
		setDpsfover(inputRecord.getDpsfover());
		setIrecodestart(inputRecord.getIrecodestart());
		setNpayamt(inputRecord.getNpayamt());
		setNnpayamt(inputRecord.getNnpayamt());
		setNsvctimd(inputRecord.getNsvctimd());
		setNnsvctimd(inputRecord.getNnsvctimd());
		setNsvctimh(inputRecord.getNsvctimh());
		setNnsvctimh(inputRecord.getNnsvctimh());
		setNnumprob(inputRecord.getNnumprob());
		setNhndovr(inputRecord.getNhndovr());
		setIrecodeend(inputRecord.getIrecodeend());
		setIntdate(inputRecord.getIntdate());
		setRecodestart(inputRecord.getRecodestart());
		setTsisched(inputRecord.getTsisched());
		setTsiflex(inputRecord.getTsiflex());
		setTsitime(inputRecord.getTsitime());
		setTsiover(inputRecord.getTsiover());
		setTsafren(inputRecord.getTsafren());
		setTsaresp(inputRecord.getTsaresp());
		setTsathor(inputRecord.getTsathor());
		setTsaover(inputRecord.getTsaover());
		setTsfdrive(inputRecord.getTsfdrive());
		setTsflocat(inputRecord.getTsflocat());
		setTsfclean(inputRecord.getTsfclean());
		setTsfcomfy(inputRecord.getTsfcomfy());
		setTsfover(inputRecord.getTsfover());
		setTvptime(inputRecord.getTvptime());
		setTvpfair(inputRecord.getTvpfair());
		setTvpstaff(inputRecord.getTvpstaff());
		setTvpover(inputRecord.getTvpover());
		setTsqtime(inputRecord.getTsqtime());
		setTsqthor(inputRecord.getTsqthor());
		setTsqclean(inputRecord.getTsqclean());
		setTsqover(inputRecord.getTsqover());
		setTosat(inputRecord.getTosat());
		setTsatford(inputRecord.getTsatford());
		setNsisched(inputRecord.getNsisched());
		setNsiflex(inputRecord.getNsiflex());
		setNsitime(inputRecord.getNsitime());
		setNsiover(inputRecord.getNsiover());
		setNsafren(inputRecord.getNsafren());
		setNsaresp(inputRecord.getNsaresp());
		setNsathor(inputRecord.getNsathor());
		setNsaover(inputRecord.getNsaover());
		setNsfdrive(inputRecord.getNsfdrive());
		setNsflocat(inputRecord.getNsflocat());
		setNsfclean(inputRecord.getNsfclean());
		setNsfcomfy(inputRecord.getNsfcomfy());
		setNsfover(inputRecord.getNsfover());
		setNvptime(inputRecord.getNvptime());
		setNvpfair(inputRecord.getNvpfair());
		setNvpstaff(inputRecord.getNvpstaff());
		setNvpover(inputRecord.getNvpover());
		setNsqtime(inputRecord.getNsqtime());
		setNsqthor(inputRecord.getNsqthor());
		setNsqclean(inputRecord.getNsqclean());
		setNsqover(inputRecord.getNsqover());
		setNosat(inputRecord.getNosat());
		setNsatford(inputRecord.getNsatford());
		setAsisched(inputRecord.getAsisched());
		setAsiflex(inputRecord.getAsiflex());
		setAsitime(inputRecord.getAsitime());
		setAsiover(inputRecord.getAsiover());
		setAsafren(inputRecord.getAsafren());
		setAsaresp(inputRecord.getAsaresp());
		setAsathor(inputRecord.getAsathor());
		setAsaover(inputRecord.getAsaover());
		setAsfdrive(inputRecord.getAsfdrive());
		setAsflocat(inputRecord.getAsflocat());
		setAsfclean(inputRecord.getAsfclean());
		setAsfcomfy(inputRecord.getAsfcomfy());
		setAsfover(inputRecord.getAsfover());
		setAvptime(inputRecord.getAvptime());
		setAvpfair(inputRecord.getAvpfair());
		setAvpstaff(inputRecord.getAvpstaff());
		setAvpover(inputRecord.getAvpover());
		setAsqtime(inputRecord.getAsqtime());
		setAsqthor(inputRecord.getAsqthor());
		setAsqclean(inputRecord.getAsqclean());
		setAsqover(inputRecord.getAsqover());
		setAosat(inputRecord.getAosat());
		setAsatford(inputRecord.getAsatford());
		setMthown(inputRecord.getMthown());
		setTimeoilh(inputRecord.getTimeoilh());
		setTimeoild(inputRecord.getTimeoild());
		setTimermh(inputRecord.getTimermh());
		setTimermd(inputRecord.getTimermd());
		setTimeerh(inputRecord.getTimeerh());
		setTimeerd(inputRecord.getTimeerd());
		setTimenerh(inputRecord.getTimenerh());
		setTimenerd(inputRecord.getTimenerd());
		setTimebph(inputRecord.getTimebph());
		setTimebpd(inputRecord.getTimebpd());
		setNsahist(inputRecord.getNsahist());
		setNrdyprom(inputRecord.getNrdyprom());
		setNvehwash(inputRecord.getNvehwash());
		setNpayment(inputRecord.getNpayment());
		setRightoil(inputRecord.getRightoil());
		setRightrm(inputRecord.getRightrm());
		setRighter(inputRecord.getRighter());
		setRightner(inputRecord.getRightner());
		setRightbp(inputRecord.getRightbp());
		setNwdright(inputRecord.getNwdright());
		setNreptype(inputRecord.getNreptype());
		setSop(inputRecord.getSop());
		setNsop(inputRecord.getNsop());
		setNnsop(inputRecord.getNnsop());
		setRmsop(inputRecord.getRmsop());
		setNrmsop(inputRecord.getNrmsop());
		setNnrmsop(inputRecord.getNnrmsop());
		setRelmeas(inputRecord.getRelmeas());
		setRecodeend(inputRecord.getRecodeend());
		setFdstart(inputRecord.getFdstart());
		setDealer2(inputRecord.getDealer2());
		setCity2(inputRecord.getCity2());
		setRegiona(inputRecord.getRegiona());
		setState2(inputRecord.getState2());
		setCity3(inputRecord.getCity3());
		setModel2(inputRecord.getModel2());
		setIndex(inputRecord.getIndex());
		setSi(inputRecord.getSi());
		setSa(inputRecord.getSa());
		setSf(inputRecord.getSf());
		setVp(inputRecord.getVp());
		setSq(inputRecord.getSq());
		setNindex(inputRecord.getNindex());
		setBiweekly(inputRecord.getBiweekly());
		setMonth(inputRecord.getMonth());
		setQuarter(inputRecord.getQuarter());
		setYear(inputRecord.getYear());
		setRstatus(inputRecord.getRstatus());
		setLastacc(inputRecord.getLastacc());
		setCreatedby(inputRecord.getCreatedby());
		setCreatedat(inputRecord.getCreatedat());
		setModifiedat(inputRecord.getModifiedat());
		setModifiedby(inputRecord.getModifiedby());
		setClosetype(inputRecord.getClosetype());
		setClosedat(inputRecord.getClosedat());
	}

	public void loadNonNullContent(LICsitranslatedRecord inputRecord)
	{
		if (StringUtils.hasChanged(getId(), inputRecord.getId()))
		{
			setId(StringUtils.noNull(inputRecord.getId()));
		}
		if (StringUtils.hasChanged(getSrno(), inputRecord.getSrno()))
		{
			setSrno(StringUtils.noNull(inputRecord.getSrno()));
		}
		if (StringUtils.hasChanged(getDbsrno(), inputRecord.getDbsrno()))
		{
			setDbsrno(StringUtils.noNull(inputRecord.getDbsrno()));
		}
		if (StringUtils.hasChanged(getTrack(), inputRecord.getTrack()))
		{
			setTrack(StringUtils.noNull(inputRecord.getTrack()));
		}
		if (StringUtils.hasChanged(getListenobs(), inputRecord.getListenobs()))
		{
			setListenobs(StringUtils.noNull(inputRecord.getListenobs()));
		}
		if (StringUtils.hasChanged(getIntadt(), inputRecord.getIntadt()))
		{
			setIntadt(StringUtils.noNull(inputRecord.getIntadt()));
		}
		if (StringUtils.hasChanged(getTapeadt(), inputRecord.getTapeadt()))
		{
			setTapeadt(StringUtils.noNull(inputRecord.getTapeadt()));
		}
		if (StringUtils.hasChanged(getThirdadt(), inputRecord.getThirdadt()))
		{
			setThirdadt(StringUtils.noNull(inputRecord.getThirdadt()));
		}
		if (StringUtils.hasChanged(getCityres(), inputRecord.getCityres()))
		{
			setCityres(StringUtils.noNull(inputRecord.getCityres()));
		}
		if (StringUtils.hasChanged(getDbsvdate(), inputRecord.getDbsvdate()))
		{
			setDbsvdate(StringUtils.noNull(inputRecord.getDbsvdate()));
		}
		if (StringUtils.hasChanged(getDbpdate(), inputRecord.getDbpdate()))
		{
			setDbpdate(StringUtils.noNull(inputRecord.getDbpdate()));
		}
		if (StringUtils.hasChanged(getCaruse(), inputRecord.getCaruse()))
		{
			setCaruse(StringUtils.noNull(inputRecord.getCaruse()));
		}
		if (StringUtils.hasChanged(getDbdealer(), inputRecord.getDbdealer()))
		{
			setDbdealer(StringUtils.noNull(inputRecord.getDbdealer()));
		}
		if (StringUtils.hasChanged(getScdlr(), inputRecord.getScdlr()))
		{
			setScdlr(StringUtils.noNull(inputRecord.getScdlr()));
		}
		if (StringUtils.hasChanged(getSvdate(), inputRecord.getSvdate()))
		{
			setSvdate(StringUtils.noNull(inputRecord.getSvdate()));
		}
		if (StringUtils.hasChanged(getRsvcdate(), inputRecord.getRsvcdate()))
		{
			setRsvcdate(StringUtils.noNull(inputRecord.getRsvcdate()));
		}
		if (StringUtils.hasChanged(getOwner(), inputRecord.getOwner()))
		{
			setOwner(StringUtils.noNull(inputRecord.getOwner()));
		}
		if (StringUtils.hasChanged(getDropveh(), inputRecord.getDropveh()))
		{
			setDropveh(StringUtils.noNull(inputRecord.getDropveh()));
		}
		if (StringUtils.hasChanged(getPickveh(), inputRecord.getPickveh()))
		{
			setPickveh(StringUtils.noNull(inputRecord.getPickveh()));
		}
		if (StringUtils.hasChanged(getOilchng(), inputRecord.getOilchng()))
		{
			setOilchng(StringUtils.noNull(inputRecord.getOilchng()));
		}
		if (StringUtils.hasChanged(getMaintain(), inputRecord.getMaintain()))
		{
			setMaintain(StringUtils.noNull(inputRecord.getMaintain()));
		}
		if (StringUtils.hasChanged(getEmergrep(), inputRecord.getEmergrep()))
		{
			setEmergrep(StringUtils.noNull(inputRecord.getEmergrep()));
		}
		if (StringUtils.hasChanged(getNemerrep(), inputRecord.getNemerrep()))
		{
			setNemerrep(StringUtils.noNull(inputRecord.getNemerrep()));
		}
		if (StringUtils.hasChanged(getBpaintrep(), inputRecord.getBpaintrep()))
		{
			setBpaintrep(StringUtils.noNull(inputRecord.getBpaintrep()));
		}
		if (StringUtils.hasChanged(getMrdk(), inputRecord.getMrdk()))
		{
			setMrdk(StringUtils.noNull(inputRecord.getMrdk()));
		}
		if (StringUtils.hasChanged(getSvctime(), inputRecord.getSvctime()))
		{
			setSvctime(StringUtils.noNull(inputRecord.getSvctime()));
		}
		if (StringUtils.hasChanged(getCddate(), inputRecord.getCddate()))
		{
			setCddate(StringUtils.noNull(inputRecord.getCddate()));
		}
		if (StringUtils.hasChanged(getScddate(), inputRecord.getScddate()))
		{
			setScddate(StringUtils.noNull(inputRecord.getScddate()));
		}
		if (StringUtils.hasChanged(getQrot(), inputRecord.getQrot()))
		{
			setQrot(StringUtils.noNull(inputRecord.getQrot()));
		}
		if (StringUtils.hasChanged(getNotified(), inputRecord.getNotified()))
		{
			setNotified(StringUtils.noNull(inputRecord.getNotified()));
		}
		if (StringUtils.hasChanged(getSchedule(), inputRecord.getSchedule()))
		{
			setSchedule(StringUtils.noNull(inputRecord.getSchedule()));
		}
		if (StringUtils.hasChanged(getAptdesir(), inputRecord.getAptdesir()))
		{
			setAptdesir(StringUtils.noNull(inputRecord.getAptdesir()));
		}
		if (StringUtils.hasChanged(getHndovr(), inputRecord.getHndovr()))
		{
			setHndovr(StringUtils.noNull(inputRecord.getHndovr()));
		}
		if (StringUtils.hasChanged(getStartsi(), inputRecord.getStartsi()))
		{
			setStartsi(StringUtils.noNull(inputRecord.getStartsi()));
		}
		if (StringUtils.hasChanged(getSisched(), inputRecord.getSisched()))
		{
			setSisched(StringUtils.noNull(inputRecord.getSisched()));
		}
		if (StringUtils.hasChanged(getSiflex(), inputRecord.getSiflex()))
		{
			setSiflex(StringUtils.noNull(inputRecord.getSiflex()));
		}
		if (StringUtils.hasChanged(getSitime(), inputRecord.getSitime()))
		{
			setSitime(StringUtils.noNull(inputRecord.getSitime()));
		}
		if (StringUtils.hasChanged(getSiover(), inputRecord.getSiover()))
		{
			setSiover(StringUtils.noNull(inputRecord.getSiover()));
		}
		if (StringUtils.hasChanged(getGreetwk(), inputRecord.getGreetwk()))
		{
			setGreetwk(StringUtils.noNull(inputRecord.getGreetwk()));
		}
		if (StringUtils.hasChanged(getReaswait(), inputRecord.getReaswait()))
		{
			setReaswait(StringUtils.noNull(inputRecord.getReaswait()));
		}
		if (StringUtils.hasChanged(getSaease(), inputRecord.getSaease()))
		{
			setSaease(StringUtils.noNull(inputRecord.getSaease()));
		}
		if (StringUtils.hasChanged(getSaadd(), inputRecord.getSaadd()))
		{
			setSaadd(StringUtils.noNull(inputRecord.getSaadd()));
		}
		if (StringUtils.hasChanged(getSaneat(), inputRecord.getSaneat()))
		{
			setSaneat(StringUtils.noNull(inputRecord.getSaneat()));
		}
		if (StringUtils.hasChanged(getSaattn(), inputRecord.getSaattn()))
		{
			setSaattn(StringUtils.noNull(inputRecord.getSaattn()));
		}
		if (StringUtils.hasChanged(getSarepeat(), inputRecord.getSarepeat()))
		{
			setSarepeat(StringUtils.noNull(inputRecord.getSarepeat()));
		}
		if (StringUtils.hasChanged(getSafocus(), inputRecord.getSafocus()))
		{
			setSafocus(StringUtils.noNull(inputRecord.getSafocus()));
		}
		if (StringUtils.hasChanged(getSawalk(), inputRecord.getSawalk()))
		{
			setSawalk(StringUtils.noNull(inputRecord.getSawalk()));
		}
		if (StringUtils.hasChanged(getSastatus(), inputRecord.getSastatus()))
		{
			setSastatus(StringUtils.noNull(inputRecord.getSastatus()));
		}
		if (StringUtils.hasChanged(getSahist(), inputRecord.getSahist()))
		{
			setSahist(StringUtils.noNull(inputRecord.getSahist()));
		}
		if (StringUtils.hasChanged(getPriorexp(), inputRecord.getPriorexp()))
		{
			setPriorexp(StringUtils.noNull(inputRecord.getPriorexp()));
		}
		if (StringUtils.hasChanged(getPriorfm(), inputRecord.getPriorfm()))
		{
			setPriorfm(StringUtils.noNull(inputRecord.getPriorfm()));
		}
		if (StringUtils.hasChanged(getEstimate(), inputRecord.getEstimate()))
		{
			setEstimate(StringUtils.noNull(inputRecord.getEstimate()));
		}
		if (StringUtils.hasChanged(getGivready(), inputRecord.getGivready()))
		{
			setGivready(StringUtils.noNull(inputRecord.getGivready()));
		}
		if (StringUtils.hasChanged(getStartsa(), inputRecord.getStartsa()))
		{
			setStartsa(StringUtils.noNull(inputRecord.getStartsa()));
		}
		if (StringUtils.hasChanged(getSafren(), inputRecord.getSafren()))
		{
			setSafren(StringUtils.noNull(inputRecord.getSafren()));
		}
		if (StringUtils.hasChanged(getSaresp(), inputRecord.getSaresp()))
		{
			setSaresp(StringUtils.noNull(inputRecord.getSaresp()));
		}
		if (StringUtils.hasChanged(getSathor(), inputRecord.getSathor()))
		{
			setSathor(StringUtils.noNull(inputRecord.getSathor()));
		}
		if (StringUtils.hasChanged(getSaover(), inputRecord.getSaover()))
		{
			setSaover(StringUtils.noNull(inputRecord.getSaover()));
		}
		if (StringUtils.hasChanged(getStayleft(), inputRecord.getStayleft()))
		{
			setStayleft(StringUtils.noNull(inputRecord.getStayleft()));
		}
		if (StringUtils.hasChanged(getOfftran(), inputRecord.getOfftran()))
		{
			setOfftran(StringUtils.noNull(inputRecord.getOfftran()));
		}
		if (StringUtils.hasChanged(getWaitcln(), inputRecord.getWaitcln()))
		{
			setWaitcln(StringUtils.noNull(inputRecord.getWaitcln()));
		}
		if (StringUtils.hasChanged(getCleanlou(), inputRecord.getCleanlou()))
		{
			setCleanlou(StringUtils.noNull(inputRecord.getCleanlou()));
		}
		if (StringUtils.hasChanged(getConvop(), inputRecord.getConvop()))
		{
			setConvop(StringUtils.noNull(inputRecord.getConvop()));
		}
		if (StringUtils.hasChanged(getFacweb(), inputRecord.getFacweb()))
		{
			setFacweb(StringUtils.noNull(inputRecord.getFacweb()));
		}
		if (StringUtils.hasChanged(getFacrefr(), inputRecord.getFacrefr()))
		{
			setFacrefr(StringUtils.noNull(inputRecord.getFacrefr()));
		}
		if (StringUtils.hasChanged(getFactv(), inputRecord.getFactv()))
		{
			setFactv(StringUtils.noNull(inputRecord.getFactv()));
		}
		if (StringUtils.hasChanged(getFacseat(), inputRecord.getFacseat()))
		{
			setFacseat(StringUtils.noNull(inputRecord.getFacseat()));
		}
		if (StringUtils.hasChanged(getFacread(), inputRecord.getFacread()))
		{
			setFacread(StringUtils.noNull(inputRecord.getFacread()));
		}
		if (StringUtils.hasChanged(getFacvpark(), inputRecord.getFacvpark()))
		{
			setFacvpark(StringUtils.noNull(inputRecord.getFacvpark()));
		}
		if (StringUtils.hasChanged(getFacaircon(), inputRecord.getFacaircon()))
		{
			setFacaircon(StringUtils.noNull(inputRecord.getFacaircon()));
		}
		if (StringUtils.hasChanged(getFacfood(), inputRecord.getFacfood()))
		{
			setFacfood(StringUtils.noNull(inputRecord.getFacfood()));
		}
		if (StringUtils.hasChanged(getFacoth(), inputRecord.getFacoth()))
		{
			setFacoth(StringUtils.noNull(inputRecord.getFacoth()));
		}
		if (StringUtils.hasChanged(getFacdk(), inputRecord.getFacdk()))
		{
			setFacdk(StringUtils.noNull(inputRecord.getFacdk()));
		}
		if (StringUtils.hasChanged(getFacothv(), inputRecord.getFacothv()))
		{
			setFacothv(StringUtils.noNull(inputRecord.getFacothv()));
		}
		if (StringUtils.hasChanged(getStartsf(), inputRecord.getStartsf()))
		{
			setStartsf(StringUtils.noNull(inputRecord.getStartsf()));
		}
		if (StringUtils.hasChanged(getSfdrive(), inputRecord.getSfdrive()))
		{
			setSfdrive(StringUtils.noNull(inputRecord.getSfdrive()));
		}
		if (StringUtils.hasChanged(getSflocat(), inputRecord.getSflocat()))
		{
			setSflocat(StringUtils.noNull(inputRecord.getSflocat()));
		}
		if (StringUtils.hasChanged(getSfclean(), inputRecord.getSfclean()))
		{
			setSfclean(StringUtils.noNull(inputRecord.getSfclean()));
		}
		if (StringUtils.hasChanged(getSfcomfy(), inputRecord.getSfcomfy()))
		{
			setSfcomfy(StringUtils.noNull(inputRecord.getSfcomfy()));
		}
		if (StringUtils.hasChanged(getSfover(), inputRecord.getSfover()))
		{
			setSfover(StringUtils.noNull(inputRecord.getSfover()));
		}
		if (StringUtils.hasChanged(getDelivery(), inputRecord.getDelivery()))
		{
			setDelivery(StringUtils.noNull(inputRecord.getDelivery()));
		}
		if (StringUtils.hasChanged(getPayment(), inputRecord.getPayment()))
		{
			setPayment(StringUtils.noNull(inputRecord.getPayment()));
		}
		if (StringUtils.hasChanged(getPayamt(), inputRecord.getPayamt()))
		{
			setPayamt(StringUtils.noNull(inputRecord.getPayamt()));
		}
		if (StringUtils.hasChanged(getInfready(), inputRecord.getInfready()))
		{
			setInfready(StringUtils.noNull(inputRecord.getInfready()));
		}
		if (StringUtils.hasChanged(getExplwrka(), inputRecord.getExplwrka()))
		{
			setExplwrka(StringUtils.noNull(inputRecord.getExplwrka()));
		}
		if (StringUtils.hasChanged(getSareview(), inputRecord.getSareview()))
		{
			setSareview(StringUtils.noNull(inputRecord.getSareview()));
		}
		if (StringUtils.hasChanged(getSaadvice(), inputRecord.getSaadvice()))
		{
			setSaadvice(StringUtils.noNull(inputRecord.getSaadvice()));
		}
		if (StringUtils.hasChanged(getExplchrg(), inputRecord.getExplchrg()))
		{
			setExplchrg(StringUtils.noNull(inputRecord.getExplchrg()));
		}
		if (StringUtils.hasChanged(getSchdnext(), inputRecord.getSchdnext()))
		{
			setSchdnext(StringUtils.noNull(inputRecord.getSchdnext()));
		}
		if (StringUtils.hasChanged(getReastime(), inputRecord.getReastime()))
		{
			setReastime(StringUtils.noNull(inputRecord.getReastime()));
		}
		if (StringUtils.hasChanged(getSvctimh(), inputRecord.getSvctimh()))
		{
			setSvctimh(StringUtils.noNull(inputRecord.getSvctimh()));
		}
		if (StringUtils.hasChanged(getSvctimd(), inputRecord.getSvctimd()))
		{
			setSvctimd(StringUtils.noNull(inputRecord.getSvctimd()));
		}
		if (StringUtils.hasChanged(getRdyprom(), inputRecord.getRdyprom()))
		{
			setRdyprom(StringUtils.noNull(inputRecord.getRdyprom()));
		}
		if (StringUtils.hasChanged(getStartvp(), inputRecord.getStartvp()))
		{
			setStartvp(StringUtils.noNull(inputRecord.getStartvp()));
		}
		if (StringUtils.hasChanged(getVptime(), inputRecord.getVptime()))
		{
			setVptime(StringUtils.noNull(inputRecord.getVptime()));
		}
		if (StringUtils.hasChanged(getVpfair(), inputRecord.getVpfair()))
		{
			setVpfair(StringUtils.noNull(inputRecord.getVpfair()));
		}
		if (StringUtils.hasChanged(getVpstaff(), inputRecord.getVpstaff()))
		{
			setVpstaff(StringUtils.noNull(inputRecord.getVpstaff()));
		}
		if (StringUtils.hasChanged(getVpover(), inputRecord.getVpover()))
		{
			setVpover(StringUtils.noNull(inputRecord.getVpover()));
		}
		if (StringUtils.hasChanged(getWdright(), inputRecord.getWdright()))
		{
			setWdright(StringUtils.noNull(inputRecord.getWdright()));
		}
		if (StringUtils.hasChanged(getCompreq(), inputRecord.getCompreq()))
		{
			setCompreq(StringUtils.noNull(inputRecord.getCompreq()));
		}
		if (StringUtils.hasChanged(getVehwash(), inputRecord.getVehwash()))
		{
			setVehwash(StringUtils.noNull(inputRecord.getVehwash()));
		}
		if (StringUtils.hasChanged(getContact(), inputRecord.getContact()))
		{
			setContact(StringUtils.noNull(inputRecord.getContact()));
		}
		if (StringUtils.hasChanged(getRepprob(), inputRecord.getRepprob()))
		{
			setRepprob(StringUtils.noNull(inputRecord.getRepprob()));
		}
		if (StringUtils.hasChanged(getDlrresol(), inputRecord.getDlrresol()))
		{
			setDlrresol(StringUtils.noNull(inputRecord.getDlrresol()));
		}
		if (StringUtils.hasChanged(getStartsq(), inputRecord.getStartsq()))
		{
			setStartsq(StringUtils.noNull(inputRecord.getStartsq()));
		}
		if (StringUtils.hasChanged(getSqtime(), inputRecord.getSqtime()))
		{
			setSqtime(StringUtils.noNull(inputRecord.getSqtime()));
		}
		if (StringUtils.hasChanged(getSqthor(), inputRecord.getSqthor()))
		{
			setSqthor(StringUtils.noNull(inputRecord.getSqthor()));
		}
		if (StringUtils.hasChanged(getSqclean(), inputRecord.getSqclean()))
		{
			setSqclean(StringUtils.noNull(inputRecord.getSqclean()));
		}
		if (StringUtils.hasChanged(getSqover(), inputRecord.getSqover()))
		{
			setSqover(StringUtils.noNull(inputRecord.getSqover()));
		}
		if (StringUtils.hasChanged(getOsat(), inputRecord.getOsat()))
		{
			setOsat(StringUtils.noNull(inputRecord.getOsat()));
		}
		if (StringUtils.hasChanged(getSatford(), inputRecord.getSatford()))
		{
			setSatford(StringUtils.noNull(inputRecord.getSatford()));
		}
		if (StringUtils.hasChanged(getSvcexp(), inputRecord.getSvcexp()))
		{
			setSvcexp(StringUtils.noNull(inputRecord.getSvcexp()));
		}
		if (StringUtils.hasChanged(getRecodlr(), inputRecord.getRecodlr()))
		{
			setRecodlr(StringUtils.noNull(inputRecord.getRecodlr()));
		}
		if (StringUtils.hasChanged(getRevwar(), inputRecord.getRevwar()))
		{
			setRevwar(StringUtils.noNull(inputRecord.getRevwar()));
		}
		if (StringUtils.hasChanged(getRevpwar(), inputRecord.getRevpwar()))
		{
			setRevpwar(StringUtils.noNull(inputRecord.getRevpwar()));
		}
		if (StringUtils.hasChanged(getPurchase(), inputRecord.getPurchase()))
		{
			setPurchase(StringUtils.noNull(inputRecord.getPurchase()));
		}
		if (StringUtils.hasChanged(getRecomake(), inputRecord.getRecomake()))
		{
			setRecomake(StringUtils.noNull(inputRecord.getRecomake()));
		}
		if (StringUtils.hasChanged(getReprmake(), inputRecord.getReprmake()))
		{
			setReprmake(StringUtils.noNull(inputRecord.getReprmake()));
		}
		if (StringUtils.hasChanged(getDlrstand(), inputRecord.getDlrstand()))
		{
			setDlrstand(StringUtils.noNull(inputRecord.getDlrstand()));
		}
		if (StringUtils.hasChanged(getReascost(), inputRecord.getReascost()))
		{
			setReascost(StringUtils.noNull(inputRecord.getReascost()));
		}
		if (StringUtils.hasChanged(getEmodeal(), inputRecord.getEmodeal()))
		{
			setEmodeal(StringUtils.noNull(inputRecord.getEmodeal()));
		}
		if (StringUtils.hasChanged(getEmoford(), inputRecord.getEmoford()))
		{
			setEmoford(StringUtils.noNull(inputRecord.getEmoford()));
		}
		if (StringUtils.hasChanged(getSvcnauth(), inputRecord.getSvcnauth()))
		{
			setSvcnauth(StringUtils.noNull(inputRecord.getSvcnauth()));
		}
		if (StringUtils.hasChanged(getProblems(), inputRecord.getProblems()))
		{
			setProblems(StringUtils.noNull(inputRecord.getProblems()));
		}
		if (StringUtils.hasChanged(getNumprob(), inputRecord.getNumprob()))
		{
			setNumprob(StringUtils.noNull(inputRecord.getNumprob()));
		}
		if (StringUtils.hasChanged(getToldds(), inputRecord.getToldds()))
		{
			setToldds(StringUtils.noNull(inputRecord.getToldds()));
		}
		if (StringUtils.hasChanged(getToldcc(), inputRecord.getToldcc()))
		{
			setToldcc(StringUtils.noNull(inputRecord.getToldcc()));
		}
		if (StringUtils.hasChanged(getFc(), inputRecord.getFc()))
		{
			setFc(StringUtils.noNull(inputRecord.getFc()));
		}
		if (StringUtils.hasChanged(getRevres(), inputRecord.getRevres()))
		{
			setRevres(StringUtils.noNull(inputRecord.getRevres()));
		}
		if (StringUtils.hasChanged(getFeedbk(), inputRecord.getFeedbk()))
		{
			setFeedbk(StringUtils.noNull(inputRecord.getFeedbk()));
		}
		if (StringUtils.hasChanged(getGender(), inputRecord.getGender()))
		{
			setGender(StringUtils.noNull(inputRecord.getGender()));
		}
		if (StringUtils.hasChanged(getDtmn(), inputRecord.getDtmn()))
		{
			setDtmn(StringUtils.noNull(inputRecord.getDtmn()));
		}
		if (StringUtils.hasChanged(getIntid(), inputRecord.getIntid()))
		{
			setIntid(StringUtils.noNull(inputRecord.getIntid()));
		}
		if (StringUtils.hasChanged(getIntgen(), inputRecord.getIntgen()))
		{
			setIntgen(StringUtils.noNull(inputRecord.getIntgen()));
		}
		if (StringUtils.hasChanged(getLmn(), inputRecord.getLmn()))
		{
			setLmn(StringUtils.noNull(inputRecord.getLmn()));
		}
		if (StringUtils.hasChanged(getIntme(), inputRecord.getIntme()))
		{
			setIntme(StringUtils.noNull(inputRecord.getIntme()));
		}
		if (StringUtils.hasChanged(getDdate(), inputRecord.getDdate()))
		{
			setDdate(StringUtils.noNull(inputRecord.getDdate()));
		}
		if (StringUtils.hasChanged(getSvcdate(), inputRecord.getSvcdate()))
		{
			setSvcdate(StringUtils.noNull(inputRecord.getSvcdate()));
		}
		if (StringUtils.hasChanged(getDpwaitcln(), inputRecord.getDpwaitcln()))
		{
			setDpwaitcln(StringUtils.noNull(inputRecord.getDpwaitcln()));
		}
		if (StringUtils.hasChanged(getDpcleanlou(), inputRecord.getDpcleanlou()))
		{
			setDpcleanlou(StringUtils.noNull(inputRecord.getDpcleanlou()));
		}
		if (StringUtils.hasChanged(getDpsfdrive(), inputRecord.getDpsfdrive()))
		{
			setDpsfdrive(StringUtils.noNull(inputRecord.getDpsfdrive()));
		}
		if (StringUtils.hasChanged(getDpsflocat(), inputRecord.getDpsflocat()))
		{
			setDpsflocat(StringUtils.noNull(inputRecord.getDpsflocat()));
		}
		if (StringUtils.hasChanged(getDpsfclean(), inputRecord.getDpsfclean()))
		{
			setDpsfclean(StringUtils.noNull(inputRecord.getDpsfclean()));
		}
		if (StringUtils.hasChanged(getDpsfcomfy(), inputRecord.getDpsfcomfy()))
		{
			setDpsfcomfy(StringUtils.noNull(inputRecord.getDpsfcomfy()));
		}
		if (StringUtils.hasChanged(getDpsfover(), inputRecord.getDpsfover()))
		{
			setDpsfover(StringUtils.noNull(inputRecord.getDpsfover()));
		}
		if (StringUtils.hasChanged(getIrecodestart(), inputRecord.getIrecodestart()))
		{
			setIrecodestart(StringUtils.noNull(inputRecord.getIrecodestart()));
		}
		if (StringUtils.hasChanged(getNpayamt(), inputRecord.getNpayamt()))
		{
			setNpayamt(StringUtils.noNull(inputRecord.getNpayamt()));
		}
		if (StringUtils.hasChanged(getNnpayamt(), inputRecord.getNnpayamt()))
		{
			setNnpayamt(StringUtils.noNull(inputRecord.getNnpayamt()));
		}
		if (StringUtils.hasChanged(getNsvctimd(), inputRecord.getNsvctimd()))
		{
			setNsvctimd(StringUtils.noNull(inputRecord.getNsvctimd()));
		}
		if (StringUtils.hasChanged(getNnsvctimd(), inputRecord.getNnsvctimd()))
		{
			setNnsvctimd(StringUtils.noNull(inputRecord.getNnsvctimd()));
		}
		if (StringUtils.hasChanged(getNsvctimh(), inputRecord.getNsvctimh()))
		{
			setNsvctimh(StringUtils.noNull(inputRecord.getNsvctimh()));
		}
		if (StringUtils.hasChanged(getNnsvctimh(), inputRecord.getNnsvctimh()))
		{
			setNnsvctimh(StringUtils.noNull(inputRecord.getNnsvctimh()));
		}
		if (StringUtils.hasChanged(getNnumprob(), inputRecord.getNnumprob()))
		{
			setNnumprob(StringUtils.noNull(inputRecord.getNnumprob()));
		}
		if (StringUtils.hasChanged(getNhndovr(), inputRecord.getNhndovr()))
		{
			setNhndovr(StringUtils.noNull(inputRecord.getNhndovr()));
		}
		if (StringUtils.hasChanged(getIrecodeend(), inputRecord.getIrecodeend()))
		{
			setIrecodeend(StringUtils.noNull(inputRecord.getIrecodeend()));
		}
		if (StringUtils.hasChanged(getIntdate(), inputRecord.getIntdate()))
		{
			setIntdate(StringUtils.noNull(inputRecord.getIntdate()));
		}
		if (StringUtils.hasChanged(getRecodestart(), inputRecord.getRecodestart()))
		{
			setRecodestart(StringUtils.noNull(inputRecord.getRecodestart()));
		}
		if (StringUtils.hasChanged(getTsisched(), inputRecord.getTsisched()))
		{
			setTsisched(StringUtils.noNull(inputRecord.getTsisched()));
		}
		if (StringUtils.hasChanged(getTsiflex(), inputRecord.getTsiflex()))
		{
			setTsiflex(StringUtils.noNull(inputRecord.getTsiflex()));
		}
		if (StringUtils.hasChanged(getTsitime(), inputRecord.getTsitime()))
		{
			setTsitime(StringUtils.noNull(inputRecord.getTsitime()));
		}
		if (StringUtils.hasChanged(getTsiover(), inputRecord.getTsiover()))
		{
			setTsiover(StringUtils.noNull(inputRecord.getTsiover()));
		}
		if (StringUtils.hasChanged(getTsafren(), inputRecord.getTsafren()))
		{
			setTsafren(StringUtils.noNull(inputRecord.getTsafren()));
		}
		if (StringUtils.hasChanged(getTsaresp(), inputRecord.getTsaresp()))
		{
			setTsaresp(StringUtils.noNull(inputRecord.getTsaresp()));
		}
		if (StringUtils.hasChanged(getTsathor(), inputRecord.getTsathor()))
		{
			setTsathor(StringUtils.noNull(inputRecord.getTsathor()));
		}
		if (StringUtils.hasChanged(getTsaover(), inputRecord.getTsaover()))
		{
			setTsaover(StringUtils.noNull(inputRecord.getTsaover()));
		}
		if (StringUtils.hasChanged(getTsfdrive(), inputRecord.getTsfdrive()))
		{
			setTsfdrive(StringUtils.noNull(inputRecord.getTsfdrive()));
		}
		if (StringUtils.hasChanged(getTsflocat(), inputRecord.getTsflocat()))
		{
			setTsflocat(StringUtils.noNull(inputRecord.getTsflocat()));
		}
		if (StringUtils.hasChanged(getTsfclean(), inputRecord.getTsfclean()))
		{
			setTsfclean(StringUtils.noNull(inputRecord.getTsfclean()));
		}
		if (StringUtils.hasChanged(getTsfcomfy(), inputRecord.getTsfcomfy()))
		{
			setTsfcomfy(StringUtils.noNull(inputRecord.getTsfcomfy()));
		}
		if (StringUtils.hasChanged(getTsfover(), inputRecord.getTsfover()))
		{
			setTsfover(StringUtils.noNull(inputRecord.getTsfover()));
		}
		if (StringUtils.hasChanged(getTvptime(), inputRecord.getTvptime()))
		{
			setTvptime(StringUtils.noNull(inputRecord.getTvptime()));
		}
		if (StringUtils.hasChanged(getTvpfair(), inputRecord.getTvpfair()))
		{
			setTvpfair(StringUtils.noNull(inputRecord.getTvpfair()));
		}
		if (StringUtils.hasChanged(getTvpstaff(), inputRecord.getTvpstaff()))
		{
			setTvpstaff(StringUtils.noNull(inputRecord.getTvpstaff()));
		}
		if (StringUtils.hasChanged(getTvpover(), inputRecord.getTvpover()))
		{
			setTvpover(StringUtils.noNull(inputRecord.getTvpover()));
		}
		if (StringUtils.hasChanged(getTsqtime(), inputRecord.getTsqtime()))
		{
			setTsqtime(StringUtils.noNull(inputRecord.getTsqtime()));
		}
		if (StringUtils.hasChanged(getTsqthor(), inputRecord.getTsqthor()))
		{
			setTsqthor(StringUtils.noNull(inputRecord.getTsqthor()));
		}
		if (StringUtils.hasChanged(getTsqclean(), inputRecord.getTsqclean()))
		{
			setTsqclean(StringUtils.noNull(inputRecord.getTsqclean()));
		}
		if (StringUtils.hasChanged(getTsqover(), inputRecord.getTsqover()))
		{
			setTsqover(StringUtils.noNull(inputRecord.getTsqover()));
		}
		if (StringUtils.hasChanged(getTosat(), inputRecord.getTosat()))
		{
			setTosat(StringUtils.noNull(inputRecord.getTosat()));
		}
		if (StringUtils.hasChanged(getTsatford(), inputRecord.getTsatford()))
		{
			setTsatford(StringUtils.noNull(inputRecord.getTsatford()));
		}
		if (StringUtils.hasChanged(getNsisched(), inputRecord.getNsisched()))
		{
			setNsisched(StringUtils.noNull(inputRecord.getNsisched()));
		}
		if (StringUtils.hasChanged(getNsiflex(), inputRecord.getNsiflex()))
		{
			setNsiflex(StringUtils.noNull(inputRecord.getNsiflex()));
		}
		if (StringUtils.hasChanged(getNsitime(), inputRecord.getNsitime()))
		{
			setNsitime(StringUtils.noNull(inputRecord.getNsitime()));
		}
		if (StringUtils.hasChanged(getNsiover(), inputRecord.getNsiover()))
		{
			setNsiover(StringUtils.noNull(inputRecord.getNsiover()));
		}
		if (StringUtils.hasChanged(getNsafren(), inputRecord.getNsafren()))
		{
			setNsafren(StringUtils.noNull(inputRecord.getNsafren()));
		}
		if (StringUtils.hasChanged(getNsaresp(), inputRecord.getNsaresp()))
		{
			setNsaresp(StringUtils.noNull(inputRecord.getNsaresp()));
		}
		if (StringUtils.hasChanged(getNsathor(), inputRecord.getNsathor()))
		{
			setNsathor(StringUtils.noNull(inputRecord.getNsathor()));
		}
		if (StringUtils.hasChanged(getNsaover(), inputRecord.getNsaover()))
		{
			setNsaover(StringUtils.noNull(inputRecord.getNsaover()));
		}
		if (StringUtils.hasChanged(getNsfdrive(), inputRecord.getNsfdrive()))
		{
			setNsfdrive(StringUtils.noNull(inputRecord.getNsfdrive()));
		}
		if (StringUtils.hasChanged(getNsflocat(), inputRecord.getNsflocat()))
		{
			setNsflocat(StringUtils.noNull(inputRecord.getNsflocat()));
		}
		if (StringUtils.hasChanged(getNsfclean(), inputRecord.getNsfclean()))
		{
			setNsfclean(StringUtils.noNull(inputRecord.getNsfclean()));
		}
		if (StringUtils.hasChanged(getNsfcomfy(), inputRecord.getNsfcomfy()))
		{
			setNsfcomfy(StringUtils.noNull(inputRecord.getNsfcomfy()));
		}
		if (StringUtils.hasChanged(getNsfover(), inputRecord.getNsfover()))
		{
			setNsfover(StringUtils.noNull(inputRecord.getNsfover()));
		}
		if (StringUtils.hasChanged(getNvptime(), inputRecord.getNvptime()))
		{
			setNvptime(StringUtils.noNull(inputRecord.getNvptime()));
		}
		if (StringUtils.hasChanged(getNvpfair(), inputRecord.getNvpfair()))
		{
			setNvpfair(StringUtils.noNull(inputRecord.getNvpfair()));
		}
		if (StringUtils.hasChanged(getNvpstaff(), inputRecord.getNvpstaff()))
		{
			setNvpstaff(StringUtils.noNull(inputRecord.getNvpstaff()));
		}
		if (StringUtils.hasChanged(getNvpover(), inputRecord.getNvpover()))
		{
			setNvpover(StringUtils.noNull(inputRecord.getNvpover()));
		}
		if (StringUtils.hasChanged(getNsqtime(), inputRecord.getNsqtime()))
		{
			setNsqtime(StringUtils.noNull(inputRecord.getNsqtime()));
		}
		if (StringUtils.hasChanged(getNsqthor(), inputRecord.getNsqthor()))
		{
			setNsqthor(StringUtils.noNull(inputRecord.getNsqthor()));
		}
		if (StringUtils.hasChanged(getNsqclean(), inputRecord.getNsqclean()))
		{
			setNsqclean(StringUtils.noNull(inputRecord.getNsqclean()));
		}
		if (StringUtils.hasChanged(getNsqover(), inputRecord.getNsqover()))
		{
			setNsqover(StringUtils.noNull(inputRecord.getNsqover()));
		}
		if (StringUtils.hasChanged(getNosat(), inputRecord.getNosat()))
		{
			setNosat(StringUtils.noNull(inputRecord.getNosat()));
		}
		if (StringUtils.hasChanged(getNsatford(), inputRecord.getNsatford()))
		{
			setNsatford(StringUtils.noNull(inputRecord.getNsatford()));
		}
		if (StringUtils.hasChanged(getAsisched(), inputRecord.getAsisched()))
		{
			setAsisched(StringUtils.noNull(inputRecord.getAsisched()));
		}
		if (StringUtils.hasChanged(getAsiflex(), inputRecord.getAsiflex()))
		{
			setAsiflex(StringUtils.noNull(inputRecord.getAsiflex()));
		}
		if (StringUtils.hasChanged(getAsitime(), inputRecord.getAsitime()))
		{
			setAsitime(StringUtils.noNull(inputRecord.getAsitime()));
		}
		if (StringUtils.hasChanged(getAsiover(), inputRecord.getAsiover()))
		{
			setAsiover(StringUtils.noNull(inputRecord.getAsiover()));
		}
		if (StringUtils.hasChanged(getAsafren(), inputRecord.getAsafren()))
		{
			setAsafren(StringUtils.noNull(inputRecord.getAsafren()));
		}
		if (StringUtils.hasChanged(getAsaresp(), inputRecord.getAsaresp()))
		{
			setAsaresp(StringUtils.noNull(inputRecord.getAsaresp()));
		}
		if (StringUtils.hasChanged(getAsathor(), inputRecord.getAsathor()))
		{
			setAsathor(StringUtils.noNull(inputRecord.getAsathor()));
		}
		if (StringUtils.hasChanged(getAsaover(), inputRecord.getAsaover()))
		{
			setAsaover(StringUtils.noNull(inputRecord.getAsaover()));
		}
		if (StringUtils.hasChanged(getAsfdrive(), inputRecord.getAsfdrive()))
		{
			setAsfdrive(StringUtils.noNull(inputRecord.getAsfdrive()));
		}
		if (StringUtils.hasChanged(getAsflocat(), inputRecord.getAsflocat()))
		{
			setAsflocat(StringUtils.noNull(inputRecord.getAsflocat()));
		}
		if (StringUtils.hasChanged(getAsfclean(), inputRecord.getAsfclean()))
		{
			setAsfclean(StringUtils.noNull(inputRecord.getAsfclean()));
		}
		if (StringUtils.hasChanged(getAsfcomfy(), inputRecord.getAsfcomfy()))
		{
			setAsfcomfy(StringUtils.noNull(inputRecord.getAsfcomfy()));
		}
		if (StringUtils.hasChanged(getAsfover(), inputRecord.getAsfover()))
		{
			setAsfover(StringUtils.noNull(inputRecord.getAsfover()));
		}
		if (StringUtils.hasChanged(getAvptime(), inputRecord.getAvptime()))
		{
			setAvptime(StringUtils.noNull(inputRecord.getAvptime()));
		}
		if (StringUtils.hasChanged(getAvpfair(), inputRecord.getAvpfair()))
		{
			setAvpfair(StringUtils.noNull(inputRecord.getAvpfair()));
		}
		if (StringUtils.hasChanged(getAvpstaff(), inputRecord.getAvpstaff()))
		{
			setAvpstaff(StringUtils.noNull(inputRecord.getAvpstaff()));
		}
		if (StringUtils.hasChanged(getAvpover(), inputRecord.getAvpover()))
		{
			setAvpover(StringUtils.noNull(inputRecord.getAvpover()));
		}
		if (StringUtils.hasChanged(getAsqtime(), inputRecord.getAsqtime()))
		{
			setAsqtime(StringUtils.noNull(inputRecord.getAsqtime()));
		}
		if (StringUtils.hasChanged(getAsqthor(), inputRecord.getAsqthor()))
		{
			setAsqthor(StringUtils.noNull(inputRecord.getAsqthor()));
		}
		if (StringUtils.hasChanged(getAsqclean(), inputRecord.getAsqclean()))
		{
			setAsqclean(StringUtils.noNull(inputRecord.getAsqclean()));
		}
		if (StringUtils.hasChanged(getAsqover(), inputRecord.getAsqover()))
		{
			setAsqover(StringUtils.noNull(inputRecord.getAsqover()));
		}
		if (StringUtils.hasChanged(getAosat(), inputRecord.getAosat()))
		{
			setAosat(StringUtils.noNull(inputRecord.getAosat()));
		}
		if (StringUtils.hasChanged(getAsatford(), inputRecord.getAsatford()))
		{
			setAsatford(StringUtils.noNull(inputRecord.getAsatford()));
		}
		if (StringUtils.hasChanged(getMthown(), inputRecord.getMthown()))
		{
			setMthown(StringUtils.noNull(inputRecord.getMthown()));
		}
		if (StringUtils.hasChanged(getTimeoilh(), inputRecord.getTimeoilh()))
		{
			setTimeoilh(StringUtils.noNull(inputRecord.getTimeoilh()));
		}
		if (StringUtils.hasChanged(getTimeoild(), inputRecord.getTimeoild()))
		{
			setTimeoild(StringUtils.noNull(inputRecord.getTimeoild()));
		}
		if (StringUtils.hasChanged(getTimermh(), inputRecord.getTimermh()))
		{
			setTimermh(StringUtils.noNull(inputRecord.getTimermh()));
		}
		if (StringUtils.hasChanged(getTimermd(), inputRecord.getTimermd()))
		{
			setTimermd(StringUtils.noNull(inputRecord.getTimermd()));
		}
		if (StringUtils.hasChanged(getTimeerh(), inputRecord.getTimeerh()))
		{
			setTimeerh(StringUtils.noNull(inputRecord.getTimeerh()));
		}
		if (StringUtils.hasChanged(getTimeerd(), inputRecord.getTimeerd()))
		{
			setTimeerd(StringUtils.noNull(inputRecord.getTimeerd()));
		}
		if (StringUtils.hasChanged(getTimenerh(), inputRecord.getTimenerh()))
		{
			setTimenerh(StringUtils.noNull(inputRecord.getTimenerh()));
		}
		if (StringUtils.hasChanged(getTimenerd(), inputRecord.getTimenerd()))
		{
			setTimenerd(StringUtils.noNull(inputRecord.getTimenerd()));
		}
		if (StringUtils.hasChanged(getTimebph(), inputRecord.getTimebph()))
		{
			setTimebph(StringUtils.noNull(inputRecord.getTimebph()));
		}
		if (StringUtils.hasChanged(getTimebpd(), inputRecord.getTimebpd()))
		{
			setTimebpd(StringUtils.noNull(inputRecord.getTimebpd()));
		}
		if (StringUtils.hasChanged(getNsahist(), inputRecord.getNsahist()))
		{
			setNsahist(StringUtils.noNull(inputRecord.getNsahist()));
		}
		if (StringUtils.hasChanged(getNrdyprom(), inputRecord.getNrdyprom()))
		{
			setNrdyprom(StringUtils.noNull(inputRecord.getNrdyprom()));
		}
		if (StringUtils.hasChanged(getNvehwash(), inputRecord.getNvehwash()))
		{
			setNvehwash(StringUtils.noNull(inputRecord.getNvehwash()));
		}
		if (StringUtils.hasChanged(getNpayment(), inputRecord.getNpayment()))
		{
			setNpayment(StringUtils.noNull(inputRecord.getNpayment()));
		}
		if (StringUtils.hasChanged(getRightoil(), inputRecord.getRightoil()))
		{
			setRightoil(StringUtils.noNull(inputRecord.getRightoil()));
		}
		if (StringUtils.hasChanged(getRightrm(), inputRecord.getRightrm()))
		{
			setRightrm(StringUtils.noNull(inputRecord.getRightrm()));
		}
		if (StringUtils.hasChanged(getRighter(), inputRecord.getRighter()))
		{
			setRighter(StringUtils.noNull(inputRecord.getRighter()));
		}
		if (StringUtils.hasChanged(getRightner(), inputRecord.getRightner()))
		{
			setRightner(StringUtils.noNull(inputRecord.getRightner()));
		}
		if (StringUtils.hasChanged(getRightbp(), inputRecord.getRightbp()))
		{
			setRightbp(StringUtils.noNull(inputRecord.getRightbp()));
		}
		if (StringUtils.hasChanged(getNwdright(), inputRecord.getNwdright()))
		{
			setNwdright(StringUtils.noNull(inputRecord.getNwdright()));
		}
		if (StringUtils.hasChanged(getNreptype(), inputRecord.getNreptype()))
		{
			setNreptype(StringUtils.noNull(inputRecord.getNreptype()));
		}
		if (StringUtils.hasChanged(getSop(), inputRecord.getSop()))
		{
			setSop(StringUtils.noNull(inputRecord.getSop()));
		}
		if (StringUtils.hasChanged(getNsop(), inputRecord.getNsop()))
		{
			setNsop(StringUtils.noNull(inputRecord.getNsop()));
		}
		if (StringUtils.hasChanged(getNnsop(), inputRecord.getNnsop()))
		{
			setNnsop(StringUtils.noNull(inputRecord.getNnsop()));
		}
		if (StringUtils.hasChanged(getRmsop(), inputRecord.getRmsop()))
		{
			setRmsop(StringUtils.noNull(inputRecord.getRmsop()));
		}
		if (StringUtils.hasChanged(getNrmsop(), inputRecord.getNrmsop()))
		{
			setNrmsop(StringUtils.noNull(inputRecord.getNrmsop()));
		}
		if (StringUtils.hasChanged(getNnrmsop(), inputRecord.getNnrmsop()))
		{
			setNnrmsop(StringUtils.noNull(inputRecord.getNnrmsop()));
		}
		if (StringUtils.hasChanged(getRelmeas(), inputRecord.getRelmeas()))
		{
			setRelmeas(StringUtils.noNull(inputRecord.getRelmeas()));
		}
		if (StringUtils.hasChanged(getRecodeend(), inputRecord.getRecodeend()))
		{
			setRecodeend(StringUtils.noNull(inputRecord.getRecodeend()));
		}
		if (StringUtils.hasChanged(getFdstart(), inputRecord.getFdstart()))
		{
			setFdstart(StringUtils.noNull(inputRecord.getFdstart()));
		}
		if (StringUtils.hasChanged(getDealer2(), inputRecord.getDealer2()))
		{
			setDealer2(StringUtils.noNull(inputRecord.getDealer2()));
		}
		if (StringUtils.hasChanged(getCity2(), inputRecord.getCity2()))
		{
			setCity2(StringUtils.noNull(inputRecord.getCity2()));
		}
		if (StringUtils.hasChanged(getRegiona(), inputRecord.getRegiona()))
		{
			setRegiona(StringUtils.noNull(inputRecord.getRegiona()));
		}
		if (StringUtils.hasChanged(getState2(), inputRecord.getState2()))
		{
			setState2(StringUtils.noNull(inputRecord.getState2()));
		}
		if (StringUtils.hasChanged(getCity3(), inputRecord.getCity3()))
		{
			setCity3(StringUtils.noNull(inputRecord.getCity3()));
		}
		if (StringUtils.hasChanged(getModel2(), inputRecord.getModel2()))
		{
			setModel2(StringUtils.noNull(inputRecord.getModel2()));
		}
		if (StringUtils.hasChanged(getIndex(), inputRecord.getIndex()))
		{
			setIndex(StringUtils.noNull(inputRecord.getIndex()));
		}
		if (StringUtils.hasChanged(getSi(), inputRecord.getSi()))
		{
			setSi(StringUtils.noNull(inputRecord.getSi()));
		}
		if (StringUtils.hasChanged(getSa(), inputRecord.getSa()))
		{
			setSa(StringUtils.noNull(inputRecord.getSa()));
		}
		if (StringUtils.hasChanged(getSf(), inputRecord.getSf()))
		{
			setSf(StringUtils.noNull(inputRecord.getSf()));
		}
		if (StringUtils.hasChanged(getVp(), inputRecord.getVp()))
		{
			setVp(StringUtils.noNull(inputRecord.getVp()));
		}
		if (StringUtils.hasChanged(getSq(), inputRecord.getSq()))
		{
			setSq(StringUtils.noNull(inputRecord.getSq()));
		}
		if (StringUtils.hasChanged(getNindex(), inputRecord.getNindex()))
		{
			setNindex(StringUtils.noNull(inputRecord.getNindex()));
		}
		if (StringUtils.hasChanged(getBiweekly(), inputRecord.getBiweekly()))
		{
			setBiweekly(StringUtils.noNull(inputRecord.getBiweekly()));
		}
		if (StringUtils.hasChanged(getMonth(), inputRecord.getMonth()))
		{
			setMonth(StringUtils.noNull(inputRecord.getMonth()));
		}
		if (StringUtils.hasChanged(getQuarter(), inputRecord.getQuarter()))
		{
			setQuarter(StringUtils.noNull(inputRecord.getQuarter()));
		}
		if (StringUtils.hasChanged(getYear(), inputRecord.getYear()))
		{
			setYear(StringUtils.noNull(inputRecord.getYear()));
		}
		if (StringUtils.hasChanged(getRstatus(), inputRecord.getRstatus()))
		{
			setRstatus(StringUtils.noNull(inputRecord.getRstatus()));
		}
		if (StringUtils.hasChanged(getLastacc(), inputRecord.getLastacc()))
		{
			setLastacc(StringUtils.noNull(inputRecord.getLastacc()));
		}
		if (StringUtils.hasChanged(getCreatedby(), inputRecord.getCreatedby()))
		{
			setCreatedby(StringUtils.noNull(inputRecord.getCreatedby()));
		}
		if (StringUtils.hasChanged(getCreatedat(), inputRecord.getCreatedat()))
		{
			setCreatedat(StringUtils.noNull(inputRecord.getCreatedat()));
		}
		if (StringUtils.hasChanged(getModifiedat(), inputRecord.getModifiedat()))
		{
			setModifiedat(StringUtils.noNull(inputRecord.getModifiedat()));
		}
		if (StringUtils.hasChanged(getModifiedby(), inputRecord.getModifiedby()))
		{
			setModifiedby(StringUtils.noNull(inputRecord.getModifiedby()));
		}
		if (StringUtils.hasChanged(getClosetype(), inputRecord.getClosetype()))
		{
			setClosetype(StringUtils.noNull(inputRecord.getClosetype()));
		}
		if (StringUtils.hasChanged(getClosedat(), inputRecord.getClosedat()))
		{
			setClosedat(StringUtils.noNull(inputRecord.getClosedat()));
		}
	}

	public JSONObject getJSONObject()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("srno",StringUtils.noNull(srno));				
		obj.put("dbsrno",StringUtils.noNull(dbsrno));				
		obj.put("track",StringUtils.noNull(track));				
		obj.put("listenobs",StringUtils.noNull(listenobs));				
		obj.put("intadt",StringUtils.noNull(intadt));				
		obj.put("tapeadt",StringUtils.noNull(tapeadt));				
		obj.put("thirdadt",StringUtils.noNull(thirdadt));				
		obj.put("cityres",StringUtils.noNull(cityres));				
		obj.put("dbsvdate",StringUtils.noNull(dbsvdate));				
		obj.put("dbpdate",StringUtils.noNull(dbpdate));				
		obj.put("caruse",StringUtils.noNull(caruse));				
		obj.put("dbdealer",StringUtils.noNull(dbdealer));				
		obj.put("scdlr",StringUtils.noNull(scdlr));				
		obj.put("svdate",StringUtils.noNull(svdate));				
		obj.put("rsvcdate",StringUtils.noNull(rsvcdate));				
		obj.put("owner",StringUtils.noNull(owner));				
		obj.put("dropveh",StringUtils.noNull(dropveh));				
		obj.put("pickveh",StringUtils.noNull(pickveh));				
		obj.put("oilchng",StringUtils.noNull(oilchng));				
		obj.put("maintain",StringUtils.noNull(maintain));				
		obj.put("emergrep",StringUtils.noNull(emergrep));				
		obj.put("nemerrep",StringUtils.noNull(nemerrep));				
		obj.put("bpaintrep",StringUtils.noNull(bpaintrep));				
		obj.put("mrdk",StringUtils.noNull(mrdk));				
		obj.put("svctime",StringUtils.noNull(svctime));				
		obj.put("cddate",StringUtils.noNull(cddate));				
		obj.put("scddate",StringUtils.noNull(scddate));				
		obj.put("qrot",StringUtils.noNull(qrot));				
		obj.put("notified",StringUtils.noNull(notified));				
		obj.put("schedule",StringUtils.noNull(schedule));				
		obj.put("aptdesir",StringUtils.noNull(aptdesir));				
		obj.put("hndovr",StringUtils.noNull(hndovr));				
		obj.put("startsi",StringUtils.noNull(startsi));				
		obj.put("sisched",StringUtils.noNull(sisched));				
		obj.put("siflex",StringUtils.noNull(siflex));				
		obj.put("sitime",StringUtils.noNull(sitime));				
		obj.put("siover",StringUtils.noNull(siover));				
		obj.put("greetwk",StringUtils.noNull(greetwk));				
		obj.put("reaswait",StringUtils.noNull(reaswait));				
		obj.put("saease",StringUtils.noNull(saease));				
		obj.put("saadd",StringUtils.noNull(saadd));				
		obj.put("saneat",StringUtils.noNull(saneat));				
		obj.put("saattn",StringUtils.noNull(saattn));				
		obj.put("sarepeat",StringUtils.noNull(sarepeat));				
		obj.put("safocus",StringUtils.noNull(safocus));				
		obj.put("sawalk",StringUtils.noNull(sawalk));				
		obj.put("sastatus",StringUtils.noNull(sastatus));				
		obj.put("sahist",StringUtils.noNull(sahist));				
		obj.put("priorexp",StringUtils.noNull(priorexp));				
		obj.put("priorfm",StringUtils.noNull(priorfm));				
		obj.put("estimate",StringUtils.noNull(estimate));				
		obj.put("givready",StringUtils.noNull(givready));				
		obj.put("startsa",StringUtils.noNull(startsa));				
		obj.put("safren",StringUtils.noNull(safren));				
		obj.put("saresp",StringUtils.noNull(saresp));				
		obj.put("sathor",StringUtils.noNull(sathor));				
		obj.put("saover",StringUtils.noNull(saover));				
		obj.put("stayleft",StringUtils.noNull(stayleft));				
		obj.put("offtran",StringUtils.noNull(offtran));				
		obj.put("waitcln",StringUtils.noNull(waitcln));				
		obj.put("cleanlou",StringUtils.noNull(cleanlou));				
		obj.put("convop",StringUtils.noNull(convop));				
		obj.put("facweb",StringUtils.noNull(facweb));				
		obj.put("facrefr",StringUtils.noNull(facrefr));				
		obj.put("factv",StringUtils.noNull(factv));				
		obj.put("facseat",StringUtils.noNull(facseat));				
		obj.put("facread",StringUtils.noNull(facread));				
		obj.put("facvpark",StringUtils.noNull(facvpark));				
		obj.put("facaircon",StringUtils.noNull(facaircon));				
		obj.put("facfood",StringUtils.noNull(facfood));				
		obj.put("facoth",StringUtils.noNull(facoth));				
		obj.put("facdk",StringUtils.noNull(facdk));				
		obj.put("facothv",StringUtils.noNull(facothv));				
		obj.put("startsf",StringUtils.noNull(startsf));				
		obj.put("sfdrive",StringUtils.noNull(sfdrive));				
		obj.put("sflocat",StringUtils.noNull(sflocat));				
		obj.put("sfclean",StringUtils.noNull(sfclean));				
		obj.put("sfcomfy",StringUtils.noNull(sfcomfy));				
		obj.put("sfover",StringUtils.noNull(sfover));				
		obj.put("delivery",StringUtils.noNull(delivery));				
		obj.put("payment",StringUtils.noNull(payment));				
		obj.put("payamt",StringUtils.noNull(payamt));				
		obj.put("infready",StringUtils.noNull(infready));				
		obj.put("explwrka",StringUtils.noNull(explwrka));				
		obj.put("sareview",StringUtils.noNull(sareview));				
		obj.put("saadvice",StringUtils.noNull(saadvice));				
		obj.put("explchrg",StringUtils.noNull(explchrg));				
		obj.put("schdnext",StringUtils.noNull(schdnext));				
		obj.put("reastime",StringUtils.noNull(reastime));				
		obj.put("svctimh",StringUtils.noNull(svctimh));				
		obj.put("svctimd",StringUtils.noNull(svctimd));				
		obj.put("rdyprom",StringUtils.noNull(rdyprom));				
		obj.put("startvp",StringUtils.noNull(startvp));				
		obj.put("vptime",StringUtils.noNull(vptime));				
		obj.put("vpfair",StringUtils.noNull(vpfair));				
		obj.put("vpstaff",StringUtils.noNull(vpstaff));				
		obj.put("vpover",StringUtils.noNull(vpover));				
		obj.put("wdright",StringUtils.noNull(wdright));				
		obj.put("compreq",StringUtils.noNull(compreq));				
		obj.put("vehwash",StringUtils.noNull(vehwash));				
		obj.put("contact",StringUtils.noNull(contact));				
		obj.put("repprob",StringUtils.noNull(repprob));				
		obj.put("dlrresol",StringUtils.noNull(dlrresol));				
		obj.put("startsq",StringUtils.noNull(startsq));				
		obj.put("sqtime",StringUtils.noNull(sqtime));				
		obj.put("sqthor",StringUtils.noNull(sqthor));				
		obj.put("sqclean",StringUtils.noNull(sqclean));				
		obj.put("sqover",StringUtils.noNull(sqover));				
		obj.put("osat",StringUtils.noNull(osat));				
		obj.put("satford",StringUtils.noNull(satford));				
		obj.put("svcexp",StringUtils.noNull(svcexp));				
		obj.put("recodlr",StringUtils.noNull(recodlr));				
		obj.put("revwar",StringUtils.noNull(revwar));				
		obj.put("revpwar",StringUtils.noNull(revpwar));				
		obj.put("purchase",StringUtils.noNull(purchase));				
		obj.put("recomake",StringUtils.noNull(recomake));				
		obj.put("reprmake",StringUtils.noNull(reprmake));				
		obj.put("dlrstand",StringUtils.noNull(dlrstand));				
		obj.put("reascost",StringUtils.noNull(reascost));				
		obj.put("emodeal",StringUtils.noNull(emodeal));				
		obj.put("emoford",StringUtils.noNull(emoford));				
		obj.put("svcnauth",StringUtils.noNull(svcnauth));				
		obj.put("problems",StringUtils.noNull(problems));				
		obj.put("numprob",StringUtils.noNull(numprob));				
		obj.put("toldds",StringUtils.noNull(toldds));				
		obj.put("toldcc",StringUtils.noNull(toldcc));				
		obj.put("fc",StringUtils.noNull(fc));				
		obj.put("revres",StringUtils.noNull(revres));				
		obj.put("feedbk",StringUtils.noNull(feedbk));				
		obj.put("gender",StringUtils.noNull(gender));				
		obj.put("dtmn",StringUtils.noNull(dtmn));				
		obj.put("intid",StringUtils.noNull(intid));				
		obj.put("intgen",StringUtils.noNull(intgen));				
		obj.put("lmn",StringUtils.noNull(lmn));				
		obj.put("intme",StringUtils.noNull(intme));				
		obj.put("ddate",StringUtils.noNull(ddate));				
		obj.put("svcdate",StringUtils.noNull(svcdate));				
		obj.put("dpwaitcln",StringUtils.noNull(dpwaitcln));				
		obj.put("dpcleanlou",StringUtils.noNull(dpcleanlou));				
		obj.put("dpsfdrive",StringUtils.noNull(dpsfdrive));				
		obj.put("dpsflocat",StringUtils.noNull(dpsflocat));				
		obj.put("dpsfclean",StringUtils.noNull(dpsfclean));				
		obj.put("dpsfcomfy",StringUtils.noNull(dpsfcomfy));				
		obj.put("dpsfover",StringUtils.noNull(dpsfover));				
		obj.put("irecodestart",StringUtils.noNull(irecodestart));				
		obj.put("npayamt",StringUtils.noNull(npayamt));				
		obj.put("nnpayamt",StringUtils.noNull(nnpayamt));				
		obj.put("nsvctimd",StringUtils.noNull(nsvctimd));				
		obj.put("nnsvctimd",StringUtils.noNull(nnsvctimd));				
		obj.put("nsvctimh",StringUtils.noNull(nsvctimh));				
		obj.put("nnsvctimh",StringUtils.noNull(nnsvctimh));				
		obj.put("nnumprob",StringUtils.noNull(nnumprob));				
		obj.put("nhndovr",StringUtils.noNull(nhndovr));				
		obj.put("irecodeend",StringUtils.noNull(irecodeend));				
		obj.put("intdate",StringUtils.noNull(intdate));				
		obj.put("recodestart",StringUtils.noNull(recodestart));				
		obj.put("tsisched",StringUtils.noNull(tsisched));				
		obj.put("tsiflex",StringUtils.noNull(tsiflex));				
		obj.put("tsitime",StringUtils.noNull(tsitime));				
		obj.put("tsiover",StringUtils.noNull(tsiover));				
		obj.put("tsafren",StringUtils.noNull(tsafren));				
		obj.put("tsaresp",StringUtils.noNull(tsaresp));				
		obj.put("tsathor",StringUtils.noNull(tsathor));				
		obj.put("tsaover",StringUtils.noNull(tsaover));				
		obj.put("tsfdrive",StringUtils.noNull(tsfdrive));				
		obj.put("tsflocat",StringUtils.noNull(tsflocat));				
		obj.put("tsfclean",StringUtils.noNull(tsfclean));				
		obj.put("tsfcomfy",StringUtils.noNull(tsfcomfy));				
		obj.put("tsfover",StringUtils.noNull(tsfover));				
		obj.put("tvptime",StringUtils.noNull(tvptime));				
		obj.put("tvpfair",StringUtils.noNull(tvpfair));				
		obj.put("tvpstaff",StringUtils.noNull(tvpstaff));				
		obj.put("tvpover",StringUtils.noNull(tvpover));				
		obj.put("tsqtime",StringUtils.noNull(tsqtime));				
		obj.put("tsqthor",StringUtils.noNull(tsqthor));				
		obj.put("tsqclean",StringUtils.noNull(tsqclean));				
		obj.put("tsqover",StringUtils.noNull(tsqover));				
		obj.put("tosat",StringUtils.noNull(tosat));				
		obj.put("tsatford",StringUtils.noNull(tsatford));				
		obj.put("nsisched",StringUtils.noNull(nsisched));				
		obj.put("nsiflex",StringUtils.noNull(nsiflex));				
		obj.put("nsitime",StringUtils.noNull(nsitime));				
		obj.put("nsiover",StringUtils.noNull(nsiover));				
		obj.put("nsafren",StringUtils.noNull(nsafren));				
		obj.put("nsaresp",StringUtils.noNull(nsaresp));				
		obj.put("nsathor",StringUtils.noNull(nsathor));				
		obj.put("nsaover",StringUtils.noNull(nsaover));				
		obj.put("nsfdrive",StringUtils.noNull(nsfdrive));				
		obj.put("nsflocat",StringUtils.noNull(nsflocat));				
		obj.put("nsfclean",StringUtils.noNull(nsfclean));				
		obj.put("nsfcomfy",StringUtils.noNull(nsfcomfy));				
		obj.put("nsfover",StringUtils.noNull(nsfover));				
		obj.put("nvptime",StringUtils.noNull(nvptime));				
		obj.put("nvpfair",StringUtils.noNull(nvpfair));				
		obj.put("nvpstaff",StringUtils.noNull(nvpstaff));				
		obj.put("nvpover",StringUtils.noNull(nvpover));				
		obj.put("nsqtime",StringUtils.noNull(nsqtime));				
		obj.put("nsqthor",StringUtils.noNull(nsqthor));				
		obj.put("nsqclean",StringUtils.noNull(nsqclean));				
		obj.put("nsqover",StringUtils.noNull(nsqover));				
		obj.put("nosat",StringUtils.noNull(nosat));				
		obj.put("nsatford",StringUtils.noNull(nsatford));				
		obj.put("asisched",StringUtils.noNull(asisched));				
		obj.put("asiflex",StringUtils.noNull(asiflex));				
		obj.put("asitime",StringUtils.noNull(asitime));				
		obj.put("asiover",StringUtils.noNull(asiover));				
		obj.put("asafren",StringUtils.noNull(asafren));				
		obj.put("asaresp",StringUtils.noNull(asaresp));				
		obj.put("asathor",StringUtils.noNull(asathor));				
		obj.put("asaover",StringUtils.noNull(asaover));				
		obj.put("asfdrive",StringUtils.noNull(asfdrive));				
		obj.put("asflocat",StringUtils.noNull(asflocat));				
		obj.put("asfclean",StringUtils.noNull(asfclean));				
		obj.put("asfcomfy",StringUtils.noNull(asfcomfy));				
		obj.put("asfover",StringUtils.noNull(asfover));				
		obj.put("avptime",StringUtils.noNull(avptime));				
		obj.put("avpfair",StringUtils.noNull(avpfair));				
		obj.put("avpstaff",StringUtils.noNull(avpstaff));				
		obj.put("avpover",StringUtils.noNull(avpover));				
		obj.put("asqtime",StringUtils.noNull(asqtime));				
		obj.put("asqthor",StringUtils.noNull(asqthor));				
		obj.put("asqclean",StringUtils.noNull(asqclean));				
		obj.put("asqover",StringUtils.noNull(asqover));				
		obj.put("aosat",StringUtils.noNull(aosat));				
		obj.put("asatford",StringUtils.noNull(asatford));				
		obj.put("mthown",StringUtils.noNull(mthown));				
		obj.put("timeoilh",StringUtils.noNull(timeoilh));				
		obj.put("timeoild",StringUtils.noNull(timeoild));				
		obj.put("timermh",StringUtils.noNull(timermh));				
		obj.put("timermd",StringUtils.noNull(timermd));				
		obj.put("timeerh",StringUtils.noNull(timeerh));				
		obj.put("timeerd",StringUtils.noNull(timeerd));				
		obj.put("timenerh",StringUtils.noNull(timenerh));				
		obj.put("timenerd",StringUtils.noNull(timenerd));				
		obj.put("timebph",StringUtils.noNull(timebph));				
		obj.put("timebpd",StringUtils.noNull(timebpd));				
		obj.put("nsahist",StringUtils.noNull(nsahist));				
		obj.put("nrdyprom",StringUtils.noNull(nrdyprom));				
		obj.put("nvehwash",StringUtils.noNull(nvehwash));				
		obj.put("npayment",StringUtils.noNull(npayment));				
		obj.put("rightoil",StringUtils.noNull(rightoil));				
		obj.put("rightrm",StringUtils.noNull(rightrm));				
		obj.put("righter",StringUtils.noNull(righter));				
		obj.put("rightner",StringUtils.noNull(rightner));				
		obj.put("rightbp",StringUtils.noNull(rightbp));				
		obj.put("nwdright",StringUtils.noNull(nwdright));				
		obj.put("nreptype",StringUtils.noNull(nreptype));				
		obj.put("sop",StringUtils.noNull(sop));				
		obj.put("nsop",StringUtils.noNull(nsop));				
		obj.put("nnsop",StringUtils.noNull(nnsop));				
		obj.put("rmsop",StringUtils.noNull(rmsop));				
		obj.put("nrmsop",StringUtils.noNull(nrmsop));				
		obj.put("nnrmsop",StringUtils.noNull(nnrmsop));				
		obj.put("relmeas",StringUtils.noNull(relmeas));				
		obj.put("recodeend",StringUtils.noNull(recodeend));				
		obj.put("fdstart",StringUtils.noNull(fdstart));				
		obj.put("dealer2",StringUtils.noNull(dealer2));				
		obj.put("city2",StringUtils.noNull(city2));				
		obj.put("regiona",StringUtils.noNull(regiona));				
		obj.put("state2",StringUtils.noNull(state2));				
		obj.put("city3",StringUtils.noNull(city3));				
		obj.put("model2",StringUtils.noNull(model2));				
		obj.put("index",StringUtils.noNull(index));				
		obj.put("si",StringUtils.noNull(si));				
		obj.put("sa",StringUtils.noNull(sa));				
		obj.put("sf",StringUtils.noNull(sf));				
		obj.put("vp",StringUtils.noNull(vp));				
		obj.put("sq",StringUtils.noNull(sq));				
		obj.put("nindex",StringUtils.noNull(nindex));				
		obj.put("biweekly",StringUtils.noNull(biweekly));				
		obj.put("month",StringUtils.noNull(month));				
		obj.put("quarter",StringUtils.noNull(quarter));				
		obj.put("year",StringUtils.noNull(year));				
		obj.put("rstatus",StringUtils.noNull(rstatus));				
		obj.put("lastacc",StringUtils.noNull(lastacc));				
		obj.put("createdby",StringUtils.noNull(createdby));				
		obj.put("createdat",StringUtils.noNull(createdat));				
		obj.put("modifiedat",StringUtils.noNull(modifiedat));				
		obj.put("modifiedby",StringUtils.noNull(modifiedby));				
		obj.put("closetype",StringUtils.noNull(closetype));				
		obj.put("closedat",StringUtils.noNull(closedat));
		return obj;
	}

	public void loadJSONObject(JSONObject obj)
	throws Exception
	{
		if (obj == null) return;

		id = StringUtils.getValueFromJSONObject(obj, "id");				
		srno = StringUtils.getValueFromJSONObject(obj, "srno");				
		dbsrno = StringUtils.getValueFromJSONObject(obj, "dbsrno");				
		track = StringUtils.getValueFromJSONObject(obj, "track");				
		listenobs = StringUtils.getValueFromJSONObject(obj, "listenobs");				
		intadt = StringUtils.getValueFromJSONObject(obj, "intadt");				
		tapeadt = StringUtils.getValueFromJSONObject(obj, "tapeadt");				
		thirdadt = StringUtils.getValueFromJSONObject(obj, "thirdadt");				
		cityres = StringUtils.getValueFromJSONObject(obj, "cityres");				
		dbsvdate = StringUtils.getValueFromJSONObject(obj, "dbsvdate");				
		dbpdate = StringUtils.getValueFromJSONObject(obj, "dbpdate");				
		caruse = StringUtils.getValueFromJSONObject(obj, "caruse");				
		dbdealer = StringUtils.getValueFromJSONObject(obj, "dbdealer");				
		scdlr = StringUtils.getValueFromJSONObject(obj, "scdlr");				
		svdate = StringUtils.getValueFromJSONObject(obj, "svdate");				
		rsvcdate = StringUtils.getValueFromJSONObject(obj, "rsvcdate");				
		owner = StringUtils.getValueFromJSONObject(obj, "owner");				
		dropveh = StringUtils.getValueFromJSONObject(obj, "dropveh");				
		pickveh = StringUtils.getValueFromJSONObject(obj, "pickveh");				
		oilchng = StringUtils.getValueFromJSONObject(obj, "oilchng");				
		maintain = StringUtils.getValueFromJSONObject(obj, "maintain");				
		emergrep = StringUtils.getValueFromJSONObject(obj, "emergrep");				
		nemerrep = StringUtils.getValueFromJSONObject(obj, "nemerrep");				
		bpaintrep = StringUtils.getValueFromJSONObject(obj, "bpaintrep");				
		mrdk = StringUtils.getValueFromJSONObject(obj, "mrdk");				
		svctime = StringUtils.getValueFromJSONObject(obj, "svctime");				
		cddate = StringUtils.getValueFromJSONObject(obj, "cddate");				
		scddate = StringUtils.getValueFromJSONObject(obj, "scddate");				
		qrot = StringUtils.getValueFromJSONObject(obj, "qrot");				
		notified = StringUtils.getValueFromJSONObject(obj, "notified");				
		schedule = StringUtils.getValueFromJSONObject(obj, "schedule");				
		aptdesir = StringUtils.getValueFromJSONObject(obj, "aptdesir");				
		hndovr = StringUtils.getValueFromJSONObject(obj, "hndovr");				
		startsi = StringUtils.getValueFromJSONObject(obj, "startsi");				
		sisched = StringUtils.getValueFromJSONObject(obj, "sisched");				
		siflex = StringUtils.getValueFromJSONObject(obj, "siflex");				
		sitime = StringUtils.getValueFromJSONObject(obj, "sitime");				
		siover = StringUtils.getValueFromJSONObject(obj, "siover");				
		greetwk = StringUtils.getValueFromJSONObject(obj, "greetwk");				
		reaswait = StringUtils.getValueFromJSONObject(obj, "reaswait");				
		saease = StringUtils.getValueFromJSONObject(obj, "saease");				
		saadd = StringUtils.getValueFromJSONObject(obj, "saadd");				
		saneat = StringUtils.getValueFromJSONObject(obj, "saneat");				
		saattn = StringUtils.getValueFromJSONObject(obj, "saattn");				
		sarepeat = StringUtils.getValueFromJSONObject(obj, "sarepeat");				
		safocus = StringUtils.getValueFromJSONObject(obj, "safocus");				
		sawalk = StringUtils.getValueFromJSONObject(obj, "sawalk");				
		sastatus = StringUtils.getValueFromJSONObject(obj, "sastatus");				
		sahist = StringUtils.getValueFromJSONObject(obj, "sahist");				
		priorexp = StringUtils.getValueFromJSONObject(obj, "priorexp");				
		priorfm = StringUtils.getValueFromJSONObject(obj, "priorfm");				
		estimate = StringUtils.getValueFromJSONObject(obj, "estimate");				
		givready = StringUtils.getValueFromJSONObject(obj, "givready");				
		startsa = StringUtils.getValueFromJSONObject(obj, "startsa");				
		safren = StringUtils.getValueFromJSONObject(obj, "safren");				
		saresp = StringUtils.getValueFromJSONObject(obj, "saresp");				
		sathor = StringUtils.getValueFromJSONObject(obj, "sathor");				
		saover = StringUtils.getValueFromJSONObject(obj, "saover");				
		stayleft = StringUtils.getValueFromJSONObject(obj, "stayleft");				
		offtran = StringUtils.getValueFromJSONObject(obj, "offtran");				
		waitcln = StringUtils.getValueFromJSONObject(obj, "waitcln");				
		cleanlou = StringUtils.getValueFromJSONObject(obj, "cleanlou");				
		convop = StringUtils.getValueFromJSONObject(obj, "convop");				
		facweb = StringUtils.getValueFromJSONObject(obj, "facweb");				
		facrefr = StringUtils.getValueFromJSONObject(obj, "facrefr");				
		factv = StringUtils.getValueFromJSONObject(obj, "factv");				
		facseat = StringUtils.getValueFromJSONObject(obj, "facseat");				
		facread = StringUtils.getValueFromJSONObject(obj, "facread");				
		facvpark = StringUtils.getValueFromJSONObject(obj, "facvpark");				
		facaircon = StringUtils.getValueFromJSONObject(obj, "facaircon");				
		facfood = StringUtils.getValueFromJSONObject(obj, "facfood");				
		facoth = StringUtils.getValueFromJSONObject(obj, "facoth");				
		facdk = StringUtils.getValueFromJSONObject(obj, "facdk");				
		facothv = StringUtils.getValueFromJSONObject(obj, "facothv");				
		startsf = StringUtils.getValueFromJSONObject(obj, "startsf");				
		sfdrive = StringUtils.getValueFromJSONObject(obj, "sfdrive");				
		sflocat = StringUtils.getValueFromJSONObject(obj, "sflocat");				
		sfclean = StringUtils.getValueFromJSONObject(obj, "sfclean");				
		sfcomfy = StringUtils.getValueFromJSONObject(obj, "sfcomfy");				
		sfover = StringUtils.getValueFromJSONObject(obj, "sfover");				
		delivery = StringUtils.getValueFromJSONObject(obj, "delivery");				
		payment = StringUtils.getValueFromJSONObject(obj, "payment");				
		payamt = StringUtils.getValueFromJSONObject(obj, "payamt");				
		infready = StringUtils.getValueFromJSONObject(obj, "infready");				
		explwrka = StringUtils.getValueFromJSONObject(obj, "explwrka");				
		sareview = StringUtils.getValueFromJSONObject(obj, "sareview");				
		saadvice = StringUtils.getValueFromJSONObject(obj, "saadvice");				
		explchrg = StringUtils.getValueFromJSONObject(obj, "explchrg");				
		schdnext = StringUtils.getValueFromJSONObject(obj, "schdnext");				
		reastime = StringUtils.getValueFromJSONObject(obj, "reastime");				
		svctimh = StringUtils.getValueFromJSONObject(obj, "svctimh");				
		svctimd = StringUtils.getValueFromJSONObject(obj, "svctimd");				
		rdyprom = StringUtils.getValueFromJSONObject(obj, "rdyprom");				
		startvp = StringUtils.getValueFromJSONObject(obj, "startvp");				
		vptime = StringUtils.getValueFromJSONObject(obj, "vptime");				
		vpfair = StringUtils.getValueFromJSONObject(obj, "vpfair");				
		vpstaff = StringUtils.getValueFromJSONObject(obj, "vpstaff");				
		vpover = StringUtils.getValueFromJSONObject(obj, "vpover");				
		wdright = StringUtils.getValueFromJSONObject(obj, "wdright");				
		compreq = StringUtils.getValueFromJSONObject(obj, "compreq");				
		vehwash = StringUtils.getValueFromJSONObject(obj, "vehwash");				
		contact = StringUtils.getValueFromJSONObject(obj, "contact");				
		repprob = StringUtils.getValueFromJSONObject(obj, "repprob");				
		dlrresol = StringUtils.getValueFromJSONObject(obj, "dlrresol");				
		startsq = StringUtils.getValueFromJSONObject(obj, "startsq");				
		sqtime = StringUtils.getValueFromJSONObject(obj, "sqtime");				
		sqthor = StringUtils.getValueFromJSONObject(obj, "sqthor");				
		sqclean = StringUtils.getValueFromJSONObject(obj, "sqclean");				
		sqover = StringUtils.getValueFromJSONObject(obj, "sqover");				
		osat = StringUtils.getValueFromJSONObject(obj, "osat");				
		satford = StringUtils.getValueFromJSONObject(obj, "satford");				
		svcexp = StringUtils.getValueFromJSONObject(obj, "svcexp");				
		recodlr = StringUtils.getValueFromJSONObject(obj, "recodlr");				
		revwar = StringUtils.getValueFromJSONObject(obj, "revwar");				
		revpwar = StringUtils.getValueFromJSONObject(obj, "revpwar");				
		purchase = StringUtils.getValueFromJSONObject(obj, "purchase");				
		recomake = StringUtils.getValueFromJSONObject(obj, "recomake");				
		reprmake = StringUtils.getValueFromJSONObject(obj, "reprmake");				
		dlrstand = StringUtils.getValueFromJSONObject(obj, "dlrstand");				
		reascost = StringUtils.getValueFromJSONObject(obj, "reascost");				
		emodeal = StringUtils.getValueFromJSONObject(obj, "emodeal");				
		emoford = StringUtils.getValueFromJSONObject(obj, "emoford");				
		svcnauth = StringUtils.getValueFromJSONObject(obj, "svcnauth");				
		problems = StringUtils.getValueFromJSONObject(obj, "problems");				
		numprob = StringUtils.getValueFromJSONObject(obj, "numprob");				
		toldds = StringUtils.getValueFromJSONObject(obj, "toldds");				
		toldcc = StringUtils.getValueFromJSONObject(obj, "toldcc");				
		fc = StringUtils.getValueFromJSONObject(obj, "fc");				
		revres = StringUtils.getValueFromJSONObject(obj, "revres");				
		feedbk = StringUtils.getValueFromJSONObject(obj, "feedbk");				
		gender = StringUtils.getValueFromJSONObject(obj, "gender");				
		dtmn = StringUtils.getValueFromJSONObject(obj, "dtmn");				
		intid = StringUtils.getValueFromJSONObject(obj, "intid");				
		intgen = StringUtils.getValueFromJSONObject(obj, "intgen");				
		lmn = StringUtils.getValueFromJSONObject(obj, "lmn");				
		intme = StringUtils.getValueFromJSONObject(obj, "intme");				
		ddate = StringUtils.getValueFromJSONObject(obj, "ddate");				
		svcdate = StringUtils.getValueFromJSONObject(obj, "svcdate");				
		dpwaitcln = StringUtils.getValueFromJSONObject(obj, "dpwaitcln");				
		dpcleanlou = StringUtils.getValueFromJSONObject(obj, "dpcleanlou");				
		dpsfdrive = StringUtils.getValueFromJSONObject(obj, "dpsfdrive");				
		dpsflocat = StringUtils.getValueFromJSONObject(obj, "dpsflocat");				
		dpsfclean = StringUtils.getValueFromJSONObject(obj, "dpsfclean");				
		dpsfcomfy = StringUtils.getValueFromJSONObject(obj, "dpsfcomfy");				
		dpsfover = StringUtils.getValueFromJSONObject(obj, "dpsfover");				
		irecodestart = StringUtils.getValueFromJSONObject(obj, "irecodestart");				
		npayamt = StringUtils.getValueFromJSONObject(obj, "npayamt");				
		nnpayamt = StringUtils.getValueFromJSONObject(obj, "nnpayamt");				
		nsvctimd = StringUtils.getValueFromJSONObject(obj, "nsvctimd");				
		nnsvctimd = StringUtils.getValueFromJSONObject(obj, "nnsvctimd");				
		nsvctimh = StringUtils.getValueFromJSONObject(obj, "nsvctimh");				
		nnsvctimh = StringUtils.getValueFromJSONObject(obj, "nnsvctimh");				
		nnumprob = StringUtils.getValueFromJSONObject(obj, "nnumprob");				
		nhndovr = StringUtils.getValueFromJSONObject(obj, "nhndovr");				
		irecodeend = StringUtils.getValueFromJSONObject(obj, "irecodeend");				
		intdate = StringUtils.getValueFromJSONObject(obj, "intdate");				
		recodestart = StringUtils.getValueFromJSONObject(obj, "recodestart");				
		tsisched = StringUtils.getValueFromJSONObject(obj, "tsisched");				
		tsiflex = StringUtils.getValueFromJSONObject(obj, "tsiflex");				
		tsitime = StringUtils.getValueFromJSONObject(obj, "tsitime");				
		tsiover = StringUtils.getValueFromJSONObject(obj, "tsiover");				
		tsafren = StringUtils.getValueFromJSONObject(obj, "tsafren");				
		tsaresp = StringUtils.getValueFromJSONObject(obj, "tsaresp");				
		tsathor = StringUtils.getValueFromJSONObject(obj, "tsathor");				
		tsaover = StringUtils.getValueFromJSONObject(obj, "tsaover");				
		tsfdrive = StringUtils.getValueFromJSONObject(obj, "tsfdrive");				
		tsflocat = StringUtils.getValueFromJSONObject(obj, "tsflocat");				
		tsfclean = StringUtils.getValueFromJSONObject(obj, "tsfclean");				
		tsfcomfy = StringUtils.getValueFromJSONObject(obj, "tsfcomfy");				
		tsfover = StringUtils.getValueFromJSONObject(obj, "tsfover");				
		tvptime = StringUtils.getValueFromJSONObject(obj, "tvptime");				
		tvpfair = StringUtils.getValueFromJSONObject(obj, "tvpfair");				
		tvpstaff = StringUtils.getValueFromJSONObject(obj, "tvpstaff");				
		tvpover = StringUtils.getValueFromJSONObject(obj, "tvpover");				
		tsqtime = StringUtils.getValueFromJSONObject(obj, "tsqtime");				
		tsqthor = StringUtils.getValueFromJSONObject(obj, "tsqthor");				
		tsqclean = StringUtils.getValueFromJSONObject(obj, "tsqclean");				
		tsqover = StringUtils.getValueFromJSONObject(obj, "tsqover");				
		tosat = StringUtils.getValueFromJSONObject(obj, "tosat");				
		tsatford = StringUtils.getValueFromJSONObject(obj, "tsatford");				
		nsisched = StringUtils.getValueFromJSONObject(obj, "nsisched");				
		nsiflex = StringUtils.getValueFromJSONObject(obj, "nsiflex");				
		nsitime = StringUtils.getValueFromJSONObject(obj, "nsitime");				
		nsiover = StringUtils.getValueFromJSONObject(obj, "nsiover");				
		nsafren = StringUtils.getValueFromJSONObject(obj, "nsafren");				
		nsaresp = StringUtils.getValueFromJSONObject(obj, "nsaresp");				
		nsathor = StringUtils.getValueFromJSONObject(obj, "nsathor");				
		nsaover = StringUtils.getValueFromJSONObject(obj, "nsaover");				
		nsfdrive = StringUtils.getValueFromJSONObject(obj, "nsfdrive");				
		nsflocat = StringUtils.getValueFromJSONObject(obj, "nsflocat");				
		nsfclean = StringUtils.getValueFromJSONObject(obj, "nsfclean");				
		nsfcomfy = StringUtils.getValueFromJSONObject(obj, "nsfcomfy");				
		nsfover = StringUtils.getValueFromJSONObject(obj, "nsfover");				
		nvptime = StringUtils.getValueFromJSONObject(obj, "nvptime");				
		nvpfair = StringUtils.getValueFromJSONObject(obj, "nvpfair");				
		nvpstaff = StringUtils.getValueFromJSONObject(obj, "nvpstaff");				
		nvpover = StringUtils.getValueFromJSONObject(obj, "nvpover");				
		nsqtime = StringUtils.getValueFromJSONObject(obj, "nsqtime");				
		nsqthor = StringUtils.getValueFromJSONObject(obj, "nsqthor");				
		nsqclean = StringUtils.getValueFromJSONObject(obj, "nsqclean");				
		nsqover = StringUtils.getValueFromJSONObject(obj, "nsqover");				
		nosat = StringUtils.getValueFromJSONObject(obj, "nosat");				
		nsatford = StringUtils.getValueFromJSONObject(obj, "nsatford");				
		asisched = StringUtils.getValueFromJSONObject(obj, "asisched");				
		asiflex = StringUtils.getValueFromJSONObject(obj, "asiflex");				
		asitime = StringUtils.getValueFromJSONObject(obj, "asitime");				
		asiover = StringUtils.getValueFromJSONObject(obj, "asiover");				
		asafren = StringUtils.getValueFromJSONObject(obj, "asafren");				
		asaresp = StringUtils.getValueFromJSONObject(obj, "asaresp");				
		asathor = StringUtils.getValueFromJSONObject(obj, "asathor");				
		asaover = StringUtils.getValueFromJSONObject(obj, "asaover");				
		asfdrive = StringUtils.getValueFromJSONObject(obj, "asfdrive");				
		asflocat = StringUtils.getValueFromJSONObject(obj, "asflocat");				
		asfclean = StringUtils.getValueFromJSONObject(obj, "asfclean");				
		asfcomfy = StringUtils.getValueFromJSONObject(obj, "asfcomfy");				
		asfover = StringUtils.getValueFromJSONObject(obj, "asfover");				
		avptime = StringUtils.getValueFromJSONObject(obj, "avptime");				
		avpfair = StringUtils.getValueFromJSONObject(obj, "avpfair");				
		avpstaff = StringUtils.getValueFromJSONObject(obj, "avpstaff");				
		avpover = StringUtils.getValueFromJSONObject(obj, "avpover");				
		asqtime = StringUtils.getValueFromJSONObject(obj, "asqtime");				
		asqthor = StringUtils.getValueFromJSONObject(obj, "asqthor");				
		asqclean = StringUtils.getValueFromJSONObject(obj, "asqclean");				
		asqover = StringUtils.getValueFromJSONObject(obj, "asqover");				
		aosat = StringUtils.getValueFromJSONObject(obj, "aosat");				
		asatford = StringUtils.getValueFromJSONObject(obj, "asatford");				
		mthown = StringUtils.getValueFromJSONObject(obj, "mthown");				
		timeoilh = StringUtils.getValueFromJSONObject(obj, "timeoilh");				
		timeoild = StringUtils.getValueFromJSONObject(obj, "timeoild");				
		timermh = StringUtils.getValueFromJSONObject(obj, "timermh");				
		timermd = StringUtils.getValueFromJSONObject(obj, "timermd");				
		timeerh = StringUtils.getValueFromJSONObject(obj, "timeerh");				
		timeerd = StringUtils.getValueFromJSONObject(obj, "timeerd");				
		timenerh = StringUtils.getValueFromJSONObject(obj, "timenerh");				
		timenerd = StringUtils.getValueFromJSONObject(obj, "timenerd");				
		timebph = StringUtils.getValueFromJSONObject(obj, "timebph");				
		timebpd = StringUtils.getValueFromJSONObject(obj, "timebpd");				
		nsahist = StringUtils.getValueFromJSONObject(obj, "nsahist");				
		nrdyprom = StringUtils.getValueFromJSONObject(obj, "nrdyprom");				
		nvehwash = StringUtils.getValueFromJSONObject(obj, "nvehwash");				
		npayment = StringUtils.getValueFromJSONObject(obj, "npayment");				
		rightoil = StringUtils.getValueFromJSONObject(obj, "rightoil");				
		rightrm = StringUtils.getValueFromJSONObject(obj, "rightrm");				
		righter = StringUtils.getValueFromJSONObject(obj, "righter");				
		rightner = StringUtils.getValueFromJSONObject(obj, "rightner");				
		rightbp = StringUtils.getValueFromJSONObject(obj, "rightbp");				
		nwdright = StringUtils.getValueFromJSONObject(obj, "nwdright");				
		nreptype = StringUtils.getValueFromJSONObject(obj, "nreptype");				
		sop = StringUtils.getValueFromJSONObject(obj, "sop");				
		nsop = StringUtils.getValueFromJSONObject(obj, "nsop");				
		nnsop = StringUtils.getValueFromJSONObject(obj, "nnsop");				
		rmsop = StringUtils.getValueFromJSONObject(obj, "rmsop");				
		nrmsop = StringUtils.getValueFromJSONObject(obj, "nrmsop");				
		nnrmsop = StringUtils.getValueFromJSONObject(obj, "nnrmsop");				
		relmeas = StringUtils.getValueFromJSONObject(obj, "relmeas");				
		recodeend = StringUtils.getValueFromJSONObject(obj, "recodeend");				
		fdstart = StringUtils.getValueFromJSONObject(obj, "fdstart");				
		dealer2 = StringUtils.getValueFromJSONObject(obj, "dealer2");				
		city2 = StringUtils.getValueFromJSONObject(obj, "city2");				
		regiona = StringUtils.getValueFromJSONObject(obj, "regiona");				
		state2 = StringUtils.getValueFromJSONObject(obj, "state2");				
		city3 = StringUtils.getValueFromJSONObject(obj, "city3");				
		model2 = StringUtils.getValueFromJSONObject(obj, "model2");				
		index = StringUtils.getValueFromJSONObject(obj, "index");				
		si = StringUtils.getValueFromJSONObject(obj, "si");				
		sa = StringUtils.getValueFromJSONObject(obj, "sa");				
		sf = StringUtils.getValueFromJSONObject(obj, "sf");				
		vp = StringUtils.getValueFromJSONObject(obj, "vp");				
		sq = StringUtils.getValueFromJSONObject(obj, "sq");				
		nindex = StringUtils.getValueFromJSONObject(obj, "nindex");				
		biweekly = StringUtils.getValueFromJSONObject(obj, "biweekly");				
		month = StringUtils.getValueFromJSONObject(obj, "month");				
		quarter = StringUtils.getValueFromJSONObject(obj, "quarter");				
		year = StringUtils.getValueFromJSONObject(obj, "year");				
		rstatus = StringUtils.getValueFromJSONObject(obj, "rstatus");				
		lastacc = StringUtils.getValueFromJSONObject(obj, "lastacc");				
		createdby = StringUtils.getValueFromJSONObject(obj, "createdby");				
		createdat = StringUtils.getValueFromJSONObject(obj, "createdat");				
		modifiedat = StringUtils.getValueFromJSONObject(obj, "modifiedat");				
		modifiedby = StringUtils.getValueFromJSONObject(obj, "modifiedby");				
		closetype = StringUtils.getValueFromJSONObject(obj, "closetype");				
		closedat = StringUtils.getValueFromJSONObject(obj, "closedat");
		return;
	}

	public JSONObject getJSONObjectUI()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("srno",StringUtils.noNull(srno));				
		obj.put("dbsrno",StringUtils.noNull(dbsrno));				
		obj.put("track",StringUtils.noNull(track));				
		obj.put("listenobs",StringUtils.noNull(listenobs));				
		obj.put("intadt",StringUtils.noNull(intadt));				
		obj.put("tapeadt",StringUtils.noNull(tapeadt));				
		obj.put("thirdadt",StringUtils.noNull(thirdadt));				
		obj.put("cityres",StringUtils.noNull(cityres));				
		obj.put("dbsvdate",StringUtils.noNull(dbsvdate));				
		obj.put("dbpdate",StringUtils.noNull(dbpdate));				
		obj.put("caruse",StringUtils.noNull(caruse));				
		obj.put("dbdealer",StringUtils.noNull(dbdealer));				
		obj.put("scdlr",StringUtils.noNull(scdlr));				
		obj.put("svdate",StringUtils.noNull(svdate));				
		obj.put("rsvcdate",StringUtils.noNull(rsvcdate));				
		obj.put("owner",StringUtils.noNull(owner));				
		obj.put("dropveh",StringUtils.noNull(dropveh));				
		obj.put("pickveh",StringUtils.noNull(pickveh));				
		obj.put("oilchng",StringUtils.noNull(oilchng));				
		obj.put("maintain",StringUtils.noNull(maintain));				
		obj.put("emergrep",StringUtils.noNull(emergrep));				
		obj.put("nemerrep",StringUtils.noNull(nemerrep));				
		obj.put("bpaintrep",StringUtils.noNull(bpaintrep));				
		obj.put("mrdk",StringUtils.noNull(mrdk));				
		obj.put("svctime",StringUtils.noNull(svctime));				
		obj.put("cddate",StringUtils.noNull(cddate));				
		obj.put("scddate",StringUtils.noNull(scddate));				
		obj.put("qrot",StringUtils.noNull(qrot));				
		obj.put("notified",StringUtils.noNull(notified));				
		obj.put("schedule",StringUtils.noNull(schedule));				
		obj.put("aptdesir",StringUtils.noNull(aptdesir));				
		obj.put("hndovr",StringUtils.noNull(hndovr));				
		obj.put("startsi",StringUtils.noNull(startsi));				
		obj.put("sisched",StringUtils.noNull(sisched));				
		obj.put("siflex",StringUtils.noNull(siflex));				
		obj.put("sitime",StringUtils.noNull(sitime));				
		obj.put("siover",StringUtils.noNull(siover));				
		obj.put("greetwk",StringUtils.noNull(greetwk));				
		obj.put("reaswait",StringUtils.noNull(reaswait));				
		obj.put("saease",StringUtils.noNull(saease));				
		obj.put("saadd",StringUtils.noNull(saadd));				
		obj.put("saneat",StringUtils.noNull(saneat));				
		obj.put("saattn",StringUtils.noNull(saattn));				
		obj.put("sarepeat",StringUtils.noNull(sarepeat));				
		obj.put("safocus",StringUtils.noNull(safocus));				
		obj.put("sawalk",StringUtils.noNull(sawalk));				
		obj.put("sastatus",StringUtils.noNull(sastatus));				
		obj.put("sahist",StringUtils.noNull(sahist));				
		obj.put("priorexp",StringUtils.noNull(priorexp));				
		obj.put("priorfm",StringUtils.noNull(priorfm));				
		obj.put("estimate",StringUtils.noNull(estimate));				
		obj.put("givready",StringUtils.noNull(givready));				
		obj.put("startsa",StringUtils.noNull(startsa));				
		obj.put("safren",StringUtils.noNull(safren));				
		obj.put("saresp",StringUtils.noNull(saresp));				
		obj.put("sathor",StringUtils.noNull(sathor));				
		obj.put("saover",StringUtils.noNull(saover));				
		obj.put("stayleft",StringUtils.noNull(stayleft));				
		obj.put("offtran",StringUtils.noNull(offtran));				
		obj.put("waitcln",StringUtils.noNull(waitcln));				
		obj.put("cleanlou",StringUtils.noNull(cleanlou));				
		obj.put("convop",StringUtils.noNull(convop));				
		obj.put("facweb",StringUtils.noNull(facweb));				
		obj.put("facrefr",StringUtils.noNull(facrefr));				
		obj.put("factv",StringUtils.noNull(factv));				
		obj.put("facseat",StringUtils.noNull(facseat));				
		obj.put("facread",StringUtils.noNull(facread));				
		obj.put("facvpark",StringUtils.noNull(facvpark));				
		obj.put("facaircon",StringUtils.noNull(facaircon));				
		obj.put("facfood",StringUtils.noNull(facfood));				
		obj.put("facoth",StringUtils.noNull(facoth));				
		obj.put("facdk",StringUtils.noNull(facdk));				
		obj.put("facothv",StringUtils.noNull(facothv));				
		obj.put("startsf",StringUtils.noNull(startsf));				
		obj.put("sfdrive",StringUtils.noNull(sfdrive));				
		obj.put("sflocat",StringUtils.noNull(sflocat));				
		obj.put("sfclean",StringUtils.noNull(sfclean));				
		obj.put("sfcomfy",StringUtils.noNull(sfcomfy));				
		obj.put("sfover",StringUtils.noNull(sfover));				
		obj.put("delivery",StringUtils.noNull(delivery));				
		obj.put("payment",StringUtils.noNull(payment));				
		obj.put("payamt",StringUtils.noNull(payamt));				
		obj.put("infready",StringUtils.noNull(infready));				
		obj.put("explwrka",StringUtils.noNull(explwrka));				
		obj.put("sareview",StringUtils.noNull(sareview));				
		obj.put("saadvice",StringUtils.noNull(saadvice));				
		obj.put("explchrg",StringUtils.noNull(explchrg));				
		obj.put("schdnext",StringUtils.noNull(schdnext));				
		obj.put("reastime",StringUtils.noNull(reastime));				
		obj.put("svctimh",StringUtils.noNull(svctimh));				
		obj.put("svctimd",StringUtils.noNull(svctimd));				
		obj.put("rdyprom",StringUtils.noNull(rdyprom));				
		obj.put("startvp",StringUtils.noNull(startvp));				
		obj.put("vptime",StringUtils.noNull(vptime));				
		obj.put("vpfair",StringUtils.noNull(vpfair));				
		obj.put("vpstaff",StringUtils.noNull(vpstaff));				
		obj.put("vpover",StringUtils.noNull(vpover));				
		obj.put("wdright",StringUtils.noNull(wdright));				
		obj.put("compreq",StringUtils.noNull(compreq));				
		obj.put("vehwash",StringUtils.noNull(vehwash));				
		obj.put("contact",StringUtils.noNull(contact));				
		obj.put("repprob",StringUtils.noNull(repprob));				
		obj.put("dlrresol",StringUtils.noNull(dlrresol));				
		obj.put("startsq",StringUtils.noNull(startsq));				
		obj.put("sqtime",StringUtils.noNull(sqtime));				
		obj.put("sqthor",StringUtils.noNull(sqthor));				
		obj.put("sqclean",StringUtils.noNull(sqclean));				
		obj.put("sqover",StringUtils.noNull(sqover));				
		obj.put("osat",StringUtils.noNull(osat));				
		obj.put("satford",StringUtils.noNull(satford));				
		obj.put("svcexp",StringUtils.noNull(svcexp));				
		obj.put("recodlr",StringUtils.noNull(recodlr));				
		obj.put("revwar",StringUtils.noNull(revwar));				
		obj.put("revpwar",StringUtils.noNull(revpwar));				
		obj.put("purchase",StringUtils.noNull(purchase));				
		obj.put("recomake",StringUtils.noNull(recomake));				
		obj.put("reprmake",StringUtils.noNull(reprmake));				
		obj.put("dlrstand",StringUtils.noNull(dlrstand));				
		obj.put("reascost",StringUtils.noNull(reascost));				
		obj.put("emodeal",StringUtils.noNull(emodeal));				
		obj.put("emoford",StringUtils.noNull(emoford));				
		obj.put("svcnauth",StringUtils.noNull(svcnauth));				
		obj.put("problems",StringUtils.noNull(problems));				
		obj.put("numprob",StringUtils.noNull(numprob));				
		obj.put("toldds",StringUtils.noNull(toldds));				
		obj.put("toldcc",StringUtils.noNull(toldcc));				
		obj.put("fc",StringUtils.noNull(fc));				
		obj.put("revres",StringUtils.noNull(revres));				
		obj.put("feedbk",StringUtils.noNull(feedbk));				
		obj.put("gender",StringUtils.noNull(gender));				
		obj.put("dtmn",StringUtils.noNull(dtmn));				
		obj.put("intid",StringUtils.noNull(intid));				
		obj.put("intgen",StringUtils.noNull(intgen));				
		obj.put("lmn",StringUtils.noNull(lmn));				
		obj.put("intme",StringUtils.noNull(intme));				
		obj.put("ddate",StringUtils.noNull(ddate));				
		obj.put("svcdate",StringUtils.noNull(svcdate));				
		obj.put("dp_waitcln",StringUtils.noNull(dpwaitcln));				
		obj.put("dp_cleanlou",StringUtils.noNull(dpcleanlou));				
		obj.put("dp_sfdrive",StringUtils.noNull(dpsfdrive));				
		obj.put("dp_sflocat",StringUtils.noNull(dpsflocat));				
		obj.put("dp_sfclean",StringUtils.noNull(dpsfclean));				
		obj.put("dp_sfcomfy",StringUtils.noNull(dpsfcomfy));				
		obj.put("dp_sfover",StringUtils.noNull(dpsfover));				
		obj.put("irecodestart",StringUtils.noNull(irecodestart));				
		obj.put("npayamt",StringUtils.noNull(npayamt));				
		obj.put("nnpayamt",StringUtils.noNull(nnpayamt));				
		obj.put("nsvctimd",StringUtils.noNull(nsvctimd));				
		obj.put("nnsvctimd",StringUtils.noNull(nnsvctimd));				
		obj.put("nsvctimh",StringUtils.noNull(nsvctimh));				
		obj.put("nnsvctimh",StringUtils.noNull(nnsvctimh));				
		obj.put("nnumprob",StringUtils.noNull(nnumprob));				
		obj.put("nhndovr",StringUtils.noNull(nhndovr));				
		obj.put("irecodeend",StringUtils.noNull(irecodeend));				
		obj.put("intdate",StringUtils.noNull(intdate));				
		obj.put("recodestart",StringUtils.noNull(recodestart));				
		obj.put("tsisched",StringUtils.noNull(tsisched));				
		obj.put("tsiflex",StringUtils.noNull(tsiflex));				
		obj.put("tsitime",StringUtils.noNull(tsitime));				
		obj.put("tsiover",StringUtils.noNull(tsiover));				
		obj.put("tsafren",StringUtils.noNull(tsafren));				
		obj.put("tsaresp",StringUtils.noNull(tsaresp));				
		obj.put("tsathor",StringUtils.noNull(tsathor));				
		obj.put("tsaover",StringUtils.noNull(tsaover));				
		obj.put("tsfdrive",StringUtils.noNull(tsfdrive));				
		obj.put("tsflocat",StringUtils.noNull(tsflocat));				
		obj.put("tsfclean",StringUtils.noNull(tsfclean));				
		obj.put("tsfcomfy",StringUtils.noNull(tsfcomfy));				
		obj.put("tsfover",StringUtils.noNull(tsfover));				
		obj.put("tvptime",StringUtils.noNull(tvptime));				
		obj.put("tvpfair",StringUtils.noNull(tvpfair));				
		obj.put("tvpstaff",StringUtils.noNull(tvpstaff));				
		obj.put("tvpover",StringUtils.noNull(tvpover));				
		obj.put("tsqtime",StringUtils.noNull(tsqtime));				
		obj.put("tsqthor",StringUtils.noNull(tsqthor));				
		obj.put("tsqclean",StringUtils.noNull(tsqclean));				
		obj.put("tsqover",StringUtils.noNull(tsqover));				
		obj.put("tosat",StringUtils.noNull(tosat));				
		obj.put("tsatford",StringUtils.noNull(tsatford));				
		obj.put("nsisched",StringUtils.noNull(nsisched));				
		obj.put("nsiflex",StringUtils.noNull(nsiflex));				
		obj.put("nsitime",StringUtils.noNull(nsitime));				
		obj.put("nsiover",StringUtils.noNull(nsiover));				
		obj.put("nsafren",StringUtils.noNull(nsafren));				
		obj.put("nsaresp",StringUtils.noNull(nsaresp));				
		obj.put("nsathor",StringUtils.noNull(nsathor));				
		obj.put("nsaover",StringUtils.noNull(nsaover));				
		obj.put("nsfdrive",StringUtils.noNull(nsfdrive));				
		obj.put("nsflocat",StringUtils.noNull(nsflocat));				
		obj.put("nsfclean",StringUtils.noNull(nsfclean));				
		obj.put("nsfcomfy",StringUtils.noNull(nsfcomfy));				
		obj.put("nsfover",StringUtils.noNull(nsfover));				
		obj.put("nvptime",StringUtils.noNull(nvptime));				
		obj.put("nvpfair",StringUtils.noNull(nvpfair));				
		obj.put("nvpstaff",StringUtils.noNull(nvpstaff));				
		obj.put("nvpover",StringUtils.noNull(nvpover));				
		obj.put("nsqtime",StringUtils.noNull(nsqtime));				
		obj.put("nsqthor",StringUtils.noNull(nsqthor));				
		obj.put("nsqclean",StringUtils.noNull(nsqclean));				
		obj.put("nsqover",StringUtils.noNull(nsqover));				
		obj.put("nosat",StringUtils.noNull(nosat));				
		obj.put("nsatford",StringUtils.noNull(nsatford));				
		obj.put("asisched",StringUtils.noNull(asisched));				
		obj.put("asiflex",StringUtils.noNull(asiflex));				
		obj.put("asitime",StringUtils.noNull(asitime));				
		obj.put("asiover",StringUtils.noNull(asiover));				
		obj.put("asafren",StringUtils.noNull(asafren));				
		obj.put("asaresp",StringUtils.noNull(asaresp));				
		obj.put("asathor",StringUtils.noNull(asathor));				
		obj.put("asaover",StringUtils.noNull(asaover));				
		obj.put("asfdrive",StringUtils.noNull(asfdrive));				
		obj.put("asflocat",StringUtils.noNull(asflocat));				
		obj.put("asfclean",StringUtils.noNull(asfclean));				
		obj.put("asfcomfy",StringUtils.noNull(asfcomfy));				
		obj.put("asfover",StringUtils.noNull(asfover));				
		obj.put("avptime",StringUtils.noNull(avptime));				
		obj.put("avpfair",StringUtils.noNull(avpfair));				
		obj.put("avpstaff",StringUtils.noNull(avpstaff));				
		obj.put("avpover",StringUtils.noNull(avpover));				
		obj.put("asqtime",StringUtils.noNull(asqtime));				
		obj.put("asqthor",StringUtils.noNull(asqthor));				
		obj.put("asqclean",StringUtils.noNull(asqclean));				
		obj.put("asqover",StringUtils.noNull(asqover));				
		obj.put("aosat",StringUtils.noNull(aosat));				
		obj.put("asatford",StringUtils.noNull(asatford));				
		obj.put("mthown",StringUtils.noNull(mthown));				
		obj.put("timeoilh",StringUtils.noNull(timeoilh));				
		obj.put("timeoild",StringUtils.noNull(timeoild));				
		obj.put("timermh",StringUtils.noNull(timermh));				
		obj.put("timermd",StringUtils.noNull(timermd));				
		obj.put("timeerh",StringUtils.noNull(timeerh));				
		obj.put("timeerd",StringUtils.noNull(timeerd));				
		obj.put("timenerh",StringUtils.noNull(timenerh));				
		obj.put("timenerd",StringUtils.noNull(timenerd));				
		obj.put("timebph",StringUtils.noNull(timebph));				
		obj.put("timebpd",StringUtils.noNull(timebpd));				
		obj.put("nsahist",StringUtils.noNull(nsahist));				
		obj.put("nrdyprom",StringUtils.noNull(nrdyprom));				
		obj.put("nvehwash",StringUtils.noNull(nvehwash));				
		obj.put("npayment",StringUtils.noNull(npayment));				
		obj.put("rightoil",StringUtils.noNull(rightoil));				
		obj.put("rightrm",StringUtils.noNull(rightrm));				
		obj.put("righter",StringUtils.noNull(righter));				
		obj.put("rightner",StringUtils.noNull(rightner));				
		obj.put("rightbp",StringUtils.noNull(rightbp));				
		obj.put("nwdright",StringUtils.noNull(nwdright));				
		obj.put("nreptype",StringUtils.noNull(nreptype));				
		obj.put("sop",StringUtils.noNull(sop));				
		obj.put("nsop",StringUtils.noNull(nsop));				
		obj.put("nnsop",StringUtils.noNull(nnsop));				
		obj.put("rmsop",StringUtils.noNull(rmsop));				
		obj.put("nrmsop",StringUtils.noNull(nrmsop));				
		obj.put("nnrmsop",StringUtils.noNull(nnrmsop));				
		obj.put("relmeas",StringUtils.noNull(relmeas));				
		obj.put("recodeend",StringUtils.noNull(recodeend));				
		obj.put("fdstart",StringUtils.noNull(fdstart));				
		obj.put("dealer2",StringUtils.noNull(dealer2));				
		obj.put("city2",StringUtils.noNull(city2));				
		obj.put("regiona",StringUtils.noNull(regiona));				
		obj.put("state2",StringUtils.noNull(state2));				
		obj.put("city3",StringUtils.noNull(city3));				
		obj.put("model2",StringUtils.noNull(model2));				
		obj.put("index",StringUtils.noNull(index));				
		obj.put("si",StringUtils.noNull(si));				
		obj.put("sa",StringUtils.noNull(sa));				
		obj.put("sf",StringUtils.noNull(sf));				
		obj.put("vp",StringUtils.noNull(vp));				
		obj.put("sq",StringUtils.noNull(sq));				
		obj.put("nindex",StringUtils.noNull(nindex));				
		obj.put("bi_weekly",StringUtils.noNull(biweekly));				
		obj.put("month",StringUtils.noNull(month));				
		obj.put("quarter",StringUtils.noNull(quarter));				
		obj.put("year",StringUtils.noNull(year));				
		obj.put("rstatus",StringUtils.noNull(rstatus));				
		obj.put("lastacc",StringUtils.noNull(lastacc));				
		obj.put("created_by",StringUtils.noNull(createdby));				
		obj.put("created_at",StringUtils.noNull(createdat));				
		obj.put("modified_at",StringUtils.noNull(modifiedat));				
		obj.put("modified_by",StringUtils.noNull(modifiedby));				
		obj.put("close_type",StringUtils.noNull(closetype));				
		obj.put("closed_at",StringUtils.noNull(closedat));
		return obj;
	}

	public void log()
	{
		logger.trace(this.toString());
	}

	public HashMap getTableMap()
	{
		HashMap resultMap = new HashMap();
		ArrayList columnList = new ArrayList();
		resultMap.put("table", "translated");

		columnList.add("id");				
		columnList.add("srno");				
		columnList.add("dbsrno");				
		columnList.add("track");				
		columnList.add("listenobs");				
		columnList.add("intadt");				
		columnList.add("tapeadt");				
		columnList.add("thirdadt");				
		columnList.add("cityres");				
		columnList.add("dbsvdate");				
		columnList.add("dbpdate");				
		columnList.add("caruse");				
		columnList.add("dbdealer");				
		columnList.add("scdlr");				
		columnList.add("svdate");				
		columnList.add("rsvcdate");				
		columnList.add("owner");				
		columnList.add("dropveh");				
		columnList.add("pickveh");				
		columnList.add("oilchng");				
		columnList.add("maintain");				
		columnList.add("emergrep");				
		columnList.add("nemerrep");				
		columnList.add("bpaintrep");				
		columnList.add("mrdk");				
		columnList.add("svctime");				
		columnList.add("cddate");				
		columnList.add("scddate");				
		columnList.add("qrot");				
		columnList.add("notified");				
		columnList.add("schedule");				
		columnList.add("aptdesir");				
		columnList.add("hndovr");				
		columnList.add("startsi");				
		columnList.add("sisched");				
		columnList.add("siflex");				
		columnList.add("sitime");				
		columnList.add("siover");				
		columnList.add("greetwk");				
		columnList.add("reaswait");				
		columnList.add("saease");				
		columnList.add("saadd");				
		columnList.add("saneat");				
		columnList.add("saattn");				
		columnList.add("sarepeat");				
		columnList.add("safocus");				
		columnList.add("sawalk");				
		columnList.add("sastatus");				
		columnList.add("sahist");				
		columnList.add("priorexp");				
		columnList.add("priorfm");				
		columnList.add("estimate");				
		columnList.add("givready");				
		columnList.add("startsa");				
		columnList.add("safren");				
		columnList.add("saresp");				
		columnList.add("sathor");				
		columnList.add("saover");				
		columnList.add("stayleft");				
		columnList.add("offtran");				
		columnList.add("waitcln");				
		columnList.add("cleanlou");				
		columnList.add("convop");				
		columnList.add("facweb");				
		columnList.add("facrefr");				
		columnList.add("factv");				
		columnList.add("facseat");				
		columnList.add("facread");				
		columnList.add("facvpark");				
		columnList.add("facaircon");				
		columnList.add("facfood");				
		columnList.add("facoth");				
		columnList.add("facdk");				
		columnList.add("facothv");				
		columnList.add("startsf");				
		columnList.add("sfdrive");				
		columnList.add("sflocat");				
		columnList.add("sfclean");				
		columnList.add("sfcomfy");				
		columnList.add("sfover");				
		columnList.add("delivery");				
		columnList.add("payment");				
		columnList.add("payamt");				
		columnList.add("infready");				
		columnList.add("explwrka");				
		columnList.add("sareview");				
		columnList.add("saadvice");				
		columnList.add("explchrg");				
		columnList.add("schdnext");				
		columnList.add("reastime");				
		columnList.add("svctimh");				
		columnList.add("svctimd");				
		columnList.add("rdyprom");				
		columnList.add("startvp");				
		columnList.add("vptime");				
		columnList.add("vpfair");				
		columnList.add("vpstaff");				
		columnList.add("vpover");				
		columnList.add("wdright");				
		columnList.add("compreq");				
		columnList.add("vehwash");				
		columnList.add("contact");				
		columnList.add("repprob");				
		columnList.add("dlrresol");				
		columnList.add("startsq");				
		columnList.add("sqtime");				
		columnList.add("sqthor");				
		columnList.add("sqclean");				
		columnList.add("sqover");				
		columnList.add("osat");				
		columnList.add("satford");				
		columnList.add("svcexp");				
		columnList.add("recodlr");				
		columnList.add("revwar");				
		columnList.add("revpwar");				
		columnList.add("purchase");				
		columnList.add("recomake");				
		columnList.add("reprmake");				
		columnList.add("dlrstand");				
		columnList.add("reascost");				
		columnList.add("emodeal");				
		columnList.add("emoford");				
		columnList.add("svcnauth");				
		columnList.add("problems");				
		columnList.add("numprob");				
		columnList.add("toldds");				
		columnList.add("toldcc");				
		columnList.add("fc");				
		columnList.add("revres");				
		columnList.add("feedbk");				
		columnList.add("gender");				
		columnList.add("dtmn");				
		columnList.add("intid");				
		columnList.add("intgen");				
		columnList.add("lmn");				
		columnList.add("intme");				
		columnList.add("ddate");				
		columnList.add("svcdate");				
		columnList.add("dp_waitcln");				
		columnList.add("dp_cleanlou");				
		columnList.add("dp_sfdrive");				
		columnList.add("dp_sflocat");				
		columnList.add("dp_sfclean");				
		columnList.add("dp_sfcomfy");				
		columnList.add("dp_sfover");				
		columnList.add("irecodestart");				
		columnList.add("npayamt");				
		columnList.add("nnpayamt");				
		columnList.add("nsvctimd");				
		columnList.add("nnsvctimd");				
		columnList.add("nsvctimh");				
		columnList.add("nnsvctimh");				
		columnList.add("nnumprob");				
		columnList.add("nhndovr");				
		columnList.add("irecodeend");				
		columnList.add("intdate");				
		columnList.add("recodestart");				
		columnList.add("tsisched");				
		columnList.add("tsiflex");				
		columnList.add("tsitime");				
		columnList.add("tsiover");				
		columnList.add("tsafren");				
		columnList.add("tsaresp");				
		columnList.add("tsathor");				
		columnList.add("tsaover");				
		columnList.add("tsfdrive");				
		columnList.add("tsflocat");				
		columnList.add("tsfclean");				
		columnList.add("tsfcomfy");				
		columnList.add("tsfover");				
		columnList.add("tvptime");				
		columnList.add("tvpfair");				
		columnList.add("tvpstaff");				
		columnList.add("tvpover");				
		columnList.add("tsqtime");				
		columnList.add("tsqthor");				
		columnList.add("tsqclean");				
		columnList.add("tsqover");				
		columnList.add("tosat");				
		columnList.add("tsatford");				
		columnList.add("nsisched");				
		columnList.add("nsiflex");				
		columnList.add("nsitime");				
		columnList.add("nsiover");				
		columnList.add("nsafren");				
		columnList.add("nsaresp");				
		columnList.add("nsathor");				
		columnList.add("nsaover");				
		columnList.add("nsfdrive");				
		columnList.add("nsflocat");				
		columnList.add("nsfclean");				
		columnList.add("nsfcomfy");				
		columnList.add("nsfover");				
		columnList.add("nvptime");				
		columnList.add("nvpfair");				
		columnList.add("nvpstaff");				
		columnList.add("nvpover");				
		columnList.add("nsqtime");				
		columnList.add("nsqthor");				
		columnList.add("nsqclean");				
		columnList.add("nsqover");				
		columnList.add("nosat");				
		columnList.add("nsatford");				
		columnList.add("asisched");				
		columnList.add("asiflex");				
		columnList.add("asitime");				
		columnList.add("asiover");				
		columnList.add("asafren");				
		columnList.add("asaresp");				
		columnList.add("asathor");				
		columnList.add("asaover");				
		columnList.add("asfdrive");				
		columnList.add("asflocat");				
		columnList.add("asfclean");				
		columnList.add("asfcomfy");				
		columnList.add("asfover");				
		columnList.add("avptime");				
		columnList.add("avpfair");				
		columnList.add("avpstaff");				
		columnList.add("avpover");				
		columnList.add("asqtime");				
		columnList.add("asqthor");				
		columnList.add("asqclean");				
		columnList.add("asqover");				
		columnList.add("aosat");				
		columnList.add("asatford");				
		columnList.add("mthown");				
		columnList.add("timeoilh");				
		columnList.add("timeoild");				
		columnList.add("timermh");				
		columnList.add("timermd");				
		columnList.add("timeerh");				
		columnList.add("timeerd");				
		columnList.add("timenerh");				
		columnList.add("timenerd");				
		columnList.add("timebph");				
		columnList.add("timebpd");				
		columnList.add("nsahist");				
		columnList.add("nrdyprom");				
		columnList.add("nvehwash");				
		columnList.add("npayment");				
		columnList.add("rightoil");				
		columnList.add("rightrm");				
		columnList.add("righter");				
		columnList.add("rightner");				
		columnList.add("rightbp");				
		columnList.add("nwdright");				
		columnList.add("nreptype");				
		columnList.add("sop");				
		columnList.add("nsop");				
		columnList.add("nnsop");				
		columnList.add("rmsop");				
		columnList.add("nrmsop");				
		columnList.add("nnrmsop");				
		columnList.add("relmeas");				
		columnList.add("recodeend");				
		columnList.add("fdstart");				
		columnList.add("dealer2");				
		columnList.add("city2");				
		columnList.add("regiona");				
		columnList.add("state2");				
		columnList.add("city3");				
		columnList.add("model2");				
		columnList.add("index");				
		columnList.add("si");				
		columnList.add("sa");				
		columnList.add("sf");				
		columnList.add("vp");				
		columnList.add("sq");				
		columnList.add("nindex");				
		columnList.add("bi_weekly");				
		columnList.add("month");				
		columnList.add("quarter");				
		columnList.add("year");				
		columnList.add("rstatus");				
		columnList.add("lastacc");				
		columnList.add("created_by");				
		columnList.add("created_at");				
		columnList.add("modified_at");				
		columnList.add("modified_by");				
		columnList.add("close_type");				
		columnList.add("closed_at");
		resultMap.put("ColumnList", columnList);

		return resultMap;
	}

	public void toggleStatus()
	{
		String currentStatus = StringUtils.noNull(getRstatus());
		setRstatus("1");
		if (currentStatus.equals("1"))
		{
			setRstatus("0");
		}
	}

}
