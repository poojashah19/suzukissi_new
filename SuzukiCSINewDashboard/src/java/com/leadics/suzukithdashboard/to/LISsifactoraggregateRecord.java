
/*
 * LISsifactoraggregateRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.to;
import com.leadics.suzukithdashboard.common.LIRecord;
import com.leadics.utils.*;
import org.json.simple.*;
import java.util.*;
public class LISsifactoraggregateRecord extends LIRecord
{
	static LogUtils logger = new LogUtils(LISsifactoraggregateRecord.class.getName());

	private String id;
	private String factor;
	private String factorname;
	private String factorindex;
	private String region;
	private String state;
	private String city;
	private String dealer;
	private String model;
	private String biweekly;
	private String month;
	private String quarter;
	private String dealerfactorsum;
	private String dealerfactorcount;
	private String totalcount;
	private String rstatus;
	private String lastacc;
	private String createdby;
	private String createdat;
	private String modifiedat;
	private String modifiedby;
	private String closetype;
	private String closedat;

	public String getId()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(id);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(id);
		}
		else
		{
			return id;
		}
	}

	public String getFactor()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(factor);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(factor);
		}
		else
		{
			return factor;
		}
	}

	public String getFactorname()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(factorname);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(factorname);
		}
		else
		{
			return factorname;
		}
	}

	public String getFactorindex()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(factorindex);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(factorindex);
		}
		else
		{
			return factorindex;
		}
	}

	public String getRegion()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(region);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(region);
		}
		else
		{
			return region;
		}
	}

	public String getState()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(state);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(state);
		}
		else
		{
			return state;
		}
	}

	public String getCity()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(city);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(city);
		}
		else
		{
			return city;
		}
	}

	public String getDealer()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dealer);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dealer);
		}
		else
		{
			return dealer;
		}
	}

	public String getModel()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(model);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(model);
		}
		else
		{
			return model;
		}
	}

	public String getBiweekly()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(biweekly);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(biweekly);
		}
		else
		{
			return biweekly;
		}
	}

	public String getMonth()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(month);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(month);
		}
		else
		{
			return month;
		}
	}

	public String getQuarter()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(quarter);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(quarter);
		}
		else
		{
			return quarter;
		}
	}

	public String getDealerfactorsum()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dealerfactorsum);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dealerfactorsum);
		}
		else
		{
			return dealerfactorsum;
		}
	}

	public String getDealerfactorcount()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dealerfactorcount);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dealerfactorcount);
		}
		else
		{
			return dealerfactorcount;
		}
	}

	public String getTotalcount()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(totalcount);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(totalcount);
		}
		else
		{
			return totalcount;
		}
	}

	public String getRstatus()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(rstatus);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(rstatus);
		}
		else
		{
			return rstatus;
		}
	}

	public String getLastacc()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(lastacc);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(lastacc);
		}
		else
		{
			return lastacc;
		}
	}

	public String getCreatedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdby);
		}
		else
		{
			return createdby;
		}
	}

	public String getCreatedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdat);
		}
		else
		{
			return createdat;
		}
	}

	public String getModifiedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedat);
		}
		else
		{
			return modifiedat;
		}
	}

	public String getModifiedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedby);
		}
		else
		{
			return modifiedby;
		}
	}

	public String getClosetype()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(closetype);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(closetype);
		}
		else
		{
			return closetype;
		}
	}

	public String getClosedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(closedat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(closedat);
		}
		else
		{
			return closedat;
		}
	}


	public void setId(String value)
	{
		id = value;
	}

	public void setFactor(String value)
	{
		factor = value;
	}

	public void setFactorname(String value)
	{
		factorname = value;
	}

	public void setFactorindex(String value)
	{
		factorindex = value;
	}

	public void setRegion(String value)
	{
		region = value;
	}

	public void setState(String value)
	{
		state = value;
	}

	public void setCity(String value)
	{
		city = value;
	}

	public void setDealer(String value)
	{
		dealer = value;
	}

	public void setModel(String value)
	{
		model = value;
	}

	public void setBiweekly(String value)
	{
		biweekly = value;
	}

	public void setMonth(String value)
	{
		month = value;
	}

	public void setQuarter(String value)
	{
		quarter = value;
	}

	public void setDealerfactorsum(String value)
	{
		dealerfactorsum = value;
	}

	public void setDealerfactorcount(String value)
	{
		dealerfactorcount = value;
	}

	public void setTotalcount(String value)
	{
		totalcount = value;
	}

	public void setRstatus(String value)
	{
		rstatus = value;
	}

	public void setLastacc(String value)
	{
		lastacc = value;
	}

	public void setCreatedby(String value)
	{
		createdby = value;
	}

	public void setCreatedat(String value)
	{
		createdat = value;
	}

	public void setModifiedat(String value)
	{
		modifiedat = value;
	}

	public void setModifiedby(String value)
	{
		modifiedby = value;
	}

	public void setClosetype(String value)
	{
		closetype = value;
	}

	public void setClosedat(String value)
	{
		closedat = value;
	}


	public String toString()
	{
		return "\nid:" + id +
				"\nfactor:" + factor +
				"\nfactorname:" + factorname +
				"\nfactorindex:" + factorindex +
				"\nregion:" + region +
				"\nstate:" + state +
				"\ncity:" + city +
				"\ndealer:" + dealer +
				"\nmodel:" + model +
				"\nbiweekly:" + biweekly +
				"\nmonth:" + month +
				"\nquarter:" + quarter +
				"\ndealerfactorsum:" + dealerfactorsum +
				"\ndealerfactorcount:" + dealerfactorcount +
				"\ntotalcount:" + totalcount +
				"\nrstatus:" + rstatus +
				"\nlastacc:" + lastacc +
				"\ncreatedby:" + createdby +
				"\ncreatedat:" + createdat +
				"\nmodifiedat:" + modifiedat +
				"\nmodifiedby:" + modifiedby +
				"\nclosetype:" + closetype +
				"\nclosedat:" + closedat +
				"\n";
	}

	public void loadContent(LISsifactoraggregateRecord inputRecord)
	{
		setId(inputRecord.getId());
		setFactor(inputRecord.getFactor());
		setFactorname(inputRecord.getFactorname());
		setFactorindex(inputRecord.getFactorindex());
		setRegion(inputRecord.getRegion());
		setState(inputRecord.getState());
		setCity(inputRecord.getCity());
		setDealer(inputRecord.getDealer());
		setModel(inputRecord.getModel());
		setBiweekly(inputRecord.getBiweekly());
		setMonth(inputRecord.getMonth());
		setQuarter(inputRecord.getQuarter());
		setDealerfactorsum(inputRecord.getDealerfactorsum());
		setDealerfactorcount(inputRecord.getDealerfactorcount());
		setTotalcount(inputRecord.getTotalcount());
		setRstatus(inputRecord.getRstatus());
		setLastacc(inputRecord.getLastacc());
		setCreatedby(inputRecord.getCreatedby());
		setCreatedat(inputRecord.getCreatedat());
		setModifiedat(inputRecord.getModifiedat());
		setModifiedby(inputRecord.getModifiedby());
		setClosetype(inputRecord.getClosetype());
		setClosedat(inputRecord.getClosedat());
	}

	public void loadNonNullContent(LISsifactoraggregateRecord inputRecord)
	{
		if (StringUtils.hasChanged(getId(), inputRecord.getId()))
		{
			setId(StringUtils.noNull(inputRecord.getId()));
		}
		if (StringUtils.hasChanged(getFactor(), inputRecord.getFactor()))
		{
			setFactor(StringUtils.noNull(inputRecord.getFactor()));
		}
		if (StringUtils.hasChanged(getFactorname(), inputRecord.getFactorname()))
		{
			setFactorname(StringUtils.noNull(inputRecord.getFactorname()));
		}
		if (StringUtils.hasChanged(getFactorindex(), inputRecord.getFactorindex()))
		{
			setFactorindex(StringUtils.noNull(inputRecord.getFactorindex()));
		}
		if (StringUtils.hasChanged(getRegion(), inputRecord.getRegion()))
		{
			setRegion(StringUtils.noNull(inputRecord.getRegion()));
		}
		if (StringUtils.hasChanged(getState(), inputRecord.getState()))
		{
			setState(StringUtils.noNull(inputRecord.getState()));
		}
		if (StringUtils.hasChanged(getCity(), inputRecord.getCity()))
		{
			setCity(StringUtils.noNull(inputRecord.getCity()));
		}
		if (StringUtils.hasChanged(getDealer(), inputRecord.getDealer()))
		{
			setDealer(StringUtils.noNull(inputRecord.getDealer()));
		}
		if (StringUtils.hasChanged(getModel(), inputRecord.getModel()))
		{
			setModel(StringUtils.noNull(inputRecord.getModel()));
		}
		if (StringUtils.hasChanged(getBiweekly(), inputRecord.getBiweekly()))
		{
			setBiweekly(StringUtils.noNull(inputRecord.getBiweekly()));
		}
		if (StringUtils.hasChanged(getMonth(), inputRecord.getMonth()))
		{
			setMonth(StringUtils.noNull(inputRecord.getMonth()));
		}
		if (StringUtils.hasChanged(getQuarter(), inputRecord.getQuarter()))
		{
			setQuarter(StringUtils.noNull(inputRecord.getQuarter()));
		}
		if (StringUtils.hasChanged(getDealerfactorsum(), inputRecord.getDealerfactorsum()))
		{
			setDealerfactorsum(StringUtils.noNull(inputRecord.getDealerfactorsum()));
		}
		if (StringUtils.hasChanged(getDealerfactorcount(), inputRecord.getDealerfactorcount()))
		{
			setDealerfactorcount(StringUtils.noNull(inputRecord.getDealerfactorcount()));
		}
		if (StringUtils.hasChanged(getTotalcount(), inputRecord.getTotalcount()))
		{
			setTotalcount(StringUtils.noNull(inputRecord.getTotalcount()));
		}
		if (StringUtils.hasChanged(getRstatus(), inputRecord.getRstatus()))
		{
			setRstatus(StringUtils.noNull(inputRecord.getRstatus()));
		}
		if (StringUtils.hasChanged(getLastacc(), inputRecord.getLastacc()))
		{
			setLastacc(StringUtils.noNull(inputRecord.getLastacc()));
		}
		if (StringUtils.hasChanged(getCreatedby(), inputRecord.getCreatedby()))
		{
			setCreatedby(StringUtils.noNull(inputRecord.getCreatedby()));
		}
		if (StringUtils.hasChanged(getCreatedat(), inputRecord.getCreatedat()))
		{
			setCreatedat(StringUtils.noNull(inputRecord.getCreatedat()));
		}
		if (StringUtils.hasChanged(getModifiedat(), inputRecord.getModifiedat()))
		{
			setModifiedat(StringUtils.noNull(inputRecord.getModifiedat()));
		}
		if (StringUtils.hasChanged(getModifiedby(), inputRecord.getModifiedby()))
		{
			setModifiedby(StringUtils.noNull(inputRecord.getModifiedby()));
		}
		if (StringUtils.hasChanged(getClosetype(), inputRecord.getClosetype()))
		{
			setClosetype(StringUtils.noNull(inputRecord.getClosetype()));
		}
		if (StringUtils.hasChanged(getClosedat(), inputRecord.getClosedat()))
		{
			setClosedat(StringUtils.noNull(inputRecord.getClosedat()));
		}
	}

	public JSONObject getJSONObject()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("factor",StringUtils.noNull(factor));				
		obj.put("factorname",StringUtils.noNull(factorname));				
		obj.put("factorindex",StringUtils.noNull(factorindex));				
		obj.put("region",StringUtils.noNull(region));				
		obj.put("state",StringUtils.noNull(state));				
		obj.put("city",StringUtils.noNull(city));				
		obj.put("dealer",StringUtils.noNull(dealer));				
		obj.put("model",StringUtils.noNull(model));				
		obj.put("biweekly",StringUtils.noNull(biweekly));				
		obj.put("month",StringUtils.noNull(month));				
		obj.put("quarter",StringUtils.noNull(quarter));				
		obj.put("dealerfactorsum",StringUtils.noNull(dealerfactorsum));				
		obj.put("dealerfactorcount",StringUtils.noNull(dealerfactorcount));				
		obj.put("totalcount",StringUtils.noNull(totalcount));				
		obj.put("rstatus",StringUtils.noNull(rstatus));				
		obj.put("lastacc",StringUtils.noNull(lastacc));				
		obj.put("createdby",StringUtils.noNull(createdby));				
		obj.put("createdat",StringUtils.noNull(createdat));				
		obj.put("modifiedat",StringUtils.noNull(modifiedat));				
		obj.put("modifiedby",StringUtils.noNull(modifiedby));				
		obj.put("closetype",StringUtils.noNull(closetype));				
		obj.put("closedat",StringUtils.noNull(closedat));
		return obj;
	}

	public void loadJSONObject(JSONObject obj)
	throws Exception
	{
		if (obj == null) return;

		id = StringUtils.getValueFromJSONObject(obj, "id");				
		factor = StringUtils.getValueFromJSONObject(obj, "factor");				
		factorname = StringUtils.getValueFromJSONObject(obj, "factorname");				
		factorindex = StringUtils.getValueFromJSONObject(obj, "factorindex");				
		region = StringUtils.getValueFromJSONObject(obj, "region");				
		state = StringUtils.getValueFromJSONObject(obj, "state");				
		city = StringUtils.getValueFromJSONObject(obj, "city");				
		dealer = StringUtils.getValueFromJSONObject(obj, "dealer");				
		model = StringUtils.getValueFromJSONObject(obj, "model");				
		biweekly = StringUtils.getValueFromJSONObject(obj, "biweekly");				
		month = StringUtils.getValueFromJSONObject(obj, "month");				
		quarter = StringUtils.getValueFromJSONObject(obj, "quarter");				
		dealerfactorsum = StringUtils.getValueFromJSONObject(obj, "dealerfactorsum");				
		dealerfactorcount = StringUtils.getValueFromJSONObject(obj, "dealerfactorcount");				
		totalcount = StringUtils.getValueFromJSONObject(obj, "totalcount");				
		rstatus = StringUtils.getValueFromJSONObject(obj, "rstatus");				
		lastacc = StringUtils.getValueFromJSONObject(obj, "lastacc");				
		createdby = StringUtils.getValueFromJSONObject(obj, "createdby");				
		createdat = StringUtils.getValueFromJSONObject(obj, "createdat");				
		modifiedat = StringUtils.getValueFromJSONObject(obj, "modifiedat");				
		modifiedby = StringUtils.getValueFromJSONObject(obj, "modifiedby");				
		closetype = StringUtils.getValueFromJSONObject(obj, "closetype");				
		closedat = StringUtils.getValueFromJSONObject(obj, "closedat");
		return;
	}

	public JSONObject getJSONObjectUI()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("factor",StringUtils.noNull(factor));				
		obj.put("factorname",StringUtils.noNull(factorname));				
		obj.put("factorindex",StringUtils.noNull(factorindex));				
		obj.put("region",StringUtils.noNull(region));				
		obj.put("state",StringUtils.noNull(state));				
		obj.put("city",StringUtils.noNull(city));				
		obj.put("dealer",StringUtils.noNull(dealer));				
		obj.put("model",StringUtils.noNull(model));				
		obj.put("bi_weekly",StringUtils.noNull(biweekly));				
		obj.put("month",StringUtils.noNull(month));				
		obj.put("quarter",StringUtils.noNull(quarter));				
		obj.put("dealer_factor_sum",StringUtils.noNull(dealerfactorsum));				
		obj.put("dealer_factor_count",StringUtils.noNull(dealerfactorcount));				
		obj.put("totalcount",StringUtils.noNull(totalcount));				
		obj.put("rstatus",StringUtils.noNull(rstatus));				
		obj.put("lastacc",StringUtils.noNull(lastacc));				
		obj.put("created_by",StringUtils.noNull(createdby));				
		obj.put("created_at",StringUtils.noNull(createdat));				
		obj.put("modified_at",StringUtils.noNull(modifiedat));				
		obj.put("modified_by",StringUtils.noNull(modifiedby));				
		obj.put("close_type",StringUtils.noNull(closetype));				
		obj.put("closed_at",StringUtils.noNull(closedat));
		return obj;
	}

	public void log()
	{
		logger.trace(this.toString());
	}

	public HashMap getTableMap()
	{
		HashMap resultMap = new HashMap();
		ArrayList columnList = new ArrayList();
		resultMap.put("table", "ssi_factor_aggregate");

		columnList.add("id");				
		columnList.add("factor");				
		columnList.add("factorname");				
		columnList.add("factorindex");				
		columnList.add("region");				
		columnList.add("state");				
		columnList.add("city");				
		columnList.add("dealer");				
		columnList.add("model");				
		columnList.add("bi_weekly");				
		columnList.add("month");				
		columnList.add("quarter");				
		columnList.add("dealer_factor_sum");				
		columnList.add("dealer_factor_count");				
		columnList.add("totalcount");				
		columnList.add("rstatus");				
		columnList.add("lastacc");				
		columnList.add("created_by");				
		columnList.add("created_at");				
		columnList.add("modified_at");				
		columnList.add("modified_by");				
		columnList.add("close_type");				
		columnList.add("closed_at");
		resultMap.put("ColumnList", columnList);

		return resultMap;
	}

	public void toggleStatus()
	{
		String currentStatus = StringUtils.noNull(getRstatus());
		setRstatus("1");
		if (currentStatus.equals("1"))
		{
			setRstatus("0");
		}
	}

}
