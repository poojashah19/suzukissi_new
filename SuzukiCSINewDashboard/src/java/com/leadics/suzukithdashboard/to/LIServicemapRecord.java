
/*
 * LIServicemapRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.to;
import com.leadics.suzukithdashboard.common.LIRecord;
import com.leadics.utils.*;
import org.json.simple.*;
import java.util.*;
public class LIServicemapRecord extends LIRecord
{
	static LogUtils logger = new LogUtils(LIServicemapRecord.class.getName());

	private String id;
	private String servicecode;
	private String serviceclass;
	private String servicemethod;
	private String intenv;
	private String rstatus;
	private String createdby;
	private String createdat;
	private String modifiedat;
	private String modifiedby;
	private String servicetitle;
	private String instid;
	private String madeby;
	private String madeat;
	private String checkedby;
	private String checkedat;
	private String makerlastcmt;
	private String checkerlastcmt;
	private String currappstatus;
	private String adminlastcmt;
	private String institutionid;

	public String getId()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(id);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(id);
		}
		else
		{
			return id;
		}
	}

	public String getServicecode()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(servicecode);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(servicecode);
		}
		else
		{
			return servicecode;
		}
	}

	public String getServiceclass()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(serviceclass);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(serviceclass);
		}
		else
		{
			return serviceclass;
		}
	}

	public String getServicemethod()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(servicemethod);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(servicemethod);
		}
		else
		{
			return servicemethod;
		}
	}

	public String getIntenv()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(intenv);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(intenv);
		}
		else
		{
			return intenv;
		}
	}

	public String getRstatus()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(rstatus);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(rstatus);
		}
		else
		{
			return rstatus;
		}
	}

	public String getCreatedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdby);
		}
		else
		{
			return createdby;
		}
	}

	public String getCreatedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdat);
		}
		else
		{
			return createdat;
		}
	}

	public String getModifiedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedat);
		}
		else
		{
			return modifiedat;
		}
	}

	public String getModifiedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedby);
		}
		else
		{
			return modifiedby;
		}
	}

	public String getServicetitle()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(servicetitle);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(servicetitle);
		}
		else
		{
			return servicetitle;
		}
	}

	public String getInstid()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(instid);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(instid);
		}
		else
		{
			return instid;
		}
	}

	public String getMadeby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(madeby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(madeby);
		}
		else
		{
			return madeby;
		}
	}

	public String getMadeat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(madeat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(madeat);
		}
		else
		{
			return madeat;
		}
	}

	public String getCheckedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(checkedby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(checkedby);
		}
		else
		{
			return checkedby;
		}
	}

	public String getCheckedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(checkedat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(checkedat);
		}
		else
		{
			return checkedat;
		}
	}

	public String getMakerlastcmt()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(makerlastcmt);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(makerlastcmt);
		}
		else
		{
			return makerlastcmt;
		}
	}

	public String getCheckerlastcmt()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(checkerlastcmt);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(checkerlastcmt);
		}
		else
		{
			return checkerlastcmt;
		}
	}

	public String getCurrappstatus()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(currappstatus);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(currappstatus);
		}
		else
		{
			return currappstatus;
		}
	}

	public String getAdminlastcmt()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(adminlastcmt);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(adminlastcmt);
		}
		else
		{
			return adminlastcmt;
		}
	}

	public String getInstitutionid()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(institutionid);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(institutionid);
		}
		else
		{
			return institutionid;
		}
	}


	public void setId(String value)
	{
		id = value;
	}

	public void setServicecode(String value)
	{
		servicecode = value;
	}

	public void setServiceclass(String value)
	{
		serviceclass = value;
	}

	public void setServicemethod(String value)
	{
		servicemethod = value;
	}

	public void setIntenv(String value)
	{
		intenv = value;
	}

	public void setRstatus(String value)
	{
		rstatus = value;
	}

	public void setCreatedby(String value)
	{
		createdby = value;
	}

	public void setCreatedat(String value)
	{
		createdat = value;
	}

	public void setModifiedat(String value)
	{
		modifiedat = value;
	}

	public void setModifiedby(String value)
	{
		modifiedby = value;
	}

	public void setServicetitle(String value)
	{
		servicetitle = value;
	}

	public void setInstid(String value)
	{
		instid = value;
	}

	public void setMadeby(String value)
	{
		madeby = value;
	}

	public void setMadeat(String value)
	{
		madeat = value;
	}

	public void setCheckedby(String value)
	{
		checkedby = value;
	}

	public void setCheckedat(String value)
	{
		checkedat = value;
	}

	public void setMakerlastcmt(String value)
	{
		makerlastcmt = value;
	}

	public void setCheckerlastcmt(String value)
	{
		checkerlastcmt = value;
	}

	public void setCurrappstatus(String value)
	{
		currappstatus = value;
	}

	public void setAdminlastcmt(String value)
	{
		adminlastcmt = value;
	}

	public void setInstitutionid(String value)
	{
		institutionid = value;
	}


	public String toString()
	{
		return "\nid:" + id +
				"\nservicecode:" + servicecode +
				"\nserviceclass:" + serviceclass +
				"\nservicemethod:" + servicemethod +
				"\nintenv:" + intenv +
				"\nrstatus:" + rstatus +
				"\ncreatedby:" + createdby +
				"\ncreatedat:" + createdat +
				"\nmodifiedat:" + modifiedat +
				"\nmodifiedby:" + modifiedby +
				"\nservicetitle:" + servicetitle +
				"\ninstid:" + instid +
				"\nmadeby:" + madeby +
				"\nmadeat:" + madeat +
				"\ncheckedby:" + checkedby +
				"\ncheckedat:" + checkedat +
				"\nmakerlastcmt:" + makerlastcmt +
				"\ncheckerlastcmt:" + checkerlastcmt +
				"\ncurrappstatus:" + currappstatus +
				"\nadminlastcmt:" + adminlastcmt +
				"\ninstitutionid:" + institutionid +
				"\n";
	}

	public void loadContent(LIServicemapRecord inputRecord)
	{
		setId(inputRecord.getId());
		setServicecode(inputRecord.getServicecode());
		setServiceclass(inputRecord.getServiceclass());
		setServicemethod(inputRecord.getServicemethod());
		setIntenv(inputRecord.getIntenv());
		setRstatus(inputRecord.getRstatus());
		setCreatedby(inputRecord.getCreatedby());
		setCreatedat(inputRecord.getCreatedat());
		setModifiedat(inputRecord.getModifiedat());
		setModifiedby(inputRecord.getModifiedby());
		setServicetitle(inputRecord.getServicetitle());
		setInstid(inputRecord.getInstid());
		setMadeby(inputRecord.getMadeby());
		setMadeat(inputRecord.getMadeat());
		setCheckedby(inputRecord.getCheckedby());
		setCheckedat(inputRecord.getCheckedat());
		setMakerlastcmt(inputRecord.getMakerlastcmt());
		setCheckerlastcmt(inputRecord.getCheckerlastcmt());
		setCurrappstatus(inputRecord.getCurrappstatus());
		setAdminlastcmt(inputRecord.getAdminlastcmt());
		setInstitutionid(inputRecord.getInstitutionid());
	}

	public void loadNonNullContent(LIServicemapRecord inputRecord)
	{
		if (StringUtils.hasChanged(getId(), inputRecord.getId()))
		{
			setId(StringUtils.noNull(inputRecord.getId()));
		}
		if (StringUtils.hasChanged(getServicecode(), inputRecord.getServicecode()))
		{
			setServicecode(StringUtils.noNull(inputRecord.getServicecode()));
		}
		if (StringUtils.hasChanged(getServiceclass(), inputRecord.getServiceclass()))
		{
			setServiceclass(StringUtils.noNull(inputRecord.getServiceclass()));
		}
		if (StringUtils.hasChanged(getServicemethod(), inputRecord.getServicemethod()))
		{
			setServicemethod(StringUtils.noNull(inputRecord.getServicemethod()));
		}
		if (StringUtils.hasChanged(getIntenv(), inputRecord.getIntenv()))
		{
			setIntenv(StringUtils.noNull(inputRecord.getIntenv()));
		}
		if (StringUtils.hasChanged(getRstatus(), inputRecord.getRstatus()))
		{
			setRstatus(StringUtils.noNull(inputRecord.getRstatus()));
		}
		if (StringUtils.hasChanged(getCreatedby(), inputRecord.getCreatedby()))
		{
			setCreatedby(StringUtils.noNull(inputRecord.getCreatedby()));
		}
		if (StringUtils.hasChanged(getCreatedat(), inputRecord.getCreatedat()))
		{
			setCreatedat(StringUtils.noNull(inputRecord.getCreatedat()));
		}
		if (StringUtils.hasChanged(getModifiedat(), inputRecord.getModifiedat()))
		{
			setModifiedat(StringUtils.noNull(inputRecord.getModifiedat()));
		}
		if (StringUtils.hasChanged(getModifiedby(), inputRecord.getModifiedby()))
		{
			setModifiedby(StringUtils.noNull(inputRecord.getModifiedby()));
		}
		if (StringUtils.hasChanged(getServicetitle(), inputRecord.getServicetitle()))
		{
			setServicetitle(StringUtils.noNull(inputRecord.getServicetitle()));
		}
		if (StringUtils.hasChanged(getInstid(), inputRecord.getInstid()))
		{
			setInstid(StringUtils.noNull(inputRecord.getInstid()));
		}
		if (StringUtils.hasChanged(getMadeby(), inputRecord.getMadeby()))
		{
			setMadeby(StringUtils.noNull(inputRecord.getMadeby()));
		}
		if (StringUtils.hasChanged(getMadeat(), inputRecord.getMadeat()))
		{
			setMadeat(StringUtils.noNull(inputRecord.getMadeat()));
		}
		if (StringUtils.hasChanged(getCheckedby(), inputRecord.getCheckedby()))
		{
			setCheckedby(StringUtils.noNull(inputRecord.getCheckedby()));
		}
		if (StringUtils.hasChanged(getCheckedat(), inputRecord.getCheckedat()))
		{
			setCheckedat(StringUtils.noNull(inputRecord.getCheckedat()));
		}
		if (StringUtils.hasChanged(getMakerlastcmt(), inputRecord.getMakerlastcmt()))
		{
			setMakerlastcmt(StringUtils.noNull(inputRecord.getMakerlastcmt()));
		}
		if (StringUtils.hasChanged(getCheckerlastcmt(), inputRecord.getCheckerlastcmt()))
		{
			setCheckerlastcmt(StringUtils.noNull(inputRecord.getCheckerlastcmt()));
		}
		if (StringUtils.hasChanged(getCurrappstatus(), inputRecord.getCurrappstatus()))
		{
			setCurrappstatus(StringUtils.noNull(inputRecord.getCurrappstatus()));
		}
		if (StringUtils.hasChanged(getAdminlastcmt(), inputRecord.getAdminlastcmt()))
		{
			setAdminlastcmt(StringUtils.noNull(inputRecord.getAdminlastcmt()));
		}
		if (StringUtils.hasChanged(getInstitutionid(), inputRecord.getInstitutionid()))
		{
			setInstitutionid(StringUtils.noNull(inputRecord.getInstitutionid()));
		}
	}

	public JSONObject getJSONObject()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("servicecode",StringUtils.noNull(servicecode));				
		obj.put("serviceclass",StringUtils.noNull(serviceclass));				
		obj.put("servicemethod",StringUtils.noNull(servicemethod));				
		obj.put("intenv",StringUtils.noNull(intenv));				
		obj.put("rstatus",StringUtils.noNull(rstatus));				
		obj.put("createdby",StringUtils.noNull(createdby));				
		obj.put("createdat",StringUtils.noNull(createdat));				
		obj.put("modifiedat",StringUtils.noNull(modifiedat));				
		obj.put("modifiedby",StringUtils.noNull(modifiedby));				
		obj.put("servicetitle",StringUtils.noNull(servicetitle));				
		obj.put("instid",StringUtils.noNull(instid));				
		obj.put("madeby",StringUtils.noNull(madeby));				
		obj.put("madeat",StringUtils.noNull(madeat));				
		obj.put("checkedby",StringUtils.noNull(checkedby));				
		obj.put("checkedat",StringUtils.noNull(checkedat));				
		obj.put("makerlastcmt",StringUtils.noNull(makerlastcmt));				
		obj.put("checkerlastcmt",StringUtils.noNull(checkerlastcmt));				
		obj.put("currappstatus",StringUtils.noNull(currappstatus));				
		obj.put("adminlastcmt",StringUtils.noNull(adminlastcmt));				
		obj.put("institutionid",StringUtils.noNull(institutionid));
		return obj;
	}

	public void loadJSONObject(JSONObject obj)
	throws Exception
	{
		if (obj == null) return;

		id = StringUtils.getValueFromJSONObject(obj, "id");				
		servicecode = StringUtils.getValueFromJSONObject(obj, "servicecode");				
		serviceclass = StringUtils.getValueFromJSONObject(obj, "serviceclass");				
		servicemethod = StringUtils.getValueFromJSONObject(obj, "servicemethod");				
		intenv = StringUtils.getValueFromJSONObject(obj, "intenv");				
		rstatus = StringUtils.getValueFromJSONObject(obj, "rstatus");				
		createdby = StringUtils.getValueFromJSONObject(obj, "createdby");				
		createdat = StringUtils.getValueFromJSONObject(obj, "createdat");				
		modifiedat = StringUtils.getValueFromJSONObject(obj, "modifiedat");				
		modifiedby = StringUtils.getValueFromJSONObject(obj, "modifiedby");				
		servicetitle = StringUtils.getValueFromJSONObject(obj, "servicetitle");				
		instid = StringUtils.getValueFromJSONObject(obj, "instid");				
		madeby = StringUtils.getValueFromJSONObject(obj, "madeby");				
		madeat = StringUtils.getValueFromJSONObject(obj, "madeat");				
		checkedby = StringUtils.getValueFromJSONObject(obj, "checkedby");				
		checkedat = StringUtils.getValueFromJSONObject(obj, "checkedat");				
		makerlastcmt = StringUtils.getValueFromJSONObject(obj, "makerlastcmt");				
		checkerlastcmt = StringUtils.getValueFromJSONObject(obj, "checkerlastcmt");				
		currappstatus = StringUtils.getValueFromJSONObject(obj, "currappstatus");				
		adminlastcmt = StringUtils.getValueFromJSONObject(obj, "adminlastcmt");				
		institutionid = StringUtils.getValueFromJSONObject(obj, "institutionid");
		return;
	}

	public JSONObject getJSONObjectUI()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("service_code",StringUtils.noNull(servicecode));				
		obj.put("service_class",StringUtils.noNull(serviceclass));				
		obj.put("service_method",StringUtils.noNull(servicemethod));				
		obj.put("int_env",StringUtils.noNull(intenv));				
		obj.put("rstatus",StringUtils.noNull(rstatus));				
		obj.put("created_by",StringUtils.noNull(createdby));				
		obj.put("created_at",StringUtils.noNull(createdat));				
		obj.put("modified_at",StringUtils.noNull(modifiedat));				
		obj.put("modified_by",StringUtils.noNull(modifiedby));				
		obj.put("service_title",StringUtils.noNull(servicetitle));				
		obj.put("inst_id",StringUtils.noNull(instid));				
		obj.put("made_by",StringUtils.noNull(madeby));				
		obj.put("made_at",StringUtils.noNull(madeat));				
		obj.put("checked_by",StringUtils.noNull(checkedby));				
		obj.put("checked_at",StringUtils.noNull(checkedat));				
		obj.put("maker_last_cmt",StringUtils.noNull(makerlastcmt));				
		obj.put("checker_last_cmt",StringUtils.noNull(checkerlastcmt));				
		obj.put("curr_app_status",StringUtils.noNull(currappstatus));				
		obj.put("admin_last_cmt",StringUtils.noNull(adminlastcmt));				
		obj.put("institution_id",StringUtils.noNull(institutionid));
		return obj;
	}

	public void log()
	{
		logger.trace(this.toString());
	}

	public HashMap getTableMap()
	{
		HashMap resultMap = new HashMap();
		ArrayList columnList = new ArrayList();
		resultMap.put("table", "service_map");

		columnList.add("id");				
		columnList.add("service_code");				
		columnList.add("service_class");				
		columnList.add("service_method");				
		columnList.add("int_env");				
		columnList.add("rstatus");				
		columnList.add("created_by");				
		columnList.add("created_at");				
		columnList.add("modified_at");				
		columnList.add("modified_by");				
		columnList.add("service_title");				
		columnList.add("inst_id");				
		columnList.add("made_by");				
		columnList.add("made_at");				
		columnList.add("checked_by");				
		columnList.add("checked_at");				
		columnList.add("maker_last_cmt");				
		columnList.add("checker_last_cmt");				
		columnList.add("curr_app_status");				
		columnList.add("admin_last_cmt");				
		columnList.add("institution_id");
		resultMap.put("ColumnList", columnList);

		return resultMap;
	}

	public void toggleStatus()
	{
		String currentStatus = StringUtils.noNull(getRstatus());
		setRstatus("1");
		if (currentStatus.equals("1"))
		{
			setRstatus("0");
		}
	}

}
