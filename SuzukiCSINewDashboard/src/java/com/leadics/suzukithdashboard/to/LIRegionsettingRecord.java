
/*
 * LIRegionsettingRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.to;
import com.leadics.suzukithdashboard.common.LIRecord;
import com.leadics.utils.*;
import org.json.simple.*;
import java.util.*;
public class LIRegionsettingRecord extends LIRecord
{
	static LogUtils logger = new LogUtils(LIRegionsettingRecord.class.getName());

	private String id;
	private String region;
	private String city;
	private String code;
	private String color;
	private String rstatus;
	private String createdby;
	private String createdat;
	private String modifiedat;
	private String modifiedby;
	private String institutionid;

	public String getId()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(id);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(id);
		}
		else
		{
			return id;
		}
	}

	public String getRegion()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(region);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(region);
		}
		else
		{
			return region;
		}
	}

	public String getCity()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(city);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(city);
		}
		else
		{
			return city;
		}
	}

	public String getCode()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(code);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(code);
		}
		else
		{
			return code;
		}
	}

	public String getColor()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(color);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(color);
		}
		else
		{
			return color;
		}
	}

	public String getRstatus()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(rstatus);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(rstatus);
		}
		else
		{
			return rstatus;
		}
	}

	public String getCreatedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdby);
		}
		else
		{
			return createdby;
		}
	}

	public String getCreatedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdat);
		}
		else
		{
			return createdat;
		}
	}

	public String getModifiedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedat);
		}
		else
		{
			return modifiedat;
		}
	}

	public String getModifiedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedby);
		}
		else
		{
			return modifiedby;
		}
	}

	public String getInstitutionid()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(institutionid);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(institutionid);
		}
		else
		{
			return institutionid;
		}
	}


	public void setId(String value)
	{
		id = value;
	}

	public void setRegion(String value)
	{
		region = value;
	}

	public void setCity(String value)
	{
		city = value;
	}

	public void setCode(String value)
	{
		code = value;
	}

	public void setColor(String value)
	{
		color = value;
	}

	public void setRstatus(String value)
	{
		rstatus = value;
	}

	public void setCreatedby(String value)
	{
		createdby = value;
	}

	public void setCreatedat(String value)
	{
		createdat = value;
	}

	public void setModifiedat(String value)
	{
		modifiedat = value;
	}

	public void setModifiedby(String value)
	{
		modifiedby = value;
	}

	public void setInstitutionid(String value)
	{
		institutionid = value;
	}


	public String toString()
	{
		return "\nid:" + id +
				"\nregion:" + region +
				"\ncity:" + city +
				"\ncode:" + code +
				"\ncolor:" + color +
				"\nrstatus:" + rstatus +
				"\ncreatedby:" + createdby +
				"\ncreatedat:" + createdat +
				"\nmodifiedat:" + modifiedat +
				"\nmodifiedby:" + modifiedby +
				"\ninstitutionid:" + institutionid +
				"\n";
	}

	public void loadContent(LIRegionsettingRecord inputRecord)
	{
		setId(inputRecord.getId());
		setRegion(inputRecord.getRegion());
		setCity(inputRecord.getCity());
		setCode(inputRecord.getCode());
		setColor(inputRecord.getColor());
		setRstatus(inputRecord.getRstatus());
		setCreatedby(inputRecord.getCreatedby());
		setCreatedat(inputRecord.getCreatedat());
		setModifiedat(inputRecord.getModifiedat());
		setModifiedby(inputRecord.getModifiedby());
		setInstitutionid(inputRecord.getInstitutionid());
	}

	public void loadNonNullContent(LIRegionsettingRecord inputRecord)
	{
		if (StringUtils.hasChanged(getId(), inputRecord.getId()))
		{
			setId(StringUtils.noNull(inputRecord.getId()));
		}
		if (StringUtils.hasChanged(getRegion(), inputRecord.getRegion()))
		{
			setRegion(StringUtils.noNull(inputRecord.getRegion()));
		}
		if (StringUtils.hasChanged(getCity(), inputRecord.getCity()))
		{
			setCity(StringUtils.noNull(inputRecord.getCity()));
		}
		if (StringUtils.hasChanged(getCode(), inputRecord.getCode()))
		{
			setCode(StringUtils.noNull(inputRecord.getCode()));
		}
		if (StringUtils.hasChanged(getColor(), inputRecord.getColor()))
		{
			setColor(StringUtils.noNull(inputRecord.getColor()));
		}
		if (StringUtils.hasChanged(getRstatus(), inputRecord.getRstatus()))
		{
			setRstatus(StringUtils.noNull(inputRecord.getRstatus()));
		}
		if (StringUtils.hasChanged(getCreatedby(), inputRecord.getCreatedby()))
		{
			setCreatedby(StringUtils.noNull(inputRecord.getCreatedby()));
		}
		if (StringUtils.hasChanged(getCreatedat(), inputRecord.getCreatedat()))
		{
			setCreatedat(StringUtils.noNull(inputRecord.getCreatedat()));
		}
		if (StringUtils.hasChanged(getModifiedat(), inputRecord.getModifiedat()))
		{
			setModifiedat(StringUtils.noNull(inputRecord.getModifiedat()));
		}
		if (StringUtils.hasChanged(getModifiedby(), inputRecord.getModifiedby()))
		{
			setModifiedby(StringUtils.noNull(inputRecord.getModifiedby()));
		}
		if (StringUtils.hasChanged(getInstitutionid(), inputRecord.getInstitutionid()))
		{
			setInstitutionid(StringUtils.noNull(inputRecord.getInstitutionid()));
		}
	}

	public JSONObject getJSONObject()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("region",StringUtils.noNull(region));				
		obj.put("city",StringUtils.noNull(city));				
		obj.put("code",StringUtils.noNull(code));				
		obj.put("color",StringUtils.noNull(color));				
		obj.put("rstatus",StringUtils.noNull(rstatus));				
		obj.put("createdby",StringUtils.noNull(createdby));				
		obj.put("createdat",StringUtils.noNull(createdat));				
		obj.put("modifiedat",StringUtils.noNull(modifiedat));				
		obj.put("modifiedby",StringUtils.noNull(modifiedby));				
		obj.put("institutionid",StringUtils.noNull(institutionid));
		return obj;
	}

	public void loadJSONObject(JSONObject obj)
	throws Exception
	{
		if (obj == null) return;

		id = StringUtils.getValueFromJSONObject(obj, "id");				
		region = StringUtils.getValueFromJSONObject(obj, "region");				
		city = StringUtils.getValueFromJSONObject(obj, "city");				
		code = StringUtils.getValueFromJSONObject(obj, "code");				
		color = StringUtils.getValueFromJSONObject(obj, "color");				
		rstatus = StringUtils.getValueFromJSONObject(obj, "rstatus");				
		createdby = StringUtils.getValueFromJSONObject(obj, "createdby");				
		createdat = StringUtils.getValueFromJSONObject(obj, "createdat");				
		modifiedat = StringUtils.getValueFromJSONObject(obj, "modifiedat");				
		modifiedby = StringUtils.getValueFromJSONObject(obj, "modifiedby");				
		institutionid = StringUtils.getValueFromJSONObject(obj, "institutionid");
		return;
	}

	public JSONObject getJSONObjectUI()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("region",StringUtils.noNull(region));				
		obj.put("city",StringUtils.noNull(city));				
		obj.put("code",StringUtils.noNull(code));				
		obj.put("color",StringUtils.noNull(color));				
		obj.put("rstatus",StringUtils.noNull(rstatus));				
		obj.put("created_by",StringUtils.noNull(createdby));				
		obj.put("created_at",StringUtils.noNull(createdat));				
		obj.put("modified_at",StringUtils.noNull(modifiedat));				
		obj.put("modified_by",StringUtils.noNull(modifiedby));				
		obj.put("institution_id",StringUtils.noNull(institutionid));
		return obj;
	}

	public void log()
	{
		logger.trace(this.toString());
	}

	public HashMap getTableMap()
	{
		HashMap resultMap = new HashMap();
		ArrayList columnList = new ArrayList();
		resultMap.put("table", "region_setting");

		columnList.add("id");				
		columnList.add("region");				
		columnList.add("city");				
		columnList.add("code");				
		columnList.add("color");				
		columnList.add("rstatus");				
		columnList.add("created_by");				
		columnList.add("created_at");				
		columnList.add("modified_at");				
		columnList.add("modified_by");				
		columnList.add("institution_id");
		resultMap.put("ColumnList", columnList);

		return resultMap;
	}

	public void toggleStatus()
	{
		String currentStatus = StringUtils.noNull(getRstatus());
		setRstatus("1");
		if (currentStatus.equals("1"))
		{
			setRstatus("0");
		}
	}

}
