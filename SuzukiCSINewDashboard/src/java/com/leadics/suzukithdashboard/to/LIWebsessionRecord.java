
/*
 * LIWebsessionRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.to;
import com.leadics.suzukithdashboard.common.LIRecord;
import com.leadics.utils.*;
import org.json.simple.*;
import java.util.*;
public class LIWebsessionRecord extends LIRecord
{
	static LogUtils logger = new LogUtils(LIWebsessionRecord.class.getName());

	private String id;
	private String userid;
	private String sessionid;
	private String source;
	private String rstatus;
	private String lastacc;
	private String createdby;
	private String createdat;
	private String modifiedat;
	private String modifiedby;
	private String closetype;
	private String closedat;

	public String getId()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(id);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(id);
		}
		else
		{
			return id;
		}
	}

	public String getUserid()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(userid);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(userid);
		}
		else
		{
			return userid;
		}
	}

	public String getSessionid()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sessionid);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sessionid);
		}
		else
		{
			return sessionid;
		}
	}

	public String getSource()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(source);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(source);
		}
		else
		{
			return source;
		}
	}

	public String getRstatus()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(rstatus);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(rstatus);
		}
		else
		{
			return rstatus;
		}
	}

	public String getLastacc()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(lastacc);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(lastacc);
		}
		else
		{
			return lastacc;
		}
	}

	public String getCreatedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdby);
		}
		else
		{
			return createdby;
		}
	}

	public String getCreatedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdat);
		}
		else
		{
			return createdat;
		}
	}

	public String getModifiedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedat);
		}
		else
		{
			return modifiedat;
		}
	}

	public String getModifiedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedby);
		}
		else
		{
			return modifiedby;
		}
	}

	public String getClosetype()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(closetype);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(closetype);
		}
		else
		{
			return closetype;
		}
	}

	public String getClosedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(closedat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(closedat);
		}
		else
		{
			return closedat;
		}
	}


	public void setId(String value)
	{
		id = value;
	}

	public void setUserid(String value)
	{
		userid = value;
	}

	public void setSessionid(String value)
	{
		sessionid = value;
	}

	public void setSource(String value)
	{
		source = value;
	}

	public void setRstatus(String value)
	{
		rstatus = value;
	}

	public void setLastacc(String value)
	{
		lastacc = value;
	}

	public void setCreatedby(String value)
	{
		createdby = value;
	}

	public void setCreatedat(String value)
	{
		createdat = value;
	}

	public void setModifiedat(String value)
	{
		modifiedat = value;
	}

	public void setModifiedby(String value)
	{
		modifiedby = value;
	}

	public void setClosetype(String value)
	{
		closetype = value;
	}

	public void setClosedat(String value)
	{
		closedat = value;
	}


	public String toString()
	{
		return "\nid:" + id +
				"\nuserid:" + userid +
				"\nsessionid:" + sessionid +
				"\nsource:" + source +
				"\nrstatus:" + rstatus +
				"\nlastacc:" + lastacc +
				"\ncreatedby:" + createdby +
				"\ncreatedat:" + createdat +
				"\nmodifiedat:" + modifiedat +
				"\nmodifiedby:" + modifiedby +
				"\nclosetype:" + closetype +
				"\nclosedat:" + closedat +
				"\n";
	}

	public void loadContent(LIWebsessionRecord inputRecord)
	{
		setId(inputRecord.getId());
		setUserid(inputRecord.getUserid());
		setSessionid(inputRecord.getSessionid());
		setSource(inputRecord.getSource());
		setRstatus(inputRecord.getRstatus());
		setLastacc(inputRecord.getLastacc());
		setCreatedby(inputRecord.getCreatedby());
		setCreatedat(inputRecord.getCreatedat());
		setModifiedat(inputRecord.getModifiedat());
		setModifiedby(inputRecord.getModifiedby());
		setClosetype(inputRecord.getClosetype());
		setClosedat(inputRecord.getClosedat());
	}

	public void loadNonNullContent(LIWebsessionRecord inputRecord)
	{
		if (StringUtils.hasChanged(getId(), inputRecord.getId()))
		{
			setId(StringUtils.noNull(inputRecord.getId()));
		}
		if (StringUtils.hasChanged(getUserid(), inputRecord.getUserid()))
		{
			setUserid(StringUtils.noNull(inputRecord.getUserid()));
		}
		if (StringUtils.hasChanged(getSessionid(), inputRecord.getSessionid()))
		{
			setSessionid(StringUtils.noNull(inputRecord.getSessionid()));
		}
		if (StringUtils.hasChanged(getSource(), inputRecord.getSource()))
		{
			setSource(StringUtils.noNull(inputRecord.getSource()));
		}
		if (StringUtils.hasChanged(getRstatus(), inputRecord.getRstatus()))
		{
			setRstatus(StringUtils.noNull(inputRecord.getRstatus()));
		}
		if (StringUtils.hasChanged(getLastacc(), inputRecord.getLastacc()))
		{
			setLastacc(StringUtils.noNull(inputRecord.getLastacc()));
		}
		if (StringUtils.hasChanged(getCreatedby(), inputRecord.getCreatedby()))
		{
			setCreatedby(StringUtils.noNull(inputRecord.getCreatedby()));
		}
		if (StringUtils.hasChanged(getCreatedat(), inputRecord.getCreatedat()))
		{
			setCreatedat(StringUtils.noNull(inputRecord.getCreatedat()));
		}
		if (StringUtils.hasChanged(getModifiedat(), inputRecord.getModifiedat()))
		{
			setModifiedat(StringUtils.noNull(inputRecord.getModifiedat()));
		}
		if (StringUtils.hasChanged(getModifiedby(), inputRecord.getModifiedby()))
		{
			setModifiedby(StringUtils.noNull(inputRecord.getModifiedby()));
		}
		if (StringUtils.hasChanged(getClosetype(), inputRecord.getClosetype()))
		{
			setClosetype(StringUtils.noNull(inputRecord.getClosetype()));
		}
		if (StringUtils.hasChanged(getClosedat(), inputRecord.getClosedat()))
		{
			setClosedat(StringUtils.noNull(inputRecord.getClosedat()));
		}
	}

	public JSONObject getJSONObject()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("userid",StringUtils.noNull(userid));				
		obj.put("sessionid",StringUtils.noNull(sessionid));				
		obj.put("source",StringUtils.noNull(source));				
		obj.put("rstatus",StringUtils.noNull(rstatus));				
		obj.put("lastacc",StringUtils.noNull(lastacc));				
		obj.put("createdby",StringUtils.noNull(createdby));				
		obj.put("createdat",StringUtils.noNull(createdat));				
		obj.put("modifiedat",StringUtils.noNull(modifiedat));				
		obj.put("modifiedby",StringUtils.noNull(modifiedby));				
		obj.put("closetype",StringUtils.noNull(closetype));				
		obj.put("closedat",StringUtils.noNull(closedat));
		return obj;
	}

	public void loadJSONObject(JSONObject obj)
	throws Exception
	{
		if (obj == null) return;

		id = StringUtils.getValueFromJSONObject(obj, "id");				
		userid = StringUtils.getValueFromJSONObject(obj, "userid");				
		sessionid = StringUtils.getValueFromJSONObject(obj, "sessionid");				
		source = StringUtils.getValueFromJSONObject(obj, "source");				
		rstatus = StringUtils.getValueFromJSONObject(obj, "rstatus");				
		lastacc = StringUtils.getValueFromJSONObject(obj, "lastacc");				
		createdby = StringUtils.getValueFromJSONObject(obj, "createdby");				
		createdat = StringUtils.getValueFromJSONObject(obj, "createdat");				
		modifiedat = StringUtils.getValueFromJSONObject(obj, "modifiedat");				
		modifiedby = StringUtils.getValueFromJSONObject(obj, "modifiedby");				
		closetype = StringUtils.getValueFromJSONObject(obj, "closetype");				
		closedat = StringUtils.getValueFromJSONObject(obj, "closedat");
		return;
	}

	public JSONObject getJSONObjectUI()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("user_id",StringUtils.noNull(userid));				
		obj.put("sessionid",StringUtils.noNull(sessionid));				
		obj.put("source",StringUtils.noNull(source));				
		obj.put("rstatus",StringUtils.noNull(rstatus));				
		obj.put("lastacc",StringUtils.noNull(lastacc));				
		obj.put("created_by",StringUtils.noNull(createdby));				
		obj.put("created_at",StringUtils.noNull(createdat));				
		obj.put("modified_at",StringUtils.noNull(modifiedat));				
		obj.put("modified_by",StringUtils.noNull(modifiedby));				
		obj.put("close_type",StringUtils.noNull(closetype));				
		obj.put("closed_at",StringUtils.noNull(closedat));
		return obj;
	}

	public void log()
	{
		logger.trace(this.toString());
	}

	public HashMap getTableMap()
	{
		HashMap resultMap = new HashMap();
		ArrayList columnList = new ArrayList();
		resultMap.put("table", "web_session");

		columnList.add("id");				
		columnList.add("user_id");				
		columnList.add("sessionid");				
		columnList.add("source");				
		columnList.add("rstatus");				
		columnList.add("lastacc");				
		columnList.add("created_by");				
		columnList.add("created_at");				
		columnList.add("modified_at");				
		columnList.add("modified_by");				
		columnList.add("close_type");				
		columnList.add("closed_at");
		resultMap.put("ColumnList", columnList);

		return resultMap;
	}

	public void toggleStatus()
	{
		String currentStatus = StringUtils.noNull(getRstatus());
		setRstatus("1");
		if (currentStatus.equals("1"))
		{
			setRstatus("0");
		}
	}

}
