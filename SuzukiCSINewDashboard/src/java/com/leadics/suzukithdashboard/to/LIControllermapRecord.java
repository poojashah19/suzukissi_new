
/*
 * LIControllermapRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.to;
import com.leadics.suzukithdashboard.common.LIRecord;
import com.leadics.utils.*;
import org.json.simple.*;
import java.util.*;
public class LIControllermapRecord extends LIRecord
{
	static LogUtils logger = new LogUtils(LIControllermapRecord.class.getName());

	private String id;
	private String code;
	private String classname;
	private String authflag;

	public String getId()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(id);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(id);
		}
		else
		{
			return id;
		}
	}

	public String getCode()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(code);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(code);
		}
		else
		{
			return code;
		}
	}

	public String getClassname()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(classname);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(classname);
		}
		else
		{
			return classname;
		}
	}

	public String getAuthflag()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(authflag);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(authflag);
		}
		else
		{
			return authflag;
		}
	}


	public void setId(String value)
	{
		id = value;
	}

	public void setCode(String value)
	{
		code = value;
	}

	public void setClassname(String value)
	{
		classname = value;
	}

	public void setAuthflag(String value)
	{
		authflag = value;
	}


	public String toString()
	{
		return "\nid:" + id +
				"\ncode:" + code +
				"\nclassname:" + classname +
				"\nauthflag:" + authflag +
				"\n";
	}

	public void loadContent(LIControllermapRecord inputRecord)
	{
		setId(inputRecord.getId());
		setCode(inputRecord.getCode());
		setClassname(inputRecord.getClassname());
		setAuthflag(inputRecord.getAuthflag());
	}

	public void loadNonNullContent(LIControllermapRecord inputRecord)
	{
		if (StringUtils.hasChanged(getId(), inputRecord.getId()))
		{
			setId(StringUtils.noNull(inputRecord.getId()));
		}
		if (StringUtils.hasChanged(getCode(), inputRecord.getCode()))
		{
			setCode(StringUtils.noNull(inputRecord.getCode()));
		}
		if (StringUtils.hasChanged(getClassname(), inputRecord.getClassname()))
		{
			setClassname(StringUtils.noNull(inputRecord.getClassname()));
		}
		if (StringUtils.hasChanged(getAuthflag(), inputRecord.getAuthflag()))
		{
			setAuthflag(StringUtils.noNull(inputRecord.getAuthflag()));
		}
	}

	public JSONObject getJSONObject()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("code",StringUtils.noNull(code));				
		obj.put("classname",StringUtils.noNull(classname));				
		obj.put("authflag",StringUtils.noNull(authflag));
		return obj;
	}

	public void loadJSONObject(JSONObject obj)
	throws Exception
	{
		if (obj == null) return;

		id = StringUtils.getValueFromJSONObject(obj, "id");				
		code = StringUtils.getValueFromJSONObject(obj, "code");				
		classname = StringUtils.getValueFromJSONObject(obj, "classname");				
		authflag = StringUtils.getValueFromJSONObject(obj, "authflag");
		return;
	}

	public JSONObject getJSONObjectUI()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("code",StringUtils.noNull(code));				
		obj.put("classname",StringUtils.noNull(classname));				
		obj.put("auth_flag",StringUtils.noNull(authflag));
		return obj;
	}

	public void log()
	{
		logger.trace(this.toString());
	}

	public HashMap getTableMap()
	{
		HashMap resultMap = new HashMap();
		ArrayList columnList = new ArrayList();
		resultMap.put("table", "controller_map");

		columnList.add("id");				
		columnList.add("code");				
		columnList.add("classname");				
		columnList.add("auth_flag");
		resultMap.put("ColumnList", columnList);

		return resultMap;
	}

}
