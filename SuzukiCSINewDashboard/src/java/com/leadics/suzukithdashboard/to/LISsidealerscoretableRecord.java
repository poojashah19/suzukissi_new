
/*
 * LISsidealerscoretableRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.to;
import com.leadics.suzukithdashboard.common.LIRecord;
import com.leadics.utils.*;
import org.json.simple.*;
import java.util.*;
public class LISsidealerscoretableRecord extends LIRecord
{
	static LogUtils logger = new LogUtils(LISsidealerscoretableRecord.class.getName());

	private String region;
	private String dealer;
	private String ssi;
	private String ssicolor;
	private String salesinitiation;
	private String salesinitiationcolor;
	private String dealerfacility;
	private String dealerfacilitycolor;
	private String deal;
	private String dealcolor;
	private String salesperson;
	private String salespersoncolor;
	private String deliverytiming;
	private String deliverytimingcolor;
	private String deliveryprocess;
	private String deliveryprocesscolor;
	private String sop;
	private String sopcolor;

	public String getRegion()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(region);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(region);
		}
		else
		{
			return region;
		}
	}

	public String getDealer()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dealer);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dealer);
		}
		else
		{
			return dealer;
		}
	}

	public String getSsi()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ssi);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ssi);
		}
		else
		{
			return ssi;
		}
	}

	public String getSsicolor()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(ssicolor);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(ssicolor);
		}
		else
		{
			return ssicolor;
		}
	}

	public String getSalesinitiation()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(salesinitiation);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(salesinitiation);
		}
		else
		{
			return salesinitiation;
		}
	}

	public String getSalesinitiationcolor()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(salesinitiationcolor);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(salesinitiationcolor);
		}
		else
		{
			return salesinitiationcolor;
		}
	}

	public String getDealerfacility()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dealerfacility);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dealerfacility);
		}
		else
		{
			return dealerfacility;
		}
	}

	public String getDealerfacilitycolor()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dealerfacilitycolor);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dealerfacilitycolor);
		}
		else
		{
			return dealerfacilitycolor;
		}
	}

	public String getDeal()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(deal);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(deal);
		}
		else
		{
			return deal;
		}
	}

	public String getDealcolor()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dealcolor);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dealcolor);
		}
		else
		{
			return dealcolor;
		}
	}

	public String getSalesperson()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(salesperson);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(salesperson);
		}
		else
		{
			return salesperson;
		}
	}

	public String getSalespersoncolor()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(salespersoncolor);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(salespersoncolor);
		}
		else
		{
			return salespersoncolor;
		}
	}

	public String getDeliverytiming()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(deliverytiming);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(deliverytiming);
		}
		else
		{
			return deliverytiming;
		}
	}

	public String getDeliverytimingcolor()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(deliverytimingcolor);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(deliverytimingcolor);
		}
		else
		{
			return deliverytimingcolor;
		}
	}

	public String getDeliveryprocess()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(deliveryprocess);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(deliveryprocess);
		}
		else
		{
			return deliveryprocess;
		}
	}

	public String getDeliveryprocesscolor()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(deliveryprocesscolor);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(deliveryprocesscolor);
		}
		else
		{
			return deliveryprocesscolor;
		}
	}

	public String getSop()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sop);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sop);
		}
		else
		{
			return sop;
		}
	}

	public String getSopcolor()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(sopcolor);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(sopcolor);
		}
		else
		{
			return sopcolor;
		}
	}


	public void setRegion(String value)
	{
		region = value;
	}

	public void setDealer(String value)
	{
		dealer = value;
	}

	public void setSsi(String value)
	{
		ssi = value;
	}

	public void setSsicolor(String value)
	{
		ssicolor = value;
	}

	public void setSalesinitiation(String value)
	{
		salesinitiation = value;
	}

	public void setSalesinitiationcolor(String value)
	{
		salesinitiationcolor = value;
	}

	public void setDealerfacility(String value)
	{
		dealerfacility = value;
	}

	public void setDealerfacilitycolor(String value)
	{
		dealerfacilitycolor = value;
	}

	public void setDeal(String value)
	{
		deal = value;
	}

	public void setDealcolor(String value)
	{
		dealcolor = value;
	}

	public void setSalesperson(String value)
	{
		salesperson = value;
	}

	public void setSalespersoncolor(String value)
	{
		salespersoncolor = value;
	}

	public void setDeliverytiming(String value)
	{
		deliverytiming = value;
	}

	public void setDeliverytimingcolor(String value)
	{
		deliverytimingcolor = value;
	}

	public void setDeliveryprocess(String value)
	{
		deliveryprocess = value;
	}

	public void setDeliveryprocesscolor(String value)
	{
		deliveryprocesscolor = value;
	}

	public void setSop(String value)
	{
		sop = value;
	}

	public void setSopcolor(String value)
	{
		sopcolor = value;
	}


	public String toString()
	{
		return "\nregion:" + region +
				"\ndealer:" + dealer +
				"\nssi:" + ssi +
				"\nssicolor:" + ssicolor +
				"\nsalesinitiation:" + salesinitiation +
				"\nsalesinitiationcolor:" + salesinitiationcolor +
				"\ndealerfacility:" + dealerfacility +
				"\ndealerfacilitycolor:" + dealerfacilitycolor +
				"\ndeal:" + deal +
				"\ndealcolor:" + dealcolor +
				"\nsalesperson:" + salesperson +
				"\nsalespersoncolor:" + salespersoncolor +
				"\ndeliverytiming:" + deliverytiming +
				"\ndeliverytimingcolor:" + deliverytimingcolor +
				"\ndeliveryprocess:" + deliveryprocess +
				"\ndeliveryprocesscolor:" + deliveryprocesscolor +
				"\nsop:" + sop +
				"\nsopcolor:" + sopcolor +
				"\n";
	}

	public void loadContent(LISsidealerscoretableRecord inputRecord)
	{
		setRegion(inputRecord.getRegion());
		setDealer(inputRecord.getDealer());
		setSsi(inputRecord.getSsi());
		setSsicolor(inputRecord.getSsicolor());
		setSalesinitiation(inputRecord.getSalesinitiation());
		setSalesinitiationcolor(inputRecord.getSalesinitiationcolor());
		setDealerfacility(inputRecord.getDealerfacility());
		setDealerfacilitycolor(inputRecord.getDealerfacilitycolor());
		setDeal(inputRecord.getDeal());
		setDealcolor(inputRecord.getDealcolor());
		setSalesperson(inputRecord.getSalesperson());
		setSalespersoncolor(inputRecord.getSalespersoncolor());
		setDeliverytiming(inputRecord.getDeliverytiming());
		setDeliverytimingcolor(inputRecord.getDeliverytimingcolor());
		setDeliveryprocess(inputRecord.getDeliveryprocess());
		setDeliveryprocesscolor(inputRecord.getDeliveryprocesscolor());
		setSop(inputRecord.getSop());
		setSopcolor(inputRecord.getSopcolor());
	}

	public void loadNonNullContent(LISsidealerscoretableRecord inputRecord)
	{
		if (StringUtils.hasChanged(getRegion(), inputRecord.getRegion()))
		{
			setRegion(StringUtils.noNull(inputRecord.getRegion()));
		}
		if (StringUtils.hasChanged(getDealer(), inputRecord.getDealer()))
		{
			setDealer(StringUtils.noNull(inputRecord.getDealer()));
		}
		if (StringUtils.hasChanged(getSsi(), inputRecord.getSsi()))
		{
			setSsi(StringUtils.noNull(inputRecord.getSsi()));
		}
		if (StringUtils.hasChanged(getSsicolor(), inputRecord.getSsicolor()))
		{
			setSsicolor(StringUtils.noNull(inputRecord.getSsicolor()));
		}
		if (StringUtils.hasChanged(getSalesinitiation(), inputRecord.getSalesinitiation()))
		{
			setSalesinitiation(StringUtils.noNull(inputRecord.getSalesinitiation()));
		}
		if (StringUtils.hasChanged(getSalesinitiationcolor(), inputRecord.getSalesinitiationcolor()))
		{
			setSalesinitiationcolor(StringUtils.noNull(inputRecord.getSalesinitiationcolor()));
		}
		if (StringUtils.hasChanged(getDealerfacility(), inputRecord.getDealerfacility()))
		{
			setDealerfacility(StringUtils.noNull(inputRecord.getDealerfacility()));
		}
		if (StringUtils.hasChanged(getDealerfacilitycolor(), inputRecord.getDealerfacilitycolor()))
		{
			setDealerfacilitycolor(StringUtils.noNull(inputRecord.getDealerfacilitycolor()));
		}
		if (StringUtils.hasChanged(getDeal(), inputRecord.getDeal()))
		{
			setDeal(StringUtils.noNull(inputRecord.getDeal()));
		}
		if (StringUtils.hasChanged(getDealcolor(), inputRecord.getDealcolor()))
		{
			setDealcolor(StringUtils.noNull(inputRecord.getDealcolor()));
		}
		if (StringUtils.hasChanged(getSalesperson(), inputRecord.getSalesperson()))
		{
			setSalesperson(StringUtils.noNull(inputRecord.getSalesperson()));
		}
		if (StringUtils.hasChanged(getSalespersoncolor(), inputRecord.getSalespersoncolor()))
		{
			setSalespersoncolor(StringUtils.noNull(inputRecord.getSalespersoncolor()));
		}
		if (StringUtils.hasChanged(getDeliverytiming(), inputRecord.getDeliverytiming()))
		{
			setDeliverytiming(StringUtils.noNull(inputRecord.getDeliverytiming()));
		}
		if (StringUtils.hasChanged(getDeliverytimingcolor(), inputRecord.getDeliverytimingcolor()))
		{
			setDeliverytimingcolor(StringUtils.noNull(inputRecord.getDeliverytimingcolor()));
		}
		if (StringUtils.hasChanged(getDeliveryprocess(), inputRecord.getDeliveryprocess()))
		{
			setDeliveryprocess(StringUtils.noNull(inputRecord.getDeliveryprocess()));
		}
		if (StringUtils.hasChanged(getDeliveryprocesscolor(), inputRecord.getDeliveryprocesscolor()))
		{
			setDeliveryprocesscolor(StringUtils.noNull(inputRecord.getDeliveryprocesscolor()));
		}
		if (StringUtils.hasChanged(getSop(), inputRecord.getSop()))
		{
			setSop(StringUtils.noNull(inputRecord.getSop()));
		}
		if (StringUtils.hasChanged(getSopcolor(), inputRecord.getSopcolor()))
		{
			setSopcolor(StringUtils.noNull(inputRecord.getSopcolor()));
		}
	}

	public JSONObject getJSONObject()
	{
		JSONObject obj = new JSONObject();

		obj.put("region",StringUtils.noNull(region));				
		obj.put("dealer",StringUtils.noNull(dealer));				
		obj.put("ssi",StringUtils.noNull(ssi));				
		obj.put("ssicolor",StringUtils.noNull(ssicolor));				
		obj.put("salesinitiation",StringUtils.noNull(salesinitiation));				
		obj.put("salesinitiationcolor",StringUtils.noNull(salesinitiationcolor));				
		obj.put("dealerfacility",StringUtils.noNull(dealerfacility));				
		obj.put("dealerfacilitycolor",StringUtils.noNull(dealerfacilitycolor));				
		obj.put("deal",StringUtils.noNull(deal));				
		obj.put("dealcolor",StringUtils.noNull(dealcolor));				
		obj.put("salesperson",StringUtils.noNull(salesperson));				
		obj.put("salespersoncolor",StringUtils.noNull(salespersoncolor));				
		obj.put("deliverytiming",StringUtils.noNull(deliverytiming));				
		obj.put("deliverytimingcolor",StringUtils.noNull(deliverytimingcolor));				
		obj.put("deliveryprocess",StringUtils.noNull(deliveryprocess));				
		obj.put("deliveryprocesscolor",StringUtils.noNull(deliveryprocesscolor));				
		obj.put("sop",StringUtils.noNull(sop));				
		obj.put("sopcolor",StringUtils.noNull(sopcolor));
		return obj;
	}

	public void loadJSONObject(JSONObject obj)
	throws Exception
	{
		if (obj == null) return;

		region = StringUtils.getValueFromJSONObject(obj, "region");				
		dealer = StringUtils.getValueFromJSONObject(obj, "dealer");				
		ssi = StringUtils.getValueFromJSONObject(obj, "ssi");				
		ssicolor = StringUtils.getValueFromJSONObject(obj, "ssicolor");				
		salesinitiation = StringUtils.getValueFromJSONObject(obj, "salesinitiation");				
		salesinitiationcolor = StringUtils.getValueFromJSONObject(obj, "salesinitiationcolor");				
		dealerfacility = StringUtils.getValueFromJSONObject(obj, "dealerfacility");				
		dealerfacilitycolor = StringUtils.getValueFromJSONObject(obj, "dealerfacilitycolor");				
		deal = StringUtils.getValueFromJSONObject(obj, "deal");				
		dealcolor = StringUtils.getValueFromJSONObject(obj, "dealcolor");				
		salesperson = StringUtils.getValueFromJSONObject(obj, "salesperson");				
		salespersoncolor = StringUtils.getValueFromJSONObject(obj, "salespersoncolor");				
		deliverytiming = StringUtils.getValueFromJSONObject(obj, "deliverytiming");				
		deliverytimingcolor = StringUtils.getValueFromJSONObject(obj, "deliverytimingcolor");				
		deliveryprocess = StringUtils.getValueFromJSONObject(obj, "deliveryprocess");				
		deliveryprocesscolor = StringUtils.getValueFromJSONObject(obj, "deliveryprocesscolor");				
		sop = StringUtils.getValueFromJSONObject(obj, "sop");				
		sopcolor = StringUtils.getValueFromJSONObject(obj, "sopcolor");
		return;
	}

	public JSONObject getJSONObjectUI()
	{
		JSONObject obj = new JSONObject();

		obj.put("region",StringUtils.noNull(region));				
		obj.put("dealer",StringUtils.noNull(dealer));				
		obj.put("ssi",StringUtils.noNull(ssi));				
		obj.put("ssicolor",StringUtils.noNull(ssicolor));				
		obj.put("salesinitiation",StringUtils.noNull(salesinitiation));				
		obj.put("salesinitiationcolor",StringUtils.noNull(salesinitiationcolor));				
		obj.put("dealerfacility",StringUtils.noNull(dealerfacility));				
		obj.put("dealerfacilitycolor",StringUtils.noNull(dealerfacilitycolor));				
		obj.put("deal",StringUtils.noNull(deal));				
		obj.put("dealcolor",StringUtils.noNull(dealcolor));				
		obj.put("salesperson",StringUtils.noNull(salesperson));				
		obj.put("salespersoncolor",StringUtils.noNull(salespersoncolor));				
		obj.put("deliverytiming",StringUtils.noNull(deliverytiming));				
		obj.put("deliverytimingcolor",StringUtils.noNull(deliverytimingcolor));				
		obj.put("deliveryprocess",StringUtils.noNull(deliveryprocess));				
		obj.put("deliveryprocesscolor",StringUtils.noNull(deliveryprocesscolor));				
		obj.put("sop",StringUtils.noNull(sop));				
		obj.put("sopcolor",StringUtils.noNull(sopcolor));
		return obj;
	}

	public void log()
	{
		logger.trace(this.toString());
	}

	public HashMap getTableMap()
	{
		HashMap resultMap = new HashMap();
		ArrayList columnList = new ArrayList();
		resultMap.put("table", "ssi_dealerscoretable");

		columnList.add("region");				
		columnList.add("dealer");				
		columnList.add("ssi");				
		columnList.add("ssicolor");				
		columnList.add("salesinitiation");				
		columnList.add("salesinitiationcolor");				
		columnList.add("dealerfacility");				
		columnList.add("dealerfacilitycolor");				
		columnList.add("deal");				
		columnList.add("dealcolor");				
		columnList.add("salesperson");				
		columnList.add("salespersoncolor");				
		columnList.add("deliverytiming");				
		columnList.add("deliverytimingcolor");				
		columnList.add("deliveryprocess");				
		columnList.add("deliveryprocesscolor");				
		columnList.add("sop");				
		columnList.add("sopcolor");
		resultMap.put("ColumnList", columnList);

		return resultMap;
	}

}
