
/*
 * LISystemuserRecord.java
 *
 * Copyright (c) Leadics 
 *
 *
 * This software is the confidential and proprietary information of 
 * Leadics  ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Leadics 
 *
 * Project Name             : Hundai Motors SR
 * Module                   : Hundai Motors SR
 * Author                   : Varma, Leadics
 * Date                     : Nov 24, 2015
 * Change Revision
 * ----------------------------------------------------------------
 * Date            Author         Version#    Remarks/Description
 *-----------------------------------------------------------------
 *
 */
package com.leadics.suzukithdashboard.to;
import com.leadics.suzukithdashboard.common.LIRecord;
import com.leadics.utils.*;
import org.json.simple.*;
import java.util.*;
public class LISystemuserRecord extends LIRecord
{
	static LogUtils logger = new LogUtils(LISystemuserRecord.class.getName());

	private String id;
	private String userid;
	private String userpassword;
	private String prefix;
	private String firstname;
	private String middlename;
	private String lastname;
	private String email;
	private String mobile;
	private String institutionid;
	private String country;
	private String lastlogindate;
	private String lastloginfdate;
	private String loginattempts;
	private String preflang;
	private String prefcomm;
	private String userroleid;
	private String supid;
	private String seqq1;
	private String seqq2;
	private String seqq3;
	private String seqa1;
	private String seqa2;
	private String seqa3;
	private String madeby;
	private String madeat;
	private String checkedby;
	private String checkedat;
	private String makerlastcmt;
	private String checkerlastcmt;
	private String currappstatus;
	private String adminlastcmt;
	private String rstatus;
	private String createdby;
	private String createdat;
	private String modifiedat;
	private String modifiedby;
	private String blockedflag;
	private String pwdsetdate;
	private String initialpwd;
	private String groupcode;
	private String otpgen;
	private String otpgenat;
	private String address;
	private String city;
	private String dregion;
	private String dgroup;

	public String getId()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(id);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(id);
		}
		else
		{
			return id;
		}
	}

	public String getUserid()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(userid);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(userid);
		}
		else
		{
			return userid;
		}
	}

	public String getUserpassword()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(userpassword);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(userpassword);
		}
		else
		{
			return userpassword;
		}
	}

	public String getPrefix()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(prefix);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(prefix);
		}
		else
		{
			return prefix;
		}
	}

	public String getFirstname()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(firstname);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(firstname);
		}
		else
		{
			return firstname;
		}
	}

	public String getMiddlename()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(middlename);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(middlename);
		}
		else
		{
			return middlename;
		}
	}

	public String getLastname()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(lastname);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(lastname);
		}
		else
		{
			return lastname;
		}
	}

	public String getEmail()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(email);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(email);
		}
		else
		{
			return email;
		}
	}

	public String getMobile()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(mobile);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(mobile);
		}
		else
		{
			return mobile;
		}
	}

	public String getInstitutionid()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(institutionid);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(institutionid);
		}
		else
		{
			return institutionid;
		}
	}

	public String getCountry()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(country);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(country);
		}
		else
		{
			return country;
		}
	}

	public String getLastlogindate()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(lastlogindate);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(lastlogindate);
		}
		else
		{
			return lastlogindate;
		}
	}

	public String getLastloginfdate()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(lastloginfdate);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(lastloginfdate);
		}
		else
		{
			return lastloginfdate;
		}
	}

	public String getLoginattempts()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(loginattempts);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(loginattempts);
		}
		else
		{
			return loginattempts;
		}
	}

	public String getPreflang()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(preflang);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(preflang);
		}
		else
		{
			return preflang;
		}
	}

	public String getPrefcomm()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(prefcomm);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(prefcomm);
		}
		else
		{
			return prefcomm;
		}
	}

	public String getUserroleid()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(userroleid);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(userroleid);
		}
		else
		{
			return userroleid;
		}
	}

	public String getSupid()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(supid);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(supid);
		}
		else
		{
			return supid;
		}
	}

	public String getSeqq1()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(seqq1);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(seqq1);
		}
		else
		{
			return seqq1;
		}
	}

	public String getSeqq2()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(seqq2);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(seqq2);
		}
		else
		{
			return seqq2;
		}
	}

	public String getSeqq3()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(seqq3);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(seqq3);
		}
		else
		{
			return seqq3;
		}
	}

	public String getSeqa1()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(seqa1);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(seqa1);
		}
		else
		{
			return seqa1;
		}
	}

	public String getSeqa2()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(seqa2);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(seqa2);
		}
		else
		{
			return seqa2;
		}
	}

	public String getSeqa3()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(seqa3);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(seqa3);
		}
		else
		{
			return seqa3;
		}
	}

	public String getMadeby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(madeby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(madeby);
		}
		else
		{
			return madeby;
		}
	}

	public String getMadeat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(madeat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(madeat);
		}
		else
		{
			return madeat;
		}
	}

	public String getCheckedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(checkedby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(checkedby);
		}
		else
		{
			return checkedby;
		}
	}

	public String getCheckedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(checkedat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(checkedat);
		}
		else
		{
			return checkedat;
		}
	}

	public String getMakerlastcmt()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(makerlastcmt);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(makerlastcmt);
		}
		else
		{
			return makerlastcmt;
		}
	}

	public String getCheckerlastcmt()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(checkerlastcmt);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(checkerlastcmt);
		}
		else
		{
			return checkerlastcmt;
		}
	}

	public String getCurrappstatus()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(currappstatus);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(currappstatus);
		}
		else
		{
			return currappstatus;
		}
	}

	public String getAdminlastcmt()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(adminlastcmt);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(adminlastcmt);
		}
		else
		{
			return adminlastcmt;
		}
	}

	public String getRstatus()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(rstatus);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(rstatus);
		}
		else
		{
			return rstatus;
		}
	}

	public String getCreatedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdby);
		}
		else
		{
			return createdby;
		}
	}

	public String getCreatedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(createdat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(createdat);
		}
		else
		{
			return createdat;
		}
	}

	public String getModifiedat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedat);
		}
		else
		{
			return modifiedat;
		}
	}

	public String getModifiedby()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(modifiedby);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(modifiedby);
		}
		else
		{
			return modifiedby;
		}
	}

	public String getBlockedflag()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(blockedflag);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(blockedflag);
		}
		else
		{
			return blockedflag;
		}
	}

	public String getPwdsetdate()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(pwdsetdate);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(pwdsetdate);
		}
		else
		{
			return pwdsetdate;
		}
	}

	public String getInitialpwd()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(initialpwd);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(initialpwd);
		}
		else
		{
			return initialpwd;
		}
	}

	public String getGroupcode()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(groupcode);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(groupcode);
		}
		else
		{
			return groupcode;
		}
	}

	public String getOtpgen()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(otpgen);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(otpgen);
		}
		else
		{
			return otpgen;
		}
	}

	public String getOtpgenat()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(otpgenat);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(otpgenat);
		}
		else
		{
			return otpgenat;
		}
	}

	public String getAddress()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(address);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(address);
		}
		else
		{
			return address;
		}
	}

	public String getCity()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(city);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(city);
		}
		else
		{
			return city;
		}
	}

	public String getDregion()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dregion);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dregion);
		}
		else
		{
			return dregion;
		}
	}

	public String getDgroup()
	{
		if (isNoNullForHTMLEnabled())
		{
			return StringUtils.noNullForHTML(dgroup);
		}
		else if (isNoNullEnabled())
		{
			return StringUtils.noNull(dgroup);
		}
		else
		{
			return dgroup;
		}
	}


	public void setId(String value)
	{
		id = value;
	}

	public void setUserid(String value)
	{
		userid = value;
	}

	public void setUserpassword(String value)
	{
		userpassword = value;
	}

	public void setPrefix(String value)
	{
		prefix = value;
	}

	public void setFirstname(String value)
	{
		firstname = value;
	}

	public void setMiddlename(String value)
	{
		middlename = value;
	}

	public void setLastname(String value)
	{
		lastname = value;
	}

	public void setEmail(String value)
	{
		email = value;
	}

	public void setMobile(String value)
	{
		mobile = value;
	}

	public void setInstitutionid(String value)
	{
		institutionid = value;
	}

	public void setCountry(String value)
	{
		country = value;
	}

	public void setLastlogindate(String value)
	{
		lastlogindate = value;
	}

	public void setLastloginfdate(String value)
	{
		lastloginfdate = value;
	}

	public void setLoginattempts(String value)
	{
		loginattempts = value;
	}

	public void setPreflang(String value)
	{
		preflang = value;
	}

	public void setPrefcomm(String value)
	{
		prefcomm = value;
	}

	public void setUserroleid(String value)
	{
		userroleid = value;
	}

	public void setSupid(String value)
	{
		supid = value;
	}

	public void setSeqq1(String value)
	{
		seqq1 = value;
	}

	public void setSeqq2(String value)
	{
		seqq2 = value;
	}

	public void setSeqq3(String value)
	{
		seqq3 = value;
	}

	public void setSeqa1(String value)
	{
		seqa1 = value;
	}

	public void setSeqa2(String value)
	{
		seqa2 = value;
	}

	public void setSeqa3(String value)
	{
		seqa3 = value;
	}

	public void setMadeby(String value)
	{
		madeby = value;
	}

	public void setMadeat(String value)
	{
		madeat = value;
	}

	public void setCheckedby(String value)
	{
		checkedby = value;
	}

	public void setCheckedat(String value)
	{
		checkedat = value;
	}

	public void setMakerlastcmt(String value)
	{
		makerlastcmt = value;
	}

	public void setCheckerlastcmt(String value)
	{
		checkerlastcmt = value;
	}

	public void setCurrappstatus(String value)
	{
		currappstatus = value;
	}

	public void setAdminlastcmt(String value)
	{
		adminlastcmt = value;
	}

	public void setRstatus(String value)
	{
		rstatus = value;
	}

	public void setCreatedby(String value)
	{
		createdby = value;
	}

	public void setCreatedat(String value)
	{
		createdat = value;
	}

	public void setModifiedat(String value)
	{
		modifiedat = value;
	}

	public void setModifiedby(String value)
	{
		modifiedby = value;
	}

	public void setBlockedflag(String value)
	{
		blockedflag = value;
	}

	public void setPwdsetdate(String value)
	{
		pwdsetdate = value;
	}

	public void setInitialpwd(String value)
	{
		initialpwd = value;
	}

	public void setGroupcode(String value)
	{
		groupcode = value;
	}

	public void setOtpgen(String value)
	{
		otpgen = value;
	}

	public void setOtpgenat(String value)
	{
		otpgenat = value;
	}

	public void setAddress(String value)
	{
		address = value;
	}

	public void setCity(String value)
	{
		city = value;
	}

	public void setDregion(String value)
	{
		dregion = value;
	}

	public void setDgroup(String value)
	{
		dgroup = value;
	}


	public String toString()
	{
		return "\nid:" + id +
				"\nuserid:" + userid +
				"\nuserpassword:" + userpassword +
				"\nprefix:" + prefix +
				"\nfirstname:" + firstname +
				"\nmiddlename:" + middlename +
				"\nlastname:" + lastname +
				"\nemail:" + email +
				"\nmobile:" + mobile +
				"\ninstitutionid:" + institutionid +
				"\ncountry:" + country +
				"\nlastlogindate:" + lastlogindate +
				"\nlastloginfdate:" + lastloginfdate +
				"\nloginattempts:" + loginattempts +
				"\npreflang:" + preflang +
				"\nprefcomm:" + prefcomm +
				"\nuserroleid:" + userroleid +
				"\nsupid:" + supid +
				"\nseqq1:" + seqq1 +
				"\nseqq2:" + seqq2 +
				"\nseqq3:" + seqq3 +
				"\nseqa1:" + seqa1 +
				"\nseqa2:" + seqa2 +
				"\nseqa3:" + seqa3 +
				"\nmadeby:" + madeby +
				"\nmadeat:" + madeat +
				"\ncheckedby:" + checkedby +
				"\ncheckedat:" + checkedat +
				"\nmakerlastcmt:" + makerlastcmt +
				"\ncheckerlastcmt:" + checkerlastcmt +
				"\ncurrappstatus:" + currappstatus +
				"\nadminlastcmt:" + adminlastcmt +
				"\nrstatus:" + rstatus +
				"\ncreatedby:" + createdby +
				"\ncreatedat:" + createdat +
				"\nmodifiedat:" + modifiedat +
				"\nmodifiedby:" + modifiedby +
				"\nblockedflag:" + blockedflag +
				"\npwdsetdate:" + pwdsetdate +
				"\ninitialpwd:" + initialpwd +
				"\ngroupcode:" + groupcode +
				"\notpgen:" + otpgen +
				"\notpgenat:" + otpgenat +
				"\naddress:" + address +
				"\ncity:" + city +
				"\ndregion:" + dregion +
				"\ndgroup:" + dgroup +
				"\n";
	}

	public void loadContent(LISystemuserRecord inputRecord)
	{
		setId(inputRecord.getId());
		setUserid(inputRecord.getUserid());
		setUserpassword(inputRecord.getUserpassword());
		setPrefix(inputRecord.getPrefix());
		setFirstname(inputRecord.getFirstname());
		setMiddlename(inputRecord.getMiddlename());
		setLastname(inputRecord.getLastname());
		setEmail(inputRecord.getEmail());
		setMobile(inputRecord.getMobile());
		setInstitutionid(inputRecord.getInstitutionid());
		setCountry(inputRecord.getCountry());
		setLastlogindate(inputRecord.getLastlogindate());
		setLastloginfdate(inputRecord.getLastloginfdate());
		setLoginattempts(inputRecord.getLoginattempts());
		setPreflang(inputRecord.getPreflang());
		setPrefcomm(inputRecord.getPrefcomm());
		setUserroleid(inputRecord.getUserroleid());
		setSupid(inputRecord.getSupid());
		setSeqq1(inputRecord.getSeqq1());
		setSeqq2(inputRecord.getSeqq2());
		setSeqq3(inputRecord.getSeqq3());
		setSeqa1(inputRecord.getSeqa1());
		setSeqa2(inputRecord.getSeqa2());
		setSeqa3(inputRecord.getSeqa3());
		setMadeby(inputRecord.getMadeby());
		setMadeat(inputRecord.getMadeat());
		setCheckedby(inputRecord.getCheckedby());
		setCheckedat(inputRecord.getCheckedat());
		setMakerlastcmt(inputRecord.getMakerlastcmt());
		setCheckerlastcmt(inputRecord.getCheckerlastcmt());
		setCurrappstatus(inputRecord.getCurrappstatus());
		setAdminlastcmt(inputRecord.getAdminlastcmt());
		setRstatus(inputRecord.getRstatus());
		setCreatedby(inputRecord.getCreatedby());
		setCreatedat(inputRecord.getCreatedat());
		setModifiedat(inputRecord.getModifiedat());
		setModifiedby(inputRecord.getModifiedby());
		setBlockedflag(inputRecord.getBlockedflag());
		setPwdsetdate(inputRecord.getPwdsetdate());
		setInitialpwd(inputRecord.getInitialpwd());
		setGroupcode(inputRecord.getGroupcode());
		setOtpgen(inputRecord.getOtpgen());
		setOtpgenat(inputRecord.getOtpgenat());
		setAddress(inputRecord.getAddress());
		setCity(inputRecord.getCity());
		setDregion(inputRecord.getDregion());
		setDgroup(inputRecord.getDgroup());
	}

	public void loadNonNullContent(LISystemuserRecord inputRecord)
	{
		if (StringUtils.hasChanged(getId(), inputRecord.getId()))
		{
			setId(StringUtils.noNull(inputRecord.getId()));
		}
		if (StringUtils.hasChanged(getUserid(), inputRecord.getUserid()))
		{
			setUserid(StringUtils.noNull(inputRecord.getUserid()));
		}
		if (StringUtils.hasChanged(getUserpassword(), inputRecord.getUserpassword()))
		{
			setUserpassword(StringUtils.noNull(inputRecord.getUserpassword()));
		}
		if (StringUtils.hasChanged(getPrefix(), inputRecord.getPrefix()))
		{
			setPrefix(StringUtils.noNull(inputRecord.getPrefix()));
		}
		if (StringUtils.hasChanged(getFirstname(), inputRecord.getFirstname()))
		{
			setFirstname(StringUtils.noNull(inputRecord.getFirstname()));
		}
		if (StringUtils.hasChanged(getMiddlename(), inputRecord.getMiddlename()))
		{
			setMiddlename(StringUtils.noNull(inputRecord.getMiddlename()));
		}
		if (StringUtils.hasChanged(getLastname(), inputRecord.getLastname()))
		{
			setLastname(StringUtils.noNull(inputRecord.getLastname()));
		}
		if (StringUtils.hasChanged(getEmail(), inputRecord.getEmail()))
		{
			setEmail(StringUtils.noNull(inputRecord.getEmail()));
		}
		if (StringUtils.hasChanged(getMobile(), inputRecord.getMobile()))
		{
			setMobile(StringUtils.noNull(inputRecord.getMobile()));
		}
		if (StringUtils.hasChanged(getInstitutionid(), inputRecord.getInstitutionid()))
		{
			setInstitutionid(StringUtils.noNull(inputRecord.getInstitutionid()));
		}
		if (StringUtils.hasChanged(getCountry(), inputRecord.getCountry()))
		{
			setCountry(StringUtils.noNull(inputRecord.getCountry()));
		}
		if (StringUtils.hasChanged(getLastlogindate(), inputRecord.getLastlogindate()))
		{
			setLastlogindate(StringUtils.noNull(inputRecord.getLastlogindate()));
		}
		if (StringUtils.hasChanged(getLastloginfdate(), inputRecord.getLastloginfdate()))
		{
			setLastloginfdate(StringUtils.noNull(inputRecord.getLastloginfdate()));
		}
		if (StringUtils.hasChanged(getLoginattempts(), inputRecord.getLoginattempts()))
		{
			setLoginattempts(StringUtils.noNull(inputRecord.getLoginattempts()));
		}
		if (StringUtils.hasChanged(getPreflang(), inputRecord.getPreflang()))
		{
			setPreflang(StringUtils.noNull(inputRecord.getPreflang()));
		}
		if (StringUtils.hasChanged(getPrefcomm(), inputRecord.getPrefcomm()))
		{
			setPrefcomm(StringUtils.noNull(inputRecord.getPrefcomm()));
		}
		if (StringUtils.hasChanged(getUserroleid(), inputRecord.getUserroleid()))
		{
			setUserroleid(StringUtils.noNull(inputRecord.getUserroleid()));
		}
		if (StringUtils.hasChanged(getSupid(), inputRecord.getSupid()))
		{
			setSupid(StringUtils.noNull(inputRecord.getSupid()));
		}
		if (StringUtils.hasChanged(getSeqq1(), inputRecord.getSeqq1()))
		{
			setSeqq1(StringUtils.noNull(inputRecord.getSeqq1()));
		}
		if (StringUtils.hasChanged(getSeqq2(), inputRecord.getSeqq2()))
		{
			setSeqq2(StringUtils.noNull(inputRecord.getSeqq2()));
		}
		if (StringUtils.hasChanged(getSeqq3(), inputRecord.getSeqq3()))
		{
			setSeqq3(StringUtils.noNull(inputRecord.getSeqq3()));
		}
		if (StringUtils.hasChanged(getSeqa1(), inputRecord.getSeqa1()))
		{
			setSeqa1(StringUtils.noNull(inputRecord.getSeqa1()));
		}
		if (StringUtils.hasChanged(getSeqa2(), inputRecord.getSeqa2()))
		{
			setSeqa2(StringUtils.noNull(inputRecord.getSeqa2()));
		}
		if (StringUtils.hasChanged(getSeqa3(), inputRecord.getSeqa3()))
		{
			setSeqa3(StringUtils.noNull(inputRecord.getSeqa3()));
		}
		if (StringUtils.hasChanged(getMadeby(), inputRecord.getMadeby()))
		{
			setMadeby(StringUtils.noNull(inputRecord.getMadeby()));
		}
		if (StringUtils.hasChanged(getMadeat(), inputRecord.getMadeat()))
		{
			setMadeat(StringUtils.noNull(inputRecord.getMadeat()));
		}
		if (StringUtils.hasChanged(getCheckedby(), inputRecord.getCheckedby()))
		{
			setCheckedby(StringUtils.noNull(inputRecord.getCheckedby()));
		}
		if (StringUtils.hasChanged(getCheckedat(), inputRecord.getCheckedat()))
		{
			setCheckedat(StringUtils.noNull(inputRecord.getCheckedat()));
		}
		if (StringUtils.hasChanged(getMakerlastcmt(), inputRecord.getMakerlastcmt()))
		{
			setMakerlastcmt(StringUtils.noNull(inputRecord.getMakerlastcmt()));
		}
		if (StringUtils.hasChanged(getCheckerlastcmt(), inputRecord.getCheckerlastcmt()))
		{
			setCheckerlastcmt(StringUtils.noNull(inputRecord.getCheckerlastcmt()));
		}
		if (StringUtils.hasChanged(getCurrappstatus(), inputRecord.getCurrappstatus()))
		{
			setCurrappstatus(StringUtils.noNull(inputRecord.getCurrappstatus()));
		}
		if (StringUtils.hasChanged(getAdminlastcmt(), inputRecord.getAdminlastcmt()))
		{
			setAdminlastcmt(StringUtils.noNull(inputRecord.getAdminlastcmt()));
		}
		if (StringUtils.hasChanged(getRstatus(), inputRecord.getRstatus()))
		{
			setRstatus(StringUtils.noNull(inputRecord.getRstatus()));
		}
		if (StringUtils.hasChanged(getCreatedby(), inputRecord.getCreatedby()))
		{
			setCreatedby(StringUtils.noNull(inputRecord.getCreatedby()));
		}
		if (StringUtils.hasChanged(getCreatedat(), inputRecord.getCreatedat()))
		{
			setCreatedat(StringUtils.noNull(inputRecord.getCreatedat()));
		}
		if (StringUtils.hasChanged(getModifiedat(), inputRecord.getModifiedat()))
		{
			setModifiedat(StringUtils.noNull(inputRecord.getModifiedat()));
		}
		if (StringUtils.hasChanged(getModifiedby(), inputRecord.getModifiedby()))
		{
			setModifiedby(StringUtils.noNull(inputRecord.getModifiedby()));
		}
		if (StringUtils.hasChanged(getBlockedflag(), inputRecord.getBlockedflag()))
		{
			setBlockedflag(StringUtils.noNull(inputRecord.getBlockedflag()));
		}
		if (StringUtils.hasChanged(getPwdsetdate(), inputRecord.getPwdsetdate()))
		{
			setPwdsetdate(StringUtils.noNull(inputRecord.getPwdsetdate()));
		}
		if (StringUtils.hasChanged(getInitialpwd(), inputRecord.getInitialpwd()))
		{
			setInitialpwd(StringUtils.noNull(inputRecord.getInitialpwd()));
		}
		if (StringUtils.hasChanged(getGroupcode(), inputRecord.getGroupcode()))
		{
			setGroupcode(StringUtils.noNull(inputRecord.getGroupcode()));
		}
		if (StringUtils.hasChanged(getOtpgen(), inputRecord.getOtpgen()))
		{
			setOtpgen(StringUtils.noNull(inputRecord.getOtpgen()));
		}
		if (StringUtils.hasChanged(getOtpgenat(), inputRecord.getOtpgenat()))
		{
			setOtpgenat(StringUtils.noNull(inputRecord.getOtpgenat()));
		}
		if (StringUtils.hasChanged(getAddress(), inputRecord.getAddress()))
		{
			setAddress(StringUtils.noNull(inputRecord.getAddress()));
		}
		if (StringUtils.hasChanged(getCity(), inputRecord.getCity()))
		{
			setCity(StringUtils.noNull(inputRecord.getCity()));
		}
		if (StringUtils.hasChanged(getDregion(), inputRecord.getDregion()))
		{
			setDregion(StringUtils.noNull(inputRecord.getDregion()));
		}
		if (StringUtils.hasChanged(getDgroup(), inputRecord.getDgroup()))
		{
			setDgroup(StringUtils.noNull(inputRecord.getDgroup()));
		}
	}

	public JSONObject getJSONObject()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("userid",StringUtils.noNull(userid));				
		obj.put("userpassword",StringUtils.noNull(userpassword));				
		obj.put("prefix",StringUtils.noNull(prefix));				
		obj.put("firstname",StringUtils.noNull(firstname));				
		obj.put("middlename",StringUtils.noNull(middlename));				
		obj.put("lastname",StringUtils.noNull(lastname));				
		obj.put("email",StringUtils.noNull(email));				
		obj.put("mobile",StringUtils.noNull(mobile));				
		obj.put("institutionid",StringUtils.noNull(institutionid));				
		obj.put("country",StringUtils.noNull(country));				
		obj.put("lastlogindate",StringUtils.noNull(lastlogindate));				
		obj.put("lastloginfdate",StringUtils.noNull(lastloginfdate));				
		obj.put("loginattempts",StringUtils.noNull(loginattempts));				
		obj.put("preflang",StringUtils.noNull(preflang));				
		obj.put("prefcomm",StringUtils.noNull(prefcomm));				
		obj.put("userroleid",StringUtils.noNull(userroleid));				
		obj.put("supid",StringUtils.noNull(supid));				
		obj.put("seqq1",StringUtils.noNull(seqq1));				
		obj.put("seqq2",StringUtils.noNull(seqq2));				
		obj.put("seqq3",StringUtils.noNull(seqq3));				
		obj.put("seqa1",StringUtils.noNull(seqa1));				
		obj.put("seqa2",StringUtils.noNull(seqa2));				
		obj.put("seqa3",StringUtils.noNull(seqa3));				
		obj.put("madeby",StringUtils.noNull(madeby));				
		obj.put("madeat",StringUtils.noNull(madeat));				
		obj.put("checkedby",StringUtils.noNull(checkedby));				
		obj.put("checkedat",StringUtils.noNull(checkedat));				
		obj.put("makerlastcmt",StringUtils.noNull(makerlastcmt));				
		obj.put("checkerlastcmt",StringUtils.noNull(checkerlastcmt));				
		obj.put("currappstatus",StringUtils.noNull(currappstatus));				
		obj.put("adminlastcmt",StringUtils.noNull(adminlastcmt));				
		obj.put("rstatus",StringUtils.noNull(rstatus));				
		obj.put("createdby",StringUtils.noNull(createdby));				
		obj.put("createdat",StringUtils.noNull(createdat));				
		obj.put("modifiedat",StringUtils.noNull(modifiedat));				
		obj.put("modifiedby",StringUtils.noNull(modifiedby));				
		obj.put("blockedflag",StringUtils.noNull(blockedflag));				
		obj.put("pwdsetdate",StringUtils.noNull(pwdsetdate));				
		obj.put("initialpwd",StringUtils.noNull(initialpwd));				
		obj.put("groupcode",StringUtils.noNull(groupcode));				
		obj.put("otpgen",StringUtils.noNull(otpgen));				
		obj.put("otpgenat",StringUtils.noNull(otpgenat));				
		obj.put("address",StringUtils.noNull(address));				
		obj.put("city",StringUtils.noNull(city));				
		obj.put("dregion",StringUtils.noNull(dregion));				
		obj.put("dgroup",StringUtils.noNull(dgroup));
		return obj;
	}

	public void loadJSONObject(JSONObject obj)
	throws Exception
	{
		if (obj == null) return;

		id = StringUtils.getValueFromJSONObject(obj, "id");				
		userid = StringUtils.getValueFromJSONObject(obj, "userid");				
		userpassword = StringUtils.getValueFromJSONObject(obj, "userpassword");				
		prefix = StringUtils.getValueFromJSONObject(obj, "prefix");				
		firstname = StringUtils.getValueFromJSONObject(obj, "firstname");				
		middlename = StringUtils.getValueFromJSONObject(obj, "middlename");				
		lastname = StringUtils.getValueFromJSONObject(obj, "lastname");				
		email = StringUtils.getValueFromJSONObject(obj, "email");				
		mobile = StringUtils.getValueFromJSONObject(obj, "mobile");				
		institutionid = StringUtils.getValueFromJSONObject(obj, "institutionid");				
		country = StringUtils.getValueFromJSONObject(obj, "country");				
		lastlogindate = StringUtils.getValueFromJSONObject(obj, "lastlogindate");				
		lastloginfdate = StringUtils.getValueFromJSONObject(obj, "lastloginfdate");				
		loginattempts = StringUtils.getValueFromJSONObject(obj, "loginattempts");				
		preflang = StringUtils.getValueFromJSONObject(obj, "preflang");				
		prefcomm = StringUtils.getValueFromJSONObject(obj, "prefcomm");				
		userroleid = StringUtils.getValueFromJSONObject(obj, "userroleid");				
		supid = StringUtils.getValueFromJSONObject(obj, "supid");				
		seqq1 = StringUtils.getValueFromJSONObject(obj, "seqq1");				
		seqq2 = StringUtils.getValueFromJSONObject(obj, "seqq2");				
		seqq3 = StringUtils.getValueFromJSONObject(obj, "seqq3");				
		seqa1 = StringUtils.getValueFromJSONObject(obj, "seqa1");				
		seqa2 = StringUtils.getValueFromJSONObject(obj, "seqa2");				
		seqa3 = StringUtils.getValueFromJSONObject(obj, "seqa3");				
		madeby = StringUtils.getValueFromJSONObject(obj, "madeby");				
		madeat = StringUtils.getValueFromJSONObject(obj, "madeat");				
		checkedby = StringUtils.getValueFromJSONObject(obj, "checkedby");				
		checkedat = StringUtils.getValueFromJSONObject(obj, "checkedat");				
		makerlastcmt = StringUtils.getValueFromJSONObject(obj, "makerlastcmt");				
		checkerlastcmt = StringUtils.getValueFromJSONObject(obj, "checkerlastcmt");				
		currappstatus = StringUtils.getValueFromJSONObject(obj, "currappstatus");				
		adminlastcmt = StringUtils.getValueFromJSONObject(obj, "adminlastcmt");				
		rstatus = StringUtils.getValueFromJSONObject(obj, "rstatus");				
		createdby = StringUtils.getValueFromJSONObject(obj, "createdby");				
		createdat = StringUtils.getValueFromJSONObject(obj, "createdat");				
		modifiedat = StringUtils.getValueFromJSONObject(obj, "modifiedat");				
		modifiedby = StringUtils.getValueFromJSONObject(obj, "modifiedby");				
		blockedflag = StringUtils.getValueFromJSONObject(obj, "blockedflag");				
		pwdsetdate = StringUtils.getValueFromJSONObject(obj, "pwdsetdate");				
		initialpwd = StringUtils.getValueFromJSONObject(obj, "initialpwd");				
		groupcode = StringUtils.getValueFromJSONObject(obj, "groupcode");				
		otpgen = StringUtils.getValueFromJSONObject(obj, "otpgen");				
		otpgenat = StringUtils.getValueFromJSONObject(obj, "otpgenat");				
		address = StringUtils.getValueFromJSONObject(obj, "address");				
		city = StringUtils.getValueFromJSONObject(obj, "city");				
		dregion = StringUtils.getValueFromJSONObject(obj, "dregion");				
		dgroup = StringUtils.getValueFromJSONObject(obj, "dgroup");
		return;
	}

	public JSONObject getJSONObjectUI()
	{
		JSONObject obj = new JSONObject();

		obj.put("id",StringUtils.noNull(id));				
		obj.put("user_id",StringUtils.noNull(userid));				
		obj.put("user_password",StringUtils.noNull(userpassword));				
		obj.put("prefix",StringUtils.noNull(prefix));				
		obj.put("first_name",StringUtils.noNull(firstname));				
		obj.put("middle_name",StringUtils.noNull(middlename));				
		obj.put("last_name",StringUtils.noNull(lastname));				
		obj.put("email",StringUtils.noNull(email));				
		obj.put("mobile",StringUtils.noNull(mobile));				
		obj.put("institution_id",StringUtils.noNull(institutionid));				
		obj.put("country",StringUtils.noNull(country));				
		obj.put("last_login_date",StringUtils.noNull(lastlogindate));				
		obj.put("last_login_fdate",StringUtils.noNull(lastloginfdate));				
		obj.put("login_attempts",StringUtils.noNull(loginattempts));				
		obj.put("pref_lang",StringUtils.noNull(preflang));				
		obj.put("pref_comm",StringUtils.noNull(prefcomm));				
		obj.put("user_role_id",StringUtils.noNull(userroleid));				
		obj.put("sup_id",StringUtils.noNull(supid));				
		obj.put("seq_q1",StringUtils.noNull(seqq1));				
		obj.put("seq_q2",StringUtils.noNull(seqq2));				
		obj.put("seq_q3",StringUtils.noNull(seqq3));				
		obj.put("seq_a1",StringUtils.noNull(seqa1));				
		obj.put("seq_a2",StringUtils.noNull(seqa2));				
		obj.put("seq_a3",StringUtils.noNull(seqa3));				
		obj.put("made_by",StringUtils.noNull(madeby));				
		obj.put("made_at",StringUtils.noNull(madeat));				
		obj.put("checked_by",StringUtils.noNull(checkedby));				
		obj.put("checked_at",StringUtils.noNull(checkedat));				
		obj.put("maker_last_cmt",StringUtils.noNull(makerlastcmt));				
		obj.put("checker_last_cmt",StringUtils.noNull(checkerlastcmt));				
		obj.put("curr_app_status",StringUtils.noNull(currappstatus));				
		obj.put("admin_last_cmt",StringUtils.noNull(adminlastcmt));				
		obj.put("rstatus",StringUtils.noNull(rstatus));				
		obj.put("created_by",StringUtils.noNull(createdby));				
		obj.put("created_at",StringUtils.noNull(createdat));				
		obj.put("modified_at",StringUtils.noNull(modifiedat));				
		obj.put("modified_by",StringUtils.noNull(modifiedby));				
		obj.put("blocked_flag",StringUtils.noNull(blockedflag));				
		obj.put("pwdsetdate",StringUtils.noNull(pwdsetdate));				
		obj.put("initial_pwd",StringUtils.noNull(initialpwd));				
		obj.put("group_code",StringUtils.noNull(groupcode));				
		obj.put("otp_gen",StringUtils.noNull(otpgen));				
		obj.put("otp_gen_at",StringUtils.noNull(otpgenat));				
		obj.put("address",StringUtils.noNull(address));				
		obj.put("city",StringUtils.noNull(city));				
		obj.put("dregion",StringUtils.noNull(dregion));				
		obj.put("dgroup",StringUtils.noNull(dgroup));
		return obj;
	}

	public void log()
	{
		logger.trace(this.toString());
	}

	public HashMap getTableMap()
	{
		HashMap resultMap = new HashMap();
		ArrayList columnList = new ArrayList();
		resultMap.put("table", "system_user");

		columnList.add("id");				
		columnList.add("user_id");				
		columnList.add("user_password");				
		columnList.add("prefix");				
		columnList.add("first_name");				
		columnList.add("middle_name");				
		columnList.add("last_name");				
		columnList.add("email");				
		columnList.add("mobile");				
		columnList.add("institution_id");				
		columnList.add("country");				
		columnList.add("last_login_date");				
		columnList.add("last_login_fdate");				
		columnList.add("login_attempts");				
		columnList.add("pref_lang");				
		columnList.add("pref_comm");				
		columnList.add("user_role_id");				
		columnList.add("sup_id");				
		columnList.add("seq_q1");				
		columnList.add("seq_q2");				
		columnList.add("seq_q3");				
		columnList.add("seq_a1");				
		columnList.add("seq_a2");				
		columnList.add("seq_a3");				
		columnList.add("made_by");				
		columnList.add("made_at");				
		columnList.add("checked_by");				
		columnList.add("checked_at");				
		columnList.add("maker_last_cmt");				
		columnList.add("checker_last_cmt");				
		columnList.add("curr_app_status");				
		columnList.add("admin_last_cmt");				
		columnList.add("rstatus");				
		columnList.add("created_by");				
		columnList.add("created_at");				
		columnList.add("modified_at");				
		columnList.add("modified_by");				
		columnList.add("blocked_flag");				
		columnList.add("pwdsetdate");				
		columnList.add("initial_pwd");				
		columnList.add("group_code");				
		columnList.add("otp_gen");				
		columnList.add("otp_gen_at");				
		columnList.add("address");				
		columnList.add("city");				
		columnList.add("dregion");				
		columnList.add("dgroup");
		resultMap.put("ColumnList", columnList);

		return resultMap;
	}

	public void toggleStatus()
	{
		String currentStatus = StringUtils.noNull(getRstatus());
		setRstatus("1");
		if (currentStatus.equals("1"))
		{
			setRstatus("0");
		}
	}

}
