(function () {
    'use strict';

    angular
            .module('app.module1.page7')
            .controller('m2page7Controller', m2page7Controller);

    /* @ngInject */
    function m2page7Controller($scope,$state, service, $http, $rootScope) {
        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776","#92D400","#00A1DE","#72C7E7","#3C8A2E","#C9DD03","#335291","#A8DD33","#33B4E5"];
        $rootScope.subpage="";
        $rootScope.subpagexs="";
        $rootScope.subpage+=" ";
        $rootScope.hideyear=false;
        $rootScope.hidewave=true;
        $rootScope.hideregion=false;
        $rootScope.hidearea=false;
        $rootScope.hidezone=false;
        $rootScope.hideprovince=false;
        $rootScope.hidecity=false;
        $rootScope.hidemodel=true;
        $rootScope.hidefactor=false;
        $rootScope.dealerhide=true;
        $rootScope.pagenumber='3';
        $rootScope.starlabelhide=false;
        $rootScope.legendname="Study Best";
        $rootScope.priorityhide=true;
        $rootScope.summaryRefresh=1;
        $rootScope.checkboxInitiliser();
        
         var parameters=[];
        var botparameters=[];
        $scope.legendname="Study Best";
        $scope.factor=$rootScope.Filterarray.factor[0];
        $scope.area="All";
        $scope.zone="All";
        $scope.province="All";
        $scope.year=$rootScope.Filterarray.year[$rootScope.Filterarray.year.length-1];
        $scope.region="All";
        $scope.city="All";
        $scope.model="All";
        
        
        
        //////////////////////////////////////////////////////////////
            var w = window.innerWidth|| document.documentElement.clientWidth|| document.body.clientWidth;
            var h = window.innerHeight|| document.documentElement.clientHeight|| document.body.clientHeight;

            console.log( " height :" , h);
            console.log( " width :" , w);
            var margin=300;
            var lineheight=25;
            $scope.xstrue=false;
            var weight=50;
            if(w < 1280 && w > 960){margin=250;}
            else if(w < 960 && w > 600){ margin=200;lineheight=12;weight=40;}
            else if(w < 600){margin=150;lineheight=12;weight=30;$scope.xstrue=true;}
            console.log('margin',margin);
        
        $scope.$on('changeyear', function (event, data) {
            $scope.year=data;
            $scope.filterchange();
          });
        
        $scope.$on('changeregion', function (event, data) {
            $scope.region=data;
            $scope.area = "All";
                $scope.zone = "All";
                $scope.province = "All";
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
          $scope.$on('changearea', function (event, data) {
            $scope.area=data;
            
                $scope.zone = "All";
                $scope.province = "All";
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
          $scope.$on('changezone', function (event, data) {
            $scope.zone=data;
            
                $scope.province = "All";
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
          $scope.$on('changeprovince', function (event, data) {
            $scope.province=data;
            
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
        
        $scope.$on('changecity', function (event, data) {
            $scope.city=data;
            
                $scope.dealer="All";
            $scope.filterchange();
          });
          
          $scope.$on('changefactor', function (event, data) {
            $scope.factor=data;
             $scope.botgraph();
          });
               //////////////////////////////////////////////////////////////
 
       
       
        parameters=[{"name":"area","value":$scope.area},{"name":"zone","value":$scope.zone},{"name":"province","value":$scope.province},{"name":"year","value":$scope.year},{"name":"region","value":$scope.region},{"name":"city","value":encodeURIComponent($scope.city)},{"name":"factor","value":$scope.factor},{"name":"model","value":$scope.model}];
      
      
                $scope.graphs=function(){   

            //  SSITrendAnalysis-getSSITrendByWave 1
                       service.mastersrv("SSITrendAnalysis-getSSITrendByWave",parameters).then(function (response) {
                            console.log('response.data',response.data);
                            console.log('response.data.data',response.data.data);
                            console.log('response.data.samplecount',response.data.samplecount);
                            if(response.data.samplecount < 10){
                                 $scope.samplespaceless=true;
                                 console.log(  $scope.samplespaceless);
                            }
                            else{
                                 $scope.samplespaceless=false;
                                 console.log(  $scope.samplespaceless);
                            }

                            $('#top1').highcharts(service.plainbar('column',['#2979FF'],response.data.data,'SSI Score',0,55,.3,[1,1,1,1,1,1],5,false));
                               $('#top11').highcharts(service.plainbar('column',['#2979FF'],response.data.data,'SSI Score',0,55,.3,[1,1,1,1,1,1],5,false));
                               $rootScope.legendname=response.data[0].scatter;
                       });

            //  SSITrendAnalysis-getSSIFactorTrendByWave 2
                       service.mastersrv("SSITrendAnalysis-getSSIFactorTrendByWave",parameters).then(function (response) {
                             $('#top2').highcharts(service.plainbar('column',['#7E57C2'],response.data,'Factor Score',0,55,.3,[1,1,1,1,1,1],5,false));
                               $('#top21').highcharts(service.plainbar('column',['#7E57C2'],response.data,'Factor Score',0,55,.3,[1,1,1,1,1,1],5,false));
                                $rootScope.legendname=response.data[0].scatter;
                       });

            //  SSITrendAnalysis-getSSIAttributeTrendByWave 3
                    service.mastersrv("SSITrendAnalysis-getSSIAttributeTrendByWave",parameters).then(function (response) {
                                      $('#bot').highcharts(service.multigraph(response,false,color));
                                       $('#bot1').highcharts(service.multigraph(response,false,color));
                      });
                  } 
                  
                  
                   $scope.botrender=function(){
                    //  SSITrendAnalysis-getSSIFactorTrendByWave 2
                               service.mastersrv("SSITrendAnalysis-getSSIFactorTrendByWave",botparameters).then(function (response) {
                                     $('#top2').highcharts(service.plainbar('column',['#7E57C2'],response.data,'Factor Score',0,55,.3,[1,1,1,1,1,1],5,false));
                              $scope.legendname=response.data[0].scatter;
                               });

                    //  SSITrendAnalysis-getSSIAttributeTrendByWave 3
                            service.mastersrv("SSITrendAnalysis-getSSIAttributeTrendByWave",botparameters).then(function (response) {
                                              $('#bot').highcharts(service.multigraph(response,false,color));
                              });
            
        }
                  
             $scope.filterchange=function(){
                             parameters=[{"name":"area","value":$scope.area},{"name":"zone","value":$scope.zone},{"name":"province","value":$scope.province},{"name":"year","value":$scope.year},{"name":"region","value":$scope.region},{"name":"city","value":encodeURIComponent($scope.city)},{"name":"factor","value":$scope.factor},{"name":"model","value":$scope.model}];
                             $scope.graphs();
                              $scope.graphs();
                     }
                     

               $scope.botgraph=function(){
                    botparameters=[{"name":"area","value":$scope.area},{"name":"zone","value":$scope.zone},{"name":"province","value":$scope.province},{"name":"region","value":$scope.region},{"name":"year","value":$scope.year},{"name":"wave","value":$scope.waves},{"name":"city","value":$scope.city},{"name":"model","value":$scope.model},{"name":"factor","value":$scope.factor}];
                    $scope.botrender();
              }
    }
})();