(function() {
    'use strict';

    angular
        .module('app.module1.page7')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

          $stateProvider
          .state('triangular.page7', {
            url: '/page7',
            
             views: {
                '': {
                   templateUrl: 'app/module1/page7/page7.tmpl.html',
            controller: 'm2page7Controller',
            controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module1/page7/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        });
        
         triMenuProvider.addMenu({
                   name: 'Trend Analysis',
                    icon: 'zmdi zmdi-menu',
                    type: 'link',
                    priority: 1.2,
                     state: 'triangular.page7',
                });
             triMenuProvider.removeMenu('triangular.page7');   
                
                
    }
})();