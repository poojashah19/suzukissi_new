(function () {
    'use strict';

    angular
            .module('app.module1.page9')
            .controller('m2page9Controller', m2page9Controller);

    /* @ngInject */
    function m2page9Controller($scope,$state, service, $http, $rootScope) {
        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776","#92D400","#00A1DE","#72C7E7","#3C8A2E","#C9DD03","#335291","#A8DD33","#33B4E5"];
        $rootScope.subpage="";
        $rootScope.subpagexs="";
        $rootScope.subpage+=" ";
        $rootScope.hideyear=false;
        $rootScope.hidewave=true;
        $rootScope.hideregion=false;
        $rootScope.hidearea=false;
        $rootScope.hidezone=false;
        $rootScope.hideprovince=false;
        $rootScope.hidecity=true;
        $rootScope.hidemodel=true;
        $rootScope.hidefactor=true;
        $rootScope.dealerhide=false;
        $rootScope.pagenumber='6';
        $rootScope.starlabelhide=true;
        $rootScope.priorityhide=true;
        $rootScope.summaryRefresh=1;
       $rootScope.checkboxInitiliser();
        
        //////////////////////////////////////////////////////////////
            var w = window.innerWidth|| document.documentElement.clientWidth|| document.body.clientWidth;
            var h = window.innerHeight|| document.documentElement.clientHeight|| document.body.clientHeight;

            console.log( " height :" , h);
            console.log( " width :" , w);
            var margin=300;
            var lineheight=25;
            $scope.xstrue=false;
            var weight=50;
            if(w < 1280 && w > 960){margin=250;}
            else if(w < 960 && w > 600){ margin=200;lineheight=12;weight=40;}
            else if(w < 600){margin=150;lineheight=12;weight=30;$scope.xstrue=true;}
            console.log('margin',margin);
        
            $scope.$on('changeyear', function (event, data) {
                $scope.year=data;
                $scope.filterchange();
              });

            $scope.$on('changeregion', function (event, data) {
                $scope.region=data;
                $scope.area = "All";
                $scope.zone = "All";
                $scope.province = "All";
                $scope.city = "All";
                $scope.dealer="All";
                $scope.filterchange();
              });
              
           $scope.$on('changearea', function (event, data) {
            $scope.area=data;
                $scope.zone = "All";
                $scope.province = "All";
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
          $scope.$on('changezone', function (event, data) {
            $scope.zone=data;
                $scope.province = "All";
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
          $scope.$on('changeprovince', function (event, data) {
            $scope.province=data;
            
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });

            $scope.$on('changedealer', function (event, data) {
                $scope.dealer=data;
                $scope.filterchange();
              });
               //////////////////////////////////////////////////////////////
               
               
               
              $scope.area="All";
        $scope.zone="All";
        $scope.province="All";
        $scope.region="All";
        $scope.model="All";
        $scope.dealer="All";
        $scope.year=$rootScope.Filterarray.year[$rootScope.Filterarray.year.length-1];
        $scope.sopswitchoutput='study';
        $scope.sopswitchoutputcaps='Study';
        $scope.sop=$rootScope.Filterarray.sop[0];
        var parameters=[{"name":"compareto","value":$scope.sopswitchoutput},{"name":"attribute","value":$scope.sop},{"name":"area","value":$scope.area},{"name":"zone","value":$scope.zone},{"name":"province","value":$scope.province},{"name":"region","value":$scope.region},{"name":"year","value":$scope.year},{"name":"model","value":$scope.model},{"name":"dealer","value":$scope.dealer}];
        vm.sopswitch=sopswitch;
           $scope.group1="Total";
        
        function sopswitch(flag){
            if(flag == 'Total'){
                $scope.sopswitchoutput='total';$scope.sopswitchoutputcaps='Total';
            }
            else if(flag == 'Zone'){
                $scope.sopswitchoutput='zone';$scope.sopswitchoutputcaps='Zone';
            }
            else if(flag == 'Region'){
                $scope.sopswitchoutput='region';$scope.sopswitchoutputcaps='Region';
            }
            
            $scope.filterchange();
        }
        
        
       $scope.graphs=function(){
             service.mastersrv("SSISOPPerformance-getSSISOPTrend",parameters).then(function (response) {
//                 console.log('response.data',response.data);
                    console.log('response.data.data',response.data.data);
                    console.log('response.data.samplecount',response.data.samplecount);
                        if(response.data.samplecount < 10){
                             $scope.samplespaceless=true;
                             console.log(  $scope.samplespaceless);
                         }
                        else{
                             $scope.samplespaceless=false;
                             console.log(  $scope.samplespaceless);
                         }
                  if(response.data.data.length==0){
                                response.data.data=[{"bartitle":"100%"},{"y":"0"}];
                            }
                 $('#mainchart').highcharts(service.barline(['#2196F3'],response.data.data));

          });
          
        }
        
         $scope.sopchanged=function(sop){
                $scope.sop = sop;
                $scope.filterchange();
            }
        
         $scope.filterchange=function(){
//             $scope.switchdisable=false;
                 parameters=[{"name":"compareto","value":$scope.sopswitchoutput},{"name":"attribute","value":$scope.sop},{"name":"area","value":$scope.area},{"name":"zone","value":$scope.zone},{"name":"province","value":$scope.province},{"name":"region","value":$scope.region},{"name":"year","value":$scope.year},{"name":"model","value":$scope.model},{"name":"dealer","value":$scope.dealer}];
                 $scope.graphs();
         }
    }
})();