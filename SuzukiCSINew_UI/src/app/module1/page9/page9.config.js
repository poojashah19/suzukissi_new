(function() {
    'use strict';

    angular
        .module('app.module1.page9')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

          $stateProvider
          .state('triangular.page9', {
            url: '/page9',
            
             views: {
                '': {
                   templateUrl: 'app/module1/page9/page9.tmpl.html',
            controller: 'm2page9Controller',
            controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module1/page9/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        });
        
         triMenuProvider.addMenu({
                   name: 'SOP Trend',
                    icon: 'zmdi zmdi-hourglass',
                    type: 'link',
                    priority: 1.5,
                     state: 'triangular.page9',
                });
             triMenuProvider.removeMenu('triangular.page9');   
                
                
    }
})();