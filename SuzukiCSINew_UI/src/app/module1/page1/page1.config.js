(function() {
    'use strict';

    angular
        .module('app.module1.page1')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

          $stateProvider
         .state('triangular.page1', {
            url: '/page1',
            views: {
                '': {
                   templateUrl: 'app/module1/page1/page1.tmpl.html',
                    controller: 'm2page1Controller',
                    controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module1/page1/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
            
        });
        
        
         triMenuProvider.addMenu({
                    name: 'Priority Analysis',
                    icon: 'zmdi zmdi-badge-check',
                    type: 'link',
                    priority: 1.3,
                     state: 'triangular.page1',
                });
                triMenuProvider.removeMenu('triangular.page1'); 
  
                
    }
})();