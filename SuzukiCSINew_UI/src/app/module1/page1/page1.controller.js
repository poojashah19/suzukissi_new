
(function () {
    'use strict';

    angular
            .module('app.module1.page1')
            .controller('m2page1Controller', m2page1Controller);

    /* @ngInject */
    function m2page1Controller($scope, service,$state, $http, $rootScope) {
        
        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776","#92D400","#00A1DE","#72C7E7","#3C8A2E","#C9DD03","#335291","#A8DD33","#33B4E5"];
        $rootScope.subpage="";
        $rootScope.subpagexs="";
        $rootScope.subpage+=" ";
        $rootScope.pagenumber='4';
        $rootScope.starlabelhide=true;
        $rootScope.priorityhide=false;
        $rootScope.summaryRefresh=1;
       $rootScope.checkboxInitiliser();
        
        //////////////////////////////////////////////////////////////
            var w = window.innerWidth|| document.documentElement.clientWidth|| document.body.clientWidth;
            var h = window.innerHeight|| document.documentElement.clientHeight|| document.body.clientHeight;

            console.log( " height :" , h);
            console.log( " width :" , w);
            var margin=300;
            var lineheight=25;
            $scope.xstrue=false;
            var weight=50;
            if(w < 1280 && w > 960){margin=250;}
            else if(w < 960 && w > 600){ margin=200;lineheight=12;weight=40;}
            else if(w < 600){margin=150;lineheight=12;weight=30;$scope.xstrue=true;}
            console.log('margin',margin);
        //////////////////////////////////////////////////////////////
        
     
        $scope.$on('changewaves', function (event, data) {
            console.log('console.log(fuel)',data);
            $scope.waves=data;
            if($scope.default=='region')
                   { $scope.regionfilterchange()}
                else
                    {$scope.dealerfilterchange()}
          });
          
          $scope.$on('changeRadioSwitch', function (event, data) {
                if(data=='dealer'){
                    $scope.default='dealer';
                    $scope.dealerfilterchange();}
                else if(data =='region'){
                    $scope.default='region';
                    $scope.regionfilterchange();
                } 
          });
        
        
         $scope.$on('changeregion1', function (event, data) {
             $scope.region1=data;
                   { $scope.regionfilterchange()}
          });
          $scope.$on('changeregion2', function (event, data) {
              $scope.region2=data;
                   { $scope.regionfilterchange()}
          });
          
           $scope.$on('changedealer1', function (event, data) {
               $scope.dealer1=data;
                    {$scope.dealerfilterchange()}
          });
          $scope.$on('changedealer2', function (event, data) {
              $scope.dealer2=data;
                    {$scope.dealerfilterchange()}
          });
               //////////////////////////////////////////////////////////////
             var vm = this;
            $scope.radioSwitch="Region Comparison";
            $scope.default="region";

            $scope.samplespaceless=false;
             $scope.dealer1=$rootScope.Filterarray.dealer[1];
            $scope.dealer2=$rootScope.Filterarray.dealer[2];
            $scope.waves=$rootScope.Filterarray.waves[$rootScope.Filterarray.waves.length-1];
            $scope.region1=$rootScope.Filterarray.region[1];
            $scope.region2=$rootScope.Filterarray.region[2];
    //        var parameters=[{"name":"measure","value":$scope.default},{"name":"region1","value":$scope.region1},{"name":"region2","value":$scope.region2}];
            var parameters=[{"name":"measure","value":$scope.default}, {"name": "wave", "value": $scope.waves},{"name":"region1","value":$scope.region1},{"name":"region2","value":$scope.region2}];
            service.ssi_filterfunction();

                    $scope.onchangeRadioSwitch=function(radio){
                        if(radio== 'Dealer Comparison'){
                            $scope.priorityRegionHide=true;
                            $scope.default='dealer';
                            $scope.dealerfilterchange();
                        }
                        else{
                            $scope.priorityRegionHide=false;
                            $scope.default='region';
                            $scope.regionfilterchange();
                        }
                    }
        
       $scope.graphs=function(){
            service.mastersrv("SSIPriority-getSSIPriority",parameters).then(function (response) {
                      $('#mainchart').highcharts(service.verticalline(color,true,response.data,'left','top',true,'-50','50','2'));
                       $('#mainchart1').highcharts(service.verticalline(color,true,response.data,'left','top',true,'-50','50','2'));
           
            });
        }
       
      
         $scope.dealerfilterchange=function(){
                 parameters=[{"name":"measure","value":$scope.default},{"name":"wave","value":$scope.waves},{"name":"dealer1","value":$scope.dealer1},{"name":"dealer2","value":$scope.dealer2}];
                 $scope.graphs();
         }
         
          $scope.regionfilterchange=function(){
                 parameters=[{"name":"measure","value":$scope.default},{"name":"region1","value":$scope.region1},{"name":"region2","value":$scope.region2},{"name":"wave","value":$scope.waves}];
                 $scope.graphs();
         }

       
        
    }
})();