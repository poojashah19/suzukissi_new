(function () {
    'use strict';

    angular
            .module('app.module1.page5')
            .controller('m2page5Controller', m2page5Controller);

    /* @ngInject */
    function m2page5Controller($scope,$state, service, $http, $rootScope) {
        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776","#92D400","#00A1DE","#72C7E7","#3C8A2E","#C9DD03","#335291","#A8DD33","#33B4E5"];
        $rootScope.subpage="";
        $rootScope.subpagexs="";
        $rootScope.subpage+=" ";
        $rootScope.hideyear=false;
        $rootScope.hidewave=false;
        $rootScope.hideregion=false;
        $rootScope.hidearea=false;
        $rootScope.hidezone=false;
        $rootScope.hideprovince=false;
        $rootScope.hidecity=true;
        $rootScope.hidemodel=true;
        $rootScope.hidefactor=true;
        $rootScope.dealerhide=false;
        $rootScope.pagenumber='7';
        $rootScope.starlabelhide=true;
        $rootScope.priorityhide=true;
        $rootScope.summaryRefresh=1;
        
         $rootScope.dsnapshot="not";
       $rootScope.checkboxInitiliser();
        
        //////////////////////////////////////////////////////////////
            var w = window.innerWidth|| document.documentElement.clientWidth|| document.body.clientWidth;
            var h = window.innerHeight|| document.documentElement.clientHeight|| document.body.clientHeight;

            console.log( " height :" , h);
            console.log( " width :" , w);
            var margin=300;
            var lineheight=25;
            $scope.xstrue=false;
            var weight=50;
            if(w < 1280 && w > 960){margin=250;}
            else if(w < 960 && w > 600){ margin=200;lineheight=12;weight=40;}
            else if(w < 600){margin=150;lineheight=12;weight=30;$scope.xstrue=true;}
            console.log('margin',margin);
        
        $scope.$on('changeyear', function (event, data) {
            $scope.year=data;
            $scope.filterchange();
          });
        
        $scope.$on('changewaves', function (event, data) {
            $scope.waves=data;
            $scope.filterchange();
          });
          $scope.$on('changeregion', function (event, data) {
            $scope.region=data;
            $scope.area = "All";
                $scope.zone = "All";
                $scope.province = "All";
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
        
        $scope.$on('changearea', function (event, data) {
            $scope.area=data;
                $scope.zone = "All";
                $scope.province = "All";
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
          $scope.$on('changezone', function (event, data) {
            $scope.zone=data;
                $scope.province = "All";
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
          $scope.$on('changeprovince', function (event, data) {
            $scope.province=data;
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
          
       $scope.$on('changedealer', function (event, data) {
            $scope.dealer=data;
            $scope.filterchange();
         });
               //////////////////////////////////////////////////////////////
 
        $("#otherexportCS").click(function () {
                        $("#table22").table2excel({
                            // exclude CSS class
                            exclude: ".noExl",
                            filename: "otherTableDownload"
                        });
                });
       
       var parameters = [];
        $scope.year = $rootScope.Filterarray.year[$rootScope.Filterarray.year.length - 1];
        $scope.waves = $rootScope.Filterarray.waves[$rootScope.Filterarray.waves.length - 1];
        $scope.region = "All";
        $scope.dealer = "All";
        $scope.model = "All";
        service.ssi_filterfunction();
         parameters = [ {"name": "wave", "value": $scope.waves}];
         //////////////////////////////////////////////////////////////
        var marginp1;
        var marginp2;
        var marginp3;
        var w = window.innerWidth|| document.documentElement.clientWidth|| document.body.clientWidth;
        console.log( " width :" , w);

         if(w <1400)
             {marginp2=80}
         else
            {marginp2=76}

         if(w > 1600){
              marginp3=null
         }
         else
         { marginp3=90}

          if(w > 1680)
                {marginp1=null
                       }
            else
                {marginp1=80
                       }
        console.log('marginp1',marginp1);
        console.log('marginp2',marginp2);
        console.log('marginp3',marginp3)
//////////////////////////////////////////////////////////////
          $scope.graphs = function () {
                    //       SSIOthers-getSSIOthersTable
                    service.mastersrv("SSIOthers-getSSIOthersTable", parameters).then(function (response) {
                    $scope.testdata = response.data.data;
                    console.log('response.data',response.data);
                    console.log('response.data.data',response.data.data);
                    console.log('response.data',response.data.samplecount);

                        if(response.data.samplecount < 10){
                             $scope.samplespaceless=true;
                             console.log(  $scope.samplespaceless);
                         }
                        else{
                             $scope.samplespaceless=false;
                             console.log(  $scope.samplespaceless);
                         }


                        $scope.Heading1=response.data.data[0].Heading1;
                        $scope.Heading2=response.data.data[0].Heading2;
                        $scope.Heading3=response.data.data[0].Heading3;
                        $scope.Heading4=response.data.data[0].Heading4;

                        var Value1=response.data.data[0].Heading1;
                        var Value2=response.data.data[0].Heading2;
                        var Value3=response.data.data[0].Heading3;
                        var Value4=response.data.data[0].Heading4;
        //             $scope.format1( $scope.testdata[i].Value1); $scope.format1( $scope.testdata[i].Value2); $scope.format1( $scope.testdata[i].Value3); $scope.format1( $scope.testdata[i].Value4);
                    });

        //SSIOthers-getSSIOthersCharts
                    service.mastersrv("SSIOthers-getSSIOthersCharts", parameters).then(function (response) {
                        $scope.response = response;
                   
                  
                    $scope.heading1=response.data[0].headingname;
                    $('#top01').highcharts(service.stackedgraph(response.data[0].headingdata,color,110));
                    $scope.heading2=response.data[1].headingname;
                    $('#top02').highcharts(service.stackedgraph(response.data[1].headingdata,color));
                    $scope.heading3=response.data[2].headingname;
                    $('#bot01').highcharts(service.stackedgraph(response.data[2].headingdata,color));
                    $scope.heading4=response.data[9].headingname;
                    $('#bot02').highcharts(service.stackedgraph(response.data[9].headingdata,color,90));
                    
                    
                    $scope.heading7=response.data[5].headingname;
                    $('#bot11').highcharts(service.stackedgraph(response.data[5].headingdata,color));
                    $scope.heading6=response.data[8].headingname;
                    $('#top12').highcharts(service.stackedgraph(response.data[8].headingdata,color,80));
                    $scope.heading5=response.data[7].headingname;
                    $('#top11').highcharts(service.stackedgraph(response.data[7].headingdata,color));
                    $scope.heading8=response.data[6].headingname;
                    $('#bot12').highcharts(service.stackedgraph(response.data[6].headingdata,color));
                    
                    
                    $scope.heading0b = response.data[4].headingname;
                    $('#page0b').highcharts(service.stackmultigraph(response.data[4].headingdata, color, 20, '10px', '.07'));
                    $scope.heading0a = response.data[3].headingname;
                    $('#page0a').highcharts(service.stackmultigraph(response.data[3].headingdata, color, 35, '12px', '.07'));
                    $scope.heading0c=response.data[10].headingname;
                    $('#page0c').highcharts(service.stackedgraph(response.data[10].headingdata,color));
                    
                   
                    
                    
                    $('#top01x').highcharts(service.stackedgraph(response.data[6].headingdata,color,80));        
                    $('#top02x').highcharts(service.stackedgraph(response.data[2].headingdata,color));
                    $('#bot01x').highcharts(service.stackedgraph(response.data[7].headingdata,color));
                    $('#bot02x').highcharts(service.stackedgraph(response.data[0].headingdata,color));
                    $('#top11x').highcharts(service.stackedgraph(response.data[4].headingdata,color,marginp1));
                    $('#top12x').highcharts(service.stackedgraph(response.data[5].headingdata,color));
                    $('#bot11x').highcharts(service.stackedgraph(response.data[1].headingdata,color,marginp2));
                    $('#bot12x').highcharts(service.stackedgraph(response.data[3].headingdata,color));
                    $('#page0bx').highcharts(service.stackmultigraph(response.data[9].headingdata, color, 25, '11px', '.07'));
                    $('#page0ax').highcharts(service.stackmultigraph(response.data[10].headingdata, color, 25, '11px', '.07'));
                    $('#page0cx').highcharts(service.stackedgraph(response.data[8].headingdata,color));

                     
                    });

                }

        $scope.filterchange = function () {
            parameters = [{"name":"area","value":$scope.area},{"name":"zone","value":$scope.zone},{"name":"province","value":$scope.province}, {"name": "region", "value": $scope.region}, {"name": "year", "value": $scope.year}, {"name": "wave", "value": $scope.waves}, {"name": "dealer", "value": $scope.dealer}, {"name": "model", "value": $scope.model}];
            $scope.graphs();
        }
        
    }
})();