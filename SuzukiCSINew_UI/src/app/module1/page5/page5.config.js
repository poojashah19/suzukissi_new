(function() {
    'use strict';

    angular
        .module('app.module1.page5')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

          $stateProvider
          .state('triangular.page5', {
            url: '/page5',
            
             views: {
                '': {
                   templateUrl: 'app/module1/page5/page5.tmpl.html',
            controller: 'm2page5Controller',
            controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module1/page5/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        });
        
         triMenuProvider.addMenu({
                   name: 'Others Analysis',
                    icon: 'zmdi zmdi-assignment-o',
                    type: 'link',
                    priority: 1.6,
                     state: 'triangular.page5',
                });
             triMenuProvider.removeMenu('triangular.page5');   
                
                
    }
})();