(function() {
    'use strict';

    angular
        .module('app.module1.page2_1')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

          $stateProvider
         .state('triangular.page2_1', {
            url: '/page2_1',
            views: {
                '': {
                   templateUrl: 'app/module1/page2_1/page2_1.tmpl.html',
                    controller: 'm2page2_1Controller',
                    controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module1/page2_1/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    },
                    permissions: {
                    only: ['viewGitHub']
                }
            
        });
        
        
         triMenuProvider.addMenu({
                    name: 'Loyalty & Advocacy',
                    icon: 'zmdi zmdi-favorite',
                    type: 'link',
                    priority: 2.0,
                     state: 'triangular.page2_1',
                });
          triMenuProvider.removeMenu('triangular.page2_1');  
                
    }
})();