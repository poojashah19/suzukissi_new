
(function () {
    'use strict';

    angular
            .module('app.module1.page2_1')
            .controller('m2page2_1Controller', m2page2_1Controller);

    /* @ngInject */
    function m2page2_1Controller($scope, service,$state, $http, $rootScope) {
        
        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776","#92D400","#00A1DE","#72C7E7","#3C8A2E","#C9DD03","#335291","#A8DD33","#33B4E5"];
        $rootScope.subpage="";
        $rootScope.subpagexs="";
        $rootScope.subpage+=" ";
        $rootScope.hideyear=false;
        $rootScope.hidewave=false;
        $rootScope.hideregion=false;
        $rootScope.hidearea=false;
        $rootScope.hidezone=false;
        $rootScope.hideprovince=false;
        $rootScope.hidecity=true;
        $rootScope.hidemodel=false;
        $rootScope.hidefactor=true;
        $rootScope.dealerhide=false;
        $rootScope.pagenumber='10';
        $rootScope.starlabelhide=true;
        $rootScope.priorityhide=true;
        $rootScope.summaryRefresh=1;
        $rootScope.modelsnap="not";
       $rootScope.checkboxInitiliser();
        
        //////////////////////////////////////////////////////////////
            var w = window.innerWidth|| document.documentElement.clientWidth|| document.body.clientWidth;
            var h = window.innerHeight|| document.documentElement.clientHeight|| document.body.clientHeight;

            console.log( " height :" , h);
            console.log( " width :" , w);
            var margin=300;
            var lineheight=25;
            $scope.xstrue=false;
            var weight=50;
            if(w < 1280 && w > 960){margin=250;}
            else if(w < 960 && w > 600){ margin=200;lineheight=12;weight=40;}
            else if(w < 600){margin=150;lineheight=12;weight=30;$scope.xstrue=true;}
            console.log('margin',margin);
        //////////////////////////////////////////////////////////////
        
        $scope.$on('changeyear', function (event, data) {
            $scope.year=data;
            $scope.filterchange();
          });
        
        $scope.$on('changewaves', function (event, data) {
            alert(data)
            $scope.waves=data;
            $scope.filterchange();
          });
          $scope.$on('changeregion', function (event, data) {
            $scope.region=data;
            $scope.area = "All";
            $scope.zone = "All";
            $scope.province = "All";
            $scope.city = "All";
            $scope.dealer="All";
            $scope.filterchange();
          });
        
         $scope.$on('changearea', function (event, data) {
            $scope.area=data;
            $scope.zone = "All";
            $scope.province = "All";
            $scope.city = "All";
            $scope.dealer="All";
            $scope.filterchange();
          });
          $scope.$on('changezone', function (event, data) {
            $scope.zone=data;
                $scope.province = "All";
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
          $scope.$on('changeprovince', function (event, data) {
            $scope.province=data;
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
          
       $scope.$on('changedealer', function (event, data) {
            $scope.dealer=data;
            $scope.filterchange();
         });
         
         $scope.$on('changemodel', function (event, data) {
            $scope.model=data;
            $scope.filterchange();
         });
         $scope.$on('changecity', function (event, data) {
            $scope.city=data;
            $scope.dealer="All";
            $scope.filterchange();
         });
        
               //////////////////////////////////////////////////////////////

        
        var parameters=[];
        $scope.area="All";
        $scope.zone="All";
        $scope.province="All";
        $scope.year=$rootScope.Filterarray.year[$rootScope.Filterarray.year.length-1];
        $scope.waves=$rootScope.Filterarray.waves[$rootScope.Filterarray.waves.length-1];
        $scope.region="All";
        $scope.model="All";
        $scope.dealer="All";
        service.ssi_filterfunction();
        parameters = [ {"name": "wave", "value": $scope.waves}];
        
        
        
        $scope.graphs=function(){  
//            SSILoyaltyAndAdvocacy-getSSILoyaltyAndAdvocacy
        service.mastersrv("SSILoyaltyAndAdvocacy-getSSILoyaltyAndAdvocacy",parameters).then(function (response) {
                
                console.log('response.data',response.data);
                 console.log('response.data.data',response.data);
                        console.log('response.data',response.data.samplecount);
                        if(response.data.samplecount < 10){
                             $scope.samplespaceless=true;
                             console.log(  $scope.samplespaceless);
                         }
                        else{
                             $scope.samplespaceless=false;
                             console.log(  $scope.samplespaceless);
                         }
                
                
                 
                 
                 
                  if(response.data.data.length==0){
                                response.data.data=[{"bartitle":"100%"},{"y":"0"}];
                            }
                 
            
            $scope.heading1=response.data.data[0].chartheading;
                $('#top1').highcharts(service.loyalitybar($scope.heading1,response.data.data[0].chartdata,false,color));
            
            $scope.heading2=response.data.data[1].chartheading;
                $('#top2').highcharts(service.loyalitybar($scope.heading2,response.data.data[1].chartdata,false,color));
            
//            $scope.heading3=response.data.data[2].chartheading;
//                $('#top3').highcharts(service.loyalitybar($scope.heading3,response.data.data[2].chartdata,false,color));
            
//            $scope.heading4=response.data.data[3].chartheading;
//                $('#bot1').highcharts(service.loyalitybar($scope.heading4,response.data.data[3].chartdata,false,color));
            
          
//            obligatory xs size
                $('#top1x').highcharts(service.loyalitybar($scope.heading1,response.data.data[0].chartdata,false,color));
                $('#top2x').highcharts(service.loyalitybar($scope.heading2,response.data.data[1].chartdata,false,color));
//                $('#top3x').highcharts(service.loyalitybar($scope.heading3,response.data.data[2].chartdata,false,color));
//                $('#bot1x').highcharts(service.loyalitybar($scope.heading4,response.data.data[3].chartdata,false,color));
                
        });
          
        }
        
        $scope.filterchange=function(){
                         parameters=[{"name":"area","value":$scope.area},{"name":"zone","value":$scope.zone},{"name":"province","value":$scope.province},{"name":"region","value":$scope.region},{"name":"year","value":$scope.year},{"name":"wave","value":$scope.waves},{"name":"model","value":$scope.model},{"name":"city","value":$scope.city},{"name":"dealer","value":$scope.dealer}];
                         $scope.graphs();
                 }
      
        
    }
})();