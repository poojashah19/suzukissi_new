(function () {
    'use strict';

    angular
            .module('app.module1.page3')
            .controller('m2page3Controller', m2page3Controller);

    /* @ngInject */
    function m2page3Controller($scope, service, $state,$http, $rootScope) {
        
        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776","#92D400","#00A1DE","#72C7E7","#3C8A2E","#C9DD03","#335291","#A8DD33","#33B4E5"];
        $rootScope.subpage="";
        $rootScope.subpagexs="";
        $rootScope.subpage+=" ";
        $rootScope.hideyear=false;
        $rootScope.hidewave=false;
        $rootScope.hideregion=false;
        $rootScope.hidearea=false;
        $rootScope.hidezone=false;
        $rootScope.hideprovince=false;
        $rootScope.hidecity=true;
        $rootScope.hidemodel=true;
        $rootScope.hidefactor=true;
        $rootScope.dealerhide=true;
        $rootScope.pagenumber='8';
        $rootScope.starlabelhide=true;
        $rootScope.priorityhide=true;
        $rootScope.summaryRefresh=1;
         $rootScope.dsnapshot="not";
         $rootScope.dealerScoreDealer=="null"
       $rootScope.checkboxInitiliser();
        $scope.region="All";
         $scope.area="All";
        $scope.zone="All";
        $scope.province="All";
         
                $scope.year='2016';
                $scope.waves = "2016 Q2";
        
        //////////////////////////////////////////////////////////////
            var w = window.innerWidth|| document.documentElement.clientWidth|| document.body.clientWidth;
            var h = window.innerHeight|| document.documentElement.clientHeight|| document.body.clientHeight;

            console.log( " height :" , h);
            console.log( " width :" , w);
            var margin=300;
            var lineheight=25;
            $scope.xstrue=false;
            var weight=50;
            if(w < 1280 && w > 960){margin=250;}
            else if(w < 960 && w > 600){ margin=200;lineheight=12;weight=40;}
            else if(w < 600){margin=150;lineheight=12;weight=30;$scope.xstrue=true;}
            console.log('margin',margin);
        //////////////////////////////////////////////////////////////
        
        $scope.$on('changeyear', function (event, data) {
            $scope.year=data;
            $scope.filterchange();
          });
        
        $scope.$on('changewaves', function (event, data) {
            $scope.waves=data;
            $scope.filterchange();
          });
          $scope.$on('changeregion', function (event, data) {
            $scope.region=data;
            $scope.area = "All";
                $scope.zone = "All";
                $scope.province = "All";
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
        
         $scope.$on('changearea', function (event, data) {
            $scope.area=data;
                $scope.zone = "All";
                $scope.province = "All";
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
          $scope.$on('changezone', function (event, data) {
            $scope.zone=data;
                $scope.province = "All";
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
          $scope.$on('changeprovince', function (event, data) {
            $scope.province=data;
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
        
              //////////////////////////////////////////////////////////////
 
        var parameters=[];
         $scope.alert_dealer=function(dealer){
            dealer = dealer.replace( /[*]/g, "" );
            $rootScope.dealerScoreDealer=dealer;
            $state.go('triangular.page11',{ dealer : '' });
             console.log('state2 params:', dealer);
        }
        
        
        $("#exportCS").click(function () {
                        $("#table2").table2excel({
                            // exclude CSS class
                            exclude: ".noExl",
                            filename: "DealerScoresDownload"
                        });
                });
        
        
//        $scope.alert_dealer=function(dealer){
//                    dealer = dealer.replace( /[*]/g, "" );
//                    $rootScope.dealerScoreDealer=dealer;
//                    $state.go('triangular.admin-default.ssi_dsnapshot-page',{ dealer : 'broken magic' });
//                }
        
         function createData() {
              service.mastersrv("SSIDealerScoreTable-getSSIDealerScoreTable",parameters).then(function (response) {
              vm.salesData=response.data;
//                       $scope.testdata = response.data;
        })

        }
        createData();
        
         $scope.filterchange=function(){
             console.log("filter change of dscore called");
                         parameters=[{"name":"area","value":$scope.area},{"name":"zone","value":$scope.zone},{"name":"province","value":$scope.province},{"name":"region","value":$scope.region},{"name":"year","value":$scope.year},{"name":"wave","value":$scope.waves}];
                         createData();
                 }
    }
})();