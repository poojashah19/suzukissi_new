(function() {
    'use strict';

    angular
        .module('app.module1.page3')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

           $stateProvider
         .state('triangular.page3', {
            url: '/page3',
            views: {
                '': {
                   templateUrl: 'app/module1/page3/page3.tmpl.html',
                    controller: 'm2page3Controller',
                    controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module1/page3/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
            
        });
        
         triMenuProvider.addMenu({
                      name: 'Dealer Score',
                    icon: 'zmdi zmdi-view-list-alt',
                    type: 'link',
                    priority: 1.7,
                     state: 'triangular.page3',
                });
             triMenuProvider.removeMenu('triangular.page3');   
                
                
    }
})();