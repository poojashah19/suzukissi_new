(function () {
    'use strict';

    angular
            .module('app.module1.page8')
            .controller('m2page8Controller', m2page8Controller);

    /* @ngInject */
    function m2page8Controller($scope,$state, service, $http, $rootScope) {
        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776","#92D400","#00A1DE","#72C7E7","#3C8A2E","#C9DD03","#335291","#A8DD33","#33B4E5"];
        $rootScope.subpage="";
        $rootScope.subpagexs="";
        $rootScope.subpage+=" ";
        $rootScope.hideyear=false;
        $rootScope.hidewave=false;
        $rootScope.hideregion=false;
        $rootScope.hidearea=true;
        $rootScope.hidezone=true;
        $rootScope.hideprovince=true;
        $rootScope.hidecity=true;
        $rootScope.hidemodel=false;
        $rootScope.hidefactor=true;
        $rootScope.dealerhide=true;
        $rootScope.pagenumber='2';
        $rootScope.legendname="Study Best";        
        $rootScope.starlabelhide=false;
        $rootScope.priorityhide=true;
        $rootScope.summaryRefresh=1;
        $rootScope.modelsnap="not";
        $rootScope.checkboxInitiliser();
        //////////////////////////////////////////////////////////////
            var w = window.innerWidth|| document.documentElement.clientWidth|| document.body.clientWidth;
            var h = window.innerHeight|| document.documentElement.clientHeight|| document.body.clientHeight;

            console.log( " height :" , h);
            console.log( " width :" , w);
            var margin=300;
            var lineheight=25;
            $scope.xstrue=false;
            var weight=50;
            if(w < 1280 && w > 960){margin=250;}
            else if(w < 960 && w > 600){ margin=200;lineheight=12;weight=40;}
            else if(w < 600){margin=150;lineheight=12;weight=30;$scope.xstrue=true;}
            console.log('margin',margin);
        
        
        
        $scope.state="All";
        $scope.year=$rootScope.Filterarray.year[$rootScope.Filterarray.year.length-1];
        $scope.waves=$rootScope.Filterarray.waves[$rootScope.Filterarray.waves.length-1];
        $scope.region="All";
        $scope.legendname="Study Best";
        $scope.model="All";
        $scope.city="All";
        $scope.factor=$rootScope.Filterarray.factor[0];
        var parameters=[{"name":"factor","value":$scope.factor}, {"name": "wave", "value": $scope.waves}];
         
         
        $scope.$on('changeyear', function (event, data) {
            console.log('console.log(timepersiod)',data);
            $scope.year=data;
            $scope.filterchange();
          });
        
        $scope.$on('changewaves', function (event, data) {
            console.log('console.log(city)',data);
            $scope.waves=data;
            $scope.filterchange();
          });
          $scope.$on('changeregion', function (event, data) {
            console.log('console.log(model)',data);
            $scope.region=data;
            $scope.filterchange();
          });
        
        $scope.$on('changemodel', function (event, data) {
            console.log('console.log(fuel)',data);
            $scope.model=data;
            $scope.filterchange();
          });
               //////////////////////////////////////////////////////////////
 
 
       
          $scope.graphs=function(){  
                    //SSIRegional-getSSIFactorAverage
                    service.mastersrv("SSIRegional-getSSIFactorAverage",parameters).then(function (response) {
                         console.log('response.data.data',response.data.data);
                        console.log('response.data',response.data.samplecount);
                        if(response.data.samplecount < 10){
                             $scope.samplespaceless=true;
                             console.log(  $scope.samplespaceless);
                         }
                        else{
                             $scope.samplespaceless=false;
                             console.log(  $scope.samplespaceless);
                         }
                         
                         if(response.data.data.length==0){
                                response.data.data=[{"bartitle":"100%"},{"Score":"0"}];
                            }
                            $scope.svgvalue=response.data.data[0].Score;
                            $scope.svgname=response.data.data[0].Factor;
                        })
                //SSIRegional-getSSIRegionalFactorAverage
                  service.mastersrv("SSIRegional-getSSIRegionalFactorAverage",parameters).then(function (response) {
                               $('#barlinecontainer').highcharts(service.regplainbar('column',color,response.data,'Factor Score',0,60,.3,[0],8,true));
                                  $('#barlinecontainer1').highcharts(service.regplainbar('column',color,response.data,'Factor Score',0,60,.3,[0],8,true));
                                  $rootScope.legendname=response.data[0].scatter;  
                  
                  });


                  //SSIRegional-getSSIRegionalAttributeAverage
                  service.mastersrv("SSIRegional-getSSIRegionalAttributeAverage",parameters).then(function (response) {
                            $('#bot1').highcharts(service.barplainbar('bar',['#2979FF'],response.data,'Attribute Score',220,35,0,[0],5,false," "));
                            $('#bot11').highcharts(service.barplainbar('bar',['#2979FF'],response.data,'Attribute Score',150,35,0,[0],5,false," "));
                    });


               //SSIRegional-getSSIDissatisfied
               service.mastersrv("SSIRegional-getSSIDissatisfied",parameters).then(function (response) {
                            $('#bot2').highcharts(service.barplainbar('bar',['#2979FF'],response.data,'Attribute Score',220,35,0,[0],5,false,"%"));
                            $('#bot21').highcharts(service.barplainbar('bar',['#2979FF'],response.data,'Attribute Score',150,35,0,[0],5,false,"%"));
                  
               });
        }
        
         $scope.isSemi = false;
            $scope.getStyle = function(){
            
                var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

                return {
                    'top': $scope.isSemi ? 'auto' : '50%',
                    'bottom': $scope.isSemi ? '5%' : 'auto',
                    'left': '50%',
                    'transform': transform,
                    '-moz-transform': transform,
                    '-webkit-transform': transform,
                    'font-size': '36px'
                };
            };
            
             $scope.getStyle1 = function(){
            
                var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

                return {
                    'top': $scope.isSemi ? 'auto' : '50%',
                    'bottom': $scope.isSemi ? '5%' : 'auto',
                    'left': '50%',
                    'transform': transform,
                    '-moz-transform': transform,
                    '-webkit-transform': transform,
                    'font-size': '36px'
                };
            };
            
            
             $scope.factorchanged=function(factor){
                    $scope.factor = factor;
                    $scope.filterchange();
                }
                
             $scope.filterchange=function(){
                         parameters=[{"name":"state","value":$scope.state},{"name":"region","value":$scope.region},{"name":"year","value":$scope.year},{"name":"wave","value":$scope.waves},{"name":"model","value":$scope.model},{"name":"city","value":$scope.city},{"name":"factor","value":$scope.factor}];
                         $scope.graphs();
                 }   
            
       
        
    }
})();