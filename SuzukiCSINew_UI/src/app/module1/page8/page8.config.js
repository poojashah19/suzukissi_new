(function() {
    'use strict';

    angular
        .module('app.module1.page8')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

          $stateProvider
          .state('triangular.page8', {
            url: '/page8',
            
             views: {
                '': {
                  templateUrl: 'app/module1/page8/page8.tmpl.html',
            controller: 'm2page8Controller',
            controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module1/page8/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        });
        
         triMenuProvider.addMenu({
                   name: 'Regional Analysis',
                    icon: 'zmdi zmdi-map',
                    type: 'link',
                    priority: 1.1,
                     state: 'triangular.page8',
                });
             triMenuProvider.removeMenu('triangular.page8');   
                
                
    }
})();