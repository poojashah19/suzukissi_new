(function () {
    'use strict';

    angular
            .module('app.module1.page4')
            .controller('m2page4Controller', m2page4Controller);

    /* @ngInject */
    function m2page4Controller($scope, service, $http, $rootScope,$timeout) {
        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776","#92D400","#00A1DE","#72C7E7","#3C8A2E","#C9DD03","#335291","#A8DD33","#33B4E5"];
        $rootScope.subpage="";
        $rootScope.subpagexs="";
        $rootScope.subpage+=" ";
        $rootScope.hideyear=false;
        $rootScope.hidewave=false;
        $rootScope.hideregion=false;
        $rootScope.hidearea=false;
        $rootScope.hidezone=false;
        $rootScope.hideprovince=false;
        $rootScope.hidecity=false;
        $rootScope.hidemodel=false;
        $rootScope.hidefactor=true;
        $rootScope.dealerhide=true;
        $rootScope.pagenumber='1';
        $rootScope.starlabelhide=false;
        $rootScope.legendname="Study Best";
        $rootScope.priorityhide=true;
        $rootScope.modelsnap="not";
          $scope.area="All";
        $scope.zone="All";
        $scope.province="All";
//        $scope.waves=$rootScope.Filterarray.waves[$rootScope.Filterarray.waves.length-1];
        $scope.year='2016';
        $scope.legendname = "Study Best";
        $scope.waves = "2016 Q2";
        $rootScope.dtwave=$scope.waves;
        $rootScope.dealerScoreDealer = 'Honda Cars Alabang';
        $scope.region = "All";
        $scope.model = "All";
        $scope.city = "All";
//      
        console.log("filters filters",$rootScope.hideyear,$rootScope.hidewave,$rootScope.hideregion,$rootScope.hidestate,$rootScope.hidecity	,$rootScope.hidemodel,$rootScope.hidefactor,$rootScope.dealerhide)
        
        //////////////////////////////////////////////////////////////
            var w = window.innerWidth|| document.documentElement.clientWidth|| document.body.clientWidth;
            var h = window.innerHeight|| document.documentElement.clientHeight|| document.body.clientHeight;

            console.log( " height :" , h);
            console.log( " width :" , w);
            var margin=300;
            var lineheight=25;
            $scope.xstrue=false;
            var weight=50;
            if(w < 1280 && w > 960){margin=250;}
            else if(w < 960 && w > 600){ margin=200;lineheight=12;weight=40;}
            else if(w < 600){margin=150;lineheight=12;weight=30;$scope.xstrue=true;}
            console.log('margin',margin);
        //////////////////////////////////////////////////////////////
        
        $scope.$on('changeyear', function (event, data) {
            $scope.year=data;
            $scope.filterchange();
          });
        
        $scope.$on('changewaves', function (event, data) {
            $scope.waves=data;
            $scope.filterchange();
          });
          $scope.$on('changeregion', function (event, data) {
            $scope.region=data;
            $scope.area = "All";
                $scope.zone = "All";
                $scope.province = "All";
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
        
        $scope.$on('changearea', function (event, data) {
            $scope.area=data;
            $scope.area = "All";
                $scope.zone = "All";
                $scope.province = "All";
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
          $scope.$on('changezone', function (event, data) {
            $scope.zone=data;
            
                $scope.province = "All";
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
          $scope.$on('changeprovince', function (event, data) {
            $scope.province=data;
            
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
          
          $scope.$on('changecity', function (event, data) {
            $scope.city=data;
            
                $scope.dealer="All";
            $scope.filterchange();
          });
          
          $scope.$on('changemodel', function (event, data) {
            $scope.model=data;
            $scope.filterchange();
          });
               //////////////////////////////////////////////////////////////
 
       
        var parameters = [];
        
      
//        $rootScope.dealerScoreDealer = $rootScope.Filterarray.dealer[0];
        $rootScope.mapState = 0;
        
         parameters = [{"name":"wave", "value": $scope.waves}];

        $scope.getStyle = function () {
             var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '50%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': $scope.radius / 3.5 + 'px'
            };
        };
         $scope.getStyle1 = function () { 
             var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '50%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': $scope.radius / 3.5 + 'px'
            };
        };
        
        
         $scope.graphs = function () {
             
//             if($rootScope.summaryRefresh == 1){
//                  $rootScope.checkboxInitiliser();
//             }
             
            //SSISummary-getNationalSSIScores
            service.mastersrv("SSISummary-getNationalSSIScores", parameters).then(function (response) {

                console.log('response.data', response.data);
                console.log('response.data.data', response.data.data);
                console.log('response.data', response.data.samplecount);
                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    console.log($scope.samplespaceless);
                }
                $scope.currmonth = response.data.data[0].Wave;
                $scope.svg1 = response.data.data[0].Score;
                $scope.prevmonth = response.data.data[1].Wave;
                $scope.svg2 = response.data.data[1].Score;
                $scope.prevquater = response.data.data[2].Wave;
                $scope.svg3 = response.data.data[2].Score;


            })

            //SSISummary-getSSISummaryRegionFactorScores
            service.mastersrv("SSISummary-getSSISummaryRegionFactorScores", parameters).then(function (response) {
                
                
                 $timeout(function () {
                 $('#barlinecontainer').highcharts(service.plainbar('column', ["#4292FF"], response.data, 'Factor Score', 0, 45, .3, [1, 1, 1, 1, 1, 1], 5, false));
                $('#barlinecontainer1').highcharts(service.plainbar('column', ["#4292FF"], response.data, 'Factor Score', 0, 45, .3, [1, 1, 1, 1, 1, 1], 5, false));

            }, 3);
               
                $rootScope.legendname = response.data[0].scatter;
            });


            //SSISummary-getSSITop5Bottom5
            service.mastersrv("SSISummary-getSSITop5Bottom5", parameters).then(function (response) {
                
                $timeout(function(){
                $('#treecontainer').highcharts(service.multbarchart(1005, 'bar', response.data, 180, 15, 0, 'SOP Score', 10, '11px', " "));
                 $('#treecontainer1').highcharts(service.multbarchart(1005, 'bar', response.data, 180, 15, 0, 'SOP Score', 10, '11px', " "));
               },3);
                console.log(response.data);
                console.log(response.data.length);
                if (response.data.length == 0) {
                    $scope.samplespaceless = true;
                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    console.log($scope.samplespaceless);
                }

            });


            //SSISummary-getSSITop5Bottom5SOP
            service.mastersrv("SSISummary-getSSITop5Bottom5SOP", parameters).then(function (response) {
                $timeout(function () {
                    $('#bot1').highcharts(service.multbarchart(105, 'bar', response.data, 250, 15, 0, 'SSI Score', 9, '8.5px', "%"));
                    $('#bot11').highcharts(service.multbarchart(105, 'bar', response.data, 180, 15, 0, 'SSI Score', 9, '8.5px', "%"));
                }, 3)

            });


            //SSISummary-RegionWiseSSI
            service.mastersrv("SSISummary-RegionWiseSSI", parameters).
                    then(function (response) {
                        $scope.mapData = response.data;
                        $scope.maprender();

                        var myData = [];
                        $scope.printdata = function () {
                            $scope.mapData.forEach(function (obj2) {
                                myData.push({region: obj2.region, score: obj2.value, repondents: obj2.repondents});
                            });
                            return myData;
                        }
                    })
            }
            
            
            
             $scope.maprender = function () {

                var latlong = {};
//                latlong["Luzon"] = {"latitude": 18.500995, "longitude": 122.000015}; //luzon
//                latlong["Visayas"] = {"latitude": 11.000995, "longitude": 125.000815}; //Visayas
                latlong["Provincial"] = {"latitude": 8.000099, "longitude": 124.000063}; //Mindano
                latlong["Metro Manila"] = {"latitude": 13.500084, "longitude": 120.500056};//Manila




                var mapcolor = ["#92D400", "#33B4E5",
                    "#4200d4",
                    "#c3d327", "#3C8A2E",
                    "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];

                var map;
                var minBulletSize = 30;
                var maxBulletSize = 60;
                var min = Infinity;
                var max = -Infinity;

                for (var i = 0; i < $scope.mapData.length; i++) {
                    var value = $scope.mapData[i].value;
                    if (value < min) {
                        min = value;
                    }
                    if (value > max) {
                        max = value;
                    }


                }

                if (min == max) {
                    min = 0;
                    max = value;
                }


                AmCharts.ready = function () {
                    map = new AmCharts.AmMap();
                    map.pathToImages = "http://www.amcharts.com/lib/3/images/";
                    map.panEventsEnabled = false;
                    map.dragMap=false;
                    map.areasSettings = {
                        selectable: true,
                        unlistedAreasColor: "#EFEFEF",
                        unlistedAreasOutlineAlpha: .9,
                        unlistedAreasOutlineColor: "#7F7F7F",
                    };
                    map.imagesSettings.balloonText = "<span style='font-size:14px;' align='left'>[[title]]</span>";

                    map.addListener("clickMapObject", function (event) {
                        console.log(event.mapObject.code);
                        console.log(event.mapObject.value);
                        if ($rootScope.mapState == 0) {
                            $scope.region = event.mapObject.code;
                            $scope.filterchange();
                            $rootScope.mapState = 1;
                        } else {
                            $scope.region = "All";
                            $scope.filterchange();
                            $rootScope.mapState = 0;
                        }


                    });


                    var dataProvider = {
                        mapVar: AmCharts.maps.philippinesHigh,
    //                       
                        images: []
                    }

                    map.zoomControl = {
                        panControlEnabled: false,
                        zoomControlEnabled: false,
                        homeButtonEnabled: false
                    };


                    // it's better to use circle square to show difference between values, not a radius
                    var maxSquare = maxBulletSize * maxBulletSize * 2 * Math.PI;
                    var minSquare = minBulletSize * minBulletSize * 2 * Math.PI;


                    // create circle for each country
                    for (var i = 0; i < $scope.mapData.length; i++) {
                        var dataItem = $scope.mapData[i];
                        var value = dataItem.value;
                        var datacolor = mapcolor[i];

                        // calculate size of a bubble   

                        var square = ((value - min) / (max - min) * (maxSquare - minSquare)) + minSquare;
                        if (square < minSquare) {
                            square = minSquare;
                        }
                        var size = Math.sqrt(square / (Math.PI * 2));
                        var id = dataItem.code;

    //                   console.log("size"+size);
    //                   console.log("SQUARE"+square);
    //                   console.log("MINSQUARE"+minSquare);
    //                    console.log("MINBULLETSIZE"+minBulletSize);
    //                   console.log("VALUE"+value);

                        dataProvider.images.push({
                            type: "bubble",
                            width: size,
                            height: size,
                            color: datacolor,
                            longitude: latlong[id].longitude,
                            latitude: latlong[id].latitude,
                            label: (value),
                            alpha: .30,
                            code: dataItem.code,
                            labelPosition: "inside",
                            title: dataItem.name,
                            value: value,
                            rollOverScale: "1.3",
//                            outlineColor: "#104E8B",
                            outlineAlpha: ".5",
                            outlineThickness: "1",
                            selectable: true,
                            bringForwardOnHover: false,
                            selectedOutlineThickness: "5"

                        });
                    }

                    map.dataProvider = dataProvider;
                    map.write("map");
                };
                
                
                 AmCharts.ready1 = function () {
                    map = new AmCharts.AmMap();
                    map.pathToImages = "http://www.amcharts.com/lib/3/images/";
                    map.panEventsEnabled = false;
                    map.dragMap=false;
                    map.areasSettings = {
                        selectable: true,
                        unlistedAreasColor: "#EFEFEF",
                        unlistedAreasOutlineAlpha: .9,
                        unlistedAreasOutlineColor: "#7F7F7F",
                    };
                    map.imagesSettings.balloonText = "<span style='font-size:14px;' align='left'>[[title]]</span>";

                    map.addListener("clickMapObject", function (event) {
                        console.log(event.mapObject.code);
                        console.log(event.mapObject.value);
                        if ($rootScope.mapState == 0) {
                            $scope.region = event.mapObject.code;
                            $scope.filterchange();
                            $rootScope.mapState = 1;
                        } else {
                            $scope.region = "All";
                            $scope.filterchange();
                            $rootScope.mapState = 0;
                        }


                    });
                    var dataProvider = {
                        mapVar: AmCharts.maps.philippinesHigh,
                        images: []
                    }
                    map.zoomControl = {
                        panControlEnabled: false,
                        zoomControlEnabled: false,
                        homeButtonEnabled: false
                    };
                    var maxSquare = maxBulletSize * maxBulletSize * 2 * Math.PI;
                    var minSquare = minBulletSize * minBulletSize * 2 * Math.PI;
                    for (var i = 0; i < $scope.mapData.length; i++) {
                        var dataItem = $scope.mapData[i];
                        var value = dataItem.value;
                        var datacolor = mapcolor[i];
                        var square = ((value - min) / (max - min) * (maxSquare - minSquare)) + minSquare;
                        if (square < minSquare) {
                            square = minSquare;
                        }
                        var size = Math.sqrt(square / (Math.PI * 2));
                        var id = dataItem.code;
                        dataProvider.images.push({
                            type: "bubble",
                            width: size,
                            height: size,
                            color: datacolor,
                            longitude: latlong[id].longitude,
                            latitude: latlong[id].latitude,
                            label: (value),
                            alpha: .30,
                            code: dataItem.code,
                            labelPosition: "inside",
                            title: dataItem.name,
                            value: value,
                            rollOverScale: "1.3",
//                            outlineColor: "#104E8B",
                            outlineAlpha: ".5",
                            outlineThickness: "1",
                            selectable: true,
                            bringForwardOnHover: false,
                            selectedOutlineThickness: "5"
                        });
                    }

                    map.dataProvider = dataProvider;
                    map.write("map1");
                };
                AmCharts.ready();
                 AmCharts.ready1();
        }
       
        $scope.filterchange = function () {
            parameters = [{"name":"area","value":$scope.area},{"name":"zone","value":$scope.zone},{"name":"province","value":$scope.province}, {"name": "region", "value": $scope.region}, {"name": "year", "value": $scope.year}, {"name": "wave", "value": $scope.waves}, {"name": "model", "value": $scope.model}, {"name": "city", "value": encodeURIComponent($scope.city)}];
            $scope.graphs();
//            $scope.graphs();
        }
        
    }
})();

