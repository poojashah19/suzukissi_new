(function() {
    'use strict';

    angular
        .module('app.module1.page11')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

          $stateProvider
          .state('triangular.page11', {
            url: '/page11',
             views: {
                '': {
                   templateUrl: 'app/module1/page11/page11.tmpl.html',
            controller: 'm2page11Controller',
            controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module1/page11/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        });
        
         triMenuProvider.addMenu({
                   name: 'Dealer Snapshot',
                    icon: 'zmdi zmdi-picture-in-picture',
                    type: 'link',
                    priority: 1.8,
                     state: 'triangular.page11',
                });
                
             triMenuProvider.removeMenu('triangular.page11');   
                
    }
})();