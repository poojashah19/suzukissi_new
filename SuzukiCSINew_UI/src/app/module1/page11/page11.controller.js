(function () {
    'use strict';

    angular
            .module('app.module1.page11')
            .controller('m2page11Controller', m2page11Controller);

    /* @ngInject */
    function m2page11Controller($scope,$state, service, $http, $rootScope) {
        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776","#92D400","#00A1DE","#72C7E7","#3C8A2E","#C9DD03","#335291","#A8DD33","#33B4E5"];
        $rootScope.subpage="";
        $rootScope.subpagexs="";
        $rootScope.subpage+=" ";
        $rootScope.hideyear=false;
        $rootScope.hidewave=false;
        $rootScope.hideregion=false;
        $rootScope.hidearea=false;
        $rootScope.hidezone=false;
        $rootScope.hideprovince=false;
        $rootScope.hidecity=true;
        $rootScope.hidemodel=true;
        $rootScope.hidefactor=false;
        $rootScope.dealerhide=false;
        $rootScope.pagenumber='9';
        $rootScope.starlabelhide=true;
        $rootScope.priorityhide=true;
        $rootScope.summaryRefresh=1;
        $rootScope.dsnapshot="dsnap";
         $rootScope.checkboxInitiliser();
        //////////////////////////////////////////////////////////////
            var w = window.innerWidth|| document.documentElement.clientWidth|| document.body.clientWidth;
            var h = window.innerHeight|| document.documentElement.clientHeight|| document.body.clientHeight;

            console.log( " height :" , h);
            console.log( " width :" , w);
            var margin=300;
            var lineheight=25;
            $scope.xstrue=false;
            var weight=50;
            if(w < 1280 && w > 960){margin=250;}
            else if(w < 960 && w > 600){ margin=200;lineheight=12;weight=40;}
            else if(w < 600){margin=150;lineheight=12;weight=30;$scope.xstrue=true;}
            console.log('margin',margin);
        
        $scope.$on('changeyear', function (event, data) {
            $scope.year=data;
            $scope.filterchange();
          });
          
          $scope.$on('changefactor', function (event, data) {
            $scope.factor=data;
            $scope.filterchange();
          });
        
        $scope.$on('changewaves', function (event, data) {
            $scope.waves=data;
            $scope.filterchange();
          });
          $scope.$on('changeregion', function (event, data) {
            $scope.region=data;
            $scope.area = "All";
                $scope.zone = "All";
                $scope.province = "All";
                $scope.city = "All";
                $scope.dealer=$rootScope.Filterarray.dealer[0];
            $scope.filterchange();
          });
        
        $scope.$on('changearea', function (event, data) {
            $scope.area=data;
                $scope.zone = "All";
                $scope.province = "All";
                $scope.city = "All";
                $scope.dealer=$rootScope.Filterarray.dealer[0];
            $scope.filterchange();
          });
          $scope.$on('changezone', function (event, data) {
            $scope.zone=data;
                $scope.province = "All";
                $scope.city = "All";
                $scope.dealer=$rootScope.Filterarray.dealer[0];
            $scope.filterchange();
          });
          $scope.$on('changeprovince', function (event, data) {
            $scope.province=data;
                $scope.city = "All";
                $scope.dealer=$rootScope.Filterarray.dealer[0];
            $scope.filterchange();
          });
          
       $scope.$on('changedealer', function (event, data) {
            $scope.dealer=data;
            $scope.filterchange();
         });
               //////////////////////////////////////////////////////////////
 
            $scope.area="All";
            $scope.zone="All";
            $scope.province="All";
            $scope.legendname="Study Best";
            $scope.year=$rootScope.Filterarray.year[$rootScope.Filterarray.year.length-1];
            $scope.waves=$rootScope.Filterarray.waves[$rootScope.Filterarray.waves.length-1];
            $scope.region="All";
            $scope.dealer="Honda Cars Alabang";
            $scope.factor=$rootScope.Filterarray.factor[0];
//            var parameters=[{"name":"dealer","value":$scope.dealer}];
            var parameters=[{"name":"dealer","value":$scope.dealer}, {"name": "wave", "value": $scope.waves}];
            var regionobj=[{"name": "state", "value": "All"},{"name":"region","value":"All"}];
                             service.mastersrv("SSIFilters-getDealers",regionobj).then(function (result) {
                                 $rootScope.Filterarray.dealer=result.data;
                            });
                    
            $scope.dealer=$rootScope.dealerScoreDealer;               
             
            $scope.graphs=function(){
                console.log(parameters);
                //SSIDealerSnapshot-getDealerSSIScores
                service.mastersrv("SSIDealerSnapshot-getDealerSSIScores",parameters).then(function (response) {    
                 console.log('response.data',response.data);
                 console.log('response.data.data',response.data.data);
                        console.log('response.data',response.data.samplecount);
                        if(response.data.samplecount < 10){
                             $scope.samplespaceless=true;
                             console.log(  $scope.samplespaceless);
                         }
                        else{
                             $scope.samplespaceless=false;
                             console.log(  $scope.samplespaceless);
                         }
                
                
                    $scope.currmonth=response.data.data[0].Wave;
                    $scope.svg1=response.data.data[0].Score;
                    $scope.prevmonth=response.data.data[1].Wave;
                    $scope.svg2=response.data.data[1].Score;
                     if(!response.data.data[1].Wave){
                            $scope.prevmonth = 0;
                            $scope.svg2 = 0;
               
                       }
                    $scope.prevquater=response.data.data[2].Wave;
                    $scope.svg3=response.data.data[2].Score;
                })
                
//                SSIDealerSnapshot-getSSIDealerFactorScores
                 service.mastersrv("SSIDealerSnapshot-getSSIDealerFactorScores",parameters).then(function (response) {
                         $('#top1').highcharts(service.plainbar('column',['#2979FF'],response.data,'Factor Score',0,35,.2,[0],3,false));
                          $('#top11').highcharts(service.plainbar('column',['#2979FF'],response.data,'Factor Score',0,35,.2,[0],3,false));
                             $scope.legendname=response.data[0].scatter;
                 
                 });
              //  SSIDealerSnapshot-getSSIDealerSOPTopBottom
                service.mastersrv("SSIDealerSnapshot-getSSIDealerSOPTopBottom",parameters).then(function (response) {
                         $('#top2').highcharts(service.multbarchart(105,'bar',response.data,180,15,10,'Attribute Score',9,'8.5px',"%"));
                 $('#top21').highcharts(service.multbarchart(105,'bar',response.data,180,15,10,'Attribute Score',9,'8.5px',"%"));
                  });                               

                //SSIDealerSnapshot-getSSIDealerFactorTrend
                service.mastersrv("SSIDealerSnapshot-getSSIDealerFactorTrend",parameters).then(function (response) {
                         $('#bot1').highcharts(service.barchart('column','#002776',response.data,0,35,10,'Attribute Score'));
                  $('#bot11').highcharts(service.barchart('column','#002776',response.data,0,35,10,'Attribute Score'));
                  });

               // SSIDealerSnapshot-getSSIDealerAttributeTrend
//                service.mastersrv("SSIDealerSnapshot-getSSIDealerAttributeTrend",parameters).then(function (response) {
//                        $('#bot2').highcharts(service.dsmultigraph(response.data,false,color));
////                         console.log(response.data);
////                         console.log(response.data[0].data);
//                       $('#bot21').highcharts(service.dsmultigraph(response.data,false,color));
//                });
                service.mastersrv("SSIDealerSnapshot-getSSIDealerAttributeTrend",parameters).then(function (response) {
                         $('#bot2').highcharts(service.multigraph(response,false,color));
                         $('#bot21').highcharts(service.multigraph(response,false,color));
                });

            
        }
       
       
       
        $scope.getStyle = function(){
                var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

                return {
                    'top': $scope.isSemi ? 'auto' : '50%',
                    'bottom': $scope.isSemi ? '5%' : 'auto',
                    'left': '50%',
                    'transform': transform,
                    '-moz-transform': transform,
                    '-webkit-transform': transform,
                    'font-size': $scope.radius/3.5 + 'px'
                };
            };
            
            $scope.getStyle1 = function(){
                var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

                return {
                    'top': $scope.isSemi ? 'auto' : '50%',
                    'bottom': $scope.isSemi ? '5%' : 'auto',
                    'left': '50%',
                    'transform': transform,
                    '-moz-transform': transform,
                    '-webkit-transform': transform,
                    'font-size': $scope.radius/3.5 + 'px'
                };
            };
            
            
             $scope.filterchange=function(){
                         parameters=[{"name":"year","value":$scope.year},{"name":"wave","value":$scope.waves},{"name":"dealer","value":$scope.dealer},{"name":"factor","value":$scope.factor}];
                     console.log("dealer" + $scope.dealer);   
                     $scope.graphs();
                 }
        
    }
})();