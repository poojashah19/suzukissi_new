(function () {
    'use strict';

    angular
            .module('app.module1.page6')
            .controller('m2page6Controller', m2page6Controller);

    /* @ngInject */
    function m2page6Controller($scope,$state, service, $http, $rootScope) {
        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776","#92D400","#00A1DE","#72C7E7","#3C8A2E","#C9DD03","#335291","#A8DD33","#33B4E5"];
        $rootScope.subpage="";
        $rootScope.subpagexs="";
        $rootScope.subpage+=" ";
        $rootScope.hideyear=false;
        $rootScope.hidewave=false;
        $rootScope.hideregion=false;
        $rootScope.hidearea=false;
        $rootScope.hidezone=false;
        $rootScope.hideprovince=false;
        $rootScope.hidecity=false;
        $rootScope.hidemodel=false;
        $rootScope.hidefactor=false;
        $rootScope.dealerhide=true;
        $rootScope.pagenumber='10';
        $rootScope.starlabelhide=true;
        $rootScope.priorityhide=true;
        $rootScope.summaryRefresh=1;
         $rootScope.dsnapshot="not";
         $rootScope.dealerScoreDealer=="null"
         $rootScope.modelsnap="modelsnap";
       $rootScope.checkboxInitiliser();
        
        //////////////////////////////////////////////////////////////
            var w = window.innerWidth|| document.documentElement.clientWidth|| document.body.clientWidth;
            var h = window.innerHeight|| document.documentElement.clientHeight|| document.body.clientHeight;

            console.log( " height :" , h);
            console.log( " width :" , w);
            var margin=300;
            var lineheight=25;
            $scope.xstrue=false;
            var weight=50;
            if(w < 1280 && w > 960){margin=250;}
            else if(w < 960 && w > 600){ margin=200;lineheight=12;weight=40;}
            else if(w < 600){margin=150;lineheight=12;weight=30;$scope.xstrue=true;}
            console.log('margin',margin);
        
        $scope.$on('changeyear', function (event, data) {
            $scope.year=data;
            $scope.filterchange();
          });
        
        $scope.$on('changewaves', function (event, data) {
            $scope.waves=data;
            $scope.filterchange();
          });
          $scope.$on('changeregion', function (event, data) {
            $scope.region=data;
            $scope.area = "All";
                $scope.zone = "All";
                $scope.province = "All";
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
        
        $scope.$on('changearea', function (event, data) {
            $scope.area=data;
                $scope.zone = "All";
                $scope.province = "All";
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
          $scope.$on('changezone', function (event, data) {
            $scope.zone=data;
                $scope.province = "All";
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
          $scope.$on('changeprovince', function (event, data) {
            $scope.province=data;
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
          
       $scope.$on('changemodel', function (event, data) {
            $scope.model=data;
            $scope.botgraph();
         });
         
         $scope.$on('changecity', function (event, data) {
            $scope.city=data;
                $scope.dealer="All";
            $scope.filterchange();
         });
         
         $scope.$on('changefactor', function (event, data) {
            $scope.factor=data;
            $scope.botgraph();
         });
               //////////////////////////////////////////////////////////////
 
        $("#modelexportCS").click(function () {
                        $("#table").table2excel({
                            // exclude CSS class
                            exclude: ".noExl",
                            filename: "modelTableDownload"
                        });
                });
        
       
        var botparameters=[];
        var parameters=[];
        $scope.area="All";
        $scope.zone="All";
        $scope.province="All";
        $scope.year=$rootScope.Filterarray.year[$rootScope.Filterarray.year.length-1];
        $scope.waves=$rootScope.Filterarray.waves[$rootScope.Filterarray.waves.length-1];
        $scope.region="All";
        $scope.city="All";
        $scope.factor=$rootScope.Filterarray.factor[0];
        $scope.model=$rootScope.Filterarray.model[0];
//        $scope.model="All";
         botparameters=[{"name":"area","value":$scope.area},{"name":"zone","value":$scope.zone},{"name":"province","value":$scope.province},{"name":"region","value":$scope.region},{"name":"year","value":$scope.year},{"name":"wave","value":$scope.waves},{"name":"city","value":encodeURIComponent($scope.city)},{"name":"model","value":$rootScope.Filterarray.model[0]},{"name":"factor","value":$scope.factor}];
             service.ssi_filterfunction();
             
           parameters = [ {"name": "wave", "value": $scope.waves}];   
        
     $scope.graphs=function(){   
         
         //        SSIModelAnalysis-getModelSSIScores 1
                 service.mastersrv("SSIModelAnalysis-getModelSSIScores",parameters).then(function (response) {
                                $('#leftgraph').highcharts(service.barchart('bar','#2979FF ',response.data,100,40,0,'Avg SSI'));
                    });
                     service.mastersrv("SSIModelAnalysis-getModelSSIScores",parameters).then(function (response) {
                                $('#leftgraph1').highcharts(service.barchart('bar','#2979FF ',response.data,100,20,0,'Avg SSI'));
                    });
                    
         //        SSIModelAnalysis-getModelFactorSSIScores  2
                service.mastersrv("SSIModelAnalysis-getModelFactorSSIScores",parameters).then(function (response) {
                                 vm.salesData = response.data;
            
                    });
//         $scope.botrender();
//            
        }
        
        
        $scope.botrender=function(){    
            
       //        SSIModelAnalysis-getModelFactorTrend 3
                 service.mastersrv("SSIModelAnalysis-getModelFactorTrend",botparameters).then(function (response) {
                                
                        $('#botgraph').highcharts(service.barchart('column','#92D400 ',response.data.data, 0,40,0,'Model Score',12,1,1005));
                     $('#botgraph1').highcharts(service.barchart('column','#92D400 ',response.data.data, 0,40,0,'Model Score',12,1,1005));
                             
                                console.log('response.data',response.data);
                                console.log('response.data.data',response.data.data);
                                console.log('response.data.totalcount',response.data.totalcount);
                                if(response.data.totalcount < 10){
                                     $scope.modelname=$scope.model+"**";
                                 }
                                else if(response.data.totalcount <= 30 && response.data.totalcount >= 10){
                                     $scope.modelname=$scope.model+"*";
                                 }
                                else{
                                     $scope.modelname=$scope.model;
                                     console.log(  $scope.samplespaceless);
                                 }
                    });
        }
        
        
        $scope.filterchange=function(){
                 parameters=[{"name":"area","value":$scope.area},{"name":"zone","value":$scope.zone},{"name":"province","value":$scope.province},{"name":"region","value":$scope.region},{"name":"year","value":$scope.year},{"name":"wave","value":$scope.waves},{"name":"city","value":encodeURIComponent($scope.city)}];
                 $scope.graphs();
         }
         
          $scope.botgraph=function(){
                 botparameters=[{"name":"area","value":$scope.area},{"name":"zone","value":$scope.zone},{"name":"province","value":$scope.province},{"name":"region","value":$scope.region},{"name":"year","value":$scope.year},{"name":"wave","value":$scope.waves},{"name":"city","value":encodeURIComponent($scope.city)},{"name":"model","value":$scope.model},{"name":"factor","value":$scope.factor}];
                 $scope.botrender();
         }
        
        
        
        
    }
})();