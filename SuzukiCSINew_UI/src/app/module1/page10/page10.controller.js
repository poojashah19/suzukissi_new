(function () {
    'use strict';

    angular
            .module('app.module1.page10')
            .controller('m2page10Controller', m2page10Controller);

    /* @ngInject */
    function m2page10Controller($scope,$state, service, $http, $rootScope) {
        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776","#92D400","#00A1DE","#72C7E7","#3C8A2E","#C9DD03","#335291","#A8DD33","#33B4E5"];
        $rootScope.subpage="";
        $rootScope.subpagexs="";
        $rootScope.subpage+=" ";
        $rootScope.hideyear=false;
        $rootScope.hidewave=false;
        $rootScope.hideregion=false;
        $rootScope.hidearea=false;
        $rootScope.hidezone=false;
        $rootScope.hideprovince=false;
        $rootScope.hidecity=true;
        $rootScope.hidemodel=true;
        $rootScope.hidefactor=true;
        $rootScope.dealerhide=false;
        $rootScope.pagenumber='5';
        $rootScope.starlabelhide=true;
        $rootScope.priorityhide=true;
        $rootScope.summaryRefresh=1;
        
         $rootScope.dsnapshot="not";
         $rootScope.checkboxInitiliser();
        //////////////////////////////////////////////////////////////
            var w = window.innerWidth|| document.documentElement.clientWidth|| document.body.clientWidth;
            var h = window.innerHeight|| document.documentElement.clientHeight|| document.body.clientHeight;

            console.log( " height :" , h);
            console.log( " width :" , w);
            var margin=300;
            var lineheight=25;
            $scope.xstrue=false;
            var weight=50;
            if(w < 1280 && w > 960){margin=250;}
            else if(w < 960 && w > 600){ margin=200;lineheight=12;weight=40;}
            else if(w < 600){margin=180;lineheight=12;weight=30;$scope.xstrue=true;}
            console.log('margin',margin);
        
        $scope.$on('changeyear', function (event, data) {
            $scope.year=data;
            $scope.filterchange();
          });
        
        $scope.$on('changewaves', function (event, data) {
            $scope.waves=data;
            $scope.filterchange();
          });
          $scope.$on('changeregion', function (event, data) {
            $scope.region=data;
            $scope.area = "All";
                $scope.zone = "All";
                $scope.province = "All";
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
        
        $scope.$on('changearea', function (event, data) {
            $scope.area=data;
            
                $scope.zone = "All";
                $scope.province = "All";
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
          $scope.$on('changezone', function (event, data) {
            $scope.zone=data;
            
                $scope.province = "All";
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
          $scope.$on('changeprovince', function (event, data) {
            $scope.province=data;
            
            
                $scope.city = "All";
                $scope.dealer="All";
            $scope.filterchange();
          });
          
      $scope.$on('changedealer', function (event, data) {
        $scope.dealer=data;
        $scope.filterchange();
      });
               //////////////////////////////////////////////////////////////
 
       
       vm.sopwswitch=sopswitch;
       $scope.area="All";
        $scope.zone="All";
        $scope.province="All";
        $scope.year=$rootScope.Filterarray.year[$rootScope.Filterarray.year.length-1];
        $scope.waves=$rootScope.Filterarray.waves[$rootScope.Filterarray.waves.length-1];
        $scope.region="All";
        $scope.model="All";
        $scope.dealer="All";
        $scope.sopswitchoutput='study';
        $scope.sopswitchoutputcaps='Study';
        var parameters=[{"name":"compareto","value":$scope.sopswitchoutput},{"name": "wave", "value": $scope.waves}];
        
          $scope.group1="Total";
        
        function sopswitch(flag){
            if(flag == 'Total'){
                $scope.sopswitchoutput='total';$scope.sopswitchoutputcaps='Total';
            }
            else if(flag == 'Zone'){
                $scope.sopswitchoutput='zone';$scope.sopswitchoutputcaps='Zone';
            }
            else if(flag == 'Region'){
                $scope.sopswitchoutput='region';$scope.sopswitchoutputcaps='Region';
            }
            
            $scope.filterchange();
        }
        
        
        
        
        $scope.graphs=function(){
                   
    //        SSISOPPerformance-getSSISOPPerformance&compareto=region
              service.mastersrv("SSISOPPerformance-getSSISOPPerformance",parameters).then(function (response) {
//                 
                 console.log('response.data',response.data);
                        console.log('response.data.data',response.data.data);
                        console.log('response.data.samplecount',response.data.samplecount);
                        if(response.data.samplecount < 10){
                             $scope.samplespaceless=true;
                             console.log(  $scope.samplespaceless);
                         }
                        else{
                             $scope.samplespaceless=false;
                             console.log(  $scope.samplespaceless);
                         }
                  if(response.data.data.length==0){
                                response.data.data=[{"bartitle":"100%"},{"y":"0"}];
                            }
                    
                           
                      $('#sop').highcharts(service.sopplainbar('bar','11px',color,response.data.data,'Attribute Score',margin,15,0,[0],5,false));
                        $('#sop1').highcharts(service.sopplainbar('bar','9px',color,response.data.data,'Attribute Score',margin,25,0,[0],5,false));
                          
              });
          }
          
           $scope.filterchange=function(){
//             $scope.switchdisable=false;
                 parameters=[{"name":"compareto","value":$scope.sopswitchoutput},{"name":"area","value":$scope.area},{"name":"zone","value":$scope.zone},{"name":"province","value":$scope.province},{"name":"region","value":$scope.region},{"name":"year","value":$scope.year},{"name":"wave","value":$scope.waves},{"name":"model","value":$scope.model},{"name":"dealer","value":$scope.dealer}];
                
             $scope.graphs();
         }
    }
})();