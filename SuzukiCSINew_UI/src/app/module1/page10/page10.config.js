(function() {
    'use strict';

    angular
        .module('app.module1.page10')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

          $stateProvider
          .state('triangular.page10', {
            url: '/page10',
            views: {
                '': {
                   templateUrl: 'app/module1/page10/page10.tmpl.html',
            controller: 'm2page10Controller',
            controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module1/page10/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        });
        
         triMenuProvider.addMenu({
                   name: 'SOP Performance',
                    icon: 'zmdi zmdi-collection-text',
                    type: 'link',
                    priority: 1.4,
                     state: 'triangular.page10',
                });
             triMenuProvider.removeMenu('triangular.page10');   
                
    }
})();