(function() {
    'use strict';

    angular
        .module('app')
        .run(runFunction);

    /* @ngInject */
    function runFunction($rootScope, $state,$window) {

        // default redirect if access is denied
        function redirectError() {
             $state.go('authentication.login', {});
              $window.location.reload(true);
        }

        // watches

        // redirect all errors to permissions to 500
        var errorHandle = $rootScope.$on('$stateChangeError', redirectError);
        
//        var windowElement = angular.element($window);
//        windowElement.on('beforeunload', function (event) {
//           //Do Something
//            console.log("page refreshed");
//           //After this will prevent reload or navigating away.
////           event.preventDefault();
//        });
        
        
          $window.onbeforeunload = function (e) {
             $state.go('authentication.login', {});
              $window.location.reload(true);
        }
        
        // remove watch on destroy
        $rootScope.$on('$destroy', function() {
             $state.go('authentication.login', {});
              $window.location.reload(true);
            errorHandle();
        });
    }
})();
