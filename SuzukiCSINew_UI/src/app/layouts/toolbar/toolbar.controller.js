(function() {
    'use strict';

    angular
        .module('triangular.components')
        .controller('ToolbarController', DefaultToolbarController);

    /* @ngInject */
    function DefaultToolbarController($scope,$mdDialog, $injector, $rootScope, $mdMedia, $state, $element, $filter, $mdUtil, $mdSidenav, $mdToast, $timeout, $document,$window, triBreadcrumbsService, triSettings, triLayout,service) {
        var vm = this;
        vm.breadcrumbs = triBreadcrumbsService.breadcrumbs;
        vm.emailNew = false;
        vm.languages = triSettings.languages;
        vm.openSideNav = openSideNav;
        vm.hideMenuButton = hideMenuButton;
        vm.languageswitch = languageswitch;
        vm.toggleNotificationsTab = toggleNotificationsTab;
        vm.isFullScreen = false;
        vm.fullScreenIcon = 'zmdi zmdi-fullscreen';
        vm.toggleFullScreen = toggleFullScreen;
        $scope.toplineTooltip='Topline Download';
        $scope.informationTooltip='Information';
        $scope.logoutTooltip='Logout';
        $scope.namer=$rootScope.nameit;
//        vm.currentUser = UserService.getCurrentUser();
        
        ////////////////
        
        console.log('$rootScope.oemDownloadShow',$rootScope.oemDownloadShow);
        console.log('$rootScope.oemDownloadShow',$rootScope.oemDownloadShow);
        
        
        function openSideNav(navID) {
            $mdUtil.debounce(function(){
                $mdSidenav(navID).toggle();
            }, 300)();
        }
        
        
         function languageswitch(languageselection) {
             
             var modelpara=[{"name":"english","value":$scope.namer}];
             
              if(languageselection){
                  
                    $rootScope.englishshow=false;
                    $rootScope.thaishow=true;
                    $rootScope.language="TH";
                    $scope.radio=$rootScope.language;
                    $scope.toplineTooltip='รายงานเบื้องต้น ดาวน์โหลดไฟล์';
                    $scope.informationTooltip='ข้อมูลการศึกษา';
                    $scope.logoutTooltip='ออกจากระบบ';
                    $rootScope.downloadpng="ดาวน์โหลดไฟล์รูปภาพ PNG";
                    $rootScope.downloadcsv="ดาวน์โหลดไฟล์ CSV";
                    $rootScope.downloadxls="ดาวน์โหลดไฟล์ XLS";
                    $rootScope.downloadheader="เมนูเลือกการแสดงแผนภูมิ (Chart)";
                    $rootScope.$broadcast("changeLanguage",$scope.radio);
              }
              else{
                  
                    $rootScope.englishshow=true;
                    $rootScope.thaishow=false;
                    $rootScope.language="EN";
                    $scope.toplineTooltip='Topline Download';
                    $scope.informationTooltip='Information';
                    $scope.logoutTooltip='Logout';
                    $rootScope.downloadpng="Download PNG Image";
                    $rootScope.downloadcsv="Download CSV";
                    $rootScope.downloadxls="Download XLS";
                    $rootScope.downloadheader="Chart Context Menu";
                    $scope.radio=$rootScope.language;
                    $rootScope.$broadcast("changeLanguage",$scope.radio);
              }
              if($scope.namer != 'CSI'){
                    service.mastersrv("Systemuserview-getName",modelpara).then(function (response) {
                          console.log("languageselection",languageselection);
                          console.log("languageselection",languageselection);
                          console.log('response response[0]',response.data[0].ContentReganal);
                          if($rootScope.language == 'EN'){
                              $rootScope.nameit=response.data[0].ContentEN;
                          }
                          else{
                              $rootScope.nameit=response.data[0].ContentReganal;
                          }
                    });
                }
                else{
                    $scope.namer=$rootScope.nameit;
                }
        }
        
        
        

       

        function hideMenuButton() {
            switch(triLayout.layout.sideMenuSize) {
                case 'hidden':
                    // always show button if menu is hidden
                    return false;
                case 'off':
                    // never show button if menu is turned off
                    return true;
                default:
                    // show the menu button when screen is mobile and menu is hidden
                    return $mdMedia('gt-sm');
            }
        }

        function toggleNotificationsTab(tab) {
            $rootScope.$broadcast('triSwitchNotificationTab', tab);
            vm.openSideNav('notifications');
        }
        
         $scope.refresh=function(){
          $state.go('authentication.login', {});
          $rootScope.sessionid = 0;
//           
//            alert('$rootScope.logou192.168.0.106')
//            $window.location.href = "http://serviceretention.leadicsapps.com//Login/#/login";
//            console.log('$rootScope.logout','192.168.0.106');
//            console.log('$rootScope.logout','192.168.0.106');
//          $window.location.reload(true);
         };

        function toggleFullScreen() {
            vm.isFullScreen = !vm.isFullScreen;
            vm.fullScreenIcon = vm.isFullScreen ? 'zmdi zmdi-fullscreen-exit':'zmdi zmdi-fullscreen';
            // more info here: https://developer.mozilla.org/en-US/docs/Web/API/Fullscreen_API
            var doc = $document[0];
            if (!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement ) {
                if (doc.documentElement.requestFullscreen) {
                    doc.documentElement.requestFullscreen();
                } else if (doc.documentElement.msRequestFullscreen) {
                    doc.documentElement.msRequestFullscreen();
                } else if (doc.documentElement.mozRequestFullScreen) {
                    doc.documentElement.mozRequestFullScreen();
                } else if (doc.documentElement.webkitRequestFullscreen) {
                    doc.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
                }
            } else {
                if (doc.exitFullscreen) {
                    doc.exitFullscreen();
                } else if (doc.msExitFullscreen) {
                    doc.msExitFullscreen();
                } else if (doc.mozCancelFullScreen) {
                    doc.mozCancelFullScreen();
                } else if (doc.webkitExitFullscreen) {
                    doc.webkitExitFullscreen();
                }
            }
        }
        
        
        
        function DialogController($rootScope, $scope, $mdDialog) {
            $scope.helpimage2 = "$rootScope.helpimage";
            $scope.newlink="new downloadss";
            $scope.downloaddata=$rootScope.Filterarray.download;
            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };
            $scope.downloadfunction=function(x){
                console.log("downloaded");
                
//                console.log(x);
            };
        }
        
        
        
        
         $rootScope.cancel = function () {
                $mdDialog.cancel();
            };
        
         $scope.information = function (ev) {
                console.log($rootScope.csissi);
                 if($rootScope.csissi){
                         if($rootScope.language=="EN"){
                             console.log("english",$rootScope.language);
                             console.log("english",$rootScope.language);
                             console.log("english",$rootScope.language);
                             console.log("english",$rootScope.language);
                            $mdDialog.show({
                                    templateUrl: 'csiinfo.html',
                                    parent: angular.element(document.body),
                                      controller: DialogController,
                                     clickOutsideToClose: true
                                });
                            }
                            else if($rootScope.language=="TH"){
                             console.log("Thai",$rootScope.language);
                             console.log("Thai",$rootScope.language);
                             console.log("Thai",$rootScope.language);
                             console.log("Thai",$rootScope.language);
                                $mdDialog.show({
                                    templateUrl: 'csiinfoth.html',
                                    parent: angular.element(document.body),
                                      controller: DialogController,
                                     clickOutsideToClose: true
                                });
                            }
                     
                      
                    }

                    else{
                        $mdDialog.show({
                            templateUrl: 'ssiinfo.html',
                            parent: angular.element(document.body),
                              controller: DialogController,
                             clickOutsideToClose: true
                        });
                    }
             };
             
             
         $scope.downloadshow = function (ev) {
                console.log($rootScope.csissi);
//                 if($rootScope.csissi){
//                        $mdDialog.show({
//                            templateUrl: 'csiinfo.html',
//                            parent: angular.element(document.body),
//                              controller: DialogController,
//                             clickOutsideToClose: true
//                        });
//                    }
//
//                    else{
//                        $mdDialog.show({
//                            templateUrl: 'ssiinfo.html',
//                            parent: angular.element(document.body),
//                              controller: DialogController,
//                             clickOutsideToClose: true
//                        });
//                    }
             };
        
        
        $scope.$on('newMailNotification', function(){
            vm.emailNew = true;
        });
    }
})();


