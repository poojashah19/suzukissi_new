(function () {
    'use strict';

    angular
            .module('triangular.components')
            .controller('RightSidenavController', RightSidenavController);

    /* @ngInject */
    function RightSidenavController($scope, $http, $mdSidenav, $state, $log, $rootScope, service, $sce) {
        var vm = this;
        // Function call start
//        service.csi_filterfunction();
//        service.csi_titles();
        //function calls end
        // sets the current active tab
        vm.close = close;
        $scope.pagenumber1 = '1';
        vm.currentTab = 0;
//        $scope.region1 = "Bangkok & Greater";
//        $scope.region2 = "Central and East";
//        $scope.dealer1 = "$rootScope.Filterarray.dealer[1].name;";
//        $scope.dealer2 = "Ariyakij";
        $scope.zone = "All";
        $scope.region = "All";
        $scope.model = "All";
        $scope.dealer = "All";
        $scope.dealersnap = $rootScope.dealerScoreDealer;
        $scope.snapregion = "All";
        $scope.snapzone = "All";
        $scope.snapmodel = "All";
        $scope.year = $rootScope.year;//'2017';
        $scope.biannual = "All";
        $scope.month = $rootScope.Filterarray.month[1].name;//"Mar'17";
        $scope.factor = "CSI";
        
        $scope.trendfactorlabel = "Trend Period";
        $scope.zone1l = "Zone 1";
        $scope.zone2l = "Zone 2";
        $scope.dealer1l = "Dealer 1";
        $scope.dealer2l = "Dealer 2";
        $scope.langconversionname = "Zone Comparison";
        $scope.zone1 = $rootScope.Filterarray.zones[1].name;
        $scope.zone2 = $rootScope.Filterarray.zones[2].name;
        $scope.dealer1 = $rootScope.Filterarray.dealer[1].name;
        $scope.dealer2 = $rootScope.Filterarray.dealer[2].name;

//        alert( $scope.dealer1+ $scope.dealer2);
////////////////////////////////
//        $rootScope.year = 'All';
//        $rootScope.biannual = "All";
//        $rootScope.month = "All";
        /////////////////////////
        $scope.periodl = "Trend Period";
        $scope.period = "Month";

        $rootScope.legendforselectionb = "Best Overall";
        $rootScope.legendforselectionw = "Worst Overall";
        $scope.langcode = $rootScope.language;
        $scope.enghide = true;
        $scope.thaihide = false;
        var study = {"name": "All", "value": "All"};
        var all = {"name": "All", "value": "All"};
        var none = {"name": "None", "value": "None"};
        if ($rootScope.language == 'EN') {
            study = {"name": "All", "value": "All"};
            all = {"name": "All", "value": "All"};
            none = {"name": "None", "value": "None"};
        } else if ($rootScope.language == 'TH') {
            study = {"name": "All", "value": "全部"};
            all = {"name": "All", "value": "全部"};
            none = {"name": "None", "value": "ไม่แสดงผล"};
        }
        if ($scope.langcode == 'EN') {
//                    $scope.legendforselectionb="Study Best for Selection";
//                   $scope.legendforselectionw="Study Worst for Selection";
            $scope.choosecomparision = "Choose Comparison";
            $scope.zonecomparision = "Zone Comparison";
            $scope.dealercomparision = "Dealer Comparison";
            $scope.radioSwitch = "Zone Comparison";

            $scope.loyalSwitch = "Study Total";
            $scope.opt1 = "Study Total";
            $scope.loyaltotal = '';
            $scope.loyalzone = 'Zone of the dealer';
            $scope.loyalregion = 'Region of the dealer';
            $scope.zonel = "Zone";
            $scope.regionl = "Region";
            $scope.modell = "Product";
            $scope.dealerl = "Dealer";
            $scope.yearl = 'Year';
            $scope.biannuall = "Bi-Annual Period";
            $scope.monthl = "Month";
//            $scope.region11 = $rootScope.Filterarray.region[1].name;
//            $scope.region21 = $rootScope.Filterarray.region[2].name;
//            $scope.dealer11 = $rootScope.Filterarray.dealer[1].name;
//            $scope.dealer21 = $rootScope.Filterarray.dealer[2].name;
            $scope.region1l = "Region 1";
            $scope.region2l = "Region 2";
            $scope.dealer1l = "Dealer 1";
            $scope.dealer2l = "Dealer 2";
            $scope.factorl = 'Factor';
            $scope.factorlt = 'Factor';
            $scope.mainSelectionTooltip = "Main Selection";
            $scope.filterTooltip = "Filters";
            $scope.sumtip = $rootScope.PageNotearray[0].ContentEN;
            $scope.regiontip = $rootScope.PageNotearray[4].ContentEN;
            $scope.trendtip = $rootScope.PageNotearray[6].ContentEN;
            $scope.prioritytip = $rootScope.PageNotearray[7].ContentEN;
            $scope.soptip = $rootScope.PageNotearray[8].ContentEN;
            $scope.strendtip = $rootScope.PageNotearray[9].ContentEN;
            $scope.otherstip = $rootScope.PageNotearray[10].ContentEN; // actual
            $scope.dscoretip = $rootScope.PageNotearray[11].ContentEN;// actual
            $scope.dsnaptip = $rootScope.PageNotearray[12].ContentEN;
            $scope.modeltip = $rootScope.PageNotearray[13].ContentEN;
            $scope.loyaltip = $rootScope.PageNotearray[14].ContentEN;
//                    $scope.loyaltip=$rootScope.PageNotearray[15].ContentEN ;

        } else {
//                    $scope.legendforselectionb="ดีที่สุดจากหัวข้อที่เลือก";
//                    $scope.legendforselectionw="แย่ที่สุดจากหัวข้อที่เลือก";
            $scope.choosecomparision = "รูปแบบที่ต้องการเปรียบเทียบ";
            $scope.zonecomparision = "โซน รายภูมิภาค";
            $scope.opt1 = "ทั่วประเทศ";
            $scope.loyaltotal = '';
            $scope.loyalzone = 'โซนของศูนย์บริการนี้';
            $scope.loyalregion = 'ภูมิภาคของศูนย์บริการนี้';
            $scope.dealercomparision = "เปรียบเทียบ รายศูนย์บริการ";
            $scope.radioSwitch = "Zone Comparison";
            $scope.langconversionname = "โซน รายภูมิภาค";

            $scope.loyalSwitch = "ทั่วประเทศ";
            $scope.zonel = "โซน";
            $scope.regionl = "ภูมิภาค";
            $scope.modell = "เครื่องจักรกล";
            $scope.dealerl = "ศูนย์บริการ";
            $scope.yearl = 'ปีของการสำรวจ';
            $scope.biannuall = "ครึ่งปีของการสำรวจ";
            $scope.monthl = "เดือนของการสำรวจ";
            $scope.zone11 = $rootScope.Filterarray.zone[1].name;
            $scope.zone21 = $rootScope.Filterarray.zone[2].name;
            $scope.dealer11 = $rootScope.Filterarray.dealer[1].name;
            $scope.dealer21 = $rootScope.Filterarray.dealer[2].name;
            $scope.factorl = 'ปัจจัยต่างๆ';
            $scope.factorlt = 'ปัจจัยต่างๆ';
            $scope.mainSelectionTooltip = "ข้อมูลที่แสดงผล";
            $scope.filterTooltip = "ช่วงเวลาที่แสดงผล";
            $scope.sumtip = $rootScope.PageNotearray[0].ContentReganal;
            $scope.regiontip = $rootScope.PageNotearray[4].ContentReganal;
            $scope.trendtip = $rootScope.PageNotearray[6].ContentReganal;
            $scope.prioritytip = $rootScope.PageNotearray[7].ContentReganal;
            $scope.soptip = $rootScope.PageNotearray[8].ContentReganal;
            $scope.strendtip = $rootScope.PageNotearray[9].ContentReganal;
            $scope.otherstip = $rootScope.PageNotearray[10].ContentReganal;
            $scope.dscoretip = $rootScope.PageNotearray[11].ContentReganal;
            $scope.dsnaptip = $rootScope.PageNotearray[12].ContentReganal;
            $scope.modeltip = $rootScope.PageNotearray[13].ContentReganal;
            $scope.loyaltip = $rootScope.PageNotearray[14].ContentReganal;
//                    $scope.loyaltip=$rootScope.PageNotearray[15].ContentReganal ;
        }


        $scope.$on('changemodelgraph', function (event, data) {
            $scope.model = data;
        });
        $scope.$on('changefactordsnap', function (event, data) {
            $scope.factor = data;
        });

        $scope.$on('changeLanguage', function (event, data) {
            $scope.langcode = data;
            $rootScope.languagechangernav();

            var study = {"name": "All", "value": "All"};
            var none = {"name": "None", "value": "None"};
            var all = {"name": "All", "value": "All"};
            if ($rootScope.language == 'EN') {
                
                 $scope.trendfactorlabel = "Trend Period";
                study = {"name": "All", "value": "All"};
                all = {"name": "All", "value": "All"};
                none = {"name": "None", "value": "None"};
            } else if ($rootScope.language == 'TH') {
                 $scope.trendfactorlabel = "ช่วงเวลา";
                study = {"name": "All", "value": "全部"};
                none = {"name": "None", "value": "ไม่แสดงผล"};
                all = {"name": "All", "value": "全部"};
            }
            if ($scope.langcode == 'EN') {

                if ($scope.radioSwitch == "Dealer Comparison") {
                    $scope.langconversionname = "Dealer Comparison";
                } else

                if ($scope.radioSwitch == "Zone Comparison") {
                    $scope.langconversionname = "Zone Comparison";
                }
                $scope.enghide = false;
                $scope.thaihide = true;
            } else if ($scope.langcode == 'TH') {

                if ($scope.radioSwitch == "Dealer Comparison") {
                    $scope.langconversionname = "เปรียบเทียบ รายศูนย์บริการ";
                } else
                if ($scope.radioSwitch == "Zone Comparison") {
                    $scope.langconversionname = "โซน รายภูมิภาค";
                }



                $scope.enghide = true;
                $scope.thaihide = false;
            }


            var parameter = [{"name": "langCode", "value": $rootScope.language}, {"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "dealer", "value": $scope.dealer}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}];
            service.mastersrv("Filters-getCSIZone", parameter).then(function (result) {
                $rootScope.Filterarray.zone = result.data;
                $rootScope.Filterarray.zones = angular.copy($rootScope.Filterarray.zone);
                $rootScope.Filterarray.zones2 = angular.copy($rootScope.Filterarray.zone);
                $rootScope.Filterarray.zones.splice(0, 0, none);
                $rootScope.Filterarray.zone.splice(0, 0, study);
                console.log("zone filter", $rootScope.Filterarray.zone);
            });
            service.mastersrv("Filters-getRegion", parameter).then(function (result) {
//                console.log("In //Right Side : - " + $rootScope.Filterarray.region);
                $rootScope.Filterarray.region = result.data;
                $rootScope.Filterarray.region.splice(0, 0, all);
                $rootScope.Filterarray.regions = result.data;
//                $rootScope.Filterarray.regions.splice(0, 0, study);
                console.log("Region filter::::::::::::::::::::::::::", $rootScope.Filterarray.region);
                //                  $rootScope.Filterarray.region.push("All");
            });
            service.mastersrv("Filters-getDealers", parameter).then(function (result) {
                $rootScope.Filterarray.dealer = result.data;
                $rootScope.Filterarray.dealers =angular.copy(result.data);
                $rootScope.Filterarray.dealers2=angular.copy(result.data);
                $rootScope.Filterarray.dealer.splice(0, 0, all);
                $rootScope.Filterarray.dealers.splice(0, 0, none);
                //                    console.log("zone filter",$rootScope.Filterarray.dealer);
            });
            service.mastersrv("Filters-getModels", parameter).then(function (result) {
                $rootScope.Filterarray.model = result.data;
                $rootScope.Filterarray.model.splice(0, 0, study);
            });
            service.mastersrv("Filters-getPeriods", parameter).then(function (result) {
                $rootScope.Filterarray.period = result.data;
//                console.log("getPeriod::::::::::::::::::::::", $rootScope.Filterarray.period);
                $rootScope.period = $rootScope.Filterarray.period[0].name;
//                    $rootScope.Filterarray.dealer.splice(0, 0, study);
            });



            if ($rootScope.redfeedback == "redfeedback") {
                var langparameter = [{"name": "langCode", "value": $rootScope.language}];
                console.log('$rootScope.redfeedback', $rootScope.redfeedback);
                console.log('$rootScope.redfeedback', $rootScope.redfeedback);
                console.log('$rootScope.redfeedback', $rootScope.redfeedback);
                service.mastersrv("Filters-getYear", langparameter).then(function (result) {
                    if ($rootScope.language == 'EN') {
                        var all = {"name": "All", "value": "All"};
                    } else if ($rootScope.language == 'TH') {
                        all = {"name": "All", "value": "全部"};
                    }
                    $rootScope.Filterarray.year = result.data;
                    $rootScope.Filterarray.year.splice(0, 0, all);
//                    $scope.year = "All";
                    $scope.year = $rootScope.year;
                });
                var redothersparameter = [{"name": "langCode", "value": $rootScope.language}, {"name": "tablename", "value": $rootScope.tablename}];

                service.mastersrv("Filters-getfilteredDealers", redothersparameter).then(function (result) {
                    if ($rootScope.language == 'EN') {
                        var all = {"name": "All", "value": "All"};
                    } else if ($rootScope.language == 'TH') {
                        all = {"name": "All", "value": "全部"};
                    }
                    $rootScope.Filterarray.redalertdealer = result.data;
                    $rootScope.Filterarray.redalertdealer.splice(0, 0, all);
                    $scope.redalertdealer = $rootScope.Filterarray.redalertdealer[0].name;
//                    $scope.year = "All";

                });


            } else {

                console.log('$rootScope.redfeedback', $rootScope.redfeedback);
                console.log('$rootScope.redfeedback', $rootScope.redfeedback);
                console.log('$rootScope.redfeedback', $rootScope.redfeedback);
                var langparameter = [{"name": "langCode", "value": $rootScope.language}];
                service.mastersrv("Filters-getYear", langparameter).then(function (result) {
                    $rootScope.Filterarray.year = result.data;
//                    $scope.year = '2017';
                    $scope.year = $rootScope.year;
                });
            }

//                    service.mastersrv("Filters-getYear",parameter).then(function (result) {
//                            $rootScope.Filterarray.year=result.data;
//                        });
            service.mastersrv("Filters-getBiannual", parameter).then(function (result) {
                $rootScope.Filterarray.biannual = result.data;
                $rootScope.Filterarray.biannual.splice(0, 0, all);
                //                    console.log("zone filter",$rootScope.Filterarray.biannual);
            });

            service.mastersrv("Filters-getBiannual", [{"name": "year", "value": 'All'}, {"name": "langCode", "value": $rootScope.language}]).then(function (result) {
                $rootScope.Filterarray.biannualx = result.data;
                $rootScope.Filterarray.biannualx.splice(0, 0, all);

            });
            service.mastersrv("Filters-getMonth", parameter).then(function (result) {
                $rootScope.Filterarray.month = result.data;
                $rootScope.Filterarray.month.splice(0, 0, all);
                //                    console.log("zone filter",$rootScope.Filterarray.month);
                $rootScope.downloadurl = "http://webapps.wheelmonk.com/SuzukiTHDashboardUAT/assets/DCSI_Wave_12_Client.xlsx";
            });
            service.mastersrv("Filters-getFactors", parameter).then(function (result) {
                $rootScope.Filterarray.factor = result.data;
                //                     $rootScope.Filterarray.factor.push("All");
            });
            service.mastersrv("Filters-getSOPAttributes", parameter).then(function (result) {
                $rootScope.Filterarray.sop = result.data;
            });
        });


        $scope.filterReset = function () {
            $scope.fueltoggleAll();
            $scope.toggleAll();
        };


        $scope.onchangeRadioSwitch = function (radio) {
//          
            if (radio == 'Dealer Comparison') {
                $scope.priorityRegionHide = true;
                $scope.radio = 'dealer';
                if ($scope.langcode == 'EN') {
                    $scope.radioSwitch = "Dealer Comparison";
                    $scope.langconversionname = "Dealer Comparison";
                } else if ($scope.langcode == 'TH') {
                    $scope.radioSwitch = "Dealer Comparison";
                    $scope.langconversionname = "เปรียบเทียบ รายศูนย์บริการ";

                }

                $rootScope.$broadcast("changeRadioSwitch", $scope.radio);
            } else {
                $scope.priorityRegionHide = false;
                $scope.radio = 'zone';
                if ($scope.langcode == 'EN') {
                    $scope.radioSwitch = "Zone Comparison";
                    $scope.langconversionname = "Zone Comparison";
                } else if ($scope.langcode == 'TH') {
                    $scope.radioSwitch = "Zone Comparison";
                    $scope.langconversionname = "โซน รายภูมิภาค";
                }
                $rootScope.$broadcast("changeRadioSwitch", $scope.radio);
            }

        };



        $scope.onchangeloyalSwitch = function (radio) {

            $rootScope.$broadcast("changeloyalSwitch", radio);

            if (radio == 'Study Total') {


                $rootScope.$broadcast("changeRadioSwitch", $scope.radio);
            } else
            if (radio == 'Zone') {

                $rootScope.$broadcast("changeRadioSwitch", $scope.radio);
            }
        };

        $scope.region1changed = function (value) {

            $scope.region1 = value;
            $scope.region1array = value;
            $rootScope.region1selected = $scope.region1array;
            $rootScope.$broadcast("changeregion1", $scope.region1array);
        };


        $scope.region2changed = function (value) {
            $scope.region2 = value;
            $scope.region2array = value;
            $rootScope.region2selected = $scope.region2array;
            $rootScope.$broadcast("changeregion2", $scope.region2array);
        };

        $scope.zone1changed = function (value) {

            $scope.zone1 = value;
//            $scope.region1array = value;
//            $rootScope.region1selected = $scope.region1array;
            $rootScope.$broadcast("changezone1", value);
        };


        $scope.zone2changed = function (value) {
            $scope.zone2 = value;
//            $scope.region2array = value;
//            $rootScope.region2selected = $scope.region2array;
            $rootScope.$broadcast("changezone2", value);
        };


        $scope.dealer1changed = function (value) {
            $scope.dealer1 = value;
            $scope.dealer1array = value;
            $rootScope.dealer1selected = $scope.dealer1array;
            $rootScope.$broadcast("changedealer1", $scope.dealer1array);
        };


        $scope.dealer2changed = function (value) {
            $scope.dealer2 = value;
            $scope.dealer2array = value;
            $rootScope.dealer2selected = $scope.dealer2array;
            $rootScope.$broadcast("changedealer2", $scope.dealer2array);
        };

        $scope.periodchanged = function (value) {
            $scope.period = value;
            $rootScope.$broadcast("changeperiod", $scope.period);
        };

        $scope.yearchanged = function (value) {
            $scope.year = value;
            $rootScope.year = value;
            $scope.yeararray = value;
            $rootScope.yearselected = $scope.yeararray;
            $rootScope.$broadcast("changeyear", $scope.yeararray);
            if ($rootScope.language == 'EN') {
                study = {"name": "All", "value": "All"};
                all = {"name": "All", "value": "All"};
            } else if ($rootScope.language == 'TH') {
                study = {"name": "All", "value": "全部"};
                all = {"name": "All", "value": "全部"};
            }
            var yearobj = [{"name": "langCode", "value": $scope.langcode}, {"name": "year", "value": $scope.year}];
            service.mastersrv("Filters-getBiannual", yearobj).then(function (result) {
                $rootScope.Filterarray.biannual = result.data;
                $rootScope.Filterarray.biannual.splice(0, 0, all);
                console.log($rootScope.Filterarray.biannual);
                $scope.biannual = "All";
            });
            var yearobj1 = [{"name": "langCode", "value": $scope.langcode}, {"name": "year", "value": $scope.year}];
            service.mastersrv("Filters-getMonth", yearobj1).then(function (result) {
                $rootScope.Filterarray.month = result.data;
                $rootScope.Filterarray.month.splice(0, 0, all);
                console.log($rootScope.Filterarray.month);
                $scope.month = "All";

//                $rootScope.month = "All";
            });
        };


        $scope.biannualchanged = function (value) {
            $scope.biannual = value;

            $rootScope.biannual = value;
            $scope.biannualarray = value;
            $rootScope.biannualseleted = $scope.biannualarray;
            $rootScope.$broadcast("changebiannual", $scope.biannualarray);
            $scope.month = "All";
            if ($rootScope.language == 'EN') {
                study = {"name": "All", "value": "All"};
                all = {"name": "All", "value": "All"};
            } else if ($rootScope.language == 'TH') {
                study = {"name": "All", "value": "全部"};
                all = {"name": "All", "value": "全部"};
            }
            var biannualobj = [{"name": "langCode", "value": $scope.langcode}, {"name": "biannual", "value": $scope.biannual}];
            service.mastersrv("Filters-getMonth", biannualobj).then(function (result) {
                $rootScope.Filterarray.month = result.data;
                $rootScope.Filterarray.month.splice(0, 0, all);
                console.log($rootScope.Filterarray.month);
                $scope.month = "All";
                $rootScope.month = "All";
            });
        };


        $scope.monthchanged = function (value) {
            $scope.month = value;

            $rootScope.month = value;
            $scope.montharray = value;
            $rootScope.monthselected = $scope.montharray;
            $rootScope.$broadcast("changemonth", $scope.montharray);
            var monthobj = [{"name": "month", "value": $scope.month}];
        };


        $scope.zonechanged = function (value) {
            $scope.zone = value;
            $scope.zonearray = value;
            $rootScope.zoneseleted = $scope.zonearray;
            $rootScope.$broadcast("changezone", $scope.zonearray);
            if ($rootScope.language == 'EN') {
                study = {"name": "All", "value": "All"};
                all = {"name": "All", "value": "All"};
            } else if ($rootScope.language == 'TH') {
                study = {"name": "All", "value": "全部"};
                all = {"name": "All", "value": "全部"};
            }

            var zoneobj = [{"name": "langCode", "value": $scope.langcode}, {"name": "zone", "value": encodeURIComponent($scope.zone)}];
            service.mastersrv("Filters-getRegion", zoneobj).then(function (result) {
                console.log("zoneobj reg first" + $rootScope.Filterarray.region);
                $rootScope.Filterarray.region = result.data;
                $rootScope.Filterarray.regions = result.data;
                console.log("zoneobj reg befpre splice" + $rootScope.Filterarray.region);
                $rootScope.Filterarray.region.splice(0, 0, study);
                console.log("zoneobj reg after" + $rootScope.Filterarray.region);
                $scope.region = "All";
                console.log("zoneobj reg after region" + $rootScope.Filterarray.region);
            });

            service.mastersrv("Filters-getDealers", zoneobj).then(function (result) {
                $rootScope.Filterarray.dealer = result.data;
                $rootScope.Filterarray.dealers = result.data;
                $rootScope.Filterarray.dealers2=angular.copy(result.data);
                $rootScope.Filterarray.dealer.splice(0, 0, study);

                $scope.dealer = "All";
            });
        };


        $scope.regionchanged = function (value) {
            $scope.regionarray = value;
            $scope.region = value;
            $rootScope.regionseleted = $scope.regionarray;
            $rootScope.$broadcast("changeregion", $scope.regionarray);
            $scope.dealer = "All";
            if ($rootScope.language == 'EN') {
                study = {"name": "All", "value": "All"};
                all = {"name": "All", "value": "All"};
            } else if ($rootScope.language == 'TH') {
                study = {"name": "All", "value": "全部"};
                all = {"name": "All", "value": "全部"};
            }
            var regionobj = [{"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}];
            service.mastersrv("Filters-getDealers", regionobj).then(function (result) {
                $rootScope.Filterarray.dealer = result.data;
               
                
                $rootScope.Filterarray.dealer.splice(0, 0, study);
                $scope.dealer = 'All';
                $scope.dealer1 = $rootScope.Filterarray.dealers[1];
                $scope.dealer2 = $rootScope.Filterarray.dealers[2];
                //                                console.log("city dealer " +result.data);
            });

        };


        $scope.dealerchanged = function (value) {
            $scope.dealerarray = value;
            $scope.dealer = value;
            $rootScope.dealerseleted = $scope.dealerarray;
            $rootScope.$broadcast("changedealer", $scope.dealerarray);
            var dealerobj = [{"name": "month", "value": $scope.month}];
            $scope.dealer1 = $rootScope.Filterarray.dealers[1];
            $scope.dealer2 = $rootScope.Filterarray.dealers[2];
            //                        service.mastersrv("Filters-getCSIZone",dealerobj).then(function (result) {
            //                                       $scope.zone=result.data.value;
            //                           });
            //                         service.mastersrv("Filters-getRegion",dealerobj).then(function (result) {
            //                                       $scope.region=result.data.value;
            //                           });   
        };


        $scope.redalertdealerchanged = function (value) {

            $scope.redalertdealer = value;

            $rootScope.$broadcast("changeredalertdealer", value);

        };

        $scope.snapzonechanged = function (value) {
            $scope.snapzone = value;
            $scope.zonearray = value;
            $rootScope.zoneseleted = $scope.zonearray;
            $rootScope.$broadcast("snapzonechanged", $scope.zonearray);
            if ($rootScope.language == 'EN') {
                study = {"name": "All", "value": "All"};
                all = {"name": "All", "value": "All"};
            } else if ($rootScope.language == 'TH') {
                study = {"name": "All", "value": "全部"};
                all = {"name": "All", "value": "全部"};
            }

            var zoneobj = [{"name": "langCode", "value": $scope.langcode}, {"name": "zone", "value": encodeURIComponent($scope.snapzone)}];
            service.mastersrv("Filters-getRegion", zoneobj).then(function (result) {
                $rootScope.Filterarray.region = result.data;
                $rootScope.Filterarray.regions = result.data;
                $rootScope.Filterarray.region.splice(0, 0, study);
                $scope.snapregion = "All";
            });

            service.mastersrv("Filters-getDealers", zoneobj).then(function (result) {
                $rootScope.Filterarray.dealer = result.data;
                $rootScope.Filterarray.dealers = result.data;
                $scope.dealersnap = $rootScope.Filterarray.dealer[0].value;
                console.log("$rootScope.Filterarray.dealer[0].value", $rootScope.Filterarray.dealer[0].value);
                console.log("$rootScope.Filterarray.dealer[0].value", $rootScope.Filterarray.dealer[0].value)
                console.log("$rootScope.Filterarray.dealer[0].value", $rootScope.Filterarray.dealer[0].value)

            });
        };


        $scope.snapregionchanged = function (value) {
            $scope.regionarray = value;
            $scope.snapregion = value;
            $rootScope.regionseleted = $scope.regionarray;
            $rootScope.$broadcast("snapregionchanged", $scope.regionarray);
            $scope.dealersnap = "All";
            if ($rootScope.language == 'EN') {
                study = {"name": "All", "value": "All"};
                all = {"name": "All", "value": "All"};
            } else if ($rootScope.language == 'TH') {
                study = {"name": "All", "value": "全部"};
                all = {"name": "All", "value": "全部"};
            }
            var regionobj = [{"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.snapregion)}];
            service.mastersrv("Filters-getDealers", regionobj).then(function (result) {
                $rootScope.Filterarray.dealer = result.data;
                $scope.dealersnap = $rootScope.Filterarray.dealer[0].value;
                console.log("$rootScope.Filterarray.dealer[0].value", $rootScope.Filterarray.dealer[0].value);
                console.log("$rootScope.Filterarray region.dealer[0].value", $rootScope.Filterarray.dealer[0].value)
                console.log("$rootScope.Filterarray region.dealer[0].value", $rootScope.Filterarray.dealer[0].value)

            });

        };
        $scope.dealersnapchanged = function (value) {
            $scope.dealerarray = value;
            $scope.dealersnap = value;
            $rootScope.dealerseleted = $scope.dealerarray;
            $rootScope.$broadcast("dealersnapchanged", $scope.dealerarray);

        };


        $scope.modelchanged = function (value) {
            $scope.modelarray = value;
            $scope.model = value;
            $rootScope.modelseleted = $scope.modelarray;
            $rootScope.$broadcast("changemodel", $scope.modelarray);
        };


        $scope.factorchanged = function (value) {
            $scope.factorarray = value;
            $scope.factor = value;
            $rootScope.factorseleted = $scope.factorarray;
            $rootScope.$broadcast("changefactor", $scope.factorarray);
        };


        $scope.changed = function (value) {
            $scope.allmodel = false;
            $scope.allfuel = false;

        };


        $rootScope.languagechangernav = function (type) {

            if ($scope.langcode == 'EN') {
//                $scope.legendforselectionb="Study Best for Selection";
//                $scope.legendforselectionw="Study Worst for Selection";
                $scope.choosecomparision = "Choose Comparison";
                $scope.zonecomparision = "Zone Comparison";
                $scope.opt1 = "Study Total";
                $scope.loyaltotal = '';
                $scope.loyalzone = 'Zone of the dealer';
                $scope.loyalregion = 'Region of the dealer';
                $scope.dealercomparision = "Dealer Comparison";
//                $scope.radioSwitch = "Zone Comparison";
                $scope.loyalSwitch = "Study Total";
                $scope.zonel = "Zone";
                $scope.regionl = "Region";
                $scope.modell = "Product";
                $scope.dealerl = "Dealer";
                $scope.yearl = 'Year';
                $scope.biannuall = "Bi-Annual Period";
                $scope.monthl = "Month";
                $scope.month = "All";



//                $scope.region11 = $rootScope.Filterarray.region[1].name;
//                $scope.region21 = $rootScope.Filterarray.region[2].name;
//                $scope.dealer11 = $rootScope.Filterarray.dealer[1].name;
//                $scope.dealer21 = $rootScope.Filterarray.dealer[2].name;
                $scope.zone1l = "Zone 1";
                $scope.zone2l = "Zone 2";
                $scope.dealer1l = "Dealer 1";
                $scope.dealer2l = "Dealer 2";
                $scope.factorl = 'Factor';
                $scope.factorlt = 'Factor';
                $scope.mainSelectionTooltip = "Main Selection";
                $scope.filterTooltip = "Filters";


                $scope.sumtip = $rootScope.PageNotearray[0].ContentEN;
                $scope.regiontip = $rootScope.PageNotearray[4].ContentEN;
                $scope.trendtip = $rootScope.PageNotearray[6].ContentEN;
                $scope.prioritytip = $rootScope.PageNotearray[7].ContentEN;
                $scope.soptip = $rootScope.PageNotearray[8].ContentEN;
                $scope.strendtip = $rootScope.PageNotearray[9].ContentEN;
                $scope.otherstip = $rootScope.PageNotearray[10].ContentEN;
                $scope.dscoretip = $rootScope.PageNotearray[11].ContentEN;
                $scope.dsnaptip = $rootScope.PageNotearray[12].ContentEN;
                $scope.modeltip = $rootScope.PageNotearray[13].ContentEN;
                $scope.loyaltip = $rootScope.PageNotearray[14].ContentEN;
//                $scope.loyaltip=$rootScope.PageNotearray[15].ContentEN ;
                $scope.year = $rootScope.year;

            } else {
//                $scope.legendforselectionb="ดีที่สุดจากหัวข้อที่เลือก";
//                $scope.legendforselectionw="แย่ที่สุดจากหัวข้อที่เลือก";
                $scope.choosecomparision = "รูปแบบที่ต้องการเปรียบเทียบ";
                $scope.zonecomparision = "โซน รายภูมิภาค";
                $scope.opt1 = "ทั่วประเทศ";
                $scope.loyaltotal = '';
                $scope.loyalzone = 'โซนของศูนย์บริการนี้';
                $scope.loyalregion = 'ภูมิภาคของศูนย์บริการนี้';
                $scope.dealercomparision = "เปรียบเทียบ รายศูนย์บริการ";
//                  $scope.radioSwitch = "Zone Comparison";


                $scope.loyalSwitch = "ทั่วประเทศ";
                $scope.zonel = "โซน";
                $scope.regionl = "ภูมิภาค";
                $scope.modell = "เครื่องจักรกล";
                $scope.dealerl = "ศูนย์บริการ";
                $scope.yearl = 'ปีของการสำรวจ';
                $scope.biannuall = "ครึ่งปีของการสำรวจ";
                $scope.monthl = "เดือนของการสำรวจ";
                $scope.zone1l = "โซน 1";
                $scope.zone2l = "โซน 2";
                $scope.dealer1l = "ศูนย์บริการ 1";
                $scope.dealer2l = "ศูนย์บริการ 2";
//                $scope.region11 = $rootScope.Filterarray.region[1].name;
//                $scope.region21 = $rootScope.Filterarray.region[2].name;
//                $scope.dealer11 = $rootScope.Filterarray.dealer[1].name;
//                $scope.dealer21 = $rootScope.Filterarray.dealer[2].name;
                $scope.factorl = 'ปัจจัยต่างๆ';
                $scope.factorlt = 'ปัจจัยต่างๆ';
                $scope.mainSelectionTooltip = "ข้อมูลที่แสดงผล";
                $scope.filterTooltip = "ช่วงเวลาที่แสดงผล";

                $scope.sumtip = $rootScope.PageNotearray[0].ContentReganal;
                $scope.regiontip = $rootScope.PageNotearray[4].ContentReganal;
                $scope.trendtip = $rootScope.PageNotearray[6].ContentReganal;
                $scope.prioritytip = $rootScope.PageNotearray[7].ContentReganal;
                $scope.soptip = $rootScope.PageNotearray[8].ContentReganal;
                $scope.strendtip = $rootScope.PageNotearray[9].ContentReganal;
                $scope.otherstip = $rootScope.PageNotearray[10].ContentReganal;
                $scope.dscoretip = $rootScope.PageNotearray[11].ContentReganal;
                $scope.dsnaptip = $rootScope.PageNotearray[12].ContentReganal;
                $scope.modeltip = $rootScope.PageNotearray[13].ContentReganal;
                $scope.loyaltip = $rootScope.PageNotearray[14].ContentReganal;
//                $scope.loyaltip=$rootScope.PageNotearray[15].ContentReganal ;
                $scope.year = $rootScope.year;
            }

        };


        $rootScope.checkboxInitiliser = function (type) {
            $scope.priorityRegionHide = false;
            if ($rootScope.language == 'EN') {
                study = {"name": "All", "value": "All"};
                var none = {"name": "None", "value": "None"};
                all = {"name": "All", "value": "All"};
            } else if ($rootScope.language == 'TH') {
                study = {"name": "All", "value": "全部"};
                var none = {"name": "None", "value": "ไม่แสดงผล"};
                all = {"name": "All", "value": "全部"};
            }
            if ($rootScope.dsnapshot == "dsnap") {
                $scope.dealersnap = $rootScope.dealerScoreDealer;
            } else {
                $scope.dealer = "All";
            }
            $scope.period = "Month";
            if ($rootScope.redfeedback == "redfeedback") {
                var langparameter = [{"name": "langCode", "value": $rootScope.language}];
                var redothersparameter = [{"name": "langCode", "value": $rootScope.language}, {"name": "tablename", "value": $rootScope.tablename}];
                console.log('$rootScope.redfeedback', $rootScope.redfeedback);
                console.log('$rootScope.redfeedback', $rootScope.redfeedback);
                console.log('$rootScope.redfeedback', $rootScope.redfeedback);
                service.mastersrv("Filters-getYear", langparameter).then(function (result) {
                    if ($rootScope.language == 'EN') {
                        var all = {"name": "All", "value": "All"};
                    } else if ($rootScope.language == 'TH') {
                        all = {"name": "All", "value": "全部"};
                    }
                    $rootScope.Filterarray.year = result.data;
                    $rootScope.Filterarray.year.splice(0, 0, all);
//                    $scope.year = "All";
                    $scope.year = $rootScope.year;
                });

                service.mastersrv("Filters-getfilteredDealers", redothersparameter).then(function (result) {
                    if ($rootScope.language == 'EN') {
                        var all = {"name": "All", "value": "All"};
                    } else if ($rootScope.language == 'TH') {
                        all = {"name": "All", "value": "全部"};
                    }
                    $rootScope.Filterarray.redalertdealer = result.data;
                    $rootScope.Filterarray.redalertdealer.splice(0, 0, all);
                    $scope.redalertdealer = $rootScope.Filterarray.redalertdealer[0].name;
//                    $scope.year = "All";

                });
            } else {

                console.log('$rootScope.redfeedback', $rootScope.redfeedback);
                console.log('$rootScope.redfeedback', $rootScope.redfeedback);
                console.log('$rootScope.redfeedback', $rootScope.redfeedback);
                var langparameter = [{"name": "langCode", "value": $rootScope.language}];
                service.mastersrv("Filters-getYear", langparameter).then(function (result) {
                    $rootScope.Filterarray.year = result.data;
                    $scope.year = $rootScope.year;

//                    $rootScope.year = '2017';
                });
            }

            $scope.pagenumber1 = $rootScope.pagenumber;
            $scope.model = "All";
            $scope.zone = "All";
//                $scope.year='2016';
            service.mastersrv("Filters-getYear").then(function (result) {
                $rootScope.Filterarray.year = result.data;
                $scope.year = $rootScope.year;
            });
            var yearobj = [{"name": "langCode", "value": $scope.langcode}, {"name": "year", "value": '2017'}];
            service.mastersrv("Filters-getBiannual", yearobj).then(function (result) {
                $rootScope.Filterarray.biannual = result.data;
                $rootScope.Filterarray.biannual.splice(0, 0, all);
                console.log($rootScope.Filterarray.biannual);
                $scope.biannual = "All";
            });
            service.mastersrv("Filters-getBiannual", [{"name": "langCode", "value": $scope.langcode}, {"name": "year", "value": 'Study Total'}]).then(function (result) {
                $rootScope.Filterarray.biannualx = result.data;
                $rootScope.Filterarray.biannualx.splice(0, 0, all);
                $scope.biannualx = "All";
            });
            service.mastersrv("Filters-getMonth", yearobj).then(function (result) {
                $rootScope.Filterarray.month = result.data;
                $rootScope.Filterarray.month.splice(0, 0, all);
                console.log($rootScope.Filterarray.month);
                $scope.month = $rootScope.Filterarray.month[1].name;//"Dec'16";
            });
            $scope.region = "All";
            $scope.factor = "CSI";
            $scope.radioSwitch = "Zone Comparison";
            $scope.loyalSwitch = "Study Total";


            if ($scope.langcode == 'EN') {
                $scope.choosecomparision = "Choose Comparison";
                $scope.zonecomparision = "Zone Comparison";
                $scope.opt1 = "Study Total";
                $scope.loyaltotal = '';
                $scope.loyalzone = 'Zone of the dealer';
                $scope.loyalregion = 'Region of the dealer';
                $scope.dealercomparision = "Dealer Comparison";
                $scope.radioSwitch = "Zone Comparison";
                $scope.loyalSwitch = "Study Total";
                $scope.zonel = "Zone";
                $scope.regionl = "Region";
                $scope.modell = "Product";
                $scope.dealerl = "Dealer";
                $scope.yearl = 'Year';
                $scope.biannuall = "Bi-Annual Period";
                $scope.monthl = "Month";
                $scope.region1l = "Region 1";
                $scope.region2l = "Region 2";
                $scope.dealer1l = "Dealer 1";
                $scope.dealer2l = "Dealer 2";
                $scope.factorl = 'Factor';
                $scope.factorlt = 'Factor';
                $scope.mainSelectionTooltip = "Main Selection";
                $scope.filterTooltip = "Filters";

                $scope.zone1 = $rootScope.Filterarray.zones[1].name;
                $scope.zone2 = $rootScope.Filterarray.zones[2].name;
                $scope.dealer1 = $rootScope.Filterarray.dealer[1].name;
                $scope.dealer2 = $rootScope.Filterarray.dealer[2].name;


                $scope.sumtip = $rootScope.PageNotearray[0].ContentEN;
                $scope.regiontip = $rootScope.PageNotearray[4].ContentEN;
                $scope.trendtip = $rootScope.PageNotearray[6].ContentEN;
                $scope.prioritytip = $rootScope.PageNotearray[7].ContentEN;
                $scope.soptip = $rootScope.PageNotearray[8].ContentEN;
                $scope.strendtip = $rootScope.PageNotearray[9].ContentEN;
                $scope.otherstip = $rootScope.PageNotearray[10].ContentEN;
                $scope.dscoretip = $rootScope.PageNotearray[11].ContentEN;
                $scope.dsnaptip = $rootScope.PageNotearray[12].ContentEN;
                $scope.modeltip = $rootScope.PageNotearray[13].ContentEN;
                $scope.loyaltip = $rootScope.PageNotearray[14].ContentEN;

            } else {
                $scope.choosecomparision = "รูปแบบที่ต้องการเปรียบเทียบ";
                $scope.zonecomparision = "โซน รายภูมิภาค";
                $scope.dealercomparision = "เปรียบเทียบ รายศูนย์บริการ";
                $scope.radioSwitch = "Zone Comparison";
                $scope.opt1 = "ทั่วประเทศ";
                $scope.loyaltotal = '';
                $scope.loyalzone = 'โซนของศูนย์บริการนี้';
                $scope.loyalregion = 'ภูมิภาคของศูนย์บริการนี้';
                $scope.zonel = "โซน";
                $scope.regionl = "ภูมิภาค";
                $scope.modell = "เครื่องจักรกล";
                $scope.dealerl = "ศูนย์บริการ";
                $scope.yearl = 'ปีของการสำรวจ';
                $scope.biannuall = "ครึ่งปีของการสำรวจ";
                $scope.monthl = "เดือนของการสำรวจ";


                $scope.factorl = 'ปัจจัยต่างๆ';
                $scope.factorlt = 'ปัจจัยต่างๆ';
                $scope.mainSelectionTooltip = "ข้อมูลที่แสดงผล";
                $scope.filterTooltip = "ช่วงเวลาที่แสดงผล";

                $scope.sumtip = $rootScope.PageNotearray[0].ContentReganal;
                $scope.regiontip = $rootScope.PageNotearray[4].ContentReganal;
                $scope.trendtip = $rootScope.PageNotearray[6].ContentReganal;
                $scope.prioritytip = $rootScope.PageNotearray[7].ContentReganal;
                $scope.soptip = $rootScope.PageNotearray[8].ContentReganal;
                $scope.strendtip = $rootScope.PageNotearray[9].ContentReganal;
                $scope.otherstip = $rootScope.PageNotearray[10].ContentReganal;
                $scope.dscoretip = $rootScope.PageNotearray[11].ContentReganal;
                $scope.dsnaptip = $rootScope.PageNotearray[12].ContentReganal;
                $scope.modeltip = $rootScope.PageNotearray[13].ContentReganal;
                $scope.loyaltip = $rootScope.PageNotearray[14].ContentReganal;
//                    $scope.loyaltip=$rootScope.PageNotearray[15].ContentReganal ;
            }

            console.log("onchange right", $scope.langcode);

        };

        // add an event to switch tabs (used when user clicks a menu item before sidebar opens)
        $scope.$on('triSwitchNotificationTab', function ($event, tab) {
            vm.currentTab = tab;
        });


        function close() {
            $mdSidenav('notifications').close();
        }
    }
})();
