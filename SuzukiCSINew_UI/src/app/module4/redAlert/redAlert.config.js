(function() {
    'use strict';

    angular
        .module('app.module4.redAlert')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

           $stateProvider
         .state('triangular.ssiredAlert', {
            url: '/ssiredAlert',
            views: {
                '': {
                   templateUrl: 'app/module4/redAlert/redAlert.tmpl.html',
                    controller: 'm4redAlertController',
                    controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module4/redAlert/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
            
        });
        
//         triMenuProvider.addMenu({
//                      name: 'Red Alert',
//                    icon: 'zmdi zmdi-view-list-alt',
//                    type: 'link',
//                    priority: 2.2,
//                     state: 'triangular.csiredAlert',
//                });
//             triMenuProvider.removeMenu('triangular.redAlert');   
                
                
    }
})();