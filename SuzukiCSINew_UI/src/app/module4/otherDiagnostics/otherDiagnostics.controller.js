(function () {
    'use strict';

    angular
            .module('app.module4.otherDiagnostics')
            .controller('m4otherDiagnosticsController', m4otherDiagnosticsController);

    /* @ngInject */
    function m4otherDiagnosticsController($scope,$state, service, $http, $rootScope) {
        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776","#92D400","#00A1DE","#72C7E7","#3C8A2E","#C9DD03","#335291","#A8DD33","#33B4E5"];
        $rootScope.subpage="";
        $rootScope.subpagexs="";
        $rootScope.subpage+=" ";
         $rootScope.showFilterTooltip=true;
        $rootScope.hideyear=false;
        $rootScope.hidebiannual=false;
        $rootScope.hidemonth=false;
        $rootScope.hidezone=false;
        $rootScope.hideregion=false;
        $rootScope.hidemodel=true;
        $rootScope.hidefactor=true;
        $rootScope.hidetrendfactor=true;
        $rootScope.hidetrendfactor=true;
        $rootScope.snaphidezone=true;
        $rootScope.snaphideregion=true;
        $rootScope.dealerhide=false;
        $rootScope.dealersnaphide=true;
        $rootScope.pagenumber='7';
        $rootScope.starlabelhide=true;
        $rootScope.priorityhide=true;
        $rootScope.summaryRefresh=1;
        $scope.zone=$rootScope.ALL;
        $scope.region = $rootScope.ALL;
        $scope.model = $rootScope.ALL;
        $scope.year=$rootScope.Filterarray.year[0].value;
        $scope.biannual = $rootScope.ALL;
        $scope.month=$rootScope.Filterarray.month[1].value;
        $rootScope.dealerScoreDealer=$rootScope.Filterarray.dealer[0].value;
        $scope.langcode=$rootScope.language;
        
        $rootScope.dsnapshot="not";
        $rootScope.redfeedback="not";
        $rootScope.checkboxInitiliser();
        
         service.mastersrv("SSIFilters-getDealers").then(function (result) {
            var study={"name":$rootScope.ALL,"value":$rootScope.ALL};
                    if($rootScope.language == 'EN'){
                        study={"name":$rootScope.ALL,"value":$rootScope.ALL};
                    }
                    else if($rootScope.language == 'TH'){
                        study={"name":$rootScope.ALL,"value":"ทั่วประเทศ"};
                    }
                    $rootScope.Filterarray.dealer=result.data;
                    $rootScope.Filterarray.dealer.splice(0, 0, study);
                }); 
        
        if($scope.langcode == 'EN'){
                $scope.othertitle1=$rootScope.Titlearray[61].ContentEN;
                $scope.page1="Page 1";
                $scope.page2="Page 2";
                $scope.page3="Page 3";
                $scope.Questions=$rootScope.Titlearray[62].ContentEN;
                $scope.footer1="(1) Base: By Appointment";
                $scope.footer2="(2) Base: Vehicle ready on the same day of service";
                $scope.footer3="(3) Base: Contacted After Service";
                
             }
            else {
               $scope.othertitle1=$rootScope.Titlearray[61].ContentReganal;
                $scope.page1="หน้า 1";
                $scope.page2="หน้า 2";
                $scope.page3="หน้า 3";
                $scope.Questions=$rootScope.Titlearray[62].ContentReganal;
                $scope.footer1="(1) หมายเหตุ: เฉพาะกลุ่มตัวอย่างที่ทำการนัดหมายล่วงหน้า";
                $scope.footer2="(2) หมายเหตุ: เฉพาะกลุ่มตัวอย่างที่ได้รับบริการเสร็จสิ้นภายในวันเดียวกัน";
                $scope.footer3="(3) หมายเหตุ: เฉพาะกลุ่มตัวอย่างที่ได้รับการติดต่อรับเข้ารับบริการ";
            }
        
        //////////////////////////////////////////////////////////////
            var w = window.innerWidth|| document.documentElement.clientWidth|| document.body.clientWidth;
            var h = window.innerHeight|| document.documentElement.clientHeight|| document.body.clientHeight;

            console.log( " height :" , h);
            console.log( " width :" , w);
            var margin=300;
            var lineheight=25;
            $scope.xstrue=false;
            var weight=50;
            if(w < 1280 && w > 960){margin=250;}
            else if(w < 960 && w > 600){ margin=200;lineheight=12;weight=40;}
            else if(w < 600){margin=150;lineheight=12;weight=30;$scope.xstrue=true;}
            console.log('margin',margin);
         
        
        
        $scope.$on('changeLanguage', function (event, data) {
            $scope.langcode=data;
            $scope.filterchange();
          });
          
          
        $scope.$on('changeyear', function (event, data) {
            $scope.year=data;
            $scope.biannual=$rootScope.ALL;
            $scope.month=$rootScope.ALL;
            $scope.filterchange();
          });
        
        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual=data;
            $scope.month=$rootScope.ALL;
            $scope.filterchange();
          });
          
       $scope.$on('changemonth', function (event, data) {
            $scope.month=data;
            $scope.filterchange();
          });
          
        $scope.$on('changezone', function (event, data) {
            $scope.zone=data;
            $scope.region = $rootScope.ALL;
            $scope.dealer=$rootScope.ALL;
            $scope.filterchange();
          });
          
        $scope.$on('changeregion', function (event, data) {
            $scope.region=data;
            $scope.dealer=$rootScope.ALL;
            $scope.filterchange();
          });
          
          $scope.$on('changemodel', function (event, data) {
            $scope.model=data;
            $scope.filterchange();
          });
          
          $scope.$on('changedealer', function (event, data) {
            $scope.dealer=data;
            $scope.filterchange();
          });
               //////////////////////////////////////////////////////////////
 
        $("#otherexportCS").click(function () {
                        $("#table22").table2excel({
                            // exclude CSS class
                            exclude: ".noExl",
                            filename: "otherTableDownload"
                        });
                });
       
       var parameters = [];
//        $scope.year = $rootScope.Filterarray.year[$rootScope.Filterarray.year.length - 1];
        $scope.region = $rootScope.ALL;
        $scope.dealer = $rootScope.ALL;
        $scope.model = $rootScope.ALL;
        service.ssi_filterfunction();
         parameters = [ {"name":"langCode", "value": $scope.langcode},{"name":"zone","value":encodeURIComponent($scope.zone)},{"name":"dealer","value":$scope.dealer},{"name":"langCode", "value": $scope.langcode}, {"name": "region", "value":encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}];
         //////////////////////////////////////////////////////////////
        var marginp1;
        var marginp2;
        var marginp3;
        var w = window.innerWidth|| document.documentElement.clientWidth|| document.body.clientWidth;
        console.log( " width :" , w);

         if(w <1400)
             {marginp2=80;}
         else
            {marginp2=76;}

         if(w > 1600){
              marginp3=null;
         }
         else
         { marginp3=90;}

          if(w > 1680)
                {marginp1=null;
                       }
            else
                {marginp1=80;
                       }
        console.log('marginp1',marginp1);
        console.log('marginp2',marginp2);
        console.log('marginp3',marginp3);
//////////////////////////////////////////////////////////////
          $scope.graphs = function () {
              
                        $scope.hideloader=false;
               if($scope.langcode == 'EN'){
                    $scope.othertitle1=$rootScope.Titlearray[61].ContentEN;
                    $scope.page1="Page 1";
                    $scope.page2="Page 2";
                    $scope.page3="Page 3";
                    $scope.Questions=$rootScope.Titlearray[62].ContentEN;
                    $scope.footer1="(1) Base: By Appointment";
                    $scope.footer2="(2) Base: Vehicle ready on the same day of service";
                    $scope.footer3="(3) Base: Contacted After Service";
                 }
                else {
                   $scope.othertitle1=$rootScope.Titlearray[61].ContentReganal;
                    $scope.page1="หน้า 1";
                    $scope.page2="หน้า 2";
                    $scope.page3="หน้า 3";
                    $scope.Questions=$rootScope.Titlearray[62].ContentReganal;
                    $scope.footer1="(1) หมายเหตุ: เฉพาะกลุ่มตัวอย่างที่ทำการนัดหมายล่วงหน้า";
                    $scope.footer2="(2) หมายเหตุ: เฉพาะกลุ่มตัวอย่างที่ได้รับบริการเสร็จสิ้นภายในวันเดียวกัน";
                    $scope.footer3="(3) หมายเหตุ: เฉพาะกลุ่มตัวอย่างที่ได้รับการติดต่อรับเข้ารับบริการ";
                }
              
                    //       SSIOthers-getSSIOthersTable
                    service.mastersrv("SSIOthers-getSSIOthersTable", parameters).then(function (response) {
                    $scope.testdata = response.data.data;
                    if(response.data){
                        $scope.hideloader=true;
                    }
                    console.log('response.data',response.data);
                    console.log('response.data.data',response.data.data);
                    console.log('response.data',response.data.samplecount);

                        if(response.data.samplecount < 10){
                             $scope.samplespaceless=true;
                             console.log(  $scope.samplespaceless);
                         }
                        else{
                             $scope.samplespaceless=false;
                             console.log(  $scope.samplespaceless);
                         }


                        $scope.Heading1=response.data.data[0].Heading1;
                        $scope.Heading2=response.data.data[0].Heading2;
                        $scope.Heading3=response.data.data[0].Heading3;
                        $scope.Heading4=response.data.data[0].Heading4;

                        var Value1=response.data.data[0].Heading1;
                        var Value2=response.data.data[0].Heading2;
                        var Value3=response.data.data[0].Heading3;
                        var Value4=response.data.data[0].Heading4;
        //             $scope.format1( $scope.testdata[i].Value1); $scope.format1( $scope.testdata[i].Value2); $scope.format1( $scope.testdata[i].Value3); $scope.format1( $scope.testdata[i].Value4);
                    });

        //SSIOthers-getSSIOthersCharts
                    service.mastersrv("SSIOthers-getSSIOthersCharts", parameters).then(function (response) {
                        $scope.response = response;
                   
                  //page1
                    $scope.heading0=response.data[2].headingname;
                    $('#top01').highcharts(service.stackedgraph(response.data[2].headingdata,color));
                    $scope.heading1=response.data[3].headingname;
                    $('#top02').highcharts(service.stackedgraph(response.data[3].headingdata,color));
                    $scope.heading2=response.data[4].headingname;
                    $('#bot01').highcharts(service.stackedgraph(response.data[4].headingdata,color));
                    $scope.heading3=response.data[7].headingname;
                    $('#bot02').highcharts(service.stackedgraph(response.data[7].headingdata,color));
                    
                    //page2
                    $scope.heading4=response.data[1].headingname;
                    $('#top11').highcharts(service.stackedgraph(response.data[1].headingdata,color));
                    $scope.heading5=response.data[0].headingname;
                    $('#top12').highcharts(service.stackedgraph(response.data[0].headingdata,color));
                    
                    //page3
                    $scope.heading6=response.data[6].headingname;
                    $('#bot11').highcharts(service.stackedgraph(response.data[6].headingdata,color));
                    $scope.heading7=response.data[8].headingname;
                    $('#bot12').highcharts(service.stackedgraph(response.data[8].headingdata,color));
                     $scope.heading8=response.data[5].headingname;
                    $('#bot22').highcharts(service.stackedgraph(response.data[5].headingdata,color));

                    
                    $('#top01x').highcharts(service.stackedgraph(response.data[6].headingdata,color,80));        
                    $('#top02x').highcharts(service.stackedgraph(response.data[2].headingdata,color));
                    $('#bot01x').highcharts(service.stackedgraph(response.data[7].headingdata,color));
                    $('#bot02x').highcharts(service.stackedgraph(response.data[0].headingdata,color));
                    $('#top11x').highcharts(service.stackedgraph(response.data[4].headingdata,color,marginp1));
                    $('#top12x').highcharts(service.stackedgraph(response.data[5].headingdata,color));
                    $('#bot11x').highcharts(service.stackedgraph(response.data[1].headingdata,color,marginp2));
                    $('#bot12x').highcharts(service.stackedgraph(response.data[3].headingdata,color));
                    $('#page0bx').highcharts(service.stackmultigraph(response.data[9].headingdata, color, 25, '11px', '.07'));
                    $('#page0ax').highcharts(service.stackmultigraph(response.data[10].headingdata, color, 25, '11px', '.07'));
                    $('#page0cx').highcharts(service.stackedgraph(response.data[8].headingdata,color));

                     
                    });

                };

        $scope.filterchange = function () {
            parameters = [{"name":"langCode", "value": $scope.langcode},{"name":"zone","value":encodeURIComponent($scope.zone)},{"name":"langCode", "value": $scope.langcode}, {"name": "region", "value":encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model},{"name": "dealer", "value": $scope.dealer}];
            $scope.graphs();
        };
        
    }
})();