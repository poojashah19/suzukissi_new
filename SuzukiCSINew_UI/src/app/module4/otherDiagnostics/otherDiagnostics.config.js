(function() {
    'use strict';

    angular
        .module('app.module4.otherDiagnostics')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

          $stateProvider
          .state('triangular.ssiotherDiagnostics', {
            url: '/ssiotherDiagnostics',
            
             views: {
                '': {
                   templateUrl: 'app/module4/otherDiagnostics/otherDiagnostics.tmpl.html',
            controller: 'm4otherDiagnosticsController',
            controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module4/otherDiagnostics/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        });

//         triMenuProvider.removeMenu('triangular.dealercsipage35'); 
//         triMenuProvider.addMenu({
//                   name: 'Other Diagnostics',
//                    icon: 'zmdi zmdi-assignment-o',
//                    type: 'link',
//                    priority: 1.6,
//                     state: 'triangular.csiotherDiagnostics',
//                });  
                

    }
})();