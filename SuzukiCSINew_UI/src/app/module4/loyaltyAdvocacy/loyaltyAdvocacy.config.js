(function() {
    'use strict';

    angular
        .module('app.module4.loyaltyAdvocacy')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

          $stateProvider
         .state('triangular.ssiloyaltyAdvocacy', {
            url: '/ssiloyaltyAdvocacy',
            views: {
                '': {
                   templateUrl: 'app/module4/loyaltyAdvocacy/loyaltyAdvocacy.tmpl.html',
                    controller: 'm4loyaltyAdvocacyController',
                    controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module4/loyaltyAdvocacy/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    },
                    permissions: {
                    only: ['viewGitHub']
                }
            
        });
        

//       triMenuProvider.removeMenu('triangular.dealercsipage32'); 
//         triMenuProvider.addMenu({
//                    name: 'Loyalty & Advocacy',
//                    icon: 'zmdi zmdi-favorite',
//                    type: 'link',
//                    priority: 2.0,
//                     state: 'triangular.csiloyaltyAdvocacy',
//                }); 
   
    }
})();