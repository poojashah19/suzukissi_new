(function () {
    'use strict';

    angular
            .module('app.module4.productAnalysis')
            .controller('m4productAnalysisController', m4productAnalysisController);

    /* @ngInject */
    function m4productAnalysisController($scope,$state, service, $http, $rootScope) {
        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776","#92D400","#00A1DE","#72C7E7","#3C8A2E","#C9DD03","#335291","#A8DD33","#33B4E5"];
        $rootScope.subpage="";
        $rootScope.subpagexs="";
        $rootScope.subpage+=" ";
        $rootScope.hideyear=false;
         $rootScope.showFilterTooltip=true;
        $rootScope.hidebiannual=false;
        $rootScope.hidemonth=false;
        $rootScope.hidezone=true;
        $rootScope.hideregion=true;
        //special case for product analysis
        $rootScope.isProductAnalysis=true;
         
       $rootScope.$on('$stateChangeStart', 
            function(event, toState, toParams, fromState, fromParams){ 
             
                $rootScope.isProductAnalysis=false;
            });
        //
        $rootScope.hidemodel=true;
        $rootScope.snaphidezone=true;
        $rootScope.snaphideregion=true;
        $rootScope.hidefactor=false;
        $rootScope.hidetrendfactor=true;
        $rootScope.hidetrendfactor=true;
        $rootScope.dealerhide=true;
        $rootScope.dealersnaphide=true;
        $rootScope.pagenumber='10';
        $rootScope.starlabelhide=false;
        $rootScope.legendforselectionb="Best Overall";
        $rootScope.legendforselectionw="Worst Overall";
        $rootScope.priorityhide=true;
        $rootScope.redfeedback="not";
        
        $rootScope.dealerScoreDealer=$rootScope.Filterarray.dealer[0].value;
        $rootScope.summaryRefresh=1;
        $rootScope.dsnapshot="not";
        $scope.zone=$rootScope.ALL;
        $scope.region = $rootScope.ALL;
        $scope.model = $rootScope.ALL;
        $scope.year=$rootScope.Filterarray.year[0].value;
        $scope.biannual = $rootScope.ALL;
        $scope.month=$rootScope.Filterarray.month[1].value;
        
        $rootScope.modelsnap="modelsnap";
        $scope.langcode=$rootScope.language;
        $rootScope.checkboxInitiliser();
        var languagepara=[];
        var modelpara=[];
        var parameters=[];
        $scope.area=$rootScope.ALL;
        $scope.zone=$rootScope.ALL;
        $scope.province=$rootScope.ALL;
        $scope.zone=$rootScope.ALL;
        $scope.region = $rootScope.ALL;
        $scope.model = $rootScope.ALL;
     
        $scope.biannual = $rootScope.ALL;
        $scope.month=$rootScope.Filterarray.month[0].name;
        $scope.region=$rootScope.ALL;
        $scope.city=$rootScope.ALL;
        $scope.factor=$rootScope.Filterarray.factor[0].name;
        $scope.factorname=$rootScope.Filterarray.factor[0].value;
        $scope.model=$rootScope.Filterarray.model[0].name;
        console.log('model model '+$scope.model);
        $scope.modelname=$rootScope.Filterarray.model[0].value+" - ";
         if($scope.model==$rootScope.ALL){
             $scope.modelname=" ";
         }
        
        if($scope.langcode == 'EN'){
            $scope.model1=$rootScope.Titlearray[118].ContentEN;
            $scope.model2=$rootScope.Titlearray[119].ContentEN;
              $scope.mtable1=$rootScope.Titlearray[83].ContentEN;
                    $scope.mtable2=$rootScope.Titlearray[84].ContentEN;
                    $scope.mtable3=$rootScope.Titlearray[85].ContentEN;
                    $scope.mtable4=$rootScope.Titlearray[86].ContentEN;
                    $scope.mtable5=$rootScope.Titlearray[87].ContentEN;
        $rootScope.legendforselectionb="Best Overall";
        $rootScope.legendforselectionw="Worst Overall";
            $scope.mtable0="Model";
            $scope.model = $rootScope.ALL;
            $scope.factorscore="Factor Score";
            $scope.model3=$rootScope.Titlearray[129].ContentEN;
            $scope.footnote=$rootScope.Footarray[1].ContentEN;
         }
        else {
            $scope.model1=$rootScope.Titlearray[118].ContentReganal;
            $scope.model2=$rootScope.Titlearray[119].ContentReganal;
             $scope.mtable1=$rootScope.Titlearray[83].ContentReganal;
             $rootScope.legendforselectionb="ดีที่สุดจากทั่วประเทศ";
            $rootScope.legendforselectionw="แย่ที่สุดจากทั่วประเทศ";
                    $scope.mtable2=$rootScope.Titlearray[84].ContentReganal;
                    $scope.mtable3=$rootScope.Titlearray[85].ContentReganal;
                    $scope.mtable4=$rootScope.Titlearray[86].ContentReganal;
                    $scope.mtable5=$rootScope.Titlearray[87].ContentReganal;
            $scope.mtable0="รุ่นรถ";
            $scope.model = "ทั่วประเทศ";
            $scope.factorscore="คะแนนปัจจัย";
            $scope.model3=$rootScope.Titlearray[129].ContentReganal;
            $scope.footnote=$rootScope.Footarray[1].ContentReganal;
        }
        
        //////////////////////////////////////////////////////////////
            var w = window.innerWidth|| document.documentElement.clientWidth|| document.body.clientWidth;
            var h = window.innerHeight|| document.documentElement.clientHeight|| document.body.clientHeight;

            console.log( " height :" , h);
            console.log( " width :" , w);
            var margin=300;
            var lineheight=25;
            $scope.xstrue=false;
            var weight=50;
            if(w < 1280 && w > 960){margin=250;}
            else if(w < 960 && w > 600){ margin=200;lineheight=12;weight=40;}
            else if(w < 600){margin=150;lineheight=12;weight=30;$scope.xstrue=true;}
            console.log('margin',margin);
        
         
        $scope.$on('changeLanguage', function (event, data) {
            $scope.langcode=data;
            languagepara=[{"name":"english","value":$scope.factor}];
            modelpara=[{"name":"english","value":$scope.model}];
            
                service.mastersrv("Systemuserview-getName",languagepara).then(function (response) {
                        if($scope.langcode == 'EN'){
                            $scope.factorname=response.data[0].ContentEN;
                        }
                        else{ 
                            $scope.factorname=response.data[0].ContentReganal;}
                  });
                  service.mastersrv("Systemuserview-getName",modelpara).then(function (response) {
                        if($scope.langcode == 'EN'){
                            $scope.modelname=response.data[0].ContentEN+" - ";
                        }
                        else{
                            $scope.modelname=response.data[0].ContentReganal+" - ";
                        }
                        if($scope.model==$rootScope.ALL){
                           $scope.modelname=" ";
                       }
                       
                      });
            $scope.filterchange();
          });
          
           $scope.$on('changeyear', function (event, data) {
            $scope.year=data;
            $scope.biannual=$rootScope.ALL;
            $scope.month=$rootScope.ALL;
            $scope.filterchange();
                $scope.botgraph();
          });
        
        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual=data;
            $scope.month=$rootScope.ALL;
            $scope.filterchange();
            $scope.botgraph();
          });
          
       $scope.$on('changemonth', function (event, data) {
            $scope.month=data;
            $scope.filterchange();
          });
          
        $scope.$on('changezone', function (event, data) {
            $scope.zone=data;
            $scope.region = $rootScope.ALL;
            $scope.dealer=$rootScope.ALL;
            $scope.filterchange();
                $scope.botgraph();
          });
          
        $scope.$on('changeregion', function (event, data) {
                $scope.region=data;
                $scope.dealer=$rootScope.ALL;
                $scope.filterchange();
                $scope.botgraph();
          });
          
          $scope.$on('changemodel', function (event, data) {
                $scope.model=data;
                modelpara=[{"name":"english","value":$scope.model}];
                $scope.botgraph();
          });
          
          $scope.$on('changemodelgraph', function (event, data) {
                $scope.model=data;
                modelpara=[{"name":"english","value":$scope.model}];
                $scope.botgraph();
          });
          
          $scope.$on('changefactor', function (event, data) {
                $scope.factor=data;
                languagepara=[{"name":"english","value":$scope.factor}];
                service.mastersrv("Systemuserview-getName",languagepara).then(function (response) {
                        if($scope.langcode == 'EN'){
                                     $scope.factorname=response.data[0].ContentEN;
                        }
                        else{
                                     $scope.factorname=response.data[0].ContentReganal;
                        }
               });
                $scope.botgraph();
          });
               //////////////////////////////////////////////////////////////
 
        $("#modelexportCS").click(function () {
                        $("#table").table2excel({
                            // exclude CSS class
                            exclude: ".noExl",
                            filename: "modelTableDownload"
                        });
                });
        
       
        var botparameters=[];
        
         botparameters=[{"name":"zone","value":encodeURIComponent($scope.zone)},{"name":"factor","value":$scope.factor},{"name":"langCode", "value": $scope.langcode}, {"name": "region", "value":encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model},{"name":"factor","value":$scope.factor}];
             service.ssi_filterfunction();
             
           parameters = [ {"name":"langCode", "value": $scope.langcode},{"name":"zone","value":encodeURIComponent($scope.zone)},{"name":"factor","value":$scope.factor},{"name":"langCode", "value": $scope.langcode}, {"name": "region", "value":encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}];   
        
     $scope.graphs=function(){   
         
           if($scope.langcode == 'EN'){
                    $scope.model1=$rootScope.Titlearray[118].ContentEN;
                    $scope.model2=$rootScope.Titlearray[119].ContentEN;
        $rootScope.legendforselectionb="Best Overall";
        $rootScope.legendforselectionw="Worst Overall";

                    
                    $scope.mtable1=$rootScope.Titlearray[83].ContentEN;
                    $scope.mtable2=$rootScope.Titlearray[84].ContentEN;
                    $scope.mtable3=$rootScope.Titlearray[85].ContentEN;
                    $scope.mtable4=$rootScope.Titlearray[86].ContentEN;
                    $scope.mtable5=$rootScope.Titlearray[87].ContentEN;
                    $scope.mtable0="Product";
                    $scope.model3=$rootScope.Titlearray[129].ContentEN;
                    $scope.footnote=$rootScope.Footarray[0].ContentEN;
            $scope.footnote+='<br>'+$rootScope.Footarray[6].ContentEN;
                 }
                else {
                    $scope.model1=$rootScope.Titlearray[118].ContentReganal;
                    $scope.model2=$rootScope.Titlearray[119].ContentReganal;
                    $rootScope.legendforselectionb="ดีที่สุดจากทั่วประเทศ";
            $rootScope.legendforselectionw="แย่ที่สุดจากทั่วประเทศ";
//                    $scope.mtable1=$rootScope.Titlearray[120].ContentReganal+'(26%)';
//                    $scope.mtable2=$rootScope.Titlearray[121].ContentReganal+'(15%)';
//                    $scope.mtable3=$rootScope.Titlearray[122].ContentReganal+'(12%)';
//                    $scope.mtable4=$rootScope.Titlearray[123].ContentReganal+'(18%)';
//                    $scope.mtable5=$rootScope.Titlearray[124].ContentReganal+'(29%)';
                    $scope.mtable1=$rootScope.Titlearray[83].ContentReganal;
                    $scope.mtable2=$rootScope.Titlearray[84].ContentReganal;
                    $scope.mtable3=$rootScope.Titlearray[85].ContentReganal;
                    $scope.mtable4=$rootScope.Titlearray[86].ContentReganal;
                    $scope.mtable5=$rootScope.Titlearray[87].ContentReganal;
                    $scope.mtable0="รุ่นรถ";
                    $scope.model3=$rootScope.Titlearray[129].ContentReganal;
                   $scope.footnote=$rootScope.Footarray[0].ContentReganal;
                     $scope.footnote+='<br>'+$rootScope.Footarray[6].ContentReganal;
                }

         
         
         //        SSIModelAnalysis-getModelSSIScores 1
                 service.mastersrv("SSIModelAnalysis-getModelSSIScores",parameters).then(function (response) {
                                $('#leftgraph').highcharts(service.modelbarchart('bar','#2979FF ',response.data,100,40,0,'Avg SSI'));
                    });
                     service.mastersrv("SSIModelAnalysis-getModelSSIScores",parameters).then(function (response) {
                                $('#leftgraph1').highcharts(service.modelbarchart('bar','#2979FF ',response.data,100,20,0,'Avg SSI'));
                    });
                    
         //        SSIModelAnalysis-getModelFactorSSIScores  2
                service.mastersrv("SSIModelAnalysis-getModelFactorSSIScores",parameters).then(function (response) {
                                 
                                  
                     $scope.FSmodeltable = {
                                    id: "modeltable",
                                    view: "datatable",
                                    height: 220,

                //                    headerRowHeight: 60,
                                    fixedRowHeight: false,

                                    scroll: 'y',
                                     position:"flex",

                                    select: "row",
                                     hover:"myhover",
                                    css: "my_style",

                                    tooltip: true,
                                    columns: [

                                        {id: "model", css: "columnstyle", header:{text: $scope.mtable0,  css:'multiline'},minWidth:80,adjust: 'true',fillspace:true },
                                        {id: "SalesInitiation",header:{text: $scope.mtable1,  css:'multiline'},cssFormat: mark_serviceinitiation, adjust: 'true'},
                                        {id: "DealerFacility", header:{text: $scope.mtable2,  css:'multiline'},cssFormat: mark_serviceadvisor,adjust: 'true'},
                                        {id: "Deal",header:{text: $scope.mtable3,  css:'multiline'},cssFormat: mark_servicefacility,adjust: 'true' },
                                        {id: "DeliveryTiming", header:{text: $scope.mtable4,  css:'multiline'},cssFormat: mark_vehiclepickup,adjust: 'true'},
                                        {id: "DeliveryProcess", header:{text: $scope.mtable5,  css:'multiline'},cssFormat: mark_servicequality,adjust: 'true'},

                                    ],
                       data: response.data,
                                    resizeColumn: true,


                                };
                    });
                   
                    
                    function mark_serviceinitiation(value, config) {
            if(config["SalesInitiationcolor"]!="White")
                {console.log({"background-color": config["SalesInitiationcolor"],"color": '#FFFFFF !important'});
                    
                    return {"background-color": config["SalesInitiationcolor"],"color": '#FFFFFF !important'}; 
                }
                else
                {
                     return {"background-color": config["SalesInitiationcolor"]}; 
                }
                return value;
    
        }
        
        
        function mark_serviceadvisor(value, config) {
            if(config["DealerFacilitycolor"]!="White")
                {console.log({"background-color": config["DealerFacilitycolor"],"color": '#FFFFFF !important'});
                    
                    return {"background-color": config["DealerFacilitycolor"],"color": '#FFFFFF !important'}; 
                }
                else
                {
                     return {"background-color": config["serviceadvisorcolor"]}; 
                }
                return value;
           
        }
        function mark_servicefacility(value, config) {
            if(config["Dealcolor"]!="White")
                {console.log({"background-color": config["Dealcolor"],"color": '#FFFFFF !important'});
                    
                    return {"background-color": config["Dealcolor"],"color": '#FFFFFF !important'}; 
                }
                else
                {
                     return {"background-color": config["Dealcolor"]}; 
                }
               
            return value;
        }
        function mark_vehiclepickup(value, config) {
             if(config["DeliveryTimingcolor"]!="White")
                {console.log({"background-color": config["DeliveryTimingcolor"],"color": '#FFFFFF !important'});
                    
                    return {"background-color": config["DeliveryTimingcolor"],"color": '#FFFFFF !important'}; 
                }
                else
                {
                     return {"background-color": config["DeliveryTimingcolor"]}; 
                }
               
            return value;
        }
        function mark_servicequality(value, config) {
            if(config["DeliveryProcesscolor"]!="White")
                {console.log({"background-color": config["DeliveryProcesscolor"],"color": '#FFFFFF !important'});
                    
                    return {"background-color": config["DeliveryProcesscolor"],"color": '#FFFFFF !important'}; 
                }
                else
                {
                     return {"background-color": config["DeliveryProcesscolor"]}; 
                }
                return value;
            
        }
//         $scope.botrender();
//            
        };
        
        
        $scope.botrender=function(){    
            
       //        SSIModelAnalysis-getModelFactorTrend 3
                 service.mastersrv("SSIModelAnalysis-getModelFactorTrend",botparameters).then(function (response) {
                                
                     $('#botgraph').highcharts(service.barchartmiddle('column','#92D400 ',response.data.data, 0,40,0,'Model Score',12,1,1000));
                     $('#botgraph1').highcharts(service.barchartmiddle('column','#92D400 ',response.data.data, 0,40,0,'Model Score',12,1,1000));
                             
                                modelpara=[{"name":"english","value":$scope.model}];
                                service.mastersrv("Systemuserview-getName",modelpara).then(function (response) {
                                console.log('response response[0] modelgraph',response.data[0].ContentReganal);
                                   
                                    if($scope.langcode == 'EN'){
                                         if($scope.model==$rootScope.ALL){
                                                $scope.modelname=" ";
                                            }
                                            else{$scope.modelname=response.data[0].ContentEN+" - ";}
                                   }
                                   else{
                                        if($scope.model==$rootScope.ALL){
                                                $scope.modelname=" ";
                                            }
                                            else{ $scope.modelname=response.data[0].ContentReganal+" - ";}
                                   }
                                     });
                                     
                                   
                                   

                    });
        };
        
        
        $scope.filterchange=function(){
                 parameters=[{"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month},{"name":"langCode", "value": $scope.langcode},{"name":"zone","value":encodeURIComponent($scope.zone)},{"name":"province","value":$scope.province},{"name":"region","value":encodeURIComponent($scope.region)},{"name":"year","value":$scope.year},{"name":"city","value":encodeURIComponent($scope.city)}];
                 $scope.graphs();
         };
         
          $scope.botgraph=function(){
                 botparameters=[{"name":"area","value":$scope.area},{"name":"langCode", "value": $scope.langcode},{"name":"zone","value":encodeURIComponent($scope.zone)},{"name":"province","value":$scope.province},{"name":"region","value":encodeURIComponent($scope.region)},{"name":"year","value":$scope.year},{"name":"biannual","value":$scope.biannual},{"name":"model","value":$scope.model},{"name":"factor","value":$scope.factor}];
                 $scope.botrender();
         };
        
        
        
        
    }
})();