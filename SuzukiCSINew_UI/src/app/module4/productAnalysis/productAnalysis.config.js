(function() {
    'use strict';

    angular
        .module('app.module4.productAnalysis')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

          $stateProvider
          .state('triangular.ssiproductAnalysis', {
            url: '/ssiproductAnalysis',
            
             views: {
                '': {
                   templateUrl: 'app/module4/productAnalysis/productAnalysis.tmpl.html',
            controller: 'm4productAnalysisController',
            controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module4/productAnalysis/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        });
        
//         triMenuProvider.addMenu({
//                   name: 'Product Analysis',
//                    icon: 'zmdi zmdi-car-taxi',
//                    type: 'link',
//                    priority: 1.9,
//                     state: 'triangular.csiproductAnalysis',
//                });
//             triMenuProvider.removeMenu('triangular.productAnalysis');   
                
                
    }
})();