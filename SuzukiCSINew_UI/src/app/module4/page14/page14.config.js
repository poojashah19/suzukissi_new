(function() {
    'use strict';

    angular
        .module('app.module4.page14')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

           $stateProvider
         .state('triangular.ssipage14', {
            url: '/ssipage14',
            views: {
                '': {
                   templateUrl: 'app/module4/page14/page14.tmpl.html',
                    controller: 'm4page14Controller',
                    controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module4/page14/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
            
        });
        
//         triMenuProvider.addMenu({
//                      name: 'Other Feedback',
//                    icon: 'zmdi zmdi-view-list-alt',
//                    type: 'link',
//                    priority: 2.3,
//                     state: 'triangular.csipage14',
//                });
//             triMenuProvider.removeMenu('triangular.page14');   
                
                
    }
})();