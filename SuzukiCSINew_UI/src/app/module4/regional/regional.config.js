(function() {
    'use strict';

    angular
        .module('app.module4.regional')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

          $stateProvider
          .state('triangular.ssiregional', {
            url: '/ssiregional',
            
             views: {
                '': {
                  templateUrl: 'app/module4/regional/regional.tmpl.html',
            controller: 'm4regionalController',
            controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module4/regional/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        });
        
//         triMenuProvider.addMenu({
//                   name: 'Regional Analysis',
//                    icon: 'zmdi zmdi-map',
//                    type: 'link',
//                    priority: 1.1,
//                     state: 'triangular.csiregional',
//                });
//             triMenuProvider.removeMenu('triangular.regional');   
                
                
    }
})();