(function () {
    'use strict';

    angular
            .module('app.module4.regional')
            .controller('m4regionalController', m4regionalController);

    /* @ngInject */
    function m4regionalController($scope,$state, service, $http, $rootScope) {
        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776","#92D400","#00A1DE","#72C7E7","#3C8A2E","#C9DD03","#335291","#A8DD33","#33B4E5"];
        $rootScope.subpage="";
        $rootScope.subpagexs="";
        $rootScope.subpage+=" ";
        $rootScope.hideyear=false;
         $rootScope.showFilterTooltip=true;
        $rootScope.hidebiannual=false;
        $rootScope.hidemonth=false;
        $rootScope.hidezone=false;
        $rootScope.hideregion=false;
        $rootScope.hidemodel=true;
        $rootScope.snaphidezone=true;
        $rootScope.snaphideregion=true;
        $rootScope.hidefactor=true;
        $rootScope.hidetrendfactor=true;
        $rootScope.dealersnaphide=true
        $rootScope.dealerhide=true;
        $rootScope.pagenumber='2';
        $rootScope.dsnapshot = "not";
        $rootScope.legendname="Study Best";    
        $rootScope.legendnamew="Worst Score";    
        $rootScope.starlabelhide=false;
        $rootScope.priorityhide=true;
        $rootScope.summaryRefresh=1;
        $rootScope.modelsnap="not";
        $scope.langcode=$rootScope.language;
        $scope.zone=$rootScope.ALL;
        $scope.region = $rootScope.ALL;
        $scope.model = $rootScope.ALL;
        $scope.year=$rootScope.Filterarray.year[0].value;
        $scope.biannual = $rootScope.ALL;
        $scope.month=$rootScope.Filterarray.month[1].value;
        $rootScope.dealerScoreDealer=$rootScope.Filterarray.dealer[0].value;
        $scope.svgname=$rootScope.Filterarray.factor[0].value;
        $rootScope.redfeedback="not";
        $rootScope.checkboxInitiliser();
        //////////////////////////////////////////////////////////////
            var w = window.innerWidth|| document.documentElement.clientWidth|| document.body.clientWidth;
            var h = window.innerHeight|| document.documentElement.clientHeight|| document.body.clientHeight;

            console.log( " height :" , h);
            console.log( " width :" , w);
            var margin=300;
            var lineheight=25;
            $scope.xstrue=false;
            var weight=50;
            if(w < 1280 && w > 960){margin=250;}
            else if(w < 960 && w > 600){ margin=200;lineheight=12;weight=40;}
            else if(w < 600){margin=150;lineheight=12;weight=30;$scope.xstrue=true;}
            console.log('margin',margin);
        
        
           if($scope.langcode == 'EN'){
                    $scope.factorl="Factor";
                    $scope.regfacttitle=$rootScope.Titlearray[11].ContentEN;
                    $scope.byregiontitle=$rootScope.Titlearray[72].ContentEN;
                    $scope.meantitle=$rootScope.Titlearray[73].ContentEN;
                    $scope.percenttitle=$rootScope.Titlearray[74].ContentEN;
                    $scope.factorscore="Factor Score";
                    $scope.attscore='Attribute Score';
                   $scope.footnote=$rootScope.Footarray[1].ContentEN;
                 }
            else {
               $scope.regfacttitle=$rootScope.Titlearray[11].ContentReganal;
                $scope.byregiontitle=$rootScope.Titlearray[72].ContentReganal;
                $scope.meantitle=$rootScope.Titlearray[73].ContentReganal;
                $scope.percenttitle=$rootScope.Titlearray[74].ContentReganal;
                $scope.factorscore="คะแนนปัจจัย";
                $scope.attscore='คุณลักษณะ คะแนน';
                $scope.factorl='ปัจจัยต่างๆ';
                $scope.footnote=$rootScope.Footarray[1].ContentReganal;
            }
        
        
            $scope.state=$rootScope.ALL;
            $scope.zone=$rootScope.ALL;
            $scope.region = $rootScope.ALL;
            $scope.model = $rootScope.ALL;
            $scope.year=$rootScope.Filterarray.year[0].value;
            $scope.biannual = $rootScope.ALL;
        $scope.month=$rootScope.Filterarray.month[0].name;
            $scope.legendname="Study Best";
            $scope.legendnamew="Worst Score";
            $scope.model=$rootScope.ALL;
            $scope.city=$rootScope.ALL;
            $scope.factor=$rootScope.Filterarray.factor[0].name;
            console.log()
            var parameters=[{"name":"factor","value":$scope.factor},{"name":"langCode", "value": $scope.langcode}, {"name": "region", "value":encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model} ];

         
        
        $scope.$on('changeLanguage', function (event, data) {
            $scope.langcode=data;
            $scope.filterchange();
          });
          
            $scope.$on('changeyear', function (event, data) {
            $scope.year=data;
            $scope.biannual=$rootScope.ALL;
            $scope.month=$rootScope.ALL;
            $scope.filterchange();
          });
        
        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual=data;
            $scope.month=$rootScope.ALL;
            $scope.filterchange();
          });
          
       $scope.$on('changemonth', function (event, data) {
            $scope.month=data;
            $scope.filterchange();
          });
          
        $scope.$on('changezone', function (event, data) {
            $scope.zone=data;
            $scope.region = $rootScope.ALL;
            $scope.dealer=$rootScope.ALL;
            $scope.filterchange();
          });
                    
        $scope.$on('changeregionone', function (event, data) {
            $scope.region=data;
            $scope.dealer=$rootScope.ALL;
            $scope.filterchange1();
          });
          
          $scope.$on('changemodel', function (event, data) {
            $scope.model=data;
            $scope.filterchange();
          });
          
          
           $scope.factorchanged=function(factor){
                    $scope.factor = factor;
//                    $scope.svgname=factor.value;
                    $scope.filterchange();
                };
               //////////////////////////////////////////////////////////////
 
                
       
          $scope.graphs=function(){  
              
               if($scope.langcode == 'EN'){
                        $scope.regfacttitle=$rootScope.Titlearray[11].ContentEN;
                        $scope.byregiontitle=$rootScope.Titlearray[72].ContentEN;
                        $scope.meantitle=$rootScope.Titlearray[73].ContentEN;
                        $scope.percenttitle=$rootScope.Titlearray[74].ContentEN;
                        $scope.factorscore="Factor Score";
                        $scope.attscore='Attribute Score';
                           $scope.footnote=$rootScope.Footarray[2].ContentEN;
            $scope.footnote+='<br>'+$rootScope.Footarray[0].ContentEN;
            $scope.footnote+='<br>'+$rootScope.Footarray[6].ContentEN;
                     }
                    else {
                       $scope.regfacttitle=$rootScope.Titlearray[11].ContentReganal;
                        $scope.byregiontitle=$rootScope.Titlearray[72].ContentReganal;
                        $scope.meantitle=$rootScope.Titlearray[73].ContentReganal;
                        $scope.percenttitle=$rootScope.Titlearray[74].ContentReganal;
                        $scope.factorscore="คะแนนปัจจัย";
                        $scope.attscore='คุณลักษณะ คะแนน';
                            $scope.footnote=$rootScope.Footarray[2].ContentReganal;
                     $scope.footnote+='<br>'+$rootScope.Footarray[0].ContentReganal;
                      $scope.footnote+='<br>'+$rootScope.Footarray[6].ContentReganal;
                    }
              
              
                    //SSIRegional-getSSIFactorAverage
                    service.mastersrv("SSIRegional-getSSIFactorAverage",parameters).then(function (response) {
                        console.log('response.data.data',response.data.data);
                        console.log('response.data',response.data.samplecount);
                        $scope.svgname=response.data.data[0].Factor;
                        if(response.data.samplecount < 10){
                             $scope.samplespaceless=true;
                             console.log(  $scope.samplespaceless);
                         }
                        else{
                             $scope.samplespaceless=false;
                             console.log(  $scope.samplespaceless);
                         }
                         
                         if(response.data.data.length==0){
                                response.data.data=[{"bartitle":"100%"},{"Score":"0"}];
                            }
                            $scope.svgvalue=response.data.data[0].Score;
//                            $scope.svgname=response.data.data[0].Factor;
                        });
                        
                        
                //SSIRegional-getSSIRegionalFactorAverage
                  service.mastersrv("SSIRegional-getSSIRegionalFactorAverage",parameters).then(function (response) {
                               $('#barlinecontainer').highcharts(service.regplainbar('column',color,response.data,$scope.factorscore,0,60,.3,[0],8,true));
                                  $('#barlinecontainer1').highcharts(service.regplainbar('column',color,response.data,$scope.factorscore,0,60,.3,[0],8,true));
                                  
                                  
                  });


                  //SSIRegional-getSSIRegionalAttributeAverage
                  service.mastersrv("SSIRegional-getSSIRegionalAttributeAverage",parameters).then(function (response) {
                            $('#regbot1').highcharts(service.barplainbar('bar',['#2979FF'],response.data,$scope.attscore,170,35,0,[0],5,false," "));
                            $('#regbot11').highcharts(service.barplainbar('bar',['#2979FF'],response.data,$scope.attscore,150,35,0,[0],5,false," "));
                                $rootScope.legendforselectionb= response.data[0].scatter;
                                $rootScope.legendforselectionw=response.data[0].scatter1;
                  });


               //SSIRegional-getSSIDissatisfied
                service.mastersrv("SSIRegional-getSSIDelighted",parameters).then(function (response) {
                    
                            $('#regbot2').highcharts(service.barplainbar('bar',['#2979FF'],response.data,$scope.attscore,170,35,0,[0],5,false,"%"));
                            $('#regbot21').highcharts(service.barplainbar('bar',['#2979FF'],response.data,$scope.attscore,150,35,0,[0],5,false,"%"));
                  
               });
        };
      
         $scope.isSemi = false;
            $scope.getStyle = function(){
            
                var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

                return {
                    'top': $scope.isSemi ? 'auto' : '50%',
                    'bottom': $scope.isSemi ? '5%' : 'auto',
                    'left': '50%',
                    'transform': transform,
                    '-moz-transform': transform,
                    '-webkit-transform': transform,
                    'font-size': '36px'
                };
            };
            
             $scope.getStyle1 = function(){
            
                var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

                return {
                    'top': $scope.isSemi ? 'auto' : '50%',
                    'bottom': $scope.isSemi ? '5%' : 'auto',
                    'left': '50%',
                    'transform': transform,
                    '-moz-transform': transform,
                    '-webkit-transform': transform,
                    'font-size': '36px'
                };
            };
            
            
            
                
             $scope.filterchange=function(){
                         parameters=[{"name":"langCode", "value": $scope.langcode}, {"name": "zone", "value":encodeURIComponent($scope.zone)},  {"name": "region", "value":encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model},{"name":"factor","value":$scope.factor}];
                         $scope.graphs();
                 }   ;
            
            
//             $scope.filterchange1=function(){
//                         parameters=[{"name":"langCode", "value": $scope.langcode}, {"name": "zone", "value":encodeURIComponent($scope.zone)},  {"name": "region", "value":encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model},{"name":"factor","value":$scope.factor}];
//                         $scope.graphs1();
//                 }   ;
//       
        
    }
})();