(function () {
    'use strict';

    angular
            .module('app.module4.trendanalysis')
            .controller('m4trendanalysisController', m4trendanalysisController);

    /* @ngInject */
    function m4trendanalysisController($scope,$state, service, $http, $rootScope) {
        
       
        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776","#92D400","#00A1DE","#72C7E7","#3C8A2E","#C9DD03","#335291","#A8DD33","#33B4E5"];
        $rootScope.subpage="";
        $rootScope.subpagexs="";
        $rootScope.subpage+=" ";
        $rootScope.showFilterTooltip=false;
        $rootScope.hideyear=false;
        $rootScope.hidebiannual=false;
        $rootScope.hidemonth=false;
        $rootScope.hidezone=false;
        $rootScope.hideregion=false;
        $rootScope.snaphidezone=true;
        $rootScope.snaphideregion=true;
        $rootScope.hidemodel=true;
        $rootScope.hidefactor=true;
        $rootScope.hidetrendfactor=false;
        $rootScope.dealersnaphide=true
        $rootScope.dealerhide=true;
        $rootScope.pagenumber='3';
        $rootScope.starlabelhide=false;
        $rootScope.legendname="Study Best";
        $rootScope.legendnamew="Worst Score";
        $rootScope.priorityhide=true;
        $rootScope.summaryRefresh=1;
        $scope.langcode=$rootScope.language;
        $rootScope.dealerScoreDealer=$rootScope.Filterarray.dealer[0].value;
        $rootScope.redfeedback="not";
        $rootScope.checkboxInitiliser();
        
         var parameters=[];
        var botparameters=[];
        var languagepara=[];
        $scope.legendname="Study Best";
        $scope.legendnamew="Worst Score";
        $scope.factor="SSI";
        $scope.factorname=$rootScope.Filterarray.factor[0].value;
        $scope.zone=$rootScope.ALL;
        $scope.region =$rootScope.ALL;
        $scope.model =$rootScope.ALL;
        $scope.year=$rootScope.Filterarray.year[0].value;
        $scope.biannual =$rootScope.ALL;
        $scope.month = $rootScope.Filterarray.month[1].value;
        
      if($scope.langcode == 'EN'){
                    $scope.trend1=$rootScope.Titlearray[27].ContentEN;
                    $scope.trend2=$rootScope.Titlearray[75].ContentEN;
                    $scope.trend3=$rootScope.Titlearray[76].ContentEN;
                    $scope.footnote=$rootScope.Footarray[1].ContentEN;
                    $scope.factorscore="Factor Score";
                    $scope.ssiscore='SSI Score';
                 }
                else {
                    $scope.trend1=$rootScope.Titlearray[27].ContentReganal;
                    $scope.trend2=$rootScope.Titlearray[75].ContentReganal;
                    $scope.trend3=$rootScope.Titlearray[76].ContentReganal;
                    $scope.footnote=$rootScope.Footarray[1].ContentReganal;
                    $scope.factorscore="คะแนนปัจจัย";
                    $scope.ssiscore='SSI คะแนน';
                }

        
        //////////////////////////////////////////////////////////////
            var w = window.innerWidth|| document.documentElement.clientWidth|| document.body.clientWidth;
            var h = window.innerHeight|| document.documentElement.clientHeight|| document.body.clientHeight;

            console.log( " height :" , h);
            console.log( " width :" , w);
            var margin=300;
            var lineheight=25;
            $scope.xstrue=false;
            var weight=50;
            if(w < 1280 && w > 960){margin=250;}
            else if(w < 960 && w > 600){ margin=200;lineheight=12;weight=40;}
            else if(w < 600){margin=150;lineheight=12;weight=30;$scope.xstrue=true;}
            console.log('margin',margin);
        
          
        $scope.$on('changeLanguage', function (event, data) {
            $scope.langcode=data;
            languagepara=[{"name":"english","value":$scope.factor}];
            service.mastersrv("Systemuserview-getName",languagepara).then(function (response) {
                    console.log('response response[0]',response.data[0].ContentReganal);
                     if($scope.langcode == 'EN'){
                        $scope.factorname=response.data[0].ContentEN;
                    }
                    else{
                        $scope.factorname=response.data[0].ContentReganal;
                    }
                });
            $scope.filterchange();
          });
          
          $scope.$on('changeyear', function (event, data) {
            $scope.year=data;
            $scope.biannual=$rootScope.ALL;
            $scope.month=$rootScope.ALL;
            $scope.filterchange();
          });
        
        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual=data;
            $scope.month=$rootScope.ALL;
            $scope.filterchange();
          });
          
       $scope.$on('changemonth', function (event, data) {
            $scope.month=data;
            $scope.filterchange();
          });
          
        $scope.$on('changezone', function (event, data) {
            $scope.zone=data;
            $scope.region =$rootScope.ALL;
            $scope.dealer=$rootScope.ALL;
            $scope.filterchange();
          });
          
        $scope.$on('changeregion', function (event, data) {
            $scope.region=data;
            $scope.dealer=$rootScope.ALL;
            $scope.filterchange();
          });
          
          $scope.$on('changemodel', function (event, data) {
            $scope.model=data;
            $scope.filterchange();
          });
          
          $scope.$on('changefactor', function (event, data) {
            $scope.factor=data;
            languagepara=[{"name":"english","value":$scope.factor}];
            service.mastersrv("Systemuserview-getName",languagepara).then(function (response) {
                    console.log('response response[0]',response.data[0].ContentReganal);
                     if($scope.langcode == 'EN'){
                        $scope.factorname=response.data[0].ContentEN;
                    }
                    else{
                        $scope.factorname=response.data[0].ContentReganal;
                    }
                
               });
           
             $scope.botgraph();
          });
               //////////////////////////////////////////////////////////////
 
       
       
        parameters=[{"name":"zone","value":encodeURIComponent($scope.zone)},{"name":"factor","value":$scope.factor},{"name":"langCode", "value": $scope.langcode}, {"name": "region", "value":encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}];
        $scope.graphs=function(){   

             if($scope.langcode == 'EN'){
                    $scope.trend1=$rootScope.Titlearray[27].ContentEN;
                    $scope.trend2=$rootScope.Titlearray[75].ContentEN;
                    $scope.trend3=$rootScope.Titlearray[76].ContentEN;
                    $scope.footnote=$rootScope.Footarray[4].ContentEN;
            $scope.footnote+='<br>'+$rootScope.Footarray[0].ContentEN;
            $scope.footnote+='<br>'+$rootScope.Footarray[6].ContentEN;
                    $scope.factorscore="Factor Score";
                    $scope.ssiscore='SSI Score';
                 }
                else {
                    $scope.trend1=$rootScope.Titlearray[27].ContentReganal;
                    $scope.trend2=$rootScope.Titlearray[75].ContentReganal;
                    $scope.trend3=$rootScope.Titlearray[76].ContentReganal;
                   $scope.footnote=$rootScope.Footarray[4].ContentReganal;
                     $scope.footnote+='<br>'+$rootScope.Footarray[0].ContentReganal;
                      $scope.footnote+='<br>'+$rootScope.Footarray[6].ContentReganal;
                    $scope.factorscore="คะแนนปัจจัย";
                    $scope.ssiscore='SSI คะแนน';
                }

            //  SSITrendAnalysis-getSSITrendByWave 1
                       service.mastersrv("SSITrendAnalysis-getSSITrendByWave",parameters).then(function (response) {
                            console.log('response.data',response.data);
                            console.log('response.data.data',response.data.data);
                            console.log('response.data.samplecount',response.data.samplecount);
                            if(response.data.samplecount < 10){
                                 $scope.samplespaceless=true;
                                 console.log(  $scope.samplespaceless);
                            }
                            else{
                                 $scope.samplespaceless=false;
                                 console.log(  $scope.samplespaceless);
                            }

                            $('#top1').highcharts(service.summaryplainbar('column',['#2979FF'],response.data.data,$scope.ssiscore,0,25,.3,[1,1,1,1,1,1],5,false));
                               $('#top11').highcharts(service.summaryplainbar('column',['#2979FF'],response.data.data,$scope.ssiscore,0,25,.3,[1,1,1,1,1,1],5,false));
//                                $rootScope.legendforselectionb= response.data[0].scatter;
//                                $rootScope.legendforselectionw=response.data[0].scatter1;
                       });

            //  SSITrendAnalysis-getSSIFactorTrendByWave 2
                       service.mastersrv("SSITrendAnalysis-getSSIFactorTrendByWave",parameters).then(function (response) {
                             $('#top2').highcharts(service.summaryplainbar('column',['#7E57C2'],response.data,$scope.factorscore,0,25,.3,[1,1,1,1,1,1],5,false));
                               $('#top21').highcharts(service.summaryplainbar('column',['#7E57C2'],response.data,$scope.factorscore,0,25,.3,[1,1,1,1,1,1],5,false));
                                 $rootScope.legendforselectionb= response.data[0].scatter;
                                $rootScope.legendforselectionw=response.data[0].scatter1;
                       });

            //  SSITrendAnalysis-getSSIAttributeTrendByWave 3
                    service.mastersrv("SSITrendAnalysis-getSSIAttributeTrendByWave",parameters).then(function (response) {
                                      $('#bot').highcharts(service.horizontalline(response.data,color));
                                       $('#bot1').highcharts(service.horizontalline(response.data,color));
                      });
                  } ;
                  
                  
                   $scope.botrender=function(){
                    //  SSITrendAnalysis-getSSIFactorTrendByWave 2
                               service.mastersrv("SSITrendAnalysis-getSSIFactorTrendByWave",botparameters).then(function (response) {
                                     $('#top2').highcharts(service.summaryplainbar('column',['#7E57C2'],response.data,$scope.factorscore,0,25,.3,[1,1,1,1,1,1],5,false));
                              $scope.legendname=response.data[0].scatter;
                        $rootScope.legendnamew=response.data[0].scatter1;
                               });

                    //  SSITrendAnalysis-getSSIAttributeTrendByWave 3
                            service.mastersrv("SSITrendAnalysis-getSSIAttributeTrendByWave",botparameters).then(function (response) {
                                      $('#bot').highcharts(service.horizontalline(response.data,color));
                                       $('#bot1').highcharts(service.horizontalline(response.data,color));
                      });
            
        };
                  
             $scope.filterchange=function(){
                             parameters=[{"name":"zone","value":encodeURIComponent($scope.zone)},{"name":"langCode", "value": $scope.langcode}, {"name": "region", "value":encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month},{"name":"factor","value":$scope.factor}];
                              $scope.graphs();
                     };
                     

               $scope.botgraph=function(){
                    botparameters=[{"name":"zone","value":encodeURIComponent($scope.zone)},{"name":"langCode", "value": $scope.langcode}, {"name": "region", "value":encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month},{"name":"factor","value":$scope.factor}];
                    $scope.botrender();
              };
    }
})();