(function() {
    'use strict';

    angular
        .module('app.module4.trendanalysis')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {
 
          $stateProvider
          .state('triangular.ssitrendanalysis', {
            url: '/ssitrendanalysis',
            
             views: {
                '': {
                   templateUrl: 'app/module4/trendanalysis/trendanalysis.tmpl.html',
            controller: 'm4trendanalysisController',
            controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module4/trendanalysis/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        });

   
                
    }
})();