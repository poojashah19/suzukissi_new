
(function () {
    'use strict';

    angular
            .module('app.module4.priorityAnalysis')
            .controller('m4priorityAnalysisController', m4priorityAnalysisController);

    /* @ngInject */
    function m4priorityAnalysisController($scope, service,$state, $http, $rootScope) {
        
        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776","#92D400","#00A1DE","#f8ae19","#72C7E7","#3C8A2E","#335291","#A8DD33","#33B4E5"];
        $rootScope.subpage="";
        $rootScope.subpagexs="";
        $rootScope.subpage+=" ";
        $rootScope.pagenumber='4';
        $rootScope.showFilterTooltip=true;
         $rootScope.showMainSelectionTooltip=false;
        $rootScope.starlabelhide=true;
        $rootScope.dealersnaphide=true
        $rootScope.priorityhide=false;
        $rootScope.dealerpriority=false;
        $rootScope.dealerpriorityhide=true;
        $rootScope.snaphidezone=true;
        $rootScope.snaphideregion=true;
        $rootScope.summaryRefresh=1;
        $rootScope.redfeedback="not";
        $rootScope.checkboxInitiliser();
        $scope.langcode=$rootScope.language;
        $scope.zone=$rootScope.ALL;
        $scope.region = $rootScope.ALL;
        $scope.model = $rootScope.ALL;
        $scope.year=$rootScope.Filterarray.year[0].value;
        $scope.biannual =  $rootScope.Filterarray.biannualx[0].value;
        $scope.month =  $rootScope.Filterarray.month[1].value;
        
        
        if($scope.langcode == 'EN'){
                    $rootScope.choosecomp=$rootScope.Titlearray[40].ContentEN;
                    $rootScope.regcomp=$rootScope.Titlearray[41].ContentEN;
                    $rootScope.dealercomp=$rootScope.Titlearray[42].ContentEN;
                    $rootScope.reg1=$rootScope.Titlearray[43].ContentEN;
                    $rootScope.reg2=$rootScope.Titlearray[42].ContentEN;
                    $rootScope.dealer1=$rootScope.Titlearray[45].ContentEN;
                    $rootScope.dealer2=$rootScope.Titlearray[45].ContentEN;
                    $scope.footnote=$rootScope.Footarray[1].ContentEN;
                 }
                else {
                    $rootScope.choosecomp=$rootScope.Titlearray[40].ContentReganal;
                    $rootScope.regcomp=$rootScope.Titlearray[41].ContentReganal;
                    $rootScope.dealercomp=$rootScope.Titlearray[42].ContentReganal;
                    $rootScope.reg1=$rootScope.Titlearray[43].ContentReganal;
                    $rootScope.reg2=$rootScope.Titlearray[42].ContentReganal;
                    $rootScope.dealer1=$rootScope.Titlearray[45].ContentReganal;
                    $rootScope.dealer2=$rootScope.Titlearray[45].ContentReganal;
                    $scope.footnote=$rootScope.Footarray[1].ContentReganal;
                }
           
        
        
        
        
        //////////////////////////////////////////////////////////////
            var w = window.innerWidth|| document.documentElement.clientWidth|| document.body.clientWidth;
            var h = window.innerHeight|| document.documentElement.clientHeight|| document.body.clientHeight;

            console.log( " height :" , h);
            console.log( " width :" , w);
            var margin=300;
            var lineheight=25;
            $scope.xstrue=false;
            var weight=50;
            if(w < 1280 && w > 960){margin=250;}
            else if(w < 960 && w > 600){ margin=200;lineheight=12;weight=40;}
            else if(w < 600){margin=150;lineheight=12;weight=30;$scope.xstrue=true;}
            console.log('margin',margin);
        //////////////////////////////////////////////////////////////
        
        $scope.$on('changeLanguage', function (event, data) {
            $scope.langcode=data;
            if($scope.default=='dealer'){
                    $scope.dealerfilterchange();}
                else if($scope.default =='region'){
                    $scope.regionfilterchange();
                } 
          });
          
          
       
          
          $scope.$on('changeRadioSwitch', function (event, data) {
                if(data=='dealer'){
                    $scope.default='dealer';
                    $scope.dealerfilterchange();}
                else if(data =='region'){
                    $scope.default='region';
                    $scope.regionfilterchange();
                } 
          });
        
        
         $scope.$on('changeregion1', function (event, data) {
             $scope.region1=data;
                   { $scope.regionfilterchange();}
          });
          $scope.$on('changeregion2', function (event, data) {
              $scope.region2=data;
                   { $scope.regionfilterchange();}
          });
          
           $scope.$on('changedealer1', function (event, data) {
               $scope.dealer1=data;
                    {$scope.dealerfilterchange();}
          });
          $scope.$on('changedealer2', function (event, data) {
              $scope.dealer2=data;
                    {$scope.dealerfilterchange();}
          });
          
           $scope.$on('changebiannual', function (event, data) {
            $scope.biannual=data;
             if($scope.default=='dealer'){
                    $scope.default='dealer';
                    $scope.dealerfilterchange();}
                else if($scope.default =='region'){
                    $scope.default='region';
                    $scope.regionfilterchange();
                } 
//            $scope.filterchange();
          });
          
          
               //////////////////////////////////////////////////////////////
             var vm = this;
            $scope.radioSwitch="Region Comparison";
            $scope.default="region";

            $scope.samplespaceless=false;
            $scope.region1=$rootScope.Filterarray.regionPriority[0].value;
            $scope.region2=$rootScope.Filterarray.regionPriority[1].value;
            $scope.dealer1=$rootScope.Filterarray.dealer[0].value;
            $scope.dealer2=$rootScope.Filterarray.dealer[1].value;
    
            var parameters=[{"name":"measure","value":$scope.default},{"name":"langCode", "value": $scope.langcode},{"name":"region1","value":encodeURIComponent($scope.region1)},{"name":"region2","value":encodeURIComponent($scope.region2)}, {"name": "biannual", "value": $scope.biannual}];
//            service.ssi_filterfunction();

                    $scope.onchangeRadioSwitch=function(radio){
                        if(radio== 'Dealer Comparison'){
                            $scope.priorityRegionHide=true;
                            $scope.default='dealer';
                            $scope.dealerfilterchange();
                        }
                        else{
                            $scope.priorityRegionHide=false;
                            $scope.default='region';
                            $scope.regionfilterchange();
                        }
                    };
        
       $scope.graphs=function(){
           if($scope.langcode == 'EN'){
                    $rootScope.choosecomp=$rootScope.Titlearray[40].ContentEN;
                    $rootScope.regcomp=$rootScope.Titlearray[41].ContentEN;
                    $rootScope.dealercomp=$rootScope.Titlearray[42].ContentEN;
                    $rootScope.reg1=$rootScope.Titlearray[43].ContentEN;
                    $rootScope.reg2=$rootScope.Titlearray[42].ContentEN;
                    $rootScope.dealer1=$rootScope.Titlearray[45].ContentEN;
                    $rootScope.dealer2=$rootScope.Titlearray[45].ContentEN;
                    $scope.footnote=$rootScope.Footarray[4].ContentEN;
            $scope.footnote+='<br>'+$rootScope.Footarray[0].ContentEN;
            $scope.footnote+='<br>'+$rootScope.Footarray[6].ContentEN;
                 }
                else {
                    $rootScope.choosecomp=$rootScope.Titlearray[40].ContentReganal;
                    $rootScope.regcomp=$rootScope.Titlearray[41].ContentReganal;
                    $rootScope.dealercomp=$rootScope.Titlearray[42].ContentReganal;
                    $rootScope.reg1=$rootScope.Titlearray[43].ContentReganal;
                    $rootScope.reg2=$rootScope.Titlearray[42].ContentReganal;
                    $rootScope.dealer1=$rootScope.Titlearray[45].ContentReganal;
                    $rootScope.dealer2=$rootScope.Titlearray[45].ContentReganal;
                    $scope.footnote=$rootScope.Footarray[4].ContentReganal;
                     $scope.footnote+='<br>'+$rootScope.Footarray[0].ContentReganal;
                      $scope.footnote+='<br>'+$rootScope.Footarray[6].ContentReganal;
                }
           
           
            service.mastersrv("SSIPriority-getSSIPriority",parameters).then(function (response) {
                      $('#mainchart').highcharts(service.verticalline(color,true,response.data,'left','top',true,'-100','100','2'));
                       $('#mainchart1').highcharts(service.verticalline(color,true,response.data,'left','top',true,'-100','100','2'));
           
            });
        };
       
      
         $scope.dealerfilterchange=function(){
                 parameters=[{"name":"measure","value":$scope.default},{"name":"langCode", "value": $scope.langcode},{"name":"dealer1","value":encodeURIComponent($scope.dealer1)},{"name":"dealer2","value":encodeURIComponent($scope.dealer2)}, {"name": "biannual", "value": $scope.biannual}];
                 $scope.graphs();
         };
         
          $scope.regionfilterchange=function(){
                 parameters=[{"name":"measure","value":$scope.default},{"name":"langCode", "value": $scope.langcode},{"name":"region1","value":encodeURIComponent($scope.region1)},{"name":"region2","value":encodeURIComponent($scope.region2)}, {"name": "biannual", "value": $scope.biannual}];
                 $scope.graphs();
         };

       
        
    }
})();