(function() {
    'use strict';

    angular
        .module('app.module4.priorityAnalysis')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

          $stateProvider
         .state('triangular.ssipriorityAnalysis', {
            url: '/ssipriorityAnalysis',
            views: {
                '': {
                   templateUrl: 'app/module4/priorityAnalysis/priorityAnalysis.tmpl.html',
                    controller: 'm4priorityAnalysisController',
                    controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module4/priorityAnalysis/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
            
        });
        

//        triMenuProvider.removeMenu('triangular.dealercsipage31'); 
//         triMenuProvider.addMenu({
//                    name: 'Priority Analysis',
//                    icon: 'zmdi zmdi-badge-check',
//                    type: 'link',
//                    priority: 1.3,
//                     state: 'triangular.csipriorityAnalysis',
//                });
//  
                
    }
})();