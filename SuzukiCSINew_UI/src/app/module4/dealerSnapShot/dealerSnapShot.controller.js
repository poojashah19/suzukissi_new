(function () {
    'use strict';

    angular
            .module('app.module4.dealerSnapShot')
            .controller('m4dealerSnapShotController', m4dealerSnapShotController);

    /* @ngInject */
    function m4dealerSnapShotController($scope,$state, service, $http, $rootScope) {
        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776","#92D400","#00A1DE","#72C7E7","#3C8A2E","#C9DD03","#335291","#A8DD33","#33B4E5"];
        $rootScope.subpage="";
        $rootScope.subpagexs="";
        $rootScope.subpage+=" ";
        $rootScope.hideyear=false;
         $rootScope.showFilterTooltip=true;
        $rootScope.hidebiannual=false;
        $rootScope.hidemonth=false;
        $rootScope.hidezone=true;
        $rootScope.hideregion=true;
        $rootScope.snaphidezone=false;
        $rootScope.snaphideregion=false;
        $rootScope.hidemodel=true;
        $rootScope.hidetrendfactor=true;
        $rootScope.hidefactor=false;
        $rootScope.dealerhide=true;
        $rootScope.dealersnaphide=false;
        $rootScope.pagenumber='9';
        $rootScope.starlabelhide=false;
        $rootScope.priorityhide=true;
        $rootScope.summaryRefresh=1;
        $rootScope.dsnapshot=$rootScope.Filterarray.dealer[0].value;
        $rootScope.factor="SSI";
         $scope.yearset=false;
            $scope.biset=false;
            $scope.monthset=true;
        $scope.factor="SSI";
        $scope.factorname=$rootScope.Filterarray.factor[0].value;
        var parameters=[];
        $scope.langcode=$rootScope.language;
        $rootScope.redfeedback="not";
        $rootScope.checkboxInitiliser();
        var languagepara=[];
        $scope.zone= $rootScope.ALL;
        $scope.region =  $rootScope.ALL;
        $scope.model =  $rootScope.ALL;
        $scope.year=$rootScope.Filterarray.year[0].value;
        $scope.biannual =  $rootScope.ALL;
        $scope.month= $rootScope.Filterarray.month[1].value;
        $scope.dealer=$rootScope.Filterarray.dealer[0].value;
        $scope.dealername=$rootScope.Filterarray.dealer[0].value;
        var modelpara=[];
        var languagepara=[];
            $scope.show4=true;
            $scope.show3=false;
         
         
         if($scope.langcode == 'EN'){
                $scope.dsnap1=$rootScope.Titlearray[92].ContentEN;
                $scope.dsnap2=$rootScope.Titlearray[96].ContentEN;
                $scope.dsnap3=$rootScope.Titlearray[139].ContentEN;//deligh
                $scope.dsnap4=$rootScope.Titlearray[140].ContentEN;//trent
                $scope.dsnap5=$rootScope.Titlearray[112].ContentEN;
                $scope.dsnapf6=$rootScope.Titlearray[113].ContentEN;
                $scope.dsnapf7=$rootScope.Titlearray[114].ContentEN;
                $scope.dsnapf8=$rootScope.Titlearray[115].ContentEN;
                $scope.footnote=$rootScope.Footarray[1].ContentEN;
                $scope.factorscore="Factor Score";
                $scope.sopscore='SSI Score';
                $scope.attscore='Attribute Score';
                $scope.factorname="SSI";
                $scope.firstchart="Note: Data updated up to the lastest available month";

             }
            else {
                
                $scope.dsnap1=$rootScope.Titlearray[92].ContentReganal;
                $scope.dsnap2=$rootScope.Titlearray[96].ContentReganal;
                $scope.dsnap3=$rootScope.Titlearray[139].ContentReganal;//deligh
                $scope.dsnap4=$rootScope.Titlearray[140].ContentReganal;//trent
                $scope.dsnap5=$rootScope.Titlearray[112].ContentReganal;
                $scope.dsnapf6=$rootScope.Titlearray[113].ContentReganal;
                $scope.dsnapf7=$rootScope.Titlearray[114].ContentReganal;
                $scope.dsnapf8=$rootScope.Titlearray[115].ContentReganal;
                $scope.footnote=$rootScope.Footarray[1].ContentReganal;
                $scope.firstchart="หมายเหตุ: แสดงเฉพาะข้อมูลล่าสุดที่มี";
                $scope.factorscore="คะแนนปัจจัย";
                $scope.sopscore='SOP คะแนน';
                $scope.attscore='คุณลักษณะ คะแนน';
                
                 modelpara=[{"name":"english","value":$scope.dealer}];
                service.mastersrv("Systemuserview-getName",modelpara).then(function (response) {
                      $scope.dealername=response.data[0].ContentReganal;
                      });
                 service.mastersrv("Systemuserview-getName",languagepara).then(function (response) {
                        $scope.factorname=response.data[0].ContentReganal;
                    });
                   

            }
         
         
                
        //////////////////////////////////////////////////////////////
            var w = window.innerWidth|| document.documentElement.clientWidth|| document.body.clientWidth;
            var h = window.innerHeight|| document.documentElement.clientHeight|| document.body.clientHeight;

            console.log( " height :" , h);
            console.log( " width :" , w);
            var margin=300;
            var lineheight=25;
            $scope.xstrue=false;
            var weight=50;
            if(w < 1280 && w > 960){margin=250;}
            else if(w < 960 && w > 600){ margin=200;lineheight=12;weight=40;}
            else if(w < 600){margin=150;lineheight=12;weight=30;$scope.xstrue=true;}
            console.log('margin',margin);
        
            $scope.$on('changeLanguage', function (event, data) {
                $scope.langcode=data;

                modelpara=[{"name":"english","value":$scope.dealer}];
                languagepara=[{"name":"english","value":$scope.factor}];
                    service.mastersrv("Systemuserview-getName",languagepara).then(function (response) {
                            console.log('response response[0]',response.data[0].ContentReganal);
                             if($scope.langcode == 'EN'){
                                $scope.factorname=response.data[0].ContentEN;
                            }
                            else{
                                $scope.factorname=response.data[0].ContentReganal;
                            }
                        });
                service.mastersrv("Systemuserview-getName",modelpara).then(function (response) {
                    console.log('response response[0]',response.data[0].ContentReganal);
                    if($scope.langcode == 'EN'){
                        $scope.dealername=response.data[0].ContentEN;
                    }
                    else{
                        $scope.dealername=response.data[0].ContentReganal;
                    }
                      });
                    $scope.filterchange();
              });   
            
          
          
        $scope.$on('changeyear', function (event, data) {
            $scope.year=data;
            $scope.show4=false;
            $scope.show3=true;
            $scope.yearset=true;
            $scope.biset=false;
            $scope.monthset=false;
            $scope.biannual= $rootScope.ALL;
            $scope.month= $rootScope.ALL;
            $scope.filterchange();
            $scope.yearscore();
          });
          
           $scope.$on('changebiannual', function (event, data) {
           $scope.biannual=data;
            if( $scope.biannual== $rootScope.ALL){
                 $scope.show4=false;
                $scope.show3=true;
            $scope.yearset=true;
            $scope.biset=false;
            $scope.monthset=false;
                $scope.yearscore();
            }
            else{
                $scope.show4=false;
            $scope.show3=true;
            $scope.yearset=false;
            $scope.biset=true;
            $scope.monthset=false;
            $scope.month= $rootScope.ALL;
             $scope.filterchange();
            }
            
            $scope.month= $rootScope.ALL;
        });
        
         $scope.$on('changemonth', function (event, data) {
                    $scope.month=data;
                    if($scope.month== $rootScope.ALL){
                        $scope.show4=false;
                        $scope.show3=true;
                    $scope.yearset=true;
                    $scope.biset=false;
                    $scope.monthset=false;
                        $scope.yearscore();
                    }
                    else{
                        $scope.show4=true;
                        $scope.show3=false;
                    $scope.yearset=false;
                    $scope.biset=false;
                    $scope.monthset=true;
                        $scope.monthscore();
                    }

                    $scope.filterchange();
        });
          
        $scope.$on('changefactor', function (event, data) {
            $scope.factor=data;
            languagepara=[{"name":"english","value":$scope.factor}];
            service.mastersrv("Systemuserview-getName",languagepara).then(function (response) {
                    console.log('response response[0]',response.data[0].ContentReganal);
                     if($scope.langcode == 'EN'){
                        $scope.factorname=response.data[0].ContentEN;
                    }
                    else{
                        $scope.factorname=response.data[0].ContentReganal;
                    }
                
               });
            $scope.filterchange();
          });
          
      $scope.$on('changefactordsnap', function (event, data) {
        $scope.factor=data;
        languagepara=[{"name":"english","value":$scope.factor}];
            service.mastersrv("Systemuserview-getName",languagepara).then(function (response) {
                    console.log('response response[0]',response.data[0].ContentReganal);
                     if($scope.langcode == 'EN'){
                        $scope.factorname=response.data[0].ContentEN;
                    }
                    else{
                        $scope.factorname=response.data[0].ContentReganal;
                    }
                
               });
        $scope.filterchange();
      });
          
        $scope.$on('factorClick', function (event, data) {
                $scope.factor=data;
                console.log('state2 params:', $rootScope.factorClick);
                $scope.filterchange();
          });
        
       
        $scope.$on('snapzonechanged', function (event, data) {
                $scope.zone=data;
                 if($rootScope.language == 'EN'){
                     var study={"name": $rootScope.ALL,"value": $rootScope.ALL};
                    var all={"name": $rootScope.ALL,"value": $rootScope.ALL};
                }
                else if($rootScope.language == 'TH'){
                    study={"name": $rootScope.ALL,"value":"ทั่วประเทศ"};
                    all={"name": $rootScope.ALL,"value":"全部"};
                }
                var zoneobj=[{"name": "langCode", "value": $scope.langcode},{"name":"zone","value":encodeURIComponent($scope.zone)}];
                    service.mastersrv("SSIFilters-getRegion",zoneobj).then(function (result) {
                            $rootScope.Filterarray.region=result.data;
                            $rootScope.Filterarray.region.splice(0, 0, study);
                            $scope.region= $rootScope.ALL;
                        });

                    service.mastersrv("SSIFilters-getDealers",zoneobj).then(function (result) {
                                $rootScope.Filterarray.dealerp=result.data;
                                $scope.dealer=$rootScope.Filterarray.dealerp[0].name;
                                 $scope.dealername=$rootScope.Filterarray.dealerp[0].value;
                                console.log("$rootScope.Filterarray.dealerp[0].value",$rootScope.Filterarray.dealerp[0].value);
                                console.log("$rootScope.Filterarray.dealerp[0].value",$rootScope.Filterarray.dealerp[0].value)
                                console.log("$rootScope.Filterarray.dealerp[0].value",$rootScope.Filterarray.dealerp[0].value)
                                
                         $scope.filterchange();
                        });
          });
          
        $scope.$on('snapregionchanged', function (event, data) {
                $scope.region=data;
                if($rootScope.language == 'EN'){
                    var study={"name": $rootScope.ALL,"value": $rootScope.ALL};
                    var all={"name": $rootScope.ALL,"value": $rootScope.ALL};
                }
                else if($rootScope.language == 'TH'){
                    study={"name": $rootScope.ALL,"value":"ทั่วประเทศ"};
                    all={"name": $rootScope.ALL,"value":"全部"};
                }
             var regionobj=[{"name": "langCode", "value": $scope.langcode},{"name":"region","value":encodeURIComponent($scope.region)}];
                     service.mastersrv("SSIFilters-getDealers",regionobj).then(function (result) {
                            $rootScope.Filterarray.dealerp=result.data;
                            $scope.dealer=$rootScope.Filterarray.dealerp[0].name;
                            $scope.dealername=$rootScope.Filterarray.dealerp[0].value;
                             console.log("$rootScope.Filterarray region.dealerp[0].value",$rootScope.Filterarray.dealerp[0].value);
                                console.log("$rootScope.Filterarray reg.dealerp[0].value",$rootScope.Filterarray.dealerp[0].value)
                                console.log("$rootScope.Filterarray reg.dealerp[0].value",$rootScope.Filterarray.dealerp[0].value)
                                
                $scope.filterchange();
                    });
          });
         
          
       $scope.$on('dealersnapchanged', function (event, data) {
            $scope.dealer=data;
            languagepara=[{"name":"english","value":$scope.dealer}];
            service.mastersrv("Systemuserview-getName",languagepara).then(function (response) {
                    console.log('response response[0]',response.data[0].ContentReganal);
                    if($scope.langcode == 'EN'){
                        $scope.dealername=response.data[0].ContentEN;
                        console.log('response $scope.dealername[0]',$scope.dealername);
                    }
                    else{
                        $scope.dealername=response.data[0].ContentReganal;
                        console.log('response $scope.dealername[0]',$scope.dealername);
                    }
               });
            $scope.filterchange();
         });
               //////////////////////////////////////////////////////////////
 
           
            $scope.factor="SSI";
            parameters=[{"name":"factor","value":$scope.factor},{"name":"zone","value":encodeURIComponent($scope.zone)}, {"name": "region", "value":encodeURIComponent($scope.region)},{"name":"dealer","value":$scope.dealer},{"name":"biannual","value":$scope.biannual},{"name":"month","value":$scope.month},{"name":"year","value":$scope.year},{"name":"langCode", "value": $scope.langcode}];
            var regionobj=[{"name": "state", "value":  $rootScope.ALL},{"name":"langCode", "value": $scope.langcode},{"name":"region","value": $rootScope.ALL}];
                     service.mastersrv("SSIFilters-getDealers",regionobj).then(function (result) {
                         $rootScope.Filterarray.dealer=result.data;
                    });
              
        
        
        
            $scope.yearscore=function(){
                 //SSIDealerSnapshot-getDealerSSIScores
                service.mastersrv("SSIDealerSnapshot-getDealerSSIScoresYear",parameters).then(function (response) {    
                        console.log('response.data',response.data);
                        console.log('response.data.data',response.data.data);
                               console.log('response.data',response.data.samplecount);
                               if(response.data.samplecount < 10){
                                    $scope.samplespaceless=true;
                                    console.log(  $scope.samplespaceless);
                                }
                               else{
                                    $scope.samplespaceless=false;
                                    console.log(  $scope.samplespaceless);
                                }


                           $scope.currmonth=response.data.data[0].Wave;
                           $scope.svg1=response.data.data[0].Score;
                           $scope.prevmonth=response.data.data[1].Wave;
                           $scope.svg2=response.data.data[1].Score;
                            if(!response.data.data[1].Wave){
                                   $scope.prevmonth = 0;
                                   $scope.svg2 = 0;

                              }
                           $scope.prevquater=response.data.data[2].Wave;
                           $scope.svg3=response.data.data[2].Score;
                    });
            };
            
            $scope.biannualscore=function(){
                //SSIDealerSnapshot-getDealerSSIScores
                service.mastersrv("SSIDealerSnapshot-getDealerSSIScoresBiannual",parameters).then(function (response) {    
                        console.log('response.data',response.data);
                        console.log('response.data.data',response.data.data);
                               console.log('response.data',response.data.samplecount);
                               if(response.data.samplecount < 10){
                                    $scope.samplespaceless=true;
                                    console.log(  $scope.samplespaceless);
                                }
                               else{
                                    $scope.samplespaceless=false;
                                    console.log(  $scope.samplespaceless);
                                }


                           $scope.currmonth=response.data.data[0].Wave;
                           $scope.svg1=response.data.data[0].Score;
                           $scope.prevmonth=response.data.data[1].Wave;
                           $scope.svg2=response.data.data[1].Score;
                            if(!response.data.data[1].Wave){
                                   $scope.prevmonth = 0;
                                   $scope.svg2 = 0;

                              }
                           $scope.prevquater=response.data.data[2].Wave;
                           $scope.svg3=response.data.data[2].Score;
                    });
            };
            
            $scope.monthscore=function(){
               //SSIDealerSnapshot-getDealerSSIScores
                service.mastersrv("SSIDealerSnapshot-getDealerSSIScoresMonth",parameters).then(function (response) {    
                        console.log('response.data',response.data);
                        console.log('response.data.data',response.data.data);
                               console.log('response.data',response.data.samplecount);
                               if(response.data.samplecount < 10){
                                    $scope.samplespaceless=true;
                                    console.log(  $scope.samplespaceless);
                                }
                               else{
                                    $scope.samplespaceless=false;
                                    console.log(  $scope.samplespaceless);
                                }


                           $scope.currmonth=response.data.data[0].Wave;
                           $scope.svg1=response.data.data[0].Score;
                           $scope.prevmonth=response.data.data[1].Wave;
                           $scope.svg2=response.data.data[1].Score;
                            if(!response.data.data[1].Wave){
                                   $scope.prevmonth = 0;
                                   $scope.svg2 = 0;

                              }
                        $scope.currquater = response.data.data[2].Wave;
                        $scope.svg3 = response.data.data[2].Score;
                        $scope.prevquater = response.data.data[3].Wave;
                        $scope.svg4 = response.data.data[3].Score;
                    });
            };
        
        
        
        
        
        
                 
             
            $scope.graphs=function(){
                
                  if($scope.langcode == 'EN'){
                        $scope.dsnap1=$rootScope.Titlearray[92].ContentEN;
                        $scope.dsnap2=$rootScope.Titlearray[96].ContentEN;
                        $scope.dsnap3=$rootScope.Titlearray[139].ContentEN;//deligh
                        $scope.dsnap4=$rootScope.Titlearray[140].ContentEN;//trent
                        $scope.dsnap5=$rootScope.Titlearray[112].ContentEN;
                        $scope.dsnapf6=$rootScope.Titlearray[113].ContentEN;
                        $scope.dsnapf7=$rootScope.Titlearray[114].ContentEN;
                        $scope.dsnapf8=$rootScope.Titlearray[115].ContentEN;
                           $scope.footnote=$rootScope.Footarray[5].ContentEN;
            $scope.footnote+='<br>'+$rootScope.Footarray[0].ContentEN;
            $scope.footnote+='<br>'+$rootScope.Footarray[6].ContentEN;
                        $scope.factorscore="Factor Score";
                        $scope.sopscore='SOP Score';
                        $scope.attscore='Attribute Score';
                $scope.firstchart="Note: Data updated up to the lastest available month";
                        
                     }
                    else {
                        $scope.dsnap1=$rootScope.Titlearray[92].ContentReganal;
                        $scope.dsnap2=$rootScope.Titlearray[96].ContentReganal;
                        $scope.dsnap3=$rootScope.Titlearray[139].ContentReganal;//deligh
                        $scope.dsnap4=$rootScope.Titlearray[140].ContentReganal;//trent
                        $scope.dsnap5=$rootScope.Titlearray[112].ContentReganal;
                        $scope.dsnapf6=$rootScope.Titlearray[113].ContentReganal;
                        $scope.dsnapf7=$rootScope.Titlearray[114].ContentReganal;
                        $scope.dsnapf8=$rootScope.Titlearray[115].ContentReganal;
                         $scope.footnote=$rootScope.Footarray[5].ContentReganal;
                     $scope.footnote+='<br>'+$rootScope.Footarray[0].ContentReganal;
                      $scope.footnote+='<br>'+$rootScope.Footarray[6].ContentReganal;
                        $scope.factorscore="คะแนนปัจจัย";
                        $scope.sopscore='SOP คะแนน';
                        $scope.attscore='คุณลักษณะ คะแนน';
                $scope.firstchart="หมายเหตุ: แสดงเฉพาะข้อมูลล่าสุดที่มี";
                        
                    }
                
                
                
                console.log(parameters);
                
                
//                SSIDealerSnapshot-getSSIDealerFactorScores
                service.mastersrv("SSIDealerSnapshot-getSSIDealerFactorScores",parameters).then(function (response) {
                        $('#top1').highcharts(service.clicksummaryplainbar($scope.firstchart,'column',['#2979FF'],response.data, $scope.factorscore,0,35,.2,[0],3,false));
                        $('#top11').highcharts(service.clicksummaryplainbar($scope.firstchart,'column',['#2979FF'],response.data, $scope.factorscore,0,35,.2,[0],3,false));
//                        $scope.legendname=response.data[0].scatter;
//                        $scope.legendnamew=response.data[0].scatter1;
                        $rootScope.legendforselectionb= response.data[0].scatter;
                        $rootScope.legendforselectionw=response.data[0].scatter1;
                    });
              //  SSIDealerSnapshot-getSSIDealerSOPTopBottom
                service.mastersrv("SSIDealerSnapshot-getSSIDealerSOPTopBottom",parameters).then(function (response) {
                        $('#top2').highcharts(service.multbarchartsplit(105,'bar',response.data,170,10,10,$scope.sopscore,9,'6.2px',"%"));
                        $('#top21').highcharts(service.multbarchartsplit(105,'bar',response.data,170,10,10,$scope.sopscore,9,'6.2px',"%"));
                    });                               

  //line graph
                service.mastersrv("SSIDealerSnapshot-getSSIAttributeTrendByWave",parameters).then(function (response) {
                        $('#dsnapbot1').highcharts(service.horizontalline(response.data,color));
                        $('#dsnapbot11').highcharts(service.horizontalline(response.data,color));
                    });

//delighted
                service.mastersrv("SSIDealerSnapshot-getSSIDelighted",parameters).then(function (response) {
                        $('#bot2').highcharts(service.barplainbarsplit('bar',['#2979FF'],response.data,$scope.attscore,150,35,0,[0],5,false,"%"));
                        $('#bot21').highcharts(service.barplainbarsplit('bar',['#2979FF'],response.data,$scope.attscore,150,35,0,[0],5,false,"%"));
                   });

            
        };
       
       
       
        $scope.getStyle = function(){
                var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

                return {
                    'top': $scope.isSemi ? 'auto' : '50%',
                    'bottom': $scope.isSemi ? '5%' : 'auto',
                    'left': '50%',
                    'transform': transform,
                    '-moz-transform': transform,
                    '-webkit-transform': transform,
                    'font-size': $scope.radius/3.5 + 'px'
                };
            };
            
        $scope.getStyle1 = function(){
            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '50%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': $scope.radius/3.5 + 'px'
            };
        };
        
        
         $scope.getStyle2 = function(){
                var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

                return {
                    'top': $scope.isSemi ? 'auto' : '50%',
                    'bottom': $scope.isSemi ? '5%' : 'auto',
                    'left': '50%',
                    'transform': transform,
                    '-moz-transform': transform,
                    '-webkit-transform': transform,
                    'font-size': $scope.radius/4.5 + 'px'
                };
            };
            
         $scope.filterchange=function(){
                parameters=[{"name":"year","value":$scope.year},{"name":"zone","value":encodeURIComponent($scope.zone)}, {"name": "region", "value":encodeURIComponent($scope.region)},{"name":"biannual","value":$scope.biannual},{"name":"month","value":$scope.month},{"name":"langCode", "value": $scope.langcode},{"name":"dealer","value":$scope.dealer},{"name":"factor","value":$scope.factor}];
                console.log("dealer" + $scope.dealer);   
                
                 if($scope.yearset==true && $scope.biset==false &&  $scope.monthset==false ){
                
                        $scope.yearscore();
                        }
                        if($scope.yearset==false && $scope.biset==true &&  $scope.monthset==false ){

                        $scope.biannualscore();
                        }
                        if($scope.yearset==false && $scope.biset==false &&  $scope.monthset==true ){

                        $scope.monthscore();
                        }
                $scope.graphs();
             };
        
    }
})();