(function() {
    'use strict';

    angular
        .module('app.module4.dealerSnapShot')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

          $stateProvider
          .state('triangular.ssidealerSnapShot', {
            url: '/ssidealerSnapShot',
             views: {
                '': {
                   templateUrl: 'app/module4/dealerSnapShot/dealerSnapShot.tmpl.html',
            controller: 'm4dealerSnapShotController',
            controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module4/dealerSnapShot/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        });
        
//         triMenuProvider.addMenu({
//                   name: 'Dealer Snapshot',
//                    icon: 'zmdi zmdi-picture-in-picture',
//                    type: 'link',
//                    priority: 1.8,
//                     state: 'triangular.csidealerSnapShot',
//                });
//                
//             triMenuProvider.removeMenu('triangular.dealerSnapShot');   
                
    }
})();