    (function () {
    'use strict';

    angular
            .module('app.module4.summary')
            .controller('m4page4Controller', m4page4Controller);

    /* @ngInject */
    function m4page4Controller($scope, service, $http, $rootScope) {
        var vm = this;
        
        
        //language
        
       
       
      
    
        //
        $rootScope.lockLeft = true;
        var color = ["#002776","#92D400","#00A1DE","#72C7E7","#3C8A2E","#C9DD03","#335291","#A8DD33","#33B4E5"];
        $rootScope.subpage="";
        $rootScope.subpagexs="";
        $rootScope.subpage+=" ";
         $rootScope.showFilterTooltip=true;
        $rootScope.hideyear=false;
        $rootScope.hidebiannual=false;
        $rootScope.hidemonth=false;
        $rootScope.hidezone=false;
        $rootScope.hideregion=false;
        $rootScope.hidemodel=true;
        $rootScope.hidefactor=true;
        $rootScope.hidetrendfactor=true;
        $rootScope.snaphidezone=true;
        $rootScope.snaphideregion=true;
        $scope.show4=true;
        $scope.show3=false;
        $rootScope.redfeedback="not";
        $scope.month= $rootScope.Filterarray.month[1].value;
            $scope.yearset=true;
            $scope.biset=false;
            $scope.monthset=false;
        $rootScope.dealersnaphide=true;
        $rootScope.dealerhide=true;
        $rootScope.pagenumber='1';
        $rootScope.starlabelhide=false;
        $rootScope.legendname="Study Best";
        $rootScope.legendnamew="Worst Score";
        $rootScope.priorityhide=true;
        
        
        $scope.langcode=$rootScope.language;
        $rootScope.modelsnap="not";
        $scope.zone=$rootScope.ALL;
        $scope.region = $rootScope.ALL;
        $scope.model = $rootScope.ALL;
        $scope.year=$rootScope.Filterarray.year[0].value;
        $scope.biannual = $rootScope.ALL;
//      
        $scope.legendname = "Study Best";
        $scope.legendnamew="Worst Score";
//        $rootScope.dtwave=$scope.waves;
        $rootScope.dealerScoreDealer =$rootScope.Filterarray.dealer[0].value;
        
        
        //initial call:
        
//        $scope.yearscore();
        
        //
        
        
        if($rootScope.summaryoffset){
            $rootScope.summaryoffset=false;
        }
        else{
//        $rootScope.checkboxInitiliser();
    
        }
        
        
        if($scope.langcode == 'EN'){
            $scope.nationaltitle=$rootScope.Titlearray[1].ContentEN;
            $scope.regionaltitle=$rootScope.Titlearray[2].ContentEN;
            $scope.factortitle=$rootScope.Titlearray[3].ContentEN;
            $scope.top5title=$rootScope.Titlearray[9].ContentEN;
            $scope.delightedtitle=$rootScope.Titlearray[10].ContentEN;
            $scope.footnote+=$rootScope.Footarray[1].ContentEN;
            $scope.footnote+=$rootScope.Footarray[0].ContentEN;
            $scope.footnote+=$rootScope.Footarray[6].ContentEN;
                
            $scope.factorscore="Factor Score";
            $scope.firstchart="Note: Data updated up to the lastest available month";
            
         }
        else {
            $scope.nationaltitle=$rootScope.Titlearray[1].ContentReganal;
            $scope.regionaltitle=$rootScope.Titlearray[2].ContentReganal;
            $scope.factortitle=$rootScope.Titlearray[3].ContentReganal;
            $scope.top5title=$rootScope.Titlearray[9].ContentReganal;
            $scope.delightedtitle=$rootScope.Titlearray[10].ContentReganal;
            $scope.footnote=$rootScope.Footarray[1].ContentReganal;
            $scope.footnote+=$rootScope.Footarray[0].ContentReganal;
            $scope.footnote+=$rootScope.Footarray[6].ContentReganal;
            
            $scope.factorscore="คะแนนปัจจัย";
            $scope.firstchart="หมายเหตุ: แสดงเฉพาะข้อมูลล่าสุดที่มี";
        }
        
        
       
        //////////////////////////////////////////////////////////////
            var w = window.innerWidth|| document.documentElement.clientWidth|| document.body.clientWidth;
            var h = window.innerHeight|| document.documentElement.clientHeight|| document.body.clientHeight;

            console.log( " height :" , h);
            console.log( " width :" , w);
            var margin=300;
            var lineheight=25;
            $scope.xstrue=false;
            var weight=50;
            if(w < 1280 && w > 960){margin=250;}
            else if(w < 960 && w > 600){ margin=200;lineheight=12;weight=40;}
            else if(w < 600){margin=150;lineheight=12;weight=30;$scope.xstrue=true;}
            console.log('margin',margin);
        //////////////////////////////////////////////////////////////
        
        $scope.$on('changeLanguage', function (event, data) {
            $scope.$watch();
            $scope.langcode=data;
            $scope.filterchange();
            
            
            $rootScope.languagechangernav();
          });
        
        
        $scope.$on('changeyear', function (event, data) {
            $scope.year=data;
            $scope.yearset=true;
            $scope.biset=false;
            $scope.monthset=false;
            $scope.show4=false;
            $scope.show3=true;
            $scope.biannual=$rootScope.ALL;
            $scope.month=$rootScope.ALL;
            $scope.filterchange();
          });
        
        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual=data;
            if( $scope.biannual=="All"){
                 $scope.show4=false;
                $scope.show3=true;
            $scope.yearset=true;
            $scope.biset=false;
            $scope.monthset=false;
                $scope.yearscore();
            }
            else{
                $scope.show4=false;
            $scope.show3=true;
            $scope.yearset=false;
            $scope.biset=true;
            $scope.monthset=false;
            $scope.month=$rootScope.ALL;
             $scope.filterchange();
            }
            
            $scope.month=$rootScope.ALL;
           
          });
          
       $scope.$on('changemonth', function (event, data) {
            $scope.month=data;
            if($scope.month=="All"){
                $scope.show4=false;
                $scope.show3=true;
            $scope.yearset=true;
            $scope.biset=false;
            $scope.monthset=false;
                $scope.yearscore();
            }
            else{
                $scope.show4=true;
                $scope.show3=false;
            $scope.yearset=false;
            $scope.biset=false;
            $scope.monthset=true;
                $scope.monthscore();
            }
            
            $scope.filterchange();
          });
          
        $scope.$on('changezone', function (event, data) {
            $scope.zone=data;
            $scope.region = $rootScope.ALL;
            $scope.dealer=$rootScope.ALL;
            $scope.filterchange();
          });
          
        $scope.$on('changeregion', function (event, data) {
            $scope.region=data;
            $scope.dealer=$rootScope.ALL;
            $scope.filterchange();
          });
          
          $scope.$on('changemodel', function (event, data) {
            $scope.model=data;
            $scope.filterchange();
          });
       //////////////////////////////////////////////////////////////
 
       
        var parameters = [];
      
//        $rootScope.dealerScoreDealer = $rootScope.Filterarray.dealer[0];
        $rootScope.mapState = 0;
        
         parameters = [{"name":"zone","value":encodeURIComponent($scope.zone)},{"name":"langCode", "value": $scope.langcode}, {"name": "region", "value":encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}];

        $scope.getStyle = function () {
             var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '53%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': $scope.radius / 3.5 + 'px'
            };
        };
         $scope.getStyle1 = function () { 
             var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '53%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': $scope.radius / 3.5 + 'px'
            };
        };
        
        
        
            $scope.yearscore=function(){
                 //SSISummary-getNationalSSIScores
                 console.log('In year score ');
                    service.mastersrv("SSISummary-getNationalSSIScoresYear", parameters).then(function (response) {
                     
                        if (response.data.samplecount < 10) {
                            $scope.samplespaceless = true;
        //                    console.log($scope.samplespaceless);
                        } else {
                            $scope.samplespaceless = false;
        //                    console.log($scope.samplespaceless);
                        }
                        
                         $scope.currmonth = '';
                        $scope.svg1 = '';
                        $scope.prevmonth = '';
                        $scope.svg2 = '';
                        $scope.prevquater = '';
                        $scope.svg3 ='';
                        
                        
                        $scope.currmonth = response.data.data[0].Wave;
                        $scope.svg1 = response.data.data[0].Score;
                        $scope.prevmonth = response.data.data[1].Wave;
                        $scope.svg2 = response.data.data[1].Score;
                        $scope.prevquater = response.data.data[2].Wave;
                        $scope.svg3 = response.data.data[2].Score;


                    });
            };
            
            $scope.biannualscore=function(){
                 //SSISummary-getNationalSSIScores
                    service.mastersrv("SSISummary-getNationalSSIScoresBiannual", parameters).then(function (response) {

                        if (response.data.samplecount < 10) {
                            $scope.samplespaceless = true;
        //                    console.log($scope.samplespaceless);
                        } else {
                            $scope.samplespaceless = false;
        //                    console.log($scope.samplespaceless);
                        }
                         $scope.currmonth = '';
                        $scope.svg1 = '';
                        $scope.prevmonth = '';
                        $scope.svg2 = '';
                        $scope.prevquater = '';
                        $scope.svg3 = '';
                        
                        
                        $scope.currmonth = response.data.data[0].Wave;
                        $scope.svg1 = response.data.data[0].Score;
                        $scope.prevmonth = response.data.data[1].Wave;
                        $scope.svg2 = response.data.data[1].Score;
                        $scope.prevquater = response.data.data[2].Wave;
                        $scope.svg3 = response.data.data[2].Score;
                        


                    });
            };
            
            $scope.monthscore=function(){
                 //SSISummary-getNationalSSIScores
                    service.mastersrv("SSISummary-getNationalSSIScoresMonth", parameters).then(function (response) {

                        if (response.data.samplecount < 10) {
                            $scope.samplespaceless = true;
        //                    console.log($scope.samplespaceless);
                        } else {
                            $scope.samplespaceless = false;
        //                    console.log($scope.samplespaceless);
                        }
                        $scope.currmonth = '';
                        $scope.svg1 = '';
                        $scope.prevmonth = '';
                        $scope.svg2 = '';
                        $scope.currquater = '';
                        $scope.svg3 = '';
                        $scope.prevquater = '';
                        $scope.svg4 = '';
                       
                         
                         
                        $scope.currmonth = response.data.data[0].Wave;
                        $scope.svg1 = response.data.data[0].Score;
                        $scope.prevmonth = response.data.data[1].Wave;
                        $scope.svg2 = response.data.data[1].Score;
                        $scope.currquater = response.data.data[2].Wave;
                        $scope.svg3 = response.data.data[2].Score;
                        
                        $scope.prevquater = response.data.data[3].Wave;
                        $scope.svg4 = response.data.data[3].Score;
                        
                       
                        
                        

                    });
            };
        
        
         $scope.graphs = function () {
             
                        $scope.hideloader=false;
            if($scope.langcode == 'EN'){
                    $scope.nationaltitle=$rootScope.Titlearray[1].ContentEN;
                    $scope.regionaltitle=$rootScope.Titlearray[2].ContentEN;
                    $scope.factortitle=$rootScope.Titlearray[3].ContentEN;
                    $scope.top5title=$rootScope.Titlearray[9].ContentEN;
                    $scope.delightedtitle=$rootScope.Titlearray[10].ContentEN;
                     $scope.footnote=$rootScope.Footarray[1].ContentEN;
            $scope.footnote+='<br>'+$rootScope.Footarray[0].ContentEN;
            $scope.footnote+='<br>'+$rootScope.Footarray[6].ContentEN;
            
            console.log('footnotepage4'+$scope.footnote);
                    $scope.factorscore="Factor Score";
                    $scope.firstchart="Note: Data updated up to the lastest available month";

                 }
                else {
                    $scope.nationaltitle=$rootScope.Titlearray[1].ContentReganal;
                    $scope.regionaltitle=$rootScope.Titlearray[2].ContentReganal;
                    $scope.factortitle=$rootScope.Titlearray[3].ContentReganal;
                    $scope.top5title=$rootScope.Titlearray[9].ContentReganal;
                    $scope.delightedtitle=$rootScope.Titlearray[10].ContentReganal;
                    $scope.footnote=$rootScope.Footarray[6].ContentReganal;
                     $scope.footnote+='<br>'+$rootScope.Footarray[0].ContentReganal;
                      $scope.footnote+='<br>'+$rootScope.Footarray[1].ContentReganal;
                    $scope.factorscore="คะแนนปัจจัย";
            $scope.firstchart="หมายเหตุ: แสดงเฉพาะข้อมูลล่าสุดที่มี";
                }
        
           

            //SSISummary-getSSISummaryRegionFactorScores
            service.mastersrv("SSISummary-getSSISummaryRegionFactorScores", parameters).then(function (response) {
                $('#barlinecontainer').highcharts(service.creditsummaryplainbar($scope.firstchart,'column', ["#4292FF"], response.data, $scope.factorscore, 0, 45, .3, [1, 1, 1, 1, 1, 1], 5, false));
                $('#barlinecontainer1').highcharts(service.creditsummaryplainbar($scope.firstchart,'column', ["#4292FF"], response.data, $scope.factorscore, 0, 45, .3, [1, 1, 1, 1, 1, 1], 5, false));
                
                 if($scope.langcode == 'EN'){
                        $rootScope.legendforselectionb= response.data[0].scatter;
                        $rootScope.legendforselectionw=response.data[0].scatter1;}
                else{
                    $rootScope.legendforselectionb= response.data[0].scatter;
                    $rootScope.legendforselectionw=response.data[0].scatter1;}
                 $rootScope.legendforselectionb= response.data[0].scatter;
                $rootScope.legendforselectionw=response.data[0].scatter1;
            });


            //SSISummary-getSSITop5Bottom5
            service.mastersrv("SSISummary-getSSITop5Bottom5", parameters).then(function (response) {
                $('#treecontainer').highcharts(service.multbarchart(1005, 'bar', response.data, 170, 15, 0, 'SOP Score', 10, '11px', " "));
                 $('#treecontainer1').highcharts(service.multbarchart(1005, 'bar', response.data, 170, 15, 0, 'SOP Score', 10, '11px', " "));

                if (response.data.length == 0) {
                    $scope.samplespaceless = true;
                } else {
                    $scope.samplespaceless = false;
                }

            });


            //SSISummary-getSSITop5Bottom5SOP
            service.mastersrv("SSISummary-getSSITop5Bottom5SOP", parameters).then(function (response) {
                $('#bot1').highcharts(service.multbarchartsplit(100, 'bar', response.data, 180, 15, 0, 'SSI Score', 9, '8.5px', "%"));
                $('#bot11').highcharts(service.multbarchartsplit(100, 'bar', response.data, 180, 15, 0, 'SSI Score', 9, '8.5px', "%"));
                if(response.data){
                        $scope.hideloader=true;
                    }
            });


            //SSISummary-RegionWiseSSI
            service.mastersrv("SSISummary-RegionWiseSSI", parameters).
                    then(function (response) {
                        $scope.mapData = response.data;
                        $scope.maprender();

                        
                    });
            };
            
            
                         var myData = [];
                        $scope.printdata = function () {
                            console.log("map data printing...");
                            console.log("map data printed");
                            $scope.mapData.forEach(function (obj2) {
                                myData.push({region: obj2.region, score: obj2.value, repondents: obj2.repondents});
                            });
                            console.log("mapdata",myData);
                            return myData;
                        };
            
            
             $scope.maprender = function () {

                var latlong = {};
                
               
//                latlong["Bangkok & Greater"] = {"latitude": 13.756331, "longitude": 100.501765};//Bangkok & Greater
//                latlong["Central and East"] = {"latitude": 12.558601, "longitude": 103.322009}; //Central and East
//                latlong["Northeast"] = {"latitude": 17.364697, "longitude": 102.815892};//Northeast
//                latlong["North"] = {"latitude": 18.787747, "longitude": 98.993128}; //North
//                latlong["South"] = {"latitude": 8.508647, "longitude": 98.474688};//South
        latlong["Central East"] = {"latitude": 14.058601, "longitude": 101.022009};
            latlong["Central West"] = {"latitude": 14.856331, "longitude": 98.901765};
            latlong["Lower North"] = {"latitude": 17.087747, "longitude": 100.093128};
            latlong["Northeast Low"] = {"latitude": 15.964697, "longitude": 104.015892};
            latlong["Northeast Middle"] = {"latitude": 16.564697, "longitude": 102.815892};
            latlong["Northeast Up"] = {"latitude": 17.964697, "longitude": 103.974688};
            latlong["South"] = {"latitude": 8.508647, "longitude": 98.474688};
            latlong["Upper North"] = {"latitude": 19.777747, "longitude": 98.983128};
                


                var mapcolor = ["#92D400", "#33B4E5",
                    "#4200d4",
                    "#c3d327", "#3C8A2E",
                    "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];

                var map;
                var minBulletSize = 30;
                var maxBulletSize = 60;
                var min = Infinity;
                var max = -Infinity;

                for (var i = 0; i < $scope.mapData.length; i++) {
                    var value = $scope.mapData[i].value;
                    if (value < min) {
                        min = value;
                    }
                    if (value > max) {
                        max = value;
                    }


                }

                if (min == max) {
                    min = 0;
                    max = value;
                }


                AmCharts.ready = function () {
                    map = new AmCharts.AmMap();
                    map.pathToImages = "http://www.amcharts.com/lib/3/images/";
                    map.panEventsEnabled = false;
                    map.dragMap=false;
                    map.areasSettings = {
                        selectable: true,
                        unlistedAreasColor: "#EFEFEF",
                        unlistedAreasOutlineAlpha: .9,
                        unlistedAreasOutlineColor: "#7F7F7F"
                    };
                    map.imagesSettings.balloonText = "<span style='font-size:14px;' align='left'>[[title]]</span>";

                    map.addListener("clickMapObject", function (event) {
                        console.log(event.mapObject.code);
                        console.log(event.mapObject.value);
                        if ($rootScope.mapState == 0) {
                            $scope.region = event.mapObject.code;
                            $scope.filterchange();
                            $rootScope.mapState = 1;
                        } else {
                            $scope.region = $rootScope.ALL;
                            $scope.filterchange();
                            $rootScope.mapState = 0;
                        }


                    });


                    var dataProvider = {
                        mapVar: AmCharts.maps.thailandHigh,
    //                       
                        images: []
                    };

                    map.zoomControl = {
                        panControlEnabled: false,
                        zoomControlEnabled: false,
                        homeButtonEnabled: false
                    };


                    // it's better to use circle square to show difference between values, not a radius
                    var maxSquare = maxBulletSize * maxBulletSize * 2 * Math.PI;
                    var minSquare = minBulletSize * minBulletSize * 2 * Math.PI;


                    // create circle for each country
                    for (var i = 0; i < $scope.mapData.length; i++) {
                        var dataItem = $scope.mapData[i];
                        var value = dataItem.value;
                        var datacolor = mapcolor[i];

                        // calculate size of a bubble   

                        var square = ((value - min) / (max - min) * (maxSquare - minSquare)) + minSquare;
                        if (square < minSquare) {
                            square = minSquare;
                        }
                        var size = Math.sqrt(square / (Math.PI * 2));
                        var id = dataItem.code;


                        dataProvider.images.push({
                            type: "bubble",
                            width: size,
                            height: size,
                            color: datacolor,
                            longitude: latlong[id].longitude,
                            latitude: latlong[id].latitude,
                            label: (value),
                            alpha: .30,
                            code: dataItem.code,
                            labelPosition: "inside",
                            title: dataItem.name,
                            value: value,
                            rollOverScale: "1.3",
//                            outlineColor: "#104E8B",
                            outlineAlpha: ".5",
                            outlineThickness: "1",
                            selectable: true,
                            bringForwardOnHover: false,
                            selectedOutlineThickness: "5"

                        });
                    }

                    map.dataProvider = dataProvider;
                    map.write("map");
                };
                
                
                 AmCharts.ready1 = function () {
                    map = new AmCharts.AmMap();
                    map.pathToImages = "http://www.amcharts.com/lib/3/images/";
                    map.panEventsEnabled = false;
                    map.dragMap=false;
                    map.areasSettings = {
                        selectable: true,
                        unlistedAreasColor: "#EFEFEF",
                        unlistedAreasOutlineAlpha: .9,
                        unlistedAreasOutlineColor: "#7F7F7F"
                    };
                    map.imagesSettings.balloonText = "<span style='font-size:14px;' align='left'>[[title]]</span>";

                    map.addListener("clickMapObject", function (event) {
                        console.log(event.mapObject.code);
                        console.log(event.mapObject.value);
                        if ($rootScope.mapState == 0) {
                            $scope.region = event.mapObject.code;
                            $scope.filterchange();
                            $rootScope.mapState = 1;
                        } else {
                            $scope.region = $rootScope.ALL;
                            $scope.filterchange();
                            $rootScope.mapState = 0;
                        }


                    });
                    var dataProvider = {
                        mapVar: AmCharts.maps.thailandHigh,
                        images: []
                    };
                    map.zoomControl = {
                        panControlEnabled: false,
                        zoomControlEnabled: false,
                        homeButtonEnabled: false
                    };
                    var maxSquare = maxBulletSize * maxBulletSize * 2 * Math.PI;
                    var minSquare = minBulletSize * minBulletSize * 2 * Math.PI;
                    for (var i = 0; i < $scope.mapData.length; i++) {
                        var dataItem = $scope.mapData[i];
                        var value = dataItem.value;
                        var datacolor = mapcolor[i];
                        var square = ((value - min) / (max - min) * (maxSquare - minSquare)) + minSquare;
                        if (square < minSquare) {
                            square = minSquare;
                        }
                        var size = Math.sqrt(square / (Math.PI * 2));
                        var id = dataItem.code;
                        dataProvider.images.push({
                            type: "bubble",
                            width: size,
                            height: size,
                            color: datacolor,
                            longitude: latlong[id].longitude,
                            latitude: latlong[id].latitude,
                            label: (value),
                            alpha: .30,
                            code: dataItem.code,
                            labelPosition: "inside",
                            title: dataItem.name,
                            value: value,
                            rollOverScale: "1.3",
//                            outlineColor: "#104E8B",
                            outlineAlpha: ".5",
                            outlineThickness: "1",
                            selectable: true,
                            bringForwardOnHover: false,
                            selectedOutlineThickness: "5"
                        });
                    }

                    map.dataProvider = dataProvider;
                    map.write("map1");
                };
                AmCharts.ready();
                 AmCharts.ready1();
        };
       
        $scope.filterchange = function () {
            
            parameters = [{"name":"zone","value":encodeURIComponent($scope.zone)},{"name":"langCode", "value": $scope.langcode}, {"name": "region", "value":encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}];
             if($scope.yearset==true && $scope.biset==false &&  $scope.monthset==false ){
               console.log('debug one') ;
            $scope.yearscore();
            }
            if($scope.yearset==false && $scope.biset==true &&  $scope.monthset==false ){
                 console.log('debug two') ;
            $scope.biannualscore();
            }
            if($scope.yearset==false && $scope.biset==false &&  $scope.monthset==true ){
                 console.log('debugthree') ;
            $scope.monthscore();
            }
            $scope.graphs();
        };
        
    }
})();

