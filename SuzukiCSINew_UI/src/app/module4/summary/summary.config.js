(function() {
    'use strict';

    angular
        .module('app.module4.summary')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

           $stateProvider
         .state('triangular.ssisummary', {
            url: '/ssisummary',
            views: {
                '': {
                   templateUrl: 'app/module4/summary/summary.tmpl.html',
                    controller: 'm4page4Controller',
                    controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module4/summary/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
            
        });

//       triMenuProvider.removeMenu('triangular.dealerSSIpage34'); 
//         triMenuProvider.addMenu({
//                    name: 'Summary',
//                    icon: 'zmdi zmdi-file-text',
//                    type: 'link',
//                    priority: 1.0,
//                    state: 'triangular.SSIpage4',
//                });  
  
                
    }
})();