(function() {
    'use strict';

    angular
        .module('app.module4', [
                'app.module4.priorityAnalysis',
                'app.module4.dealerRanking',
                'app.module4.summary',
                'app.module4.otherDiagnostics',
                'app.module4.loyaltyAdvocacy',
                'app.module4.productAnalysis',
                'app.module4.trendanalysis',
                'app.module4.sopTrend',
                'app.module4.regional',
                'app.module4.sopPerformance',
                'app.module4.dealerSnapShot',
//                'app.module4.page12',
                'app.module4.page13',
                'app.module4.page14',
                'app.module4.redAlert'
//                'app.module4.page31',
//                'app.module4.page32',
//                'app.module4.page34',
//                'app.module4.page35',
//                'app.module4.page37',
//                'app.module4.page39',
//                'app.module4.page310'
          ]);
})();