(function() {
    'use strict';

    angular
        .module('app.module4.page13')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

           $stateProvider
         .state('triangular.ssipage13', {
            url: '/ssipage13',
            views: {
                '': {
                   templateUrl: 'app/module4/page13/page13.tmpl.html',
                    controller: 'm4page13Controller',
                    controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module4/page13/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
            
        });
        
//         triMenuProvider.addMenu({
//                      name: 'Individual Downloadable Dealer PDF',
//                    icon: 'zmdi zmdi-view-list-alt',
//                    type: 'link',
//                    priority: 2.4,
//                     state: 'triangular.csipage13',
//                });
//             triMenuProvider.removeMenu('triangular.page13');   
                
                
    }
})();