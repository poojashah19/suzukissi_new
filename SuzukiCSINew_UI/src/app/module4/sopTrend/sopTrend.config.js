(function() {
    'use strict';

    angular
        .module('app.module4.sopTrend')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

          $stateProvider
          .state('triangular.ssisopTrend', {
            url: '/ssisopTrend',
            
             views: {
                '': {
                   templateUrl: 'app/module4/sopTrend/sopTrend.tmpl.html',
            controller: 'm4sopTrendController',
            controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module4/sopTrend/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        });
        
//           triMenuProvider.removeMenu('triangular.dealercsipage39'); 
//         triMenuProvider.addMenu({
//                   name: 'SOP Trend',
//                    icon: 'zmdi zmdi-hourglass',
//                    type: 'link',
//                    priority: 1.5,
//                     state: 'triangular.csisopTrend',
//                });  
                         
 
                
    }
})();