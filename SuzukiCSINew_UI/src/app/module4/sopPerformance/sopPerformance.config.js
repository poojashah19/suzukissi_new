(function() {
    'use strict';

    angular
        .module('app.module4.sopPerformance')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

          $stateProvider
          .state('triangular.ssisopPerformance', {
            url: '/ssisopPerformance',
            views: {
                '': {
                   templateUrl: 'app/module4/sopPerformance/sopPerformance.tmpl.html',
            controller: 'm4sopPerformanceController',
            controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module4/sopPerformance/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        });
        

//         triMenuProvider.removeMenu('triangular.dealercsipage310'); 
//         triMenuProvider.addMenu({
//                   name: 'SOP Performance',
//                    icon: 'zmdi zmdi-collection-text',
//                    type: 'link',
//                    priority: 1.4,
//                     state: 'triangular.csisopPerformance',
//                });
    }
})();