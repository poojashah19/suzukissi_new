(function () {
    'use strict';

    angular
            .module('app.module4.page12')
            .controller('m4page12Controller', m4page12Controller);

    /* @ngInject */
    function m4page12Controller($scope, service, $state,$http, $rootScope) {
        
        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776","#92D400","#00A1DE","#72C7E7","#3C8A2E","#C9DD03","#335291","#A8DD33","#33B4E5"];
        $rootScope.subpage="";
        $rootScope.subpagexs="";
        $rootScope.subpage+=" ";
         $rootScope.showFilterTooltip=true;
        $rootScope.hideyear=true;
        $rootScope.hidewave=true;
        $rootScope.hideregion=true;
        $rootScope.hidearea=true;
        $rootScope.hidezone=true;
        $rootScope.hideprovince=true;
        $rootScope.snaphidezone=true;
        $rootScope.snaphideregion=true;
        $rootScope.hidecity=true;
        $rootScope.hidemodel=true;
        $rootScope.hidefactor=true;
        $rootScope.dealerhide=true;
        $rootScope.pagenumber='8';
        $rootScope.starlabelhide=true;
        $rootScope.priorityhide=true;
        $rootScope.summaryRefresh=1;
         $rootScope.dsnapshot="not";
         $rootScope.dealerScoreDealer="null";
       $rootScope.checkboxInitiliser();
        $scope.region="All";
         $scope.area="All";
        $scope.zone="All";
        $scope.province="All";
         
                $scope.year='2016';
                
        
        //////////////////////////////////////////////////////////////
            var w = window.innerWidth|| document.documentElement.clientWidth|| document.body.clientWidth;
            var h = window.innerHeight|| document.documentElement.clientHeight|| document.body.clientHeight;

            console.log( " height :" , h);
            console.log( " width :" , w);
            var margin=300;
            var lineheight=25;
            $scope.xstrue=false;
            var weight=50;
            if(w < 1280 && w > 960){margin=250;}
            else if(w < 960 && w > 600){ margin=200;lineheight=12;weight=40;}
            else if(w < 600){margin=150;lineheight=12;weight=30;$scope.xstrue=true;}
            console.log('margin',margin);
        //////////////////////////////////////////////////////////////
    
         $scope.outputer=function(value){
             console.log("filter change of dscore called",value);
                        
                 };
    }
})();