(function () {
    'use strict';

    angular
            .module('app.module4.dealerRanking')
            .controller('m4dealerRankingController', m4dealerRankingController);

    /* @ngInject */
    function m4dealerRankingController($scope, service, $state, $http, $rootScope, $timeout) {

        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776", "#92D400", "#00A1DE", "#72C7E7", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
         $rootScope.showFilterTooltip=true;
        $rootScope.hideyear = false;
        $rootScope.hidebiannual = false;
        $rootScope.hidemonth = false;
        $rootScope.hidezone = false;
        $rootScope.hideregion = false;
        $rootScope.hidemodel = true;
        $rootScope.snaphidezone=true;
        $rootScope.snaphideregion=true;
        $rootScope.hidefactor = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.hidefactor = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.dealerhide = true;
        $rootScope.dealerScoreDealer=$rootScope.Filterarray.dealer[0].value;
        $rootScope.dealersnaphide = true;
        $rootScope.pagenumber = '8';
        $rootScope.starlabelhide = false;
        $rootScope.priorityhide = true;
        $rootScope.summaryRefresh = 1;
        $rootScope.dsnapshot = "not";
        $rootScope.dealerScoreDealer == "null"
        $scope.langcode = $rootScope.language;
        $rootScope.redfeedback="not";
        $rootScope.checkboxInitiliser();
        $scope.region = $rootScope.ALL;
        $scope.area = $rootScope.ALL;
        $scope.zone = $rootScope.ALL;
        $scope.province = $rootScope.ALL;
        $scope.zone = $rootScope.ALL;
        $scope.region = $rootScope.ALL;
        $scope.model = $rootScope.ALL;
        $scope.year =$rootScope.Filterarray.year[0].value;
        $scope.biannual =$rootScope.Filterarray.biannualx[0].value;
        $scope.month = $rootScope.Filterarray.month[1].value;
        $rootScope.legendforselectionb="Best Overall";
        $rootScope.legendforselectionw="Worst Overall";
        $scope.factor='';

        if ($scope.langcode == 'EN') {
             console.log($rootScope.Titlearray)
            $scope.export = $rootScope.Titlearray[79].ContentEN;
            $scope.table0 = $rootScope.Titlearray[80].ContentEN;
            $scope.table1 = $rootScope.Titlearray[81].ContentEN;
            $scope.table2 = $rootScope.Titlearray[82].ContentEN;
            $scope.table3 = $rootScope.Titlearray[83].ContentEN;
            $scope.table4 = $rootScope.Titlearray[84].ContentEN;
            $scope.table5 = $rootScope.Titlearray[85].ContentEN;
            $scope.table6 = $rootScope.Titlearray[86].ContentEN;
            $scope.table7 = $rootScope.Titlearray[87].ContentEN;
            $scope.table8 = "SOP Implem- ented";
            $scope.footnote = $rootScope.Footarray[1].ContentEN;
            $scope.footer = "indicates small sample and is not considered for color rankings";
            $rootScope.legendforselectionb="Best Overall";
            $rootScope.legendforselectionw="Worst Overall";
        }
        else {
            $scope.export = $rootScope.Titlearray[79].ContentReganal;
            $scope.table0 = $rootScope.Titlearray[80].ContentReganal;
            $scope.table1 = $rootScope.Titlearray[81].ContentReganal;
            $scope.table2 = $rootScope.Titlearray[82].ContentReganal;
            $scope.table3 = $rootScope.Titlearray[83].ContentReganal;
            $scope.table4 = $rootScope.Titlearray[84].ContentReganal;
            $scope.table5 = $rootScope.Titlearray[85].ContentReganal;
            $scope.table6 = $rootScope.Titlearray[86].ContentReganal;
            $scope.table7 = $rootScope.Titlearray[87].ContentReganal;
            $scope.table8 = $rootScope.Titlearray[88].ContentReganal;
            $scope.footnote = $rootScope.Footarray[1].ContentReganal;
            $scope.footer = "บ่งชี้ว่ากลุ่มตัวอย่างขนาดเล็กและไม่ได้รับการพิจารณาสำหรับการจัดอันดับของสี การส่งออกตาราง";
            $rootScope.legendforselectionb="ดีที่สุดจากทั่วประเทศ";
            $rootScope.legendforselectionw="แย่ที่สุดจากทั่วประเทศ";
        }

        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        console.log(" height :", h);
        console.log(" width :", w);
        var margin = 300;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 250;
        }
        else if (w < 960 && w > 600) {
            margin = 200;
            lineheight = 12;
            weight = 40;
        }
        else if (w < 600) {
            margin = 150;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        console.log('margin', margin);
        //////////////////////////////////////////////////////////////

        $scope.$on('changeLanguage', function (event, data) {
            $scope.langcode = data;
            $scope.showLoader = true;
             if ($scope.langcode == 'EN') {
            $rootScope.legendforselectionb="Best Overall";
            $rootScope.legendforselectionw="Worst Overall";
        }
        else {
            $rootScope.legendforselectionb="ดีที่สุดจากทั่วประเทศ";
            $rootScope.legendforselectionw="แย่ที่สุดจากทั่วประเทศ";
        }

            
            $scope.filterchange();
        });

        $scope.$on('changeyear', function (event, data) {
            $scope.year = data;
            $scope.biannual=$rootScope.ALL;
            $scope.month=$rootScope.ALL;
            $scope.showLoader = true;
            $scope.filterchange();
        });

        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual = data;
            $scope.month=$rootScope.ALL;
            $scope.showLoader = true;
            $scope.filterchange();
        });

        $scope.$on('changemonth', function (event, data) {
            $scope.month = data;
            $scope.showLoader = true;
            $scope.filterchange();
        });

        $scope.$on('changezone', function (event, data) {
            $scope.zone = data;
            $scope.region = $rootScope.ALL;
            $scope.dealer = $rootScope.ALL;
            $scope.showLoader = true;
            $scope.filterchange();
        });

        $scope.$on('changeregion', function (event, data) {
            $scope.region = data;
            $scope.dealer = $rootScope.ALL;
            $scope.showLoader = true;
            $scope.filterchange();
        });

        $scope.$on('changemodel', function (event, data) {
            $scope.model = data;
            $scope.showLoader = true;
            $scope.filterchange();
        });

        //////////////////////////////////////////////////////////////


        $scope.finished = function () {
            $timeout(function () {
                $(document).ready(function () {
                    $("#fixTable").tableHeadFixer();
                });
            }, 0);

        };

        var parameters = [{"name": "langCode", "value": $scope.langcode}, {"name":"zone","value":encodeURIComponent($scope.zone)}, {"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value":encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}];
        $scope.alert_dealer = function (dealer) {
            dealer = dealer.replace(/[*]/g, "");
            $rootScope.dealerScoreDealer = dealer;
            $state.go('triangular.ssipage11', {dealer: ''});
            console.log('state2 params:', dealer);
        }


        $("#exportCS").click(function () {
            $("#table2").table2excel({
                // exclude CSS class
                exclude: ".noExl",
                filename: "DealerScoresDownload"
            });
        });






        

           


        function createData() {
            if ($scope.langcode == 'EN') {
                console.log($rootScope.Titlearray)
                $scope.export = $rootScope.Titlearray[79].ContentEN;
                $scope.table0 = $rootScope.Titlearray[80].ContentEN;
                $scope.table1 = $rootScope.Titlearray[81].ContentEN;
                $scope.table2 = $rootScope.Titlearray[82].ContentEN;
                $scope.table3 = $rootScope.Titlearray[83].ContentEN;
                $scope.table4 = $rootScope.Titlearray[84].ContentEN;
                $scope.table5 = $rootScope.Titlearray[85].ContentEN;
                $scope.table6 = $rootScope.Titlearray[86].ContentEN;
                $scope.table7 = $rootScope.Titlearray[87].ContentEN;
                $scope.table8 = "SOP Implem- ented";
              $scope.footnote=$rootScope.Footarray[0].ContentEN;
            $scope.footnote+='<br>'+$rootScope.Footarray[1].ContentEN;
                $scope.footer = "indicates small sample and is not considered for color rankings";
                  $scope.tableregion = 'Region';
                $rootScope.legendforselectionb="Best Overall";
                $rootScope.legendforselectionw="Worst Overall";
                
            }
            else {
                $scope.export = $rootScope.Titlearray[79].ContentReganal;
                $scope.table0 = $rootScope.Titlearray[80].ContentReganal;
                $scope.table1 = $rootScope.Titlearray[81].ContentReganal;
                $scope.table2 = $rootScope.Titlearray[82].ContentReganal;
                $scope.table3 = $rootScope.Titlearray[83].ContentReganal;
                $scope.table4 = $rootScope.Titlearray[84].ContentReganal;
                $scope.table5 = $rootScope.Titlearray[85].ContentReganal;
                $scope.table6 = $rootScope.Titlearray[86].ContentReganal;
                $scope.table7 = $rootScope.Titlearray[87].ContentReganal;
                $scope.table8 = $rootScope.Titlearray[88].ContentReganal;
               $scope.footnote=$rootScope.Footarray[0].ContentReganal;
                     $scope.footnote+='<br>'+$rootScope.Footarray[1].ContentReganal;
                 $scope.tableregion = 'ภูมิภาค';
                $scope.footer = "บ่งชี้ว่ากลุ่มตัวอย่างขนาดเล็กและไม่ได้รับการพิจารณาสำหรับการจัดอันดับของสี การส่งออกตาราง";
                $rootScope.legendforselectionb="ดีที่สุดจากทั่วประเทศ";
                $rootScope.legendforselectionw="แย่ที่สุดจากทั่วประเทศ";
                 
            }



 var dealerclick = " <span class='downloadtext'>#dealer#</span>";

 $scope.dalerscorettable = {
                    id: "dealertable",
                    view: "datatable",
                     headerRowHeight :75,
                    fixedRowHeight: false,
                    rowLineHeight:30, 
                    rowHeight:30,
                    scroll: 'y',
                    position:"flex",
                    css: "my_style",
                    columns: [
                        {id: "rank", css: "columnstyle", header:{text: $scope.table0,css:'multiline'},width:50},
                        {id: "region", css: "columnstyle", header:{text: $scope.tableregion },adjust: 'true', minWidth:125},
                        {id: "dealer", css: "columnstyle", header:{text: $scope.table1},minWidth:155,fillspace:true ,adjust: 'true'},
                        {id: "ssi", header: [$scope.table2], width:70},
                        {id: "salesinitiation", header: {text: $scope.table3,  css:'multiline'},width:70},
                        {id: "dealerfacility", header:{text: $scope.table4,  css:'multiline'},width:70},
                        {id: "deal", header:{text: $scope.table5,  css:'multiline'},width:70},
                        {id: "salesperson", header:{text: $scope.table6,  css:'multiline'},width:70},
                        {id: "deliverytiming", header:{text: $scope.table7,  css:'multiline'},width:70},
                        {id: "deliveryprocess", header:{text: $scope.table8,  css:'multiline'},width:70}
                    ],
                 
                };


            service.mastersrv("SSIDealerScoreTable-getSSIDealerScoreTable", parameters).then(function (response) {
//              vm.salesData=response.data;
                $scope.records = response.data;
                console.log('response.data'+response.data[0]);
                console.log('response.data[0].counterfreeze;',response.data[0].counterfreeze);
                console.log('response.data[0].counterfreeze;',response.data[0].counterfreeze);
                console.log('response.data[0].counterfreeze;',response.data[0].counterfreeze);
                console.log('response.data[0].counterfreeze;',response.data[0].counterfreeze);
                $scope.countfreeze=response.data[0].counterfreeze;
                console.log( $scope.records);
                $scope.dalerscorettable = {
                    id: "dealertable",
                    view: "datatable",
                    fixedRowHeight: false,
                    headerRowHeight :75,
                    rowLineHeight:30, 
                    rowHeight:30,
                    scroll: 'y',
                     position:"flex",
                    topSplit: $scope.countfreeze,
                    select: "row",
                    css: "my_style",
                    tooltip: true,
                    columns: [
                        {id: "rank", css: "columnstyle", header:{text: $scope.table0 ,css:'multiline'},width:50},
                        {id: "region", css: "columnstyle", header:{text: $scope.tableregion },adjust: 'true', minWidth:125},
                        {id: "regioneng", css: "columnstyle", header:{text: $scope.tableregion },adjust: 'true'},
                        {id: "dealereng", css: "columnstyle", header:{text: $scope.table1 },adjust: 'true'},
                        {id: "dealer", css: "columnstyle", header:{text: $scope.table1},minWidth:155,fillspace:true ,adjust: 'true',template:dealerclick},
                        {id: "ssi", header: [$scope.table2],width:70,  cssFormat: mark_ssi},
                        {id: "salesinitiation", header: {text: $scope.table3,  css:'multiline'},width:70, cssFormat: mark_serviceinitiation},
                        {id: "dealerfacility", header:{text: $scope.table4,  css:'multiline'},width:70,cssFormat: mark_dealerfacility},
                        {id: "deal", header:{text: $scope.table5,  css:'multiline'},width:70,  cssFormat: mark_deal},
                        {id: "salesperson", header:{text: $scope.table6,  css:'multiline'},width:70, cssFormat: mark_salesperson},
                        {id: "deliverytiming", header:{text: $scope.table7,  css:'multiline'},width:70,  cssFormat: mark_deliverytiming},
                        {id: "deliveryprocess", header:{text: $scope.table8,  css:'multiline'},width:70,cssFormat: mark_deliveryprocess}
                        ],
                        data: $scope.records,
                        resizeColumn: false,
                        ready: function () {
                            $$("dealertable").hideProgress();
                            $$("dealertable").hideColumn("regioneng");
                            $$("dealertable").hideColumn("dealereng");
                        },
//                        on: {
//                        'onClick': function (e, id) {
//                            var datatable = $$("dealertable");
//                             var selectedId = datatable.getSelectedId()
//                              var row = datatable.getItem(selectedId);
//                             $scope.alert_dealer(row.dealereng)
//                        }
                        onClick:{
                                "downloadtext":function(e, id){
                                    console.log("this.getItem(id).dealereng",this.getItem(id).dealereng);
                                    
                                    console.log("this.getItem(id).dealereng",this.getItem(id).dealereng)
                                    console.log("this.getItem(id).dealereng",this.getItem(id).dealereng)
                                    console.log("this.getItem(id).dealereng",this.getItem(id).dealereng)
                                    
                                    console.log("this.getItem(id).dealereng",this.getItem(id).dealereng)
                                    console.log("this.getItem(id).dealereng",this.getItem(id).dealereng)
                                    
                                    console.log("this.getItem(id).dealereng",this.getItem(id).dealereng)
                                    console.log("this.getItem(id).dealereng",this.getItem(id).dealereng)
                                    $scope.dealer=  this.getItem(id).dealereng;
                                    $scope.region=  this.getItem(id).region;
                                    $scope.alert_dealer(this.getItem(id).dealereng);

                                }}

//                    },
                        
//                        on: {
//                       
//                        "onresize":webix.once(function(){ 
//                            this.adjustRowHeight("dealer", true); 
//                        })
//                        ,
//                        'onItemClick': function (e, id) {
//                            var datatable = $$("dealertable");
//                            var firstid = datatable.getFirstId();
//                            var secondid = datatable.getNextId(firstid, 1);
//                            var thirdid = datatable.getNextId(secondid, 1);
//                            var forthid = datatable.getNextId(thirdid, 1);
//                            var fifthid = datatable.getNextId(forthid, 1);
//                            var sixthid = datatable.getNextId(fifthid, 1);
//                            var seventhid = datatable.getNextId(sixthid, 1);
//                            var eighthid = datatable.getNextId(seventhid, 1);
//                            var p=0;
//                            if($scope.countfreeze==8){
////                                alert("count 7");
//                                    $$("dealertable").remove(firstid);
//                                    $$("dealertable").remove(secondid);
//                                    $$("dealertable").remove(thirdid);
//                                    $$("dealertable").remove(forthid);
//                                    $$("dealertable").remove(fifthid);
//                                    $$("dealertable").remove(sixthid);
//                                    $$("dealertable").remove(seventhid);
//                                    $$("dealertable").remove(eighthid);
//                                     var selectedId = datatable.getSelectedId()
//                                    if (selectedId != firstid && selectedId != secondid && selectedId != thirdid && selectedId != forthid && selectedId != fifthid && selectedId != sixthid && selectedId != seventhid && selectedId != eighthid )
//                                    {
//                                        var row = datatable.getItem(selectedId);
//                                        $scope.dealer = row.dealereng;
//                                        $scope.region = row.regioneng;
//                                        parameters = [{"name": "langCode", "value": $scope.langcode}, {"name": "dealer", "value": $scope.dealer},{"name":"zone","value":encodeURIComponent($scope.zone)}, {"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value":encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}]
//                                        service.mastersrv("SSIDealerScoreTable-getSSITOP3RowsDealerScore", parameters).then(function (response) {
//                                            console.log(response.data);
//                                                $$("dealertable").add(response.data[0], 0);
//                                                $$("dealertable").add(response.data[1], 1);
//                                                $$("dealertable").add(response.data[2], 2);
//                                               $$("dealertable").define("topSplit", 3);
//                                                $$("dealertable").refreshColumns();
//                                                $scope.countfreeze=3;
//                                           });
//                                    }
//                                     
//                            }
//                            else  if($scope.countfreeze==6){
////                                alert("count 6");
//                                $$("dealertable").remove(firstid);
//                                    $$("dealertable").remove(secondid);
//                                    $$("dealertable").remove(thirdid);
//                                    $$("dealertable").remove(forthid);
//                                    $$("dealertable").remove(fifthid);
//                                    $$("dealertable").remove(sixthid);
//                                     var selectedId = datatable.getSelectedId()
//                                    if (selectedId != firstid && selectedId != secondid && selectedId != thirdid && selectedId != forthid && selectedId != fifthid && selectedId != sixthid)
//                                    {
//                                        var row = datatable.getItem(selectedId);
//                                        $scope.dealer = row.dealereng;
//                                        $scope.region = row.regioneng;
//                                        parameters = [{"name": "langCode", "value": $scope.langcode}, {"name": "dealer", "value": $scope.dealer},{"name":"zone","value":encodeURIComponent($scope.zone)}, {"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value":encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}]
//                                        service.mastersrv("SSIDealerScoreTable-getSSITOP3RowsDealerScore", parameters).then(function (response) {
//                                            console.log(response.data);
//                                                $$("dealertable").add(response.data[0], 0);
//                                                $$("dealertable").add(response.data[1], 1);
//                                                $$("dealertable").add(response.data[2], 2);
//                                                $$("dealertable").define("topSplit", 3);
//                                                $$("dealertable").refreshColumns();
//                                                $scope.countfreeze=3;
//                                           });
//                                    }
//                            }
//                            else  if($scope.countfreeze==3){
////                                alert("count 3");
//                                $$("dealertable").remove(firstid);
//                                    $$("dealertable").remove(secondid);
//                                    $$("dealertable").remove(thirdid);
//                                     var selectedId = datatable.getSelectedId()
//                                    if (selectedId != firstid && selectedId != secondid && selectedId != thirdid)
//                                    {
//                                        var row = datatable.getItem(selectedId);
//                                        $scope.dealer = row.dealereng;
//                                        $scope.region = row.regioneng;
//                                        parameters = [{"name": "langCode", "value": $scope.langcode}, {"name": "dealer", "value": $scope.dealer},{"name":"zone","value":encodeURIComponent($scope.zone)}, {"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value":encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}]
//                                        service.mastersrv("SSIDealerScoreTable-getSSITOP3RowsDealerScore", parameters).then(function (response) {
//                                            console.log(response.data);
//                                                $$("dealertable").add(response.data[0], 0);
//                                                $$("dealertable").add(response.data[1], 1);
//                                                $$("dealertable").add(response.data[2], 2);
//                                                $$("dealertable").define("topSplit", 3);
//                                                $$("dealertable").refreshColumns();
//                                                $scope.countfreeze=3;
//                                           });
//                                    }
//                            }
//
//                           
//                        }
//
//                    },

                   
                };
                
                  
              
            })
            
//            dealertable

        }
        
        
         $scope.doSome = function(){
                webix.extend($$("dealertable"), webix.ProgressBar);
               $$("dealertable").showProgress({
             type:"icon",
           });
         }
        createData();
        
                
        function mark_ssi(value, config) {
            if(config["ssicolor"]!="White")
                {console.log({"background-color": config["ssicolor"],"color": '#FFFFFF !important'});
                    
                    return {"background-color": config["ssicolor"],"color": '#FFFFFF !important'}; 
                }
                else
                {
                     return {"background-color": config["ssicolor"]}; 
                }
                return value;
           
        }

        function mark_serviceinitiation(value, config) {
            if(config["salesinitiationcolor"]!="White")
                {console.log({"background-color": config["salesinitiationcolor"],"color": '#FFFFFF !important'});
                    
                    return {"background-color": config["salesinitiationcolor"],"color": '#FFFFFF !important'}; 
                }
                else
                {
                     return {"background-color": config["salesinitiationcolor"]}; 
                }
                return value;
    
        }
        
        
        function mark_dealerfacility(value, config) {
            if(config["dealerfacilitycolor"]!="White")
                {console.log({"background-color": config["dealerfacilitycolor"],"color": '#FFFFFF !important'});
                    
                    return {"background-color": config["dealerfacilitycolor"],"color": '#FFFFFF !important'}; 
                }
                else
                {
                     return {"background-color": config["dealerfacilitycolor"]}; 
                }
                return value;
           
        }
        function mark_deal(value, config) {
            if(config["dealcolor"]!="White")
                {console.log({"background-color": config["dealcolor"],"color": '#FFFFFF !important'});
                    
                    return {"background-color": config["dealcolor"],"color": '#FFFFFF !important'}; 
                }
                else
                {
                     return {"background-color": config["dealcolor"]}; 
                }
               
            return value;
        }
        function mark_salesperson(value, config) {
             if(config["salespersoncolor"]!="White")
                {console.log({"background-color": config["salespersoncolor"],"color": '#FFFFFF !important'});
                    
                    return {"background-color": config["salespersoncolor"],"color": '#FFFFFF !important'}; 
                }
                else
                {
                     return {"background-color": config["salespersoncolor"]}; 
                }
               
            return value;
        }
        function mark_deliverytiming(value, config) {
            if(config["deliverytimingcolor"]!="White")
                {console.log({"background-color": config["deliverytimingcolor"],"color": '#FFFFFF !important'});
                    
                    return {"background-color": config["deliverytimingcolor"],"color": '#FFFFFF !important'}; 
                }
                else
                {
                     return {"background-color": config["deliverytimingcolor"]}; 
                }
                return value;
            
        }
        function mark_deliveryprocess(value, config) {
            if(config["deliveryprocesscolor"]!="White")
                {console.log({"background-color": config["deliveryprocesscolor"],"color": '#FFFFFF !important'});
                    
                    return {"background-color": config["deliveryprocesscolor"],"color": '#FFFFFF !important'}; 
                }
                else
                {
                     return {"background-color": config["deliveryprocesscolor"]}; 
                }
                return value;
           
        }
$scope.exportdata = function() {
  webix.toExcel($$("dealertable"), {
     filterHTML:true
});
//  webix.toExcel($$("dealertable"));
}
        $scope.filterchange = function () {
            console.log("filter change of dscore called");
            parameters = [{"name": "langCode", "value": $scope.langcode}, {"name":"zone","value":encodeURIComponent($scope.zone)}, {"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value":encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}];
            createData();
        }
    }
})();