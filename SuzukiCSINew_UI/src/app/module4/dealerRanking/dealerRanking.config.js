(function() {
    'use strict';

    angular
        .module('app.module4.dealerRanking')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

           $stateProvider
         .state('triangular.ssidealerRanking', {
            url: '/ssidealerRanking',
            views: {
                '': {
                   templateUrl: 'app/module4/dealerRanking/dealerRanking.tmpl.html',
                    controller: 'm4dealerRankingController',
                    controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module4/dealerRanking/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
            
        });
        
//         triMenuProvider.addMenu({
//                      name: 'Dealer Ranking',
//                    icon: 'zmdi zmdi-view-list-alt',
//                    type: 'link',
//                    priority: 1.7,
//                     state: 'triangular.csidealerRanking',
//                });
//             triMenuProvider.removeMenu('triangular.dealerRanking');   
                
                
    }
})();