/* global Highcharts */

(function () {
    'use strict';
    angular.module('app')
            .service('service', service);

    function service($http, URL, $rootScope, $state) {
        this.barchart = barchart;
        this.modelbarchart = modelbarchart;
        this.barchartmiddle = barchartmiddle;
        this.dsnapplainbar = dsnapplainbar;
        this.plainbar = plainbar;
        this.barplainbarsplit = barplainbarsplit;
        this.barplainbar = barplainbar;
        this.barline = barline;
        this.verticalline = verticalline;
        this.horizontalline = horizontalline;
        this.multigraph = multigraph;
        this.stackedgraph = stackedgraph;
        this.mastersrv = mastersrv;
        this.multbarchartsplit = multbarchartsplit;
        this.multbarchart = multbarchart;
        this.sopplainbar = sopplainbar;
        this.ssi_filterfunction = ssi_filterfunction;
        this.csi_filterfunction = csi_filterfunction;
        this.loyalitybar = loyalitybar;
        this.stackmultigraph = stackmultigraph;
        this.regplainbar = regplainbar;
        this.dsnapbarchart = dsnapbarchart;
        this.dlrstackedgraph = dlrstackedgraph;
        this.dsmultigraph = dsmultigraph;
        this.csi_titles = csi_titles;
        this.summaryplainbar = summaryplainbar;
        this.creditsummaryplainbar = creditsummaryplainbar;
        this.clicksummaryplainbar = clicksummaryplainbar;
        this
        var axis;
        var position;
        var align;
        if ($rootScope.language == 'TH') {
            $rootScope.downloadpng = "ดาวน์โหลดไฟล์รูปภาพ PNG";
            $rootScope.downloadcsv = "ดาวน์โหลดไฟล์ CSV";
            $rootScope.downloadxls = "ดาวน์โหลดไฟล์ XLS";
            $rootScope.downloadheader = "เมนูเลือกการแสดงแผนภูมิ (Chart)";
            $rootScope.firstchart = "หมายเหตุ: แสดงเฉพาะข้อมูลล่าสุดที่มี";
        } else {
            $rootScope.downloadpng = "Download PNG Image";
            $rootScope.downloadcsv = "Download CSV";
            $rootScope.downloadxls = "Download XLS";
            $rootScope.downloadheader = "Chart Context Menu";
            $rootScope.firstchart = "Note: Data updated up to the lastest available month";
        }


        function destroyCharts(chartName) {
            alert(chartName)
            if (chartName)
                chartName.destroy();
        }
        function csi_titles() {
//        $rootScope.Titlearray=[];
//        $rootScope.Footarray=[];
            var parameters = [{"name": "moduleType", "value": "CSI"}];
            mastersrv("Systemuserview-getTitles", parameters).then(function (result) {
                $rootScope.Titlearray = result.data;
            });
            mastersrv("Systemuserview-getFootnotes", parameters).then(function (result) {
                $rootScope.Footarray = result.data;
            });
            mastersrv("Systemuserview-getPagenote", parameters).then(function (result) {
                $rootScope.PageNotearray = result.data;
            });
        }


        function csi_filterfunction() {
            $rootScope.Filterarray = [];
            var study = {"name": "All", "value": "All"};
            var none = {"name": "None", "value": "None"};
            var all = {"name": "All", "value": "All"};
            if ($rootScope.language == 'EN') {
                study = {"name": "All", "value": "All"};
                none = {"name": "None", "value": "None"};
                all = {"name": "All", "value": "All"};
            } else if ($rootScope.language == 'TH') {
                study = {"name": "All", "value": "全部"};
                none = {"name": "None", "value": "ไม่แสดงผล"};
                all = {"name": "All", "value": "全部"};
            }
            var parameter = [{"name": "langCode", "value": $rootScope.language}];
            mastersrv("Filters-getCSIZone", parameter).then(function (result) {
                $rootScope.Filterarray.zone = angular.copy(result.data);
                $rootScope.Filterarray.zones = angular.copy($rootScope.Filterarray.zone);
                $rootScope.Filterarray.zones2 = angular.copy($rootScope.Filterarray.zone);
                $rootScope.Filterarray.zone.splice(0, 0, study);
                $rootScope.Filterarray.zones.splice(0, 0, none);
                console.log("zone filter", $rootScope.Filterarray.zone);
            });
            mastersrv("Filters-getRegion", parameter).then(function (result) {
                $rootScope.Filterarray.region = result.data;
                $rootScope.Filterarray.regions = angular.copy($rootScope.Filterarray.region);
                $rootScope.Filterarray.region.splice(0, 0, study);
                console.log("zone filter", $rootScope.Filterarray.region);
//                  $rootScope.Filterarray.region.push("All");
                $rootScope.region1 = $rootScope.Filterarray.region[1].name;//"Bangkok & Greater";
                $rootScope.region2 = $rootScope.Filterarray.region[2].name;//"Central and East";


            });
            mastersrv("Filters-getDealers", parameter).then(function (result) {
                $rootScope.Filterarray.dealer = result.data;
                $rootScope.Filterarray.dealers = angular.copy($rootScope.Filterarray.dealer);
                $rootScope.Filterarray.dealers2 = angular.copy(result.data);

                $rootScope.Filterarray.dealer.splice(0, 0, study);
                $rootScope.Filterarray.dealers.splice(0, 0, none);
//                 console.log("dealerdsssssssssssssssss::::::::::::::::::")
//                console.log($rootScope.Filterarray.dealer)
//                $rootScope.dealer = $rootScope.Filterarray.dealer[0].name;
//                $rootScope.dealer1 = $rootScope.Filterarray.dealer[1].name;//"$rootScope.Filterarray.dealer[1].name;";
//                $rootScope.dealer2 = $rootScope.Filterarray.dealer[2].name;//"Ariyakij";
            });
            mastersrv("Filters-getModels", parameter).then(function (result) {
                $rootScope.Filterarray.model = result.data;
                $rootScope.Filterarray.model.splice(0, 0, study);
                $rootScope.model = $rootScope.Filterarray.model[0].name;
            });
            mastersrv("Filters-getYear", parameter).then(function (result) {
                $rootScope.Filterarray.year = result.data;
                $rootScope.year = $rootScope.Filterarray.year[0].name;
            });
            var yearobj = [{"name": "year", "value": $rootScope.year}, {"name": "langCode", "value": $rootScope.language}];
            mastersrv("Filters-getBiannual", yearobj).then(function (result) {
                $rootScope.Filterarray.biannual = result.data;
                $rootScope.Filterarray.biannual.splice(0, 0, all);
            });

            mastersrv("Filters-getBiannual", [{"name": "year", "value": 'All'}, {"name": "langCode", "value": $rootScope.language}]).then(function (result) {
                $rootScope.Filterarray.biannualx = result.data;
                $rootScope.Filterarray.biannualx.splice(0, 0, all);

            });
            mastersrv("Filters-getMonth", yearobj).then(function (result) {
                $rootScope.Filterarray.month = result.data;
                $rootScope.Filterarray.month.splice(0, 0, all);
                $rootScope.month = $rootScope.Filterarray.month[1].name;
//                    console.log("zone filter",$rootScope.Filterarray.month);
                $rootScope.downloadurl = "http://webapps.wheelmonk.com/SuzukiTHDashboardUAT/assets/DCSI_Wave_12_Client.xlsx";
            });
            mastersrv("Filters-getFactors", parameter).then(function (result) {
                $rootScope.Filterarray.factor = result.data;
//                     $rootScope.Filterarray.factor.push("All");
            });
            mastersrv("Filters-getSOPAttributes", parameter).then(function (result) {
                $rootScope.Filterarray.sop = result.data;
                console.log("KPI Attributes ::::::::", $rootScope.Filterarray.sop);
            });
            mastersrv("Filters-getPeriods", parameter).then(function (result) {
                $rootScope.Filterarray.period = result.data;
                console.log("FiltersgetPeriod", $rootScope.Filterarray.period);
                $rootScope.period = $rootScope.Filterarray.period[0].name;
//                    $rootScope.Filterarray.dealer.splice(0, 0, study);
            });
        }

        function ssi_filterfunction() {
            $rootScope.Filterarray = [];
            mastersrv("SSIFilters-getSSIArea").then(function (result) {
                $rootScope.Filterarray.area = result.data;
                $rootScope.Filterarray.area.splice(0, 0, "All");

            });

            mastersrv("SSIFilters-getSSIZone").then(function (result) {
                $rootScope.Filterarray.zone = result.data;
                $rootScope.Filterarray.zone.splice(0, 0, "All");

            });

            mastersrv("SSIFilters-getSSIProvince").then(function (result) {
                $rootScope.Filterarray.province = result.data;
                $rootScope.Filterarray.province.splice(0, 0, "All");

            });

            mastersrv("SSIFilters-getYears").then(function (result) {
                $rootScope.Filterarray.year = result.data;
            });
            mastersrv("SSIFilters-getWaves").then(function (result) {
                $rootScope.Filterarray.waves = result.data;
                $rootScope.downloadurl = "http://webapps.wheelmonk.com/SuzukiTHDashboardUAT/assets/DCSI_Wave_12_Client.xlsx";
            });
            mastersrv("SSIFilters-getCity").then(function (result) {
                $rootScope.Filterarray.city = result.data;
                $rootScope.Filterarray.city.splice(0, 0, "All");
            });
            mastersrv("SSIFilters-getRegion").then(function (result) {
                $rootScope.Filterarray.region = result.data;

                $rootScope.Filterarray.region.splice(0, 0, "All");

//                 $rootScope.Filterarray.region.push("All");

            });
            mastersrv("SSIFilters-getModels").then(function (result) {
                $rootScope.Filterarray.model = result.data;
                $rootScope.Filterarray.model.splice(0, 0, "All");

            });
            mastersrv("SSIFilters-getDealers").then(function (result) {
                $rootScope.Filterarray.dealer = result.data;
                $rootScope.Filterarray.dealers = angular.copy($rootScope.Filterarray.dealer);
                $rootScope.Filterarray.dealer.splice(0, 0, "All");

            });
            mastersrv("SSIFilters-getFactors").then(function (result) {
                $rootScope.Filterarray.factor = result.data;
//                 $rootScope.Filterarray.factor.push("All");

            });
            mastersrv("SSIFilters-getSOPAttributes").then(function (result) {
                $rootScope.Filterarray.sop = result.data;
            });


        }
//filter end



        Highcharts.setOptions({
            chart: {
                style: {
//            fontFamily: 'Roboto',
                    fontFamily: 'Roboto Condensed'
                }
            }
        });



//THE MASTER SERVICE replace in filter
        function mastersrv(actionstr, obj) {

            var tempurl = URL + actionstr;

            if (obj) {
                for (var k = 0; k < obj.length; k++) {
                    tempurl = tempurl + "&" + obj[k].name;
                    tempurl = tempurl + "=" + obj[k].value;
                }
            }

            console.log(tempurl);
            var temp = $http({
                method: "GET",
                url: tempurl,
                dataType: "jsonp",
                data: '',
                headers: {
                    "Content-Type": "application/json;charset=UTF-8"
                }
            });

            return temp;

        }




//bar chart service
        function barchartmiddle(type, color, data, margin, weight, spacing, seriesname, fontsize, stepsize, maxlength) {
            var category = [];
            for (var i = 0; i < data.length; i++) {
                console.log("inisde here is the fucniton");
                category[i] = data[i].name;
            }

            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    type: type,
                    marginLeft: margin,
                    marginRight: 25,
//              marginTop: 20

                },
                title: {
                    text: null
                },
                xAxis: {
                    categories: category,
                    title: {
                        text: null
                    },
                    "labels": {
                        "overflow": "justify",
                        style: {fontSize: fontsize},
                        step: stepsize
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
                    min: 300,
                    max: maxlength,
                    title: {
                        text: null
                    },
                    gridLineWidth: 0,
                    minorGridLineWidth: 0,
                    gridLineColor: 'transparent',
                    labels: {enabled: false,
                        overflow: 'justify'
                    }
                },
                plotOptions: {
                    series: {
                        pointWidth: weight,
                        groupPadding: spacing
//                         cursor: 'pointer',
//                        point: {
//                            events: {
//                                click: function () {
//                                    alert('Category: ' + this.category + ', value: ' + this.y);
//                                }
//                            }
//                        }

                    },
                    bar: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            inside: true,
                            align: 'right',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }

                        }
                    },
                    column: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            y: 30,
                            align: 'center',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }


                        },
                        depth: 25
                    }
                },
                credits: {
                    enabled: false
                },
//         size: {height:height,
//                width:width},

                legend: {
                    enabled: false
                },
                tooltip: {
                    formatter: function () {
                        var s =
                                this.point.options.tooltip;
                        return s;
                    },
                    crosshairs: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChart();
                                    }
                                }]
                        }
                    }
                },
                navigation: {
                    buttonOptions: {
                        verticalAlign: 'top',
                        align: 'right',
                        y: -15
                    }
                },
                series: [{
                        color: color,
                        data: data,
                        name: seriesname
                    }]
            }
            ;
            return tempbar;

        }





//bar chart service
        function modelbarchart(type, color, data, margin, weight, spacing, seriesname, fontsize, stepsize, maxlength) {
            var category = [];
            for (var i = 0; i < data.length; i++) {
                console.log("inisde here is the fucniton");
                category[i] = data[i].name;
            }

            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    type: type,
                    marginLeft: margin,
                    marginRight: 25,
//              marginTop: 20

                },
                title: {
                    text: null
                },
                xAxis: {
                    categories: category,
                    title: {
                        text: null
                    },
                    "labels": {
                        "overflow": "justify",
                        style: {fontSize: fontsize},
                        step: stepsize
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
                    max: maxlength,
                    min: 0,
                    title: {
                        text: null
                    },
                    gridLineWidth: 0,
                    minorGridLineWidth: 0,
                    gridLineColor: 'transparent',
                    labels: {enabled: false,
                        overflow: 'justify'
                    }
                },
                plotOptions: {
                    series: {
                        pointWidth: weight,
                        groupPadding: spacing,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    var str = this.options.modelname;
                                    str = str.replace("*", '');
                                    str = str.replace("*", '');
                                    $rootScope.changedmodel = str;
                                    $rootScope.$broadcast("changemodelgraph", $rootScope.changedmodel);
                                    console.log('state2 params:', $rootScope.changedmodel);
                                    console.log('state2 params:', $rootScope.changedmodel);

                                }
                            }
                        }

                    },
                    bar: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            inside: true,
                            align: 'right',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }

                        }
                    },
                    column: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            y: 30,
                            align: 'right',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }


                        },
                        depth: 25
                    }
                },
                credits: {
                    enabled: false
                },
//         size: {height:height,
//                width:width},

                legend: {
                    enabled: false
                },
                tooltip: {
                    formatter: function () {
                        var s =
                                this.point.options.tooltip;
                        return s;
                    },
                    crosshairs: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChart();
                                    }
                                }]
                        }
                    }
                },
                navigation: {
                    buttonOptions: {
                        verticalAlign: 'top',
                        align: 'right',
                        y: -15
                    }
                },
                series: [{
                        color: color,
                        data: data,
                        name: seriesname
                    }]
            }
            ;
            return tempbar;

        }

//bar chart service
        function barchart(type, color, data, margin, weight, spacing, seriesname, fontsize, stepsize, maxlength) {
            var category = [];
            for (var i = 0; i < data.length; i++) {
                console.log("inisde here is the fucniton");
                category[i] = data[i].name;
            }

            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    events: {
                        load: function () {
                            $rootScope.chartloadcount = $rootScope.chartloadcount + 1;
                            console.log($rootScope.chartloadcount);
                            if ($rootScope.chartloadcount > 21) {

                                $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                            }

                            if ($rootScope.chartloadcount > 23) {
                                $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                            }
                        }},
                    type: type,
                    marginLeft: margin
//              marginTop: 20

                },
                title: {
                    text: null
                },
                xAxis: {
                    categories: category,
                    title: {
                        text: null
                    },
                    "labels": {
                        "overflow": "justify",
                        style: {fontSize: fontsize},
                        step: stepsize
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
                    max: maxlength,
                    min: 0,
                    title: {
                        text: null
                    },
                    gridLineWidth: 0,
                    minorGridLineWidth: 0,
                    gridLineColor: 'transparent',
                    labels: {enabled: false,
                        overflow: 'justify'
                    }
                },
                plotOptions: {
                    series: {
                        pointWidth: weight,
                        groupPadding: spacing
//                         cursor: 'pointer',
//                        point: {
//                            events: {
//                                click: function () {
//                                    alert('Category: ' + this.category + ', value: ' + this.y);
//                                }
//                            }
//                        }

                    },
                    bar: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            inside: true,
                            align: 'right',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }

                        }
                    },
                    column: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            y: 30,
                            align: 'right',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }


                        },
                        depth: 25
                    }
                },
                credits: {
                    enabled: false
                },
//         size: {height:height,
//                width:width},

                legend: {
                    enabled: false
                },
                tooltip: {
                    formatter: function () {
                        var s =
                                this.point.options.tooltip;
                        return s;
                    },
                    crosshairs: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChart();
                                    }
                                }]
                        }
                    }
                },
                series: [{
                        color: color,
                        data: data,
                        name: seriesname
                    }]
            }
            ;
            return tempbar;

        }




        function dsnapbarchart(maxlength, type, data, margin, weight, spacing, seriesname, fontsize, stepsize, suffix) {

            var scattername = [];
            for (var i = 0; i < data.length; i++) {
                scattername[i] = data[i].name;
            }

            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    type: type,
                    marginLeft: margin,
                    marginTop: 20

                },
                title: {
                    text: null
                },
                xAxis: {
                    categories: scattername,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    title: {
                        text: null
                    },
                    "labels": {
                        style: {fontSize: fontsize
                        },
                        step: stepsize
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
                    max: maxlength,
                    title: {
                        text: null
                    },
                    gridLineWidth: 0,
                    minorGridLineWidth: 0,
                    gridLineColor: 'transparent',
                    labels: {enabled: false,
                        overflow: 'justify'
                    }
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            inside: true,
                            align: 'right',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '10px'
                            }

                        },
                        pointWidth: weight,
                        groupPadding: spacing,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    var str = this.options.dealername;
                                    str = str.replace("*", '');
                                    str = str.replace("*", '');
                                    $rootScope.dealerScoreDealer = str;
                                    $state.go('triangular.csipage11', {dealer: 'All-'});
                                    console.log('state2 params:');
                                }
                            }
                        }

                    },
                    column: {
                        depth: 25
                    }
                },
                credits: {
                    enabled: false
                },
//         size: {height:height,
//                width:width},

                legend: {
                    enabled: false
                },
                tooltip: {
                    formatter: function () {
                        var s = this.point.options.tooltip;
                        return s + suffix;
                    },
                    crosshairs: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChart();
                                    }
                                }]
                        }
                    }
                },
                series: [{
                        color: data.color,
                        data: data

                    }]
            }
            ;
            return tempbar;

        }



//multideaer spilt summaru

        function multbarchartsplit(maxlength, type, data, margin, weight, spacing, seriesname, fontsize, stepsize, suffix) {
            var categories = [];
            for (var i = 0; i < data.length; i++) {
                categories[i] = data[i].name;
            }

            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    events: {
                        load: function () {
                            $rootScope.chartloadcount = $rootScope.chartloadcount + 1;
                            console.log($rootScope.chartloadcount);

                            if ($rootScope.chartloadcount > 21) {

                                $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                            }


                            if ($rootScope.chartloadcount > 23) {

                                $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                            }
                        },
                    },
                    type: type,
                    marginLeft: margin,
                    marginTop: 35,
                    marginBottom: 5

                },
                title: {
                    text: null
                },
                xAxis: {
                    categories: categories,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    title: {
                        text: null
                    },
                    "labels": {
//                        "overflow": "justify",
                        style: {fontSize: "8px",
                            textOverflow: 'none'
//                            paddingBottom: "1px"
                        },
                        step: 1
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
                    max: maxlength,
                    title: {
                        text: null
                    },
                    gridLineWidth: 0,
                    minorGridLineWidth: 0,
                    gridLineColor: 'transparent',
                    labels: {enabled: false,
                        overflow: 'justify',
                        step: 1
                    }
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            inside: true,
                            align: 'right',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '10px'
                            }

                        },
                        pointWidth: weight,
                        groupPadding: spacing,
                        cursor: 'pointer',
                        point: {
//                            events: {
//                                click: function () {
//                                    var str = this.options.dealername;
//                                    str = str.replace("*", '');
//                                    str = str.replace("*", '');
//                                    $rootScope.dealerScoreDealer = str;
//                                    $state.go('triangular.csipage11', {dealer: 'All-'});
//                                    console.log('state2 params:');
//                                }
//                            }
                        }

                    },
                    column: {
                        depth: 25
                    }
                },
                credits: {
                    enabled: false
                },
//         size: {height:height,
//                width:width},

                legend: {
                    enabled: false
                },
                tooltip: {
                    formatter: function () {
                        var s = this.point.options.tooltip;
                        return s + suffix;
                    },
                    crosshairs: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChart();
                                    }
                                }]
                        }
                    }
                },
                series: [{
                        color: data.color,
                        data: data

                    }]
            }
            ;
            return tempbar;

        }

//multicolor barchart
//bar chart service
        function multbarchart(maxlength, type, data, margin, weight, spacing, seriesname, fontsize, stepsize, suffix) {
            var categories = [];
            for (var i = 0; i < data.length; i++) {
                categories[i] = data[i].name;
            }

            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    events: {
                        load: function () {
                            $rootScope.chartloadcount = $rootScope.chartloadcount + 1
                            console.log($rootScope.chartloadcount)
                            if ($rootScope.chartloadcount > 21) {

                                $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                            }
                            if ($rootScope.chartloadcount > 23) {

                                $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                            }
                        }
                    },
                    type: type,
                    marginLeft: margin,
                    marginTop: 35,
                    marginBottom: 5

                },
                title: {
                    text: null
                },
                xAxis: {
                    categories: categories,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    title: {
                        text: null
                    },
                    "labels": {
                        "overflow": "justify",
                        style: {fontSize: fontsize,
                            paddingBottom: "1px"
                        },
                        step: stepsize
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
                    max: maxlength,
                    title: {
                        text: null
                    },
                    gridLineWidth: 0,
                    minorGridLineWidth: 0,
                    gridLineColor: 'transparent',
                    labels: {enabled: false,
                        overflow: 'justify'
                    }
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            inside: true,
                            align: 'right',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '10px'
                            }

                        },
                        pointWidth: weight,
                        groupPadding: spacing,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    var str = this.options.dealername;
                                    str = str.replace("*", '');
                                    str = str.replace("*", '');
                                    $rootScope.dealerScoreDealer = str;
                                    $state.go('triangular.csipage11', {dealer: 'All-'});
                                    console.log('state2 params:');
                                }
                            }
                        }

                    },
                    column: {
                        depth: 25
                    }
                },
                credits: {
                    enabled: false
                },
//         size: {height:height,
//                width:width},

                legend: {
                    enabled: false
                },
                tooltip: {
                    formatter: function () {
                        var s = this.point.options.tooltip;
                        return s + suffix;
                    },
                    crosshairs: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChart();
                                    }
                                }]
                        }
                    }
                },
                series: [{
                        color: data.color,
                        data: data

                    }]
            }
            ;
            return tempbar;

        }




        function loyalitybar(title, response, colorpoint, color) {

//           var categories=[];
//         for(var i=0;i<response.data[0].data.length;i++){
//                        categories[i]=response.data[0].data[i].category;
//
//                    }
            var xaxisname = response[0].axisname;
            var categoryname = title;
            console.log(categoryname);
            var multi_return =
                    {lang: {
                            downloadPNG: $rootScope.downloadpng,
                            downloadCSV: $rootScope.downloadcsv,
                            downloadXLS: $rootScope.downloadxls,
                            contextButtonTitle: $rootScope.downloadheader
                        },
                        "chart": {
                            events: {
                                load: function () {
                                    $rootScope.chartloadcount = $rootScope.chartloadcount + 1;
                                    console.log($rootScope.chartloadcount);
                                    if ($rootScope.chartloadcount > 21) {

                                        $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                                    }
                                    if ($rootScope.chartloadcount > 23) {
                                        $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                                    }
                                }
                            },
                            "type": "column"
                        },
                        colors: color,
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                            crosshairs: false
                        },
                        plotOptions: {
                            series: {
                                borderWidth: 0,
                                maxPointWidth: 40,
//                             cursor: 'pointer',
//                                point: {
//                                    events: {
//                                        click: function () {
//                                            alert('Category: ' + this.category + ', value: ' + this.y);
//                                        }
//                                    }
//                                },

                                dataLabels: {
                                    enabled: true,
                                    formatter: function () {
                                        var pcnt = this.point.options.bartitle;
                                        return pcnt;
                                    },
                                    color: '#000000',
                                    y: -2,
//                                            align: 'right',
                                    style: {
                                        //color: '#FFFFFF',
                                        textShadow: 'none',
                                        fontWeight: 'light',
                                        fontSize: '14px'
                                    }
                                }
                            }
                        },
                        "series": response,
                        "title": {
                            "text": title,
                            align: "center",
                            margin: 10,
                            style: {
                                textShadow: 'none',
                                fontWeight: '400',
                                fontSize: '19px'

                            }
                        },
                        "credits": {
                            "enabled": false
                        },
                        "loading": false,
                        "size": {},
                        "xAxis": {
                            gridLineWidth: 0,
                            categories: [categoryname],
                            lineColor: 'transparent',
                            tickLength: 0,
                            minorGridLineWidth: 0,
                            labels: {
                                enabled: false
                            },
                            title: {
                                style: {
                                    color: '#556977',
                                    fontSize: '12px'
                                },
                                text: xaxisname
                            }

                        },
                        "yAxis": {
                            title: {
                                text: null,
                                align: 'high'
                            },
                            labels: {
                                enabled: false
                            },
                            gridLineWidth: 0,
                            allowDecimals: true,
                            min: 0

                        },
                        legend: {
                            itemStyle: {
                                // font: '9pt Trebuchet MS, Verdana, sans-serif',
                                fontSize: '11px'
                                        //color: '#A0A0A0'
                            },
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom',
                            enabled: true

                        },
                        exporting: {
                            formAttributes: {
                                target: '_blank'
                            },
                            buttons: {
                                contextButton: {
                                    menuItems: [{
                                            text: $rootScope.downloadxls,
                                            onclick: function () {
                                                this.downloadXLS();
                                            }
                                        }, {
                                            text: $rootScope.downloadcsv,
                                            onclick: function () {
                                                this.downloadCSV();
                                            }
                                        }, {
                                            text: $rootScope.downloadpng,
                                            onclick: function () {
                                                this.exportChart();
                                            }
                                        }]
                                }
                            }
                        }
                    };

            return multi_return;


        }



        function summaryplainbar(type, color, data, seriesname, margin, weight, spacing, pointdata, markersize, colorpoint, maxwidth, labelenable, xaxistitle) {

            var sdata = [];
            var wdata = [];
            var scattername = [];
            for (var i = 0; i < data.length; i++) {
                sdata[i] = data[i].svalue;
            }
            for (var i = 0; i < data.length; i++) {
                wdata[i] = data[i].svalue1;
            }

            for (var i = 0; i < data.length; i++) {
                scattername[i] = data[i].name;
            }


            if (maxwidth == undefined) {
                maxwidth = '460px';
            }

            var bestname = data[0].scatter;
            var worstname = data[0].scatter1;

            if (labelenable == undefined) {
                labelenable = true;
            }

            console.log(scattername);
            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    events: {
                        load: function () {
                            $rootScope.chartloadcount = $rootScope.chartloadcount + 1;
                            console.log($rootScope.chartloadcount);
                            if ($rootScope.chartloadcount > 21) {

                                $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                            }
                            if ($rootScope.chartloadcount > 23) {
                                $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                            }
                        }
                    },
                    type: type,
                    marginLeft: margin,
                    marginRight: 25,
                    marginTop: 10
                },
                colors: color,
                title: {
                    text: '',
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
//                    max:1000,
                    categories: scattername,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0,
                    minPadding: 0.5,
                    maxPadding: 0.5,
                    title: {
                        text: xaxistitle
                    },
                    "labels": {
                        enabled: labelenable,
//                        "overflow": "justify",
                        style: {fontSize: 12.5},
                        step: 1
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
                    min: 300,
                    max: 1000,
                    gridLineWidth: 0,
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: null
                    }
                },
                plotOptions: {
                    series: {
                    },
                    scatter: {
                        dataLabels: {
                            enabled: false,
                            align: 'left',
                            x: 5,
                            y: 12,
                            marker: {
                                radius: 8,
                                fillColor: 'blue'
                            },
                            style: {
                                fontWeight: 'normal'
                            }
                        }, tooltip: {
                            pointFormat: '<b> </b><br> {point.y}'
                        }
                    },
                    column: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            y: 30,
                            align: 'center',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }
                        },
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                            crosshairs: false
                        },
                        pointWidth: weight,
                        groupPadding: spacing,
                        depth: 25,
                        colorByPoint: colorpoint


                    }
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        verticalAlign: 'top',
                        align: 'right',
                        y: -15
                    }
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChart();
                                    }
                                }]
                        }
                    }
                },
                size: {width: maxwidth},
                series: [{
                        data: data,
                        name: seriesname

                    },
                    {
                        type: 'scatter',
                        color: '#689F38',
                        data: sdata,
                        name: bestname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    },
                    {
                        type: 'scatter',
                        color: '#800000',
                        data: wdata,
                        name: worstname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    }]
            }
            ;
            return tempbar;

        }





        function creditsummaryplainbar(creditor, type, color, data, seriesname, margin, weight, spacing, pointdata, markersize, colorpoint, maxwidth, labelenable, xaxistitle) {

            var sdata = [];
            var wdata = [];
            var scattername = [];
            for (var i = 0; i < data.length; i++) {
                sdata[i] = data[i].svalue;
            }
            for (var i = 0; i < data.length; i++) {
                wdata[i] = data[i].svalue1;
            }

            for (var i = 0; i < data.length; i++) {
                scattername[i] = data[i].name;
            }


            if (maxwidth == undefined) {
                maxwidth = '460px';
            }

            var bestname = data[0].scatter;
            var worstname = data[0].scatter1;

            if (labelenable == undefined) {
                labelenable = true;
            }

            console.log(scattername);
            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    events: {
                        load: function () {
                            $rootScope.chartloadcount = $rootScope.chartloadcount + 1;
                            console.log($rootScope.chartloadcount);
                            if ($rootScope.chartloadcount > 21) {

                                $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                            }
                            if ($rootScope.chartloadcount > 23) {
                                $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                            }
                        }
                    },
                    type: type,
                    marginLeft: margin,
                    marginRight: 25,
                    marginTop: 10
                },
                colors: color,
                title: {
                    text: '',
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
//                    max:1000,
                    categories: scattername,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0,
                    minPadding: 0.5,
                    maxPadding: 0.5,
                    title: {
                        text: xaxistitle
                    },
                    "labels": {
                        enabled: labelenable,
//                        "overflow": "justify",
                        style: {fontSize: 12.5},
                        step: 1
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
                    min: 300,
                    max: 1000,
                    gridLineWidth: 0,
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: null
                    }
                },
                plotOptions: {
                    series: {
                    },
                    scatter: {
                        dataLabels: {
                            enabled: false,
                            align: 'left',
                            x: 5,
                            y: 12,
                            marker: {
                                radius: 8,
                                fillColor: 'blue'
                            },
                            style: {
                                fontWeight: 'normal'
                            }
                        }, tooltip: {
                            pointFormat: '<b> </b><br> {point.y}'
                        }
                    },
                    column: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            y: 30,
                            align: 'center',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }
                        },
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                            crosshairs: false
                        },
                        pointWidth: weight,
                        groupPadding: spacing,
                        depth: 25,
                        colorByPoint: colorpoint


                    }
                },
                credits: {
                    enabled: false
                },
//            credits: {
//                text: creditor,
//            },

                legend: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        verticalAlign: 'top',
                        align: 'right',
                        y: -15
                    }
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChart();
                                    }
                                }]
                        }
                    }
                },
                size: {width: maxwidth},
                series: [{
                        data: data,
                        name: seriesname

                    },
                    {
                        type: 'scatter',
                        color: '#689F38',
                        data: sdata,
                        name: bestname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    },
                    {
                        type: 'scatter',
                        color: '#800000',
                        data: wdata,
                        name: worstname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    }]
            }
            ;
            return tempbar;

        }



        function clicksummaryplainbar(creditor, type, color, data, seriesname, margin, weight, spacing, pointdata, markersize, colorpoint, maxwidth, labelenable, xaxistitle) {

            var sdata = [];
            var wdata = [];
            var scattername = [];
            for (var i = 0; i < data.length; i++) {
                sdata[i] = data[i].svalue;
            }
            for (var i = 0; i < data.length; i++) {
                wdata[i] = data[i].svalue1;
            }

            for (var i = 0; i < data.length; i++) {
                scattername[i] = data[i].name;
            }


            if (maxwidth == undefined) {
                maxwidth = '460px';
            }

            var bestname = data[0].scatter;
            var worstname = data[0].scatter1;

            if (labelenable == undefined) {
                labelenable = true;
            }

            console.log(scattername);
            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    type: type,
                    marginLeft: margin,
                    marginTop: 10,
                    marginRight: 25
                },
                colors: color,
                title: {
                    text: '',
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
//                    max:1000,
                    categories: scattername,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0,
                    minPadding: 0.5,
                    maxPadding: 0.5,
                    title: {
                        text: xaxistitle
                    },
                    "labels": {
                        enabled: labelenable,
//                        "overflow": "justify",
                        style: {fontSize: 12.5},
                        step: 1
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
                    min: 300,
                    max: 1000,
                    gridLineWidth: 0,
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: null
                    }
                },
                plotOptions: {
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    $rootScope.changedfactor = this.options.factorname;
                                    $rootScope.$broadcast("changefactordsnap", $rootScope.changedfactor);
                                    console.log('state2 params:', $rootScope.changedfactor);
                                    console.log('state2 params:', $rootScope.changedfactor);

                                }
                            }
                        }

                    },
                    scatter: {
                        dataLabels: {
                            enabled: false,
                            align: 'left',
                            x: 5,
                            y: 12,
                            marker: {
                                radius: 8,
                                fillColor: 'blue'
                            },
                            style: {
                                fontWeight: 'normal'
                            }
                        }, tooltip: {
                            pointFormat: '<b> </b><br> {point.y}'
                        }
                    },
                    column: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            y: 30,
                            align: 'center',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }
                        },
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                            crosshairs: false
                        },
                        pointWidth: weight,
                        groupPadding: spacing,
                        depth: 25,
                        colorByPoint: colorpoint


                    }
                },
                credits: {
                    enabled: false
                },
//            credits: {
//                text: creditor,
//            },

                legend: {
                    enabled: false
                },
//          navigation: {
//            buttonOptions: {
//                verticalAlign: 'top',
//                y: -20
//            }
//        },

                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChart();
                                    }
                                }]
                        }
                    }
                },
                size: {width: maxwidth},
                series: [{
                        data: data,
                        name: seriesname

                    },
                    {
                        type: 'scatter',
                        color: '#689F38',
                        data: sdata,
                        name: bestname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    },
                    {
                        type: 'scatter',
                        color: '#800000',
                        data: wdata,
                        name: worstname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    }]
            }
            ;
            return tempbar;

        }




        function plainbar(type, color, data, seriesname, margin, weight, spacing, pointdata, markersize, colorpoint, maxwidth, labelenable, xaxistitle) {

            var sdata = [];
            var scattername = [];
            for (var i = 0; i < data.length; i++) {
                sdata[i] = data[i].svalue;
            }

            for (var i = 0; i < data.length; i++) {
                scattername[i] = data[i].name;
            }

            if (maxwidth == undefined) {
                maxwidth = '460px';
            }

            var bestname = data[0].scatter;

            if (labelenable == undefined) {
                labelenable = true;
            }

            console.log(scattername);
            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    type: type,
                    marginLeft: margin,
                    marginTop: 10
                },
                colors: color,
                title: {
                    text: '',
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
//                    max:1000,
                    categories: scattername,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0,
                    minPadding: 0.5,
                    maxPadding: 0.5,
                    title: {
                        text: xaxistitle
                    },
                    "labels": {
                        enabled: labelenable,
//                        "overflow": "justify",
                        style: {fontSize: 12},
                        step: 1
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
//                     min:250,
                    max: 1000,
                    gridLineWidth: 0,
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: null
                    }
                },
                plotOptions: {
                    series: {
                    },
                    scatter: {
                        dataLabels: {
                            enabled: true,
                            align: 'left',
                            x: 5,
                            y: 12,
                            marker: {
                                radius: 8,
                                fillColor: 'blue'
                            },
                            style: {
                                fontWeight: 'normal'
                            }
                        }, tooltip: {
                            pointFormat: '<b> </b><br> {point.y}'
                        }
                    },
                    column: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            y: 30,
                            align: 'center',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }
                        },
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                            crosshairs: false
                        },
                        pointWidth: weight,
                        groupPadding: spacing,
                        depth: 25,
                        colorByPoint: colorpoint


                    }
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChart();
                                    }
                                }]
                        }
                    }
                },
                size: {width: maxwidth},
                series: [{
                        data: data,
                        name: seriesname

                    },
                    {
                        type: 'scatter',
                        color: '#689F38',
                        data: sdata,
                        name: bestname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    }]
            }
            ;
            return tempbar;

        }


//dsnap plainbar with click

        function dsnapplainbar(type, color, data, seriesname, margin, weight, spacing, pointdata, markersize, colorpoint, maxwidth, labelenable, xaxistitle) {

            var sdata = [];
            var scattername = [];
            for (var i = 0; i < data.length; i++) {
                sdata[i] = data[i].svalue;
            }

            for (var i = 0; i < data.length; i++) {
                scattername[i] = data[i].name;
            }

            if (maxwidth == undefined) {
                maxwidth = '460px';
            }

            var bestname = data[0].scatter;

            if (labelenable == undefined) {
                labelenable = true;
            }

            console.log(scattername);
            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    type: type,
                    marginLeft: margin,
                    marginTop: 10
                },
                colors: color,
                title: {
                    text: '',
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
//                    max:1000,
                    categories: scattername,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0,
                    minPadding: 0.5,
                    maxPadding: 0.5,
                    title: {
                        text: xaxistitle
                    },
                    "labels": {
                        enabled: labelenable,
//                        "overflow": "justify",
                        style: {fontSize: 12},
                        step: 1
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
//                     min:250,
                    max: 1000,
                    gridLineWidth: 0,
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: null
                    }
                },
                plotOptions: {
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    $rootScope.factorClick = this.options.name;
                                    $rootScope.$broadcast("factorClick", $rootScope.factorClick);
                                    console.log('state2 params:', $rootScope.factorClick);

                                }
                            }
                        }
                    },
                    scatter: {
                        dataLabels: {
                            enabled: true,
                            align: 'left',
                            x: 5,
                            y: 12,
                            marker: {
                                radius: 8,
                                fillColor: 'blue'
                            },
                            style: {
                                fontWeight: 'normal'
                            }
                        }, tooltip: {
                            pointFormat: '<b> </b><br> {point.y}'
                        }
                    },
                    column: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            y: 30,
                            align: 'center',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }
                        },
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    $rootScope.factorClick = this.options.name;
                                    $rootScope.$broadcast("factorClick", $rootScope.factorClick);
                                    console.log('state2 params:', $rootScope.factorClick);
                                }
                            }
                        },
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                            crosshairs: false
                        },
                        pointWidth: weight,
                        groupPadding: spacing,
                        depth: 25,
                        colorByPoint: colorpoint


                    }
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
//        exporting: {
//        csv: {
//            dateFormat: '%Y-%m-%d'
//        }
//    },

                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChart();
                                    }
                                }]
                        }
                    }
                },
                size: {width: maxwidth},
                series: [{
                        data: data,
                        name: seriesname

                    },
                    {
                        type: 'scatter',
                        color: '#689F38',
                        data: sdata,
                        name: bestname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    }]
            }
            ;
            return tempbar;

        }


//for Regional Plainbar


        function regplainbar(type, color, data, seriesname, margin, weight, spacing, pointdata, markersize, colorpoint, labelenable, xaxistitle) {

            var sdata = [];
            var wdata = [];
            var scattername = [];
            for (var i = 0; i < data.length; i++) {
                sdata[i] = data[i].svalue;
            }
            for (var i = 0; i < data.length; i++) {
                wdata[i] = data[i].svalue1;
            }

            for (var i = 0; i < data.length; i++) {
                scattername[i] = data[i].name;
            }

            var bestname = data[0].scatter;
            var worstname = data[0].scatter1;

            if (labelenable == undefined) {
                labelenable = true;
            }

            console.log(scattername);
            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    events: {
                        load: function () {
                            $rootScope.chartloadcount = $rootScope.chartloadcount + 1;
                            console.log($rootScope.chartloadcount);
                            if ($rootScope.chartloadcount > 21) {

                                $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                            }
                            if ($rootScope.chartloadcount > 23) {
                                $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                            }
                        }
                    },
                    type: type,
                    marginLeft: margin
                },
                colors: color,
                title: {
                    text: '',
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
//                    max:1000,
                    categories: scattername,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0,
                    minPadding: 0.5,
                    maxPadding: 0.5,
                    title: {
                        text: xaxistitle
                    },
                    "labels": {
                        enabled: labelenable,
//                        "overflow": "justify",
                        style: {fontSize: 13},
                        step: 1
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
                    min: 300,
                    max: 1000,
                    gridLineWidth: 0,
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: null
                    }
                },
                plotOptions: {
                    series: {
                        pointWidth: weight,
                        groupPadding: spacing,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    var str = this.options.regionname;
                                    str = str.replace("*", '');
                                    str = str.replace("*", '');
                                    $rootScope.changedmodel = str;
                                    $rootScope.$broadcast("changeregionone", $rootScope.changedmodel);
                                    console.log('state2 params:', $rootScope.changedmodel);
                                    console.log('state2 params:', $rootScope.changedmodel);

                                }
                            }
                        }
                    },
                    scatter: {
                        dataLabels: {
                            enabled: false,
                            align: 'left',
                            x: 5,
                            y: 12,
                            marker: {
                                radius: 8,
                                fillColor: 'blue'
                            },
                            style: {
                                fontWeight: 'normal'
                            }
                        }, tooltip: {
                            pointFormat: '<b> </b><br> {point.y}'
                        }
                    },
                    column: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            y: 30,
                            align: 'center',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }
                        },
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                            crosshairs: false
                        },
                        pointWidth: weight,
                        groupPadding: spacing,
                        depth: 25,
                        colorByPoint: colorpoint


                    }
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChart();
                                    }
                                }]
                        }
                    }
                },
                series: [
                    {
                        data: data,
                        name: seriesname
                    },
                    {
                        type: 'scatter',
                        color: '#689F38',
                        data: sdata,
                        name: bestname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    },
                    {
                        type: 'scatter',
                        color: '#800000',
                        data: wdata,
                        name: worstname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    }]
            }
            ;
            return tempbar;

        }



//plainbar bar

        function barplainbar(type, color, data, seriesname, margin, weight, spacing, pointdata, markersize, colorpoint, suffix, labelenable, xaxistitle) {
            console.log(suffix);
            var sdata = [];
            var wdata = [];
            var scattername = [];
            for (var i = 0; i < data.length; i++) {
                sdata[i] = data[i].svalue;
            }
            for (var i = 0; i < data.length; i++) {
                wdata[i] = data[i].svalue1;
            }

            for (var i = 0; i < data.length; i++) {
                scattername[i] = data[i].name;
            }

            if (labelenable == undefined) {
                labelenable = true;
            }

            var bestname = data[0].scatter;
            var worstname = data[0].scatter1;

            console.log(scattername);
            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    events: {
                        load: function () {
                            $rootScope.chartloadcount = $rootScope.chartloadcount + 1;
                            console.log($rootScope.chartloadcount);
                            if ($rootScope.chartloadcount > 21) {

                                $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                            }
                            if ($rootScope.chartloadcount > 23) {
                                $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                            }
                        }
                    },
                    type: type,
                    marginLeft: margin
                },
                colors: color,
                title: {
                    text: '',
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
//                    max:1000,
                    categories: scattername,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0,
                    minPadding: 0.5,
                    maxPadding: 0.5,
                    title: {
                        text: xaxistitle
                    },
                    "labels": {
                        enabled: labelenable,
                        "overflow": "justify",
                        layout: 'vertical',
                        style: {fontSize: 11},
                        step: 1
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
                    gridLineWidth: 0,
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: null
                    }
                },
                plotOptions: {
                    series: {
                    },
                    scatter: {
                        dataLabels: {
                            enabled: false,
                            align: 'left',
                            x: 5,
                            y: 12,
                            marker: {
                                radius: 8,
                                fillColor: 'blue'
                            },
                            style: {
                                fontWeight: 'normal'
                            }
                        }, tooltip: {
                            pointFormat: '<b> </b><br> {point.y} ' + suffix
                        }
                    },
                    bar: {
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s + suffix;
                            }

                        },
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            inside: true,
                            align: 'right',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }

                        }
                    },
                    column: {
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s + suffix;
                            }

                        },
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            y: 30,
                            align: 'center',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }
                        },
                        pointWidth: weight,
                        groupPadding: spacing,
                        depth: 25,
                        colorByPoint: colorpoint


                    }
                },
                tooltip: {
                    formatter: function () {
                        var s = this.point.options.tooltip;
                        return s + suffix;
                    },
                    crosshairs: false
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChart();
                                    }
                                }]
                        }
                    }
                },
                series: [{
                        data: data,
                        name: seriesname

                    },
                    {
                        type: 'scatter',
                        color: '#689F38',
                        data: sdata,
                        name: bestname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    },
                    {
                        type: 'scatter',
                        color: '#800000',
                        data: wdata,
                        name: worstname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    }]
            }
            ;
            return tempbar;

        }

//barplain bar split

        function barplainbarsplit(type, color, data, seriesname, margin, weight, spacing, pointdata, markersize, colorpoint, suffix, labelenable, xaxistitle) {
            console.log(suffix);
            var sdata = [];
            var wdata = [];
            var scattername = [];
            for (var i = 0; i < data.length; i++) {
                sdata[i] = data[i].svalue;
            }
            for (var i = 0; i < data.length; i++) {
                wdata[i] = data[i].svalue1;
            }

            for (var i = 0; i < data.length; i++) {
                scattername[i] = data[i].name;
            }

            if (labelenable == undefined) {
                labelenable = true;
            }

            var bestname = data[0].scatter;
            var worstname = data[0].scatter1;

            console.log(scattername);
            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    type: type,
                    marginLeft: margin,
                    marginRight: 25
                },
                colors: color,
                title: {
                    text: '',
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
//                    max:1000,
                    categories: scattername,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0,
                    minPadding: 0.5,
                    maxPadding: 0.5,
                    title: {
                        text: xaxistitle
                    },
                    "labels": {
                        enabled: labelenable,
//                        "overflow": "justify",
                        style: {fontSize: "10px",
                            textOverflow: 'none'
//                            paddingBottom: "1px"
                        },
                        step: 1
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
                    gridLineWidth: 0,
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: null
                    }
                },
                plotOptions: {
                    series: {
                    },
                    scatter: {
                        dataLabels: {
                            enabled: false,
                            align: 'left',
                            x: 5,
                            y: 12,
                            marker: {
                                radius: 8,
                                fillColor: 'blue'
                            },
                            style: {
                                fontWeight: 'normal'
                            }
                        }, tooltip: {
                            pointFormat: '<b> </b><br> {point.y} ' + suffix
                        }
                    },
                    bar: {
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s + suffix;
                            }

                        },
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            inside: true,
                            align: 'right',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }

                        }
                    },
                    column: {
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s + suffix;
                            }

                        },
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            y: 30,
                            align: 'center',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }
                        },
                        pointWidth: weight,
                        groupPadding: spacing,
                        depth: 25,
                        colorByPoint: colorpoint


                    }
                },
                tooltip: {
                    formatter: function () {
                        var s = this.point.options.tooltip;
                        return s + suffix;
                    },
                    crosshairs: false
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChart();
                                    }
                                }]
                        }
                    }
                },
                series: [{
                        data: data,
                        name: seriesname

                    },
                    {
                        type: 'scatter',
                        color: '#689F38',
                        data: sdata,
                        name: bestname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    },
                    {
                        type: 'scatter',
                        color: '#800000',
                        data: wdata,
                        name: worstname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    }]
            }
            ;
            return tempbar;

        }



        function sopplainbar(type, fontsize, color, data, seriesname, margin, weight, spacing, pointdata, markersize, colorpoint, suffix) {
            var category = [];
            for (var i = 0; i < data.length; i++) {
                category[i] = data[i].name;
            }
            console.log(data);
            var bdata = [];
            var sdata = [];
            var scatteravg = data[0].scatteravg;
            var scatterbest = data[0].scatterbest;
            var selectionName = data[0].mainSelectionName;
            for (var i = 0; i < data.length; i++) {
                bdata[i] = data[i].bvalue;
                sdata[i] = data[i].svalue;

            }
            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    events: {
                        load: function () {
                            $rootScope.chartloadcount = $rootScope.chartloadcount + 1
                            console.log($rootScope.chartloadcount)
                            if ($rootScope.chartloadcount > 21) {

                                $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                            }

                            if ($rootScope.chartloadcount > 23) {
                                $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                            }
                        }
                    },
                    type: type,
                    marginLeft: margin,
                    marginTop: 20
                },
                colors: color,
                title: {
                    text: null
                },
                xAxis: {
                    categories: category,
//                    lineColor: 'transparent',
//                    tickLength: 0,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0,
                    minPadding: 0.5,
                    maxPadding: 0.5,
                    title: {
                        text: null
                    },
                    "labels": {
                        "overflow": "justify",
                        style: {
                            //   font:'14px bold Vardana'
                            color: '#556977',
                            fontSize: fontsize,
                        },
                        step: 1
                    },
                    legend: {
                        enabled: false
                    },
                },
                yAxis: {
                    gridLineWidth: 0,
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: null
                    },
                },
                plotOptions: {
                    series: {
                        pointWidth: weight,
                    },
                    scatter: {
                        dataLabels: {
                            enabled: false,
                            align: 'left',
                            x: 5,
                            y: 12,
                            format: '{y}%',
                            marker: {
                                radius: 8,
                                fillColor: 'blue',
                            },
                            style: {
                                fontWeight: 'normal'
                            }
                        }, tooltip: {
                            pointFormat: '<b> </b><br> {point.y}%'
                        }
                    },
                    bar: {
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                        },
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            inside: true,
                            align: 'right',
                            tooltip: {
                                formatter: function () {
                                    var s =
                                            this.point.options.bartitle;
                                    return;
                                },
                            },
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '10px',
                            }

                        },
                    },
                    column: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#cdcdcd',
                            inside: true
                        },
                        groupPadding: spacing,
                        colorByPoint: colorpoint

                    }
                },
                tooltip: {
                    formatter: function () {
                        var s = this.point.options.tooltip;
                        return s + suffix;
                    },
                    crosshairs: false
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: true,
                    itemStyle: {
                        fontSize: '10px',
                        //color: '#A0A0A0'
                    },
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'

                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChart();
                                    }
                                }]
                        }
                    }
                },
                series: [{
                        data: data,
                        name: selectionName

                    },
                    {
                        type: 'scatter',
                        color: '#689F38',
                        data: bdata,
                        name: scatterbest,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    },
                    {
                        type: 'scatter',
                        color: '#9D78DD',
                        data: sdata,
                        name: scatteravg,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    }]
            }
            ;
            return tempbar;

        }



//bar and line graph
        function barline(color, data, namer, suffix) {
            var bestdata = [];
            var linedata = [];
            var bestname = data[0].bestname;
            var avgname = data[0].linename;
            var mainSelection = data[0].mainSelectionName;
            var categories = [];
            for (var i = 0; i < data.length; i++) {
                categories[i] = data[i].name;
            }
            for (var i = 0; i < data.length; i++) {
                bestdata[i] = data[i].bestvalue;
                linedata[i] = data[i].linescore;

            }

            var tempbline = {
                lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    events: {
                        load: function () {

                            $rootScope.chartloadcount = $rootScope.chartloadcount + 1;
                            console.log($rootScope.chartloadcount);
                            if ($rootScope.chartloadcount > 21) {

                                $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                            }

                            if ($rootScope.chartloadcount > 23) {
                                $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                            }
                        }
                    },
                    zoomType: 'xy',
                    marginTop: 20
                },
                colors: color,
                title: {
                    text: null
                },
                xAxis: [{
                        categories: categories,
                        gridLineWidth: 0,
                        lineColor: 'transparent',
                        tickLength: 0,
                        minorGridLineWidth: 0,
                        labels: {
                            enabled: true
                        }
                    }],
                yAxis: [{// Primary yAxis

                        gridLineWidth: 0,
                        lineColor: 'transparent',
                        tickLength: 0,
                        minorGridLineWidth: 0,
                        title: {
                            text: null

                        },
                        labels: {
                            enabled: false
                        }
                    }, {// Secondary yAxis
                        title: {
                            text: null

                        },
                        labels: {
                            enabled: false
                        },
                        gridLineWidth: 0,
                        minorGridLineWidth: 0,
                        opposite: true
                    }],
                gridLineWidth: 0,
                minorGridLineWidth: 0,
//                tooltip: {
//                    shared: false
//                },
                tooltip: {
                    formatter: function () {
                        var s = this.point.options.tooltip;
                        return s + suffix;
                    },
                    crosshairs: false
                },
                legend: {
                    enabled: true,
                    itemStyle: {
                        fontSize: '11.5px'
                                //color: '#A0A0A0'
                    },
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'

                },
                plotOptions: {
                    series: {
                        maxPointWidth: 50
//                 cursor: 'pointer',
//                        point: {
//                            events: {
//                                click: function () {
//                                    alert('Category: ' + this.category + ', value: ' + this.y);
//                                }
//                            }
//                        }
                    },
                    scatter: {
                        dataLabels: {
                            enabled: false,
                            align: 'left',
                            allowOverlap: true,
                            x: 5,
                            y: 12,
                            format: '{y}%',
                            marker: {
                                radius: 8,
                                fillColor: 'blue'
                            },
                            style: {
                                fontWeight: 'normal'
                            }
                        }, tooltip: {
                            pointFormat: '<b> </b><br> {point.y}%'
                        }
                    },
                    spline: {
                        dataLabels: {
                            enabled: false,
                            align: 'left',
                            x: 5,
                            y: 12,
                            marker: {
                                radius: 8,
                                fillColor: 'blue'
                            },
                            style: {
                                fontWeight: 'normal'
                            }
                        }, tooltip: {
                            pointFormat: '<b> </b><br> {point.y}%'
                        }
                    },
                    column: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            y: 30,
                            align: 'center',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }
                        },
                        depth: 25
//                colorByPoint: colorpoint
                    }
                },
                credits: {
                    enabled: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChart();
                                    }
                                }]
                        }
                    }
                },
                series:
                        [{type: 'column',
                                data: data,
                                name: mainSelection

                            },
//                 {
//                  type: 'scatter',
//                  color:'#689F38',
//                  data: bestdata,
//                  name:bestname,
//                  
//                  marker: {
//                     symbol: 'diamond',
//                     radius : 5
//                  }
//                },
                            {
                                type: 'spline',
                                data: linedata,
                                name: avgname,
                                color: '#7E57C2',
                                marker: {
                                    symbol: 'diamond',
                                    radius: 5
                                }
                            }]

            };


            return tempbline;
        }





//vertical line
        function verticalline(color, inverted, data, halign, valign, labelflag, ymin, ymax, yline, plotlineName) {
            var categories = [];
            for (var i = 0; i < data[0].data.length; i++) {
                categories[i] = data[0].data[i].name;
            }

//       if(data[1].data[0].y==null){
//           
//       }
//       console.log()
            var tempvline = {
                lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    events: {
                        load: function () {
                            $rootScope.chartloadcount = $rootScope.chartloadcount + 1;
                            console.log($rootScope.chartloadcount);

                            if ($rootScope.chartloadcount > 21) {

                                $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                            }
                            if ($rootScope.chartloadcount > 23) {
                                $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                            }
                        }
                    },
                    type: 'spline',
                    inverted: inverted

                },
                colors: color,
                title: {
                    text: null
                },
                credits: {
                    enabled: false
                },
//                
                legend: {
                    itemStyle: {
                        fontSize: '12px'
                    },
                    enabled: true,
                    layout: 'horizontal',
                    align: 'left',
                    verticalAlign: 'top',
                    y: 5
                },
                xAxis: {
                    categories: categories,
                    gridLineWidth: 0,
                    lineColor: '#c5c5c5',
                    tickLength: 0,
                    minorGridLineWidth: 0,
                    "labels": {
                        "overflow": "justified",
                        layout: 'vertical',
                        style: {fontSize: 11, textOverflow: 'none'},
                        step: 1
                    },
                },
                yAxis: {
                    gridLineWidth: 0,
                    minorGridLineWidth: 0,
                    lineWidth: yline,
                    tickWidth: yline,
                    min: ymin,
                    max: ymax,
                    plotLines: [{
                            color: '#4B0082',
                            width: 2,
                            value: 0,
                            label: {
                                text: plotlineName,
//                                y: -10
                            }
                        }],
                    title: {
                        text: null
                    },
                    labels: {
                        enabled: labelflag,
                        style: {
                            fontSize: '12px'
                        },
                        formatter: function () {
                            return this.value + '%';
                        }
                    }
                },
                tooltip: {
                    formatter: function () {
                        var s =
                                this.point.options.tooltip + "%";
                        return s;
                    },
                    crosshairs: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
//                                textKey: 'downloadXLS',
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
//                                textKey: 'downloadXLS',
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
//                                     textKey: 'downloadXLS',
                                    onclick: function () {
                                        this.exportChart();
                                    }
                                }]
                        }
                    }
                },
                series: data

            };
            return tempvline;
        }




//vertical line
        function horizontalline(response, color) {
            var categories = [];
            for (var i = 0; i < response[0].data.length; i++) {
                categories[i] = response[0].data[i].name;
            }
            var line =
                    {lang: {
                            downloadPNG: $rootScope.downloadpng,
                            downloadCSV: $rootScope.downloadcsv,
                            downloadXLS: $rootScope.downloadxls,
                            contextButtonTitle: $rootScope.downloadheader
                        },
                        chart: {
                            events: {
                                load: function () {
                                    $rootScope.chartloadcount = $rootScope.chartloadcount + 1;
                                    console.log($rootScope.chartloadcount)
                                    if ($rootScope.chartloadcount > 21) {

                                        $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                                    }

                                    if ($rootScope.chartloadcount > 23) {
                                        $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                                    }
                                }
                            },
                            type: 'spline',
//                animation: Highcharts.svg, // don't animate in old IE

                            marginRight: 25, backgroundColor: '#FFFFFF'
                        },
                        colors: ['#FFC000', '#808080', '#A9D08E', '#305496', '#ff4100', '#002060', '#92D050', '#000000'],
                        title: {
                            text: ''
                        },
                        xAxis: {
                            categories: categories,
                            gridLineWidth: 0,
//                    lineColor: 'transparent',
                            tickLength: 0,
                            minorGridLineWidth: 0,
                            labels: {
                                style: {fontSize: '1.6vmin',
                                    lineHeight: 25
                                }
                            }
                        },
                        yAxis: {
                            min: 8,
                            max: 10,
                            gridLineWidth: 1,
                            lineColor: '#eeeeee',
                            lineWidth: 1,
                            tickLength: 1,
                            minorGridLineWidth: 1,
                            title: {
                                text: ''
                            },
                            labels: {enabled: true,
                                overflow: 'justify',
                                format: '{value:.2f}'
                            }
                        },
                        tooltip: {
                            style: {fontSize: '2vmin'},
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                            crosshairs: false
                        },
                        legend: {
                            enabled: true,
                            itemStyle: {
                                fontSize: '10px'
                                        //color: '#A0A0A0'
                            },
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'

                        },
                        exporting: {
                            formAttributes: {
                                target: '_blank'
                            },
                            buttons: {
                                contextButton: {
                                    menuItems: [{
                                            text: $rootScope.downloadxls,
                                            onclick: function () {
                                                this.downloadXLS();
                                            }
                                        }, {
                                            text: $rootScope.downloadcsv,
                                            onclick: function () {
                                                this.downloadCSV();
                                            }
                                        }, {
                                            text: $rootScope.downloadpng,
                                            onclick: function () {
                                                this.exportChart();
                                            }
                                        }]
                                }
                            }
                        },
                        series: response,
                        "credits": {
                            "enabled": false
                        }
                        ,
                        navigation: {
                            buttonOptions: {
                                verticalAlign: 'top',
                                align: 'right',
                                y: -15
                            }
                        }
                    };
            return line;
        }



        function dsmultigraph(response, colorbypoint, color) {

            var categories = [];
            console.log(response);
            console.log(response.name);
            console.log(response.data);
            console.log(response[0].name);
            console.log(response[0].data);
//           console.log(response.data);
//           console.log(response[0].name);
//           console.log(response[0].data);
            for (var i = 0; i < response[0].data.length; i++) {
                categories[i] = response[0].data[i].name;

            }

            var multi_return =
                    {lang: {
                            downloadPNG: $rootScope.downloadpng,
                            downloadCSV: $rootScope.downloadcsv,
                            downloadXLS: $rootScope.downloadxls,
                            contextButtonTitle: $rootScope.downloadheader
                        },
                        "chart": {
                            "type": "column"
                        },
                        colors: color,
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                            crosshairs: false
                        },
                        plotOptions: {
                            series: {
                                borderWidth: 0,
                                maxPointWidth: 40,
//                             cursor: 'pointer',
//                            point: {
//                                events: {
//                                    click: function () {
//                                        alert('Category: ' + this.category + ', value: ' + this.y);
//                                    }
//                                }
//                            },

                                dataLabels: {
                                    enabled: true,
                                    formatter: function () {
                                        var pcnt = this.point.options.bartitle;
                                        return pcnt;
                                    },
                                    color: '#ffffff',
                                    y: 30,
                                    align: 'right',
                                    style: {
                                        //color: '#FFFFFF',
                                        textShadow: 'none',
                                        fontWeight: 'light',
                                        fontSize: '11px'
                                    }
                                }
                            }
                        },
                        "series": response[0].data,
                        "title": {
                            "text": null
                        },
                        "credits": {
                            "enabled": false
                        },
                        "loading": false,
                        "size": {},
                        "xAxis": {
                            gridLineWidth: 0,
                            categories: categories,
                            currentMin: 0,
//                        lineColor: 'transparent',
//                        tickLength: 0,
                            minorGridLineWidth: 0,
                            labels: {
                                style: {
                                    fontSize: '10px'
                                }
                            }

                        },
                        "yAxis": {
                            title: {
                                text: null,
                                align: 'high'
                            },
                            labels: {
                                enabled: false
                            },
                            gridLineWidth: 0,
                            allowDecimals: true,
                            min: 0

                        },
                        exporting: {
                            formAttributes: {
                                target: '_blank'
                            },
                            buttons: {
                                contextButton: {
                                    menuItems: [{
                                            text: $rootScope.downloadxls,
                                            onclick: function () {
                                                this.downloadXLS();
                                            }
                                        }, {
                                            text: $rootScope.downloadcsv,
                                            onclick: function () {
                                                this.downloadCSV();
                                            }
                                        }, {
                                            text: $rootScope.downloadpng,
                                            onclick: function () {
                                                this.exportChart();
                                            }
                                        }]
                                }
                            }
                        },
                        legend: {
                            enabled: true

                        }
                    };

            return multi_return;

        }

//" City Level retention for Total Sales", null, response, 250, 1150,false
//the multigraph
//function multigraph(response,colorpoint,color) {
        function multigraph(response, colorbypoint, color) {

            var categories = [];
            console.log(response);
//           console.log(response.data);
//           console.log(response[0].name);
//           console.log(response[0].data);
            for (var i = 0; i < response.data[0].data.length; i++) {
                categories[i] = response.data[0].data[i].name;
//
            }

            var multi_return =
                    {lang: {
                            downloadPNG: $rootScope.downloadpng,
                            downloadCSV: $rootScope.downloadcsv,
                            downloadXLS: $rootScope.downloadxls,
                            contextButtonTitle: $rootScope.downloadheader
                        },
                        "chart": {
                            "type": "column"
                        },
                        colors: color,
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                            crosshairs: false
                        },
                        plotOptions: {
                            series: {
                                borderWidth: 0,
                                maxPointWidth: 40,
//                             cursor: 'pointer',
//                            point: {
//                                events: {
//                                    click: function () {
//                                        alert('Category: ' + this.category + ', value: ' + this.y);
//                                    }
//                                }
//                            },

                                dataLabels: {
                                    enabled: true,
                                    formatter: function () {
                                        var pcnt = this.point.options.bartitle;
                                        return pcnt;
                                    },
                                    color: '#ffffff',
                                    y: 30,
                                    align: 'right',
                                    style: {
                                        //color: '#FFFFFF',
                                        textShadow: 'none',
                                        fontWeight: 'light',
                                        fontSize: '11px'
                                    }
                                }
                            }
                        },
                        "series": response.data,
                        "title": {
                            "text": null
                        },
                        "credits": {
                            "enabled": false
                        },
                        "loading": false,
                        "size": {},
                        "xAxis": {
                            gridLineWidth: 0,
                            categories: categories,
                            currentMin: 0,
//                        lineColor: 'transparent',
//                        tickLength: 0,
                            minorGridLineWidth: 0,
                            labels: {
                                style: {
                                    fontSize: '10px'
                                }
                            }

                        },
                        "yAxis": {
                            title: {
                                text: null,
                                align: 'high'
                            },
                            labels: {
                                enabled: false
                            },
                            gridLineWidth: 0,
                            allowDecimals: true,
                            min: 0

                        },
                        exporting: {
                            formAttributes: {
                                target: '_blank'
                            },
                            buttons: {
                                contextButton: {
                                    menuItems: [{
                                            text: $rootScope.downloadxls,
                                            onclick: function () {
                                                this.downloadXLS();
                                            }
                                        }, {
                                            text: $rootScope.downloadcsv,
                                            onclick: function () {
                                                this.downloadCSV();
                                            }
                                        }, {
                                            text: $rootScope.downloadpng,
                                            onclick: function () {
                                                this.exportChart();
                                            }
                                        }]
                                }
                            }
                        },
                        legend: {
                            enabled: true

                        }
                    };

            return multi_return;

        }


        function stackmultigraph(response, color, pointwidth, fontsize, grouppadding) {
            var category = [];
            for (var i = 0; i < response[0].data.length; i++) {
                console.log("inisde here is the fucniton");
                category[i] = response[0].data[i].name;
            }
            var multi_return =
                    {lang: {
                            downloadPNG: $rootScope.downloadpng,
                            downloadCSV: $rootScope.downloadcsv,
                            downloadXLS: $rootScope.downloadxls,
                            contextButtonTitle: $rootScope.downloadheader
                        },
                        "chart": {
                            "type": "column"
                        },
                        colors: color,
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                            crosshairs: false
                        },
                        plotOptions: {
                            series: {
                                pointWidth: pointwidth,
                                groupPadding: grouppadding,
                                minPointLength: 20,
                                dataLabels: {
                                    enabled: true,
                                    allowOverlap: true,
                                    formatter: function () {
                                        var pcnt = this.point.options.bartitle;
                                        return pcnt;
                                    },
                                    inside: true,
                                    color: '#ffffff',
                                    y: -5,
                                    verticalAlign: 'top',
//                                            align: 'right',
                                    style: {
                                        textShadow: 'none',
                                        fontSize: fontsize
                                    }
                                }
                            }
                        },
                        "series": response,
                        "title": {
                            "text": null
                        },
                        "credits": {
                            "enabled": false
                        },
                        "loading": false,
                        "size": {},
                        "xAxis": {
                            gridLineWidth: 0,
                            categories: category,
                            currentMin: 0,
                            tickLength: 0,
                            lineColor: 'transparent',
                            minorGridLineWidth: 0,
                            labels: {
                                style: {
                                    //   font:'14px bold Vardana'
                                    color: '#556977',
                                    fontSize: '11px'
                                            // fontFamily: 'Roboto',
                                            //  fontWeight: 'bold'
                                }
                            }

                        },
                        "yAxis": {
                            title: {
                                text: null,
                                align: 'high'
                            },
                            labels: {
                                enabled: false
                            },
                            gridLineWidth: 0,
                            allowDecimals: true,
                            min: 0

                        },
                        exporting: {
                            formAttributes: {
                                target: '_blank'
                            },
                            buttons: {
                                contextButton: {
                                    menuItems: [{
                                            text: $rootScope.downloadxls,
                                            onclick: function () {
                                                this.downloadXLS();
                                            }
                                        }, {
                                            text: $rootScope.downloadcsv,
                                            onclick: function () {
                                                this.downloadCSV();
                                            }
                                        }, {
                                            text: $rootScope.downloadpng,
                                            onclick: function () {
                                                this.exportChart();
                                            }
                                        }]
                                }
                            }
                        },
                        legend: {
                            enabled: true,
                            itemStyle: {
                                fontSize: '10px'
                                        //color: '#A0A0A0'
                            },
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'

                        }
                    };

            return multi_return;

        }



        function stackedgraph(data, color, marginBottom) {

            var category = [];
            for (var i = 0; i < data[0].data.length; i++) {
                console.log("inisde here is the fucniton");
                category[i] = data[0].data[i].name;
            }


            var stack =
                    {lang: {
                            downloadPNG: $rootScope.downloadpng,
                            downloadCSV: $rootScope.downloadcsv,
                            downloadXLS: $rootScope.downloadxls,
                            contextButtonTitle: $rootScope.downloadheader
                        },
                        chart: {events: {
                                load: function () {
                                    $rootScope.chartloadcount = $rootScope.chartloadcount + 1;
                                    console.log($rootScope.chartloadcount);
                                    if ($rootScope.chartloadcount > 21) {

                                        $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                                    }

                                    if ($rootScope.chartloadcount > 23) {
                                        $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                                    }
                                }
                            },
                            type: 'column',
                            marginRight: 25,
                            marginBottom: marginBottom

                        },
                        title: {
                            text: null
                        },
                        colors: color,
                        xAxis: {
                            categories: category,
                            gridLineWidth: 0,
                            lineColor: 'transparent',
                            tickLength: 0,
                            minorGridLineWidth: 0,
                            labels: {
                                "overflow": "justify",
                                style: {fontSize: "11px"},
                                step: 1
                            }
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: null
                            },
                            gridLineWidth: 0,
                            minorGridLineWidth: 0,
                            gridLineColor: 'transparent',
                            labels: {enabled: false,
                                overflow: 'justify'
                            }
                        },
                        legend: {
                            enabled: true,
//            floating: true,
                            itemStyle: {
                                fontSize: '10px'

                            },
                            layout: 'horizontal',
                            verticalAlign: 'bottom',
                            align: 'center',
                            y: 10
                        },
                        plotOptions: {
                            series: {
                                dataLabels: {
                                    enabled: true,
                                    allowOverlap: true,
                                    formatter: function () {
                                        var pcnt = this.point.options.bartitle;
                                        return pcnt;
                                    },
                                    style: {
                                        textShadow: 'none',
                                        fontWeight: 'light',
                                        fontSize: '11px'
                                    },
                                    color: '#FFFFFF',
                                    inside: true

                                },
                                stacking: 'percent'
                            }

                        },
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                            crosshairs: false
                        },
                        exporting: {
                            formAttributes: {
                                target: '_blank'
                            },
                            buttons: {
                                contextButton: {
                                    menuItems: [{
                                            text: $rootScope.downloadxls,
                                            onclick: function () {
                                                this.downloadXLS();
                                            }
                                        }, {
                                            text: $rootScope.downloadcsv,
                                            onclick: function () {
                                                this.downloadCSV();
                                            }
                                        }, {
                                            text: $rootScope.downloadpng,
                                            onclick: function () {
                                                this.exportChart();
                                            }
                                        }]
                                }
                            }
                        },
                        series: data,
                        "credits": {
                            "enabled": false
                        },
                        navigation: {
                            buttonOptions: {
                                verticalAlign: 'top',
                                align: 'right',
                                y: -15
                            }
                        }


                    };
            return stack;
        }




        function dlrstackedgraph(data, color, marginBottom) {

            var category = [];
            for (var i = 0; i < data[0].data[i].length; i++) {
                category[i] = data[0].data[i].name;
            }


            var stack =
                    {lang: {
                            downloadPNG: $rootScope.downloadpng,
                            downloadCSV: $rootScope.downloadcsv,
                            downloadXLS: $rootScope.downloadxls,
                            contextButtonTitle: $rootScope.downloadheader
                        },
                        chart: {
                            type: 'column',
                            marginRight: 25,
                            marginBottom: marginBottom

                        },
                        title: {
                            text: null
                        },
                        colors: color,
                        xAxis: {
                            categories: category,
                            gridLineWidth: 0,
                            lineColor: 'transparent',
                            tickLength: 0,
                            minorGridLineWidth: 0,
                            labels: {
                                "overflow": "justify",
                                style: {fontSize: "120px"},
                                step: 1
                            }
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: null
                            },
                            gridLineWidth: 0,
                            minorGridLineWidth: 0,
                            gridLineColor: 'transparent',
                            labels: {enabled: false,
                                overflow: 'justify'
                            }
                        },
                        legend: {
                            enabled: true,
//            floating: true,
                            itemStyle: {
                                fontSize: '10px'

                            },
                            layout: 'horizontal',
                            verticalAlign: 'bottom',
                            align: 'center',
                            y: 10
                        },
                        plotOptions: {
                            series: {
                                dataLabels: {
                                    enabled: true,
                                    allowOverlap: true,
                                    formatter: function () {
                                        var pcnt = this.point.options.bartitle;
                                        return pcnt;
                                    },
                                    style: {
                                        textShadow: 'none',
                                        fontWeight: 'light',
                                        fontSize: '11px'
                                    },
                                    color: '#FFFFFF',
                                    inside: true

                                },
                                stacking: 'percent'
                            }






                        },
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                            crosshairs: false
                        },
                        exporting: {
                            formAttributes: {
                                target: '_blank'
                            },
                            buttons: {
                                contextButton: {
                                    menuItems: [{
                                            text: $rootScope.downloadxls,
                                            onclick: function () {
                                                this.downloadXLS();
                                            }
                                        }, {
                                            text: $rootScope.downloadcsv,
                                            onclick: function () {
                                                this.downloadCSV();
                                            }
                                        }, {
                                            text: $rootScope.downloadpng,
                                            onclick: function () {
                                                this.exportChart();
                                            }
                                        }]
                                }
                            }
                        },
                        series: data,
                        "credits": {
                            "enabled": false
                        },
                        navigation: {
                            buttonOptions: {
                                verticalAlign: 'top',
                                align: 'right',
                                y: -15
                            }
                        }
                    };
            return stack;
        }



    }
})();




