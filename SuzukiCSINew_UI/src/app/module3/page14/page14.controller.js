(function () {
    'use strict';

    angular
            .module('app.module3.page14')
            .controller('m3page14Controller', m3page14Controller);

    /* @ngInject */
    function m3page14Controller($scope, service, $state, $http, $rootScope) {

        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776", "#92D400", "#00A1DE", "#72C7E7", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.hideyear = false;
        $rootScope.hidebiannual = false;
        $rootScope.hidemonth = false;
        $rootScope.hideregion = true;
        $rootScope.hidearea = true;
        $rootScope.hidezone = true;
        $rootScope.snaphidezone = true;
        $rootScope.snaphideregion = true;
        $rootScope.hideprovince = true;
        $rootScope.loyaltydealerhide = true;
        $rootScope.dealerpriorityhide = true;
        $rootScope.mainselectionhide = true;
        $rootScope.filterSelShow = true;
        $rootScope.hidecity = true;
        $rootScope.hidemodel = true;
        $rootScope.hidefactor = true;
        $rootScope.dealerhide = true;
        $rootScope.pagenumber = '11';
        $rootScope.starlabelhide = true;
        $rootScope.priorityhide = true;
        $rootScope.summaryRefresh = 1;
        $rootScope.dsnapshot = "not";
        $rootScope.dealerScoreDealer = "null";
        $scope.Report_Month1 = "Report<br>Month";
        $scope.Dealer1 = "Dealer";
        $scope.Model1 = "Model";
        $scope.Job_Detail1 = "Job Detail";
        $scope.Response1 = "Response";
       
       
        $scope.year = $rootScope.year;
        $scope.biannual = "All";
        $scope.month = $rootScope.Filterarray.month[1].name;
        $scope.region = $rootScope.region;
 $rootScope.periodhide = true;
        var langparameter = [{"name": "langCode", "value": $rootScope.language}];
        service.mastersrv("Filters-getYear", langparameter).then(function (result) {
            if ($rootScope.language == 'EN') {
                var all = {"name": "All", "value": "All"};
            } else if ($rootScope.language == 'TH') {
                all = {"name": "All", "value": "全部"};
            }
            $rootScope.Filterarray.year = result.data;
            $rootScope.Filterarray.year.splice(0, 0, all);
        });

        $scope.dealer = $rootScope.dealer;

        $scope.langcode = $rootScope.language;
        $rootScope.redfeedback = "redfeedback";
        $rootScope.checkboxInitiliser();

        if ($scope.langcode == 'EN') {
            $scope.export = $rootScope.Titlearray[79].ContentEN;
            $scope.footer = "indicates small sample and is not considered for color rankings";
            $scope.Report_Month1 = "Report Month";
            $scope.Dealer1 = "Dealer";
            $scope.Model1 = "Model";
            $scope.Job_Detail1 = "Job Detail";
            $scope.Response1 = "Response";
        } else {
            $scope.export = $rootScope.Titlearray[79].ContentReganal;
            $scope.footer = "บ่งชี้ว่ากลุ่มตัวอย่างขนาดเล็กและไม่ได้รับการพิจารณาสำหรับการจัดอันดับของสี การส่งออกตาราง";
        }
        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        console.log(" height :", h);
        console.log(" width :", w);
        var margin = 300;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 250;
        } else if (w < 960 && w > 600) {
            margin = 200;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 150;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        console.log('margin', margin);
        //////////////////////////////////////////////////////////////

        $scope.$on('changezone', function (event, data) {
            $scope.zone = data;
            $scope.region = "Study Total";
            $scope.filterchange();
        });

        $scope.$on('changeregion', function (event, data) {
            $scope.region = data;
            $scope.filterchange();
        });

        $scope.$on('changeyear', function (event, data) {
            $scope.year = data;
            $scope.biannual = "Study Total";
            $scope.month = "Study Total";
            $scope.filterchange();
        });

        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual = data;
            $scope.month = "Study Total";
            $scope.filterchange();
        });

        $scope.$on('changemonth', function (event, data) {
            $scope.month = data;
            $scope.filterchange();
        });
        //////////////////////////////////////////////////////////////

        var parameters = [];
        parameters = [{"name": "dealer", "value": $scope.dealer}, {"name": "month", "value": $scope.month}, {"name": "langCode", "value": $scope.langcode},{"name": "region", "value": encodeURIComponent($scope.region)}];
        service.csi_filterfunction();
        $scope.alert_dealer = function (dealer) {
            dealer = dealer.replace(/[*]/g, "");
            $rootScope.dealerScoreDealer = dealer;
            $state.go('triangular.csipage11', {dealer: ''});
            console.log('state2 params:', dealer);
        };



        function createData() {
            if ($scope.langcode == 'EN') {
                $scope.export = $rootScope.Titlearray[79].ContentEN;
//                $scope.footer = "indicates small sample and is not considered for color rankings";
                 $scope.footnote = $rootScope.Footarray[0].ContentEN;
                $scope.footnotetitle = $rootScope.Footarray[0].ContentEN;
                $scope.Report_Month1 = "Report<br>Month";
                $scope.Zone1 = "Zone";
                $scope.Region1 = "Region";
                $scope.Dealer1 = "Dealer";
                $scope.Model1 = "Model";
                $scope.Job_Detail1 = "Job Detail";
                $scope.Response1 = "Response";
                 $scope.mproduct = "Product";
                 
            } else {
                $scope.export = $rootScope.Titlearray[79].ContentReganal;
                $scope.footer = "บ่งชี้ว่ากลุ่มตัวอย่างขนาดเล็กและไม่ได้รับการพิจารณาสำหรับการจัดอันดับของสี การส่งออกตาราง";
                 $scope.footnote = $rootScope.Footarray[0].ContentReganal;
                $scope.footnotetitle = $rootScope.Footarray[0].ContentReganal;
                $scope.Report_Month1 = "รอบการ<br>สำรวจ";
                $scope.Zone1 = "โซน";
                $scope.Region1 = "ภูมิภาค";
                $scope.Dealer1 = "ศูนย์บริการ";
                $scope.Model1 = "รุ่น";
                 $scope.mproduct = "ประเภทเครื่องจักรกล";
                $scope.Job_Detail1 = "รายละเอียดงาน";
                $scope.Response1 = "ความคิดเห็นของลูกค้า";
            }


            service.mastersrv("CSIDealerScoreTable-getCSIotherFeedback", parameters).then(function (response) {
                //                       $scope.testdata = response.data;


                $scope.records = response.data[0].values;

                $scope.othersfeedback = {
                    id: "others",
                    view: "datatable",
//                          height:495,
                    fixedRowHeight: false,
                    rowLineHeight: 25, rowHeight: 25,
                    scroll: 'y',
                    position: "flex",
                    select: "row",
                    hover: "myhover",
                    css: "my_style",
                    tooltip: true,
                    columns: [
                        {id: "Report_Month", css: "columnstyle", header: {text: $scope.Report_Month1, css: 'multiline'}, minWidth: 80, adjust: 'header'},
                        {id: "Dealer", css: "columnstyle", header: {text: $scope.Dealer1}, minWidth: 210, adjust: 'true'},
                         {id: "product", header: [$scope.mproduct], adjust: 'data'},
                        {id: "Model", header: [$scope.Model1], adjust: 'data'},
                        
                        {id: "Response", header: [$scope.Response1], minWidth: 220, fillspace: true}

                    ],
                    data: $scope.records,

                    resizeColumn: true,

                    on: {
                        onAfterLoad: function () {
                            webix.delay(function () {
                                this.adjustRowHeight("Response", true);
                                this.render();
                            }, this);

                        },

                    },

                };


            });

        }
        function mark_votes(value, config) {
            if (value > 400000)
                return {"text-align": "right"};
            return value;
        }



        createData();

        $scope.exportdata = function () {
            webix.toExcel($$("others"), {
                filterHTML: true
            });
        }

        $scope.$on('changeLanguage', function (event, data) {
            $scope.langcode = data;
            $scope.filterchange();
        });


        $scope.filterchange = function () {
            console.log("filter change of dscore called");
            parameters = [{"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "dealer", "value": $scope.dealer}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}];
            createData();
        };
    }
    ;
})();