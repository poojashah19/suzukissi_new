(function() {
    'use strict';

    angular
        .module('app.module3.page15')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

           $stateProvider
         .state('triangular.dealercsipage15', {
            url: '/dealercsipage15',
            views: {
                '': {
                   templateUrl: 'app/module3/page15/page15.tmpl.html',
                    controller: 'm3page15Controller',
                    controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module3/page15/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
            
        });
        
//         triMenuProvider.addMenu({
//                      name: 'Red Alert',
//                    icon: 'zmdi zmdi-view-list-alt',
//                    type: 'link',
//                    priority: 2.2,
//                     state: 'triangular.csipage15',
//                });
//             triMenuProvider.removeMenu('triangular.page15');   
                
                
    }
})();