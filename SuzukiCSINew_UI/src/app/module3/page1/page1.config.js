(function() {
    'use strict';

    angular
        .module('app.module3.page1')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

          $stateProvider
         .state('triangular.dealercsipage1', {
            url: '/dealercsipage1',
            views: {
                '': {
                   templateUrl: 'app/module3/page1/page1.tmpl.html',
                    controller: 'm3page1Controller',
                    controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module3/page1/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
            
        });
//        
//        
//         triMenuProvider.addMenu({
//                    name: 'Priority Analysis',
//                    icon: 'zmdi zmdi-car',
//                    type: 'link',
//                    priority: 1.3,
//                     state: 'triangular.dealercsipage1',
//                });
//                triMenuProvider.removeMenu('triangular.dealercsipage1'); 
  
                
    }
})();