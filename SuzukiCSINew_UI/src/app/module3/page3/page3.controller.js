(function () {
    'use strict';

    angular
            .module('app.module3.page3')
            .controller('m3page3Controller', m3page3Controller);

    /* @ngInject */
    function m3page3Controller($scope, service, $state, $http, $rootScope, $timeout) {

        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776", "#92D400", "#00A1DE", "#72C7E7", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.hideyear = false;
        $rootScope.hidebiannual = false;
        $rootScope.hidemonth = false;
        $rootScope.hidezone = true;
        $rootScope.hideregion = true;
        $rootScope.snaphidezone = true;
        $rootScope.snaphideregion = true;
        $rootScope.hidemodel = true;
        $rootScope.hidefactor = true;
        $rootScope.loyaltydealerhide = false;
        $rootScope.dealerpriorityhide = true;
        $rootScope.mainselectionhide = false;
        $rootScope.filterSelShow = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.hidefactor = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.dealerhide = true;

        $scope.changeloyalSwitch = 'Study Total';
        $scope.dealer1 = $rootScope.dealer;
        $scope.region1 = $rootScope.region;
        $rootScope.dealersnaphide = true;
        $rootScope.pagenumber = '8';
        $rootScope.starlabelhide = false;
        $rootScope.priorityhide = true;
        $rootScope.summaryRefresh = 1;
        $rootScope.dsnapshot = "not";
        $rootScope.dealerScoreDealer == "null"
        $scope.langcode = $rootScope.language;
        $rootScope.redfeedback = "not";
        $rootScope.checkboxInitiliser();
        $scope.region = "All";
        $scope.area = "All";
        $scope.zone = "All";
        $scope.province = "All";
        $scope.zone = "All";
        $scope.region = "All";
        $scope.model = "All";
        $scope.year = $rootScope.year;
        $scope.biannual = "All";
        $scope.month = $rootScope.Filterarray.month[1].name;
        $rootScope.legendforselectionb = "Best Overall";
        $rootScope.legendforselectionw = "Worst Overall";
        $rootScope.periodhide = true;
        if ($scope.langcode == 'EN') {
            console.log($rootScope.Titlearray)
            $scope.export = $rootScope.Titlearray[79].ContentEN;
            $scope.table0 = $rootScope.Titlearray[80].ContentEN;
            $scope.table1 = $rootScope.Titlearray[81].ContentEN;
            $scope.table3 = $rootScope.Titlearray[149].ContentEN;
//            $scope.table4 = $rootScope.Titlearray[84].ContentEN;
            $scope.table5 = $rootScope.Titlearray[150].ContentEN;
            $scope.table6 = $rootScope.Titlearray[151].ContentEN;
            $scope.table7 = $rootScope.Titlearray[152].ContentEN;
//            $scope.table5 = $rootScope.Titlearray[85].ContentEN;
//            $scope.table6 = $rootScope.Titlearray[86].ContentEN;
//            $scope.table7 = $rootScope.Titlearray[87].ContentEN;
//            $scope.table8 = "SOP Implem- ented";
            $scope.footnote = $rootScope.Footarray[0].ContentEN;
            $scope.footer = "indicates small sample and is not considered for color rankings";
            $rootScope.legendforselectionb = "Best Overall";
            $rootScope.legendforselectionw = "Worst Overall";
        } else {
            $scope.export = $rootScope.Titlearray[79].ContentReganal;
            $scope.table0 = $rootScope.Titlearray[80].ContentReganal;
            $scope.table1 = $rootScope.Titlearray[81].ContentReganal;
            $scope.table2 = $rootScope.Titlearray[82].ContentReganal;
            $scope.table3 = $rootScope.Titlearray[149].ContentReganal;
//                $scope.table4 = $rootScope.Titlearray[84].ContentEN;
            $scope.table5 = $rootScope.Titlearray[150].ContentReganal;
            $scope.table6 = $rootScope.Titlearray[151].ContentReganal;
            $scope.table7 = $rootScope.Titlearray[152].ContentReganal;
//            $scope.table8 = $rootScope.Titlearray[88].ContentReganal;
            $scope.footnote = $rootScope.Footarray[0].ContentReganal;
            $scope.footer = "บ่งชี้ว่ากลุ่มตัวอย่างขนาดเล็กและไม่ได้รับการพิจารณาสำหรับการจัดอันดับของสี การส่งออกตาราง";
            $rootScope.legendforselectionb = "ดีที่สุดจากทั่วประเทศ";
            $rootScope.legendforselectionw = "แย่ที่สุดจากทั่วประเทศ";
        }

        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        console.log(" height :", h);
        console.log(" width :", w);
        var margin = 300;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 250;
        } else if (w < 960 && w > 600) {
            margin = 200;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 150;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        console.log('margin', margin);
        //////////////////////////////////////////////////////////////

        $scope.$on('changeLanguage', function (event, data) {
            $scope.langcode = data;
            $scope.showLoader = true;
            if ($scope.langcode == 'EN') {
                $rootScope.legendforselectionb = "Best Overall";
                $rootScope.legendforselectionw = "Worst Overall";
            } else {
                $rootScope.legendforselectionb = "ดีที่สุดจากทั่วประเทศ";
                $rootScope.legendforselectionw = "แย่ที่สุดจากทั่วประเทศ";
            }
            $scope.filterchange();
        });

        $scope.$on('changeyear', function (event, data) {
            $scope.year = data;
            $scope.biannual = "All";
            $scope.month = "All";
            $scope.showLoader = true;
            $scope.filterchange();
        });

        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual = data;
            $scope.month = "All";
            $scope.showLoader = true;
            $scope.filterchange();
        });

        $scope.$on('changemonth', function (event, data) {
            $scope.month = data;
            $scope.showLoader = true;
            $scope.filterchange();
        });

        $scope.$on('changezone', function (event, data) {
            $scope.zone = data;
            $scope.region = "All";
            $scope.dealer = "All";
            $scope.showLoader = true;
            $scope.filterchange();
        });

        $scope.$on('changeregion', function (event, data) {
            $scope.region = data;
            $scope.dealer = "All";
            $scope.showLoader = true;
            $scope.filterchange();
        });

        $scope.$on('changemodel', function (event, data) {
            $scope.model = data;
            $scope.showLoader = true;
            $scope.filterchange();
        });


        $scope.$on('changeloyalSwitch', function (event, data) {
            $scope.changeloyalSwitch = data;
//            alert($scope.changeloyalSwitch + "$scope.changeloyalSwitch")
            $scope.filterchange();
        });


        //////////////////////////////////////////////////////////////


        $scope.finished = function () {
            $timeout(function () {
                $(document).ready(function () {
                    $("#fixTable").tableHeadFixer();
                });
            }, 0);

        };
        $scope.dealer = $rootScope.dealer;
        var parameters = [{"name": "changeloyalSwitch", "value": $scope.changeloyalSwitch}, {"name": "dealerRegion", "value": $scope.region1}, {"name": "langCode", "value": $scope.langcode}, {"name": "dealer", "value": $scope.dealer}, {"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}];
        service.csi_filterfunction();
        $scope.alert_dealer = function (dealer) {
            dealer = dealer.replace(/[*]/g, "");
            $rootScope.dealerScoreDealer = dealer;
            $state.go('triangular.csipage11', {dealer: ''});
            console.log('state2 params:', dealer);
        }


        $("#exportCS").click(function () {
            $("#table2").table2excel({
                // exclude CSS class
                exclude: ".noExl",
                filename: "DealerScoresDownload"
            });
        });






        function createData() {
            if ($scope.langcode == 'EN') {
                console.log($rootScope.Titlearray)
                $scope.export = $rootScope.Titlearray[79].ContentEN;
                $scope.table0 = $rootScope.Titlearray[80].ContentEN;
                $scope.table1 = $rootScope.Titlearray[81].ContentEN;
                $scope.table2 = $rootScope.Titlearray[82].ContentEN;
                $scope.table3 = $rootScope.Titlearray[149].ContentEN;
//                $scope.table4 = $rootScope.Titlearray[84].ContentEN;
                $scope.table5 = $rootScope.Titlearray[150].ContentEN;
                $scope.table6 = $rootScope.Titlearray[151].ContentEN;
                $scope.table7 = $rootScope.Titlearray[152].ContentEN;
//                $scope.table5 = $rootScope.Titlearray[85].ContentEN;
//                $scope.table6 = $rootScope.Titlearray[86].ContentEN;
//                $scope.table7 = $rootScope.Titlearray[87].ContentEN;
//                $scope.table8 = "SOP Implem- ented";
                $scope.footnote = $rootScope.Footarray[0].ContentEN;
                $scope.footnotetitle = $rootScope.Footarray[0].ContentEN;
                $scope.footer = "indicates small sample and is not considered for color rankings";
                $scope.tableregion = 'Region';
                $rootScope.legendforselectionb = "Best Overall";
                $rootScope.legendforselectionw = "Worst Overall";

            } else {
                $scope.export = $rootScope.Titlearray[79].ContentReganal;
                $scope.table0 = $rootScope.Titlearray[80].ContentReganal;
                $scope.table1 = $rootScope.Titlearray[81].ContentReganal;
                $scope.table2 = $rootScope.Titlearray[82].ContentReganal;
                $scope.table3 = $rootScope.Titlearray[149].ContentReganal;
//                $scope.table4 = $rootScope.Titlearray[84].ContentEN;
                $scope.table5 = $rootScope.Titlearray[150].ContentReganal;
                $scope.table6 = $rootScope.Titlearray[151].ContentReganal;
                $scope.table7 = $rootScope.Titlearray[152].ContentReganal;
                $scope.footnotetitle = $rootScope.Footarray[0].ContentReganal;
//                $scope.table5 = $rootScope.Titlearray[85].ContentReganal;
//                $scope.table6 = $rootScope.Titlearray[86].ContentReganal;
//                $scope.table7 = $rootScope.Titlearray[87].ContentReganal;
//                $scope.table8 = $rootScope.Titlearray[88].ContentReganal;
                $scope.footnote = $rootScope.Footarray[0].ContentReganal;
//                $scope.table2 = $rootScope.Titlearray[82].ContentReganal;
//                $scope.table3 = $rootScope.Titlearray[83].ContentReganal;
//                $scope.table4 = $rootScope.Titlearray[84].ContentReganal;
//                $scope.table5 = $rootScope.Titlearray[85].ContentReganal;
//                $scope.table6 = $rootScope.Titlearray[86].ContentReganal;
//                $scope.table7 = $rootScope.Titlearray[87].ContentReganal;
//                $scope.table8 = $rootScope.Titlearray[88].ContentReganal;
//                $scope.footnote = $rootScope.Footarray[1].ContentReganal;
                $scope.tableregion = 'ภูมิภาค';
                $scope.footer = "บ่งชี้ว่ากลุ่มตัวอย่างขนาดเล็กและไม่ได้รับการพิจารณาสำหรับการจัดอันดับของสี การส่งออกตาราง";
                $rootScope.legendforselectionb = "ดีที่สุดจากทั่วประเทศ";
                $rootScope.legendforselectionw = "แย่ที่สุดจากทั่วประเทศ";

            }


            var dealerclick = " <span class='downloadtext'>#dealer#</span>";
            $scope.dalerscorettable = {
                id: "dealertable",
                view: "datatable",
                headerRowHeight: 75,
                fixedRowHeight: false,
                rowLineHeight: 30,
                rowHeight: 30,
                scroll: 'y',
                position: "flex",
                css: "my_style",
                columns: [
                    {id: "rank", css: "columnstyle", header: {text: $scope.table0, css: 'multiline'}, width: 50},
                    {id: "region", css: "columnstyle", header: {text: $scope.tableregion}, adjust: 'true', minWidth: 105},
                    {id: "dealer", css: "columnstyle", header: {text: $scope.table1}, minWidth: 45, fillspace: true, adjust: 'true'},
                    {id: "csi", header: [$scope.table2], width: 70},
                    {id: "serviceinitiation", header: {text: $scope.table3, css: 'multiline'}, width: 70},
//                    {id: "serviceadvisor", header: {text: $scope.table4, css: 'multiline'}, width: 70},
                    {id: "servicefacility", header: {text: $scope.table5, css: 'multiline'}, width: 150},
                    {id: "vehiclepickup", header: {text: $scope.table6, css: 'multiline'}, width: 120},
                    {id: "servicequality", header: {text: $scope.table7, css: 'multiline'}, width: 70},
//                    {id: "sop", header: {text: $scope.table8, css: 'multiline'}, width: 70}
                ],
            };


            service.mastersrv("CSIDealerScoreTableDealer-getCSIDealerScoreTable1", parameters).then(function (response) {
//              vm.salesData=response.data;
                $scope.records = response.data;
                console.log('response.data[0].counterfreeze;', response.data[0].counterfreeze);
                console.log('response.data[0].counterfreeze;', response.data[0].counterfreeze);
                console.log('response.data[0].counterfreeze;', response.data[0].counterfreeze);
                console.log('response.data[0].counterfreeze;', response.data[0].counterfreeze);
                $scope.countfreeze = response.data[0].counterfreeze;
                console.log($scope.records);
                $scope.dalerscorettable = {
                    id: "dealertable",
                    view: "datatable",
                    fixedRowHeight: false,
                    headerRowHeight: 75,
                    rowLineHeight: 30,
                    rowHeight: 30,
                    scroll: 'y',
                    position: "flex",
                    topSplit: $scope.countfreeze,
                    select: "row",
                    css: "my_style",
                    tooltip: true,
                    columns: [
                        {id: "rank", css: "columnstyle", header: {text: $scope.table0, css: 'multiline'}, width: 50},
                        {id: "region", css: "columnstyle", header: {text: $scope.tableregion}, adjust: 'true', minWidth: 100},
                        {id: "regioneng", css: "columnstyle", header: {text: $scope.tableregion}, adjust: 'true'},
                        {id: "dealereng", css: "columnstyle", header: {text: $scope.table1}, adjust: 'true'},
                        {id: "dealer", css: "columnstyle", header: {text: $scope.table1}, minWidth: 45, fillspace: true, adjust: 'true', template: dealerclick},
                        {id: "csi", header: [$scope.table2], width: 70, cssFormat: mark_csi},
                        {id: "serviceinitiation", header: {text: $scope.table3, css: 'multiline'}, width: 70, cssFormat: mark_serviceinitiation},
//                        {id: "serviceadvisor", header: {text: $scope.table4, css: 'multiline'}, width: 70, cssFormat: mark_serviceadvisor},
                        {id: "servicefacility", header: {text: $scope.table5, css: 'multiline'}, width: 120, cssFormat: mark_ServiceEngineer},
                        {id: "vehiclepickup", header: {text: $scope.table6, css: 'multiline'}, width: 80, cssFormat: mark_vehiclepickup},
                        {id: "servicequality", header: {text: $scope.table7, css: 'multiline'}, width: 70, cssFormat: mark_servicequality},
//                        {id: "sop", header: {text: $scope.table8, css: 'multiline'}, width: 70, cssFormat: mark_sop}
                    ],
                    data: $scope.records,
                    resizeColumn: true,
                    ready: function () {
                        $$("dealertable").hideProgress();
                        $$("dealertable").hideColumn("regioneng");
                        $$("dealertable").hideColumn("dealereng");
                    },
                    onClick: {
                        "downloadtext": function (e, id) {

                            $scope.dealer = this.getItem(id).dealereng;
                            $scope.region = this.getItem(id).region;
                            $scope.alert_dealer(this.getItem(id).dealereng);

                        }}


//                        on: {
//                       
////                        "onresize":webix.once(function(){ 
////                            this.adjustRowHeight("dealer", true); 
////                        }),
//                        'onItemClick': function (e, id) {
//                            var datatable = $$("dealertable");
//                            var firstid = datatable.getFirstId();
//                            var secondid = datatable.getNextId(firstid, 1);
//                            var thirdid = datatable.getNextId(secondid, 1);
//                            var forthid = datatable.getNextId(thirdid, 1);
//                            var fifthid = datatable.getNextId(forthid, 1);
//                            var sixthid = datatable.getNextId(fifthid, 1);
//                            var seventhid = datatable.getNextId(sixthid, 1);
//                            var eighthid = datatable.getNextId(seventhid, 1);
//                            var p=0;
//                            if($scope.countfreeze==8){
////                                alert("count 7");
//                                    $$("dealertable").remove(firstid);
//                                    $$("dealertable").remove(secondid);
//                                    $$("dealertable").remove(thirdid);
//                                    $$("dealertable").remove(forthid);
//                                    $$("dealertable").remove(fifthid);
//                                    $$("dealertable").remove(sixthid);
//                                    $$("dealertable").remove(seventhid);
//                                    $$("dealertable").remove(eighthid);
//                                     var selectedId = datatable.getSelectedId()
//                                    if (selectedId != firstid && selectedId != secondid && selectedId != thirdid && selectedId != forthid && selectedId != fifthid && selectedId != sixthid && selectedId != seventhid && selectedId != eighthid )
//                                    {
//                                        var row = datatable.getItem(selectedId);
//                                        $scope.dealer = row.dealereng;
//                                        $scope.region = row.regioneng;
//                                        parameters = [{"name": "langCode", "value": $scope.langcode}, {"name": "dealer", "value": $scope.dealer},{"name":"zone","value":encodeURIComponent($scope.zone)}, {"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value":encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}]
//                                        service.mastersrv("CSIDealerScoreTable-getCSITOP3RowsDealerScore", parameters).then(function (response) {
//                                            console.log(response.data);
//                                                $$("dealertable").add(response.data[0], 0);
//                                                $$("dealertable").add(response.data[1], 1);
//                                                $$("dealertable").add(response.data[2], 2);
//                                               $$("dealertable").define("topSplit", 3);
//                                                $$("dealertable").refreshColumns();
//                                                $scope.countfreeze=3;
//                                           });
//                                    }
//                                     
//                            }
//                            else  if($scope.countfreeze==6){
////                                alert("count 6");
//                                $$("dealertable").remove(firstid);
//                                    $$("dealertable").remove(secondid);
//                                    $$("dealertable").remove(thirdid);
//                                    $$("dealertable").remove(forthid);
//                                    $$("dealertable").remove(fifthid);
//                                    $$("dealertable").remove(sixthid);
//                                     var selectedId = datatable.getSelectedId()
//                                    if (selectedId != firstid && selectedId != secondid && selectedId != thirdid && selectedId != forthid && selectedId != fifthid && selectedId != sixthid)
//                                    {
//                                        var row = datatable.getItem(selectedId);
//                                        $scope.dealer = row.dealereng;
//                                        $scope.region = row.regioneng;
//                                        parameters = [{"name": "langCode", "value": $scope.langcode}, {"name": "dealer", "value": $scope.dealer},{"name":"zone","value":encodeURIComponent($scope.zone)}, {"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value":encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}]
//                                        service.mastersrv("CSIDealerScoreTable-getCSITOP3RowsDealerScore", parameters).then(function (response) {
//                                            console.log(response.data);
//                                                $$("dealertable").add(response.data[0], 0);
//                                                $$("dealertable").add(response.data[1], 1);
//                                                $$("dealertable").add(response.data[2], 2);
//                                                $$("dealertable").define("topSplit", 3);
//                                                $$("dealertable").refreshColumns();
//                                                $scope.countfreeze=3;
//                                           });
//                                    }
//                            }
//                            else  if($scope.countfreeze==3){
////                                alert("count 3");
//                                $$("dealertable").remove(firstid);
//                                    $$("dealertable").remove(secondid);
//                                    $$("dealertable").remove(thirdid);
//                                     var selectedId = datatable.getSelectedId()
//                                    if (selectedId != firstid && selectedId != secondid && selectedId != thirdid)
//                                    {
//                                        var row = datatable.getItem(selectedId);
//                                        $scope.dealer = row.dealereng;
//                                        $scope.region = row.regioneng;
//                                        parameters = [{"name": "langCode", "value": $scope.langcode}, {"name": "dealer", "value": $scope.dealer},{"name":"zone","value":encodeURIComponent($scope.zone)}, {"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value":encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}]
//                                        service.mastersrv("CSIDealerScoreTable-getCSITOP3RowsDealerScore", parameters).then(function (response) {
//                                            console.log(response.data);
//                                                $$("dealertable").add(response.data[0], 0);
//                                                $$("dealertable").add(response.data[1], 1);
//                                                $$("dealertable").add(response.data[2], 2);
//                                                $$("dealertable").define("topSplit", 3);
//                                                $$("dealertable").refreshColumns();
//                                                $scope.countfreeze=3;
//                                           });
//                                    }
//                            }
//                           
//                        }
//
//                    },


                };



            })

//            dealertable

        }



        $scope.doSome = function () {
            webix.extend($$("dealertable"), webix.ProgressBar);
            $$("dealertable").showProgress({
                type: "icon",
            });
        }
        createData();

        function mark_dealer(value, config) {
            if (config["dealereng"] == $scope.dealer1)
            {
                console.log({"background-color": '#557DFA  !important', "color": '#FFFFFF !important'});

                return {"background-color": '#557DFA  !important', "color": '#FFFFFF !important'};
            } else
            {
                return {"background-color": '#EEEEEE !important', "color": '#000000 !important'};
            }
            return value;

        }

        function mark_csi(value, config) {
            if (config["csicolor"] != "White")
            {
                console.log({"background-color": config["csicolor"], "color": '#FFFFFF !important'});

                return {"background-color": config["csicolor"], "color": '#FFFFFF !important'};
            } else
            {
                return {"background-color": config["csicolor"]};
            }
            return value;

        }

        function mark_serviceinitiation(value, config) {
            if (config["serviceinitiationcolor"] != "White")
            {
                console.log({"background-color": config["serviceinitiationcolor"], "color": '#FFFFFF !important'});

                return {"background-color": config["serviceinitiationcolor"], "color": '#FFFFFF !important'};
            } else
            {
                return {"background-color": config["serviceinitiationcolor"]};
            }
            return value;

        }


        function mark_serviceadvisor(value, config) {
            if (config["serviceadvisorcolor"] != "White")
            {
                console.log({"background-color": config["serviceadvisorcolor"], "color": '#FFFFFF !important'});

                return {"background-color": config["serviceadvisorcolor"], "color": '#FFFFFF !important'};
            } else
            {
                return {"background-color": config["serviceadvisorcolor"]};
            }
            return value;

        }
        function mark_ServiceEngineer(value, config) {
            if (config["servicefacilitycolor"] != "White")
            {
                console.log({"background-color": config["servicefacilitycolor"], "color": '#FFFFFF !important'});

                return {"background-color": config["servicefacilitycolor"], "color": '#FFFFFF !important'};
            } else
            {
                return {"background-color": config["servicefacilitycolor"]};
            }

            return value;
        }
        function mark_vehiclepickup(value, config) {
            if (config["vehiclepickupcolor"] != "White")
            {
                console.log({"background-color": config["vehiclepickupcolor"], "color": '#FFFFFF !important'});

                return {"background-color": config["vehiclepickupcolor"], "color": '#FFFFFF !important'};
            } else
            {
                return {"background-color": config["vehiclepickupcolor"]};
            }

            return value;
        }
        function mark_servicequality(value, config) {
            if (config["servicequalitycolor"] != "White")
            {
                console.log({"background-color": config["servicequalitycolor"], "color": '#FFFFFF !important'});

                return {"background-color": config["servicequalitycolor"], "color": '#FFFFFF !important'};
            } else
            {
                return {"background-color": config["servicequalitycolor"]};
            }
            return value;

        }
        function mark_sop(value, config) {
            if (config["sopcolor"] != "White")
            {
                console.log({"background-color": config["sopcolor"], "color": '#FFFFFF !important'});

                return {"background-color": config["sopcolor"], "color": '#FFFFFF !important'};
            } else
            {
                return {"background-color": config["sopcolor"]};
            }
            return value;

        }

        $scope.exportdata = function () {
            webix.toExcel($$("dealertable"), {
                filterHTML: true
            });
            //  webix.toExcel($$("dealertable"));
        }
        $scope.filterchange = function () {
            console.log("filter change of dscore called");
            parameters = [{"name": "changeloyalSwitch", "value": $scope.changeloyalSwitch}, {"name": "dealerRegion", "value": $scope.region1}, {"name": "langCode", "value": $scope.langcode}, {"name": "dealer", "value": $scope.dealer}, {"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}];
            createData();
        }
    }
})();