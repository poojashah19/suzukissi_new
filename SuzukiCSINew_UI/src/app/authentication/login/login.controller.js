(function () {
    'use strict';

    angular
            .module('authentication')
            .controller('LoginController', LoginController);

    /* @ngInject */
    function LoginController($window, $http, $cookies, $timeout, $scope, $rootScope, $state, triSettings, service, $mdDialog) {
        var vm = this;
        vm.loginClick = loginClick;

        $rootScope.themes = ["#EC407A", "#FFC000", "#6A1B9A", "#00E676", "#536DFE"];
        var rand = Math.floor(Math.random() * $rootScope.themes.length);
        $scope.themecolor = $rootScope.themes[rand];
        $rootScope.oemDownloadShow = true;

        vm.triSettings = triSettings;
        // create blank user variable for login form
        vm.user = {
            email: '',
            password: ''
        };
        $rootScope.language = 'EN';
        $rootScope.englishshow = true;
        $rootScope.thaishow = false;

        $rootScope.loyaltydealerhide = true;
        $rootScope.dealerpriorityhide = true;
        $rootScope.mainselectionhide = false;
        $rootScope.filterSelShow = true;

        $rootScope.Filterarray = [];
        $rootScope.language = "EN";
        service.csi_filterfunction();
        $rootScope.swichenable = true;
        $rootScope.nameit = "CSI";
        $rootScope.sessionid = 0;
        $rootScope.csissi = true;
        $rootScope.dataTableShow = true;
        service.csi_titles();
        service.mastersrv("SSIFilters-getWaves").then(function (result) {
            $rootScope.Filterarray.waves = result.data;
        });
//report type  false- oem     && true- dealer
//switchenable false- ssi,csi && true- both
//csissi       false-csi      && true- ssi

        function loginClick() {
//             $cookies.put('tri-user', vm.user.email);
//            $window.location.reload();
            $rootScope.summaryoffset = true;
            $rootScope.themes = ["#EC407A", "#FFC000", "#6A1B9A", "#00E676", "#536DFE"];


            $rootScope.color = ["#002776", "#92D400",
                "#00A1DE",
                "#72C7E7", "#3C8A2E",
                "#C9DD03", "#335291", "#A8DD33", "#33B4E5",
                "#8ED2EC",
                "#63A158",
                "#D4E435",
                "#4D689F",
                "#B3E14D",
                "#4DBDE8",
                "#9DD8EE",
                "#77AD6D",
                "#D9E74F",
                "#7F93BA",
                "#C8E97F",
                "#7FD0EE",
                "#B8E3F3",
                "#9DC496",
                "#E4EE81",
                "#99A 9C8",
                "#D3EE99",
                "#99D9F2",
                "#C7E9F5",
                "#B1D0AB",
                "#E9F19A",
                "#BFC9DD",
                "#E4F4BF",
                "#BFE7F7",
                "#DCF1F9",
                "#CEE2CB", "#F1F6C0",
                "#8C8C8C",
                "#B4B4B4",
                "#DCDCDC"];

            var reporttype;
            var dealername;
            var cityname;
            var statename;
            var regionname;

            if (vm.user.email == '' || vm.user.password == '') {
                $mdDialog.show({
                    templateUrl: 'failed.html',
                    parent: angular.element(document.body),
                    clickOutsideToClose: true
                })
                $state.go('login');
            }
            var loginobj = [{"name": "user_id", "value": vm.user.email}, {"name": "user_password", "value": vm.user.password}];
            service.mastersrv("Systemuserview-Login", loginobj).then(function (result) {
                reporttype = result.data.userroleid;
                dealername = result.data.lastname;
                cityname = result.data.city;
                statename = result.data.dregion;
                regionname = result.data.dregion;

                if (reporttype == 9) {

                    $rootScope.reporttype = true;
                    $rootScope.csissi = true;

                    service.csi_filterfunction();
                    service.csi_titles();
                    $timeout(function () {
                        $state.go('triangular.csipage4');
                    }, 2000);
                    $rootScope.swichenable = true;
                    $rootScope.nameit = "CSI";
                    $rootScope.dealer = dealername;
//                    $rootScope.nameit = dealername;
                    $rootScope.nameitx = "oemcsi";
                    $rootScope.oemDownloadShow = false;
                    $rootScope.sessionid = 1001;
//                                vm.toggleExtraMenu('ssi');
//                                $rootScope.tempfunction($rootScope.reporttype)

                } else if (reporttype == 1) {
                    $rootScope.reporttype = true;
                    $rootScope.csissi = false;
                    service.ssi_filterfunction();
                    app.constant('rightsidecntrl', 'RightSidenavControllercsi')
                    $state.go('triangular.csipage4');
                    $rootScope.swichenable = true;
                    $rootScope.nameit = "CSI";
                    $rootScope.dealer = dealername;
                    $rootScope.nameitx = "oemcsi";
                    $rootScope.sessionid = 1001;
                    service.csi_titles();
                    $rootScope.oemDownloadShow = false;
                    vm.toggleExtraMenu('csi');
//                                $rootScope.tempfunction($rootScope.reporttype)
//                                 service.ssi_filterfunction();
                } else if (reporttype == 11) {
                    service.ssi_filterfunction();
                    $rootScope.reporttype = false;
                    $rootScope.csissi = false;
                    service.ssi_filterfunction();
                    $state.go('triangular.dealercsipage34');
                    $rootScope.swichenable = true;
                    $rootScope.nameit = dealername;
                    $rootScope.sessionid = 1001;
                    $rootScope.dealer = dealername;
                    $rootScope.dataTableShow = false;
                    service.csi_titles();
                    $rootScope.oemDownloadShow = true;
                    $rootScope.city = cityname;
                    $rootScope.state = statename;
                    $rootScope.region = regionname;
//                                vm.toggleExtraMenu('dealerssi');
//                                $rootScope.tempfunction($rootScope.csissi)
                } else if (reporttype == 10) {

                    $rootScope.reporttype = false;
                    $rootScope.csissi = true;
                    $rootScope.dealer = dealername;
                    $rootScope.nameit = dealername;
                    console.log("dealer", dealername);
                    console.log("$rootScope.dealer", $rootScope.dealer);
                    console.log("$rootScope.nameit", $rootScope.nameit);
                    console.log("dealer", dealername);
                    console.log("$rootScope.dealer", $rootScope.dealer);
                    console.log("$rootScope.nameit", $rootScope.nameit);
                    $rootScope.oemDownloadShow = true;
                    service.csi_filterfunction();
                    service.csi_titles();
                    $timeout(function () {


                        $state.go('triangular.dealercsipage4');
                    }, 2000);

                    $rootScope.swichenable = true;
                    $rootScope.sessionid = 1001;
                    $rootScope.dataTableShow = false;

                    $rootScope.city = cityname;
                    $rootScope.state = statename;
                    $rootScope.region = regionname;
//                    alert($rootScope.region)
//                                vm.toggleExtraMenu('dealercsi');
//                                $rootScope.tempfunction($rootScope.reporttype)

                } else {
                    $mdDialog.show({
                        templateUrl: 'failed.html',
                        parent: angular.element(document.body),
                        clickOutsideToClose: true
                    })
                }
            })


        }


        $scope.showAdvanced = function (ev) {
            console.log($rootScope.csissi);
            $mdDialog.show({
                templateUrl: 'forgot.html',
                parent: angular.element(document.body),
                clickOutsideToClose: true
            })

        };
    }
})();
