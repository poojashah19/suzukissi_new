(function() {
    'use strict';

    angular
        .module('app.module2.page3')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

           $stateProvider
         .state('triangular.csipage3', {
            url: '/csipage3',
            views: {
                '': {
                   templateUrl: 'app/module2/page3/page3.tmpl.html',
                    controller: 'm2page3Controller',
                    controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module2/page3/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
            
        });
        
//         triMenuProvider.addMenu({
//                      name: 'Dealer Ranking',
//                    icon: 'zmdi zmdi-view-list-alt',
//                    type: 'link',
//                    priority: 1.7,
//                     state: 'triangular.csipage3',
//                });
//             triMenuProvider.removeMenu('triangular.page3');   
                
                
    }
})();