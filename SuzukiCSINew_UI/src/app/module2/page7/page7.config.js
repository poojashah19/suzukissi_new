(function() {
    'use strict';

    angular
        .module('app.module2.page7')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

          $stateProvider
          .state('triangular.csipage7', {
            url: '/csipage7',
            
             views: {
                '': {
                   templateUrl: 'app/module2/page7/page7.tmpl.html',
            controller: 'm2page7Controller',
            controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module2/page7/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        });

//          triMenuProvider.removeMenu('triangular.dealercsipage37');  
//         triMenuProvider.addMenu({
//                   name: 'Trend Analysis',
//                    icon: 'zmdi zmdi-menu',
//                    type: 'link',
//                    priority: 1.2,
//                     state: 'triangular.csipage7',
//                }); 
   
                
    }
})();