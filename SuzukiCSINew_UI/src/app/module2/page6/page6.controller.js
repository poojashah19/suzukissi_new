(function () {
    'use strict';

    angular
            .module('app.module2.page6')
            .controller('m2page6Controller', m2page6Controller);

    /* @ngInject */
    function m2page6Controller($scope, $state, service, $http, $rootScope) {
        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776", "#92D400", "#00A1DE", "#72C7E7", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.hideyear = false;
        $rootScope.hidebiannual = false;
        $rootScope.hidemonth = false;
        $rootScope.hidezone = true;
        $rootScope.hideregion = true;
        $rootScope.hidemodel = false;
        $rootScope.snaphidezone = true;
        $rootScope.snaphideregion = true;
        $rootScope.hidefactor = false;
        $rootScope.hidetrendfactor = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.dealerhide = true;
        $rootScope.dealersnaphide = true;
        $rootScope.pagenumber = '10';
        $rootScope.starlabelhide = false;
        $rootScope.legendforselectionb = "Best Overall";
        $rootScope.legendforselectionw = "Worst Overall";
        $rootScope.priorityhide = true;
        $rootScope.loyaltydealerhide = true;
        $rootScope.redfeedback = "not";
        $rootScope.mainselectionhide = false;
        $rootScope.filterSelShow = true;
        $rootScope.dealerScoreDealer = $rootScope.Filterarray.dealer[1].name;
        $rootScope.summaryRefresh = 1;
        $rootScope.dsnapshot = "not";
        $scope.zone = "All";
        $scope.region = "All";
        $rootScope.redalertdealerhide = true;

        $scope.model = "All";
        $scope.year = $rootScope.year;
        $scope.biannual = "All";
        $scope.month = $rootScope.Filterarray.month[1].name;
        $rootScope.periodhide = false;
        $scope.period = "Month";

        $rootScope.dealerScoreDealer == "null";
        $rootScope.modelsnap = "modelsnap";
        $scope.langcode = $rootScope.language;
        $rootScope.checkboxInitiliser();
        var languagepara = [];
        var modelpara = [];
        var parameters = [];
        $scope.area = "All";
        $scope.zone = "All";
        $scope.province = "All";
        $scope.city = "All";
        $scope.factor = $rootScope.Filterarray.factor[0].name;
        $scope.factorname = $rootScope.Filterarray.factor[0].value;
        $scope.model = $rootScope.Filterarray.model[0].name;
        $scope.modelname = $rootScope.Filterarray.model[0].value + " - ";
        if ($scope.model == "All") {
            $scope.modelname = " ";
        }

        if ($scope.langcode == 'EN') {
            $scope.model1 = $rootScope.Titlearray[116].ContentEN;
            $scope.model2 = $rootScope.Titlearray[117].ContentEN;
            $scope.mtable1 = $rootScope.Titlearray[149].ContentEN;
//                $scope.mtable2 = $rootScope.Titlearray[145].ContentEN;
            $scope.mtable3 = $rootScope.Titlearray[150].ContentEN;
            $scope.mtable4 = $rootScope.Titlearray[151].ContentEN;
            $scope.mtable5 = $rootScope.Titlearray[152].ContentEN;

            $rootScope.legendforselectionb = "Best Overall";
            $rootScope.legendforselectionw = "Worst Overall";
            $scope.mtable0 = "Product";
            $scope.model = "All";
            $scope.factorscore = "Factor Score";
            $scope.model3 = $rootScope.Titlearray[129].ContentEN;
            $scope.footnote = $rootScope.Footarray[0].ContentEN;
        } else {
            $scope.model1 = $rootScope.Titlearray[116].ContentReganal;
            $scope.model2 = $rootScope.Titlearray[117].ContentReganal;
            $scope.mtable1 = $rootScope.Titlearray[83].ContentReganal;
            $rootScope.legendforselectionb = "ดีที่สุดจากทั่วประเทศ";
            $rootScope.legendforselectionw = "แย่ที่สุดจากทั่วประเทศ";
            $scope.mtable2 = $rootScope.Titlearray[149].ContentReganal;
            $scope.mtable3 = $rootScope.Titlearray[150].ContentReganal;
            $scope.mtable4 = $rootScope.Titlearray[151].ContentReganal;
            $scope.mtable5 = $rootScope.Titlearray[152].ContentReganal;
//            $scope.mtable2 = $rootScope.Titlearray[84].ContentReganal;
//            $scope.mtable3 = $rootScope.Titlearray[85].ContentReganal;
//            $scope.mtable4 = $rootScope.Titlearray[86].ContentReganal;
//            $scope.mtable5 = $rootScope.Titlearray[87].ContentReganal;
            $scope.mtable0 = "เครื่องจักรกล";
            $scope.model = "All";
            $scope.factorscore = "คะแนนปัจจัย";
            $scope.model3 = $rootScope.Titlearray[129].ContentReganal;
            $scope.footnote = $rootScope.Footarray[0].ContentReganal;
        }

        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        console.log(" height :", h);
        console.log(" width :", w);
        var margin = 300;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 250;
        } else if (w < 960 && w > 600) {
            margin = 200;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 150;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        console.log('margin', margin);


        $scope.$on('changeLanguage', function (event, data) {
            $scope.langcode = data;
            languagepara = [{"name": "english", "value": $scope.factor}, {"name": "moduleType", "value": "CSI"}];
            modelpara = [{"name": "english", "value": $scope.model}, {"name": "moduleType", "value": "CSI"}];

            service.mastersrv("Systemuserview-getName", languagepara).then(function (response) {
                if ($scope.langcode == 'EN') {
                    $scope.factorname = response.data[0].ContentEN;
                } else {
                    $scope.factorname = response.data[0].ContentReganal;
                }
            });
            service.mastersrv("Systemuserview-getName", modelpara).then(function (response) {
                if ($scope.langcode == 'EN') {
                    $scope.modelname = response.data[0].ContentEN + " - ";
                } else {
                    $scope.modelname = response.data[0].ContentReganal + " - ";
                }
                if ($scope.model == "All") {
                    $scope.modelname = " ";
                }

            });
            $scope.filterchange();
            $scope.botgraph();
        });

        $scope.$on('changeyear', function (event, data) {
            $scope.year = data;
            $scope.biannual = "All";
            $scope.month = "All";
            $scope.filterchange();
            $scope.botgraph();
        });

        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual = data;
            $scope.month = "All";
            $scope.filterchange();
            $scope.botgraph();
        });

        $scope.$on('changemonth', function (event, data) {
            $scope.month = data;
            $scope.filterchange();
        });

        $scope.$on('changezone', function (event, data) {
            $scope.zone = data;
            $scope.region = "All";
            $scope.dealer = "All";
            $scope.filterchange();
            $scope.botgraph();
        });

        $scope.$on('changeregion', function (event, data) {
            $scope.region = data;
            $scope.dealer = "All";
            $scope.filterchange();
            $scope.botgraph();
        });

        $scope.$on('changemodel', function (event, data) {
            $scope.model = data;
            modelpara = [{"name": "english", "value": $scope.model}];
            $scope.botgraph();
        });

        $scope.$on('changemodelgraph', function (event, data) {
            $scope.model = data;
            modelpara = [{"name": "english", "value": $scope.model}];
            $scope.botgraph();
        });

        $scope.$on('changefactor', function (event, data) {
            $scope.factor = data;
            languagepara = [{"name": "english", "value": $scope.factor}, {"name": "moduleType", "value": "CSI"}];
            service.mastersrv("Systemuserview-getName", languagepara).then(function (response) {
                if ($scope.langcode == 'EN') {
                    $scope.factorname = response.data[0].ContentEN;
                } else {
                    $scope.factorname = response.data[0].ContentReganal;
                }
            });
            $scope.botgraph();
        });
        $scope.$on('changeperiod', function (event, data) {
            $scope.period = data;
            $scope.botgraph();
            $scope.year = "All";
        });
        //////////////////////////////////////////////////////////////

        $("#modelexportCS").click(function () {
            $("#table").table2excel({
                // exclude CSS class
                exclude: ".noExl",
                filename: "modelTableDownload"
            });
        });


        var botparameters = [];

        botparameters = [{"name": "period", "value": $scope.period}, {"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}, {"name": "factor", "value": $scope.factor}];
        service.csi_filterfunction();

        parameters = [
            {"name": "langCode", "value": $scope.langcode},
            {"name": "zone", "value": encodeURIComponent($scope.zone)},
            {"name": "factor", "value": $scope.factor},
            {"name": "langCode", "value": $scope.langcode},
            {"name": "region", "value": encodeURIComponent($scope.region)},
            {"name": "year", "value": $scope.year},
            {"name": "biannual", "value": $scope.biannual},
            {"name": "month", "value": $scope.month}
        ];

        $scope.graphs = function () {

            if ($scope.langcode == 'EN') {
                console.log("titlearray");
                console.log($rootScope.Titlearray);
                $scope.model1 = $rootScope.Titlearray[116].ContentEN;
                $scope.model2 = $rootScope.Titlearray[117].ContentEN;
                $rootScope.legendforselectionb = "Best Overall";
                $rootScope.legendforselectionw = "Worst Overall";
//                    $scope.mtable1=$rootScope.Titlearray[120].ContentEN+'(26%)';
//                    $scope.mtable2=$rootScope.Titlearray[121].ContentEN+'(15%)';
//                    $scope.mtable3=$rootScope.Titlearray[122].ContentEN+'(12%)';
//                    $scope.mtable4=$rootScope.Titlearray[123].ContentEN+'(18%)';
//                    $scope.mtable5=$rootScope.Titlearray[124].ContentEN+'(29%)';

//                $scope.mtable1 = "Service<br>Initiation<br>(25%)";//$rootScope.Titlearray[149].ContentEN;
////                $scope.mtable2 = $rootScope.Titlearray[145].ContentEN;
//                $scope.mtable3 = "Service<br>Engineer/ Technician<br>(25%)";//$rootScope.Titlearray[150].ContentEN;
//                $scope.mtable4 = "Process<br>After Service<br>(25%)";//$rootScope.Titlearray[151].ContentEN;
//                $scope.mtable5 = "Service<br>Quality<br>(25%)";//$rootScope.Titlearray[152].ContentEN;
                $scope.mtable1 = $rootScope.Titlearray[149].ContentEN;
                $scope.mtable3 = $rootScope.Titlearray[150].ContentEN;
                $scope.mtable4 = $rootScope.Titlearray[151].ContentEN;
                $scope.mtable5 = $rootScope.Titlearray[152].ContentEN;
                $scope.mtable0 = "Product";
                $scope.model3 = $rootScope.Titlearray[129].ContentEN;
                $scope.footnote = $rootScope.Footarray[0].ContentEN;
                $scope.footnotetitle = $rootScope.Footarray[0].ContentEN;
                  $scope.trendFootnotetitle = $rootScope.Footarray[19].ContentEN;
            } else {
                $scope.model1 = $rootScope.Titlearray[116].ContentReganal;
                $scope.model2 = $rootScope.Titlearray[117].ContentReganal;
                $rootScope.legendforselectionb = "ดีที่สุดจากทั่วประเทศ";
                $rootScope.legendforselectionw = "แย่ที่สุดจากทั่วประเทศ";
//                    $scope.mtable1=$rootScope.Titlearray[120].ContentReganal+'(26%)';
//                    $scope.mtable2=$rootScope.Titlearray[121].ContentReganal+'(15%)';
//                    $scope.mtable3=$rootScope.Titlearray[122].ContentReganal+'(12%)';
//                    $scope.mtable4=$rootScope.Titlearray[123].ContentReganal+'(18%)';
//                    $scope.mtable5=$rootScope.Titlearray[124].ContentReganal+'(29%)';
//                $scope.mtable2 = $rootScope.Titlearray[83].ContentReganal;
                $scope.mtable1 = $rootScope.Titlearray[149].ContentReganal;
                $scope.mtable3 = $rootScope.Titlearray[150].ContentReganal;
                $scope.mtable4 = $rootScope.Titlearray[151].ContentReganal;
                $scope.mtable5 = $rootScope.Titlearray[152].ContentReganal;
//                $scope.mtable1 = "การเริ่มต้น ให้บริการ<br>(25%)";//$rootScope.Titlearray[149].ContentReganal;
//                $scope.mtable3 = "ิศวกร/ช่าง ที่ให้บริการ<br>(25%)";//$rootScope.Titlearray[150].ContentReganal;
//                $scope.mtable4 = "ขั้นตอน หลังบริการ<br>(25%)";//$rootScope.Titlearray[151].ContentReganal;
//                $scope.mtable5 = "คุณภาพ งานบริการ<br>(25%)";//$rootScope.Titlearray[152].ContentReganal;
                $scope.mtable0 = "เครื่องจักรกล";
                $scope.model3 = $rootScope.Titlearray[129].ContentReganal;
                $scope.footnote = $rootScope.Footarray[0].ContentReganal;
                $scope.footnotetitle = $rootScope.Footarray[0].ContentReganal;
                  $scope.trendFootnotetitle = $rootScope.Footarray[19].ContentReganal;
            }



            //        CSIModelAnalysis-getModelCSIScores 1
            service.mastersrv("CSIModelAnalysis-getModelCSIScores", parameters).then(function (response) {
                $('#leftgraph').highcharts(service.modelbarchart('bar', '#2979FF ', response.data, 120, 40, 0, 'Avg CSI'));
            });
            service.mastersrv("CSIModelAnalysis-getModelCSIScores", parameters).then(function (response) {
                $('#leftgraph1').highcharts(service.modelbarchart('bar', '#2979FF ', response.data, 120, 20, 0, 'Avg CSI'));
            });

            //        CSIModelAnalysis-getModelFactorCSIScores  2
            service.mastersrv("CSIModelAnalysis-getModelFactorCSIScores", parameters).then(function (response) {


                $scope.FSmodeltable = {
                    id: "modeltable",
                    view: "datatable",
//                    height: 220,

                    //                    headerRowHeight: 60,
                    fixedRowHeight: false,

                    scroll: 'y',
                    position: "flex",

                    select: "row",
                    hover: "myhover",
                    css: "my_style",

                    tooltip: true,
                    columns: [

//                        {id: "model", css: "columnstyle", header: {text: $scope.mtable0}, adjust: 'true', fillspace: true,minwidth: 250},
                        {id: "model", css: "columnstyle", header: {text: $scope.mtable0}, fillspace: false, minwidth: 100, width: 200},
                        {id: "ServiceInitiation", header: {text: $scope.mtable1, css: 'multiline'}, cssFormat: mark_serviceinitiation, adjust: 'true', width: 60, fillspace: true},
//                        {id: "ServiceAdvisor", header: {text: $scope.mtable2, css: 'multiline'}, cssFormat: mark_serviceadvisor, adjust: 'true'},
                        {id: "ServiceEngineer", header: {text: $scope.mtable3, css: 'multiline'}, cssFormat: ServiceEngineercolor, adjust: 'true', width: 130, fillspace: true},
                        {id: "VehiclePickup", header: {text: $scope.mtable4, css: 'multiline'}, cssFormat: mark_vehiclepickup, adjust: 'true', width: 110, fillspace: true},
                        {id: "ServiceQuality", header: {text: $scope.mtable5, css: 'multiline'}, cssFormat: mark_servicequality, adjust: 'true', width: 60, fillspace: true}
                    ],
                    data: response.data,
                    resizeColumn: true,

                };
            });


            function mark_serviceinitiation(value, config) {
                if (config["ServiceInitiationcolor"] != "White")
                {
                    console.log({"background-color": config["ServiceInitiationcolor"], "color": '#FFFFFF !important'});

                    return {"background-color": config["ServiceInitiationcolor"], "color": '#FFFFFF !important'};
                } else
                {
                    return {"background-color": config["ServiceInitiationcolor"]};
                }
                return value;

            }


            function mark_serviceadvisor(value, config) {
                if (config["ServiceAdvisorcolor"] != "White")
                {
                    console.log({"background-color": config["ServiceAdvisorcolor"], "color": '#FFFFFF !important'});

                    return {"background-color": config["ServiceAdvisorcolor"], "color": '#FFFFFF !important'};
                } else
                {
                    return {"background-color": config["serviceadvisorcolor"]};
                }
                return value;

            }
            function ServiceEngineercolor(value, config) {
                if (config["ServiceEngineercolor"] != "White")
                {
                    console.log({"background-color": config["ServiceEngineercolor"], "color": '#FFFFFF !important'});

                    return {"background-color": config["ServiceEngineercolor"], "color": '#FFFFFF !important'};
                } else
                {
                    return {"background-color": config["ServiceEngineercolor"]};
                }

                return value;
            }
            function mark_vehiclepickup(value, config) {
                if (config["VehiclePickupcolor"] != "White")
                {
                    console.log({"background-color": config["VehiclePickupcolor"], "color": '#FFFFFF !important'});

                    return {"background-color": config["VehiclePickupcolor"], "color": '#FFFFFF !important'};
                } else
                {
                    return {"background-color": config["VehiclePickupcolor"]};
                }

                return value;
            }
            function mark_servicequality(value, config) {
                if (config["ServiceQualitycolor"] != "White")
                {
                    console.log({"background-color": config["ServiceQualitycolor"], "color": '#FFFFFF !important'});

                    return {"background-color": config["ServiceQualitycolor"], "color": '#FFFFFF !important'};
                } else
                {
                    return {"background-color": config["ServiceQualitycolor"]};
                }
                return value;

            }
//         $scope.botrender();
//            
        };


        $scope.botrender = function () {

            //        CSIModelAnalysis-getModelFactorTrend 3
            service.mastersrv("CSIModelAnalysis-getModelFactorTrend", botparameters).then(function (response) {

                $('#botgraph').highcharts(service.barchartmiddle('column', '#92D400 ', response.data.data, 0, 40, 0, 'Model Score', 12, 1, 1000));
                $('#botgraph1').highcharts(service.barchartmiddle('column', '#92D400 ', response.data.data, 0, 40, 0, 'Model Score', 12, 1, 1000));

                modelpara = [{"name": "english", "value": $scope.model}, {"name": "moduleType", "value": "CSI"}];
                service.mastersrv("Systemuserview-getName", modelpara).then(function (response) {
                    console.log('response response[0] modelgraph', response.data[0].ContentReganal);

                    if ($scope.langcode == 'EN') {
                        if ($scope.model == "All") {
                            $scope.modelname = " ";
                        } else {
                            $scope.modelname = response.data[0].ContentEN + " - ";
                        }
                    } else {
                        if ($scope.model == "All") {
                            $scope.modelname = " ";
                        } else {
                            $scope.modelname = response.data[0].ContentReganal + " - ";
                        }
                    }
                });




            });
        };

//{"name": "langCode", "value": $scope.langcode},
        $scope.filterchange = function () {
            parameters = [{"name": "period", "value": $scope.period}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "langCode", "value": $scope.langcode}, {"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "province", "value": $scope.province}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "city", "value": encodeURIComponent($scope.city)}];
            $scope.graphs();
        };

        $scope.botgraph = function () {
            botparameters = [{"name": "period", "value": $scope.period}, {"name": "area", "value": $scope.area}, {"name": "langCode", "value": $scope.langcode}, {"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "province", "value": $scope.province}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "model", "value": $scope.model}, {"name": "factor", "value": $scope.factor}];
            $scope.botrender();
        };




    }
})();