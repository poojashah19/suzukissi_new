(function() {
    'use strict';

    angular
        .module('app.module2.page5')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

          $stateProvider
          .state('triangular.csipage5', {
            url: '/csipage5',
            
             views: {
                '': {
                   templateUrl: 'app/module2/page5/page5.tmpl.html',
            controller: 'm2page5Controller',
            controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module2/page5/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        });

//         triMenuProvider.removeMenu('triangular.dealercsipage35'); 
//         triMenuProvider.addMenu({
//                   name: 'Other Diagnostics',
//                    icon: 'zmdi zmdi-assignment-o',
//                    type: 'link',
//                    priority: 1.6,
//                     state: 'triangular.csipage5',
//                });  
                

    }
})();