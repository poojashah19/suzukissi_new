(function () {
    'use strict';

    angular
            .module('app.module2.page5')
            .controller('m2page5Controller', m2page5Controller);

    /* @ngInject */
    function m2page5Controller($scope, $state, service, $http, $rootScope) {
        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776", "#92D400", "#00A1DE", "#72C7E7", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.hideyear = false;
        $rootScope.hidebiannual = false;
        $rootScope.hidemonth = false;
        $rootScope.hidezone = false;
        $rootScope.hideregion = false;
        $rootScope.hidemodel = true;
        $rootScope.hidefactor = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.snaphidezone = true;
        $rootScope.snaphideregion = true;
        $rootScope.dealerhide = false;
        $rootScope.dealersnaphide = true;
        $rootScope.redalertdealerhide = true;
        $rootScope.pagenumber = '7';
        $rootScope.starlabelhide = true;
        $rootScope.priorityhide = true;
        $rootScope.summaryRefresh = 1;
        $scope.zone = $rootScope.Filterarray.zone[0].name;
        $scope.region = $rootScope.Filterarray.region[0].name;
        $scope.model = $rootScope.model;
        $scope.year = $rootScope.Filterarray.year[0].name;
        $scope.biannual = "All";
        $scope.month = $rootScope.Filterarray.month[1].name;
        $rootScope.dealerScoreDealer = $rootScope.Filterarray.dealer[1].name;
        $scope.langcode = $rootScope.language;

        $rootScope.dsnapshot = "not";
        $rootScope.redfeedback = "not";
        $rootScope.checkboxInitiliser();
        $rootScope.mainselectionhide = false;
        $rootScope.filterSelShow = true;
        $rootScope.periodhide = true;
        $rootScope.loyaltydealerhide = true;
        if ($scope.langcode == 'EN') {
            $scope.othertitle1 = $rootScope.Titlearray[61].ContentEN;
            $scope.page1 = "Page 1";
            $scope.page2 = "Page 2";
            $scope.page3 = "Page 3";
            $scope.Questions = $rootScope.Titlearray[62].ContentEN;
            $scope.footnote = $rootScope.Footarray[4].ContentEN;
            $scope.footnote =  $rootScope.Footarray[0].ContentEN;

            $scope.footer1 = "(1) Base: By Appointment";
            $scope.footer2 = "(2) Base: Vehicle ready on the same day of service";
            $scope.footer3 = "(3) Base: Contacted After Service";

        } else {
            $scope.othertitle1 = $rootScope.Titlearray[61].ContentReganal;
            $scope.page1 = "หน้า 1";
            $scope.page2 = "หน้า 2";
            $scope.page3 = "หน้า 3";
            $scope.Questions = $rootScope.Titlearray[62].ContentReganal;
            $scope.footnote = $rootScope.Footarray[4].ContentReganal;
            $scope.footnote += "<br>" + $rootScope.Footarray[0].ContentReganal;
            $scope.footer1 = "(1) หมายเหตุ: เฉพาะกลุ่มตัวอย่างที่ทำการนัดหมายล่วงหน้า";
            $scope.footer2 = "(2) หมายเหตุ: เฉพาะกลุ่มตัวอย่างที่ได้รับบริการเสร็จสิ้นภายในวันเดียวกัน";
            $scope.footer3 = "(3) หมายเหตุ: เฉพาะกลุ่มตัวอย่างที่ได้รับการติดต่อรับเข้ารับบริการ";

        }

        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        console.log(" height :", h);
        console.log(" width :", w);
        var margin = 300;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 250;
        } else if (w < 960 && w > 600) {
            margin = 200;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 150;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        console.log('margin', margin);



        $scope.$on('changeLanguage', function (event, data) {
            $scope.langcode = data;
            $scope.filterchange();
        });


        $scope.$on('changeyear', function (event, data) {
            $scope.year = data;
            $scope.biannual = "All";
            $scope.month = $rootScope.Filterarray.month[1].name;
            $scope.filterchange();
        });

        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual = data;
            $scope.month = $rootScope.Filterarray.month[1].name;
            $scope.filterchange();
        });

        $scope.$on('changemonth', function (event, data) {
            $scope.month = data;
            $scope.filterchange();
        });

        $scope.$on('changezone', function (event, data) {
            $scope.zone = data;
            $scope.region = $rootScope.Filterarray.region[0].name;
            $scope.dealer = "All";
            $scope.filterchange();
        });

        $scope.$on('changeregion', function (event, data) {
            $scope.region = data;
            $scope.dealer = "All";
            $scope.filterchange();
        });

        $scope.$on('changemodel', function (event, data) {
            $scope.model = data;
            $scope.filterchange();
        });

        $scope.$on('changedealer', function (event, data) {
            $scope.dealer = data;
            $scope.filterchange();
        });
        //////////////////////////////////////////////////////////////

        $("#otherexportCS").click(function () {
            $("#table22").table2excel({
                // exclude CSS class
                exclude: ".noExl",
                filename: "otherTableDownload"
            });
        });

        var parameters = [];
//        $scope.year = $rootScope.Filterarray.year[$rootScope.Filterarray.year.length - 1];
        $scope.region = $rootScope.Filterarray.region[0].name;
        $scope.dealer = "All";
        $scope.model = $rootScope.model;
        service.csi_filterfunction();
        parameters = [{"name": "langCode", "value": $scope.langcode}, {"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "dealer", "value": $scope.dealer}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}];
        //////////////////////////////////////////////////////////////

        var marginp1;
        var marginp2;
        var marginp3;
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        console.log(" width :", w);

        if (w < 1400)
        {
            marginp2 = 80;
        } else
        {
            marginp2 = 76;
        }

        if (w > 1600) {
            marginp3 = null;
        } else
        {
            marginp3 = 90;
        }

        if (w > 1680)
        {
            marginp1 = null;
        } else
        {
            marginp1 = 80;
        }
        console.log('marginp1', marginp1);
        console.log('marginp2', marginp2);
        console.log('marginp3', marginp3);
//////////////////////////////////////////////////////////////
        $scope.graphs = function () {

            $scope.hideloader = false;
            if ($scope.langcode == 'EN') {
                $scope.othertitle1 = $rootScope.Titlearray[61].ContentEN;
                $scope.page1 = "Page 1";
                $scope.page2 = "Page 2";
                $scope.page3 = "Page 3";
                $scope.page4 = "Page 4";
                $scope.page5 = "Page 5";
                $scope.page6 = "Page 6";

                $scope.Questions = $rootScope.Titlearray[62].ContentEN;
                $scope.footer1 = "(1) Base: By Appointment";
                $scope.footer2 = "(2) Base: Vehicle ready on the same day of service";
                $scope.footer3 = "(3) Base: Contacted After Service";
                $scope.footnote = $rootScope.Footarray[0].ContentEN;
//                $scope.footnote =  $rootScope.Footarray[0].ContentEN;
                $scope.footnotetitle = $rootScope.Footarray[0].ContentEN;
//                $scope.footnotetitle =  $rootScope.Footarray[0].ContentEN;
                $scope.tableFootnote = $rootScope.Footarray[5].ContentEN;
                $scope.tableFootnote += "\n" + $rootScope.Footarray[6].ContentEN;
                $scope.tableFootnote += "\n" + $rootScope.Footarray[7].ContentEN;
                $scope.tableFootnote += "\n" + $rootScope.Footarray[8].ContentEN;
                $scope.tableFootnote += "\n" + $rootScope.Footarray[9].ContentEN;
                $scope.chartsFootnote = $rootScope.Footarray[10].ContentEN;
                $scope.chartsFootnote += "\n" + $rootScope.Footarray[11].ContentEN;
                $scope.chartsFootnote += "\n" + $rootScope.Footarray[12].ContentEN;
                $scope.chartsFootnote += "\n" + $rootScope.Footarray[13].ContentEN;
                $scope.chartsFootnote += "\n" + $rootScope.Footarray[14].ContentEN;
                $scope.chartsFootnote += "\n" + $rootScope.Footarray[15].ContentEN;
                $scope.chartsFootnote += "\n" + $rootScope.Footarray[16].ContentEN;
                $scope.chartsFootnote += "\n" + $rootScope.Footarray[17].ContentEN;
                $scope.chartsFootnote += "\n" + $rootScope.Footarray[18].ContentEN;
            } else {
                $scope.othertitle1 = $rootScope.Titlearray[61].ContentReganal;
                $scope.page1 = "หน้า 1";
                $scope.page2 = "หน้า 2";
                $scope.page3 = "หน้า 3";

                $scope.page4 = "หน้า 4";
                $scope.page5 = "หน้า 5";
                $scope.page6 = "หน้า 6";
                $scope.Questions = $rootScope.Titlearray[62].ContentReganal;
                $scope.footnote = $rootScope.Footarray[0].ContentReganal;
//                $scope.footnote += "<br>" + $rootScope.Footarray[0].ContentReganal;

                $scope.footnotetitle = $rootScope.Footarray[0].ContentReganal;
//                $scope.footnotetitle += "\n" + $rootScope.Footarray[0].ContentReganal;
                $scope.tableFootnote = $rootScope.Footarray[5].ContentReganal;
                $scope.tableFootnote += "\n" + $rootScope.Footarray[6].ContentReganal;
                $scope.tableFootnote += "\n" + $rootScope.Footarray[7].ContentReganal;
                $scope.tableFootnote += "\n" + $rootScope.Footarray[8].ContentReganal;
                $scope.tableFootnote += "\n" + $rootScope.Footarray[9].ContentReganal;
                 $scope.chartsFootnote = $rootScope.Footarray[10].ContentReganal;
                $scope.chartsFootnote += "\n" + $rootScope.Footarray[11].ContentReganal;
                $scope.chartsFootnote += "\n" + $rootScope.Footarray[12].ContentReganal;
                $scope.chartsFootnote += "\n" + $rootScope.Footarray[13].ContentReganal;
                $scope.chartsFootnote += "\n" + $rootScope.Footarray[14].ContentReganal;
                $scope.chartsFootnote += "\n" + $rootScope.Footarray[15].ContentReganal;
                $scope.chartsFootnote += "\n" + $rootScope.Footarray[16].ContentReganal;
                $scope.chartsFootnote += "\n" + $rootScope.Footarray[17].ContentReganal;
                $scope.chartsFootnote += "\n" + $rootScope.Footarray[18].ContentReganal;
                $scope.footer1 = "(1) หมายเหตุ: เฉพาะกลุ่มตัวอย่างที่ทำการนัดหมายล่วงหน้า";
                $scope.footer2 = "(2) หมายเหตุ: เฉพาะกลุ่มตัวอย่างที่ได้รับบริการเสร็จสิ้นภายในวันเดียวกัน";
                $scope.footer3 = "(3) หมายเหตุ: เฉพาะกลุ่มตัวอย่างที่ได้รับการติดต่อรับเข้ารับบริการ";
            }

            //       CSIOthers-getCSIOthersTable
            service.mastersrv("CSIOthers-getCSIOthersTable", parameters).then(function (response) {
                $scope.testdata = response.data.data;
                if (response.data) {
                    $scope.hideloader = true;
                }
                console.log('response.data', response.data);
                console.log('response.data.data', response.data.data);
                console.log('response.data', response.data.samplecount);

                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    console.log($scope.samplespaceless);
                }


                $scope.Heading1 = response.data.data[0].Heading1;
                $scope.Heading2 = response.data.data[0].Heading2;
                $scope.Heading3 = response.data.data[0].Heading3;
                $scope.Heading4 = response.data.data[0].Heading4;

                var Value1 = response.data.data[0].Heading1;
                var Value2 = response.data.data[0].Heading2;
                var Value3 = response.data.data[0].Heading3;
                var Value4 = response.data.data[0].Heading4;
                //             $scope.format1( $scope.testdata[i].Value1); $scope.format1( $scope.testdata[i].Value2); $scope.format1( $scope.testdata[i].Value3); $scope.format1( $scope.testdata[i].Value4);
            });
            console.log("parameters::::::::::::::::::::::")
            console.log(parameters)
            //CSIOthers-getCSIOthersCharts
            service.mastersrv("CSIOthers-getCSIOthersCharts", parameters).then(function (response) {
                $scope.response = response;

                //page1
                $scope.heading0 = response.data[0].headingname;
                $('#top01').highcharts(service.stackedgraph(response.data[0].headingdata, color));
                $scope.heading1 = response.data[1].headingname;
                $('#top02').highcharts(service.stackedgraph(response.data[1].headingdata, color));
                $scope.heading2 = response.data[2].headingname;
                $('#top03').highcharts(service.stackedgraph(response.data[2].headingdata, color));
                $scope.heading3 = response.data[3].headingname;
                $('#top04').highcharts(service.stackedgraph(response.data[3].headingdata, color));

                //page2
                $scope.heading4 = response.data[4].headingname;
                $('#top05').highcharts(service.stackedgraph(response.data[4].headingdata, color));
                $scope.heading5 = response.data[5].headingname;
                $('#top06').highcharts(service.stackedgraph(response.data[5].headingdata, color));

                //page3
                $scope.heading6 = response.data[6].headingname;
                $('#top07').highcharts(service.stackedgraph(response.data[6].headingdata, color));
                $scope.heading7 = response.data[7].headingname;
                $('#top08').highcharts(service.stackedgraph(response.data[7].headingdata, color));
                $scope.heading8 = response.data[8].headingname;
                $('#top09').highcharts(service.stackedgraph(response.data[8].headingdata, color));



                $scope.heading9 = response.data[9].headingname;
                $('#top10').highcharts(service.stackedgraph(response.data[9].headingdata, color));
                $scope.heading10 = response.data[10].headingname;
                $('#top11').highcharts(service.stackedgraph(response.data[10].headingdata, color));
                $scope.heading11 = response.data[11].headingname;
                $('#top12').highcharts(service.stackedgraph(response.data[11].headingdata, color));
                $scope.heading12 = response.data[12].headingname;
                $('#top13').highcharts(service.stackedgraph(response.data[12].headingdata, color));

                //page2
                $scope.heading13 = response.data[13].headingname;
                $('#top14').highcharts(service.stackedgraph(response.data[13].headingdata, color));
                $scope.heading14 = response.data[14].headingname;
                $('#top15').highcharts(service.stackedgraph(response.data[14].headingdata, color));

                //page3
                $scope.heading15 = response.data[15].headingname;
                $('#top16').highcharts(service.stackedgraph(response.data[15].headingdata, color));
                $scope.heading16 = response.data[16].headingname;
                $('#top17').highcharts(service.stackedgraph(response.data[16].headingdata, color));
                $scope.heading17 = response.data[17].headingname;
                $('#top18').highcharts(service.stackedgraph(response.data[17].headingdata, color));
                $scope.heading18 = response.data[18].headingname;
                $('#top19').highcharts(service.stackedgraph(response.data[18].headingdata, color));
                $scope.heading19 = response.data[19].headingname;
                $('#top20').highcharts(service.stackedgraph(response.data[19].headingdata, color));

                $scope.heading20 = response.data[20].headingname;
                $('#top21').highcharts(service.stackedgraph(response.data[20].headingdata, color));
                $scope.heading21 = response.data[21].headingname;
                $('#top22').highcharts(service.stackedgraph(response.data[21].headingdata, color));

                $scope.heading22= response.data[22].headingname;
                $('#top23').highcharts(service.stackedgraph(response.data[22].headingdata, color));
//                




                $('#top01x').highcharts(service.stackedgraph(response.data[6].headingdata, color, 80));
                $('#top02x').highcharts(service.stackedgraph(response.data[2].headingdata, color));
                $('#bot01x').highcharts(service.stackedgraph(response.data[7].headingdata, color));
                $('#bot02x').highcharts(service.stackedgraph(response.data[0].headingdata, color));
                $('#top11x').highcharts(service.stackedgraph(response.data[4].headingdata, color, marginp1));
                $('#top12x').highcharts(service.stackedgraph(response.data[5].headingdata, color));
                $('#bot11x').highcharts(service.stackedgraph(response.data[1].headingdata, color, marginp2));
                $('#bot12x').highcharts(service.stackedgraph(response.data[3].headingdata, color));
                $('#page0bx').highcharts(service.stackmultigraph(response.data[9].headingdata, color, 25, '11px', '.07'));
                $('#page0ax').highcharts(service.stackmultigraph(response.data[10].headingdata, color, 25, '11px', '.07'));
                $('#page0cx').highcharts(service.stackedgraph(response.data[8].headingdata, color));


            });

        };

        $scope.filterchange = function () {
            parameters = [{"name": "langCode", "value": $scope.langcode}, {"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}, {"name": "dealer", "value": $scope.dealer}];
            $scope.graphs();
        };

    }
})();