(function () {
    'use strict';

    angular
            .module('app.module2.page8')
            .controller('m2page8Controller', m2page8Controller);

    /* @ngInject */
    function m2page8Controller($scope, $state, service, $http, $rootScope) {
        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776", "#92D400", "#00A1DE", "#72C7E7", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.hideyear = false;
        $rootScope.hidebiannual = false;
        $rootScope.hidemonth = false;
        $rootScope.hidezone = false;
        $rootScope.hideregion = true;
        $rootScope.hidemodel = true;
        $rootScope.snaphidezone = true;
        $rootScope.snaphideregion = true;
        $rootScope.hidefactor = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.dealersnaphide = true;
        $rootScope.redalertdealerhide = true;
        $rootScope.dealerhide = true;
        $rootScope.pagenumber = '2';
        $rootScope.dsnapshot = "not";
        $rootScope.legendname = "Study Best";
        $rootScope.legendnamew = "Worst Score";
        $rootScope.starlabelhide = false;
        $rootScope.priorityhide = true;
        $rootScope.loyaltydealerhide = true;
        $rootScope.summaryRefresh = 1;
        $rootScope.modelsnap = "not";
        $scope.langcode = $rootScope.language;
        $scope.zone = "All";
        $scope.region = "All";
        $scope.model = $rootScope.model;
        $scope.year = $rootScope.year;
        $scope.biannual = "All";
        $scope.month = $rootScope.month;
        $rootScope.dealerScoreDealer = $rootScope.Filterarray.dealer[1].name;
        $scope.svgname = $rootScope.Filterarray.factor[0].value;
        $rootScope.redfeedback = "not";
        $rootScope.mainselectionhide = false;
        $rootScope.filterSelShow = true;
        $rootScope.periodhide = true;
        $rootScope.checkboxInitiliser();
        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        console.log(" height :", h);
        console.log(" width :", w);
        var margin = 300;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 250;
        } else if (w < 960 && w > 600) {
            margin = 200;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 150;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        console.log('margin', margin);


        if ($scope.langcode == 'EN') {
            $scope.factorl = "Factor";
            $scope.regfacttitle = $rootScope.Titlearray[11].ContentEN;
            $scope.byregiontitle = $rootScope.Titlearray[72].ContentEN;
            $scope.meantitle = $rootScope.Titlearray[73].ContentEN;
            $scope.percenttitle = $rootScope.Titlearray[74].ContentEN;
            $scope.factorscore = "Factor Score";
            $scope.attscore = 'Attribute Score';
            $scope.footnote = $rootScope.Footarray[2].ContentEN;
            $scope.footnote =  $rootScope.Footarray[0].ContentEN;
        } else {
            $scope.regfacttitle = $rootScope.Titlearray[11].ContentReganal;
            $scope.byregiontitle = $rootScope.Titlearray[72].ContentReganal;
            $scope.meantitle = $rootScope.Titlearray[73].ContentReganal;
            $scope.percenttitle = $rootScope.Titlearray[74].ContentReganal;
            $scope.factorscore = "คะแนนปัจจัย";
            $scope.attscore = 'คุณลักษณะ คะแนน';
            $scope.factorl = 'ปัจจัยต่างๆ';
            $scope.footnote = $rootScope.Footarray[2].ContentReganal;
            $scope.footnote += "<br>" + $rootScope.Footarray[0].ContentReganal;



        }


        $scope.state = "All";
        $scope.zone = "All";
        $scope.region = "All";
        $scope.model = "All";
        $scope.year = $rootScope.year;
        $scope.biannual = "All";
        $scope.month = $rootScope.Filterarray.month[1].name;
        $scope.legendname = "Study Best";
        $scope.legendnamew = "Worst Score";
        $scope.model = "All";
        $scope.city = "All";
        $scope.factor = $rootScope.Filterarray.factor[0].name;
        console.log()
        var parameters = [{"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}];

        service.csi_filterfunction();

        $scope.$on('changeLanguage', function (event, data) {
            $scope.langcode = data;
            $scope.filterchange();
        });

        $scope.$on('changeyear', function (event, data) {
            $scope.year = data;
            $scope.biannual = "All";
            $scope.month = "All";
            $scope.filterchange();
        });

        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual = data;
            $scope.month = "All";
            $scope.filterchange();
        });

        $scope.$on('changemonth', function (event, data) {
            $scope.month = data;
            $scope.filterchange();
        });

        $scope.$on('changezone', function (event, data) {
            $scope.zone = data;
            $scope.region = "All";
            $scope.dealer = "All";
            $scope.filterchange();
        });

        $scope.$on('changeregionone', function (event, data) {
            $scope.region = data;
            $scope.dealer = "All";
            $scope.filterchange1();
        });
        $scope.$on('changeregion', function (event, data) {
            $scope.region = data;
//            $scope.dealer = "All";
            $scope.filterchange();
        });

        $scope.$on('changemodel', function (event, data) {
            $scope.model = data;
            $scope.filterchange();
        });


        $scope.factorchanged = function (factor) {
            $scope.factor = factor;
//                    $scope.svgname=factor.value;
            $scope.filterchange();
        };
        //////////////////////////////////////////////////////////////



        $scope.graphs = function () {

            if ($scope.langcode == 'EN') {
                $scope.regfacttitle = $rootScope.Titlearray[11].ContentEN;
//                $scope.regfacttitle += " " + $scope.zone;
                $scope.byregiontitle = $rootScope.Titlearray[72].ContentEN;
                $scope.meantitle = $rootScope.Titlearray[73].ContentEN;
                $scope.percenttitle = $rootScope.Titlearray[74].ContentEN;
                $scope.factorscore = "Factor Score";
                $scope.attscore = 'Attribute Score';
                $scope.footnote = $rootScope.Footarray[4].ContentEN;
                $scope.footnote += "<br> " + $rootScope.Footarray[2].ContentEN;
                $scope.footnote =  $rootScope.Footarray[0].ContentEN;
                $rootScope.overallfootnote = angular.copy($scope.footnote);

                $scope.footnotetitle = $rootScope.Footarray[4].ContentEN;
                $scope.footnotetitle += "\n " + $rootScope.Footarray[2].ContentEN;
                $scope.footnotetitle =  $rootScope.Footarray[0].ContentEN;
                $rootScope.overallfootnotetitle = angular.copy($scope.footnotetitle);
            } else {
                $scope.regfacttitle = $rootScope.Titlearray[11].ContentReganal;
//                $scope.regfacttitle += " " + $scope.zone;
                $scope.byregiontitle = $rootScope.Titlearray[72].ContentReganal;
                $scope.meantitle = $rootScope.Titlearray[73].ContentReganal;
                $scope.percenttitle = $rootScope.Titlearray[74].ContentReganal;
                $scope.factorscore = "คะแนนปัจจัย";
                $scope.attscore = 'คุณลักษณะ คะแนน';

                $scope.footnote = $rootScope.Footarray[4].ContentReganal;
                $scope.footnote += "<br> " + $rootScope.Footarray[2].ContentReganal;
                $scope.footnote += "<br> " + $rootScope.Footarray[0].ContentReganal;
                $rootScope.overallfootnote = angular.copy($scope.footnote);

                $scope.footnotetitle = $rootScope.Footarray[4].ContentReganal;
                $scope.footnotetitle += "\n  " + $rootScope.Footarray[2].ContentReganal;
                $scope.footnotetitle += "\n  " + $rootScope.Footarray[0].ContentReganal;
                $rootScope.overallfootnotetitle = angular.copy($scope.footnotetitle);
            }


            //CSIRegional-getCSIFactorAverage
            service.mastersrv("CSIRegional-getCSIFactorAverage", parameters).then(function (response) {
                console.log('response.data.data', response.data.data);
                console.log('response.data', response.data.samplecount);
                $scope.svgname = response.data.data[0].Factor;
                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    console.log($scope.samplespaceless);
                }

                if (response.data.data.length == 0) {
                    response.data.data = [{"bartitle": "100%"}, {"Score": "0"}];
                }
                $scope.svgvalue = response.data.data[0].Score;
//                            $scope.svgname=response.data.data[0].Factor;
            });


            //CSIRegional-getCSIRegionalFactorAverage
            service.mastersrv("CSIRegional-getCSIRegionalFactorAverage", parameters).then(function (response) {
                $('#barlinecontainer').highcharts(service.regplainbar('column', color, response.data, $scope.factorscore, 0, 60, .3, [0], 8, true));
                $('#barlinecontainer1').highcharts(service.regplainbar('column', color, response.data, $scope.factorscore, 0, 60, .3, [0], 8, true));


            });


            //CSIRegional-getCSIRegionalAttributeAverage
            service.mastersrv("CSIRegional-getCSIRegionalAttributeAverage", parameters).then(function (response) {
                $('#regbot1').highcharts(service.barplainbar('bar', ['#2979FF'], response.data, $scope.attscore, 230, 35, 0, [0], 5, false, " "));
                $('#regbot11').highcharts(service.barplainbar('bar', ['#2979FF'], response.data, $scope.attscore, 150, 35, 0, [0], 5, false, " "));
                $rootScope.legendforselectionb = response.data[0].scatter;
                $rootScope.legendforselectionw = response.data[0].scatter1;
            });


            //CSIRegional-getCSIDissatisfied
            service.mastersrv("CSIRegional-getCSIDelighted", parameters).then(function (response) {
                $('#regbot2').highcharts(service.barplainbar('bar', ['#2979FF'], response.data, $scope.attscore, 230, 35, 0, [0], 5, false, "%"));
                $('#regbot21').highcharts(service.barplainbar('bar', ['#2979FF'], response.data, $scope.attscore, 150, 35, 0, [0], 5, false, "%"));

            });
        };

        $scope.graphs1 = function () {

            if ($scope.langcode == 'EN') {
                $scope.regfacttitle = $rootScope.Titlearray[11].ContentEN;
                $scope.byregiontitle = $rootScope.Titlearray[72].ContentEN;
                $scope.meantitle = $rootScope.Titlearray[73].ContentEN;
                $scope.percenttitle = $rootScope.Titlearray[74].ContentEN;
                $scope.factorscore = "Factor Score";
                $scope.attscore = 'Attribute Score';
                $scope.footnote = $rootScope.Footarray[1].ContentEN;
            } else {
                $scope.regfacttitle = $rootScope.Titlearray[11].ContentReganal;
                $scope.byregiontitle = $rootScope.Titlearray[72].ContentReganal;
                $scope.meantitle = $rootScope.Titlearray[73].ContentReganal;
                $scope.percenttitle = $rootScope.Titlearray[74].ContentReganal;
                $scope.factorscore = "คะแนนปัจจัย";
                $scope.attscore = 'คุณลักษณะ คะแนน';
                $scope.footnote = $rootScope.Footarray[1].ContentReganal;
            }


            //CSIRegional-getCSIFactorAverage
            service.mastersrv("CSIRegional-getCSIFactorAverage", parameters).then(function (response) {
                console.log('response.data.data', response.data.data);
                console.log('response.data', response.data.samplecount);
                $scope.svgname = response.data.data[0].Factor;
                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    console.log($scope.samplespaceless);
                }

                if (response.data.data.length == 0) {
                    response.data.data = [{"bartitle": "100%"}, {"Score": "0"}];
                }
                $scope.svgvalue = response.data.data[0].Score;
//                            $scope.svgname=response.data.data[0].Factor;
            });


            //CSIRegional-getCSIRegionalFactorAverage
//                  service.mastersrv("CSIRegional-getCSIRegionalFactorAverage",parameters).then(function (response) {
//                               $('#barlinecontainer').highcharts(service.regplainbar('column',color,response.data,$scope.factorscore,0,60,.3,[0],8,true));
//                                  $('#barlinecontainer1').highcharts(service.regplainbar('column',color,response.data,$scope.factorscore,0,60,.3,[0],8,true));
//                                  
//                                  
//                  });


            //CSIRegional-getCSIRegionalAttributeAverage
            service.mastersrv("CSIRegional-getCSIRegionalAttributeAverage", parameters).then(function (response) {
                $('#regbot1').highcharts(service.barplainbar('bar', ['#2979FF'], response.data, $scope.attscore, 170, 35, 0, [0], 5, false, " "));
                $('#regbot11').highcharts(service.barplainbar('bar', ['#2979FF'], response.data, $scope.attscore, 150, 35, 0, [0], 5, false, " "));
                $rootScope.legendforselectionb = response.data[0].scatter;
                $rootScope.legendforselectionw = response.data[0].scatter1;
            });


            //CSIRegional-getCSIDissatisfied
            service.mastersrv("CSIRegional-getCSIDelighted", parameters).then(function (response) {
                $('#regbot2').highcharts(service.barplainbar('bar', ['#2979FF'], response.data, $scope.attscore, 170, 35, 0, [0], 5, false, "%"));
                $('#regbot21').highcharts(service.barplainbar('bar', ['#2979FF'], response.data, $scope.attscore, 150, 35, 0, [0], 5, false, "%"));

            });
        };

        $scope.isSemi = false;
        $scope.getStyle = function () {

            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '50%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': '36px'
            };
        };

        $scope.getStyle1 = function () {

            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '50%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': '36px'
            };
        };




        $scope.filterchange = function () {
            parameters = [{"name": "langCode", "value": $scope.langcode}, {"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}, {"name": "factor", "value": $scope.factor}];
            $scope.graphs();
        };


        $scope.filterchange1 = function () {
            parameters = [{"name": "langCode", "value": $scope.langcode}, {"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}, {"name": "factor", "value": $scope.factor}];
            $scope.graphs1();
        };


    }
})();