(function () {
    'use strict';

    angular
            .module('app.module2.page13')
            .controller('m2page13Controller', m2page13Controller);

    /* @ngInject */
    function m2page13Controller($scope, service, $state, $http, $mdDialog, $rootScope) {
        $rootScope.chartloadcount = 0;
        $rootScope.lockLeft = true;
        var color = ["#002776", "#92D400", "#00A1DE", "#72C7E7", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.hideyear = false;
        $rootScope.hidebiannual = false;
        $rootScope.hidemonth = false;
        $rootScope.hidezone = true;
        $rootScope.hideregion = true;
        $rootScope.hidemodel = true;
        $rootScope.hidefactor = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.redalertdealerhide = true;
//        $rootScope.mainselectionhide=true;
        $rootScope.mainselectionhide = false;
        $rootScope.filterSelShow = false;
        $rootScope.dealersnaphide = true;
        $rootScope.dealerhide = true;
        $rootScope.loyaltydealerhide = true;
        $rootScope.pagenumber = '1';
        $rootScope.starlabelhide = true;
        $rootScope.legendname = "Study Best";
        $rootScope.legendnamew = "Worst Score";
        $rootScope.priorityhide = true;
        $scope.show4 = false;
        $scope.show3 = true;

        $scope.langcode = $rootScope.language;
        $rootScope.modelsnap = "not";
        $scope.zone = "Study Total";
        $scope.region = "Study Total";
        $scope.model = "Study Total";
//        $scope.year='2016';
        $scope.year = $rootScope.year;

        $scope.biannual = $rootScope.biannual;
        $scope.month = $rootScope.month;
        $scope.legendname = "Study Best";
        $scope.legendnamew = "Worst Score";
//        $rootScope.dtwave=$scope.waves;
        $rootScope.dealerScoreDealer = '$rootScope.Filterarray.dealer[1].name;';
        $rootScope.periodhide = true;
        $rootScope.tabhide = true;
        $rootScope.$broadcast("tabindex", 1);

        if (!$rootScope.$$listenerCount['enabledownload']) {
            $rootScope.$on("enabledownload", function (event, data) {
                $scope.chartcount = data;

                $scope.printdialog();
            });
        }
        $scope.$on('changeyear', function (event, data) {
            $scope.year = data;

            $scope.show4 = false;
            $scope.show3 = true;
            $scope.biannual = "Study Total";
            $scope.month = "Study Total";

            $scope.yearscore();
        });

        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual = data;
            $scope.show4 = false;
            $scope.show3 = true;
            $scope.month = "Study Total";

            $scope.biannualscore();
        });

        $scope.$on('changemonth', function (event, data) {
            $scope.month = data;
            $scope.show4 = true;
            $scope.show3 = false;

            $scope.monthscore();
        });






        service.mastersrv("Systemuserview-getRgionDealers", parameters).then(function (response) {
            $scope.dealerrecords = response.data;
            var download = " <span class='fa-download webix_icon'>&nbsp;&nbsp;Download</span>";

            $scope.dalertable = {
                id: "dealerlisttable",
                view: "datatable",
                fixedRowHeight: false,
                headerRowHeight: 45,
                rowLineHeight: 30,
                rowHeight: 30,
                scroll: 'y',
                position: "flex",

                select: "row",
                css: "my_style",
                tooltip: true,
                columns: [
                    {id: "dealer", header: {text: 'Dealer'}, adjust: 'true', fillspace: true},
                    {id: "region", header: {text: 'Region'}, adjust: 'true', minWidth: 125},
                    {id: "PDF Download", width: 200, template: download}
                ],
                data: $scope.dealerrecords,
                resizeColumn: false,

                onClick: {
                    "webix_icon": function (e, id) {

                        $scope.dealer = this.getItem(id).dealer;
                        $scope.region = this.getItem(id).region;
                        $scope.dealershowpopup();

//         webix.confirm("PDF is Downloading", function(action){
////         $scope.dealershowpopup();
//         
//         if(action === true) {
//         	this.getParentView();
//           // here this refers to window.
//         }	
//         
//		});
//
//webix.modalbox({
////    title:"Custom title",
////    buttons:["Yes", "No", "Maybe"],
//    text:"<h3>Downloading file,one moment please...</h3> <img src='processing.gif'  />",
//    width:500,
//    
//});

                    }}

            };


        });



        $scope.printdialog = function () {


            $rootScope.imageurls = [];
            domtoimage.toJpeg(document.getElementById('summary'), {quality: 0.95})
                    .then(function (summaryurl) {



                        $rootScope.imageurls.push(summaryurl);
                        domtoimage.toJpeg(document.getElementById('regionanalysis'), {quality: 0.95})
                                .then(function (regionanalysisUrl) {
                                    $rootScope.imageurls.push(regionanalysisUrl);


                                    domtoimage.toJpeg(document.getElementById('trendanalysis'), {quality: 0.95})
                                            .then(function (trendanalysisUrl) {
                                                $rootScope.imageurls.push(trendanalysisUrl);

                                                domtoimage.toJpeg(document.getElementById('priorityanalysis'), {quality: 0.95})
                                                        .then(function (priorityanalysisUrl) {


                                                            $rootScope.imageurls.push(priorityanalysisUrl);

                                                            domtoimage.toJpeg(document.getElementById('sopperformance'), {quality: 0.95})
                                                                    .then(function (sopperformanceUrl) {
                                                                        $rootScope.imageurls.push(sopperformanceUrl);



                                                                        domtoimage.toJpeg(document.getElementById('otherdiagnostics'), {quality: 0.95})
                                                                                .then(function (otherdiagnosticsUrl) {
                                                                                    $rootScope.imageurls.push(otherdiagnosticsUrl);
                                                                                    domtoimage.toJpeg(document.getElementById('otherdiagnostics2'), {quality: 0.95})
                                                                                            .then(function (otherdiagnosticsUrl2) {
                                                                                                $rootScope.imageurls.push(otherdiagnosticsUrl2);
                                                                                                domtoimage.toJpeg(document.getElementById('otherdiagnostics3'), {quality: 0.95})
                                                                                                        .then(function (otherdiagnosticsUrl3) {
                                                                                                            $rootScope.imageurls.push(otherdiagnosticsUrl3);

                                                                                                            domtoimage.toJpeg(document.getElementById('modelanalysis'), {quality: 0.95})
                                                                                                                    .then(function (modelanalysisUrl) {
                                                                                                                        $rootScope.imageurls.push(modelanalysisUrl);

                                                                                                                        domtoimage.toJpeg(document.getElementById('loyalityadvocacy'), {quality: 0.95})
                                                                                                                                .then(function (loyalityadvocacyUrl) {
                                                                                                                                    $rootScope.imageurls.push(loyalityadvocacyUrl);

                                                                                                                                    $mdDialog.show({

                                                                                                                                        templateUrl: 'app/module2/page13/print.html',

                                                                                                                                        resolve: {
                                                                                                                                            webixmodal: (function () {
                                                                                                                                                return function () {
                                                                                                                                                    return $scope.dealerbox;
                                                                                                                                                };
                                                                                                                                            })()
                                                                                                                                        },

                                                                                                                                        locals: {dataToPass: $rootScope.imageurls},
                                                                                                                                        controllerAs: 'ctrl',
                                                                                                                                        controller: mdDialogCtrl,
                                                                                                                                        clickOutsideToClose: true,
                                                                                                                                        fullscreen: true // Only for -xs, -sm breakpoints.
                                                                                                                                    });


                                                                                                                                });


                                                                                                                    });
                                                                                                        });
                                                                                            });
                                                                                });






                                                                    });




                                                        });


                                            });


                                });


                    });


        };






        $scope.cancel = function () {


            $mdDialog.cancel();
        };
        if ($scope.langcode == 'EN') {
            $scope.nationaltitle = $rootScope.Titlearray[1].ContentEN;
            $scope.regionaltitle = $rootScope.Titlearray[2].ContentEN;
            $scope.factortitle = $rootScope.Titlearray[3].ContentEN;
            $scope.top5title = $rootScope.Titlearray[9].ContentEN;
            $scope.delightedtitle = $rootScope.Titlearray[10].ContentEN;
            $scope.footnote = $rootScope.Footarray[1].ContentEN;
            $scope.factorscore = "Factor Score";

        } else {
            $scope.nationaltitle = $rootScope.Titlearray[1].ContentReganal;
            $scope.regionaltitle = $rootScope.Titlearray[2].ContentReganal;
            $scope.factortitle = $rootScope.Titlearray[3].ContentReganal;
            $scope.top5title = $rootScope.Titlearray[9].ContentReganal;
            $scope.delightedtitle = $rootScope.Titlearray[10].ContentReganal;
            $scope.footnote = $rootScope.Footarray[1].ContentReganal;
            $scope.factorscore = "คะแนนปัจจัย";
        }



        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        console.log(" height :", h);
        console.log(" width :", w);
        var margin = 300;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 250;
        } else if (w < 960 && w > 600) {
            margin = 200;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 150;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        console.log('margin', margin);
        //////////////////////////////////////////////////////////////

        var parameters = [];

//        $rootScope.dealerScoreDealer = $rootScope.Filterarray.dealer[0];
        $rootScope.mapState = 0;

        parameters = [{"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}];
        service.csi_filterfunction();
        $scope.getStyle = function () {
            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '50%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': $scope.radius / 3.5 + 'px'
            };
        };
        $scope.reggetStyle = function () {
            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '50%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': '36px'
            };
        };
        $scope.getStyle1 = function () {
            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '50%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': $scope.radius / 3.5 + 'px'
            };
        };





        var myData = [];
        $scope.printdata = function () {
            console.log("map data printing...");
            console.log("map data printed");
            $scope.mapData.forEach(function (obj2) {
                myData.push({region: obj2.region, score: obj2.value, repondents: obj2.repondents});
            });
            console.log("mapdata", myData);
            return myData;
        };


        $scope.maprender = function () {

            var latlong = {};


            latlong["Bangkok & Greater"] = {"latitude": 13.756331, "longitude": 100.501765};//Bangkok & Greater
            latlong["Central and East"] = {"latitude": 15.118601, "longitude": 104.322009}; //Central and East
            latlong["Northeast"] = {"latitude": 17.364697, "longitude": 102.815892};//Northeast
            latlong["North"] = {"latitude": 18.787747, "longitude": 98.993128}; //North
            latlong["South"] = {"latitude": 7.008647, "longitude": 100.474688};//South



            var mapcolor = ["#92D400", "#33B4E5",
                "#4200d4",
                "#c3d327", "#3C8A2E",
                "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];

            var map;
            var minBulletSize = 30;
            var maxBulletSize = 60;
            var min = Infinity;
            var max = -Infinity;

            for (var i = 0; i < $scope.mapData.length; i++) {
                var value = $scope.mapData[i].value;
                if (value < min) {
                    min = value;
                }
                if (value > max) {
                    max = value;
                }


            }

            if (min == max) {
                min = 0;
                max = value;
            }


            AmCharts.ready = function () {
                map = new AmCharts.AmMap();
                map.pathToImages = "http://www.amcharts.com/lib/3/images/";
                map.panEventsEnabled = false;
                map.dragMap = false;
                map.areasSettings = {
                    selectable: true,
                    unlistedAreasColor: "#EFEFEF",
                    unlistedAreasOutlineAlpha: .9,
                    unlistedAreasOutlineColor: "#7F7F7F"
                };
                map.imagesSettings.balloonText = "<span style='font-size:14px;' align='left'>[[title]]</span>";

                map.addListener("clickMapObject", function (event) {
                    console.log(event.mapObject.code);
                    console.log(event.mapObject.value);
                    if ($rootScope.mapState == 0) {
                        $scope.region = event.mapObject.code;
                        $scope.filterchange();
                        $rootScope.mapState = 1;
                    } else {
                        $scope.region = "Study Total";
                        $scope.filterchange();
                        $rootScope.mapState = 0;
                    }


                });


                var dataProvider = {
                    mapVar: AmCharts.maps.thailandHigh,
                    //                       
                    images: []
                };

                map.zoomControl = {
                    panControlEnabled: false,
                    zoomControlEnabled: false,
                    homeButtonEnabled: false
                };


                // it's better to use circle square to show difference between values, not a radius
                var maxSquare = maxBulletSize * maxBulletSize * 2 * Math.PI;
                var minSquare = minBulletSize * minBulletSize * 2 * Math.PI;


                // create circle for each country
                for (var i = 0; i < $scope.mapData.length; i++) {
                    var dataItem = $scope.mapData[i];
                    var value = dataItem.value;
                    var datacolor = mapcolor[i];

                    // calculate size of a bubble   

                    var square = ((value - min) / (max - min) * (maxSquare - minSquare)) + minSquare;
                    if (square < minSquare) {
                        square = minSquare;
                    }
                    var size = Math.sqrt(square / (Math.PI * 2));
                    var id = dataItem.code;


                    dataProvider.images.push({
                        type: "bubble",
                        width: size,
                        height: size,
                        color: datacolor,
                        longitude: latlong[id].longitude,
                        latitude: latlong[id].latitude,
                        label: (value),
                        alpha: .30,
                        code: dataItem.code,
                        labelPosition: "inside",
                        title: dataItem.name,
                        value: value,
                        rollOverScale: "1.3",
//                            outlineColor: "#104E8B",
                        outlineAlpha: ".5",
                        outlineThickness: "1",
                        selectable: true,
                        bringForwardOnHover: false,
                        selectedOutlineThickness: "5"

                    });
                }

                map.dataProvider = dataProvider;
                map.write("map");
            };


            AmCharts.ready1 = function () {
                map = new AmCharts.AmMap();
                map.pathToImages = "http://www.amcharts.com/lib/3/images/";
                map.panEventsEnabled = false;
                map.dragMap = false;
                map.areasSettings = {
                    selectable: true,
                    unlistedAreasColor: "#EFEFEF",
                    unlistedAreasOutlineAlpha: .9,
                    unlistedAreasOutlineColor: "#7F7F7F"
                };
                map.imagesSettings.balloonText = "<span style='font-size:14px;' align='left'>[[title]]</span>";

                map.addListener("clickMapObject", function (event) {
                    console.log(event.mapObject.code);
                    console.log(event.mapObject.value);
                    if ($rootScope.mapState == 0) {
                        $scope.region = event.mapObject.code;
                        $scope.filterchange();
                        $rootScope.mapState = 1;
                    } else {
                        $scope.region = "Study Total";
                        $scope.filterchange();
                        $rootScope.mapState = 0;
                    }


                });
                var dataProvider = {
                    mapVar: AmCharts.maps.thailandHigh,
                    images: []
                };
                map.zoomControl = {
                    panControlEnabled: false,
                    zoomControlEnabled: false,
                    homeButtonEnabled: false
                };
                var maxSquare = maxBulletSize * maxBulletSize * 2 * Math.PI;
                var minSquare = minBulletSize * minBulletSize * 2 * Math.PI;
                for (var i = 0; i < $scope.mapData.length; i++) {
                    var dataItem = $scope.mapData[i];
                    var value = dataItem.value;
                    var datacolor = mapcolor[i];
                    var square = ((value - min) / (max - min) * (maxSquare - minSquare)) + minSquare;
                    if (square < minSquare) {
                        square = minSquare;
                    }
                    var size = Math.sqrt(square / (Math.PI * 2));
                    var id = dataItem.code;
                    dataProvider.images.push({
                        type: "bubble",
                        width: size,
                        height: size,
                        color: datacolor,
                        longitude: latlong[id].longitude,
                        latitude: latlong[id].latitude,
                        label: (value),
                        alpha: .30,
                        code: dataItem.code,
                        labelPosition: "inside",
                        title: dataItem.name,
                        value: value,
                        rollOverScale: "1.3",
//                            outlineColor: "#104E8B",
                        outlineAlpha: ".5",
                        outlineThickness: "1",
                        selectable: true,
                        bringForwardOnHover: false,
                        selectedOutlineThickness: "5"
                    });
                }

                map.dataProvider = dataProvider;
                map.write("map1");
            };
            AmCharts.ready();
            AmCharts.ready1();
        };



        $scope.$on('changeregion', function (event, data) {
            $scope.region = data;
            $scope.area = "All";
            $scope.zone = "All";
            $scope.province = "All";
            $scope.city = "All";
            $scope.dealer = "All";

        });

        $scope.$on('changearea', function (event, data) {
            $scope.area = data;
            $scope.zone = "All";
            $scope.province = "All";
            $scope.city = "All";
            $scope.dealer = "All";

        });
        $scope.$on('changezone', function (event, data) {
            $scope.zone = data;
            $scope.province = "All";
            $scope.city = "All";
            $scope.dealer = "All";

        });
        $scope.$on('changeprovince', function (event, data) {
            $scope.province = data;
            $scope.city = "All";
            $scope.dealer = "All";

        });

        //////////////////////////////////////////////////////////////

        $scope.overallshowpopup = function () {

            $rootScope.chartloadcount = 0;
            $scope.dealerbox = webix.modalbox({
                title: "Downloading file,one moment please...",
//buttons:["Yes"],
                position: "center",
                text: "<img src='assets/images/processing.gif' width='300' />",
                width: 300,
                height: 400,

            });
            $mdDialog.show({
                controller: m2page13Controller,
                bindToController: true,
                templateUrl: 'app/module2/page13/oemdialog.html',
//      parent: angular.element(document.body),

                clickOutsideToClose: true,

                fullscreen: true // Only for -xs, -sm breakpoints.
            });


        };



        $scope.dealershowpopup = function () {

            $scope.passingdata = {"dealer": $scope.dealer, "region": $scope.region},
                    $rootScope.chartloadcount = 0;

            $scope.dealerbox = webix.modalbox({
                title: "Downloading file,one moment please...",
//  buttons:["Yes", "No"],
                position: "center",
                text: "<img src='assets/images/processing.gif' width='300' />",
                width: 300,
                height: 400,

            });

            $mdDialog.show({
                controller: dealerDialogCtrl,
                templateUrl: 'app/module2/page13/dealerdialog.html',
                clickOutsideToClose: true,

                resolve: {
                    webixmodal: (function () {
                        return function () {
                            return $scope.dealerbox;
                        };
                    })()
                },
                locals: {
                    items: $scope.passingdata
                },
                fullscreen: true // Only for -xs, -sm breakpoints.
            });

        }


        $scope.yearscore = function () {
            //CSISummary-getNationalCSIScores
            service.mastersrv("CSISummary-getNationalCSIScoresYear", parameters).then(function (response) {

                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    //                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    //                    console.log($scope.samplespaceless);
                }
                $scope.currmonth = response.data.data[0].Wave;
                $scope.svg1 = response.data.data[0].Score;
                $scope.prevmonth = response.data.data[1].Wave;
                $scope.svg2 = response.data.data[1].Score;
                $scope.prevquater = response.data.data[2].Wave;
                $scope.svg3 = response.data.data[2].Score;


            });
        };

        $scope.biannualscore = function () {
            //CSISummary-getNationalCSIScores
            service.mastersrv("CSISummary-getNationalCSIScoresBiannual", parameters).then(function (response) {

                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    //                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    //                    console.log($scope.samplespaceless);
                }
                $scope.currmonth = response.data.data[0].Wave;
                $scope.svg1 = response.data.data[0].Score;
                $scope.prevmonth = response.data.data[1].Wave;
                $scope.svg2 = response.data.data[1].Score;
                $scope.prevquater = response.data.data[2].Wave;
                $scope.svg3 = response.data.data[2].Score;



            });
        };

        $scope.monthscore = function () {
            //CSISummary-getNationalCSIScores
            service.mastersrv("CSISummary-getNationalCSIScoresMonth", parameters).then(function (response) {

                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    //                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    //                    console.log($scope.samplespaceless);
                }
                $scope.currmonth = response.data.data[0].Wave;
                $scope.svg1 = response.data.data[0].Score;
                $scope.prevmonth = response.data.data[1].Wave;
                $scope.svg2 = response.data.data[1].Score;
                $scope.currquater = response.data.data[2].Wave;
                $scope.svg3 = response.data.data[2].Score;
                $scope.prevquater = response.data.data[3].Wave;
                $scope.svg4 = response.data.data[3].Score;

            });
        };

        if ($rootScope.monthchanged == true) {
            $scope.show4 = true;
            $scope.show3 = false;
            $scope.monthscore();


        } else
        if ($rootScope.biannualchanged == true)
        {
            $scope.show4 = false;
            $scope.show3 = true;
            $scope.biannualscore();
        } else
        {
            $scope.show4 = false;
            $scope.show3 = true;
            $scope.yearscore();

        }

        $scope.summarygraphs = function () {

            if ($scope.langcode == 'EN') {
                $scope.nationaltitle = $rootScope.Titlearray[1].ContentEN;
                $scope.regionaltitle = $rootScope.Titlearray[2].ContentEN;
                $scope.factortitle = $rootScope.Titlearray[3].ContentEN;
                $scope.top5title = $rootScope.Titlearray[9].ContentEN;
                $scope.delightedtitle = $rootScope.Titlearray[10].ContentEN;
                $scope.footnote = $rootScope.Footarray[1].ContentEN;
                $scope.factorscore = "Factor Score";

            } else {
                $scope.nationaltitle = $rootScope.Titlearray[1].ContentReganal;
                $scope.regionaltitle = $rootScope.Titlearray[2].ContentReganal;
                $scope.factortitle = $rootScope.Titlearray[3].ContentReganal;
                $scope.top5title = $rootScope.Titlearray[9].ContentReganal;
                $scope.delightedtitle = $rootScope.Titlearray[10].ContentReganal;
                $scope.footnote = $rootScope.Footarray[1].ContentReganal;
                $scope.factorscore = "คะแนนปัจจัย";
            }

            //CSISummary-getNationalCSIScores
//            service.mastersrv("CSISummary-getNationalCSIScores", parameters).then(function (response) {
//
//                if (response.data.samplecount < 10) {
//                    $scope.samplespaceless = true;
////                    console.log($scope.samplespaceless);
//                } else {
//                    $scope.samplespaceless = false;
////                    console.log($scope.samplespaceless);
//                }
//                $scope.currmonth = response.data.data[0].Wave;
//                $scope.svg1 = response.data.data[0].Score;
//                $scope.prevmonth = response.data.data[1].Wave;
//                $scope.svg2 = response.data.data[1].Score;
//                $scope.prevquater = response.data.data[2].Wave;
//                $scope.svg3 = response.data.data[2].Score;
//
//
//            });

            //CSISummary-getCSISummaryRegionFactorScores
            service.mastersrv("CSISummary-getCSISummaryRegionFactorScores", parameters).then(function (response) {
                $('#summarybarlinecontainer').highcharts(service.summaryplainbar('column', ["#4292FF"], response.data, $scope.factorscore, 0, 45, .3, [1, 1, 1, 1, 1, 1], 5, false));

                if ($scope.langcode == 'EN') {
                    $rootScope.legendforselectionb = response.data[0].scatter;
                    $rootScope.legendforselectionw = response.data[0].scatter1;
                } else {
                    $rootScope.legendforselectionb = response.data[0].scatter;
                    $rootScope.legendforselectionw = response.data[0].scatter1;
                }
                $rootScope.legendforselectionb = response.data[0].scatter;
                $rootScope.legendforselectionw = response.data[0].scatter1;
            });


            //CSISummary-getCSITop5Bottom5
            service.mastersrv("CSISummary-getCSITop5Bottom5", parameters).then(function (response) {
                $('#summarytreecontainer').highcharts(service.multbarchart(1005, 'bar', response.data, 170, 15, 0, 'SOP Score', 10, '11px', " "));

                if (response.data.length == 0) {
                    $scope.samplespaceless = true;
                } else {
                    $scope.samplespaceless = false;
                }

            });


            //CSISummary-getCSITop5Bottom5SOP
            service.mastersrv("CSISummary-getCSITop5Bottom5SOP", parameters).then(function (response) {
                $('#summarybot1').highcharts(service.multbarchartsplit(100, 'bar', response.data, 180, 15, 0, 'CSI Score', 9, '8.5px', "%"));

            });


            //CSISummary-RegionWiseCSI
            service.mastersrv("CSISummary-RegionWiseCSI", parameters).
                    then(function (response) {
                        $scope.mapData = response.data;
                        $scope.maprender();


                    });


        };

        $scope.state = "All";
        $scope.zone = "All";
        $scope.region = "All";
        $scope.model = "All";



        $scope.legendname = "Study Best";
        $scope.legendnamew = "Worst Score";
        $scope.model = "All";
        $scope.city = "All";
        $scope.factor = "CSI"

        $scope.regionalnAlysisGraphs = function () {
            var parameters = [{"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}];

            if ($scope.langcode == 'EN') {
                $scope.regfacttitle = $rootScope.Titlearray[11].ContentEN;
                $scope.byregiontitle = $rootScope.Titlearray[72].ContentEN;
                $scope.meantitle = $rootScope.Titlearray[73].ContentEN;
                $scope.percenttitle = $rootScope.Titlearray[74].ContentEN;
                $scope.factorscore = "Factor Score";
                $scope.attscore = 'Attribute Score';
                $scope.footnote = $rootScope.Footarray[1].ContentEN;
            } else {
                $scope.regfacttitle = $rootScope.Titlearray[11].ContentReganal;
                $scope.byregiontitle = $rootScope.Titlearray[72].ContentReganal;
                $scope.meantitle = $rootScope.Titlearray[73].ContentReganal;
                $scope.percenttitle = $rootScope.Titlearray[74].ContentReganal;
                $scope.factorscore = "คะแนนปัจจัย";
                $scope.attscore = 'คุณลักษณะ คะแนน';
                $scope.footnote = $rootScope.Footarray[1].ContentReganal;
            }


            //CSIRegional-getCSIFactorAverage
            service.mastersrv("CSIRegional-getCSIFactorAverage", parameters).then(function (response) {
                console.log('response.data.data', response.data.data);
                console.log('response.data', response.data.samplecount);
                $scope.svgname = response.data.data[0].Factor;
                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    console.log($scope.samplespaceless);
                }

                if (response.data.data.length == 0) {
                    response.data.data = [{"bartitle": "100%"}, {"Score": "0"}];
                }
                $scope.svgvalue = response.data.data[0].Score;
//                            $scope.svgname=response.data.data[0].Factor;
            });


            //CSIRegional-getCSIRegionalFactorAverage
            service.mastersrv("CSIRegional-getCSIRegionalFactorAverage", parameters).then(function (response) {
                $('#regionalbarlinecontainer').highcharts(service.regplainbar('column', color, response.data, $scope.factorscore, 0, 60, .3, [0], 8, true));


            });


            //CSIRegional-getCSIRegionalAttributeAverage
            service.mastersrv("CSIRegional-getCSIRegionalAttributeAverage", parameters).then(function (response) {
                $('#regionalbot1').highcharts(service.barplainbar('bar', ['#2979FF'], response.data, $scope.attscore, 170, 35, 0, [0], 5, false, " "));
                $rootScope.legendforselectionb = response.data[0].scatter;
                $rootScope.legendforselectionw = response.data[0].scatter1;
            });


            //CSIRegional-getCSIDissatisfied
            service.mastersrv("CSIRegional-getCSIDelighted", parameters).then(function (response) {
                $('#regionalbot2').highcharts(service.barplainbar('bar', ['#2979FF'], response.data, $scope.attscore, 170, 35, 0, [0], 5, false, "%"));

            });

        };


        $scope.factor = "CSI";
        $scope.area = "All";
        $scope.zone = "All";
        $scope.province = "All";

        $scope.region = "All";
        $scope.city = "All";
        $scope.model = "All";


        $scope.trendgraphs = function () {
            parameters = [{"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}];
            if ($scope.langcode == 'EN') {
                $scope.trend1 = $rootScope.Titlearray[27].ContentEN;
                $scope.trend2 = $rootScope.Titlearray[75].ContentEN;
                $scope.trend3 = $rootScope.Titlearray[76].ContentEN;
                $scope.footnote = $rootScope.Footarray[1].ContentEN;
                $scope.factorscore = "Factor Score";
                $scope.csiscore = 'CSI Score';
            } else {
                $scope.trend1 = $rootScope.Titlearray[27].ContentReganal;
                $scope.trend2 = $rootScope.Titlearray[75].ContentReganal;
                $scope.trend3 = $rootScope.Titlearray[76].ContentReganal;
                $scope.footnote = $rootScope.Footarray[1].ContentReganal;
                $scope.factorscore = "คะแนนปัจจัย";
                $scope.csiscore = 'CSI คะแนน';
            }

            //  CSITrendAnalysis-getCSITrendByWave 1
            service.mastersrv("CSITrendAnalysis-getCSITrendByWave", parameters).then(function (response) {
                console.log('response.data', response.data);
                console.log('response.data.data', response.data.data);
                console.log('response.data.samplecount', response.data.samplecount);
                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    console.log($scope.samplespaceless);
                }

                $('#trendtop1').highcharts(service.summaryplainbar('column', ['#2979FF'], response.data.data, $scope.csiscore, 0, 25, .3, [1, 1, 1, 1, 1, 1], 5, false));
//                                $rootScope.legendforselectionb= response.data[0].scatter;
//                                $rootScope.legendforselectionw=response.data[0].scatter1;
            });

            //  CSITrendAnalysis-getCSIFactorTrendByWave 2
            service.mastersrv("CSITrendAnalysis-getCSIFactorTrendByWave", parameters).then(function (response) {
                $('#trendtop2').highcharts(service.summaryplainbar('column', ['#7E57C2'], response.data, $scope.factorscore, 0, 25, .3, [1, 1, 1, 1, 1, 1], 5, false));
                $rootScope.legendforselectionb = response.data[0].scatter;
                $rootScope.legendforselectionw = response.data[0].scatter1;
            });

            //  CSITrendAnalysis-getCSIAttributeTrendByWave 3
            service.mastersrv("CSITrendAnalysis-getCSIAttributeTrendByWave", parameters).then(function (response) {
                $('#trendbot').highcharts(service.horizontalline(response.data, color));
            });

            //  CSITrendAnalysis-getCSIFactorTrendByWave 2
//                               service.mastersrv("CSITrendAnalysis-getCSIFactorTrendByWave",parameters).then(function (response) {
//                                     $('#trendtop2').highcharts(service.summaryplainbar('column',['#7E57C2'],response.data,$scope.factorscore,0,25,.3,[1,1,1,1,1,1],5,false));
//                              $scope.legendname=response.data[0].scatter;
//                        $rootScope.legendnamew=response.data[0].scatter1;
//                               });

            //  CSITrendAnalysis-getCSIAttributeTrendByWave 3
//                            service.mastersrv("CSITrendAnalysis-getCSIAttributeTrendByWave",parameters).then(function (response) {
//                                      $('#trendbot').highcharts(service.horizontalline(response.data,color));
//                      });


        };

        //end of  trend anaylasis page





        //priority anaaylasis page
        $scope.default = "region";
        $scope.region1 = "Bangkok & Greater";
        $scope.region2 = "Central and East";
        $scope.dealer1 = "$rootScope.Filterarray.dealer[1].name;";
        $scope.dealer2 = "Ariyakij";

        $scope.prioritygraphs = function () {
            var parameters = [{"name": "measure", "value": $scope.default}, {"name": "langCode", "value": $scope.langcode}, {"name": "region1", "value": encodeURIComponent($scope.region1)}, {"name": "region2", "value": encodeURIComponent($scope.region2)}, {"name": "biannual", "value": $scope.biannual}];

            if ($scope.langcode == 'EN') {
                $rootScope.choosecomp = $rootScope.Titlearray[40].ContentEN;
                $rootScope.regcomp = $rootScope.Titlearray[41].ContentEN;
                $rootScope.dealercomp = $rootScope.Titlearray[42].ContentEN;
                $rootScope.reg1 = $rootScope.Titlearray[43].ContentEN;
                $rootScope.reg2 = $rootScope.Titlearray[42].ContentEN;
                $rootScope.dealer1 = $rootScope.Titlearray[45].ContentEN;
                $rootScope.dealer2 = $rootScope.Titlearray[45].ContentEN;
                $scope.footnote = $rootScope.Footarray[1].ContentEN;
            } else {
                $rootScope.choosecomp = $rootScope.Titlearray[40].ContentReganal;
                $rootScope.regcomp = $rootScope.Titlearray[41].ContentReganal;
                $rootScope.dealercomp = $rootScope.Titlearray[42].ContentReganal;
                $rootScope.reg1 = $rootScope.Titlearray[43].ContentReganal;
                $rootScope.reg2 = $rootScope.Titlearray[42].ContentReganal;
                $rootScope.dealer1 = $rootScope.Titlearray[45].ContentReganal;
                $rootScope.dealer2 = $rootScope.Titlearray[45].ContentReganal;
                $scope.footnote = $rootScope.Footarray[1].ContentReganal;
            }


            service.mastersrv("CSIPriority-getCSIPriority", parameters).then(function (response) {
                $('#prioritymainchart').highcharts(service.verticalline(color, true, response.data, 'left', 'top', true, '-100', '100', '2'));

            });
        };


        //for sop performance
        $scope.zone = "All";
        $scope.region = "All";
        $scope.dealer = "All";

//      

        $scope.sopswitchoutput = 'total';
        $scope.sopswitchoutputcaps = 'Total';

        $scope.SopPerformanceGraphs = function () {
            $scope.group1 = "Total";
            var parameters = [{"name": "compareto", "value": $scope.sopswitchoutput}, {"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "dealer", "value": $scope.dealer}];
            if ($scope.langcode == 'EN') {
                $scope.soptitle1 = $rootScope.Titlearray[46].ContentEN;
                $scope.soptitle2 = $rootScope.Titlearray[47].ContentEN;
                $scope.soptitle3 = $rootScope.Titlearray[48].ContentEN;
                $scope.opt1 = "Total";
                $scope.opt2 = "Zone";
                $scope.opt3 = "Region";
                $scope.sopfooter1 = $rootScope.Titlearray[50].ContentEN;
                $scope.sopfooter2 = $rootScope.Titlearray[51].ContentEN;
                $scope.sopfooter3 = $rootScope.Titlearray[52].ContentEN;
                $scope.sopfooter4 = $rootScope.Titlearray[49].ContentEN;
                $scope.footnote = $rootScope.Footarray[1].ContentEN;
                $scope.attscore = 'Selected Main Selection';
            } else {
                $scope.soptitle1 = $rootScope.Titlearray[46].ContentReganal;
                $scope.soptitle2 = $rootScope.Titlearray[47].ContentReganal;
                $scope.soptitle3 = $rootScope.Titlearray[48].ContentReganal;
                $scope.opt1 = "全部";
                $scope.opt2 = "โซน";
                $scope.opt3 = "ภูมิภาค";
                $scope.sopfooter1 = $rootScope.Titlearray[50].ContentReganal;
                $scope.sopfooter2 = $rootScope.Titlearray[51].ContentReganal;
                $scope.sopfooter3 = $rootScope.Titlearray[52].ContentReganal;
                $scope.sopfooter4 = $rootScope.Titlearray[49].ContentReganal;
                $scope.footnote = $rootScope.Footarray[1].ContentReganal;
                $scope.attscore = 'หัวข้อที่เลือก';
            }

            //        CSISOPPerformance-getCSISOPPerformance&compareto=region
            service.mastersrv("CSISOPPerformance-getCSISOPPerformance", parameters).then(function (response) {
//                 
                console.log('response.data', response.data);
                console.log('response.data.data', response.data.data);
                console.log('response.data.samplecount', response.data.samplecount);
                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    console.log($scope.samplespaceless);
                }
                if (response.data.data.length == 0) {
                    response.data.data = [{"bartitle": "100%"}, {"y": "0"}];
                }
//                    
//                          $('#sop').highcharts(service.sopplainbar('bar','11px',color,response.data.data,'Attribute Score',margin,14,0,[0],5,false));
//                        $('#sop1').highcharts(service.sopplainbar('bar','9px',color,response.data.data,'Attribute Score',margin,25,0,[0],5,false));

                $('#sopsop').highcharts(service.sopplainbar('bar', '11px', color, response.data.data, $scope.attscore, margin, 14, 0, [0], 5, false));

            });
        };



        $scope.soptrendgraphs = function () {
            $scope.group1 = "Total";

            $scope.zone = "All";
            $scope.region = "All";
            $scope.dealer = "All";

//        

            $scope.dealer = "All";
//        $scope.year=$rootScope.Filterarray.year[$rootScope.Filterarray.year.length-1];
            $scope.sop = "Notified for routine maintenance^";
            $scope.sopswitchoutput = 'total';
            $scope.sopswitchoutputcaps = 'Total';
            var parameters = [{"name": "compareto", "value": $scope.sopswitchoutput}, {"name": "attribute", "value": encodeURIComponent($scope.sop)}, {"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "dealer", "value": $scope.dealer}];


            if ($scope.langcode == 'EN') {
                $scope.sottitle1 = $rootScope.Titlearray[54].ContentEN;
                $scope.sottitle2 = $rootScope.Titlearray[56].ContentEN;
                $scope.sottitle3 = $rootScope.Titlearray[57].ContentEN;
                $scope.opt1 = "Total";
                $scope.opt2 = "Zone";
                $scope.opt3 = "Region";
                $scope.sotfooter1 = $rootScope.Titlearray[58].ContentEN;
                $scope.sotfooter2 = $rootScope.Titlearray[59].ContentEN;
                $scope.sotfooter3 = $rootScope.Titlearray[60].ContentEN;
                $scope.footnote = $rootScope.Footarray[1].ContentEN;
                $scope.graphname = 'Selected Main Selection';
            } else {
                $scope.sottitle1 = $rootScope.Titlearray[54].ContentReganal;
                $scope.sottitle2 = $rootScope.Titlearray[56].ContentReganal;
                $scope.sottitle3 = $rootScope.Titlearray[57].ContentReganal;
                $scope.opt1 = "全部";
                $scope.opt2 = "โซน";
                $scope.opt3 = "ภูมิภาค";
                $scope.sotfooter1 = $rootScope.Titlearray[58].ContentReganal;
                $scope.sotfooter2 = $rootScope.Titlearray[59].ContentReganal;
                $scope.sotfooter3 = $rootScope.Titlearray[60].ContentReganal;
                $scope.footnote = $rootScope.Footarray[1].ContentReganal;
                $scope.graphname = 'หัวข้อที่เลือก';
            }


            service.mastersrv("CSISOPPerformance-getCSISOPTrend", parameters).then(function (response) {
//                 console.log('response.data',response.data);
                console.log('response.data.data', response.data.data);
                console.log('response.data.samplecount', response.data.samplecount);
                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    console.log($scope.samplespaceless);
                }
                if (response.data.data.length == 0) {
                    response.data.data = [{"bartitle": "100%"}, {"y": "0"}];
                }
                $('#soptrendmainchart').highcharts(service.barline(['#2196F3'], response.data.data, $scope.graphname));

            });

        };


        //for other diagnostics


        $scope.otherdiagnostics = function () {
            if ($scope.langcode == 'EN') {
                $scope.othertitle1 = $rootScope.Titlearray[61].ContentEN;
                $scope.page1 = "Page 1";
                $scope.page2 = "Page 2";
                $scope.page3 = "Page 3";
                $scope.Questions = $rootScope.Titlearray[62].ContentEN;
            } else {
                $scope.othertitle1 = $rootScope.Titlearray[61].ContentReganal;
                $scope.page1 = "หน้า 1";
                $scope.page2 = "หน้า 2";
                $scope.page3 = "หน้า 3";
                $scope.Questions = $rootScope.Titlearray[62].ContentReganal;
            }

            //       CSIOthers-getCSIOthersTable

            //CSIOthers-getCSIOthersCharts
            service.mastersrv("CSIOthers-getCSIOthersCharts", parameters).then(function (response) {
                $scope.response = response;


                $scope.heading0 = response.data[2].headingname;
                $('#ODtop01').highcharts(service.stackedgraph(response.data[2].headingdata, color));
                $scope.heading1 = response.data[3].headingname;
                $('#ODtop02').highcharts(service.stackedgraph(response.data[3].headingdata, color));
                $scope.heading2 = response.data[4].headingname;
                $('#ODbot01').highcharts(service.stackedgraph(response.data[4].headingdata, color));
                $scope.heading3 = response.data[7].headingname;
                $('#ODbot02').highcharts(service.stackedgraph(response.data[7].headingdata, color, 90));

                //page2
                $scope.heading4 = response.data[1].headingname;
                $('#ODtop11').highcharts(service.stackedgraph(response.data[1].headingdata, color));
                $scope.heading5 = response.data[0].headingname;
                $('#ODtop12').highcharts(service.stackedgraph(response.data[0].headingdata, color));

                //page3
                $scope.heading6 = response.data[6].headingname;
                $('#ODbot11').highcharts(service.stackedgraph(response.data[6].headingdata, color));
                $scope.heading7 = response.data[8].headingname;
                $('#ODbot12').highcharts(service.stackedgraph(response.data[8].headingdata, color));
                $scope.heading8 = response.data[5].headingname;
                $('#ODbot22').highcharts(service.stackedgraph(response.data[5].headingdata, color));


//                    $scope.heading0b = response.data[4].headingname;
//                    $('#page0b').highcharts(service.stackmultigraph(response.data[4].headingdata, color, 20, '10px', '.07'));
//                    $scope.heading0a = response.data[3].headingname;
//                    $('#page0a').highcharts(service.stackmultigraph(response.data[3].headingdata, color, 35, '12px', '.07'));
//                    $scope.heading0c=response.data[10].headingname;
//                    $('#page0c').highcharts(service.stackedgraph(response.data[10].headingdata,color));
//                    
//                   

//                    
//                    $('#ODtop01x').highcharts(service.stackedgraph(response.data[6].headingdata,color,80));        
//                    $('#ODtop02x').highcharts(service.stackedgraph(response.data[2].headingdata,color));
//                    $('#ODbot01x').highcharts(service.stackedgraph(response.data[7].headingdata,color));
//                    $('#ODbot02x').highcharts(service.stackedgraph(response.data[0].headingdata,color));
//                    $('#ODtop11x').highcharts(service.stackedgraph(response.data[4].headingdata,color));
//                    $('#ODtop12x').highcharts(service.stackedgraph(response.data[5].headingdata,color));
//                    $('#ODbot11x').highcharts(service.stackedgraph(response.data[1].headingdata,color));
//                    $('#ODbot12x').highcharts(service.stackedgraph(response.data[3].headingdata,color));
//                    $('#ODpage0bx').highcharts(service.stackmultigraph(response.data[9].headingdata, color, 25, '11px', '.07'));
//                    $('#ODpage0ax').highcharts(service.stackmultigraph(response.data[10].headingdata, color, 25, '11px', '.07'));
//                    $('#ODpage0cx').highcharts(service.stackedgraph(response.data[8].headingdata,color));


            });

        };




        $scope.dealerranking = function () {


            if ($scope.langcode == 'EN') {
                console.log($rootScope.Titlearray)
                $scope.export = $rootScope.Titlearray[79].ContentEN;
                $scope.table0 = $rootScope.Titlearray[80].ContentEN;
                $scope.table1 = $rootScope.Titlearray[81].ContentEN;
                $scope.table2 = $rootScope.Titlearray[82].ContentEN;
                $scope.table3 = $rootScope.Titlearray[83].ContentEN;
                $scope.table4 = $rootScope.Titlearray[84].ContentEN;
                $scope.table5 = $rootScope.Titlearray[85].ContentEN;
                $scope.table6 = $rootScope.Titlearray[86].ContentEN;
                $scope.table7 = $rootScope.Titlearray[87].ContentEN;
                $scope.table8 = $rootScope.Titlearray[88].ContentEN;
                $scope.footnote = $rootScope.Footarray[1].ContentEN;
                $scope.footer = "indicates small sample and is not considered for color rankings";
                $scope.tableregion = 'Region';

            } else {
                $scope.export = $rootScope.Titlearray[79].ContentReganal;
                $scope.table0 = $rootScope.Titlearray[80].ContentReganal;
                $scope.table1 = $rootScope.Titlearray[81].ContentReganal;
                $scope.table2 = $rootScope.Titlearray[82].ContentReganal;
                $scope.table3 = $rootScope.Titlearray[83].ContentReganal;
                $scope.table4 = $rootScope.Titlearray[84].ContentReganal;
                $scope.table5 = $rootScope.Titlearray[85].ContentReganal;
                $scope.table6 = $rootScope.Titlearray[86].ContentReganal;
                $scope.table7 = $rootScope.Titlearray[87].ContentReganal;
                $scope.table8 = $rootScope.Titlearray[88].ContentReganal;
                $scope.footnote = $rootScope.Footarray[1].ContentReganal;
                $scope.tableregion = 'ภูมิภาค';
                $scope.footer = "บ่งชี้ว่ากลุ่มตัวอย่างขนาดเล็กและไม่ได้รับการพิจารณาสำหรับการจัดอันดับของสี การส่งออกตาราง";

            }



            $scope.dalerscorettable = {
                id: "dealertable",
                view: "datatable",
                headerRowHeight: 75,
                fixedRowHeight: false,
                rowLineHeight: 30,
                rowHeight: 30,
                scroll: 'y',
                position: "flex",
                css: "my_style",
                columns: [
                    {id: "rank", css: "columnstyle", header: {text: $scope.table0, css: 'multiline'}, width: 50},
                    {id: "region", css: "columnstyle", header: {text: $scope.tableregion}, adjust: 'true', minWidth: 100},
                    {id: "dealer", css: "columnstyle", header: {text: $scope.table1}, minWidth: 150, fillspace: true, adjust: 'true'},
                    {id: "csi", header: [$scope.table2], adjust: 'true'},
                    {id: "serviceinitiation", header: {text: $scope.table3, css: 'multiline'}, adjust: 'true'},
                    {id: "serviceadvisor", header: {text: $scope.table4, css: 'multiline'}, adjust: 'true'},
                    {id: "servicefacility", header: {text: $scope.table5, css: 'multiline'}, adjust: 'true'},
                    {id: "vehiclepickup", header: {text: $scope.table6, css: 'multiline'}, adjust: 'true'},
                    {id: "servicequality", header: {text: $scope.table7, css: 'multiline'}, adjust: 'true'},
                    {id: "sop", header: {text: $scope.table8, css: 'multiline'}, adjust: 'true'}
                ],

            };


            service.mastersrv("CSIDealerScoreTable-getCSIDealerScoreTable", parameters).then(function (response) {
//              vm.salesData=response.data;
                $scope.records = response.data;
                console.log($scope.records);
                $scope.dalerscorettable = {
                    id: "dealertable",
                    view: "datatable",
                    fixedRowHeight: false,
                    headerRowHeight: 75,
                    rowLineHeight: 30,
                    rowHeight: 30,
                    scroll: 'y',
                    position: "flex",
                    topSplit: 3,
                    select: "row",
                    css: "my_style",
                    tooltip: true,
                    columns: [
                        {id: "rank", css: "columnstyle", header: {text: $scope.table0, css: 'multiline'}, width: 50},
                        {id: "region", css: "columnstyle", header: {text: $scope.tableregion}, adjust: 'true', minWidth: 100},
                        {id: "dealer", css: "columnstyle", header: {text: $scope.table1}, minWidth: 150, fillspace: true, adjust: 'true'},
                        {id: "csi", header: [$scope.table2], adjust: 'true', cssFormat: mark_csi},
                        {id: "serviceinitiation", header: {text: $scope.table3, css: 'multiline'}, adjust: 'true', cssFormat: mark_serviceinitiation},
                        {id: "serviceadvisor", header: {text: $scope.table4, css: 'multiline'}, adjust: 'true', cssFormat: mark_serviceadvisor},
                        {id: "servicefacility", header: {text: $scope.table5, css: 'multiline'}, adjust: 'true', cssFormat: mark_servicefacility},
                        {id: "vehiclepickup", header: {text: $scope.table6, css: 'multiline'}, adjust: 'true', cssFormat: mark_vehiclepickup},
                        {id: "servicequality", header: {text: $scope.table7, css: 'multiline'}, adjust: 'true', cssFormat: mark_servicequality},
                        {id: "sop", header: {text: $scope.table8, css: 'multiline'}, adjust: 'true', cssFormat: mark_sop}
                    ],
                    data: $scope.records,
                    resizeColumn: true,
                    ready: function () {
                        $$("dealertable").hideProgress();
                    },
                    on: {

                        "onresize": webix.once(function () {
                            this.adjustRowHeight("dealer", true);
                        }),
                        'onItemClick': function (e, id) {
                            var datatable = $$("dealertable");
                            var firstid = datatable.getFirstId()
                            var secondid = datatable.getNextId(firstid, 1)
                            var thirdid = datatable.getNextId(secondid, 1)
                            var firstrecord = datatable.getItem(firstid);
                            var secondrecord = datatable.getItem(secondid);
                            var thirdrecord = datatable.getItem(thirdid);
                            var selectedId = datatable.getSelectedId()
                            if (selectedId != firstid && selectedId != secondid && selectedId != thirdid)
                            {
                                var row = datatable.getItem(selectedId);
                                $scope.dealer = row.dealer;
                                $scope.region = row.region;
                                console.log('$scope.region', $scope.region);
                                console.log('$scope.region', $scope.region);
                                console.log('$scope.region', $scope.region);
                                console.log('$scope.region', $scope.region);
                                console.log('$scope.region', $scope.region);
                                parameters = [{"name": "langCode", "value": $scope.langcode}, {"name": "dealer", "value": $scope.dealer}, {"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}]
                                service.mastersrv("CSIDealerScoreTable-getCSITOP3RowsDealerScore", parameters).then(function (response) {
                                    console.log(response.data);
                                    firstrecord["dealer"] = response.data[0].dealer;
                                    firstrecord["csi"] = response.data[0].csi;
                                    firstrecord["serviceinitiation"] = response.data[0].serviceinitiation;
                                    firstrecord["serviceadvisor"] = response.data[0].serviceadvisor;
                                    firstrecord["servicefacility"] = response.data[0].servicefacility;
                                    firstrecord["vehiclepickup"] = response.data[0].vehiclepickup;
                                    firstrecord["servicequality"] = response.data[0].servicequality;

                                    secondrecord["dealer"] = response.data[1].dealer;
                                    secondrecord["csi"] = response.data[1].csi;
                                    secondrecord["serviceinitiation"] = response.data[1].serviceinitiation;
                                    secondrecord["serviceadvisor"] = response.data[1].serviceadvisor;
                                    secondrecord["servicefacility"] = response.data[1].servicefacility;
                                    secondrecord["vehiclepickup"] = response.data[1].vehiclepickup;
                                    secondrecord["servicequality"] = response.data[1].servicequality;

                                    thirdrecord["dealer"] = response.data[2].dealer;
                                    thirdrecord["csi"] = response.data[2].csi;
                                    thirdrecord["serviceinitiation"] = response.data[2].serviceinitiation;
                                    thirdrecord["serviceadvisor"] = response.data[2].serviceadvisor;
                                    thirdrecord["servicefacility"] = response.data[2].servicefacility;
                                    thirdrecord["vehiclepickup"] = response.data[2].vehiclepickup;
                                    thirdrecord["servicequality"] = response.data[2].servicequality;
                                    thirdrecord["sop"] = response.data[2].sop;

                                    datatable.updateItem(firstid, firstrecord);

                                    datatable.updateItem(secondid, secondrecord);

                                    datatable.updateItem(thirdid, thirdrecord);

                                });
                            }
                        }

                    },

                };



            })

//            dealertable

        }



        $scope.doSome = function () {
            webix.extend($$("dealertable"), webix.ProgressBar);
            $$("dealertable").showProgress({
                type: "icon",
            });
        }

        function mark_csi(value, config) {

            return {"background-color": config["csicolor"]};
            return value;
        }

        function mark_serviceinitiation(value, config) {

            return {"background-color": config["serviceinitiationcolor"]};
            return value;
        }
        function mark_serviceadvisor(value, config) {

            return {"background-color": config["serviceadvisorcolor"]};
            return value;
        }
        function mark_servicefacility(value, config) {

            return {"background-color": config["servicefacilitycolor"]};
            return value;
        }
        function mark_vehiclepickup(value, config) {

            return {"background-color": config["vehiclepickupcolor"]};
            return value;
        }
        function mark_servicequality(value, config) {

            return {"background-color": config["servicequalitycolor"]};
            return value;
        }
        function mark_sop(value, config) {

            return {"background-color": config["sopcolor"]};
            return value;
        }
        $scope.exportdata = function () {
            webix.toExcel($$("dealertable"), {
                filterHTML: true
            });
//  webix.toExcel($$("dealertable"));
        }




//        for delaer snapshot page









        $scope.modelanalysis = function () {


            $scope.area = "All";
            $scope.zone = "All";
            $scope.province = "All";
            $scope.zone = "All";
            $scope.region = "All";
            $scope.model = "All";



            $scope.region = "All";
            $scope.city = "All";
            $scope.factor = "CSI";
            $scope.factorname = "CSI";

            var botparameters = [{"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}, {"name": "factor", "value": $scope.factor}];


            var parameters = [{"name": "langCode", "value": $scope.langcode}, {"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}];



            if ($scope.langcode == 'EN') {
                $scope.model1 = $rootScope.Titlearray[118].ContentEN;
                $scope.model2 = $rootScope.Titlearray[119].ContentEN;
                $scope.mtable1 = $rootScope.Titlearray[120].ContentEN;
                $scope.mtable2 = $rootScope.Titlearray[121].ContentEN;
                $scope.mtable3 = $rootScope.Titlearray[122].ContentEN;
                $scope.mtable4 = $rootScope.Titlearray[123].ContentEN;
                $scope.mtable5 = $rootScope.Titlearray[124].ContentEN;
                $scope.mtable0 = "Model";
                $scope.model3 = $rootScope.Titlearray[129].ContentEN;
                $scope.footnote = $rootScope.Footarray[1].ContentEN;
            } else {
                $scope.model1 = $rootScope.Titlearray[118].ContentReganal;
                $scope.model2 = $rootScope.Titlearray[119].ContentReganal;
                $scope.mtable1 = $rootScope.Titlearray[120].ContentReganal;
                $scope.mtable2 = $rootScope.Titlearray[121].ContentReganal;
                $scope.mtable3 = $rootScope.Titlearray[122].ContentReganal;
                $scope.mtable4 = $rootScope.Titlearray[123].ContentReganal;
                $scope.mtable5 = $rootScope.Titlearray[124].ContentReganal;
                $scope.mtable0 = "รุ่นรถ";
                $scope.model3 = $rootScope.Titlearray[129].ContentReganal;
                $scope.footnote = $rootScope.Footarray[1].ContentReganal;
            }



            //        CSIModelAnalysis-getModelCSIScores 1
            service.mastersrv("CSIModelAnalysis-getModelCSIScores", parameters).then(function (response) {
                $('#MAleftgraph').highcharts(service.barchart('bar', '#2979FF ', response.data, 100, 40, 0, 'Avg CSI'));
            });
//                     service.mastersrv("CSIModelAnalysis-getModelCSIScores",parameters).then(function (response) {
//                                $('#MAleftgraph1').highcharts(service.barchart('bar','#2979FF ',response.data,100,20,0,'Avg CSI'));
//                    });

            //        CSIModelAnalysis-getModelFactorCSIScores  2
            service.mastersrv("CSIModelAnalysis-getModelFactorCSIScores", parameters).then(function (response) {
                $scope.salesData = response.data;

            });



            service.mastersrv("CSIModelAnalysis-getModelFactorTrend", botparameters).then(function (response) {

                $('#MAbotgraph').highcharts(service.barchart('column', '#92D400 ', response.data.data, 0, 40, 0, 'Model Score', 12, 1, 1005));
//                     $('#MAbotgraph1').highcharts(service.barchart('column','#92D400 ',response.data.data, 0,40,0,'Model Score',12,1,1005));

                console.log('response.data', response.data);
                console.log('response.data.data', response.data.data);
                console.log('response.data.totalcount', response.data.totalcount);
                if (response.data.totalcount < 10) {
                    $scope.modelname = $scope.model + "**";
                } else if (response.data.totalcount <= 30 && response.data.totalcount >= 10) {
                    $scope.modelname = $scope.model + "*";
                } else {
                    $scope.modelname = $scope.model;
                    console.log($scope.samplespaceless);
                }
            });
        };

        $scope.print = function () {

            html2canvas(document.getElementById('exportthis'), {
                onrendered: function (canvas) {
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                                image: data,
                                width: 500,
                            }]
                    };
                    pdfMake.createPdf(docDefinition).download("Score_Details.pdf");
                }
            });
        }

        $scope.loyalityandadvocacy = function () {


            $scope.zone = "All";
            $scope.region = "All";
            $scope.model = "All";



            $scope.dealer = "Study Total";
            parameters = [{"name": "langCode", "value": $scope.langcode}, {"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "dealer", "value": $scope.dealer}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}];


            if ($scope.langcode == 'EN') {
                $scope.loyal1 = $rootScope.Titlearray[134].ContentEN;
                $scope.loyal2 = $rootScope.Titlearray[135].ContentEN;
                $scope.footnote = $rootScope.Footarray[1].ContentEN;
            } else {
                $scope.loyal1 = $rootScope.Titlearray[134].ContentReganal;
                $scope.loyal2 = $rootScope.Titlearray[135].ContentReganal;
                $scope.footnote = $rootScope.Footarray[1].ContentReganal;
            }



            if ($scope.langcode == 'EN') {
                $scope.loyal1 = $rootScope.Titlearray[134].ContentEN;
                $scope.loyal2 = $rootScope.Titlearray[135].ContentEN;
                $scope.footnote = $rootScope.Footarray[1].ContentEN;
            } else {
                $scope.loyal1 = $rootScope.Titlearray[134].ContentReganal;
                $scope.loyal2 = $rootScope.Titlearray[135].ContentReganal;
                $scope.footnote = $rootScope.Footarray[1].ContentReganal;
            }



//            CSILoyaltyAndAdvocacy-getCSILoyaltyAndAdvocacy
            service.mastersrv("CSILoyaltyAndAdvocacy-getCSILoyaltyAndAdvocacy", parameters).then(function (response) {

                console.log('response.data', response.data);
                console.log('response.data.data', response.data);
                console.log('response.data', response.data.samplecount);
                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    console.log($scope.samplespaceless);
                }





                if (response.data.data.length == 0) {
                    response.data.data = [{"bartitle": "100%"}, {"y": "0"}];
                }


                $scope.heading1 = response.data.data[0].chartheading;
                $('#LAAtop1').highcharts(service.loyalitybar($scope.loyal1, response.data.data[0].chartdata, false, color));

                $scope.heading2 = response.data.data[1].chartheading;
                $('#LAAtop2').highcharts(service.loyalitybar($scope.loyal2, response.data.data[1].chartdata, false, color));

            });

        };





    }
})();
var mdDialogCtrl = function ($scope, $rootScope, webixmodal, dataToPass, $timeout, $mdDialog) {
    $scope.imageurls = [];
    $scope.imageurls = dataToPass;

    var modalclose = webixmodal;
    $scope.finished = function () {

        $timeout(function () {
            $mdDialog.cancel();
            webix.modalbox.hide(modalclose);
            return xepOnline.Formatter.Format('content', {embedLocalImages: true, pageHeight: '180mm', render: 'download', cssStyle: [{fontSize: '20px'}, {fontfamily: 'Roboto Condensed'}]});

        }, 2000);


    };





    $scope.cancel = function () {


        $mdDialog.cancel();
    };

};

var dealerDialogCtrl = function ($scope, $rootScope, service, webixmodal, items, $state, $http, $timeout, $mdDialog) {


    var vm = this;
    $rootScope.lockLeft = true;
    var color = ["#002776", "#92D400", "#00A1DE", "#72C7E7", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
    $rootScope.subpage = "";
    $rootScope.subpagexs = "";
    $rootScope.subpage += " ";
    $rootScope.hideyear = false;
    $rootScope.hidebiannual = false;
    $rootScope.hidemonth = false;
    $rootScope.hidezone = true;
    $rootScope.hideregion = true;
    $rootScope.hidemodel = true;
    $rootScope.snaphidezone = true;
    $rootScope.snaphideregion = true;
    $rootScope.hidefactor = false;
    $rootScope.hidetrendfactor = true;
    var languagepara = [];
    $scope.factor = "CSI";
    $scope.svgname = $scope.factor;
//        $scope.factorname=$rootScope.Filterarray.factor[0].value;
    $rootScope.dealersnaphide = true;
    $rootScope.dealerhide = true;
    $rootScope.pagenumber = '1';
    $rootScope.starlabelhide = true;
    $rootScope.legendname = "Study Best";
    $rootScope.legendnamew = "Worst Score";
    $rootScope.priorityhide = true;
    $scope.show4 = false;
    $scope.show3 = true;
    $scope.dealerbox = webixmodal
    $scope.dealer = items.dealer;
    $scope.langcode = 'EN'
    $rootScope.modelsnap = "not";
    $scope.zone = "Study Total";
    $scope.region = items.region;

    $scope.dealername = $scope.dealer;
    $scope.regionname = $scope.region;
    $scope.model = "Study Total";
    $scope.year = $rootScope.year;
    $scope.biannual = $rootScope.biannual;
    $scope.month = $rootScope.month;

    $scope.legendname = "Study Best";
    $scope.legendnamew = "Worst Score";
//        $rootScope.dtwave=$scope.waves;

    $scope.summaryscope = 'region';
    $scope.summarymeasure = 'best';
    $scope.summaryscopecaps = 'Region';
    $scope.summarymeasurecaps = 'Best';
    $rootScope.dealerScoreDealer = items.dealer;

    $rootScope.tabhide = true;
    $rootScope.$broadcast("tabindex", 1);


    if (!$rootScope.$$listenerCount['dealerenabledownload']) {
        $rootScope.$on("dealerenabledownload", function (event, data) {


            $scope.dealerprintdialog();
        });
    }


    $scope.dealerprintdialog = function () {

        $rootScope.dealerimageurls = [];
        domtoimage.toJpeg(document.getElementById('dealersummary'), {quality: 0.95})
                .then(function (summaryurl) {



                    $rootScope.dealerimageurls.push(summaryurl);
                    domtoimage.toJpeg(document.getElementById('dealerregionanalysis'), {quality: 0.95})
                            .then(function (regionanalysisUrl) {
                                $rootScope.dealerimageurls.push(regionanalysisUrl);


                                domtoimage.toJpeg(document.getElementById('dealertrendanalysis'), {quality: 0.95})
                                        .then(function (trendanalysisUrl) {
                                            $rootScope.dealerimageurls.push(trendanalysisUrl);

                                            domtoimage.toJpeg(document.getElementById('dealerpriorityanalysis'), {quality: 0.95})
                                                    .then(function (priorityanalysisUrl) {


                                                        $rootScope.dealerimageurls.push(priorityanalysisUrl);

                                                        domtoimage.toJpeg(document.getElementById('dealersopperformance'), {quality: 0.95})
                                                                .then(function (sopperformanceUrl) {
                                                                    $rootScope.dealerimageurls.push(sopperformanceUrl);



                                                                    domtoimage.toJpeg(document.getElementById('dealerotherdiagnostics'), {quality: 0.95})
                                                                            .then(function (otherdiagnosticsUrl) {
                                                                                $rootScope.dealerimageurls.push(otherdiagnosticsUrl);
                                                                                domtoimage.toJpeg(document.getElementById('dealerotherdiagnostics2'), {quality: 0.95})
                                                                                        .then(function (otherdiagnosticsUrl2) {
                                                                                            $rootScope.dealerimageurls.push(otherdiagnosticsUrl2);
                                                                                            domtoimage.toJpeg(document.getElementById('dealerotherdiagnostics3'), {quality: 0.95})
                                                                                                    .then(function (otherdiagnosticsUrl3) {
                                                                                                        $rootScope.dealerimageurls.push(otherdiagnosticsUrl3);

                                                                                                        domtoimage.toJpeg(document.getElementById('dealerloyalityadvocacy'), {quality: 0.95})
                                                                                                                .then(function (loyalityadvocacyUrl) {
                                                                                                                    $rootScope.dealerimageurls.push(loyalityadvocacyUrl);

                                                                                                                    $mdDialog.show({

                                                                                                                        templateUrl: 'app/module2/page13/print.html',

                                                                                                                        resolve: {
                                                                                                                            webixmodal: (function () {
                                                                                                                                return function () {
                                                                                                                                    return $scope.dealerbox;
                                                                                                                                };
                                                                                                                            })()
                                                                                                                        },
                                                                                                                        locals: {dataToPass: $rootScope.dealerimageurls},
                                                                                                                        controllerAs: 'ctrl',
                                                                                                                        controller: mdDialogCtrl,
                                                                                                                        clickOutsideToClose: true,
                                                                                                                        fullscreen: true // Only for -xs, -sm breakpoints.
                                                                                                                    });


                                                                                                                });


                                                                                                    });
                                                                                        });

                                                                            });






                                                                });




                                                    });


                                        });


                            });


                });


    };



    var benchmarkparameters = [{"name": "benchmarkmeasure", "value": $scope.summarymeasure}, {"name": "langCode", "value": $scope.langcode}, {"name": "dealer", "value": $scope.dealer}, {"name": "year", "value": $scope.year}, {"name": "benchmarkscope", "value": $scope.summaryscope}, {"name": "region", "value": encodeURIComponent($scope.region)}];





    if ($scope.langcode == 'EN') {

        $scope.nationaltitle = " CSI";
        $scope.regionaltitle = $rootScope.Titlearray[2].ContentEN;
        $scope.factortitle = $rootScope.Titlearray[3].ContentEN;
        $scope.top5title = $rootScope.Titlearray[73].ContentEN;
        $scope.delightedtitle = $rootScope.Titlearray[10].ContentEN;
        $scope.footnote = $rootScope.Footarray[1].ContentEN;
        $scope.factorscore = "Factor Score";
        $scope.svgname = "Service Advisor";
        $scope.benchmarktitle = " CSI";

    } else {
        $scope.nationaltitle = " CSI";
        $scope.regionaltitle = $rootScope.Titlearray[2].ContentReganal;
        $scope.factortitle = $rootScope.Titlearray[3].ContentReganal;
        $scope.top5title = $rootScope.Titlearray[73].ContentReganal;
        $scope.delightedtitle = $rootScope.Titlearray[10].ContentReganal;
        $scope.footnote = $rootScope.Footarray[1].ContentReganal;
        $scope.factorscore = "คะแนนปัจจัย";
        $scope.svgname = "ที่ปรึกษางานบริการ";
        $scope.benchmarktitle = " CSI";
    }




    $scope.$on('changesummaryscoreswitch', function (event, data) {
        $scope.summaryscope = data;
        $scope.benchmark();
    });

    $scope.$on('changesummarymeasureswitch', function (event, data) {
        $scope.summarymeasure = data;
        $scope.benchmark();
    });


    //////////////////////////////////////////////////////////////
    var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

    console.log(" height :", h);
    console.log(" width :", w);
    var margin = 300;
    var lineheight = 25;
    $scope.xstrue = false;
    var weight = 50;
    if (w < 1280 && w > 960) {
        margin = 250;
    } else if (w < 960 && w > 600) {
        margin = 200;
        lineheight = 12;
        weight = 40;
    } else if (w < 600) {
        margin = 150;
        lineheight = 12;
        weight = 30;
        $scope.xstrue = true;
    }
    console.log('margin', margin);
    //////////////////////////////////////////////////////////////

    $scope.$on('changeLanguage', function (event, data) {
        $scope.langcode = data;
        var dealerpara = [{"name": "english", "value": $scope.dealername}, {"name": "moduleType", "value": "CSI"}];
        var regionpara = [{"name": "english", "value": encodeURIComponent($scope.regionname)}, {"name": "moduleType", "value": "CSI"}];
        service.mastersrv("Systemuserview-getName", dealerpara).then(function (response) {
            if ($scope.langcode == 'EN') {
                $scope.dealername = response.data[0].ContentEN;
            } else {
                $scope.dealername = response.data[0].ContentReganal;
            }
        });
        service.mastersrv("Systemuserview-getName", regionpara).then(function (response) {
            if ($scope.langcode == 'EN') {
                $scope.regionname = response.data[0].ContentEN;
            } else {
                $scope.regionname = response.data[0].ContentReganal;
            }
        });
        $scope.filterchange();
        $rootScope.languagechangernav();
    });


    $scope.$on('changeyear', function (event, data) {
        $scope.year = data;
        $scope.show4 = false;
        $scope.show3 = true;
        $scope.biannual = "Study Total";
        $scope.month = "Study Total";
        $scope.yearscore();
    });

    $scope.$on('changebiannual', function (event, data) {
        $scope.biannual = data;
        $scope.show4 = false;
        $scope.show3 = true;
        $scope.month = "Study Total";

        $scope.biannualscore();
    });

    $scope.$on('changemonth', function (event, data) {

        $scope.month = data;
        if ($scope.month == "All") {
            $scope.show4 = false;
            $scope.show3 = true;
            $scope.yearscore();
        } else {
            $scope.show4 = true;
            $scope.show3 = false;
            $scope.monthscore();
        }
    });

    $scope.$on('changezone', function (event, data) {
        $scope.zone = data;
        $scope.region = "All";
        $scope.filterchange();
    });

    $scope.$on('changeregion', function (event, data) {
        $scope.region = data;
        $scope.filterchange();
    });

    $scope.$on('changemodel', function (event, data) {
        $scope.model = data;
        $scope.filterchange();
    });


    $scope.yearscore = function () {
        //CSISummary-getNationalCSIScores
        service.mastersrv("CSISummaryDealer-getNationalCSIScoresYear", parameters).then(function (response) {

            if (response.data.samplecount < 10) {
                $scope.samplespaceless = true;
            } else {
                $scope.samplespaceless = false;
            }
            $scope.currmonth = response.data.data[0].Wave;
            $scope.svg1 = response.data.data[0].Score;
            $scope.prevmonth = response.data.data[1].Wave;
            $scope.svg2 = response.data.data[1].Score;
            $scope.prevquater = response.data.data[2].Wave;
            $scope.svg3 = response.data.data[2].Score;


        });


        benchmarkparameters = [{"name": "benchmarkmeasure", "value": $scope.summarymeasure}, {"name": "langCode", "value": $scope.langcode}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "year", "value": $scope.year}, {"name": "benchmarkscope", "value": $scope.summaryscope}, {"name": "dealer", "value": $scope.dealer}, {"name": "region", "value": encodeURIComponent($scope.region)}];
        service.mastersrv("CSISummaryDealer-getCSIBenchmarkyear", benchmarkparameters).then(function (response) {
            console.log(response.data);
            $scope.benchmark1 = response.data[0].Score;
            $scope.benchmarkcurrmonth = response.data[0].Wave;
            $scope.benchmark2 = response.data[1].Score;
            $scope.benchmarkprevmonth = response.data[1].Wave;
            $scope.benchmark3 = response.data[2].Score;
            $scope.benchmarkprevquater = response.data[2].Wave;
        })
    };
    $scope.biannualscore = function () {
        //CSISummary-getNationalCSIScores
        service.mastersrv("CSISummaryDealer-getNationalCSIScoresBiannual", parameters).then(function (response) {

            if (response.data.samplecount < 10) {
                $scope.samplespaceless = true;
            } else {
                $scope.samplespaceless = false;
            }
            $scope.currmonth = response.data.data[0].Wave;
            $scope.svg1 = response.data.data[0].Score;
            $scope.prevmonth = response.data.data[1].Wave;
            $scope.svg2 = response.data.data[1].Score;
            $scope.prevquater = response.data.data[2].Wave;
            $scope.svg3 = response.data.data[2].Score;


        });


        benchmarkparameters = [{"name": "benchmarkmeasure", "value": $scope.summarymeasure}, {"name": "langCode", "value": $scope.langcode}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "year", "value": $scope.year}, {"name": "benchmarkscope", "value": $scope.summaryscope}, {"name": "dealer", "value": $scope.dealer}, {"name": "region", "value": encodeURIComponent($scope.region)}];
        service.mastersrv("CSISummaryDealer-getCSIBenchmarkbiannual", benchmarkparameters).then(function (response) {
            console.log(response.data);
            $scope.benchmark1 = response.data[0].Score;
            $scope.benchmarkcurrmonth = response.data[0].Wave;
            $scope.benchmark2 = response.data[1].Score;
            $scope.benchmarkprevmonth = response.data[1].Wave;
            $scope.benchmark3 = response.data[2].Score;
            $scope.benchmarkprevquater = response.data[2].Wave;
        })
    };


    $scope.monthscore = function () {
        //CSISummary-getNationalCSIScores
        service.mastersrv("CSISummaryDealer-getNationalCSIScoresMonth", parameters).then(function (response) {

            if (response.data.samplecount < 10) {
                $scope.samplespaceless = true;
            } else {
                $scope.samplespaceless = false;
            }
            $scope.currmonth = response.data.data[0].Wave;
            $scope.svg1 = response.data.data[0].Score;
            $scope.prevmonth = response.data.data[1].Wave;
            $scope.svg2 = response.data.data[1].Score;
            $scope.currquater = response.data.data[2].Wave;
            $scope.svg3 = response.data.data[2].Score;
            $scope.prevquater = response.data.data[3].Wave;
            $scope.svg4 = response.data.data[3].Score;


        });


        benchmarkparameters = [{"name": "benchmarkmeasure", "value": $scope.summarymeasure}, {"name": "langCode", "value": $scope.langcode}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "year", "value": $scope.year}, {"name": "benchmarkscope", "value": $scope.summaryscope}, {"name": "dealer", "value": $scope.dealer}, {"name": "region", "value": encodeURIComponent($scope.region)}];
        service.mastersrv("CSISummaryDealer-getCSIBenchmarkmonth", benchmarkparameters).then(function (response) {
            console.log(response.data);
            $scope.benchmark1 = response.data[0].Score;
            $scope.benchmarkcurrmonth = response.data[0].Wave;
            $scope.benchmark2 = response.data[1].Score;
            $scope.benchmarkprevmonth = response.data[1].Wave;
            $scope.benchmark3 = response.data[2].Score;
            $scope.benchmarkcurquater = response.data[2].Wave;
            $scope.benchmark4 = response.data[3].Score;
            $scope.benchmarkprevquater = response.data[3].Wave;
        })
    };

    //////////////////////////////////////////////////////////////
    if ($rootScope.monthchanged == true) {
        $scope.show4 = true;
        $scope.show3 = false;
        $scope.monthscore();


    } else
    if ($rootScope.biannualchanged == true)
    {
        $scope.show4 = false;
        $scope.show3 = true;
        $scope.biannualscore();
    } else
    {
        $scope.show4 = false;
        $scope.show3 = true;
        $scope.yearscore();

    }


//           $scope.benchmark=function(){
//               benchmarkparameters=[{"name":"benchmarkmeasure","value":$scope.summarymeasure},{"name":"langCode", "value": $scope.langcode},{"name": "year", "value": $scope.year},{"name":"benchmarkscope","value":$scope.summaryscope},{"name":"dealer","value":$scope.dealer},{"name":"region","value":encodeURIComponent($scope.region)}];
//               service.mastersrv("CSISummaryDealer-getCSIBenchmark", benchmarkparameters).then(function (response) {
//                        console.log(response.data);
//                        $scope.benchmark1=response.data[0].Score;
//                        $scope.benchmarkcurrmonth = response.data[0].Wave;
//                        $scope.benchmark2=response.data[1].Score;
//                        $scope.benchmarkprevmonth = response.data[1].Wave;
//                        $scope.benchmark3=response.data[2].Score;
//                        $scope.benchmarkprevquater = response.data[2].Wave;
//            })
//         }



    var parameters = [];

//        $rootScope.dealerScoreDealer = $rootScope.Filterarray.dealer[0];
    $rootScope.mapState = 0;

    parameters = [{"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "dealer", "value": $scope.dealer}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}, {"name": "factor", "value": $scope.factor}];

    $scope.getStyle = function () {
        var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

        return {
            'top': $scope.isSemi ? 'auto' : '50%',
            'bottom': $scope.isSemi ? '5%' : 'auto',
            'left': '53%',
            'transform': transform,
            '-moz-transform': transform,
            '-webkit-transform': transform,
            'font-size': $scope.radius / 3.5 + 'px'
        };
    };
    $scope.reggetStyle = function () {
        var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

        return {
            'top': $scope.isSemi ? 'auto' : '50%',
            'bottom': $scope.isSemi ? '5%' : 'auto',
            'left': '53%',
            'transform': transform,
            '-moz-transform': transform,
            '-webkit-transform': transform,
            'font-size': '36px'
        };
    };
    $scope.getStyle1 = function () {
        var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

        return {
            'top': $scope.isSemi ? 'auto' : '50%',
            'bottom': $scope.isSemi ? '5%' : 'auto',
            'left': '53%',
            'transform': transform,
            '-moz-transform': transform,
            '-webkit-transform': transform,
            'font-size': $scope.radius / 3.5 + 'px'
        };
    };


//        $scope.botgraph=function(){
//            
//                  service.mastersrv("CSISummaryDealer-getCSIRegionalAttributeAverage",parameters).then(function (response) {
//                            $('#sumtreecontainer').highcharts(service.barplainbar('bar',['#2979FF'],response.data,'Attribute Score',170,35,0,[0],5,false," "));
//                    });
//        }


    $scope.summarygraphs = function () {

        if ($scope.langcode == 'EN') {
            $scope.nationaltitle = " CSI";
            $scope.regionaltitle = $rootScope.Titlearray[2].ContentEN;
            $scope.factortitle = $rootScope.Titlearray[3].ContentEN;
            $scope.top5title = $rootScope.Titlearray[73].ContentEN;
            $scope.delightedtitle = $rootScope.Titlearray[10].ContentEN;
            $scope.footnote = $rootScope.Footarray[1].ContentEN;
            $scope.factorscore = "Factor Score";
            $scope.svgname = "CSI";
            $scope.benchmarktitle = " CSI Score";

        } else {
            $scope.nationaltitle = " CSI";
            $scope.regionaltitle = $rootScope.Titlearray[2].ContentReganal;
            $scope.factortitle = $rootScope.Titlearray[3].ContentReganal;
            $scope.top5title = $rootScope.Titlearray[73].ContentReganal;
            $scope.delightedtitle = $rootScope.Titlearray[10].ContentReganal;
            $scope.footnote = $rootScope.Footarray[1].ContentReganal;
            $scope.factorscore = "คะแนนปัจจัย";
            $scope.svgname = "CSI";
            $scope.benchmarktitle = " CSI";
        }

//            $scope.benchmark();
//           service.mastersrv("CSISummaryDealer-getCSIBenchmark", benchmarkparameters).then(function (response) {
//                    $scope.benchmark1=response.data[1].Score;
//                    $scope.benchmarkcurrmonth = response.data[1].Wave;
//                    $scope.benchmark2=response.data[0].Score;
//                    $scope.benchmarkprevmonth = response.data[0].Wave;
//                    $scope.benchmark3=response.data[2].Score;
//                    $scope.benchmarkprevquater = response.data[2].Wave;
//            })
//        

        //CSISummary-getNationalCSIScores
//            service.mastersrv("CSISummaryDealer-getNationalCSIScores", parameters).then(function (response) {
//
//                if (response.data.samplecount < 10) {
//                    $scope.samplespaceless = true;
////                    console.log($scope.samplespaceless);
//                } else {
//                    $scope.samplespaceless = false;
////                    console.log($scope.samplespaceless);
//                }
//                $scope.currmonth = response.data.data[0].Wave;
//                $scope.svg1 = response.data.data[0].Score;
//                $scope.prevmonth = response.data.data[1].Wave;
//                $scope.svg2 = response.data.data[1].Score;
//                $scope.prevquater = response.data.data[2].Wave;
//                $scope.svg3 = response.data.data[2].Score;
//
//
//            });

        service.mastersrv("CSISummaryDealer-getCSISummaryRegionFactorScores", parameters).then(function (response) {
            $('#dealersumbarlinecontainer').highcharts(service.summaryplainbar('column', ["#4292FF"], response.data, $scope.factorscore, 0, 45, .3, [1, 1, 1, 1, 1, 1], 5, false));

            if ($scope.langcode == 'EN') {
                $rootScope.legendname = response.data[0].scatter;
                $rootScope.legendnamew = response.data[0].scatter1;
            } else {
                $rootScope.legendname = response.data[0].scatter;
                $rootScope.legendnamew = response.data[0].scatter1;
            }
        });


        //SSIRegional-getSSIRegionalAttributeAverage
        service.mastersrv("CSISummaryDealer-getCSIRegionalAttributeAverage", parameters).then(function (response) {
            $('#dealersumtreecontainer').highcharts(service.barplainbar('bar', ['#2979FF'], response.data, 'Attribute Score', 170, 35, 0, [0], 5, false, " "));
        });


        //CSISummary-getCSITop5Bottom5SOP
        service.mastersrv("CSISummaryDealer-getCSITop5Bottom5SOP", parameters).then(function (response) {
            $('#dealersumbot1').highcharts(service.multbarchartsplit(105, 'bar', response.data, 180, 15, 0, 'CSI Score', 9, '8.5px', "%"));

        });



    };



    $scope.regionalgraphs = function () {
        $scope.state = "All";
        $scope.zone = "All";
        $scope.model = "All";
        $scope.year = '2016';
//            $scope.biannual = "Study Total";
//            $scope.month = "Study Total";
        $scope.legendname = "Study Best";
        $scope.legendnamew = "Worst Score";
        $scope.model = "All";
        $scope.city = "All";
        var parameters = [{"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "dealer", "value": $scope.dealer}, {"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}];

        if ($scope.langcode == 'EN') {
            $scope.regfacttitle = $rootScope.Titlearray[11].ContentEN;
            $scope.byregiontitle = $rootScope.Titlearray[72].ContentEN;
            $scope.meantitle = $rootScope.Titlearray[73].ContentEN;
            $scope.percenttitle = $rootScope.Titlearray[74].ContentEN;
            $scope.factorscore = "Factor Score";
            $scope.attscore = 'Attribute Score';
            $scope.footnote = $rootScope.Footarray[1].ContentEN;
        } else {
            $scope.regfacttitle = $rootScope.Titlearray[11].ContentReganal;
            $scope.byregiontitle = $rootScope.Titlearray[72].ContentReganal;
            $scope.meantitle = $rootScope.Titlearray[73].ContentReganal;
            $scope.percenttitle = $rootScope.Titlearray[74].ContentReganal;
            $scope.factorscore = "คะแนนปัจจัย";
            $scope.attscore = 'คุณลักษณะ คะแนน';
            $scope.footnote = $rootScope.Footarray[1].ContentReganal;
        }


        service.mastersrv("CSIRegionalDealer-getCSIFactorAverage", parameters).then(function (response) {
            console.log('response.data.data', response.data.data);
            console.log('response.data', response.data.samplecount);
            $scope.svgname = response.data.data[0].Factor;
            if (response.data.samplecount < 10) {
                $scope.samplespaceless = true;
                console.log($scope.samplespaceless);
            } else {
                $scope.samplespaceless = false;
                console.log($scope.samplespaceless);
            }

            if (response.data.data.length == 0) {
                response.data.data = [{"bartitle": "100%"}, {"Score": "0"}];
            }
            $scope.svgvalue = response.data.data[0].Score;
        });


        service.mastersrv("CSIRegionalDealer-getCSIRegionalFactorAverage", parameters).then(function (response) {
            $('#dealerregbarlinecontainer').highcharts(service.regplainbar('column', color, response.data, $scope.factorscore, 0, 60, .3, [0], 8, true));


        });


        service.mastersrv("CSIRegionalDealer-getCSIRegionalAttributeAverage", parameters).then(function (response) {
            $('#dealerregbot1').highcharts(service.barplainbar('bar', ['#2979FF'], response.data, $scope.attscore, 170, 35, 0, [0], 5, false, " "));
            $rootScope.legendforselectionb = response.data[0].scatter;
            $rootScope.legendforselectionw = response.data[0].scatter1;
        });


        service.mastersrv("CSIRegionalDealer-getCSIDelighted", parameters).then(function (response) {
            $('#dealerregbot2').highcharts(service.barplainbar('bar', ['#2979FF'], response.data, $scope.attscore, 170, 35, 0, [0], 5, false, "%"));

        });
    };



    $scope.trendanalysisgraphs = function () {
        parameters = [{"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "dealer", "value": $scope.dealer}, {"name": "month", "value": $scope.month}];

        if ($scope.langcode == 'EN') {
            $scope.trend1 = $rootScope.Titlearray[27].ContentEN;
            $scope.trend2 = $rootScope.Titlearray[75].ContentEN;
            $scope.trend3 = $rootScope.Titlearray[76].ContentEN;
            $scope.footnote = $rootScope.Footarray[1].ContentEN;
            $scope.factorscore = "Factor Score";
            $scope.csiscore = 'CSI Score';
        } else {
            $scope.trend1 = $rootScope.Titlearray[27].ContentReganal;
            $scope.trend2 = $rootScope.Titlearray[75].ContentReganal;
            $scope.trend3 = $rootScope.Titlearray[76].ContentReganal;
            $scope.footnote = $rootScope.Footarray[1].ContentReganal;
            $scope.factorscore = "คะแนนปัจจัย";
            $scope.csiscore = 'CSI คะแนน';
        }

        //  CSITrendAnalysis-getCSITrendByWave 1
        service.mastersrv("CSITrendAnalysis-getCSITrendByWave", parameters).then(function (response) {
            console.log('response.data', response.data);
            console.log('response.data.data', response.data.data);
            console.log('response.data.samplecount', response.data.samplecount);
            if (response.data.samplecount < 10) {
                $scope.samplespaceless = true;
                console.log($scope.samplespaceless);
            } else {
                $scope.samplespaceless = false;
                console.log($scope.samplespaceless);
            }

            $('#dealertatop1').highcharts(service.summaryplainbar('column', ['#2979FF'], response.data.data, $scope.csiscore, 0, 25, .3, [1, 1, 1, 1, 1, 1], 5, false));

            $rootScope.legendname = response.data[0].scatter;
            $rootScope.legendnamew = response.data[0].scatter1;
        });

        //  CSITrendAnalysis-getCSIFactorTrendByWave 2
        service.mastersrv("CSITrendAnalysis-getCSIFactorTrendByWave", parameters).then(function (response) {
            $('#dealertatop2').highcharts(service.summaryplainbar('column', ['#7E57C2'], response.data, $scope.factorscore, 0, 25, .3, [1, 1, 1, 1, 1, 1], 5, false));

            $rootScope.legendname = response.data[0].scatter;
            $rootScope.legendnamew = response.data[0].scatter1;
        });

        //  CSITrendAnalysis-getCSIAttributeTrendByWave 3
        service.mastersrv("CSITrendAnalysis-getCSIAttributeTrendByWave", parameters).then(function (response) {
            $('#dealertabot').highcharts(service.horizontalline(response.data, color));

        });
    };



    $scope.prioritygraphs = function () {
        $scope.langcode = 'EN'
//            $scope.biannual = "Study Total";
        $scope.radioSwitch = "Dealer Comparison";
        $scope.default = "dealer";
        $scope.dealer1 = $scope.dealer;
        var parameters = [{"name": "measure", "value": $scope.default}, {"name": "langCode", "value": $scope.langcode}, {"name": "dealer1", "value": $scope.dealer1}, {"name": "biannual", "value": $scope.biannual}];
        $scope.new1 = "new 111";
        if ($scope.langcode == 'EN') {
            $rootScope.choosecomp = $rootScope.Titlearray[40].ContentEN;
            $rootScope.regcomp = $rootScope.Titlearray[41].ContentEN;
            $rootScope.dealercomp = $rootScope.Titlearray[42].ContentEN;
            $rootScope.reg1 = $rootScope.Titlearray[43].ContentEN;
            $rootScope.reg2 = $rootScope.Titlearray[42].ContentEN;
            $rootScope.dealer1 = $rootScope.Titlearray[45].ContentEN;
            $rootScope.dealer2 = $rootScope.Titlearray[45].ContentEN;
            $scope.footnote = $rootScope.Footarray[1].ContentEN;
        } else {
            $rootScope.choosecomp = $rootScope.Titlearray[40].ContentReganal;
            $rootScope.regcomp = $rootScope.Titlearray[41].ContentReganal;
            $rootScope.dealercomp = $rootScope.Titlearray[42].ContentReganal;
            $rootScope.reg1 = $rootScope.Titlearray[43].ContentReganal;
            $rootScope.reg2 = $rootScope.Titlearray[42].ContentReganal;
            $rootScope.dealer1 = $rootScope.Titlearray[45].ContentReganal;
            $rootScope.dealer2 = $rootScope.Titlearray[45].ContentReganal;
            $scope.footnote = $rootScope.Footarray[1].ContentReganal;
        }

//           $scope.dataaa=[{"data":[{"name":"Ease of arranging service visit","tooltip":"Ease of arranging service visit:<br>-4.4079","y":-4.4079},{"name":"Flexibility to accommodate schedule","tooltip":"Flexibility to accommodate schedule:<br>-3.6386","y":-3.6386},{"name":"Timeliness of hand over process","tooltip":"Timeliness of hand over process:<br>-2.4440","y":-2.444},{"name":"Courtesy of Service Advisor","tooltip":"Courtesy of Service Advisor:<br>-2.3680","y":-2.368},{"name":"Responsiveness of Service Advisor","tooltip":"Responsiveness of Service Advisor:<br>-4.0104","y":-4.0104},{"name":"Thoroughness of Service Advisor explanations","tooltip":"Thoroughness of Service Advisor explanations:<br>-4.5301","y":-4.5301},{"name":"Ease of driving in/ out of facility","tooltip":"Ease of driving in/ out of facility:<br>-9.7150","y":-9.715},{"name":"Convenience of location","tooltip":"Convenience of location:<br>-6.7154","y":-6.7154},{"name":"Cleanliness of dealership","tooltip":"Cleanliness of dealership:<br>-1.4656","y":-1.4656},{"name":"Comfort of waiting area","tooltip":"Comfort of waiting area:<br>1.5707","y":1.5707},{"name":"Timeliness of the pick–up process","tooltip":"Timeliness of the pick–up process:<br>-3.6359","y":-3.6359},{"name":"Fairness of the charges","tooltip":"Fairness of the charges:<br>-2.9744","y":-2.9744},{"name":"Helpfulness of staff at pick–up","tooltip":"Helpfulness of staff at pick–up:<br>-3.5375","y":-3.5375},{"name":"Total time required to service your vehicle","tooltip":"Total time required to service your vehicle:<br>-4.4619","y":-4.4619},{"name":"Thoroughness of maintenance/ repair work performed","tooltip":"Thoroughness of maintenance/ repair work performed:<br>-4.5898","y":-4.5898},{"name":"Condition/ cleanliness of vehicle on return","tooltip":"Condition/ cleanliness of vehicle on return:<br>-4.3523","y":-4.3523}],"name":"Autowork"},{"data":[{"name":"Ease of arranging service visit","tooltip":"Ease of arranging service visit:<br>10.6235","y":10.6235},{"name":"Flexibility to accommodate schedule","tooltip":"Flexibility to accommodate schedule:<br>8.5607","y":8.5607},{"name":"Timeliness of hand over process","tooltip":"Timeliness of hand over process:<br>8.8435","y":8.8435},{"name":"Courtesy of Service Advisor","tooltip":"Courtesy of Service Advisor:<br>7.3242","y":7.3242},{"name":"Responsiveness of Service Advisor","tooltip":"Responsiveness of Service Advisor:<br>7.7319","y":7.7319},{"name":"Thoroughness of Service Advisor explanations","tooltip":"Thoroughness of Service Advisor explanations:<br>9.3525","y":9.3525},{"name":"Ease of driving in/ out of facility","tooltip":"Ease of driving in/ out of facility:<br>10.2095","y":10.2095},{"name":"Convenience of location","tooltip":"Convenience of location:<br>8.9693","y":8.9693},{"name":"Cleanliness of dealership","tooltip":"Cleanliness of dealership:<br>6.7323","y":6.7323},{"name":"Comfort of waiting area","tooltip":"Comfort of waiting area:<br>9.2495","y":9.2495},{"name":"Timeliness of the pick–up process","tooltip":"Timeliness of the pick–up process:<br>8.7888","y":8.7888},{"name":"Fairness of the charges","tooltip":"Fairness of the charges:<br>9.2815","y":9.2815},{"name":"Helpfulness of staff at pick–up","tooltip":"Helpfulness of staff at pick–up:<br>7.3956","y":7.3956},{"name":"Total time required to service your vehicle","tooltip":"Total time required to service your vehicle:<br>8.4695","y":8.4695},{"name":"Thoroughness of maintenance/ repair work performed","tooltip":"Thoroughness of maintenance/ repair work performed:<br>8.7535","y":8.7535},{"name":"Condition/ cleanliness of vehicle on return","tooltip":"Condition/ cleanliness of vehicle on return:<br>7.8493","y":7.8493}],"name":"Study Best"}]
//           console.log($scope.dataaa)  ;
//           $('#mainchart').highcharts(service.verticalline(color,true,$scope.dataaa,'left','top',true,'-50','50','2'));
//                       $('#mainchart1').highcharts(service.verticalline(color,true,$scope.dataaa,'left','top',true,'-50','50','2'));

        service.mastersrv("CSIPriorityDealer-getCSIPriority", parameters).then(function (response) {
            $('#dealerprioritymainchart').highcharts(service.verticalline(color, true, response.data, 'left', 'top', true, '-100', '100', '2'));

        });

        $scope.new2 = "new 1222";
    };

    $scope.sopperformance = function () {
        $scope.group1 = "Total";
        if ($scope.langcode == 'EN') {
            $scope.soptitle1 = $rootScope.Titlearray[46].ContentEN;
            $scope.soptitle2 = $rootScope.Titlearray[47].ContentEN;
            $scope.soptitle3 = $rootScope.Titlearray[48].ContentEN;
            $scope.opt1 = "Total";
            $scope.opt2 = "Zone";
            $scope.opt3 = "Region";
            $scope.sopfooter1 = $rootScope.Titlearray[50].ContentEN;
            $scope.sopfooter2 = $rootScope.Titlearray[51].ContentEN;
            $scope.sopfooter3 = $rootScope.Titlearray[52].ContentEN;
            $scope.sopfooter4 = $rootScope.Titlearray[49].ContentEN;
            $scope.footnote = $rootScope.Footarray[1].ContentEN;
            $scope.attscore = 'Selected Main Selection';
        } else {
            $scope.soptitle1 = $rootScope.Titlearray[46].ContentReganal;
            $scope.soptitle2 = $rootScope.Titlearray[47].ContentReganal;
            $scope.soptitle3 = $rootScope.Titlearray[48].ContentReganal;
            $scope.opt1 = "全部";
            $scope.opt2 = "โซน";
            $scope.opt3 = "ภูมิภาค";
            $scope.sopfooter1 = $rootScope.Titlearray[50].ContentReganal;
            $scope.sopfooter2 = $rootScope.Titlearray[51].ContentReganal;
            $scope.sopfooter3 = $rootScope.Titlearray[52].ContentReganal;
            $scope.sopfooter4 = $rootScope.Titlearray[49].ContentReganal;
            $scope.footnote = $rootScope.Footarray[1].ContentReganal;
            $scope.attscore = 'หัวข้อที่เลือก';
        }

        //        CSISOPPerformance-getCSISOPPerformance&compareto=region
        service.mastersrv("CSISOPPerformance-getCSISOPPerformance", parameters).then(function (response) {
//                 
            console.log('response.data', response.data);
            console.log('response.data.data', response.data.data);
            console.log('response.data.samplecount', response.data.samplecount);
            if (response.data.samplecount < 10) {
                $scope.samplespaceless = true;
                console.log($scope.samplespaceless);
            } else {
                $scope.samplespaceless = false;
                console.log($scope.samplespaceless);
            }
            if (response.data.data.length == 0) {
                response.data.data = [{"bartitle": "100%"}, {"y": "0"}];
            }
//                    
//                          $('#sop').highcharts(service.sopplainbar('bar','11px',color,response.data.data,'Attribute Score',margin,14,0,[0],5,false));
//                        $('#sop1').highcharts(service.sopplainbar('bar','9px',color,response.data.data,'Attribute Score',margin,25,0,[0],5,false));

            $('#dealersopsop').highcharts(service.sopplainbar('bar', '11px', color, response.data.data, $scope.attscore, margin, 14, 0, [0], 5, false));

        });
    };



    $scope.soptrendgraphs = function () {

        $scope.zone = "All";

        $scope.year = '2016';
//        $scope.biannual = "Study Total";
//        $scope.month = "Study Total";
        $scope.sopswitchoutput = 'total';
        $scope.sopswitchoutputcaps = 'Total';
        $scope.sop = 'Notified for routine maintenance^';
        var parameters = [{"name": "compareto", "value": $scope.sopswitchoutput}, {"name": "attribute", "value": encodeURIComponent($scope.sop)}, {"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "dealer", "value": $scope.dealer}, {"name": "dealer", "value": $scope.dealer}];

        if ($scope.langcode == 'EN') {
            $scope.sottitle1 = $rootScope.Titlearray[54].ContentEN;
            $scope.sottitle2 = $rootScope.Titlearray[56].ContentEN;
            $scope.sottitle3 = $rootScope.Titlearray[57].ContentEN;
            $scope.opt1 = "Total";
            $scope.opt2 = "Zone";
            $scope.opt3 = "Region";
            $scope.sotfooter1 = $rootScope.Titlearray[58].ContentEN;
            $scope.sotfooter2 = $rootScope.Titlearray[59].ContentEN;
            $scope.sotfooter3 = $rootScope.Titlearray[60].ContentEN;
            $scope.footnote = $rootScope.Footarray[1].ContentEN;
            $scope.graphname = 'Selected Main Selection';
        } else {
            $scope.sottitle1 = $rootScope.Titlearray[54].ContentReganal;
            $scope.sottitle2 = $rootScope.Titlearray[56].ContentReganal;
            $scope.sottitle3 = $rootScope.Titlearray[57].ContentReganal;
            $scope.opt1 = "全部";
            $scope.opt2 = "โซน";
            $scope.opt3 = "ภูมิภาค";
            $scope.sotfooter1 = $rootScope.Titlearray[58].ContentReganal;
            $scope.sotfooter2 = $rootScope.Titlearray[59].ContentReganal;
            $scope.sotfooter3 = $rootScope.Titlearray[60].ContentReganal;
            $scope.footnote = $rootScope.Footarray[1].ContentReganal;
            $scope.graphname = 'หัวข้อที่เลือก';
        }


        service.mastersrv("CSISOPPerformance-getCSISOPTrend", parameters).then(function (response) {
//                 console.log('response.data',response.data);
            console.log('response.data.data', response.data.data);
            console.log('response.data.samplecount', response.data.samplecount);
            if (response.data.samplecount < 10) {
                $scope.samplespaceless = true;
                console.log($scope.samplespaceless);
            } else {
                $scope.samplespaceless = false;
                console.log($scope.samplespaceless);
            }
            if (response.data.data.length == 0) {
                response.data.data = [{"bartitle": "100%"}, {"y": "0"}];
            }
            $('#dealersoptrendmainchart').highcharts(service.barline(['#2196F3'], response.data.data, $scope.graphname));

        });

    };



    $scope.ODgraphs = function () {
        if ($scope.langcode == 'EN') {
            $scope.othertitle1 = $rootScope.Titlearray[61].ContentEN;
            $scope.page1 = "Page 1";
            $scope.page2 = "Page 2";
            $scope.page3 = "Page 3";
            $scope.Questions = $rootScope.Titlearray[62].ContentEN;
            $scope.footer1 = "(1) Base: By Appointment";
            $scope.footer2 = "(2) Base: Vehicle ready on the same day of service";
            $scope.footer3 = "(3) Base: Contacted After Service";
        } else {
            $scope.othertitle1 = $rootScope.Titlearray[61].ContentReganal;
            $scope.page1 = "หน้า 1";
            $scope.page2 = "หน้า 2";
            $scope.page3 = "หน้า 3";
            $scope.Questions = $rootScope.Titlearray[62].ContentReganal;
            $scope.footer1 = "(1) หมายเหตุ: เฉพาะกลุ่มตัวอย่างที่ทำการนัดหมายล่วงหน้า";
            $scope.footer2 = "(2) หมายเหตุ: เฉพาะกลุ่มตัวอย่างที่ได้รับบริการเสร็จสิ้นภายในวันเดียวกัน";
            $scope.footer3 = "(3) หมายเหตุ: เฉพาะกลุ่มตัวอย่างที่ได้รับการติดต่อรับเข้ารับบริการ";
        }

        //       CSIOthers-getCSIOthersTable


        //CSIOthers-getCSIOthersCharts
        service.mastersrv("CSIOthers-getCSIOthersCharts", parameters).then(function (response) {
            $scope.response = response;


            $scope.heading0 = response.data[2].headingname;
            $('#dealerODtop01').highcharts(service.stackedgraph(response.data[2].headingdata, color));
            $scope.heading1 = response.data[3].headingname;
            $('#dealerODtop02').highcharts(service.stackedgraph(response.data[3].headingdata, color));
            $scope.heading2 = response.data[4].headingname;
            $('#dealerODbot01').highcharts(service.stackedgraph(response.data[4].headingdata, color));
            $scope.heading3 = response.data[7].headingname;
            $('#dealerODbot02').highcharts(service.stackedgraph(response.data[7].headingdata, color, 90));

            //page2
            $scope.heading4 = response.data[1].headingname;
            $('#dealerODtop11').highcharts(service.stackedgraph(response.data[1].headingdata, color));
            $scope.heading5 = response.data[0].headingname;
            $('#dealerODtop12').highcharts(service.stackedgraph(response.data[0].headingdata, color));

            //page3
            $scope.heading6 = response.data[6].headingname;
            $('#dealerODbot11').highcharts(service.stackedgraph(response.data[6].headingdata, color));
            $scope.heading7 = response.data[8].headingname;
            $('#dealerODbot12').highcharts(service.stackedgraph(response.data[8].headingdata, color));
            $scope.heading8 = response.data[5].headingname;
            $('#dealerODbot22').highcharts(service.stackedgraph(response.data[5].headingdata, color));



        });

    };



    $scope.LAAgraphs = function () {


        $scope.zone = "All";
        $scope.region = $rootScope.region;
        $scope.model = "All";
//        $scope.year='2016';

//        $scope.month = "Study Total";

        parameters = [{"name": "langCode", "value": $scope.langcode}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}];

        if ($scope.langcode == 'EN') {
            $scope.loyal1 = $rootScope.Titlearray[134].ContentEN;
            $scope.loyal2 = $rootScope.Titlearray[135].ContentEN;
            $scope.footnote = $rootScope.Footarray[1].ContentEN;
        } else {
            $scope.loyal1 = $rootScope.Titlearray[134].ContentReganal;
            $scope.loyal2 = $rootScope.Titlearray[135].ContentReganal;
            $scope.footnote = $rootScope.Footarray[1].ContentReganal;
        }



//            CSILoyaltyAndAdvocacy-getCSILoyaltyAndAdvocacy
        service.mastersrv("CSILoyaltyAndAdvocacy-getCSILoyaltyAndAdvocacy", parameters).then(function (response) {

            console.log('response.data', response.data);
            console.log('response.data.data', response.data);
            console.log('response.data', response.data.samplecount);
            if (response.data.samplecount < 10) {
                $scope.samplespaceless = true;
                console.log($scope.samplespaceless);
            } else {
                $scope.samplespaceless = false;
                console.log($scope.samplespaceless);
            }





            if (response.data.data.length == 0) {
                response.data.data = [{"bartitle": "100%"}, {"y": "0"}];
            }


            $scope.heading1 = response.data.data[0].chartheading;
            $('#dealerLAAtop1').highcharts(service.loyalitybar($scope.loyal1, response.data.data[0].chartdata, false, color));

            $scope.heading2 = response.data.data[1].chartheading;
            $('#dealerLAAtop2').highcharts(service.loyalitybar($scope.loyal2, response.data.data[1].chartdata, false, color));

        });

    };





};


