(function() {
    'use strict';

    angular
        .module('app.module2.page4')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

           $stateProvider
         .state('triangular.csipage4', {
            url: '/csipage4',
            views: {
                '': {
                   templateUrl: 'app/module2/page4/page4.tmpl.html',
                    controller: 'm2page4Controller',
                    controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module2/page4/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
            
        });

//       triMenuProvider.removeMenu('triangular.dealercsipage34'); 
//         triMenuProvider.addMenu({
//                    name: 'Summary',
//                    icon: 'zmdi zmdi-file-text',
//                    type: 'link',
//                    priority: 1.0,
//                    state: 'triangular.csipage4',
//                });  
  
                
    }
})();