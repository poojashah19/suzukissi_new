(function () {
    'use strict';

    angular
            .module('app.module2.page4')
            .controller('m2page4Controller', m2page4Controller);

    /* @ngInject */
    function m2page4Controller($scope, service, $http, $rootScope, $timeout) {
        var vm = this;

        $rootScope.langswitch = true
        $rootScope.lockLeft = true;
        var color = ["#002776", "#92D400", "#00A1DE", "#72C7E7", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.hideyear = false;
        $rootScope.hidebiannual = false;
        $rootScope.hidemonth = false;
        $rootScope.hidezone = false;
        $rootScope.hideregion = false;
        $rootScope.hidemodel = true;
        $rootScope.hidefactor = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.snaphidezone = true;
        $rootScope.snaphideregion = true;
        $rootScope.redalertdealerhide = true;
        $scope.show4 = true;
        $scope.show3 = false;
        $rootScope.redfeedback = "not";
        $scope.month = "Mar'17";
        $scope.yearset = false;
        $scope.biset = false;
        $scope.monthset = true;
        $rootScope.dealersnaphide = true;
        $rootScope.dealerhide = true;
        $rootScope.pagenumber = '1';
        $rootScope.starlabelhide = false;
        $rootScope.legendname = "Study Best";
        $rootScope.legendnamew = "Worst Score";
        $rootScope.priorityhide = true;
        $rootScope.mainselectionhide = false;
        $rootScope.filterSelShow = true;
        $rootScope.periodhide = true;
        $rootScope.loyaltydealerhide = true;
        $scope.langcode = $rootScope.language;
        $rootScope.modelsnap = "not";
        $scope.zone = "All";
        $scope.region = "All";
        $scope.model = $rootScope.model;
        $scope.year = $rootScope.year;
        $scope.biannual = "All";
        $scope.month = $rootScope.Filterarray.month[1].name;
        $scope.legendname = "Study Best";
        $scope.legendnamew = "Worst Score";
//        $rootScope.dtwave=$scope.waves;
        $rootScope.dealerScoreDealer = $rootScope.Filterarray.dealer[1].name;

        if ($rootScope.summaryoffset) {
            $rootScope.summaryoffset = false;
        } else {
            $rootScope.checkboxInitiliser();
        }


        if ($scope.langcode == 'EN') {
            $scope.nationaltitle = $rootScope.Titlearray[1].ContentEN;
            $scope.regionaltitle = $rootScope.Titlearray[2].ContentEN;
            $scope.factortitle = $rootScope.Titlearray[3].ContentEN;
            $scope.top5title = $rootScope.Titlearray[9].ContentEN;
            $scope.delightedtitle = $rootScope.Titlearray[10].ContentEN;
            $scope.footnote = $rootScope.Footarray[1].ContentEN;
            $scope.factorscore = "Factor Score";
            $scope.firstchart = "Note: Data updated up to the lastest available month";

        } else {
            $scope.nationaltitle = $rootScope.Titlearray[1].ContentReganal;
            $scope.regionaltitle = $rootScope.Titlearray[2].ContentReganal;
            $scope.factortitle = $rootScope.Titlearray[3].ContentReganal;
            $scope.top5title = $rootScope.Titlearray[9].ContentReganal;
            $scope.delightedtitle = $rootScope.Titlearray[10].ContentReganal;
            $scope.footnote = $rootScope.Footarray[1].ContentReganal;
            $scope.factorscore = "คะแนนปัจจัย";
            $scope.firstchart = "หมายเหตุ: แสดงเฉพาะข้อมูลล่าสุดที่มี";
        }



        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        console.log(" height :", h);
        console.log(" width :", w);
        var margin = 300;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 250;
        } else if (w < 960 && w > 600) {
            margin = 200;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 150;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        console.log('margin', margin);
        //////////////////////////////////////////////////////////////

        $scope.$on('changeLanguage', function (event, data) {
            $scope.langcode = data;
            $scope.filterchange();
//            alert($scope.langcode + "$scope.langcode ")
            $rootScope.languagechangernav();//langCode
        });


        $scope.$on('changeyear', function (event, data) {
            $scope.year = data;
            $scope.yearset = true;
            $scope.biset = false;
            $scope.monthset = false;
            $scope.show4 = false;
            $scope.show3 = true;
            $scope.biannual = "All";
            $scope.month = "All";
            $scope.filterchange();
        });

        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual = data;
            if ($scope.biannual == "All") {
                $scope.show4 = false;
                $scope.show3 = true;
                $scope.yearset = true;
                $scope.biset = false;
                $scope.monthset = false;
                $scope.yearscore();
            } else {
                $scope.show4 = false;
                $scope.show3 = true;
                $scope.yearset = false;
                $scope.biset = true;
                $scope.monthset = false;
                $scope.month = "All";
                $scope.filterchange();
            }

            $scope.month = "All";

        });

        $scope.$on('changemonth', function (event, data) {
            $scope.month = data;
            if ($scope.month == "All") {
                $scope.show4 = false;
                $scope.show3 = true;
                $scope.yearset = true;
                $scope.biset = false;
                $scope.monthset = false;
                $scope.yearscore();
            } else {
                $scope.show4 = true;
                $scope.show3 = false;
                $scope.yearset = false;
                $scope.biset = false;
                $scope.monthset = true;
                $scope.monthscore();
            }

            $scope.filterchange();
        });

        $scope.$on('changezone', function (event, data) {
            $scope.zone = data;
            $scope.region = "All";
            $scope.dealer = "All";
            $scope.filterchange();
        });

        $scope.$on('changeregion', function (event, data) {
            console.log("Change Region in 4" + data);
            $scope.region = data;
            $scope.dealer = "All";
            $scope.filterchange();
        });

        $scope.$on('changemodel', function (event, data) {
            $scope.model = data;
            $scope.filterchange();
        });
        //////////////////////////////////////////////////////////////


        var parameters = [];

//        $rootScope.dealerScoreDealer = $rootScope.Filterarray.dealer[0];
        $rootScope.mapState = 0;
//        
        parameters = [{"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}];

        $scope.getStyle = function () {
            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '53%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': $scope.radius / 3.5 + 'px'
            };
        };
        $scope.getStyle1 = function () {
            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '53%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': $scope.radius / 3.5 + 'px'
            };
        };



        $scope.yearscore = function () {
            //CSISummary-getNationalCSIScores
            service.mastersrv("CSISummary-getNationalCSIScoresYear", parameters).then(function (response) {

                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    //                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    //                    console.log($scope.samplespaceless);
                }

                console.log("yesr")
                console.log(response.data)
                $scope.currmonth = response.data.data[0].Wave;
                $scope.svg1 = response.data.data[0].Score;
                $scope.prevmonth = response.data.data[1].Wave;
                $scope.svg2 = response.data.data[1].Score;
                $scope.prevquater = response.data.data[2].Wave;
                $scope.svg3 = response.data.data[2].Score;


            });
        };

        $scope.biannualscore = function () {
            //CSISummary-getNationalCSIScores
            service.mastersrv("CSISummary-getNationalCSIScoresBiannual", parameters).then(function (response) {

                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    //                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    //                    console.log($scope.samplespaceless);
                }
                console.log("biannual")
                console.log(response.data)
                $scope.currmonth = response.data.data[0].Wave;
                $scope.svg1 = response.data.data[0].Score;
                if (response.data.data.length > 1) {
                    $scope.prevmonth = response.data.data[1].Wave;
                    $scope.svg2 = response.data.data[1].Score;
                } else
                {
                    $scope.prevmonth = null;
                    $scope.svg2 = null;

                }

                if (response.data.data.length > 2) {
                    $scope.prevquater = response.data.data[2].Wave;
                    $scope.svg3 = response.data.data[2].Score;
                } else
                {
                    $scope.prevquater = null
                    $scope.svg3 = null;
                }


            });
        };

        $scope.monthscore = function () {
            //CSISummary-getNationalCSIScores
            service.mastersrv("CSISummary-getNationalCSIScoresMonth", parameters).then(function (response) {

                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    //                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    //                    console.log($scope.samplespaceless);
                }
                $scope.currmonth = response.data.data[0].Wave;
                $scope.svg1 = response.data.data[0].Score;
                $scope.prevmonth = response.data.data[1].Wave;
                $scope.svg2 = response.data.data[1].Score;
                $scope.currquater = response.data.data[2].Wave;
                $scope.svg3 = response.data.data[2].Score;
                $scope.prevquater = response.data.data[3].Wave;
                $scope.svg4 = response.data.data[3].Score;

            });
        };


        $scope.graphs = function () {

            $scope.hideloader = false;
            if ($scope.langcode == 'EN') {
                $scope.nationaltitle = $rootScope.Titlearray[1].ContentEN;
                $scope.regionaltitle = $rootScope.Titlearray[2].ContentEN;
                $scope.factortitle = $rootScope.Titlearray[3].ContentEN;
                $scope.top5title = $rootScope.Titlearray[9].ContentEN;
                $scope.delightedtitle = $rootScope.Titlearray[10].ContentEN;
                $scope.footnote = $rootScope.Footarray[4].ContentEN;
                $scope.footnote += "<br> " + $rootScope.Footarray[2].ContentEN;
                $scope.footnote =  $rootScope.Footarray[0].ContentEN;
                $rootScope.overallfootnote = angular.copy($scope.footnote);

                $scope.footnotetitle = $rootScope.Footarray[4].ContentEN;
                $scope.footnotetitle += "\n " + $rootScope.Footarray[2].ContentEN;
                $scope.footnotetitle =  $rootScope.Footarray[0].ContentEN;
                $rootScope.overallfootnotetitle = angular.copy($scope.footnotetitle);

                $scope.factorscore = "Factor Score";
                $scope.firstchart = "Note: Data updated up to the lastest available month";

            } else {
                $scope.nationaltitle = $rootScope.Titlearray[1].ContentReganal;
                $scope.regionaltitle = $rootScope.Titlearray[2].ContentReganal;
                $scope.factortitle = $rootScope.Titlearray[3].ContentReganal;
                $scope.top5title = $rootScope.Titlearray[9].ContentReganal;
                $scope.delightedtitle = $rootScope.Titlearray[10].ContentReganal;
                $scope.footnote = $rootScope.Footarray[4].ContentReganal;

                $scope.footnote += "<br> " + $rootScope.Footarray[2].ContentReganal;
                $scope.footnote += "<br> " + $rootScope.Footarray[0].ContentReganal;
                $rootScope.overallfootnote = angular.copy($scope.footnote);

                $scope.footnotetitle = $rootScope.Footarray[4].ContentReganal;
                $scope.footnotetitle += "\n  " + $rootScope.Footarray[2].ContentReganal;
                $scope.footnotetitle += "\n  " + $rootScope.Footarray[0].ContentReganal;
                $rootScope.overallfootnotetitle = angular.copy($scope.footnotetitle);
                $scope.factorscore = "คะแนนปัจจัย";
                $scope.firstchart = "หมายเหตุ: แสดงเฉพาะข้อมูลล่าสุดที่มี";
            }



            //CSISummary-getCSISummaryRegionFactorScores


            service.mastersrv("CSISummary-getCSISummaryRegionFactorScores", parameters).then(function (response) {

                $timeout(function () {
                    $('#barlinecontainer').highcharts(service.creditsummaryplainbar($scope.firstchart, 'column', ["#4292FF"], response.data, $scope.factorscore, 0, 45, .3, [1, 1, 1, 1, 1, 1], 5, false));
                    $('#barlinecontainer1').highcharts(service.creditsummaryplainbar($scope.firstchart, 'column', ["#4292FF"], response.data, $scope.factorscore, 0, 45, .3, [1, 1, 1, 1, 1, 1], 5, false));
                }, 1000);
                if ($scope.langcode == 'EN') {
                    $rootScope.legendforselectionb = response.data[0].scatter;
                    $rootScope.legendforselectionw = response.data[0].scatter1;
                } else {
                    $rootScope.legendforselectionb = response.data[0].scatter;
                    $rootScope.legendforselectionw = response.data[0].scatter1;
                }
                $rootScope.legendforselectionb = response.data[0].scatter;
                $rootScope.legendforselectionw = response.data[0].scatter1;
            });


            //CSISummary-getCSITop5Bottom5
            service.mastersrv("CSISummary-getCSITop5Bottom5", parameters).then(function (response) {
                $timeout(function () {
                    $('#treecontainer').highcharts(service.multbarchart(1005, 'bar', response.data, 250, 15, 0, 'SOP Score', 10, '11px', " "));
                    $('#treecontainer1').highcharts(service.multbarchart(1005, 'bar', response.data, 250, 15, 0, 'SOP Score', 10, '11px', " "));
                }, 1000);
                if (response.data.length == 0) {
                    $scope.samplespaceless = true;
                } else {
                    $scope.samplespaceless = false;
                }

            });


            //CSISummary-getCSITop5Bottom5SOP
            service.mastersrv("CSISummary-getCSITop5Bottom5SOP", parameters).then(function (response) {
                $timeout(function () {
                    $('#bot1').highcharts(service.multbarchartsplit(100, 'bar', response.data, 250, 15, 0, 'CSI Score', 9, '8.5px', "%"));
                    $('#bot11').highcharts(service.multbarchartsplit(100, 'bar', response.data, 200, 15, 0, 'CSI Score', 9, '8.5px', "%"));
                }, 1000);
                if (response.data) {
                    $scope.hideloader = true;
                }
            });


            //CSISummary-RegionWiseCSI
            service.mastersrv("CSISummary-RegionWiseCSI", parameters).
                    then(function (response) {
                        $scope.mapData = response.data;
                        $scope.maprender();


                    });
        };


        var myData = [];
        $scope.printdata = function () {
            console.log("map data printing...");
            console.log("map data printed");
            $scope.mapData.forEach(function (obj2) {
                myData.push({region: obj2.region, score: obj2.value, repondents: obj2.repondents});
            });
            console.log("mapdata", myData);
            return myData;
        };


        $scope.maprender = function () {

            var latlong = {};


//            latlong["Bangkok & Greater"] = {"latitude": 13.756331, "longitude": 100.501765};//Bangkok & Greater
//            latlong["Central and East"] = {"latitude": 12.558601, "longitude": 103.322009}; //Central and East
//            latlong["Northeast"] = {"latitude": 17.364697, "longitude": 102.815892};//Northeast
//            latlong["North"] = {"latitude": 18.787747, "longitude": 98.993128}; //North
//            latlong["South"] = {"latitude": 8.508647, "longitude": 98.474688};//South
            latlong["Central East"] = {"latitude": 14.058601, "longitude": 101.052009,"color":'#AE79D3'};
            latlong["Central West"] = {"latitude": 14.856331, "longitude": 98.955765,"color":'#AE79D3'};
            latlong["Lower North"] = {"latitude": 17.057747, "longitude": 100.053128,"color":'#4ECFEF'};
            latlong["Northeast Low"] = {"latitude": 15.954697, "longitude": 104.055892,"color":'#8BEF4E'};
            latlong["Northeast Middle"] = {"latitude": 16.554697, "longitude": 102.855892,"color":'#8BEF4E'};
            latlong["Northeast Up"] = {"latitude": 17.954697, "longitude": 103.054688,"color":'#8BEF4E'};
            latlong["South"] = {"latitude": 8.558647, "longitude": 98.454688,"color":'#EDDA10'};
            latlong["Upper North"] = {"latitude": 19.157747, "longitude": 99.953128,"color":'#4ECFEF'};


            var mapcolor = ["#92D400", "#33B4E5",
                "#4200d4",
                "#c3d327", "#3C8A2E",
                "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];

            var map;
            var minBulletSize = 25;
            var maxBulletSize = 50;
            var min = Infinity;
            var max = -Infinity;

            for (var i = 0; i < $scope.mapData.length; i++) {
                var value = $scope.mapData[i].value;
                if (value < min) {
                    min = value;
                }
                if (value > max) {
                    max = value;
                }


            }

            if (min == max) {
                min = 0;
                max = value;
            }


            AmCharts.ready = function () {
                map = new AmCharts.AmMap();
                map.pathToImages = "http://www.amcharts.com/lib/3/images/";
                map.panEventsEnabled = false;
                map.dragMap = false;
                map.areasSettings = {
                    selectable: true,
                    unlistedAreasColor: "#EFEFEF",
                    unlistedAreasOutlineAlpha: .9,
                    unlistedAreasOutlineColor: "#7F7F7F"
                };
                map.imagesSettings.balloonText = "<span style='font-size:14px;' align='left'>[[title]]</span>";

                map.addListener("clickMapObject", function (event) {
                    console.log(event.mapObject.code);
                    console.log(event.mapObject.value);
                    if ($rootScope.mapState == 0) {
                        $scope.region = event.mapObject.code;
                        $scope.filterchange();
                        $rootScope.mapState = 1;
                    } else {
                        $scope.region = "All";
                        $scope.filterchange();
                        $rootScope.mapState = 0;
                    }


                });


                var dataProvider = {
                    mapVar: AmCharts.maps.thailandHigh,
                    //                       
                    images: []
                };

                map.zoomControl = {
                    panControlEnabled: false,
                    zoomControlEnabled: false,
                    homeButtonEnabled: false
                };


                // it's better to use circle square to show difference between values, not a radius
                var maxSquare = maxBulletSize * maxBulletSize * 2 * Math.PI;
                var minSquare = minBulletSize * minBulletSize * 2 * Math.PI;


                // create circle for each country
                for (var i = 0; i < $scope.mapData.length; i++) {
                    var dataItem = $scope.mapData[i];
                    var value = dataItem.value;
                    var datacolor = mapcolor[i];

                    // calculate size of a bubble   

                    var square = ((value - min) / (max - min) * (maxSquare - minSquare)) + minSquare;
                    if (square < minSquare) {
                        square = minSquare;
                    }
                    var size = Math.sqrt(square / (Math.PI * 2));
                    var id = dataItem.code;


                    dataProvider.images.push({
                        type: "bubble",
                        width: size,
                        height: size,
                        color: latlong[id].color,
                        longitude: latlong[id].longitude,
                        latitude: latlong[id].latitude,
                        label: (value),
                        alpha: .30,
                        code: dataItem.code,
                        labelPosition: "inside",
                        title: dataItem.name,
                        value: value,
                        rollOverScale: "1.3",
//                            outlineColor: "#104E8B",
                        outlineAlpha: ".5",
                        outlineThickness: "1",
                        selectable: true,
                        bringForwardOnHover: false,
                        selectedOutlineThickness: "5"

                    });
                }

                map.dataProvider = dataProvider;
                map.write("map");
            };


            AmCharts.ready1 = function () {
                map = new AmCharts.AmMap();
                map.pathToImages = "http://www.amcharts.com/lib/3/images/";
                map.panEventsEnabled = false;
                map.dragMap = false;
                map.areasSettings = {
                    selectable: true,
                    unlistedAreasColor: "#EFEFEF",
                    unlistedAreasOutlineAlpha: .9,
                    unlistedAreasOutlineColor: "#7F7F7F"
                };
                map.imagesSettings.balloonText = "<span style='font-size:14px;' align='left'>[[title]]</span>";

                map.addListener("clickMapObject", function (event) {
                    console.log(event.mapObject.code);
                    console.log(event.mapObject.value);
                    if ($rootScope.mapState == 0) {
                        $scope.region = event.mapObject.code;
                        $scope.filterchange();
                        $rootScope.mapState = 1;
                    } else {
                        $scope.region = "Study Total";
                        $scope.filterchange();
                        $rootScope.mapState = 0;
                    }


                });
                var dataProvider = {
                    mapVar: AmCharts.maps.thailandHigh,
                    images: []
                };
                map.zoomControl = {
                    panControlEnabled: false,
                    zoomControlEnabled: false,
                    homeButtonEnabled: false
                };
                var maxSquare = maxBulletSize * maxBulletSize * 2 * Math.PI;
                var minSquare = minBulletSize * minBulletSize * 2 * Math.PI;
                for (var i = 0; i < $scope.mapData.length; i++) {
                    var dataItem = $scope.mapData[i];
                    var value = dataItem.value;
                    var datacolor = mapcolor[i];
                    var square = ((value - min) / (max - min) * (maxSquare - minSquare)) + minSquare;
                    if (square < minSquare) {
                        square = minSquare;
                    }
                    var size = Math.sqrt(square / (Math.PI * 2));
                    var id = dataItem.code;
                    dataProvider.images.push({
                        type: "bubble",
                        width: size,
                        height: size,
                        color: datacolor,
                        longitude: latlong[id].longitude,
                        latitude: latlong[id].latitude,
                        label: (value),
                        alpha: .30,
                        code: dataItem.code,
                        labelPosition: "inside",
                        title: dataItem.name,
                        value: value,
                        rollOverScale: "1.0",
//                            outlineColor: "#104E8B",
                        outlineAlpha: ".5",
                        outlineThickness: "1",
                        selectable: true,
                        bringForwardOnHover: false,
                        selectedOutlineThickness: "5"
                    });
                }

                map.dataProvider = dataProvider;
                map.write("map1");
            };
            AmCharts.ready();
            AmCharts.ready1();
        };

        $scope.filterchange = function () {

            parameters = [
                {"name": "zone", "value": encodeURIComponent($scope.zone)},
                {"name": "langCode", "value": $scope.langcode},
                {"name": "region", "value": encodeURIComponent($scope.region)},
                {"name": "year", "value": $scope.year},
                {"name": "biannual", "value": $scope.biannual},
                {"name": "month", "value": $scope.month},
                {"name": "model", "value": $scope.model}
            ];
            if ($scope.yearset == true && $scope.biset == false && $scope.monthset == false) {

                $scope.yearscore();
            }
            if ($scope.yearset == false && $scope.biset == true && $scope.monthset == false) {

                $scope.biannualscore();
            }
            if ($scope.yearset == false && $scope.biset == false && $scope.monthset == true) {

                $scope.monthscore();
            }
            $scope.graphs();
        };

    }
})();

