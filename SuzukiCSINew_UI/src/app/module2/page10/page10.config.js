(function() {
    'use strict';

    angular
        .module('app.module2.page10')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

          $stateProvider
          .state('triangular.csipage10', {
            url: '/csipage10',
            views: {
                '': {
                   templateUrl: 'app/module2/page10/page10.tmpl.html',
            controller: 'm2page10Controller',
            controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module2/page10/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        });
        

//         triMenuProvider.removeMenu('triangular.dealercsipage310'); 
//         triMenuProvider.addMenu({
//                   name: 'SOP Performance',
//                    icon: 'zmdi zmdi-collection-text',
//                    type: 'link',
//                    priority: 1.4,
//                     state: 'triangular.csipage10',
//                });
    }
})();