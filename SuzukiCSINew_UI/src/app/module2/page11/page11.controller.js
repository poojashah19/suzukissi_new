(function () {
    'use strict';

    angular
            .module('app.module2.page11')
            .controller('m2page11Controller', m2page11Controller);

    /* @ngInject */
    function m2page11Controller($scope, $state, service, $http, $rootScope) {
        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776", "#92D400", "#00A1DE", "#72C7E7", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.hideyear = false;
        $rootScope.hidebiannual = false;
        $rootScope.hidemonth = false;
        $rootScope.hidezone = true;
        $rootScope.hideregion = true;
        $rootScope.snaphidezone = false;
        $rootScope.snaphideregion = false;
        $rootScope.hidemodel = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.hidefactor = false;
        $rootScope.dealerhide = true;
        $rootScope.dealersnaphide = false;
        $rootScope.pagenumber = '9';
        $rootScope.starlabelhide = false;
        $rootScope.priorityhide = true;
        $rootScope.loyaltydealerhide = true;

        $rootScope.redalertdealerhide = true;
        $rootScope.summaryRefresh = 1;
        $rootScope.dsnapshot = "dsnap";
        $rootScope.factor = "CSI";
        $scope.yearset = false;
        $scope.biset = false;
        $scope.monthset = true;
        $scope.factor = "CSI";
        $scope.factorname = $rootScope.Filterarray.factor[0].value;
        var parameters = [];
        $scope.langcode = $rootScope.language;
        $rootScope.redfeedback = "not";
        $rootScope.checkboxInitiliser();
        var languagepara = [];
        $scope.zone = "All";
        $scope.region = "All";
        $scope.model = "Study Total";
        $scope.year = $rootScope.year;
        $scope.biannual = "All";
        $scope.month = $rootScope.Filterarray.month[1].name;
        $scope.dealer = $rootScope.dealer;
        $scope.dealername = $rootScope.dealerScoreDealer;


        var modelpara = [];
        var languagepara = [];
        $scope.show4 = true;
        $scope.show3 = false;
        $rootScope.mainselectionhide = false;
        $rootScope.filterSelShow = true;
        $rootScope.periodhide = false;
        $scope.period = "Month";
        if ($scope.langcode == 'EN') {
            $scope.dsnap1 = $rootScope.Titlearray[92].ContentEN;
            $scope.dsnap2 = $rootScope.Titlearray[96].ContentEN;
            $scope.dsnap3 = $rootScope.Titlearray[137].ContentEN;//deligh
            $scope.dsnap4 = $rootScope.Titlearray[138].ContentEN;//trent
            $scope.dsnap5 = $rootScope.Titlearray[110].ContentEN;
            $scope.dsnapf6 = $rootScope.Titlearray[113].ContentEN;
            $scope.dsnapf7 = $rootScope.Titlearray[114].ContentEN;
            $scope.dsnapf8 = $rootScope.Titlearray[115].ContentEN;
            $scope.footnote = $rootScope.Footarray[0].ContentEN;
            $scope.factorscore = "Factor Score";
            $scope.sopscore = 'CSI Score';
            $scope.attscore = 'Attribute Score';
            $scope.factorname = "CSI";
            $scope.firstchart = "Note: Data updated up to the lastest available month";
            $scope.trendFootnotetitle = $rootScope.Footarray[19].ContentEN;

        } else {
            $scope.trendFootnotetitle = $rootScope.Footarray[19].ContentReganal;
            $scope.dsnap1 = $rootScope.Titlearray[92].ContentReganal;
            $scope.dsnap2 = $rootScope.Titlearray[96].ContentReganal;
            $scope.dsnap3 = $rootScope.Titlearray[137].ContentReganal;//deligh
            $scope.dsnap4 = $rootScope.Titlearray[138].ContentReganal;//trent
            $scope.dsnap5 = $rootScope.Titlearray[110].ContentReganal;
            $scope.dsnapf6 = $rootScope.Titlearray[113].ContentReganal;
            $scope.dsnapf7 = $rootScope.Titlearray[114].ContentReganal;
            $scope.dsnapf8 = $rootScope.Titlearray[115].ContentReganal;
            $scope.footnote = $rootScope.Footarray[0].ContentReganal;
            $scope.firstchart = "หมายเหตุ: แสดงเฉพาะข้อมูลล่าสุดที่มี";
            $scope.factorscore = "คะแนนปัจจัย";
            $scope.sopscore = 'SOP คะแนน';
            $scope.attscore = 'คุณลักษณะ คะแนน';

            modelpara = [{"name": "english", "value": $scope.dealer}, {"name": "moduleType", "value": "CSI"}];
            service.mastersrv("Systemuserview-getName", modelpara).then(function (response) {
                $scope.dealername = response.data[0].ContentReganal;

            });
            service.mastersrv("Systemuserview-getName", languagepara).then(function (response) {
                $scope.factorname = response.data[0].ContentReganal;
            });


        }


        $scope.dealer = $rootScope.dealerScoreDealer;
        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        console.log(" height :", h);
        console.log(" width :", w);
        var margin = 300;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 250;
        } else if (w < 960 && w > 600) {
            margin = 200;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 150;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        console.log('margin', margin);

        $scope.$on('changeLanguage', function (event, data) {
            $scope.langcode = data;

            modelpara = [{"name": "english", "value": $scope.dealer}, {"name": "moduleType", "value": "CSI"}];
            languagepara = [{"name": "english", "value": $scope.factor}, {"name": "moduleType", "value": "CSI"}];
            service.mastersrv("Systemuserview-getName", languagepara).then(function (response) {
                console.log('response response[0]', response.data[0].ContentReganal);
                if ($scope.langcode == 'EN') {
                    $scope.factorname = response.data[0].ContentEN;
                } else {
                    $scope.factorname = response.data[0].ContentReganal;
                }
            });
            service.mastersrv("Systemuserview-getName", modelpara).then(function (response) {
                console.log('response response[0]', response.data[0].ContentReganal);
                if ($scope.langcode == 'EN') {
                    $scope.dealername = response.data[0].ContentEN;
                } else {
                    $scope.dealername = response.data[0].ContentReganal;
                }
            });
            $scope.filterchange();
        });



        $scope.$on('changeyear', function (event, data) {
            $scope.year = data;
            $scope.show4 = false;
            $scope.show3 = true;
            $scope.yearset = true;
            $scope.biset = false;
            $scope.monthset = false;
            $scope.biannual = "All";
            $scope.month = $rootScope.Filterarray.month[1].name;
            $scope.filterchange();
//            $scope.yearscore();
        });

        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual = data;
            if ($scope.biannual == "All") {
                $scope.show4 = false;
                $scope.show3 = true;
                $scope.yearset = true;
                $scope.biset = false;
                $scope.monthset = false;
                $scope.yearscore();
            } else {
                $scope.show4 = false;
                $scope.show3 = true;
                $scope.yearset = false;
                $scope.biset = true;
                $scope.monthset = false;
                $scope.month = $rootScope.Filterarray.month[1].name;
                $scope.filterchange();
            }

            $scope.month = "Study Total";
        });

        $scope.$on('changemonth', function (event, data) {
            $scope.month = data;
            if ($scope.month == "All") {
                $scope.show4 = false;
                $scope.show3 = true;
                $scope.yearset = true;
                $scope.biset = false;
                $scope.monthset = false;
                $scope.yearscore();
            } else {
                $scope.show4 = true;
                $scope.show3 = false;
                $scope.yearset = false;
                $scope.biset = false;
                $scope.monthset = true;
                $scope.monthscore();
            }

            $scope.filterchange();
        });

        $scope.$on('changefactor', function (event, data) {
            $scope.factor = data;
            languagepara = [{"name": "english", "value": $scope.factor}, {"name": "moduleType", "value": "CSI"}];
            service.mastersrv("Systemuserview-getName", languagepara).then(function (response) {
                console.log('response response[0]', response.data[0].ContentReganal);
                if ($scope.langcode == 'EN') {
                    $scope.factorname = response.data[0].ContentEN;
                } else {
                    $scope.factorname = response.data[0].ContentReganal;
                }

            });
            $scope.filterchange();
        });

        $scope.$on('changefactordsnap', function (event, data) {
            $scope.factor = data;
            languagepara = [{"name": "english", "value": $scope.factor}, {"name": "moduleType", "value": "CSI"}];
            service.mastersrv("Systemuserview-getName", languagepara).then(function (response) {
                console.log('response response[0]', response.data[0].ContentReganal);
                if ($scope.langcode == 'EN') {
                    $scope.factorname = response.data[0].ContentEN;
                } else {
                    $scope.factorname = response.data[0].ContentReganal;
                }

            });
            $scope.loadgraphs();
        });

        $scope.$on('factorClick', function (event, data) {
            $scope.factor = data;
            console.log('state2 params:', $rootScope.factorClick);
            $scope.filterchange();
        });


        $scope.$on('snapzonechanged', function (event, data) {
            $scope.zone = data;
            if ($rootScope.language == 'EN') {
                var study = {"name": "Study Total", "value": "Study Total"};
                var all = {"name": "All", "value": "All"};
            } else if ($rootScope.language == 'TH') {
                study = {"name": "Study Total", "value": "ทั่วประเทศ"};
                all = {"name": "All", "value": "全部"};
            }
            var zoneobj = [{"name": "langCode", "value": $scope.langcode}, {"name": "zone", "value": encodeURIComponent($scope.zone)}];
            service.mastersrv("Filters-getRegion", zoneobj).then(function (result) {
                $rootScope.Filterarray.region = result.data;
                $rootScope.Filterarray.region.splice(0, 0, study);
                $scope.region = $rootScope.Filterarray.region[0].name;
            });

            service.mastersrv("Filters-getDealers", zoneobj).then(function (result) {
                $rootScope.Filterarray.dealerp = result.data;
                $scope.dealer = $rootScope.Filterarray.dealerp[0].name;
                $scope.dealername = $rootScope.Filterarray.dealerp[0].value;
                console.log("$rootScope.Filterarray.dealerp[0].value", $rootScope.Filterarray.dealerp[0].value);
                console.log("$rootScope.Filterarray.dealerp[0].value", $rootScope.Filterarray.dealerp[0].value)
                console.log("$rootScope.Filterarray.dealerp[0].value", $rootScope.Filterarray.dealerp[0].value)

                $scope.filterchange();
            });
        });

        $scope.$on('snapregionchanged', function (event, data) {
            $scope.region = data;
            if ($rootScope.language == 'EN') {
                var study = {"name": "Study Total", "value": "Study Total"};
                var all = {"name": "All", "value": "All"};
            } else if ($rootScope.language == 'TH') {
                study = {"name": "Study Total", "value": "ทั่วประเทศ"};
                all = {"name": "All", "value": "全部"};
            }
            var regionobj = [{"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}];
            service.mastersrv("Filters-getDealers", regionobj).then(function (result) {
                $rootScope.Filterarray.dealerp = result.data;
                $scope.dealer = $rootScope.Filterarray.dealerp[0].name;
                $scope.dealername = $rootScope.Filterarray.dealerp[0].value;
                console.log("$rootScope.Filterarray region.dealerp[0].value", $rootScope.Filterarray.dealerp[0].value);
                console.log("$rootScope.Filterarray reg.dealerp[0].value", $rootScope.Filterarray.dealerp[0].value)
                console.log("$rootScope.Filterarray reg.dealerp[0].value", $rootScope.Filterarray.dealerp[0].value)

                $scope.filterchange();
            });
        });

        $scope.$on('changedealer', function (event, data) {
            $scope.dealer = data;


            $scope.filterchange();
        });
        $scope.$on('dealersnapchanged', function (event, data) {



            $scope.dealer = data;
//            alert($scope.dealer )
            $scope.dealername = $scope.dealer;
            languagepara = [{"name": "english", "value": $scope.dealer}, {"name": "moduleType", "value": "CSI"}];
            service.mastersrv("Systemuserview-getName", languagepara).then(function (response) {
                console.log('response response[0]', response.data[0].ContentReganal);

                if ($scope.langcode == 'EN') {
                    $scope.dealername = response.data[0].ContentEN;
                    console.log('response $scope.dealername[0]', $scope.dealername);
                } else {
                    $scope.dealername = response.data[0].ContentReganal;
                    console.log('response $scope.dealername[0]', $scope.dealername);
                }
            });
            $scope.filterchange();
        });
        $scope.$on('changeperiod', function (event, data) {
            $scope.period = data;
            $scope.filterchange();
            $scope.year = "All";
        });
        //////////////////////////////////////////////////////////////


        $scope.factor = "CSI";
        parameters = [{"name": "period", "value": $scope.period}, {"name": "factor", "value": $scope.factor}, {"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "dealer", "value": $scope.dealer}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "year", "value": $scope.year}, {"name": "langCode", "value": $scope.langcode}];
        var regionobj = [{"name": "state", "value": "All"}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": "All"}];
        service.csi_filterfunction();
        service.mastersrv("Filters-getDealers", regionobj).then(function (result) {
            $rootScope.Filterarray.dealer = result.data;
        });




        $scope.yearscore = function () {
            //CSIDealerSnapshot-getDealerCSIScores
            service.mastersrv("CSIDealerSnapshot-getDealerCSIScoresYear", parameters).then(function (response) {
                console.log('response.data', response.data);
                console.log('response.data.data', response.data.data);
                console.log('response.data', response.data.samplecount);
                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    console.log($scope.samplespaceless);
                }
                $scope.currmonth = "";
                $scope.prevmonth = "";
                $scope.prevquater = "";

                $scope.svg1 = "";
                $scope.svg2 = "";
                $scope.svg3 = "";


                $scope.currmonth = response.data.data[0].Wave;
                $scope.svg1 = response.data.data[0].Score;
                $scope.prevmonth = response.data.data[1].Wave;
                $scope.svg2 = response.data.data[1].Score;
                if (!response.data.data[1].Wave) {
                    $scope.prevmonth = 0;
                    $scope.svg2 = 0;

                }
                $scope.prevquater = response.data.data[2].Wave;
                $scope.svg3 = response.data.data[2].Score;
            });
        };

        $scope.biannualscore = function () {
            //CSIDealerSnapshot-getDealerCSIScores
            service.mastersrv("CSIDealerSnapshot-getDealerCSIScoresBiannual", parameters).then(function (response) {
                console.log('response.data', response.data);
                console.log('response.data.data', response.data.data);
                console.log('response.data', response.data.samplecount);
                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    console.log($scope.samplespaceless);
                }
                $scope.currmonth = "";
                $scope.prevmonth = "";
                $scope.prevquater = "";
                $scope.svg1 = "";
                $scope.svg2 = "";
                $scope.svg3 = "";





                $scope.currmonth = response.data.data[0].Wave;
                $scope.svg1 = response.data.data[0].Score;
                $scope.prevmonth = response.data.data[1].Wave;
                $scope.svg2 = response.data.data[1].Score;
                if (!response.data.data[1].Wave) {
                    $scope.prevmonth = 0;
                    $scope.svg2 = 0;

                }
                $scope.prevquater = response.data.data[2].Wave;
                $scope.svg3 = response.data.data[2].Score;
            });
        };

        $scope.monthscore = function () {
            //CSIDealerSnapshot-getDealerCSIScores
            service.mastersrv("CSIDealerSnapshot-getDealerCSIScoresMonth", parameters).then(function (response) {
                console.log('response.data', response.data);
                console.log('response.data.data', response.data.data);
                console.log('response.data', response.data.samplecount);
                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    console.log($scope.samplespaceless);
                }
                $scope.currmonth = "";
                $scope.prevmonth = "";
                $scope.prevquater = "";
                $scope.currquater = "";
                $scope.svg1 = "";
                $scope.svg2 = "";
                $scope.svg3 = "";
                $scope.svg4 = "";
                $scope.currmonth = response.data.data[0].Wave;
                $scope.svg1 = response.data.data[0].Score;
                $scope.prevmonth = response.data.data[1].Wave;
                $scope.svg2 = response.data.data[1].Score;
                if (!response.data.data[1].Wave) {
                    $scope.prevmonth = 0;
                    $scope.svg2 = 0;

                }
                $scope.currquater = response.data.data[2].Wave;
                $scope.svg3 = response.data.data[2].Score;
                $scope.prevquater = response.data.data[3].Wave;
                $scope.svg4 = response.data.data[3].Score;
            });
        };








        $scope.graphs = function () {

            if ($scope.langcode == 'EN') {
                $scope.dsnap1 = $rootScope.Titlearray[92].ContentEN;
                $scope.dsnap2 = $rootScope.Titlearray[96].ContentEN;
                $scope.dsnap3 = $rootScope.Titlearray[137].ContentEN;//deligh
                $scope.dsnap4 = $rootScope.Titlearray[138].ContentEN;//trent
                $scope.dsnap5 = $rootScope.Titlearray[110].ContentEN;
                $scope.dsnapf6 = $rootScope.Titlearray[113].ContentEN;
                $scope.dsnapf7 = $rootScope.Titlearray[114].ContentEN;
                $scope.dsnapf8 = $rootScope.Titlearray[115].ContentEN;
                $scope.footnote = $rootScope.Footarray[4].ContentEN;
                $scope.footnote += "<br>" + $rootScope.Footarray[2].ContentEN;
                $scope.footnote += "<br>" + $rootScope.Footarray[3].ContentEN;
                $scope.footnote =  $rootScope.Footarray[0].ContentEN;

                $scope.footnotetitle = $rootScope.Footarray[4].ContentEN;
                $scope.footnotetitle += "\n" + $rootScope.Footarray[2].ContentEN;
                $scope.footnotetitle += "\n" + $rootScope.Footarray[3].ContentEN;
                $scope.footnotetitle =  $rootScope.Footarray[0].ContentEN;
                $scope.factorscore = "Factor Score";
                $scope.sopscore = 'SOP Score';
                $scope.attscore = 'Attribute Score';
                $scope.firstchart = "Note: Data updated up to the lastest available month";
                $scope.trendFootnotetitle = $rootScope.Footarray[19].ContentEN;
            } else {
                $scope.trendFootnotetitle = $rootScope.Footarray[19].ContentReganal;
                $scope.dsnap1 = $rootScope.Titlearray[92].ContentReganal;
                $scope.dsnap2 = $rootScope.Titlearray[96].ContentReganal;
                $scope.dsnap3 = $rootScope.Titlearray[137].ContentReganal;//deligh
                $scope.dsnap4 = $rootScope.Titlearray[138].ContentReganal;//trent
                $scope.dsnap5 = $rootScope.Titlearray[110].ContentReganal;
                $scope.dsnapf6 = $rootScope.Titlearray[113].ContentReganal;
                $scope.dsnapf7 = $rootScope.Titlearray[114].ContentReganal;
                $scope.dsnapf8 = $rootScope.Titlearray[115].ContentReganal;
                $scope.footnote = $rootScope.Footarray[4].ContentReganal;
                $scope.footnote += "<br>" + $rootScope.Footarray[2].ContentReganal;
                $scope.footnote += "<br>" + $rootScope.Footarray[3].ContentReganal;
                $scope.footnote += "<br>" + $rootScope.Footarray[0].ContentReganal;


                $scope.footnotetitle = $rootScope.Footarray[4].ContentReganal;
                $scope.footnotetitle += "\n" + $rootScope.Footarray[2].ContentReganal;
                $scope.footnotetitle += "\n" + $rootScope.Footarray[3].ContentReganal;
                $scope.footnotetitle += "\n" + $rootScope.Footarray[0].ContentReganal;
                $scope.factorscore = "คะแนนปัจจัย";
                $scope.sopscore = 'SOP คะแนน';
                $scope.attscore = 'คุณลักษณะ คะแนน';
                $scope.firstchart = "หมายเหตุ: แสดงเฉพาะข้อมูลล่าสุดที่มี";

            }



            console.log(parameters);


//                CSIDealerSnapshot-getCSIDealerFactorScores
            service.mastersrv("CSIDealerSnapshot-getCSIDealerFactorScores", parameters).then(function (response) {
                $('#top1').highcharts(service.clicksummaryplainbar($scope.firstchart, 'column', ['#2979FF'], response.data, $scope.factorscore, 0, 35, .2, [0], 3, false));
                $('#top11').highcharts(service.clicksummaryplainbar($scope.firstchart, 'column', ['#2979FF'], response.data, $scope.factorscore, 0, 35, .2, [0], 3, false));
//                        $scope.legendname=response.data[0].scatter;
//                        $scope.legendnamew=response.data[0].scatter1;
                $rootScope.legendforselectionb = response.data[0].scatter;
                $rootScope.legendforselectionw = response.data[0].scatter1;
            });
            //  CSIDealerSnapshot-getCSIDealerSOPTopBottom
            service.mastersrv("CSIDealerSnapshot-getCSIDealerSOPTopBottom", parameters).then(function (response) {
                $('#top2').highcharts(service.multbarchartsplit(105, 'bar', response.data, 250, 10, 10, $scope.sopscore, 9, '6.2px', "%"));
                $('#top21').highcharts(service.multbarchartsplit(105, 'bar', response.data, 250, 10, 10, $scope.sopscore, 9, '6.2px', "%"));
            });

            //line graph
            service.mastersrv("CSIDealerSnapshot-getCSIAttributeTrendByWave", parameters).then(function (response) {
                $('#dsnapbot1').highcharts(service.horizontalline(response.data, color));
                $('#dsnapbot11').highcharts(service.horizontalline(response.data, color));
            });

//delighted
            service.mastersrv("CSIDealerSnapshot-getCSIDelighted", parameters).then(function (response) {
                $('#bot2').highcharts(service.barplainbarsplit('bar', ['#2979FF'], response.data, $scope.attscore, 150, 35, 0, [0], 5, false, "%"));
                $('#bot21').highcharts(service.barplainbarsplit('bar', ['#2979FF'], response.data, $scope.attscore, 150, 35, 0, [0], 5, false, "%"));
            });


        };




        $scope.loadgraphs = function () {

            if ($scope.langcode == 'EN') {
                $scope.dsnap1 = $rootScope.Titlearray[92].ContentEN;
                $scope.dsnap2 = $rootScope.Titlearray[96].ContentEN;
                $scope.dsnap3 = $rootScope.Titlearray[139].ContentEN;//deligh
                $scope.dsnap4 = $rootScope.Titlearray[138].ContentEN;//trent
                $scope.dsnap5 = $rootScope.Titlearray[110].ContentEN;
                $scope.dsnapf6 = $rootScope.Titlearray[113].ContentEN;
                $scope.dsnapf7 = $rootScope.Titlearray[114].ContentEN;
                $scope.dsnapf8 = $rootScope.Titlearray[115].ContentEN;
                $scope.footnote = $rootScope.Footarray[4].ContentEN;
                $scope.footnote += "<br>" + $rootScope.Footarray[2].ContentEN;
                $scope.footnote += "<br>" + $rootScope.Footarray[3].ContentEN;
                $scope.footnote =  $rootScope.Footarray[0].ContentEN;

                $scope.footnotetitle = $rootScope.Footarray[4].ContentEN;
                $scope.footnotetitle += "\n" + $rootScope.Footarray[2].ContentEN;
                $scope.footnotetitle += "\n" + $rootScope.Footarray[3].ContentEN;
                $scope.footnotetitle =  $rootScope.Footarray[0].ContentEN;

                $scope.trendFootnotetitle = $rootScope.Footarray[19].ContentEN;

                $scope.factorscore = "Factor Score";
                $scope.sopscore = 'SOP Score';
                $scope.attscore = 'Attribute Score';
                $scope.firstchart = "Note: Data updated up to the lastest available month";

            } else {
                $scope.trendFootnotetitle = $rootScope.Footarray[19].ContentReganal;
                $scope.dsnap1 = $rootScope.Titlearray[92].ContentReganal;
                $scope.dsnap2 = $rootScope.Titlearray[96].ContentReganal;
                $scope.dsnap3 = $rootScope.Titlearray[139].ContentReganal;//deligh
                $scope.dsnap4 = $rootScope.Titlearray[138].ContentReganal;//trent
                $scope.dsnap5 = $rootScope.Titlearray[110].ContentReganal;
                $scope.dsnapf6 = $rootScope.Titlearray[113].ContentReganal;
                $scope.dsnapf7 = $rootScope.Titlearray[114].ContentReganal;
                $scope.dsnapf8 = $rootScope.Titlearray[115].ContentReganal;
                $scope.footnote = $rootScope.Footarray[4].ContentReganal;
                $scope.footnote += "<br>" + $rootScope.Footarray[2].ContentReganal;
                $scope.footnote += "<br>" + $rootScope.Footarray[3].ContentReganal;
                $scope.footnote += "<br>" + $rootScope.Footarray[0].ContentReganal;


                $scope.footnotetitle = $rootScope.Footarray[4].ContentReganal;
                $scope.footnotetitle += "\n" + $rootScope.Footarray[2].ContentReganal;
                $scope.footnotetitle += "\n" + $rootScope.Footarray[3].ContentReganal;
                $scope.footnotetitle += "\n" + $rootScope.Footarray[0].ContentReganal;


//                alert($scope.trendFootnotetitle )
                $scope.factorscore = "คะแนนปัจจัย";
                $scope.sopscore = 'SOP คะแนน';
                $scope.attscore = 'คุณลักษณะ คะแนน';
                $scope.firstchart = "หมายเหตุ: แสดงเฉพาะข้อมูลล่าสุดที่มี";

            }



            console.log(parameters);


            //  CSIDealerSnapshot-getCSIDealerSOPTopBottom
            service.mastersrv("CSIDealerSnapshot-getCSIDealerSOPTopBottom", parameters).then(function (response) {
                $('#top2').highcharts(service.multbarchartsplit(105, 'bar', response.data, 170, 10, 10, $scope.sopscore, 9, '6.2px', "%"));
                $('#top21').highcharts(service.multbarchartsplit(105, 'bar', response.data, 170, 10, 10, $scope.sopscore, 9, '6.2px', "%"));
            });

            //line graph
            service.mastersrv("CSIDealerSnapshot-getCSIAttributeTrendByWave", parameters).then(function (response) {
                $('#dsnapbot1').highcharts(service.horizontalline(response.data, color));
                $('#dsnapbot11').highcharts(service.horizontalline(response.data, color));
            });

//delighted
            service.mastersrv("CSIDealerSnapshot-getCSIDelighted", parameters).then(function (response) {
                $('#bot2').highcharts(service.barplainbarsplit('bar', ['#2979FF'], response.data, $scope.attscore, 250, 35, 0, [0], 5, false, "%"));
                $('#bot21').highcharts(service.barplainbarsplit('bar', ['#2979FF'], response.data, $scope.attscore, 250, 35, 0, [0], 5, false, "%"));
            });


        };



        $scope.getStyle = function () {
            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '50%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': $scope.radius / 3.5 + 'px'
            };
        };

        $scope.getStyle1 = function () {
            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '50%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': $scope.radius / 3.5 + 'px'
            };
        };


        $scope.getStyle2 = function () {
            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '50%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': $scope.radius / 4.5 + 'px'
            };
        };

        $scope.filterchange = function () {
            parameters = [{"name": "period", "value": $scope.period}, {"name": "year", "value": $scope.year}, {"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "langCode", "value": $scope.langcode}, {"name": "dealer", "value": $scope.dealer}, {"name": "factor", "value": $scope.factor}];
            console.log("dealer" + $scope.dealer);
//            alert("FilterChagne : " + $scope.dealername)
            if ($scope.yearset == true && $scope.biset == false && $scope.monthset == false) {

                $scope.yearscore();
            }
            if ($scope.yearset == false && $scope.biset == true && $scope.monthset == false) {

                $scope.biannualscore();
            }
            if ($scope.yearset == false && $scope.biset == false && $scope.monthset == true) {

                $scope.monthscore();
            }
            $scope.graphs();
        };
        $scope.navTrendAnalysis = function () {

            service.mastersrv("CSIDealerSnapshot-getCSIAttributeTrendByWave", parameters).then(function (response) {
                $('#dsnapbot1').highcharts(service.horizontalline(response.data, color));
                $('#dsnapbot11').highcharts(service.horizontalline(response.data, color));
            });
//            $state.go('triangular.csipage7', {});
        }
    }
})();