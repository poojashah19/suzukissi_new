
(function () {
    'use strict';

    angular
            .module('app.module2.page2_1')
            .controller('m2page2_1Controller', m2page2_1Controller);

    /* @ngInject */
    function m2page2_1Controller($scope, service, $state, $http, $rootScope) {

        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776", "#92D400", "#00A1DE", "#72C7E7", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.hideyear = false;
        $rootScope.hidebiannual = false;
        $rootScope.hidemonth = false;
        $rootScope.hidezone = false;
        $rootScope.hideregion = false;
        $rootScope.hidemodel = true;
        $rootScope.snaphidezone = true;
        $rootScope.snaphideregion = true;
        $rootScope.hidefactor = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.redalertdealerhide = true;
        $rootScope.dealerhide = false;
        $rootScope.dealersnaphide = true;
        $rootScope.pagenumber = '11';
        $rootScope.starlabelhide = true;
        $rootScope.priorityhide = true;
        $rootScope.summaryRefresh = 1;
        $rootScope.modelsnap = "not";
        $rootScope.dsnapshot = "not";
        $scope.langcode = $rootScope.language;
        $scope.dealer = "All";
        $rootScope.dealerScoreDealer = $rootScope.Filterarray.dealer[1].name;
        $rootScope.redfeedback = "not";
        $rootScope.checkboxInitiliser();
        $rootScope.mainselectionhide = false;
        $rootScope.filterSelShow = true;
        $rootScope.periodhide = true;
        $rootScope.loyaltydealerhide = true;
        if ($scope.langcode == 'EN') {
            $scope.loyal1 = $rootScope.Titlearray[134].ContentEN;
            $scope.loyal2 = $rootScope.Titlearray[135].ContentEN;
            $scope.footnote = $rootScope.Footarray[0].ContentEN;
        } else {
            $scope.loyal1 = $rootScope.Titlearray[134].ContentReganal;
            $scope.loyal2 = $rootScope.Titlearray[135].ContentReganal;
            $scope.footnote = $rootScope.Footarray[0].ContentReganal;
        }


        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        console.log(" height :", h);
        console.log(" width :", w);
        var margin = 300;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 250;
        } else if (w < 960 && w > 600) {
            margin = 200;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 150;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        console.log('margin', margin);
        //////////////////////////////////////////////////////////////

        $scope.$on('changeLanguage', function (event, data) {
            $scope.langcode = data;
            $scope.filterchange();
        });


        $scope.$on('changeyear', function (event, data) {
            $scope.year = data;
            $scope.biannual = "All";
            $scope.month = "All";
            $scope.filterchange();
        });


        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual = data;
            $scope.month = "All";
            $scope.filterchange();
        });

        $scope.$on('changemonth', function (event, data) {
            $scope.month = data;
            $scope.filterchange();
        });


        $scope.$on('changezone', function (event, data) {
            $scope.zone = data;
            $scope.region = "All";
            $scope.dealer = "All";
            $scope.filterchange();
        });

        $scope.$on('changeregion', function (event, data) {
            $scope.region = data;
            $scope.dealer = "All";
            $scope.filterchange();
        });

        $scope.$on('changemodel', function (event, data) {
            $scope.model = data;
            $scope.filterchange();
        });

        $scope.$on('changedealer', function (event, data) {
            $scope.dealer = data;
            $scope.filterchange();
        });




        //////////////////////////////////////////////////////////////


        var parameters = [];
        $scope.zone = "All";
        $scope.region = "All";
        $scope.model = "All";
        $scope.year = $rootScope.year;
        $scope.biannual = "All";
        $scope.month = $rootScope.Filterarray.month[1].name;
        service.csi_filterfunction();
        parameters = [{"name": "langCode", "value": $scope.langcode}, {"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "dealer", "value": $scope.dealer}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}];



        $scope.graphs = function () {

            if ($scope.langcode == 'EN') {
                $scope.loyal1 = "Would Recommend This Service Center to a Friend/ Relative";//$rootScope.Titlearray[134].ContentEN;
                $scope.loyal2 = "Would Use This Service Center to Service the Machine Again";//$rootScope.Titlearray[135].ContentEN; 
                $scope.footnote = $rootScope.Footarray[0].ContentEN;
                $scope.footnotetitle = $rootScope.Footarray[0].ContentEN;
            } else {
                $scope.loyal1 = "จะแนะนำศูนย์บริการนี้ให้กับเพื่อนหรือญาติ";//$rootScope.Titlearray[134].ContentReganal;
                $scope.loyal2 = "จะกลับมาใช้บริการที่ศูนย์บริการนี้อีกครั้ง";//$rootScope.Titlearray[135].ContentReganal;
                $scope.footnote = $rootScope.Footarray[0].ContentReganal;
                $scope.footnotetitle = $rootScope.Footarray[0].ContentReganal;
            }



//            CSILoyaltyAndAdvocacy-getCSILoyaltyAndAdvocacy
            service.mastersrv("CSILoyaltyAndAdvocacy-getCSILoyaltyAndAdvocacy", parameters).then(function (response) {

                console.log('response.data', response.data);
                console.log('response.data.data', response.data);
                console.log('response.data', response.data.samplecount);
                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    console.log($scope.samplespaceless);
                }

                var sampleCount = response.data.samplecount;
                var astersik = '';
                if (sampleCount < 10) {
                    astersik = '**';
                } else if (sampleCount >= 10 && sampleCount < 30) {
                    astersik = '*'
                }


                if (response.data.data.length == 0) {
                    response.data.data = [{"bartitle": "100%"}, {"y": "0"}];
                }

                var heading1 = $scope.loyal1 + astersik;
                var heading2 = $scope.loyal2 + astersik;
//                $scope.heading1 = response.data.data[0].chartheading;
                $('#top1').highcharts(service.loyalitybar(heading1, response.data.data[0].chartdata, false, color));

//                $scope.heading2 = response.data.data[1].chartheading;
                $('#top2').highcharts(service.loyalitybar(heading1, response.data.data[1].chartdata, false, color));

//            $scope.heading3=response.data.data[2].chartheading;
//                $('#top3').highcharts(service.loyalitybar($scope.heading3,response.data.data[2].chartdata,false,color));

//            $scope.heading4=response.data.data[3].chartheading;
//                $('#bot1').highcharts(service.loyalitybar($scope.heading4,response.data.data[3].chartdata,false,color));


//            obligatory xs size
//                $('#top1x').highcharts(service.loyalitybar($scope.heading1, response.data.data[0].chartdata, false, color));
//                $('#top2x').highcharts(service.loyalitybar($scope.heading2, response.data.data[1].chartdata, false, color));
//                $('#top3x').highcharts(service.loyalitybar($scope.heading3,response.data.data[2].chartdata,false,color));
//                $('#bot1x').highcharts(service.loyalitybar($scope.heading4,response.data.data[3].chartdata,false,color));

            });

        };

        $scope.filterchange = function () {
            parameters = [{"name": "langCode", "value": $scope.langcode}, {"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "dealer", "value": $scope.dealer}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}];
            $scope.graphs();
        };


    }
})();