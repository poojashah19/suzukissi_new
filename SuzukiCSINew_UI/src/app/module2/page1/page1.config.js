(function() {
    'use strict';

    angular
        .module('app.module2.page1')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {

          $stateProvider
         .state('triangular.csipage1', {
            url: '/csipage1',
            views: {
                '': {
                   templateUrl: 'app/module2/page1/page1.tmpl.html',
                    controller: 'm2page1Controller',
                    controllerAs: 'vm',
                    
                },
                 'belowContent': {
                    templateUrl: 'app/module2/page1/fab-button.tmpl.html',
                    controller: 'SalesFabController',
                    controllerAs: 'vm'
                }
                 
            },
            data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
            
        });
        

//        triMenuProvider.removeMenu('triangular.dealercsipage31'); 
//         triMenuProvider.addMenu({
//                    name: 'Priority Analysis',
//                    icon: 'zmdi zmdi-badge-check',
//                    type: 'link',
//                    priority: 1.3,
//                     state: 'triangular.csipage1',
//                });
//  
                
    }
})();