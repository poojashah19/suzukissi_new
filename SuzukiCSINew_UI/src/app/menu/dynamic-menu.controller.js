(function() {
    'use strict';

    angular
        .module('menu')
        .controller('MenuDynamicController', MenuDynamicController);

    /* @ngInject */
    function MenuDynamicController(dynamicMenuService, triMenu,$rootScope,$state,$scope) {
        var vm = this;
        // get dynamic menu service to store & keep track the state of the menu status
        vm.dynamicMenu = dynamicMenuService.dynamicMenu;
        // create toggle function
        vm.toggleExtraMenu = toggleExtraMenu;
        $rootScope.language = 'EN';
         var summary="Summary";
           var  regional="Regional Analysis";
          var   trend="Trend Analysis";
          var   priority="Priority Analysis";
          var   sopperf="SOP Performance";
          var   soptrend="SOP Trend";
          var   others="Other Diagnostics";
          var   dscore="Dealer Ranking";
          var   dsnap="Dealer Snapshot";
          var   model="Product Analysis";
          var   loyal="Loyalty and Advocacy";
          var   red="Red Alert";
          var   ofeed="Other Feedback";
          var   individ="Downloadable Dealer PDF";
          var   topline="Topline";
        vm.tempfunction = tempfunction;
        
        tempfunction($rootScope.reporttype);
        
        
         $scope.$on('changeLanguage', function (event, data) {
             tempfunction($rootScope.reporttype);
            
          });
        
        
        
        function toggleExtraMenu(showMenu) {
            if(showMenu=='oem') {
                 triMenu.removeMenu('triangular.csipage4');
                triMenu.removeMenu('triangular.csipage8');
                triMenu.removeMenu('triangular.csipage7');
                triMenu.removeMenu('triangular.csipage1');
                triMenu.removeMenu('triangular.csipage10');
                triMenu.removeMenu('triangular.csipage9');
                triMenu.removeMenu('triangular.csipage5');
                triMenu.removeMenu('triangular.csipage3');
                triMenu.removeMenu('triangular.csipage11');
                triMenu.removeMenu('triangular.csipage6');
                triMenu.removeMenu('triangular.csipage2_1');
                triMenu.removeMenu('triangular.csipage12');
                triMenu.removeMenu('triangular.csipage13');
                triMenu.removeMenu('triangular.csipage14');
                triMenu.removeMenu('triangular.csipage15');
                triMenu.removeMenu('triangular.dealercsipage10');
                triMenu.removeMenu('triangular.dealercsipage1');
                triMenu.removeMenu('triangular.dealercsipage2');
                triMenu.removeMenu('triangular.dealercsipage3');
                triMenu.removeMenu('triangular.dealercsipage4');
                triMenu.removeMenu('triangular.dealercsipage5');
                triMenu.removeMenu('triangular.dealercsipage7');
                triMenu.removeMenu('triangular.dealercsipage8');
                triMenu.removeMenu('triangular.dealercsipage9');
                triMenu.removeMenu('triangular.dealercsipage14');
                triMenu.removeMenu('triangular.dealercsipage15');
//                 console.log('inside oem',showMenu);
                 
                 
                
                 if($rootScope.language == 'EN') {
                      summary="Summary";
                      regional="Regional Analysis";
                      trend="Trend Analysis";
                      priority="Priority Analysis";
                      sopperf="SOP Performance";
                      soptrend="SOP Trend";
                      others="Other Diagnostics";
                      dscore="Dealer Ranking";
                      dsnap="Dealer Snapshot";
                      model="Product Analysis";
                      loyal="Loyalty and Advocacy";
                      red="Red Alert";
                      ofeed="Other Feedback";
                      individ="Downloadable Dealer PDF";
                      topline="Topline";
                 }
                 else  if($rootScope.language == 'TH'){
                       summary="ผลสรุป";
                       regional="การวิเคราะห์รายภูมิภาค";
                       trend="การวิเคราะห์แนวโน้ม";
                       priority="การวิเคราะห์ลำดับ \nความสำคัญ";
                       sopperf="ผลการปฏิบัติงาน \nมาตรฐานการดำเนินงาน";
                       soptrend="แนวโน้ม \nผลการปฏิบัติงาน";
                       others="ข้อมูลอื่นๆ เพิ่มเติม";
                       dscore="การจัดอันดับคะแนน \nของศูนย์บริการ";
                       dsnap="ภาพรวมของศูนย์บริการ";
                       model="การวิเคราะห์ \nายเครื่องจักรกล";
                       loyal="ความภักดีและ \nการบอกต่อ";
                       red="เหตุผลการให้คะแนน \nโดยรวมต่ำ";
                       ofeed="ข้อเสนอแนะอื่นๆ";
                       individ="รายงานศูนย์บริการ (PDF)";
                       topline="รายงานเบื้องต้น (Topline)";
                 }
                triMenu.addMenu({
                    name: summary,
                    icon: 'zmdi zmdi-file-text',
                    type: 'link',
                    priority: 1.0,
                    state: 'triangular.csipage4'
                });  
                triMenu.addMenu({
                   name: regional,
                    icon: 'zmdi zmdi-map',
                    type: 'link',
                    priority: 1.1,
                     state: 'triangular.csipage8'
                });
                triMenu.addMenu({
                   name: trend,
                    icon: 'zmdi zmdi-menu',
                    type: 'link',
                    priority: 1.2,
                     state: 'triangular.csipage7'
                }); 
                triMenu.addMenu({
                    name: priority,
                    icon: 'zmdi zmdi-badge-check',
                    type: 'link',
                    priority: 1.3,
                     state: 'triangular.csipage1'
                });
               
                triMenu.addMenu({
                   name: sopperf,
                    icon: 'zmdi zmdi-collection-text',
                    type: 'link',
                    priority: 1.4,
                     state: 'triangular.csipage10'
                });
                 triMenu.addMenu({
                   name: soptrend,
                    icon: 'zmdi zmdi-hourglass',
                    type: 'link',
                    priority: 1.5,
                     state: 'triangular.csipage9'
                });  
             triMenu.addMenu({
                   name: others,
                    icon: 'zmdi zmdi-assignment-o',
                    type: 'link',
                    priority: 1.6,
                     state: 'triangular.csipage5'
                });  
            triMenu.addMenu({
                      name: dscore,
                    icon: 'zmdi zmdi-view-list-alt',
                    type: 'link',
                    priority: 1.7,
                     state: 'triangular.csipage3'
                });
             triMenu.addMenu({
                   name: dsnap,
                    icon: 'zmdi zmdi-picture-in-picture',
                    type: 'link',
                    priority: 1.8,
                     state: 'triangular.csipage11'
                });
            triMenu.addMenu({
                   name: model,
                    icon: 'zmdi zmdi-car-taxi',
                    type: 'link',
                    priority: 1.9,
                     state: 'triangular.csipage6'
                });
            triMenu.addMenu({
                    name: loyal,
                    icon: 'zmdi zmdi-favorite',
                    type: 'link',
                    priority: 2.0,
                     state: 'triangular.csipage2_1'
                }); 
//                triMenu.addMenu({
//                      name: topline,
//                    icon: 'zmdi zmdi-border-top',
//                    type: 'link',
//                    priority: 2.5,
//                     state: 'triangular.csipage12'
//                });
//                triMenu.addMenu({
//                      name: individ,
//                    icon: 'zmdi zmdi-cloud-download',
//                    type: 'link',
//                    priority: 2.4,
//                     state: 'triangular.csipage13'
//                });
                triMenu.addMenu({
                      name: ofeed,
                    icon: 'zmdi zmdi-mood',
                    type: 'link',
                    priority: 2.3,
                     state: 'triangular.csipage14'
                });
                triMenu.addMenu({
                      name: red,
                    icon: 'zmdi zmdi-alert-circle',
                    type: 'link',
                    priority: 2.2,
                     state: 'triangular.csipage15'
                });

                triMenu.removeMenu('triangular.dealercsipage10');
                triMenu.removeMenu('triangular.dealercsipage1');
                triMenu.removeMenu('triangular.dealercsipage2');
                triMenu.removeMenu('triangular.dealercsipage4');
                triMenu.removeMenu('triangular.dealercsipage5');
                triMenu.removeMenu('triangular.dealercsipage7');
                triMenu.removeMenu('triangular.dealercsipage9');
                triMenu.removeMenu('triangular.dealercsipage3');
                triMenu.removeMenu('triangular.dealercsipage8');
                triMenu.removeMenu('triangular.dealercsipage14');
                triMenu.removeMenu('triangular.dealercsipage15');
            }
            
            else  if(showMenu=='dealer')  {
                 triMenu.removeMenu('triangular.csipage4');
                triMenu.removeMenu('triangular.csipage8');
                triMenu.removeMenu('triangular.csipage7');
                triMenu.removeMenu('triangular.csipage1');
                triMenu.removeMenu('triangular.csipage10');
                triMenu.removeMenu('triangular.csipage9');
                triMenu.removeMenu('triangular.csipage5');
                triMenu.removeMenu('triangular.csipage3');
                triMenu.removeMenu('triangular.csipage11');
                triMenu.removeMenu('triangular.csipage6');
                triMenu.removeMenu('triangular.csipage2_1');
                triMenu.removeMenu('triangular.csipage12');
                triMenu.removeMenu('triangular.csipage13');
                triMenu.removeMenu('triangular.csipage14');
                triMenu.removeMenu('triangular.csipage15');
                triMenu.removeMenu('triangular.dealercsipage10');
                triMenu.removeMenu('triangular.dealercsipage1');
                triMenu.removeMenu('triangular.dealercsipage2');
                triMenu.removeMenu('triangular.dealercsipage4');
                triMenu.removeMenu('triangular.dealercsipage5');
                triMenu.removeMenu('triangular.dealercsipage7');
                triMenu.removeMenu('triangular.dealercsipage9');
                triMenu.removeMenu('triangular.dealercsipage3');
                triMenu.removeMenu('triangular.dealercsipage8');
                triMenu.removeMenu('triangular.dealercsipage14');
                triMenu.removeMenu('triangular.dealercsipage15');
//                console.log('inside dealer',showMenu);


                 if($rootScope.language == 'EN') {
                      summary="Summary";
                      regional="Regional Analysis";
                      trend="Trend Analysis";
                      priority="Priority Analysis";
                      sopperf="SOP Performance";
                      soptrend="SOP Trend";
                      others="Other Diagnostics";
                      dscore="Dealer Ranking";
                      dsnap="Dealer Snapshot";
                      model="Product Analysis";
                      loyal="Loyalty and Advocacy";
                      red="Red Alert";
                      ofeed="Other Feedback";
                      individ="Downloadable Dealer PDF";
                      topline="Topline";
                 }
                 else  if($rootScope.language == 'TH'){
                        summary="ผลสรุป";
                        regional="การวิเคราะห์รายภูมิภาค";
                        trend="การวิเคราะห์แนวโน้ม";
                        priority="การวิเคราะห์ลำดับ \nความสำคัญ";
                        sopperf="ผลการปฏิบัติงาน \nมาตรฐานการดำเนินงาน";
                        soptrend="แนวโน้ม \nผลการปฏิบัติงาน";
                        others="ข้อมูลอื่นๆ เพิ่มเติม";
                        dscore="การจัดอันดับคะแนน \nของศูนย์บริการ";
                        dsnap="ภาพรวมของศูนย์บริการ";
                        model="การวิเคราะห์ \nรายรุ่นรถ";
                        loyal="ความภักดีและ \nการบอกต่อ";
                        red="เหตุผลการให้คะแนน \nโดยรวมต่ำ";
                        ofeed="ข้อเสนอแนะอื่นๆ";
                        individ="รายงานศูนย์บริการ (PDF)";
                        topline="รายงานเบื้องต้น (Topline)";
                 }
                    triMenu.addMenu({
                        name: summary,
                        icon: 'zmdi zmdi-file-text',
                        type: 'link',
                        priority: 1.0,
                        state: 'triangular.dealercsipage4'
                    });  
                   
                    triMenu.addMenu({
                       name: trend,
                        icon: 'zmdi zmdi-menu',
                        type: 'link',
                        priority: 1.2,
                         state: 'triangular.dealercsipage7'
                    }); 
                    triMenu.addMenu({
                        name: priority,
                        icon: 'zmdi zmdi-badge-check',
                        type: 'link',
                        priority: 1.3,
                         state: 'triangular.dealercsipage1'
                    });

                    triMenu.addMenu({
                       name: sopperf,
                        icon: 'zmdi zmdi-collection-text',
                        type: 'link',
                        priority: 1.4,
                         state: 'triangular.dealercsipage10'
                    });
                     triMenu.addMenu({
                       name: soptrend,
                        icon: 'zmdi zmdi-hourglass',
                        type: 'link',
                        priority: 1.5,
                         state: 'triangular.dealercsipage9'
                    });  
                 triMenu.addMenu({
                       name: others,
                        icon: 'zmdi zmdi-assignment-o',
                        type: 'link',
                        priority: 1.6,
                         state: 'triangular.dealercsipage5'
                    });  
               
                triMenu.addMenu({
                        name: loyal,
                        icon: 'zmdi zmdi-favorite',
                        type: 'link',
                        priority: 2.0,
                         state: 'triangular.dealercsipage2'
                    }); 
                    
                triMenu.addMenu({
                      name: ofeed,
                    icon: 'zmdi zmdi-mood',
                    type: 'link',
                    priority: 2.3,
                     state: 'triangular.dealercsipage14'
                });
                triMenu.addMenu({
                      name: red,
                    icon: 'zmdi zmdi-alert-circle',
                    type: 'link',
                    priority: 2.2,
                     state: 'triangular.dealercsipage15'
                });
                 triMenu.addMenu({
                      name: dscore,
                    icon: 'zmdi zmdi-view-list-alt',
                    type: 'link',
                    priority: 1.7,
                     state: 'triangular.dealercsipage3'
                });
                triMenu.addMenu({
                   name: regional,
                    icon: 'zmdi zmdi-map',
                    type: 'link',
                    priority: 1.1,
                     state: 'triangular.dealercsipage8'
                });
                triMenu.removeMenu('triangular.csipage4');
                triMenu.removeMenu('triangular.csipage8');
                triMenu.removeMenu('triangular.csipage7');
                triMenu.removeMenu('triangular.csipage1');
                triMenu.removeMenu('triangular.csipage10');
                triMenu.removeMenu('triangular.csipage9');
                triMenu.removeMenu('triangular.csipage5');
                triMenu.removeMenu('triangular.csipage3');
                triMenu.removeMenu('triangular.csipage11');
                triMenu.removeMenu('triangular.csipage6');
                triMenu.removeMenu('triangular.csipage2_1');
                triMenu.removeMenu('triangular.csipage12');
                triMenu.removeMenu('triangular.csipage13');
                triMenu.removeMenu('triangular.csipage14');
                triMenu.removeMenu('triangular.csipage15');
            }
        }
        
         function tempfunction(oemdealer){
//                 console.log(' tempfuncdastion($rootScope.language);' ,$rootScope.language);
//             console.log(' tempfunctadsion($rootScope.language);' ,$rootScope.language);
//             console.log(' tempfunctasdion($rootScope.language);' ,$rootScope.language);
//             console.log(' tempfuncasdtion($rootScope.language);' ,$rootScope.language);
                if(oemdealer){
                        toggleExtraMenu('oem');
//                        console.log('oem',oemdealer,$rootScope.reporttype);
                }
                else{
                        toggleExtraMenu('dealer');
//                        console.log('dealer',oemdealer,$rootScope.reporttype);
                }
        }
    }
})();