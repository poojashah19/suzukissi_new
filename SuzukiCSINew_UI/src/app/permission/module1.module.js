(function() {
    'use strict';

    angular
        .module('app.module1', [
                'app.module1.page1',
                'app.module1.page3',
                'app.module1.page4',
                'app.module1.page5',
                'app.module1.page2_1',
                'app.module1.page6',
                'app.module1.page7',
                'app.module1.page9',
                'app.module1.page8',
                'app.module1.page10',
                'app.module1.page11'
          ]);
})();