(function () {
    'use strict';

    angular
            .module('app.translate')
            .config(translateConfig);

    /* @ngInject */
    function translateConfig($translateProvider, $translatePartialLoaderProvider, triSettingsProvider) {
        var appLanguages = [{
                name: 'Chinese',
                key: 'zh'
            }, {
                name: 'English',
                key: 'triangular'
            }, {
                name: 'French',
                key: 'fr'
            }, {
                name: 'Portuguese',
                key: 'pt'
            }];

        /**
         *  each module loads its own translation file - making it easier to create translations
         *  also translations are not loaded when they aren't needed
         *  each module will have a i18n folder that will contain its translations
         */
        $translateProvider.useLoader('$translatePartialLoader', {
            urlTemplate: '{part}/i18n/{lang}.json'
        });




 $translateProvider.translations('eng', 
 
        
        
      {
  "othersdiagnosticspage2": "Page 2",
  "delightlabel": "%Delighted",
  "othersdiagnosticspage3": "Page 3",
  "othersdiagnosticspage1": "Page 1",
  "othersdiagnosticspage4": "Page 4",
  "othersdiagnosticspage5":"Page 5",
  "Informationlabel": "Information",
  "dealerrankmlvehiclepickup": "Vehicle<br>Pickup<br>(19%)",
  "sopperformancesopperform": "KPI Performance %Yes",
  "sopperformancebenchmark": "Benchmark Score",
  "dsnapshotcsisummary": "CSI Summary",
  "loyalitypurchasevehicle": "Would purchase vehicle from service dealer",
  "loyalityrecommendservice": "Would recommend service dealer to a friend/ relative",
  "menureggional": "Regional Analysis",
  "downloadcontextmenu": "Chart Context Menu",
  "modelTrend": "Trend ",
  "commonfootnote": "Note: Where relevant, * denotes small sample and ** denotes insufficient sample",
  "kpifootnote": "Note: KPIs are sorted in descending order of Total KPI Impact",
  "priorityfootnote": "Note: Attributes are sorted in descending order of attribute weights",
  "summarytop5bot5attribute": "Top 5 – Bottom 5: Attributes (% Delighted)",
  "othersdiagnosticstabletitle": "Other Diagnostics",
  "menudealeranking": "Dealer Ranking",
  "sopscorelabel": "KPI Score",
  "rightsidenavworst": "Worst Overall",
  "menuredalert": "Red Alert",
  "menupriority": "Priority Analysis",
  "rightsidenavfilters": "Filters",
  "otherstablefn1": "(1) Base: Those who made an appointment",
  "otherstablefn2": "(2) Base: Those who were contacted after the service",
//<<<<<<< HEAD
  "rightsidenavregionofdealergroup": "Region of the dealer group",
  "rightsidenavregionofdealer": "Region of the dealer",
//=======
//  "rightsidenavregionofdealer": "Region of the dealer",
//>>>>>>> e5772a336c38d28ca559714faed415b4573fbf4e
  "otherstablefn3": "(3) Base: Those who reported problems when contacted",
  "otherstablefootnote1":"(1) Base: Vehicle serviced within the same day",

  "othersdiagnosticsQuestions": "Questions",
  "meanlabel": "Attributes Mean Score",
  "rightsidenavmodel": "Model",
  "dealerrankregion": "Region",
  "sopperformancechoosebenchmark": "Choose Benchmark Score",
  "rightsidenavregion2": "Region 2",
  "rightsidenavregion1": "Region 1",
  "menuotherfeedback": "Other Feedback",
  "menudealersnapshot": "Dealer Snapshot",
  "summarytop5bot5dr": "Top 5 - Bottom 5: KPI (%Yes)",
  "loyalityrevisitservice": "Would revisit service dealer for post-warranty service",
  "menusopperform": "KPI Performance",
  "summaryfactorscore": "Factor Scores",
  "modelfactorscrbymodel": "Factor Scores by Model",
  "dealerrankcsi": "CSI",
  "downloadpng": "Download PNG Image",
  "soptrendsoptrend": "KPI Trends",
  "rightsidenavdg1": "Dealer Group 1",
  "rightsidenavdg2": "Dealer Group 2",
  "rightsidenavfactor": "Factor",
  "menumodelanalysis": "Model Analysis",
  "dsnapshottop5bot5sop": "Top 5 – Bottom 5 KPI Performance",
  "tooltiplogout": "Logout",
  "soptotal": "Total",
  "dsummaryDealer CSI Score": "Dealer CSI Score",
  "dealerrankexporttable": "exporttable",
  "trendcsiscoretrend": "CSI Score Trend",
  "rightsidenavyear": "Year",
  "menusoptrend": "KPI Trend",
  "trendattributetrend": "Attribute Trend",
  "csiavglabel": "CSI Average",
  "rightsidenavstudy": "Study Total",
  "tooltipinformation": "Information",
  "menusummary": "Summary",
  "menupdf": "Downloadable Dealer PDF",
  "trendfactortrend": "Factor Trend ",
  "rightsidenavdg": "Dealer Group",
  "dealerrankcsirank": "CSI Rank",
  "summarycsiscore": "CSI Score",
  "tooltiptopline": "Topline Download",
  "modelcsibymodel": "CSI by Model",
  "dealerrankmlserviceadvisor": "Service<br>Advisor<br>(17%)",
  "dealerrankmlservicequality": "Service<br>Quality<br>(30%)",
  "rightsidenavregioncompare": "Region Comparison",
  "regionalfactorscores": "Factor Score",
  "rightsidenavmainselection": "Main  Selection",
  "downloadcsv": "Download CSV",
  "menutrend": "Trend Analysis",
  "downloadxls": "Download XLS",
  "menukpiperform":"KPI Performance",
    "menukpitrend":"KPI Trend",
    "kpitotal":"Total",
    "kpiperformancekpiperform":"KPI Performance %Yes",
"kpiperformancebenchmark":"Benchmark Score",
        "kpidealerlable": "KPI Implemented",
     "KpiTopBottom": "Top 5 – Bottom 5: KPIs",
    "KPImsg":"Data for KPIs will only be shown after 2018 Q2",
     "kpitrendkpitrend": "KPI Trends",
  "Logoutlabel": "Logout",
  "rightsidenavdealer2": "Dealer 2",
  "rightsidenavdealer1": "Dealer 1",
  "menuothers": "Other Diagnostics",
  "rightsidenavbest": "Best Overall",
  "rightsidenavdgcompare": "Dealer Group Comparison",
  "loyalityrecommendnissan": "Would recommend Nissan to a friend/ relative",
  "rightsidenavdealer": "Dealer",
  "summarymaplabel": "Total respondents",
  "dealerranksop": "KPI<br>Implemented",
  "dealerrankdealer": "Dealer",
  "otherschartnote": "++ indicates Multiple Response Question, total might exceed 100%",
  "rightsidenavregion": "Region",
  "rightsidenavregionofdg": "Region of the Dealer Group",
  "Toplinelabel": "Topline Downlaod",
  "regionalattribute Delighted": "Attributes (% Delighted)",
  "regionalbyregion": "by Region",
  "dealerrankmlservicefacility": "Service<br>Facility<br>(15%)",
  "dsnapshotdelight": "% Delighted Attributes",
  "menuloyality": "Loyalty and Advocacy",
  "dealerrankmlserviceinit": "Service<br>Initiation<br>(18%)",
  "rightsidenavdealercompare": "Dealer Comparison",
  "sopregion": "Region",
  "regionalattributems": "Attributes Mean Score",
  "prioritylabelstudytotal": "Study Total",
  "loyalitypurchasenissan": "Would repurchase Nissan",
  "rightsidenavchcomparer": "Choose Comparison",
  "Questionnaireslabel": "Questionnaires Downlaod",
  "summaryregcsi": "Region  CSI Score",
  "filterlabelall": "All",
  "kpitopbottom": "Top 5 – Bottom 5 : KPIs",
//  "KPImsg":"KPI data will be released at a later date",
  "summarycsiindex":"CSI Index",
  "summarycityindex":"Region CSI Index",
  "top5bottom5dealerrank":" Top 5 - Bottom 5: CSI Dealer Ranking Overall",
  "top5bottom5attributemean":"Top 5 – Bottom 5: Attributes",
  "Page1":"Page 1",
  "Page2":"Page 2",
  "foot1":"# Base: All of the work not completed right the first time",
  "foot2":"++ indicates Multiple Response Question, total might exceed 100%",
  "foot3":"# Base: All of the work not completed right the first time++ indicates Multiple Response Question, total might exceed 100%",
  "dealergroupIndex":"DealerGroup CSI Index"
}
        
                
                
                
                );
  
  $translateProvider.translations('ch', 
  
       {
  "othersdiagnosticspage2": "第二頁",
  "delightlabel": "%欣喜",
  "othersdiagnosticspage3": "第三頁",
  "othersdiagnosticspage1": "第一頁",
  "othersdiagnosticspage4": "第四頁",
  "othersdiagnosticspage5":"第五頁",
  "Informationlabel": "信息",
  "dealerrankmlvehiclepickup": "完修取車<br>(19%)",
  "kpiperformancekpiperform": "KPI表現（是否有執行的百分比）",
  "kpiperformancebenchmark": "標桿分數",
  "dsnapshotcsisummary": "CSI 摘要",
  "loyalitypurchasevehicle": "會再前往這家經銷商購買車子",
  "loyalityrecommendservice": "將這家服務廠介紹給親友",
  "menureggional": "地區分析",
  "downloadcontextmenu": "圖表目錄",
  "modelTrend": "趨勢",
  "commonfootnote": "注：* 表示小樣本數，** 表示不足樣本數",
  "kpifootnote": "注意：KPIs是根據Total KPI影響分數排序",
  "priorityfootnote": "注意：細項是根據各細項權重從高到底排序",
  "summarytop5bot5attribute": "五大表現最佳與最差的細項（表示欣喜的顧客的百分比）",
  "othersdiagnosticstabletitle": "其他流程 ",
  "menudealeranking": "服務廠排名",
  "sopscorelabel": "KPI 指數",
  "rightsidenavworst": "最差",
  "menuredalert": "警示",
  "menupriority": "細項分析",
  "rightsidenavfilters": "筛选",
  "otherstablefn1": "(1) 基數：事先預約這次的保養或維修的受訪者",
  "otherstablefn2": "(2) 基數：被聯繫并詢問對當次服務的整體滿意程度的受訪者",
//<<<<<<< HEAD
  "rightsidenavregionofdealergroup": "經銷商所在地區",
  "rightsidenavregionofdealer": "生活館所在地區",
//=======
//  "rightsidenavregionofdealer": "經銷商所在地區",
//>>>>>>> e5772a336c38d28ca559714faed415b4573fbf4e
  "otherstablefn3": "(3) 基數：當被聯繫時，有反映任何維修/ 保養後車子所出現的問題的受訪者",
   "otherstablefootnote1":"(1) 基數：當天完修車輛",
  "othersdiagnosticsQuestions": "問題",
  "meanlabel": "細項平均分",
  "rightsidenavmodel": "車型",
  "dealerrankregion": "縣/市",
  "sopperformancechoosebenchmark": "選擇標桿分數",
  "rightsidenavregion2": "縣/市 2",
  "rightsidenavregion1": "縣/市 1",
  "menuotherfeedback": "其他反饋",
  "menudealersnapshot": "服務廠快照",
  "summarytop5bot5dr": "五大表現最佳與最差 KPI（是否有執行的百分比）",
  "loyalityrevisitservice": "在新車保固期後， 會再回到這家服務\n廠做付費的維修/ 保養",
  "menukpiperform": "KPI表現",
  "summaryfactorscore": "因素指數",
  "modelfactorscrbymodel": "各車型因素指數",
  "dealerrankcsi": "CSI",
  "downloadpng": "下載PNG格式圖片",
  "kpitrendkpitrend": "KPI趨勢",
  "rightsidenavdg1": "經銷商 1",
  "rightsidenavdg2": "經銷商  2",
  "rightsidenavfactor": "因素指數",
  "menumodelanalysis": "車型分析",
  "dsnapshottop5bot5sop": "五大最佳與最差的KPI",
  "tooltiplogout": "退出",
  "kpitotal": "Nissan 平均",
  "dsummaryDealer CSI Score": "各服務廠CSI指數",
  "dealerrankexporttable": "下載表格",
  "trendcsiscoretrend": "CSI 指數趨勢",
  "rightsidenavyear": "年份",
  "menukpitrend": "KPI趨勢",
  "trendattributetrend": "細項指數趨勢",
  "csiavglabel": "CSI 平均",
  "rightsidenavstudy": "Nissan 平均",
  "tooltipinformation": "信息",
  "menusummary": "摘要",
  "menupdf": "可下載版服務廠報告",
  "trendfactortrend": "因素指數趨勢",
  "rightsidenavdg": "經銷商",
  "dealerrankcsirank": "CSI 排名",
  "summarycsiscore": "CSI 指數",
  "tooltiptopline": "下載概要報告",
  "modelcsibymodel": "各車型CSI指數",
  "dealerrankmlserviceadvisor": "服務人員<br>(17%)",
  "dealerrankmlservicequality": "服務品質<br>(30%)",
  "rightsidenavregioncompare": "縣/市對比",
  "regionalfactorscores": "因素指數",
  "rightsidenavmainselection": "主要選擇",
  "downloadcsv": "下載CSV格式數據",
  "menutrend": "趨勢分析",
  "downloadxls": "下載XLS格式數據",
  "Logoutlabel": "登出",
  "rightsidenavdealer2": "服務廠  2",
  "rightsidenavdealer1": "服務廠 1",
  "menuothers": "其他流程 ",
  "rightsidenavbest": "最佳",
  "rightsidenavdgcompare": "經銷商對比",
  "loyalityrecommendnissan": "向您的朋友或親戚推薦Nissan 汽車",
  "rightsidenavdealer": "服務廠",
  "summarymaplabel": "全部受訪者",
  "dealerranksop": "KPI<br>執行數",
  "dealerrankdealer": "服務廠",
  "otherschartnote": "++復選題，總和可能會超過100%",
  "rightsidenavregion": "縣/市",
  "rightsidenavregionofdg": "經銷商所在縣/市",
  "Toplinelabel": "Topline下載",
  "regionalattribute Delighted": "細項（表示欣喜的顧客的百分比）",
  "regionalbyregion": "縣/市",
  "dealerrankmlservicefacility": "服務廠設施<br>(15%)",
  "dsnapshotdelight": "對各細項表示欣喜的顧客的百分比",
  "menuloyality": "忠誠度與推薦意願 ",
  "dealerrankmlserviceinit": "服務主動性<br>(18%)",
  "rightsidenavdealercompare": "服務廠對比",
  "sopregion": "縣/市",
  "regionalattributems": "細項平均分",
  "prioritylabelstudytotal": "Nissan 平均",
  "loyalitypurchasenissan": "再購買另一部Nissan 汽車",
  "rightsidenavchcomparer": "選擇做對比",
  "Questionnaireslabel": "問卷下載",
  "summaryregcsi": "縣/市CSI 指數",
  "filterlabelall": "全部",
  "kpitopbottom": "五大表現最佳與最差 KPI（是否有執行的百分比）",
  "KPImsg":"KPI 將會遲些公佈",
  "summarycsiindex":"CSI 指數",
  "summarycityindex":"縣/市CSI 指數",
  "top5bottom5dealerrank":"5大服務廠表現最佳與最差 CSI",
  "top5bottom5attributemean":"5大表現最佳最差的細項平均分",
  "Page1":"第一頁",
  "Page2":"第二頁",
  "foot1":"++ 複選題，總百分比可能會超過100%",
  "foot2":"# 基數：未能一次正確完成維修/ 保養",
  "foot3":"++ 複選題，總百分比可能會超過100%\n# 基數：未能一次正確完成維修/ 保養",
  "dealergroupIndex":"經銷商CSI 指數"

}
        
                
                );
        $translatePartialLoaderProvider.addPart('app');

        // make sure all values used in translate are sanitized for security
        $translateProvider.useSanitizeValueStrategy('sanitize');

        // cache translation files to save load on server
        $translateProvider.useLoaderCache(true);

        // setup available languages in angular translate & triangular
        var angularTranslateLanguageKeys = [];
        for (var language in appLanguages) {
            // add language key to array for angular translate
            angularTranslateLanguageKeys.push(appLanguages[language].key);

            // tell triangular that we support this language
            triSettingsProvider.addLanguage({
                name: appLanguages[language].name,
                key: appLanguages[language].key
            });
        }

        /**
         *  try to detect the users language by checking the following
         *      navigator.language
         *      navigator.browserLanguage
         *      navigator.systemLanguage
         *      navigator.userLanguage
         */
        $translateProvider
                .registerAvailableLanguageKeys(angularTranslateLanguageKeys, {
                    'en_US': 'triangular',
                    'en_UK': 'triangular'
                })
                .use('triangular');
        
        
        $translateProvider
                .registerAvailableLanguageKeys(angularTranslateLanguageKeys, {
                    'eng': 'eng',
                    'ch': 'ch'
                })
                .use('eng');
        
        
//         $translateProvider.translations('eng', );
 
 
 
//  $translateProvider.translations('ch', );
 
  $translateProvider.preferredLanguage('eng');
        
//        $translateProvider.preferredLanguage('en'); 
        

        // store the users language preference in a cookie
        $translateProvider.useLocalStorage();
    }
})();
