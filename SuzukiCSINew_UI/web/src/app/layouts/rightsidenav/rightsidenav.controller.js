(function () {
    'use strict';

    angular
            .module('triangular.components')
            .controller('RightSidenavController', RightSidenavController);

    /* @ngInject */
    function RightSidenavController($q, $scope, $http, $mdSidenav, $state, $log, $rootScope, service, $sce) {
        var vm = this;
        vm.close = close;
        $scope.pagenumber1 = '1';
        vm.currentTab = 0;
        $scope.region = "All";
        $scope.model = "All";
        $scope.dealer = "All";
        $scope.dealersnap = $rootScope.dealerScoreDealer;
        $scope.snapregion = "All";
        $scope.snapzone = "All";
        $scope.snapmodel = "All";
        $scope.year = $rootScope.year;
        $scope.biannual = "All";
        $scope.month = "All";
        $scope.factor = $rootScope.factor;
        $scope.trendfactorlabel = "Trend Period";
        $scope.zone1l = "Region 1";
        $scope.zone2l = "Region 2";
        $scope.dealer1l = "Dealer 1";
        $scope.dealer2l = "Dealer 2";

        $scope.periodl = "Trend Period";
//        for demo
        $scope.period = "Year";
        $rootScope.legendforselectionb = "Best Overall";
        $rootScope.legendforselectionw = "Worst Overall";
        $scope.langcode = $rootScope.language;
        $scope.enghide = true;
        $scope.thaihide = false;
        var study = {"name": "All", "value": "All"};
        var all = {"name": "All", "value": "All"};
        var none = {"name": "None", "value": "None"};
        if ($rootScope.language == 'EN') {
            study = {"name": "All", "value": "All"};
            all = {"name": "All", "value": "All"};
            none = {"name": "None", "value": "None"};
        } else if ($rootScope.language == 'TH') {
            study = {"name": "All", "value": "全部"};
            none = {"name": "None", "value": "無"};
            all = {"name": "All", "value": "全部"};
        }
        if ($scope.langcode == 'EN') {
            $scope.choosecomparision = "Choose Comparison";
            //for demo
            $scope.dealersnap = $rootScope.dealerScoreDealer;
            $scope.regioncomparision = "Region Comparison";
            $scope.dealercomparision = "Dealer Comparison";
            $scope.radioSwitch = "Region Comparison";
            $scope.loyalSwitch = "Study Total";
            $scope.opt1 = "Study Total";
            $scope.loyaltotal = '';
            $scope.loyalzone = 'Zone of the dealer';
            $scope.loyalregion = 'Region of the dealer';
            $scope.zonel = "Zone";
            $scope.regionl = "Region";
            $scope.modell = "Model";
            $scope.dealerl = "Dealer";
            $scope.yearl = 'Year';
            $scope.biannuall = "Bi-Annual Period";
            $scope.monthl = "Month";
            $scope.region1l = "Region 1";
            $scope.region2l = "Region 2";
            $scope.dealer1l = "Dealer 1";
            $scope.dealer2l = "Dealer 2";
            $scope.factorl = 'Factor';
            $scope.factorlt = 'Factor';
            $scope.mainSelectionTooltip = "Main Selection";
            $scope.filterTooltip = "Filters";


        } else {
            $scope.dealersnap = $rootScope.Filterarray.dealer[1].value;
            $scope.dealer11 = $rootScope.Filterarray.dealer[1].name;
            $scope.dealer21 = $rootScope.Filterarray.dealer[2].name;
        }


        $scope.$on('changemodelgraph', function (event, data) {
            $scope.model = data;
        });
        $scope.$on('changefactordsnap', function (event, data) {
            $scope.factor = data;
        });

        $scope.$on('changeLanguage', function (event, data) {
            $scope.langcode = data;

            $rootScope.languagechangernav();

            var study = {"name": "All", "value": "All"};
            var none = {"name": "None", "value": "None"};
            var all = {"name": "All", "value": "All"};
            if ($rootScope.language == 'EN') {

                $scope.trendfactorlabel = "Trend Period";
                study = {"name": "All", "value": "All"};
                all = {"name": "All", "value": "All"};
                none = {"name": "None", "value": "None"};
            } else if ($rootScope.language == 'TH') {
                study = {"name": "All", "value": "全部"};
                none = {"name": "None", "value": "無"};
                all = {"name": "All", "value": "全部"};
            }
            if ($scope.langcode == 'EN') {

                if ($scope.radioSwitch == "Dealer Comparison") {
                    $scope.langconversionname = "Dealer Comparison";

                } else

                if ($scope.radioSwitch == "Zone Comparison") {
                    $scope.langconversionname = "Zone Comparison";
                }
                $scope.enghide = false;
                $scope.thaihide = true;
            } else if ($scope.langcode == 'TH') {

                if ($scope.radioSwitch == "Dealer Comparison") {
//                    $scope.langconversionname = "เปรียบเทียบ รายศูนย์บริการ";
                } else
                if ($scope.radioSwitch == "Zone Comparison") {
//                    $scope.langconversionname = "โซน รายภูมิภาค";
                }



                $scope.enghide = true;
                $scope.thaihide = false;
            }


            var parameter = [{"name": "langCode", "value": $rootScope.language}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "dealer", "value": $scope.dealer}];

            service.mastersrv("Filters-getRegion", parameter).then(function (result) {
//                console.log("In //Right Side : - " + $rootScope.Filterarray.region);
                $rootScope.Filterarray.region = result.data;


                $rootScope.Filterarray.regionx = angular.copy($rootScope.Filterarray.region);
                $rootScope.Filterarray.regiony = angular.copy($rootScope.Filterarray.region);
                $rootScope.Filterarray.region.splice(0, 0, all);
                $rootScope.Filterarray.regiony.splice(0, 0, none);

                $rootScope.region1 = $rootScope.Filterarray.regionx[0].name;//"Bangkok & Greater";
                $rootScope.region2 = $rootScope.Filterarray.regiony[2].name;//"Central and East";


                $rootScope.Filterarray.regions = result.data;

//                console.log("Region filter::::::::::::::::::::::::::", $rootScope.Filterarray.region);
                //                  $rootScope.Filterarray.region.push("All");
            });

            var parameterdgr = [{"name": "langCode", "value": $rootScope.language},
                {"name": "region", "value": encodeURIComponent($scope.region)},
                {"name": "year", "value": $scope.year},
                {"name": "dealer", "value": $scope.dealer}];


            if ($rootScope.dealerandgroup)
            {
                var dealerparameter = [{"name": "langCode", "value": $rootScope.language}];
            } else
            {
                var dealerparameter = [{"name": "langCode", "value": $rootScope.language}, {"name": "region", "value": encodeURIComponent($scope.region)}];
            }


            var dealerparameterdg = [{"name": "langCode", "value": $rootScope.language}, {"name": "year", "value": $rootScope.year}];

            service.mastersrv("Filters-getDealers", dealerparameter).then(function (result) {
                $rootScope.Filterarray.dealer = result.data;

                $rootScope.Filterarray.dealerx = angular.copy($rootScope.Filterarray.dealer);
                $rootScope.Filterarray.dealery = angular.copy($rootScope.Filterarray.dealer);
                $rootScope.Filterarray.dealery.splice(0, 0, none);

                // $scope.dealersnap= $rootScope.Filterarray.dealer[0].name;
                $rootScope.dealerScoreDealer = $rootScope.Filterarray.dealer[0].name;
                $rootScope.Filterarray.dealer.splice(0, 0, study);

                if ($rootScope.Filterarray.dealerx.length > 1) {
                    $rootScope.dealer1 = $rootScope.Filterarray.dealerx[0].name;//"Bangkok & Greater";
                    $rootScope.dealer2 = $rootScope.Filterarray.dealery[2].name;
                } else
                {
                    $rootScope.dealer1 = $rootScope.Filterarray.dealerx[0].name;//"Bangkok & Greater";
                    $rootScope.dealer2 = $rootScope.Filterarray.dealery[0].name;
                }

            });

            service.mastersrv("Filters-getModels", parameter).then(function (result) {
                $rootScope.Filterarray.model = result.data;
                $rootScope.Filterarray.model.splice(0, 0, study);
            });
            service.mastersrv("Filters-getPeriods", parameter).then(function (result) {
                $rootScope.Filterarray.period = result.data;
//                console.log("getPeriod::::::::::::::::::::::", $rootScope.Filterarray.period);
                $rootScope.period = $rootScope.Filterarray.period[0].name;
//                    $rootScope.Filterarray.dealer.splice(0, 0, study);
            });



            if ($rootScope.redfeedback == "redfeedback") {
                var langparameter = [{"name": "langCode", "value": $rootScope.language}];
                console.log('$rootScope.redfeedback', $rootScope.redfeedback);
                console.log('$rootScope.redfeedback', $rootScope.redfeedback);
                console.log('$rootScope.redfeedback', $rootScope.redfeedback);
                service.mastersrv("Filters-getYear", langparameter).then(function (result) {
                    if ($rootScope.language == 'EN') {
                        var all = {"name": "All", "value": "All"};
                    } else if ($rootScope.language == 'TH') {

                        all = {"name": "All", "value": "全部"};
                    }
                    $rootScope.Filterarray.year = result.data;
                    $rootScope.Filterarray.year.splice(0, 0, all);
//                    $scope.year = "All";
                    $scope.year = $rootScope.year;
                });
                var redothersparameter = [{"name": "langCode", "value": $rootScope.language}, {"name": "tablename", "value": $rootScope.tablename}];

                service.mastersrv("Filters-getfilteredDealers", redothersparameter).then(function (result) {
                    if ($rootScope.language == 'EN') {
                        var all = {"name": "All", "value": "All"};
                    } else if ($rootScope.language == 'TH') {

                        all = {"name": "All", "value": "全部"};
                    }
                    $rootScope.Filterarray.redalertdealer = result.data;
                    $rootScope.Filterarray.redalertdealer.splice(0, 0, all);
                    $scope.redalertdealer = $rootScope.Filterarray.redalertdealer[0].name;
//                    $scope.year = "All";

                });


            } else {


                var langparameter = [{"name": "langCode", "value": $rootScope.language}];
                service.mastersrv("Filters-getYear", langparameter).then(function (result) {
                    $rootScope.Filterarray.year = result.data;
                    $scope.year = $rootScope.year;
                });
            }


            service.mastersrv("Filters-getFactors", parameter).then(function (result) {
                $rootScope.Filterarray.factor = result.data;
                //                     $rootScope.Filterarray.factor.push("All");
            });

        });


        $scope.filterReset = function () {
            $scope.fueltoggleAll();
            $scope.toggleAll();
        };

        $scope.onchangeRadioSwitch = function (radio) {
//          
            if (radio == 'Dealer Comparison') {

                $scope.radio = 'dealer';
                if ($scope.langcode == 'EN') {
                    $scope.radioSwitch = "Dealer Comparison";
                    $scope.langconversionname = "Dealer Comparison";
                } else if ($scope.langcode == 'TH') {
                    $scope.radioSwitch = "Dealer Comparison";
//                    $scope.langconversionname = "เปรียบเทียบ รายศูนย์บริการ";

                    $scope.langconversionname = "Dealer Comparison";
                }
                $scope.dealer1 = $rootScope.Filterarray.dealerx[0].name;
                $scope.dealer2 = $rootScope.Filterarray.dealery[2].name;

                $rootScope.$broadcast("changeRadioSwitch", $scope.radio);
            } else

            if (radio == 'Region Comparison') {
                {

                    $scope.radio = 'region';
                    if ($scope.langcode == 'EN') {
                        $scope.radioSwitch = "Region Comparison";
                        $scope.langconversionname = "Region Comparison";
                    } else if ($scope.langcode == 'TH') {
                        $scope.radioSwitch = "Region Comparison";
//                    $scope.langconversionname = "โซน รายภูมิภาค";
                        $scope.langconversionname = "Region Comparison";
                    }
                    $scope.region1 = $rootScope.Filterarray.regionx[0].name;
                    $scope.region2 = $rootScope.Filterarray.regiony[2].name;

                    $rootScope.$broadcast("changeRadioSwitch", $scope.radio);
                }

            } else {

                $scope.radio = 'dealer group';
                if ($scope.langcode == 'EN') {
                    $scope.radioSwitch = "Dealer Group Comparison";
                    $scope.langconversionname = "Dealer Group Comparison";
                } else if ($scope.langcode == 'TH') {
                    $scope.radioSwitch = "Dealer Group Comparison";
//                    $scope.langconversionname = "โซน รายภูมิภาค";
                    $scope.langconversionname = "Dealer Group Comparison";
                }

                $rootScope.$broadcast("changeRadioSwitch", $scope.radio);
            }

        };



        $scope.onchangeloyalSwitch = function (radio) {

            $rootScope.$broadcast("changeloyalSwitch", radio);

            if (radio == 'Study Total') {


                $rootScope.$broadcast("changeRadioSwitch", $scope.radio);
            } else
            if (radio == 'Region') {

                $rootScope.$broadcast("changeRadioSwitch", $scope.radio);
            } else
            if (radio == 'Dealer') {

                $rootScope.$broadcast("changeRadioSwitch", $scope.radio);
            }
        };

        $scope.region1changed = function (value) {

            $rootScope.filterchanged = "Region Changed";

            $scope.region1 = value;
            $scope.region1array = value;
            $rootScope.region1selected = $scope.region1array;
            $rootScope.$broadcast("changeregion1", $scope.region1array);
        };


        $scope.region2changed = function (value) {
            $rootScope.filterchanged = "Region Changed";
            $scope.region2 = value;
            $scope.region2array = value;
            $rootScope.region2selected = $scope.region2array;
            $rootScope.$broadcast("changeregion2", $scope.region2array);
        };

        $scope.dealer1changed = function (value) {
            $rootScope.filterchanged = "Dealer  Changed";
            $scope.dealer1 = value;
            $scope.dealer1array = value;
            $rootScope.dealer1selected = $scope.dealer1array;
            $rootScope.$broadcast("changedealer1", $scope.dealer1array);
        };


        $scope.dealer2changed = function (value) {
            $rootScope.filterchanged = "Dealer  Changed";
            $scope.dealer2 = value;
            $scope.dealer2array = value;
            $rootScope.dealer2selected = $scope.dealer2array;
            $rootScope.$broadcast("changedealer2", $scope.dealer2array);
        };

        $scope.periodchanged = function (value) {
            $rootScope.filterchanged = "period  Changed";
            $scope.period = value;
            $rootScope.$broadcast("changeperiod", $scope.period);
        };

        $scope.yearchanged = function (value) {
            $rootScope.filterchanged = "year  Changed";
            $scope.year = value;
            $rootScope.year = value;
            $scope.yeararray = value;
            $rootScope.yearselected = $scope.yeararray;
            var parameter = [{"name": "year", "value": value}];
            var x = service.mastersrv("Filters-getQuarter", parameter).then(function (result) {
                $rootScope.Filterarray.quarter = result.data;
                $rootScope.Filterarray.quarter.splice(0, 0, study);
                $scope.quarter = $rootScope.Filterarray.quarter[0].name;
                $rootScope.quarter = $rootScope.Filterarray.quarter[0].name;
                var monthparameter = [{"name": "langCode", "value": $rootScope.language}, {"name": "year", "value": $rootScope.year}, {"name": "quarter", "value": $rootScope.quarter}];
                service.mastersrv("Filters-getMonth", monthparameter).then(function (result) {
                    $rootScope.Filterarray.month = result.data;
                    $rootScope.Filterarray.month.splice(0, 0, study);
                    $rootScope.month = $rootScope.Filterarray.month[0].name;
                });

                $rootScope.$broadcast("changeyear", $scope.yeararray);
            });
            $q.all([x]).then(function () {
                $scope.set = 1;
                $scope.biannualchanged($rootScope.quarter);
            });

            $rootScope.$broadcast("changeyear", $scope.yeararray);
        };


        $scope.biannualchanged = function (value) {
            $rootScope.filterchanged = "Quarter Changed";
            $scope.quarterarray = value;
            $scope.quarter = value;
            $scope.region = "All";
            $scope.regiona = "All";
            $scope.month = "All";
            $rootScope.quarterseleted = $scope.quarterarray;
            var monthparameter = [{"name": "langCode", "value": $rootScope.language}, {"name": "year", "value": $rootScope.year}, {"name": "quarter", "value": encodeURIComponent($scope.quarter)}];
            service.mastersrv("Filters-getMonth", monthparameter).then(function (result) {
                $rootScope.Filterarray.month = result.data;
                study = {"name": "All", "value": "All"};
                $rootScope.Filterarray.month.splice(0, 0, study);
                $rootScope.month = "All";
                $rootScope.month = "All";

                if ($rootScope.regionalLogin) {
                    var regionaobj = [{"name": "langCode", "value": $scope.langcode}, {"name": "quarter", "value": encodeURIComponent($scope.quarter)},
                        {"name": "month", "value": encodeURIComponent($scope.month)},
                        {"name": "year", "value": encodeURIComponent($rootScope.year)}, {"name": "regiona", "value": encodeURIComponent($scope.regiona)},
                        {"name": "region", "value": encodeURIComponent($rootScope.region)}];
                } else if ($rootScope.dealerandgroup) {
                    var regionaobj = [{"name": "langCode", "value": $scope.langcode}, {"name": "quarter", "value": encodeURIComponent($scope.quarter)},
                        {"name": "month", "value": encodeURIComponent($scope.month)}, {"name": "year", "value": encodeURIComponent($rootScope.year)}];

                } else {
                    var regionaobj = [{"name": "langCode", "value": $scope.langcode}, {"name": "quarter", "value": encodeURIComponent($scope.quarter)},
                        {"name": "month", "value": encodeURIComponent($scope.month)}, {"name": "year", "value": encodeURIComponent($rootScope.year)},
                        {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "regiona", "value": encodeURIComponent($scope.regiona)}];
                    var parameter = [{"name": "langCode", "value": $scope.langcode}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual},
                        {"name": "month", "value": $scope.month}];

                    service.mastersrv("Filters-getZone", parameter).then(function (result) {
                        $rootScope.Filterarray.zone = result.data;
                        $rootScope.Filterarray.zone.splice(0, 0, study);
                        $scope.zone = $rootScope.Filterarray.zone[0].name;
                        $rootScope.zone = $rootScope.Filterarray.zone[0].name;
                    });
                    service.mastersrv("Filters-getRegion", parameter).then(function (result) {
                        $rootScope.Filterarray.region = result.data;
                        $rootScope.Filterarray.regionx = angular.copy($rootScope.Filterarray.region);
                        $rootScope.Filterarray.regiony = angular.copy($rootScope.Filterarray.region);
                        $rootScope.Filterarray.regiony.splice(0, 0, none);
                        $rootScope.Filterarray.region.splice(0, 0, study);
                        $scope.region = $rootScope.Filterarray.region[0].name;
                        $scope.snapregion = $scope.region;
                        $rootScope.region = $rootScope.Filterarray.region[0].name;
                        $scope.region1 = $rootScope.Filterarray.regionx[0].name;
                        $scope.region2 = $rootScope.Filterarray.regiony[2].name;

                        $rootScope.regionseleted = $scope.regionarray;

                    });

                }

                service.mastersrv("Filters-getDealers", regionaobj).then(function (result) {
                    $rootScope.Filterarray.dealer = result.data;
                    $rootScope.Filterarray.dealerx = angular.copy($rootScope.Filterarray.dealer);
                    $rootScope.Filterarray.dealery = angular.copy($rootScope.Filterarray.dealer);
                    $rootScope.Filterarray.dealery.splice(0, 0, none);
                    $rootScope.Filterarray.dealer.splice(0, 0, study);
                    if ($rootScope.dsnapshot == "dsnap") {
                        $scope.dealersnap = $rootScope.Filterarray.dealer[1].name;
                        $scope.dealer = $rootScope.Filterarray.dealer[1].name;
                    } else {
                        $scope.dealer = "All";
                    }
                    $scope.dealer1 = $rootScope.Filterarray.dealerx[0].name;
                    $scope.dealer2 = $rootScope.Filterarray.dealery[2].name;
                    $rootScope.quarterseleted = $scope.quarterarray;
                    $rootScope.quarter = $scope.quarterarray;
                    if ($rootScope.regionalLogin) {

                        var parameter = [{"name": "langCode", "value": $rootScope.language}, {"name": "region", "value": encodeURIComponent($rootScope.region)}, {"name": "year", "value": $scope.year}, {"name": "dealer", "value": $scope.dealer}];

                    } else if ($rootScope.dealerandgroup)
                        var parameter = [{"name": "langCode", "value": $rootScope.language}, {"name": "year", "value": $scope.year}, {"name": "quarter", "value": $scope.quarter}];

                    else
                        var parameter = [{"name": "langCode", "value": $rootScope.language}, {"name": "month", "value": encodeURIComponent($scope.month)}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "dealer", "value": $scope.dealer}];

                    var modelparameter = [{"name": "langCode", "value": $rootScope.language}, {"name": "month", "value": encodeURIComponent($scope.month)}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "dealer", "value": $scope.dealer}];

                    service.mastersrv("Filters-getModels", modelparameter).then(function (result) {
                        $rootScope.Filterarray.model = result.data;
                        $rootScope.Filterarray.model.splice(0, 0, study);
                        $rootScope.model = $rootScope.Filterarray.model[0].name;
                        $scope.model = $rootScope.Filterarray.model[0].name;

                        if ($scope.set)
                        {
                            $scope.set = 0;
                        } else
                        {
                            $rootScope.$broadcast("changebiannual", $scope.quarterarray);
                        }
                    });

                });


            });
        };

        $scope.monthchanged = function (value) {
            $rootScope.filterchanged = "Month Changed";
            $scope.month = value;
            $rootScope.month = value;
            $scope.montharray = value;
            $scope.region = "All";
            $scope.regiona = "All";
            $scope.model = "All";
            $rootScope.monthselected = $scope.montharray;
            if ($rootScope.regionalLogin) {
                var regionaobj = [{"name": "year", "value": $scope.year}, {"name": "month", "value": $scope.month},
                    {"name": "quarter", "value": $scope.quarter}, {"name": "region", "value": $rootScope.region}, {"name": "regiona", "value": encodeURIComponent($scope.regiona)}];
            } else if ($rootScope.dealerandgroup) {
                var regionaobj = [{"name": "langCode", "value": $scope.langcode}, {"name": "quarter", "value": encodeURIComponent($scope.quarter)},
                    {"name": "month", "value": encodeURIComponent($scope.month)}, {"name": "year", "value": encodeURIComponent($rootScope.year)},
                    {"name": "dealergroup", "value": encodeURIComponent($rootScope.dealergroup)}];

            } else {
                var regionaobj = [{"name": "year", "value": $scope.year}, {"name": "month", "value": $scope.month},
                    {"name": "quarter", "value": $scope.quarter}, {"name": "region", "value": $scope.region}];
                var parameter = [{"name": "langCode", "value": $scope.langcode}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual},
                    {"name": "month", "value": $scope.month}];

                service.mastersrv("Filters-getZone", parameter).then(function (result) {
                    $rootScope.Filterarray.zone = result.data;
                    $rootScope.Filterarray.zone.splice(0, 0, study);
                    $scope.zone = $rootScope.Filterarray.zone[0].name;
                    $rootScope.zone = $rootScope.Filterarray.zone[0].name;
                });
                service.mastersrv("Filters-getRegion", parameter).then(function (result) {
                    $rootScope.Filterarray.region = result.data;
                    $rootScope.Filterarray.regionx = angular.copy($rootScope.Filterarray.region);
                    $rootScope.Filterarray.regiony = angular.copy($rootScope.Filterarray.region);
                    $rootScope.Filterarray.regiony.splice(0, 0, none);
                    $rootScope.Filterarray.region.splice(0, 0, study);
                    $scope.region = $rootScope.Filterarray.region[0].name;
                    $scope.snapregion = $scope.region;
                    $rootScope.region = $rootScope.Filterarray.region[0].name;
                    $scope.region1 = $rootScope.Filterarray.regionx[0].name;
                    $scope.region2 = $rootScope.Filterarray.regiony[2].name;
                    $rootScope.regionseleted = $scope.regionarray;
                });
            }
            var modelobj = [{"name": "langCode", "value": $rootScope.language}, {"name": "month", "value": encodeURIComponent($scope.month)}, {"name": "quarter", "value": encodeURIComponent($scope.quarter)}, {"name": "year", "value": $scope.year}];
            console.log("pointer..");
            service.mastersrv("Filters-getModels", modelobj).then(function (result) {
                $rootScope.Filterarray.model = result.data;
                $rootScope.Filterarray.model.splice(0, 0, study);
                $scope.model = $rootScope.Filterarray.model[0].name;
            });

            if ($rootScope.regionalLogin) {

                var parameter = [{"name": "langCode", "value": $rootScope.language}, {"name": "month", "value": $scope.month}, {"name": "quarter", "value": encodeURIComponent($scope.quarter)}, {"name": "region", "value": encodeURIComponent($rootScope.region)}, {"name": "year", "value": $scope.year}, {"name": "dealer", "value": $scope.dealer}];

            } else if ($rootScope.dealerandgroup)
                var parameter = [{"name": "langCode", "value": $rootScope.language}, {"name": "dealergroup", "value": encodeURIComponent($rootScope.dealergroup)}, {"name": "year", "value": $scope.year}, {"name": "month", "value": $rootScope.month}];
            else
                var parameter = [{"name": "langCode", "value": $rootScope.language}, {"name": "month", "value": $scope.month}, {"name": "regiona", "value": encodeURIComponent($scope.regiona)}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "dealer", "value": $scope.dealer}];

            service.mastersrv("Filters-getDealers", regionaobj).then(function (result) {
                console.log("result: ", result.data);

                $rootScope.Filterarray.dealer = result.data;
                $rootScope.Filterarray.dealerx = angular.copy($rootScope.Filterarray.dealer);
                $rootScope.Filterarray.dealery = angular.copy($rootScope.Filterarray.dealer);
                $rootScope.Filterarray.dealery.splice(0, 0, none);
                $rootScope.Filterarray.dealer.splice(0, 0, study);
                if ($rootScope.dsnapshot == "dsnap") {
                    $scope.dealersnap = $rootScope.Filterarray.dealer[1].name;
                    $scope.dealer = $rootScope.Filterarray.dealer[1].name;
                } else {
                    $scope.dealer = "All";
                }
                $scope.dealer1 = $rootScope.Filterarray.dealerx[0].name;
                if ($rootScope.Filterarray.dealery.length <= 2) {
                    $scope.dealer2 = $rootScope.Filterarray.dealerx[0].name;
                } else {
                    $scope.dealer2 = $rootScope.Filterarray.dealery[2].name;
                }
                $rootScope.$broadcast("changemonth", $scope.montharray);
            });
            console.log("regionobj :", regionaobj);
            console.log("list od dealers:", $rootScope.Filterarray.dealer);
        };

        $scope.$on('Summaryregionclick', function (event, data) {
            $scope.regionchanged(data);
        });

        $scope.zonechanged = function (value) {
            $rootScope.filterchanged = "zone  Changed";
            $scope.region = "All";
            $scope.zone = value;
            $scope.zonearray = value;
            $rootScope.$broadcast("changezone", $scope.zonearray);

            if ($rootScope.language == 'EN') {
                study = {"name": "All", "value": "All"};
                all = {"name": "All", "value": "All"};
            } else if ($rootScope.language == 'TH') {
                study = {"name": "All", "value": "全部"};

                all = {"name": "All", "value": "全部"};
            }

            var regionobj = [{"name": "langCode", "value": $scope.langcode}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual},
                {"name": "month", "value": $scope.month}, {"name": "zone", "value": value}];
            service.mastersrv("Filters-getRegion", regionobj).then(function (result) {
                $rootScope.Filterarray.region = result.data;
                $rootScope.Filterarray.regionx = angular.copy($rootScope.Filterarray.region);
                $rootScope.Filterarray.regiony = angular.copy($rootScope.Filterarray.region);
                $rootScope.Filterarray.regiony.splice(0, 0, none);
                $rootScope.Filterarray.region.splice(0, 0, study);
                $scope.region = $rootScope.Filterarray.region[0].name;
                $scope.snapregion = $scope.region;
                $rootScope.region = $rootScope.Filterarray.region[0].name;
                $scope.region1 = $rootScope.Filterarray.regionx[0].name;
                $scope.region2 = $rootScope.Filterarray.regiony[2].name;

                $rootScope.regionseleted = $scope.regionarray;

            });

            service.mastersrv("Filters-getDealers", regionobj).then(function (result) {
                $rootScope.Filterarray.dealer = result.data;


                $rootScope.Filterarray.dealer.splice(0, 0, study);
                $rootScope.dealerScoreDealer = $rootScope.Filterarray.dealer[0].name;
                $scope.dealer1 = $rootScope.Filterarray.dealer[1].name;
                $scope.dealer2 = $rootScope.Filterarray.dealer[2].name;
                //                                console.log("city dealer " +result.data);
            });
        };

        $scope.regionchanged = function (value) {

            $rootScope.filterchanged = "region  Changed";
            $scope.dealer = "All";
            $scope.snapregion = value;
            $scope.regionarray = value;
            $scope.region = value;
            $rootScope.regionseleted = $scope.regionarray;
            $rootScope.$broadcast("changeregion", $scope.regionarray);

            if ($rootScope.language == 'EN') {
                study = {"name": "All", "value": "All"};
                all = {"name": "All", "value": "All"};
            } else if ($rootScope.language == 'TH') {
                study = {"name": "All", "value": "全部"};

                all = {"name": "All", "value": "全部"};
            }
            var regionobj = [{"name": "langCode", "value": $scope.langcode}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual},
                {"name": "month", "value": $scope.month}, {"name": "region", "value": value}, {"name":"zone", "value":$scope.zone}];
            service.mastersrv("Filters-getDealers", regionobj).then(function (result) {
                $rootScope.Filterarray.dealer = result.data;
                $rootScope.Filterarray.dealer.splice(0, 0, study);
                $rootScope.dealerScoreDealer = $rootScope.Filterarray.dealer[0].name;
                $scope.dealer1 = $rootScope.Filterarray.dealer[1].name;
                $scope.dealer2 = $rootScope.Filterarray.dealer[2].name;
            });
        };


        $scope.dgregionchanged = function (value) {

            $rootScope.filterchanged = "region  Changed";
            $scope.dealergroup = "All";
            $scope.dealer = "All";
            $scope.snapregion = value;
            $scope.regionarray = value;
            $scope.dgregion = value;
            $rootScope.regionseleted = $scope.regionarray;
            $rootScope.$broadcast("changeregion", $scope.regionarray);

            if ($rootScope.language == 'EN') {
                study = {"name": "All", "value": "All"};
                all = {"name": "All", "value": "All"};
            } else if ($rootScope.language == 'TH') {
                study = {"name": "All", "value": "全部"};

                all = {"name": "All", "value": "全部"};
            }
            var regionobj = [{"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}];
            service.mastersrv("Filters-getDealers", regionobj).then(function (result) {
                $rootScope.Filterarray.dealer = result.data;


                $rootScope.Filterarray.dealer.splice(0, 0, study);
                $rootScope.dealerScoreDealer = $rootScope.Filterarray.dealer[0].name;
                $scope.dealer1 = $rootScope.Filterarray.dealer[1].name;
                $scope.dealer2 = $rootScope.Filterarray.dealer[2].name;
            });
        };

        $scope.dealerchanged = function (value) {
            $scope.dealerarray = value;
            $scope.dealer = value;
            $rootScope.dealerseleted = $scope.dealerarray;

            $rootScope.$broadcast("changedealer", $scope.dealerarray);
            $scope.dealer1 = $rootScope.Filterarray.dealers[1].value;
            $scope.dealer2 = $rootScope.Filterarray.dealers[2].value;
        };


        $scope.redalertdealerchanged = function (value) {

            $scope.redalertdealer = value;

            $rootScope.$broadcast("changeredalertdealer", value);

        };




        $scope.snapregionchanged = function (value) {
            $scope.regionarray = value;
            $scope.snapregion = value;
            $scope.region = value;
            $rootScope.regionseleted = $scope.regionarray;
            $rootScope.$broadcast("snapregionchanged", $scope.regionarray);
            $scope.dealersnap = "All";
            if ($rootScope.language == 'EN') {
                study = {"name": "All", "value": "All"};
                all = {"name": "All", "value": "All"};
            } else if ($rootScope.language == 'TH') {
                study = {"name": "All", "value": "全部"};
                none = {"name": "None", "value": "無"};
                all = {"name": "All", "value": "全部"};
            }
        };
        $scope.dealersnapchanged = function (value) {
            $scope.dealerarray = value;
            $scope.dealersnap = value;
            $rootScope.dealerseleted = $scope.dealerarray;
            $rootScope.$broadcast("dealersnapchanged", $scope.dealerarray);

        };


        $scope.modelchanged = function (value) {
            $scope.modelarray = value;
            $scope.model = value;
            $rootScope.modelseleted = $scope.modelarray;
            $rootScope.$broadcast("changemodel", $scope.modelarray);
        };


        $scope.factorchanged = function (value) {
            $scope.factorarray = value;
            $scope.factor = value;
            $rootScope.factorseleted = $scope.factorarray;
            $rootScope.$broadcast("changefactor", $scope.factorarray);
        };


        $scope.changed = function (value) {
            $scope.allmodel = false;
            $scope.allfuel = false;

        };


        $rootScope.languagechangernav = function (type) {

            if ($scope.langcode == 'EN') {
                $scope.choosecomparision = "Choose Comparison";
                $scope.zonecomparision = "Zone Comparison";
                $scope.opt1 = "Study Total";
                $scope.loyaltotal = '';
                $scope.loyalzone = 'Zone of the dealer';
                $scope.loyalregion = 'Region of the dealer';
                $scope.dealercomparision = "Dealer Comparison";
                $scope.zonel = "Zone";
                $scope.regionl = "Region";
                $scope.modell = "roduct";
                $scope.dealerl = "Dealer";
                $scope.yearl = 'Year';
                $scope.biannuall = "Bi-Annual Period";
                $scope.monthl = "Month";
                $scope.month = "All";
                $scope.zone1l = "Zone 1";
                $scope.zone2l = "Zone 2";
                $scope.dealer1l = "Dealer 1";
                $scope.dealer2l = "Dealer 2";
                $scope.factorl = 'Factor';
                $scope.factorlt = 'Factor';
                $scope.mainSelectionTooltip = "Main Selection";
                $scope.filterTooltip = "Filters";
                $scope.year = $rootScope.year;

            } else {
                $scope.year = $rootScope.year;



                $scope.choosecomparision = "Choose Comparison";
                $scope.zonecomparision = "Zone Comparison";
                $scope.opt1 = "Study Total";
                $scope.loyaltotal = '';
                $scope.loyalzone = 'Zone of the dealer';
                $scope.loyalregion = 'Region of the dealer';
                $scope.dealercomparision = "Dealer Comparison";
                $scope.zonel = "Zone";
                $scope.regionl = "Region";
                $scope.modell = "roduct";
                $scope.dealerl = "Dealer";
                $scope.yearl = 'Year';
                $scope.biannuall = "Bi-Annual Period";
                $scope.monthl = "Month";
                $scope.month = "All";
                $scope.zone1l = "Zone 1";
                $scope.zone2l = "Zone 2";
                $scope.dealer1l = "Dealer 1";
                $scope.dealer2l = "Dealer 2";
                $scope.factorl = 'Factor';
                $scope.factorlt = 'Factor';
                $scope.mainSelectionTooltip = "Main Selection";
                $scope.filterTooltip = "Filters";
            }

        };


        $rootScope.checkboxInitiliser = function (type) {
            $scope.priorityRegionHide = false;
            $rootScope.filterchanged = "Menu changed";
            console.log("inside checkbox")
            if ($rootScope.language == 'EN') {
                study = {"name": "All", "value": "All"};
                var none = {"name": "None", "value": "None"};
                all = {"name": "All", "value": "All"};
            } else if ($rootScope.language == 'TH') {
                study = {"name": "All", "value": "全部"};
//                var none = {"name": "None", "value": "ไม่แสดงผล"};
//                all = {"name": "All", "value": "全部"};
            }

            $scope.snapregion = "All"


            $scope.dealer = "All";


//            for demo
            $scope.period = "Year";


            var dealerobj = [{"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.snapregion)}, {"name": "year", "value": $scope.year}];
            if ($rootScope.dealerandgroup)
            {
                var dealerobj = [{"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.snapregion)}, {"name": "year", "value": $scope.year}];

            }


            var dealerApi = service.mastersrv("Filters-getDealers", dealerobj).then(function (result) {
                $rootScope.Filterarray.dealer = result.data;
                if ($rootScope.dsnapshot == "dsnap")
                {
                    if ($rootScope.SnapRedirectBarClick == "true") {
                        $scope.dealersnap = $rootScope.dsnapDealer;
                        $rootScope.SnapRedirectBarClick = "false";
                        $rootScope.$broadcast("snapchange", $rootScope.dsnapDealer)
                    } else
                    {
                        $rootScope.dealerScoreDealer = $rootScope.Filterarray.dealer[0].name;
                        $scope.dealersnap = $rootScope.Filterarray.dealer[0].name;
                        $rootScope.Filterarray.dealerx = angular.copy($rootScope.Filterarray.dealer);
                        $rootScope.Filterarray.dealery = angular.copy($rootScope.Filterarray.dealer);
                        $rootScope.SnapRedirectBarClick = "false";
                        $rootScope.$broadcast("snapchange", $rootScope.dealerScoreDealer)
                    }
                }
                else if ($rootScope.dsnapshot != "dsnap")
                {
                    $rootScope.dealerScoreDealer = $rootScope.Filterarray.dealer[0].name;
                    $scope.dealersnap = $rootScope.Filterarray.dealer[0].name;
                    $rootScope.Filterarray.dealerx = angular.copy($rootScope.Filterarray.dealer);
                    $rootScope.Filterarray.dealery = angular.copy($rootScope.Filterarray.dealer);
                    $rootScope.Filterarray.dealer.splice(0, 0, study);
                    $rootScope.$broadcast("snapchange", $rootScope.dealerScoreDealer)
                }

            });
//                $rootScope.SnapRedirectBarClick = "false"; 


//               var dgRegion = service.mastersrv("Filters-getdgRegion", dealerparameterdg).then(function (result) {
//                
//                $rootScope.Filterarray.dgregion = result.data;
//                $rootScope.dgregiontemp=$rootScope.Filterarray.dgregion[0].name;
//                $scope.dgregion = $rootScope.Filterarray.dgregion[0].name;
//                $rootScope.$broadcast("setdgregion", $scope.dgregion);
//                console.log("dg regionnn -> " , $rootScope.Filterarray.dgregion);
//            });

            if ($rootScope.redfeedback == "redfeedback") {
                var langparameter = [{"name": "langCode", "value": $rootScope.language}];
                var redothersparameter = [{"name": "langCode", "value": $rootScope.language}, {"name": "tablename", "value": $rootScope.tablename}];
                console.log('$rootScope.redfeedback', $rootScope.redfeedback);
                console.log('$rootScope.redfeedback', $rootScope.redfeedback);
                console.log('$rootScope.redfeedback', $rootScope.redfeedback);
                service.mastersrv("Filters-getYear", langparameter).then(function (result) {
                    if ($rootScope.language == 'EN') {
                        var all = {"name": "All", "value": "All"};
                    } else if ($rootScope.language == 'TH') {
//                        all = {"name": "All", "value": "全部"};
                    }
                    $rootScope.Filterarray.year = result.data;
                    $rootScope.Filterarray.year.splice(0, 0, all);
//                    $scope.year = "All";
                    $scope.year = $rootScope.year;
                });

                service.mastersrv("Filters-getfilteredDealers", redothersparameter).then(function (result) {
                    if ($rootScope.language == 'EN') {
                        var all = {"name": "All", "value": "All"};
                    } else if ($rootScope.language == 'TH') {
//                        all = {"name": "All", "value": "全部"};
                    }
                    $rootScope.Filterarray.redalertdealer = result.data;

                    $rootScope.Filterarray.redalertdealer.splice(0, 0, all);
                    $scope.redalertdealer = $rootScope.Filterarray.redalertdealer[0].name;
//                    $scope.year = "All";

                });
            } else {


                var langparameter = [{"name": "langCode", "value": $rootScope.language}];
                service.mastersrv("Filters-getYear", langparameter).then(function (result) {
                    $rootScope.Filterarray.year = result.data;
                    $scope.year = $rootScope.year;

//                    $rootScope.year = '2017';
                });
            }





            $scope.pagenumber1 = $rootScope.pagenumber;
            $scope.model = "All";

            $scope.region = "All";
//            $scope.dgregion = $rootScope.Filterarray.dgregion[0].value;
            $scope.factor = "CSI";

            if ($rootScope.dealerandgroup)
            {
                $scope.radioSwitch = "Dealer Comparison";
            } else
            {
                $scope.radioSwitch = "Region Comparison";
            }

            $scope.loyalSwitch = "Study Total";


            if ($scope.langcode == 'EN') {


                $scope.choosecomparision = "Choose Comparison";

//                for demo
                $scope.regioncomparision = "Region Comparison";

                $scope.opt1 = "Study Total";
                $scope.loyaltotal = '';
                $scope.loyalzone = 'Zone of the dealer';
                if ($rootScope.dealerandgroup)
                {
                    $scope.loyalregion = 'Region of the Dealer Group';
                    $scope.radioSwitch = "Dealer Comparison";
                    $scope.langconversionname = "Dealer Comparison";
                } else if ($rootScope.dealerlogin)
                {
                    $scope.loyalregion = 'Region of the Dealer Group';
                    $scope.radioSwitch = "Dealer Comparison";
                    $scope.langconversionname = "Dealer Comparison";
                } else
                {
                    $scope.loyalregion = 'Region of the Dealer';
                    $scope.radioSwitch = "Region Comparison";
                    $scope.langconversionname = "Region Comparison";
                    $scope.region1 = $rootScope.Filterarray.regionx[0].name;
                    $scope.region2 = $rootScope.Filterarray.regiony[2].name;
                }

                $scope.dealercomparision = "Dealer Comparison";

                $scope.loyalSwitch = "Study Total";
                $scope.zonel = "Zone";
                $scope.regionl = "Region";
                $scope.modell = "Model";
                $scope.dealerl = "Dealer";
                $scope.yearl = 'Year';
                $scope.biannuall = "Bi-Annual Period";
                $scope.monthl = "Month";
                $scope.region1l = "Region 1";
                $scope.region2l = "Region 2";
                $scope.dealer1l = "Dealer 1";
                $scope.dealer2l = "Dealer 2";
                $scope.factorl = 'Factor';
                $scope.factorlt = 'Factor';
                $scope.mainSelectionTooltip = "Main Selection";
                $scope.filterTooltip = "Filters";

                if (!$rootScope.dealerlogin) {
                    if ($rootScope.Filterarray.dealerx.length > 1) {
                        $scope.dealer1 = $rootScope.Filterarray.dealerx[0].name;//"Bangkok & Greater";
                        $scope.dealer2 = $rootScope.Filterarray.dealery[2].name;
                    } else
                    {
                        $rootScope.dealer1 = $rootScope.Filterarray.dealerx[0].name;//"Bangkok & Greater";
                        $rootScope.dealer2 = $rootScope.Filterarray.dealery[0].name;
                    }
                }


            } else if ($scope.langcode == 'TH') {
                if (!$rootScope.dealerlogin) {

                    $scope.dealer1 = $rootScope.Filterarray.dealerx[0].name;//"Bangkok & Greater";
                    $scope.dealer2 = $rootScope.Filterarray.dealery[2].name;
                }

            }

            $q.all([dealerApi]).then(function () {
                $rootScope.$broadcast('qsuccess2');
                console.log("qsuccess2 done");
            });

        };

        // add an event to switch tabs (used when user clicks a menu item before sidebar opens)
        $scope.$on('triSwitchNotificationTab', function ($event, tab) {
            vm.currentTab = tab;
        });


        function close() {
            $mdSidenav('notifications').close();
        }
    }
})();
