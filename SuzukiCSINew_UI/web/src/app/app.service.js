/* global Highcharts */

(function () {
    'use strict';
    angular.module('app')
            .service('service', service);
    function service($http, URL, $rootScope, $q, $state, $filter, Idle, $mdToast, $window) {
        this.barchart = barchart;
        this.modelbarchart = modelbarchart;
        this.othersbarchart = othersbarchart;
        this.barchartmiddle = barchartmiddle;
        this.dsnapplainbar = dsnapplainbar;
        this.plainbar = plainbar;
        this.kpiplainbar = kpiplainbar;
        this.kpiperforamnce = kpiperforamnce;
        this.destroyAllCharts = destroyAllCharts;
        this.barplainbarsplit = barplainbarsplit;
        this.barplainbarsplitDelighted = barplainbarsplitDelighted;
        this.barplainbarsplitdelighted1 = barplainbarsplitdelighted1;
        this.barplainbarsplitDelightedsnap = barplainbarsplitDelightedsnap;
        this.barplainbar = barplainbar;
        this.barline = barline;
        this.verticalline = verticalline;
        this.horizontalline = horizontalline;
        this.multigraph = multigraph;
        this.stackedgraph = stackedgraph;
        this.mastersrv = mastersrv;
        this.multbarchartsplit = multbarchartsplit;
        this.multbarchart = multbarchart;
        this.sopplainbar = sopplainbar;
        this.csi_filterfunction = csi_filterfunction;
        this.loyalitybar = loyalitybar;
        this.stackmultigraph = stackmultigraph;
        this.regplainbar = regplainbar;
        this.dsnapbarchart = dsnapbarchart;
        this.dlrstackedgraph = dlrstackedgraph;
        this.dsmultigraph = dsmultigraph;
        this.csi_titles = csi_titles;
        this.summaryplainbar = summaryplainbar;
        this.creditsummaryplainbar = creditsummaryplainbar;
        this.clicksummaryplainbar = clicksummaryplainbar;
        this.sessionLogout = sessionLogout;
        var axis;
        var position;
        var align;
//         var $translate = $filter('translate');
//        
//             $rootScope.downloadpng = $translate('downloadpng');
//            $rootScope.downloadcsv = $translate('downloadcsv');
//            $rootScope.downloadxls = $translate('downloadxls');
//            $rootScope.downloadheader = $translate('downloadcontextmenu');

        $rootScope.downloadpng = "Download PNG Image";
        $rootScope.downloadcsv = "Download CSV";
        $rootScope.downloadxls = "Download XLS";
        $rootScope.downloadheader = "Chart Context Menu";
        function destroyCharts(chartName) {

            if (chartName)
                chartName.destroy();
        }
        function csi_titles() {
//        $rootScope.Titlearray=[];
//        $rootScope.Footarray=[];
            var parameters = [{"name": "moduleType", "value": "CSI"}];
            mastersrv("Systemuserview-getTitles", parameters).then(function (result) {
                $rootScope.Titlearray = result.data;
            });
            mastersrv("Systemuserview-getFootnotes", parameters).then(function (result) {
                $rootScope.Footarray = result.data;
            });
            mastersrv("Systemuserview-getPagenote", parameters).then(function (result) {
                $rootScope.PageNotearray = result.data;
            });
        }


        function csi_filterfunction() {
            $rootScope.Filterarray = [];
            var study = {"name": "All", "value": "All"};
            var none = {"name": "None", "value": "None"};
            var all = {"name": "All", "value": "All"};
            if ($rootScope.language == 'EN') {
                study = {"name": "All", "value": "All"};
                none = {"name": "None", "value": "None"};
                all = {"name": "All", "value": "All"};
            } else if ($rootScope.language == 'TH') {
                study = {"name": "All", "value": "全部"};
                none = {"name": "None", "value": "無"};
                all = {"name": "All", "value": "全部"};
            }


            var parameter = [{"name": "langCode", "value": $rootScope.language}];
            var years = mastersrv("Filters-getYear", parameter).then(function (result) {
                $rootScope.Filterarray.year = result.data;
                $rootScope.year = $rootScope.Filterarray.year[0].name;
                console.log($rootScope.year);
            });

            var biannuals = mastersrv("Filters-getBiannual", parameter).then(function (result) {
                $rootScope.Filterarray.biannual = result.data;
                $rootScope.Filterarray.biannual.splice(0, 0, study);
                $rootScope.biannual = $rootScope.Filterarray.biannual[0].name;
            });
            var months = mastersrv("Filters-getMonth", parameter).then(function (result) {
                $rootScope.Filterarray.month = result.data;
                $rootScope.Filterarray.month.splice(0, 0, study);
                $rootScope.month = $rootScope.Filterarray.month[0].name;
            });

            $q.all([years, biannuals, months]).then(function () {
                if ($rootScope.dealerandgroup)
                {
                    var dealerparameter = [{"name": "langCode", "value": $rootScope.language}, {"name": "dealergroup", "value": encodeURIComponent($rootScope.dealergroup)}, {"name": "year", "value": $rootScope.year}];
                    var dealers = mastersrv("Filters-getDealers", dealerparameter).then(function (result) {
                        $rootScope.Filterarray.dealer = result.data;
                        console.log("testtt");
                        console.log($rootScope.Filterarray.dealer);
                        $rootScope.Filterarray.dealerx = angular.copy($rootScope.Filterarray.dealer);
                        $rootScope.Filterarray.dealery = angular.copy($rootScope.Filterarray.dealer);
                        $rootScope.dealerScoreDealer = $rootScope.Filterarray.dealer[0].name;
                        $rootScope.Filterarray.dealer.splice(0, 0, study);
                        $rootScope.Filterarray.dealery.splice(0, 0, none);
                        if ($rootScope.Filterarray.dealerx.length > 1) {


                            $rootScope.dealer1 = $rootScope.Filterarray.dealerx[0].name; //"Bangkok & Greater";
                            $rootScope.dealer2 = $rootScope.Filterarray.dealery[2].name;
                        } else
                        {

                            $rootScope.dealer1 = $rootScope.Filterarray.dealerx[0].name; //"Bangkok & Greater";
                            $rootScope.dealer2 = $rootScope.Filterarray.dealery[0].name;
                        }
                        console.log("dealertresttttttttttttttttttttttttt");
                        console.log($rootScope.Filterarray.dealer);
                    });
                    $q.all([models, factors, dealers]).then(function (values) {
                        $rootScope.$broadcast("qsuccess", 'ok');
                    });
                } else if ($rootScope.dealerlogin)
                {
                    var dealerparameter = [{"name": "langCode", "value": $rootScope.language}, {"name": "dealer", "value": encodeURIComponent($rootScope.dealer)}];
                    $q.all([models, factors]).then(function (values) {
                        $rootScope.$broadcast("qsuccess", 'ok');
                    });
//                    mastersrv("Filters-getSOPAttributes", parameter).then(function (result) {
//                        $rootScope.Filterarray.sop = result.data;
//                        console.log("KPI Attributes ::::::::", $rootScope.Filterarray.sop);
//                    });
                } else
                {
                    var zones = mastersrv("Filters-getZone", parameter).then(function (result) {
                        $rootScope.Filterarray.zone = result.data;
                        $rootScope.Filterarray.zone.splice(0, 0, study);
                        $rootScope.zone = $rootScope.Filterarray.zone[0].name;
                    });

                    var regions = mastersrv("Filters-getRegion", parameter).then(function (result) {
                        $rootScope.Filterarray.region = result.data;
                        $rootScope.Filterarray.regionx = angular.copy($rootScope.Filterarray.region);
                        $rootScope.Filterarray.regiony = angular.copy($rootScope.Filterarray.region);
                        $rootScope.Filterarray.regiony.splice(0, 0, none);
                        $rootScope.Filterarray.region.splice(0, 0, study);
                        console.log("region", $rootScope.Filterarray.region);
                        console.log("region", $rootScope.Filterarray.regionx);
                        $rootScope.region1 = $rootScope.Filterarray.regionx[0].name; //"Bangkok & Greater";
                        $rootScope.region2 = $rootScope.Filterarray.regiony[2].name; //"Central and East";
                        console.log("region ::", $rootScope.Filterarray.region);
                    });

                    var dealers = mastersrv("Filters-getDealers", parameter).then(function (result) {
                        $rootScope.Filterarray.dealer = result.data;
                        console.log($rootScope.Filterarray.dealer);
                        $rootScope.Filterarray.dealerx = angular.copy($rootScope.Filterarray.dealer);
                        $rootScope.Filterarray.dealery = angular.copy($rootScope.Filterarray.dealer);
                        $rootScope.dealerScoreDealer = $rootScope.Filterarray.dealer[0].name;
                        $rootScope.Filterarray.dealer.splice(0, 0, study);
                        $rootScope.Filterarray.dealery.splice(0, 0, none);
                        if ($rootScope.Filterarray.dealerx.length > 1) {


                            $rootScope.dealer1 = $rootScope.Filterarray.dealerx[0].name; //"Bangkok & Greater";
                            $rootScope.dealer2 = $rootScope.Filterarray.dealery[2].name;
                        } else
                        {

                            $rootScope.dealer1 = $rootScope.Filterarray.dealerx[0].name; //"Bangkok & Greater";
                            $rootScope.dealer2 = $rootScope.Filterarray.dealery[0].name;
                        }
                        console.log($rootScope.Filterarray.dealer);
                    });

//                    mastersrv("Filters-getFactors", parameter).then(function (result) {
//                        $rootScope.Filterarray.factor = result.data;
//                        //                     $rootScope.Filterarray.factor.push("All");
//                    });
//                    var sopattrb = mastersrv("Filters-getSOPAttributes", parameter).then(function (result) {
//                        $rootScope.Filterarray.sop = result.data;
//                        console.log("KPI Attributes ::::::::", $rootScope.Filterarray.sop);
//                    });
                    $q.all([models, factors, dealers, sopattrb, periods]).then(function (values) {
                        console.log("qsuccess Done");
                        $rootScope.$broadcast("qsuccess", 'ok');
                    });
                }

                var sopattrb = mastersrv("Filters-getSOPAttributes", parameter).then(function (result) {
                    $rootScope.Filterarray.sop = result.data;
                    console.log("KPI Attributes ::::::::", $rootScope.Filterarray.sop);
                });
                var models = mastersrv("Filters-getModels", parameter).then(function (result) {
                    $rootScope.Filterarray.model = result.data;
                    $rootScope.Filterarray.model.splice(0, 0, study);
                    $rootScope.model = $rootScope.Filterarray.model[0].name;
                });
                var factors = mastersrv("Filters-getFactors", parameter).then(function (result) {
                    $rootScope.Filterarray.factor = result.data;
                    $rootScope.factor = $rootScope.Filterarray.factor[0].name;
                });
                var periods = mastersrv("Filters-getPeriods", parameter).then(function (result) {
                    $rootScope.Filterarray.period = result.data;
                });
            });
//            mastersrv("Filters-getSOPAttributes", parameter).then(function (result) {
//                $rootScope.Filterarray.sop = result.data;
//                console.log("KPI Attributes ::::::::", $rootScope.Filterarray.sop);
//            });
        }

        //filter end



        Highcharts.setOptions({
            chart: {
                style: {
//            fontFamily: 'Roboto',
                    fontFamily: 'Roboto Condensed'
                }
            }
        });
////sessions

        function sessionLogout()
        {

            $rootScope.events = [];
            $rootScope.idle = 300;
            $rootScope.timeout = 3;
            $rootScope.$on('IdleStart', function () {


            });
            $rootScope.$on('IdleEnd', function () {


            });
            $rootScope.$on('IdleWarn', function (e, countdown) {


            });
            $rootScope.$on('IdleTimeout', function () {
                Idle.unwatch();
                var logoutobj = [{"name": "loginsessionid", "value": $rootScope.loginsessionuserid},
                    {"name": "userSessionId", "value": $rootScope.userSessionId}];
                mastersrv("Systemuserview-logOut", logoutobj).then(function (result)
                {
                    $state.go('authentication.login', {});
                    $window.location.reload(true);
                });
                showSimpleToast();
            });
            $rootScope.$on('Keepalive', function () {


            });
            function addEvent(evt) {
                $rootScope.$evalAsync(function () {
                    $rootScope.events.push(evt);
                })
            }


            $rootScope.$watch('idle', function (value) {
                if (value !== null)
                    Idle.setIdle(value);
            });
            $rootScope.$watch('timeout', function (value) {
                if (value !== null)
                    Idle.setTimeout(value);
            });
        }

        function showSimpleToast() {
            var pinTo = "top right";
            $mdToast.show(
                    $mdToast.simple()
                    .textContent('Your Session Has Been Expired')
                    .position(pinTo)
                    .hideDelay(5000)
                    );
        }
        ;
//THE MASTER SERVICE replace in filter
        function mastersrv(actionstr, obj) {
            if ($rootScope.sessionid === 1001)
            {
                sessionLogout();
                Idle.watch();
            }

            var tempurl = URL + actionstr;
            if (obj) {
                for (var k = 0; k < obj.length; k++) {
                    tempurl = tempurl + "&" + obj[k].name;
                    tempurl = tempurl + "=" + obj[k].value;
                }
            }
            var pageName = pageNameBasedOnPageNumber();
            tempurl = tempurl + "&pageName";
            tempurl = tempurl + "=" + pageName;
            tempurl = tempurl + "&loginType";
            tempurl = tempurl + "=" + $rootScope.nameit;
            tempurl = tempurl + "&filter_change";
            tempurl = tempurl + "=" + $rootScope.filterchanged;
            tempurl = tempurl + "&loginsessionuserid"; //krishna added for session  on 13022018
            tempurl = tempurl + "=" + $rootScope.loginsessionuserid; //krishna added for session  on 13022018
            tempurl = tempurl + "&userSessionId"; //krishna added for session  on 13022018
            tempurl = tempurl + "=" + $rootScope.userSessionId; //krishna added for session  on 13022018

            var defer = $q.defer();
            console.log(tempurl);
            $http({
                method: "GET",
                url: tempurl,
                dataType: "jsonp",
                data: '',
                headers: {
                    "Content-Type": "application/json;charset=UTF-8"
                }
            }).then(function (response) {
                defer.resolve(response);
                if (response.data === -2)
                {
                    showSimpleToast();
                    $state.go('authentication.login', {});
                    $window.location.reload(true);
                }


            })


            return defer.promise;
//
//            var tempurl = URL + actionstr;
//
//            if (obj) {
//                for (var k = 0; k < obj.length; k++) {
//                    tempurl = tempurl + "&" + obj[k].name;
//                    tempurl = tempurl + "=" + obj[k].value;
//                }
//            }
// var pageName = pageNameBasedOnPageNumber();
//            tempurl = tempurl + "&pageName";
//            tempurl = tempurl + "=" + pageName;
//            tempurl = tempurl + "&loginType";
//            tempurl = tempurl + "=" +$rootScope.nameit;
//            tempurl = tempurl + "&filter_change";
//            tempurl = tempurl + "=" + $rootScope.filterchanged;
//            console.log(tempurl);
//            var temp = $http({
//                method: "GET",
//                url: tempurl,
//                dataType: "jsonp",
//                data: '',
//                headers: {
//                    "Content-Type": "application/json;charset=UTF-8"
//                }
//            });
//
//            return temp;

        }
        function pageNameBasedOnPageNumber() {
            var pageName = "";
            var finalpageName = "";
            if ($rootScope.pagenumber == '1') {
                pageName = "Summary";
            } else if ($rootScope.pagenumber == '2') {
                pageName = "Regional Analysis";
            } else if ($rootScope.pagenumber == '3') {
                pageName = "Trend Analysis";
            } else if ($rootScope.pagenumber == '4') {
                pageName = "Priority Analysis";
            } else if ($rootScope.pagenumber == '5') {
                pageName = "SOP Performance";
            } else if ($rootScope.pagenumber == '6') {
                pageName = "SOP Trend";
            } else if ($rootScope.pagenumber == '7') {
                pageName = "Other Diagnostics";
            } else if ($rootScope.pagenumber == '8') {
                pageName = "Dealer Ranking";
            } else if ($rootScope.pagenumber == '9') {
                pageName = "Dealer Snapshot";
            } else if ($rootScope.pagenumber == '10') {
                pageName = "Model Analysis";
            } else if ($rootScope.pagenumber == '11') {
                pageName = "Loyalty and Advocacy";
            } else if ($rootScope.pagenumber == '12') {
                pageName = "Red Alert";
            } else if ($rootScope.pagenumber == '13') {
                pageName = "Other Feedback";
            } else if ($rootScope.pagenumber == '14') {
                pageName = "Downloadable Dealer PDF";
            }

            return pageName;
        }





//bar chart service
        function barchartmiddle(type, color, data, margin, weight, spacing, seriesname, fontsize, stepsize, maxlength) {
            var category = [];
            for (var i = 0; i < data.length; i++) {
                console.log("inisde here is the fucniton");
                category[i] = data[i].name;
            }

            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    type: type,
                    marginLeft: margin,
                    marginRight: 25,
//              marginTop: 20

                },
                title: {
                    text: null
                },
                xAxis: {
                    categories: category,
                    title: {
                        text: null
                    },
                    "labels": {
                        "overflow": "justify",
                        style: {fontSize: fontsize},
                        step: stepsize
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
                    min: 300,
                    max: maxlength,
                    title: {
                        text: null
                    },
                    gridLineWidth: 0,
                    minorGridLineWidth: 0,
                    gridLineColor: 'transparent',
                    labels: {enabled: false,
                        overflow: 'justify'
                    }
                },
                plotOptions: {
                    series: {
                        pointWidth: weight,
                        groupPadding: spacing
//                         cursor: 'pointer',
//                        point: {
//                            events: {
//                                click: function () {
//                                    alert('Category: ' + this.category + ', value: ' + this.y);
//                                }
//                            }
//                        }

                    },
                    bar: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            inside: true,
                            align: 'right',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }

                        }
                    },
                    column: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            y: 30,
                            align: 'center',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }


                        },
                        depth: 25
                    }
                },
                credits: {
                    enabled: false
                },
//         size: {height:height,
//                width:width},

                legend: {
                    enabled: false
                },
                tooltip: {
                    formatter: function () {
                        var s =
                                this.point.options.tooltip;
                        return s;
                    },
                    crosshairs: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChartLocal();
                                    }
                                }]
                        }
                    }
                },
                navigation: {
                    buttonOptions: {
                        verticalAlign: 'top',
                        align: 'right',
                        y: -15
                    }
                },
                series: [{
                        color: color,
                        data: data,
                        name: seriesname
                    }]
            }
            ;
            return tempbar;
        }





//bar chart service
        function modelbarchart(type, color, data, margin, weight, spacing, seriesname, fontsize, stepsize, maxlength) {
            var category = [];
            for (var i = 0; i < data.length; i++) {
                console.log("inisde here is the fucniton");
                category[i] = data[i].name;
            }

            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    type: type,
                    marginLeft: margin,
                    marginRight: 25,
                    height: 450

                },
                title: {
                    text: null
                },
                xAxis: {
                    categories: category,
//                      min: 0,
//                            max:6,

// scrollbar: {
//        enabled: true
//         
//    },
                    title: {
                        text: null
                    },
                    "labels": {
                        "overflow": "justify",
                        style: {fontSize: fontsize},
                        step: stepsize
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
                    max: maxlength,
                    min: 0,
                    title: {
                        text: null
                    },
                    gridLineWidth: 0,
                    minorGridLineWidth: 0,
                    gridLineColor: 'transparent',
                    labels: {enabled: false,
                        overflow: 'justify'
                    }
                },
                plotOptions: {
                    series: {
                        pointWidth: weight,
                        groupPadding: spacing,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    var str = this.options.modelname;
                                    str = str.replace("*", '');
                                    str = str.replace("*", '');
                                    $rootScope.changedmodel = str;
                                    $rootScope.$broadcast("changemodelgraph", $rootScope.changedmodel);
                                    console.log('state2 params:', $rootScope.changedmodel);
                                    console.log('state2 params:', $rootScope.changedmodel);
                                }
                            }
                        }

                    },
                    bar: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            inside: true,
                            align: 'right',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }

                        }
                    },
                    column: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            y: 30,
                            align: 'right',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }


                        },
                        depth: 25
                    }
                },
                credits: {
                    enabled: false
                },
//         size: {height:height,
//                width:width},

                legend: {
                    enabled: false
                },
                tooltip: {
                    formatter: function () {
                        var s =
                                this.point.options.tooltip;
                        return s;
                    },
                    crosshairs: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChartLocal();
                                    }
                                }]
                        }
                    }
                },
                navigation: {
                    buttonOptions: {
                        verticalAlign: 'top',
                        align: 'right',
                        y: -15
                    }
                },
                series: [{
                        color: color,
                        data: data,
                        name: seriesname
                    }]
            }
            ;
            return tempbar;
        }

//bar chart service
        function barchart(type, color, data, margin, weight, spacing, seriesname, fontsize, stepsize, maxlength) {
            var category = [];
            for (var i = 0; i < data.length; i++) {
                console.log("inisde here is the fucniton");
                category[i] = data[i].name;
            }

            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    events: {
                        load: function () {
                            $rootScope.chartloadcount = $rootScope.chartloadcount + 1;
                            console.log($rootScope.chartloadcount);
                            if ($rootScope.chartloadcount > 21) {

                                $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                            }

                            if ($rootScope.chartloadcount > 23) {
                                $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                            }
                        }},
                    type: type,
                    marginLeft: margin
//              marginTop: 20

                },
                title: {
                    text: null
                },
                xAxis: {
                    categories: category,
                    title: {
                        text: null
                    },
                    "labels": {
                        "overflow": "justify",
                        style: {fontSize: fontsize},
                        step: stepsize
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
                    max: maxlength,
                    min: 0,
                    title: {
                        text: null
                    },
                    gridLineWidth: 0,
                    minorGridLineWidth: 0,
                    gridLineColor: 'transparent',
                    labels: {enabled: false,
                        overflow: 'justify'
                    }
                },
                plotOptions: {
                    series: {
                        pointWidth: weight,
                        groupPadding: spacing
//                         cursor: 'pointer',
//                        point: {
//                            events: {
//                                click: function () {
//                                    alert('Category: ' + this.category + ', value: ' + this.y);
//                                }
//                            }
//                        }

                    },
                    bar: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            inside: true,
                            align: 'right',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }

                        }
                    },
                    column: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            y: 30,
                            align: 'right',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }


                        },
                        depth: 25
                    }
                },
                credits: {
                    enabled: false
                },
//         size: {height:height,
//                width:width},

                legend: {
                    enabled: false
                },
                tooltip: {
                    formatter: function () {
                        var s =
                                this.point.options.tooltip;
                        return s;
                    },
                    crosshairs: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChartLocal();
                                    }
                                }]
                        }
                    }
                },
                series: [{
                        color: color,
                        data: data,
                        name: seriesname
                    }]
            }
            ;
            return tempbar;
        }




        function dsnapbarchart(maxlength, type, data, margin, weight, spacing, seriesname, fontsize, stepsize, suffix) {

            var scattername = [];
            for (var i = 0; i < data.length; i++) {
                scattername[i] = data[i].name;
            }

            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    type: type,
                    marginLeft: margin,
                    marginTop: 20

                },
                title: {
                    text: null
                },
                xAxis: {
                    categories: scattername,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    title: {
                        text: null
                    },
                    "labels": {
                        style: {fontSize: fontsize
                        },
                        step: stepsize
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
                    max: maxlength,
                    title: {
                        text: null
                    },
                    gridLineWidth: 0,
                    minorGridLineWidth: 0,
                    gridLineColor: 'transparent',
                    labels: {enabled: false,
                        overflow: 'justify'
                    }
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            inside: true,
                            align: 'right',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '10px'
                            }

                        },
                        pointWidth: weight,
                        groupPadding: spacing,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    var str = this.options.dealername;
                                    str = str.replace("*", '');
                                    str = str.replace("*", '');
                                    $rootScope.dealerScoreDealer = str;
                                    $state.go('triangular.csipage11', {dealer: 'All-'});
                                    console.log('state2 params:');
                                }
                            }
                        }

                    },
                    column: {
                        depth: 25
                    }
                },
                credits: {
                    enabled: false
                },
//         size: {height:height,
//                width:width},

                legend: {
                    enabled: false
                },
                tooltip: {
                    formatter: function () {
                        var s = this.point.options.tooltip;
                        return s + suffix;
                    },
                    crosshairs: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChartLocal();
                                    }
                                }]
                        }
                    }
                },
                series: [{
                        color: data.color,
                        data: data

                    }]
            }
            ;
            return tempbar;
        }



//multideaer spilt summaru

        function multbarchartsplit(maxlength, type, data, margin, weight, spacing, seriesname, fontsize, stepsize, suffix) {
            var categories = [];
            for (var i = 0; i < data.length; i++) {
                categories[i] = data[i].name;
            }

            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    events: {
                        load: function () {
                            $rootScope.chartloadcount = $rootScope.chartloadcount + 1;
                            console.log($rootScope.chartloadcount);
                            if ($rootScope.chartloadcount > 21) {

                                $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                            }


                            if ($rootScope.chartloadcount > 23) {

                                $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                            }
                        },
                    },
                    type: type,
                    marginLeft: 280,
                    marginRight: 23,
                    marginTop: 15,
                    marginBottom: 5

                },
                title: {
                    text: null
                },
                xAxis: {
                    categories: categories,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    title: {
                        text: null
                    },
                    "labels": {
//                        "overflow": "justify",
                        style: {fontSize: "11.5px",
                            textOverflow: 'none',
                            lineHeight: "9px"
//                            paddingBottom: "1px"
                        },
                        step: 1
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
                    max: maxlength,
                    title: {
                        text: null
                    },
                    gridLineWidth: 0,
                    minorGridLineWidth: 0,
                    gridLineColor: 'transparent',
                    labels: {enabled: false,
                        overflow: 'justify',
                        step: 1
                    }
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            inside: true,
                            align: 'right',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '11.5px'
                            }

                        },
                        pointWidth: weight,
                        groupPadding: spacing,
                        cursor: 'pointer',
                        point: {
//                            events: {
//                                click: function () {
//                                    var str = this.options.dealername;
//                                    str = str.replace("*", '');
//                                    str = str.replace("*", '');
//                                    $rootScope.dealerScoreDealer = str;
//                                    $state.go('triangular.csipage11', {dealer: 'All-'});
//                                    console.log('state2 params:');
//                                }
//                            }
                        }

                    },
                    column: {
                        depth: 25
                    }
                },
                credits: {
                    enabled: false
                },
//         size: {height:height,
//                width:width},

                legend: {
                    enabled: false
                },
                tooltip: {
                    formatter: function () {
                        var s = this.point.options.tooltip;
                        return s + suffix;
                    },
                    crosshairs: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChartLocal();
                                    }
                                }]
                        }
                    }
                },
                series: [{
                        color: data.color,
                        data: data,
                        name: seriesname

                    }]
            }
            ;
            return tempbar;
        }

//multicolor barchart
//bar chart service
        function multbarchart(maxlength, type, data, margin, weight, spacing, seriesname, fontsize, stepsize, suffix) {
            var categories = [];
            for (var i = 0; i < data.length; i++) {
                categories[i] = data[i].name;
            }

            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    events: {
                        load: function () {
                            $rootScope.chartloadcount = $rootScope.chartloadcount + 1
                            console.log($rootScope.chartloadcount)
                            if ($rootScope.chartloadcount > 21) {

                                $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                            }
                            if ($rootScope.chartloadcount > 23) {

                                $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                            }
                        }
                    },
                    type: type,
                    //    marginLeft: margin,
                    marginTop: 10,
                    marginBottom: 9,
                },
                title: {
                    text: null
                },
                xAxis: {
                    categories: categories,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    title: {
                        text: null
                    },
                    "labels": {
                        "overflow": "justify",
                        style: {fontSize: fontsize,
                            paddingBottom: "1px"
                        },
                        step: stepsize
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
                    max: maxlength,
                    title: {
                        text: null
                    },
                    gridLineWidth: 0,
                    minorGridLineWidth: 0,
                    gridLineColor: 'transparent',
                    labels: {enabled: false,
                        overflow: 'justify'
                    }
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            inside: true,
                            align: 'right',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '10px'
                            }

                        },
                        pointWidth: weight,
                        groupPadding: spacing,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    var str = this.options.dealername;
                                    str = str.replace("*", '');
                                    str = str.replace("*", '');
                                    $rootScope.SnapRedirectBarClick = "true";
                                    console.log('Dealer selected :', str);
                                    console.log('Dealer selected :', $rootScope.SnapRedirectBarClick);
                                    $rootScope.flagdealerScoreDealer = true;
                                    $rootScope.dsnapDealer = str;
                                    $rootScope.dealer = str;
                                    $state.go('triangular.csidealersnapshot', {dealer: str});
                                }
                            }
                        }

                    },
                    column: {
                        depth: 25
                    }
                },
                credits: {
                    enabled: false
                },
//         size: {height:height,
//                width:width},

                legend: {
                    enabled: false
                },
                tooltip: {
                    formatter: function () {
                        var s = this.point.options.tooltip;
                        return s + suffix;
                    },
                    crosshairs: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChartLocal();
                                    }
                                }]
                        }
                    }
                },
                series: [{
                        color: data.color,
                        data: data,
                        name: seriesname

                    }]
            }
            ;
            return tempbar;
        }




        function loyalitybar(title, response, colorpoint, color) {

//           var categories=[];
//         for(var i=0;i<response.data[0].data.length;i++){
//                        categories[i]=response.data[0].data[i].category;
//
//                    }
            var xaxisname = response[0].axisname;
            var categoryname = title;
            console.log(categoryname);
            var multi_return =
                    {lang: {
                            downloadPNG: $rootScope.downloadpng,
                            downloadCSV: $rootScope.downloadcsv,
                            downloadXLS: $rootScope.downloadxls,
                            contextButtonTitle: $rootScope.downloadheader
                        },
                        "chart": {
                            events: {
                                load: function () {
                                    $rootScope.chartloadcount = $rootScope.chartloadcount + 1;
                                    console.log($rootScope.chartloadcount);
                                    if ($rootScope.chartloadcount > 21) {

                                        $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                                    }
                                    if ($rootScope.chartloadcount > 23) {
                                        $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                                    }
                                }
                            },
                            "type": "column"
                        },
                        colors: color,
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                            crosshairs: false
                        },
                        plotOptions: {
                            series: {
                                borderWidth: 0,
                                maxPointWidth: 40,
//                             cursor: 'pointer',
//                                point: {
//                                    events: {
//                                        click: function () {
//                                            alert('Category: ' + this.category + ', value: ' + this.y);
//                                        }
//                                    }
//                                },

                                dataLabels: {
                                    enabled: true,
                                    formatter: function () {
                                        var pcnt = this.point.options.bartitle;
                                        return pcnt;
                                    },
                                    color: '#000000',
                                    y: -2,
//                                            align: 'right',
                                    style: {
                                        //color: '#FFFFFF',
                                        textShadow: 'none',
                                        fontWeight: 'light',
                                        fontSize: '14px'
                                    }
                                }
                            }
                        },
                        "series": response,
                        "title": {
                            "text": title,
                            align: "center",
                            margin: 10,
                            style: {
                                textShadow: 'none',
                                fontWeight: '400',
                                fontSize: '19px'

                            }
                        },
                        "credits": {
                            "enabled": false
                        },
                        "loading": false,
                        "size": {},
                        "xAxis": {
                            gridLineWidth: 0,
                            categories: [categoryname],
                            lineColor: 'transparent',
                            tickLength: 0,
                            minorGridLineWidth: 0,
                            labels: {
                                enabled: false
                            },
                            title: {
                                style: {
                                    color: '#556977',
                                    fontSize: '12px'
                                },
                                text: xaxisname
                            }

                        },
                        "yAxis": {
                            title: {
                                text: null,
                                align: 'high'
                            },
                            labels: {
                                enabled: false
                            },
                            gridLineWidth: 0,
                            allowDecimals: true,
                            min: 0

                        },
                        legend: {
                            itemStyle: {
                                // font: '9pt Trebuchet MS, Verdana, sans-serif',
                                fontSize: '11px'
                                        //color: '#A0A0A0'
                            },
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom',
                            enabled: true

                        },
                        exporting: {
                            formAttributes: {
                                target: '_blank'
                            },
                            buttons: {
                                contextButton: {
                                    menuItems: [{
                                            text: $rootScope.downloadxls,
                                            onclick: function () {
                                                this.downloadXLS();
                                            }
                                        }, {
                                            text: $rootScope.downloadcsv,
                                            onclick: function () {
                                                this.downloadCSV();
                                            }
                                        }, {
                                            text: $rootScope.downloadpng,
                                            onclick: function () {
                                                this.exportChartLocal();
                                            }
                                        }]
                                }
                            }
                        }
                    };
            return multi_return;
        }



        function summaryplainbar(type, color, data, seriesname, margin, weight, spacing, pointdata, markersize, colorpoint, maxwidth, labelenable, xaxistitle) {

            var sdata = [];
            var wdata = [];
            var scattername = [];
            for (var i = 0; i < data.length; i++) {
                sdata[i] = data[i].svalue;
            }
            for (var i = 0; i < data.length; i++) {
                wdata[i] = data[i].svalue1;
            }

            for (var i = 0; i < data.length; i++) {
                scattername[i] = data[i].name;
            }


            if (maxwidth == undefined) {
                maxwidth = '460px';
            }

            var bestname = data[0].scatter;
            var worstname = data[0].scatter1;
            if (labelenable == undefined) {
                labelenable = true;
            }

            console.log(scattername);
            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    events: {
                        load: function () {
                            $rootScope.chartloadcount = $rootScope.chartloadcount + 1;
                            console.log($rootScope.chartloadcount);
                            if ($rootScope.chartloadcount > 21) {

                                $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                            }
                            if ($rootScope.chartloadcount > 23) {
                                $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                            }
                        }
                    },
                    type: type,
                    marginLeft: margin,
                    marginRight: 25,
                    marginTop: 10
                },
                colors: color,
                title: {
                    text: '',
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
//                    max:1000,
                    categories: scattername,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0,
                    minPadding: 0.5,
                    maxPadding: 0.5,
                    title: {
                        text: xaxistitle
                    },
                    "labels": {
                        enabled: labelenable,
//                        "overflow": "justify",
                        style: {fontSize: 12.5},
                        step: 1
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
//                    min: 300,
                    max: 760,
                    gridLineWidth: 0,
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: null
                    }
                },
                plotOptions: {
                    series: {
                    },
                    scatter: {
                        dataLabels: {
                            enabled: false,
                            align: 'left',
                            x: 5,
                            y: 12,
                            marker: {
                                radius: 8,
                                fillColor: 'blue'
                            },
                            style: {
                                fontWeight: 'normal'
                            }
                        }, tooltip: {
                            pointFormat: '<b> </b><br> {point.y}'
                        }
                    },
                    column: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            y: 30,
                            verticalAlign: 'center',
                            align: 'center',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }
                        },
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                            crosshairs: false
                        },
                        pointWidth: weight,
                        groupPadding: spacing,
                        depth: 25,
                        colorByPoint: colorpoint


                    }
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        verticalAlign: 'top',
                        align: 'right',
                        y: -15
                    }
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChartLocal();
                                    }
                                }]
                        }
                    }
                },
                size: {width: maxwidth},
                series: [{
                        data: data,
                        name: seriesname

                    },
                    {
                        type: 'scatter',
                        color: '#689F38',
                        data: sdata,
                        name: bestname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    },
                    {
                        type: 'scatter',
                        color: '#800000',
                        data: wdata,
                        name: worstname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    }]
            }
            ;
            return tempbar;
        }

        function kpiplainbar(type, fontsize, color, data, seriesname, margin, weight, spacing, pointdata, markersize, colorpoint, suffix) {
            var category = [];
            for (var i = 0; i < data.length; i++) {
                category[i] = data[i].name;
            }
            console.log(data);
            var bdata = [];
            var sdata = [];
            var scatteravg = data[0].scatteravg;
            var scatterbest = data[0].scatterbest;
            var selectionName = data[0].mainSelectionName;
            console.log("selectionName : ::::", selectionName);
            for (var i = 0; i < data.length; i++) {
                bdata[i] = data[i].bvalue;
                sdata[i] = data[i].svalue;
            }
            var minAndMax = findingMinAndMaxValues(data);
            var scaMin = minAndMax[0];
            var scaMax = minAndMax[1];
            console.log(scaMin + " : findingMinAndMaxValues(data) : " + scaMax);
            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader,
                    noData: "Data Is Not Available For This Criteria"
                },
                chart: {
                    events: {
                        load: function () {
                            $rootScope.chartloadcount = $rootScope.chartloadcount + 1
                            console.log($rootScope.chartloadcount)
                            if ($rootScope.chartloadcount > 21) {

                                $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                            }

                            if ($rootScope.chartloadcount > 23) {
                                $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                            }
                        }
                    },
                    type: type,
                    marginLeft: margin,
                    marginTop: 20
                },
                colors: color,
                title: {
                    text: null
                },
                xAxis: {
                    categories: category,
//                    lineColor: 'transparent',
//                    tickLength: 0,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0,
                    minPadding: 0.5,
                    maxPadding: 0.5,
                    title: {
                        text: null
                    },
                    "labels": {
                        "overflow": "justify",
                        style: {
                            //   font:'14px bold Vardana'
                            color: '#556977',
                            fontSize: fontsize,
                        },
                        step: 1
                    },
                    legend: {
                        enabled: false
                    },
                },
                yAxis: {
                    max: scaMax,
                    min: scaMin,
                    gridLineWidth: 0,
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: null
                    },
                },
                plotOptions: {
                    series: {
                        pointWidth: weight,
                    },
                    scatter: {
                        dataLabels: {
                            enabled: false,
                            align: 'left',
                            x: 5,
                            y: 12,
                            format: '{y}%',
                            marker: {
                                radius: 8,
                                fillColor: 'blue',
                            },
                            style: {
                                fontWeight: 'normal'
                            }
                        }, tooltip: {
                            pointFormat: '<b> </b><br> {point.y}%'
                        }
                    },
                    bar: {
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                        },
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
//                            color: '#ffffff',
                            inside: true,
                            align: 'right',
                            tooltip: {
                                formatter: function () {
                                    var s =
                                            this.point.options.bartitle;
                                    return;
                                },
                            },
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '10px',
                            }

                        },
                    },
                    column: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#cdcdcd',
                            inside: true
                        },
                        groupPadding: spacing,
                        colorByPoint: colorpoint

                    }
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: true,
                    itemStyle: {
                        fontSize: '10px',
                        //color: '#A0A0A0'
                    },
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'

                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChart();
                                    }
                                }]
                        }
                    }
                },
                series: [{
                        data: data,
                        name: selectionName

                    },
                    {
                        type: 'scatter',
                        color: '#689F38',
                        data: bdata,
                        name: scatterbest,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    },
                    {
                        type: 'scatter',
                        color: '#9D78DD',
                        data: sdata,
                        name: scatteravg,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    }]
            }
            ;
            return tempbar;
        }



        function creditsummaryplainbar(creditor, type, color, data, seriesname, margin, weight, spacing, pointdata, markersize, colorpoint, maxwidth, labelenable, xaxistitle) {

            var sdata = [];
            var wdata = [];
            var scattername = [];
            for (var i = 0; i < data.length; i++) {
                sdata[i] = data[i].svalue;
            }
            for (var i = 0; i < data.length; i++) {
                wdata[i] = data[i].svalue1;
            }

            for (var i = 0; i < data.length; i++) {
                scattername[i] = data[i].name;
            }


            if (maxwidth == undefined) {
                maxwidth = '460px';
            }

            var bestname = data[0].scatter;
            var worstname = data[0].scatter1;
            if (labelenable == undefined) {
                labelenable = true;
            }

            console.log(scattername);
            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    events: {
                        load: function () {
                            $rootScope.chartloadcount = $rootScope.chartloadcount + 1;
                            console.log($rootScope.chartloadcount);
                            if ($rootScope.chartloadcount > 21) {

                                $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                            }
                            if ($rootScope.chartloadcount > 23) {
                                $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                            }
                        }
                    },
                    type: type,
                    marginLeft: margin,
                    marginRight: 25,
                    marginTop: 10
                },
                colors: color,
                title: {
                    text: '',
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
//                    max:1000,
                    categories: scattername,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0,
                    minPadding: 0.5,
                    maxPadding: 0.5,
                    title: {
                        text: xaxistitle
                    },
                    "labels": {
                        enabled: labelenable,
//                        "overflow": "justify",
                        style: {fontSize: 12.5},
                        step: 1
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
//                    min: 600   ,
//                    max: null ,
                    gridLineWidth: 0,
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: null
                    }
                },
                plotOptions: {
                    series: {
                    },
                    scatter: {
                        dataLabels: {
                            enabled: false,
                            align: 'left',
                            x: 5,
                            y: 12,
                            marker: {
                                radius: 8,
                                fillColor: 'blue'
                            },
                            style: {
                                fontWeight: 'normal'
                            }
                        }, tooltip: {
                            pointFormat: '<b> </b><br> {point.y}'
                        }
                    },
                    column: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            y: 30,
                            verticalAlign: 'center',
                            align: 'center',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }
                        },
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                            crosshairs: false
                        },
                        pointWidth: weight,
                        groupPadding: spacing,
                        depth: 25,
                        colorByPoint: colorpoint


                    }
                },
                credits: {
                    enabled: false
                },
//            credits: {
//                text: creditor,
//            },

                legend: {
                    enabled: false
                },
                navigation: {
                    buttonOptions: {
                        verticalAlign: 'top',
                        align: 'right',
                        y: -15
                    }
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChartLocal();
                                    }
                                }]
                        }
                    }
                },
                size: {width: maxwidth},
                series: [{
                        data: data,
                        name: seriesname

                    },
                    {
                        type: 'scatter',
                        color: '#689F38',
                        data: sdata,
                        name: bestname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    },
                    {
                        type: 'scatter',
                        color: '#800000',
                        data: wdata,
                        name: worstname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    }]
            }
            ;
            return tempbar;
        }



        function clicksummaryplainbar(creditor, type, color, data, seriesname, margin, weight, spacing, pointdata, markersize, colorpoint, maxwidth, labelenable, xaxistitle) {

            var sdata = [];
            var wdata = [];
            var scattername = [];
            for (var i = 0; i < data.length; i++) {
                sdata[i] = data[i].svalue;
            }
            for (var i = 0; i < data.length; i++) {
                wdata[i] = data[i].svalue1;
            }

            for (var i = 0; i < data.length; i++) {
                scattername[i] = data[i].name;
            }


            if (maxwidth == undefined) {
                maxwidth = '460px';
            }

            var bestname = data[0].scatter;
            var worstname = data[0].scatter1;
            if (labelenable == undefined) {
                labelenable = true;
            }

            console.log(scattername);
            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    type: type,
                    marginLeft: margin,
                    marginTop: 10,
                    marginRight: 25
                },
                colors: color,
                title: {
                    text: '',
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
//                    max:1000,
                    categories: scattername,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0,
                    minPadding: 0.5,
                    maxPadding: 0.5,
                    title: {
                        text: xaxistitle
                    },
                    "labels": {
                        enabled: labelenable,
//                        "overflow": "justify",
                        style: {fontSize: 12.5},
                        step: 1
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
//                    min: 300,
//                    max: 800,
                    gridLineWidth: 0,
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: null
                    }
                },
                plotOptions: {
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    $rootScope.changedfactor = this.options.factorname;
                                    $rootScope.$broadcast("changefactordsnap", $rootScope.changedfactor);
                                    console.log('state2 params:', $rootScope.changedfactor);
                                    console.log('state2 params:', $rootScope.changedfactor);
                                }
                            }
                        }

                    },
                    scatter: {
                        dataLabels: {
                            enabled: false,
                            align: 'left',
                            x: 5,
                            y: 12,
                            marker: {
                                radius: 8,
                                fillColor: 'blue'
                            },
                            style: {
                                fontWeight: 'normal'
                            }
                        },
                        tooltip: {
                            pointFormat: '<b> </b><br> {point.y}'
                        }
                    },
                    column: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            y: 30,
                            layout: 'vertical',
                            verticalAlign: 'center',
                            align: 'center',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }
                        },
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                            crosshairs: false
                        },
                        pointWidth: weight,
                        groupPadding: spacing,
                        depth: 25,
                        colorByPoint: colorpoint


                    }
                },
                credits: {
                    enabled: false
                },
//            credits: {
//                text: creditor,
//            },

                legend: {
                    enabled: false
                },
//          navigation: {
//            buttonOptions: {
//                verticalAlign: 'top',
//                y: -20
//            }
//        },

                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChartLocal();
                                    }
                                }]
                        }
                    }
                },
                size: {width: maxwidth},
                series: [{
                        data: data,
                        name: seriesname

                    },
                    {
                        type: 'scatter',
                        color: '#689F38',
                        data: sdata,
                        name: bestname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    },
                    {
                        type: 'scatter',
                        color: '#800000',
                        data: wdata,
                        name: worstname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    }]
            }
            ;
            return tempbar;
        }




        function plainbar(type, color, data, seriesname, margin, weight, spacing, pointdata, markersize, colorpoint, maxwidth, labelenable, xaxistitle) {

            var sdata = [];
            var scattername = [];
            for (var i = 0; i < data.length; i++) {
                sdata[i] = data[i].svalue;
            }

            for (var i = 0; i < data.length; i++) {
                scattername[i] = data[i].name;
            }

            if (maxwidth == undefined) {
                maxwidth = '460px';
            }

            var bestname = data[0].scatter;
            if (labelenable == undefined) {
                labelenable = true;
            }

            console.log(scattername);
            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    type: type,
                    marginLeft: margin,
                    marginTop: 10
                },
                colors: color,
                title: {
                    text: '',
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
//                    max:1000,
                    categories: scattername,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0,
                    minPadding: 0.5,
                    maxPadding: 0.5,
                    title: {
                        text: xaxistitle
                    },
                    "labels": {
                        enabled: labelenable,
//                        "overflow": "justify",
                        style: {fontSize: 12},
                        step: 1
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
//                     min:250,
                    max: 1000,
                    gridLineWidth: 0,
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: null
                    }
                },
                plotOptions: {
                    series: {
                    },
                    scatter: {
                        dataLabels: {
                            enabled: true,
                            align: 'left',
                            x: 5,
                            y: 12,
                            marker: {
                                radius: 8,
                                fillColor: 'blue'
                            },
                            style: {
                                fontWeight: 'normal'
                            }
                        }, tooltip: {
                            pointFormat: '<b> </b><br> {point.y}'
                        }
                    },
                    column: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            y: 30,
                            align: 'center',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }
                        },
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                            crosshairs: false
                        },
                        pointWidth: weight,
                        groupPadding: spacing,
                        depth: 25,
                        colorByPoint: colorpoint


                    }
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChartLocal();
                                    }
                                }]
                        }
                    }
                },
                size: {width: maxwidth},
                series: [{
                        data: data,
                        name: seriesname

                    },
                    {
                        type: 'scatter',
                        color: '#689F38',
                        data: sdata,
                        name: bestname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    }]
            }
            ;
            return tempbar;
        }


//dsnap plainbar with click

        function dsnapplainbar(type, color, data, seriesname, margin, weight, spacing, pointdata, markersize, colorpoint, maxwidth, labelenable, xaxistitle) {

            var sdata = [];
            var scattername = [];
            for (var i = 0; i < data.length; i++) {
                sdata[i] = data[i].svalue;
            }

            for (var i = 0; i < data.length; i++) {
                scattername[i] = data[i].name;
            }

            if (maxwidth == undefined) {
                maxwidth = '460px';
            }

            var bestname = data[0].scatter;
            if (labelenable == undefined) {
                labelenable = true;
            }

            console.log(scattername);
            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    type: type,
                    marginLeft: margin,
                    marginTop: 10
                },
                colors: color,
                title: {
                    text: '',
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
//                    max:1000,
                    categories: scattername,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0,
                    minPadding: 0.5,
                    maxPadding: 0.5,
                    title: {
                        text: xaxistitle
                    },
                    "labels": {
                        enabled: labelenable,
//                        "overflow": "justify",
                        style: {fontSize: 12},
                        step: 1
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
//                     min:250,
                    max: 1000,
                    gridLineWidth: 0,
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: null
                    }
                },
                plotOptions: {
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    $rootScope.factorClick = this.options.name;
                                    $rootScope.$broadcast("factorClick", $rootScope.factorClick);
                                    console.log('state2 params:', $rootScope.factorClick);
                                }
                            }
                        }
                    },
                    scatter: {
                        dataLabels: {
                            enabled: true,
                            align: 'left',
                            x: 5,
                            y: 12,
                            marker: {
                                radius: 8,
                                fillColor: 'blue'
                            },
                            style: {
                                fontWeight: 'normal'
                            }
                        }, tooltip: {
                            pointFormat: '<b> </b><br> {point.y}'
                        }
                    },
                    column: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            y: 30,
                            align: 'center',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }
                        },
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    $rootScope.factorClick = this.options.name;
                                    $rootScope.$broadcast("factorClick", $rootScope.factorClick);
                                    console.log('state2 params:', $rootScope.factorClick);
                                }
                            }
                        },
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                            crosshairs: false
                        },
                        pointWidth: weight,
                        groupPadding: spacing,
                        depth: 25,
                        colorByPoint: colorpoint


                    }
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
//        exporting: {
//        csv: {
//            dateFormat: '%Y-%m-%d'
//        }
//    },

                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChartLocal();
                                    }
                                }]
                        }
                    }
                },
                size: {width: maxwidth},
                series: [{
                        data: data,
                        name: seriesname

                    },
                    {
                        type: 'scatter',
                        color: '#689F38',
                        data: sdata,
                        name: bestname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    }]
            }
            ;
            return tempbar;
        }


//for Regional Plainbar


        function regplainbar(type, color, data, seriesname, margin, weight, spacing, pointdata, markersize, colorpoint, labelenable, xaxistitle) {

            var sdata = [];
            var wdata = [];
            var scattername = [];
            for (var i = 0; i < data.length; i++) {
                sdata[i] = data[i].svalue;
            }
            for (var i = 0; i < data.length; i++) {
                wdata[i] = data[i].svalue1;
            }

            for (var i = 0; i < data.length; i++) {
                scattername[i] = data[i].name;
            }

            var bestname = data[0].scatter;
            var worstname = data[0].scatter1;
            if (labelenable == undefined) {
                labelenable = true;
            }
//var min = Math.min.apply(Math, data.map(function (item) {
//                return item.svalue1-10;
//            }));
//
//            var max = Math.max.apply(Math,data.map(function (item) {
//                return item.svalue+10;
//            }));

//            alert(min+":"+max)
            console.log(scattername);
            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    events: {
                        load: function () {
                            $rootScope.chartloadcount = $rootScope.chartloadcount + 1;
                            console.log($rootScope.chartloadcount);
                            if ($rootScope.chartloadcount > 21) {

                                $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                            }
                            if ($rootScope.chartloadcount > 23) {
                                $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                            }
                        }
                    },
                    type: type,
                    marginLeft: margin
                },
                colors: color,
                title: {
                    text: '',
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
//                    max:1000,
                    categories: scattername,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0,
                    minPadding: 0.5,
                    maxPadding: 0.5,
                    title: {
                        text: xaxistitle
                    },
                    "labels": {
                        enabled: labelenable,
//                        "overflow": "justify",
                        style: {fontSize: 13},
                        step: 1
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
                    //     min: 780,
                    max: 950,
                    gridLineWidth: 0,
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: null
                    }
                },
                plotOptions: {
                    series: {
                        minPointLength: 40,
                        pointWidth: weight,
                        groupPadding: spacing,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function () {
                                    var str = this.options.regionname;
                                    str = str.replace("*", '');
                                    str = str.replace("*", '');
                                    $rootScope.changedmodel = str;
                                    $rootScope.$broadcast("changeregionone", $rootScope.changedmodel);
                                    console.log('state2 params:', $rootScope.changedmodel);
                                    console.log('state2 params:', $rootScope.changedmodel);
                                }
                            }
                        }, allowPointSelect: true,
                        states: {
                            select: {
                                color: '#D1FF33',
                                borderWidth: 5,
                                borderColor: 'Blue'
                            }
                        },
                    },
                    scatter: {
                        dataLabels: {
                            enabled: false,
                            align: 'left',
                            x: 5,
                            y: 12,
                            marker: {
                                radius: 8,
                                fillColor: 'blue'
                            },
                            style: {
                                fontWeight: 'normal'
                            }
                        }, tooltip: {
                            pointFormat: '<b> </b><br> {point.y}'
                        }
                    },
                    column: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            y: 30,
                            align: 'center',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }
                        },
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                            crosshairs: false
                        },
                        pointWidth: weight,
                        groupPadding: spacing,
                        depth: 25,
                        colorByPoint: colorpoint


                    }
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChartLocal();
                                    }
                                }]
                        }
                    }
                },
                series: [
                    {
                        data: data,
                        name: seriesname
                    },
                    {
                        type: 'scatter',
                        color: '#689F38',
                        data: sdata,
                        name: bestname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    },
                    {
                        type: 'scatter',
                        color: '#800000',
                        data: wdata,
                        name: worstname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    }
                ]
            }
            ;
            return tempbar;
        }



//plainbar bar

        function barplainbar(type, color, data, seriesname, margin, weight, spacing, pointdata, markersize, colorpoint, suffix, labelenable, xaxistitle, min, max) {


            console.log(suffix);
            var sdata = [];
            var wdata = [];
            var scattername = [];
            for (var i = 0; i < data.length; i++) {
                sdata[i] = data[i].svalue;
            }
            for (var i = 0; i < data.length; i++) {
                wdata[i] = data[i].svalue1;
            }

            for (var i = 0; i < data.length; i++) {
                scattername[i] = data[i].name;
            }

            if (labelenable == undefined) {
                labelenable = true;
            }

            var bestname = data[0].scatter;
            var worstname = data[0].scatter1;
            console.log(scattername);
            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    events: {
                        load: function () {
                            $rootScope.chartloadcount = $rootScope.chartloadcount + 1;
                            console.log($rootScope.chartloadcount);
                            if ($rootScope.chartloadcount > 21) {

                                $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                            }
                            if ($rootScope.chartloadcount > 23) {
                                $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                            }
                        }
                    },
                    type: type,
                    marginLeft: margin
                },
                colors: color,
                title: {
                    text: '',
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
//                    min:min,
//                    max:max,
//                    max:1000,
                    categories: scattername,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0,
                    minPadding: 0.5,
                    maxPadding: 0.5,
                    title: {
                        text: xaxistitle
                    },
                    "labels": {
                        enabled: labelenable,
                        "overflow": "justify",
                        layout: 'vertical',
                        style: {fontSize: "12px",
                            textOverflow: 'none'
//                            paddingBottom: "1px"
                        },
                        step: 1
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
                    gridLineWidth: 0,
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: null
                    }
                },
                plotOptions: {
                    series: {
                    },
                    scatter: {
                        dataLabels: {
                            enabled: false,
                            align: 'left',
                            x: 5,
                            y: 12,
                            marker: {
                                radius: 8,
                                fillColor: 'blue'
                            },
                            style: {
                                fontWeight: 'normal'
                            }
                        }, tooltip: {
                            pointFormat: '<b> </b><br> {point.y} ' + suffix
                        }
                    },
                    bar: {
                        tooltip: {
                            pointFormat: '<b> </b><br> {point.y} ' + suffix
                        },
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#000000',
                            inside: true,
//                            layout: 'vertical',
//                            verticalAlign: 'center', 


                            align: 'center',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }

                        }
                    },
                    column: {
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s + suffix;
                            }

                        },
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            y: 30,
//                           layout: 'vertical',
//                            verticalAlign: 'center', 


                            align: 'center',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }
                        },
                        pointWidth: weight,
                        groupPadding: spacing,
                        depth: 25,
                        colorByPoint: colorpoint


                    }
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChartLocal();
                                    }
                                }]
                        }
                    }
                },
                navigation: {
                    buttonOptions: {
                        verticalAlign: 'top',
                        align: 'right',
                        y: -15
                    }
                },
                series: [{
                        data: data,
                        name: seriesname

                    },
                    {
                        type: 'scatter',
                        color: '#689F38',
                        data: sdata,
                        name: bestname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    },
                    {
                        type: 'scatter',
                        color: '#800000',
                        data: wdata,
                        name: worstname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    }]
            }
            ;
            return tempbar;
        }

//barplain bar split

        function barplainbarsplit(type, color, data, seriesname, margin, weight, spacing, pointdata, markersize, colorpoint, suffix, labelenable, xaxistitle) {
            console.log(suffix);
            var sdata = [];
            var wdata = [];
            var scattername = [];
            for (var i = 0; i < data.length; i++) {
                sdata[i] = data[i].svalue;
            }
            for (var i = 0; i < data.length; i++) {
                wdata[i] = data[i].svalue1;
            }

            for (var i = 0; i < data.length; i++) {
                scattername[i] = data[i].name;
            }

            if (labelenable == undefined) {
                labelenable = true;
            }

            var bestname = data[0].scatter;
            var worstname = data[0].scatter1;
            console.log(scattername);
            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    type: type,
                    marginLeft: margin,
                    marginRight: 25
                },
                colors: color,
                title: {
                    text: '',
                    style: {
                        display: 'none'
                    }
                },
                xAxis: {
//                    max:1000,
                    categories: scattername,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0,
                    minPadding: 0.5,
                    maxPadding: 0.5,
                    title: {
                        text: xaxistitle
                    },
                    "labels": {
                        enabled: labelenable,
//                        "overflow": "justify",
                        style: {fontSize: "12px",
                            textOverflow: 'none'
//                            paddingBottom: "1px"
                        },
                        step: 1
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
                    gridLineWidth: 0,
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: null
                    }
                },
                plotOptions: {
                    series: {
                    },
                    scatter: {
                        dataLabels: {
                            enabled: false,
                            align: 'left',
                            x: 5,
                            y: 12,
                            marker: {
                                radius: 8,
                                fillColor: 'blue'
                            },
                            style: {
                                fontWeight: 'normal'
                            }
                        }, tooltip: {
                            pointFormat: '<b> </b><br> {point.y} ' + suffix
                        }
                    },
                    bar: {
                        tooltip: {
                            pointFormat: '<b> </b><br> {point.y} ' + suffix
                        },
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            inside: true,
                            align: 'left',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }

                        }
                    },
                    column: {
                        tooltip: {
                            pointFormat: '<b> </b><br> {point.y} ' + suffix
                        },
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            y: 30,
                            align: 'center',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }
                        },
                        pointWidth: weight,
                        groupPadding: spacing,
                        depth: 25,
                        colorByPoint: colorpoint


                    }
                },
//                tooltip: {
//                    formatter: function () {
//                        var s = this.point.options.tooltip;
//                        return s + suffix;
//                    },
//                    crosshairs: false
//                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChartLocal();
                                    }
                                }]
                        }
                    }
                },
                series: [{
                        data: data,
                        name: seriesname

                    },
                    {
                        type: 'scatter',
                        color: '#689F38',
                        data: sdata,
                        name: bestname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    },
                    {
                        type: 'scatter',
                        color: '#800000',
                        data: wdata,
                        name: worstname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    }]
            }
            ;
            return tempbar;
        }
//////

        function barplainbarsplitdelighted1(type, color, data, seriesname, margin, weight, spacing, pointdata, markersize, colorpoint, suffix, labelenable, xaxistitle) {
            console.log(suffix);
            var sdata = [];
            var wdata = [];
            var scattername = [];
            for (var i = 0; i < data.length; i++) {
                sdata[i] = data[i].svalue;
            }
            for (var i = 0; i < data.length; i++) {
                wdata[i] = data[i].svalue1;
            }

            for (var i = 0; i < data.length; i++) {
                scattername[i] = data[i].name;
            }

            if (labelenable == undefined) {
                labelenable = true;
            }

            var bestname = data[0].scatter;
            var worstname = data[0].scatter1;
            console.log(scattername);
            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    type: type,
                    marginLeft: margin,
                    marginRight: 25
                },
                colors: color,
                title: {
                    text: '',
                    style: {
                        display: 'none'
                    }
                },
                subtitle: {
                    text: 'Mean',
                    align: 'right',
                    x: -30
                },
                xAxis: {
//                    max:1000,
                    categories: scattername,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0,
                    minPadding: 0.5,
                    maxPadding: 0.5,
                    title: {
                        text: xaxistitle
                    },
                    "labels": {
                        enabled: labelenable,
//                        "overflow": "justify",
                        style: {fontSize: "12px",
                            textOverflow: 'none'
//                            paddingBottom: "1px"
                        },
                        step: 1
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
                    gridLineWidth: 0,
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: null
                    }
                },
                plotOptions: {
                    series: {
                    },
                    scatter: {
                        dataLabels: {
                            enabled: false,
                            align: 'left',
                            x: 5,
                            y: 12,
                            marker: {
                                radius: 8,
                                fillColor: 'blue'
                            },
                            style: {
                                fontWeight: 'normal'
                            }
                        }, tooltip: {
                            pointFormat: '<b> </b><br> {point.y} ' + suffix
                        }
                    },
                    bar: {
                        tooltip: {
                            pointFormat: '<b> </b><br> {point.y} ' + suffix
                        },
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            inside: true,
                            align: 'left',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }

                        }
                    },
                    column: {
                        tooltip: {
                            pointFormat: '<b> </b><br> {point.y} ' + suffix
                        },
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            y: 30,
                            align: 'center',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }
                        },
                        pointWidth: weight,
                        groupPadding: spacing,
                        depth: 25,
                        colorByPoint: colorpoint


                    }
                },
//                tooltip: {
//                    formatter: function () {
//                        var s = this.point.options.tooltip;
//                        return s + suffix;
//                    },
//                    crosshairs: false
//                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChartLocal();
                                    }
                                }]
                        }
                    }
                },
                series: [{
                        data: data,
                        name: seriesname

                    },
                    {
                        type: 'scatter',
                        color: '#689F38',
                        data: sdata,
                        name: bestname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    },
                    {
                        type: 'scatter',
                        color: '#800000',
                        data: wdata,
                        name: worstname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    }]
            }
            ;
            return tempbar;
        }


/////////
        function sopplainbar(type, fontsize, color, data, seriesname, margin, weight, spacing, pointdata, markersize, colorpoint, suffix) {
            var category = [];
            for (var i = 0; i < data.length; i++) {
                category[i] = data[i].name;
            }
            console.log(data);
            var bdata = [];
            var sdata = [];
            var scatteravg = data[0].scatteravg;
            var scatterbest = data[0].scatterbest;
            var selectionName = data[0].mainSelectionName;
            for (var i = 0; i < data.length; i++) {
                bdata[i] = data[i].bvalue;
                sdata[i] = data[i].svalue;
            }
            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    events: {
                        load: function () {
                            $rootScope.chartloadcount = $rootScope.chartloadcount + 1
                            console.log($rootScope.chartloadcount)
                            if ($rootScope.chartloadcount > 21) {

                                $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                            }

                            if ($rootScope.chartloadcount > 23) {
                                $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                            }
                        }
                    },
                    type: type,
                    marginLeft: margin,
                    marginTop: 25
                },
                colors: color,
                title: {
                    text: null
                },
                xAxis: {
                    categories: category,
//                    lineColor: 'transparent',
//                    tickLength: 0,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0,
                    minPadding: 0.5,
                    maxPadding: 0.5,
                    title: {
                        text: null
                    },
                    "labels": {
                        "overflow": "justify",
                        style: {
                            //   font:'14px bold Vardana'
                            color: '#556977',
                            fontSize: fontsize,
                        },
                        step: 1
                    },
                    legend: {
                        enabled: false
                    },
                },
                yAxis: {
                    gridLineWidth: 0,
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: null
                    },
                },
                plotOptions: {
                    series: {
                        pointWidth: weight,
                    },
                    scatter: {
                        dataLabels: {
                            enabled: false,
                            align: 'left',
                            x: 5,
                            y: 12,
                            format: '{y}%',
                            marker: {
                                radius: 8,
                                fillColor: 'blue',
                            },
                            style: {
                                fontWeight: 'normal'
                            }
                        }, tooltip: {
                            pointFormat: '<b> </b><br> {point.y}%'
                        }
                    },
                    bar: {
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                        },
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            inside: true,
                            align: 'right',
                            tooltip: {
                                formatter: function () {
                                    var s =
                                            this.point.options.bartitle;
                                    return;
                                },
                            },
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '10px',
                            }

                        },
                    },
                    column: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#cdcdcd',
                            inside: true
                        },
                        groupPadding: spacing,
                        colorByPoint: colorpoint

                    }
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: true,
                    itemStyle: {
                        fontSize: '10px',
                        //color: '#A0A0A0'
                    },
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'

                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChartLocal();
                                    }
                                }]
                        }
                    }
                },
                series: [{
                        data: data,
                        name: selectionName

                    },
                    {
                        type: 'scatter',
                        color: '#689F38',
                        data: bdata,
                        name: scatterbest,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    },
                    {
                        type: 'scatter',
                        color: '#9D78DD',
                        data: sdata,
                        name: scatteravg,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    }]
            }
            ;
            return tempbar;
        }



//bar and line graph
        function barline(color, data, namer, suffix) {
            var bestdata = [];
            var linedata = [];
            var bestname = data[0].bestname;
            var avgname = data[0].linename;
            var mainSelection = data[0].mainSelectionName;
            var categories = [];
            for (var i = 0; i < data.length; i++) {
                categories[i] = data[i].name;
            }
            for (var i = 0; i < data.length; i++) {
                bestdata[i] = data[i].bestvalue;
                linedata[i] = data[i].linescore;
            }

            var tempbline = {
                lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    events: {
                        load: function () {

                            $rootScope.chartloadcount = $rootScope.chartloadcount + 1;
                            console.log($rootScope.chartloadcount);
                            if ($rootScope.chartloadcount > 21) {

                                $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                            }

                            if ($rootScope.chartloadcount > 23) {
                                $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                            }
                        }
                    },
                    zoomType: 'xy',
                    marginTop: 20
                },
                colors: color,
                title: {
                    text: null
                },
                xAxis: [{
                        categories: categories,
                        gridLineWidth: 0,
                        lineColor: 'transparent',
                        tickLength: 0,
                        minorGridLineWidth: 0,
                        labels: {
                            enabled: true
                        }
                    }],
                yAxis: [{// Primary yAxis

                        gridLineWidth: 0,
                        lineColor: 'transparent',
                        tickLength: 0,
                        minorGridLineWidth: 0,
                        title: {
                            text: null

                        },
                        labels: {
                            enabled: false
                        }
                    }, {// Secondary yAxis
                        title: {
                            text: null

                        },
                        labels: {
                            enabled: false
                        },
                        gridLineWidth: 0,
                        minorGridLineWidth: 0,
                        opposite: true
                    }],
                gridLineWidth: 0,
                minorGridLineWidth: 0,
//                tooltip: {
//                    shared: false
//                },

                legend: {
                    enabled: true,
                    itemStyle: {
                        fontSize: '11.5px'
                                //color: '#A0A0A0'
                    },
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'

                },
                plotOptions: {
                    series: {
                        maxPointWidth: 50
//                 cursor: 'pointer',
//                        point: {
//                            events: {
//                                click: function () {
//                                    alert('Category: ' + this.category + ', value: ' + this.y);
//                                }
//                            }
//                        }
                    },
                    scatter: {
                        dataLabels: {
                            enabled: false,
                            align: 'left',
                            allowOverlap: true,
                            x: 5,
                            y: 12,
                            format: '{y}%',
                            marker: {
                                radius: 8,
                                fillColor: 'blue'
                            },
                            style: {
                                fontWeight: 'normal'
                            }
                        }, tooltip: {
                            pointFormat: '<b> </b><br> {point.y}%'
                        }
                    },
                    spline: {
                        dataLabels: {
                            enabled: false,
                            align: 'left',
                            x: 5,
                            y: 12,
                            marker: {
                                radius: 8,
                                fillColor: 'blue'
                            },
                            style: {
                                fontWeight: 'normal'
                            }
                        }, tooltip: {
                            pointFormat: '<b> </b><br> {point.y}%'
                        }
                    },
                    column: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            y: 30,
                            align: 'center',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }
                        },
                        depth: 25
//                colorByPoint: colorpoint
                    }
                },
                credits: {
                    enabled: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChartLocal();
                                    }
                                }]
                        }
                    }
                },
                series:
                        [{type: 'column',
                                data: data,
                                name: mainSelection

                            },
                            {
                                type: 'scatter',
                                color: '#689F38',
                                data: bestdata,
                                name: bestname,
                                marker: {
                                    symbol: 'diamond',
                                    radius: 5
                                }
                            },
                            {
                                type: 'spline',
                                data: linedata,
                                name: avgname,
                                color: '#7E57C2',
                                marker: {
                                    symbol: 'diamond',
                                    radius: 5
                                }
                            }]

            };
            return tempbline;
        }

        function kpiperforamnce(type, color, data, seriesname, margin, weight, spacing, pointdata, markersize, colorpoint, suffix, labelenable, xaxistitle, footter) {

            var sdata = data.data[2].data;
            var sidehead;
            var alignment;
            if ($rootScope.language == "EN")
            {

                sidehead = "Impact";
                alignment = -22;
            } else
            {
                sidehead = "Total KPI影響分數";
                alignment = -0;
            }


            var sadata = [];
            var wdata = data.data[3].data;
            var scattername = [];
            var minAndMax = [];
            var series1 = data.data[0].data;
            var series2 = data.data[1].data;
            if (labelenable == undefined) {
                labelenable = true;
            }
            var bestname = data.data[2].name;
            var worstname = data.data[3].name;
            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {events: {
                        load: function () {
                            $rootScope.chartloadcount = $rootScope.chartloadcount + 1
                            console.log($rootScope.chartloadcount)
                            if ($rootScope.chartloadcount > 21) {

                                $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                            }

                            if ($rootScope.chartloadcount > 23) {
                                $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                            }
                        }
                    },
                    type: 'bar',
                    marginRight: 35,
                    marginLeft: 360,
                    marginTop: 22,
                },
                title: {
                    text: ''
                },
                credits: {
                    enabled: true,
                    text: labelenable,
                    style: {
                        color: '#000000',
                        textShadow: 'none',
                        fontSize: '11px'
                    }
                }, exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.series[0].visible = false;
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.series[0].visible = false;
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChart();
                                    }
                                }]
                        }
                    }
                },
                subtitle: {
                    text: '<b>' + sidehead + '</b>',
                    style: {
                        textDecoration: 'underline',
                        fontSize: '9px'
                    },
                    align: 'right',
                    x: alignment
                },
                colors: ['white', '#3f6cc6'],
                xAxis: {
                    categories: data.categories,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0,
                    minPadding: 0.5,
                    maxPadding: 0.5,
                    title: {
                        text: ''
                    },
                    "labels": {
                        lineHeight: "8",
                        enabled: true,
                        //                        "overflow": "justify",
                        style: {
                            fontSize: "11px",
                            textOverflow: 'none'
                                    //                            paddingBottom: "1px"
                        },
                        step: 1
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
                    min: 0,
                    max: 110,
                    gridLineWidth: 0,
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: null
                    }
                },
                navigation: {
                    buttonOptions: {
                        y: -20

                    }
                },
                legend: {
                    reversed: false
                },
                plotOptions: {
                    series: {
                        stacking: 'normal'
                    }, scatter: {
                        dataLabels: {
                            enabled: false,
                            align: 'left',
                            x: 5,
                            y: 12,
                            format: '{y}%',
                            marker: {
                                radius: 8,
                                fillColor: 'blue',
                            },
                            style: {
                                fontWeight: 'normal'
                            }
                        }, tooltip: {
                            pointFormat: '<b> </b><br> {point.y}%'
                        }
                    },
                    bar: {
                        stacking: 'percent',
                        allowOverlap: true,
                        pointWidth: 15,
                        tooltip: {
                            pointFormat: '<b> </b><br> {point.y} ' + "%"
                        },
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                return "<b>" + this.point.label + "<b>"

                            },
                            align: 'right',
                            style: {
                                color:
                                        {
                                            formatter: function () {
//                                                            if (this.point.y == 0){
//                                                                console.log("***********3");
//                                                            return  '#acadaf';
//                                                            }
//                                                            else{
//                                                                
//                                                                console.log("***********");
//                                                            return  '#fffff';
//                                                            }
//  
                                            }
                                        },
                                //                               
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '11px'
                            }
                        }
                    },
                },
                tooltip: {
                    formatter: function () {

                        if (this.series.name == "Imapctseries" || this.series.name == "  ") {
                            return false;
                        } else {
                            return this.point.bartitle;
                        }



                    },
                    crosshairs: false
                },
                series: [{
                        name: "  ",
                        data: series1
                    }, {
                        name: data.data[1].name,
                        data: series2
                    }, {
                        type: 'scatter',
                        color: '#689F38',
                        data: sdata,
                        name: data.data[2].name,
                        marker: {
                            symbol: 'diamond',
                            radius: 5
                        }
                    },
                    {
                        type: 'scatter',
                        color: '#9D78DD',
                        data: wdata,
                        name: data.data[3].name,
                        marker: {
                            symbol: 'diamond',
                            radius: 5
                        }
                    },
                ]
            };
            console.log(JSON.stringify(tempbar));
            return tempbar;
        }



//vertical line
        function verticalline(color, inverted, data, halign, valign, labelflag, ymin, ymax, yline, plotlineName) {
            var categories = [];
            for (var i = 0; i < data[0].data.length; i++) {
                categories[i] = data[0].data[i].name;
            }

//       if(data[1].data[0].y==null){
//           
//       }
//       console.log()
            var tempvline = {
                lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    events: {
                        load: function () {
                            $rootScope.chartloadcount = $rootScope.chartloadcount + 1;
                            console.log($rootScope.chartloadcount);
                            if ($rootScope.chartloadcount > 21) {

                                $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                            }
                            if ($rootScope.chartloadcount > 23) {
                                $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                            }
                        }
                    },
                    type: 'spline',
                    inverted: inverted

                },
                colors: color,
                title: {
                    text: null
                },
                credits: {
                    enabled: false
                },
//                
                legend: {
                    itemStyle: {
                        fontSize: '12px'
                    },
                    enabled: true,
                    layout: 'horizontal',
                    align: 'left',
                    verticalAlign: 'top',
                    y: 5
                },
                xAxis: {
                    categories: categories,
                    gridLineWidth: 0,
                    lineColor: '#c5c5c5',
                    tickLength: 0,
                    minorGridLineWidth: 0,
                    "labels": {
                        "overflow": "justified",
                        layout: 'vertical',
                        style: {fontSize: 13, textOverflow: 'none'},
                        step: 1
                    },
                },
                yAxis: {
                    gridLineWidth: 0,
                    minorGridLineWidth: 0,
                    lineWidth: yline,
                    tickWidth: yline,
                    min: ymin,
                    max: ymax,
                    plotLines: [{
                            color: '#4B0082',
                            width: 2,
                            value: 0,
                            label: {
                                text: plotlineName,
//                                y: -10
                            }
                        }],
                    title: {
                        text: null
                    },
                    labels: {
                        enabled: labelflag,
                        style: {
                            fontSize: '12px'
                        },
                        formatter: function () {
                            return this.value + '%';
                        }
                    }
                },
                tooltip: {
                    formatter: function () {
                        var s =
                                this.point.options.tooltip + "%";
                        return s;
                    },
                    crosshairs: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
//                                textKey: 'downloadXLS',
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
//                                textKey: 'downloadXLS',
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
//                                     textKey: 'downloadXLS',
                                    onclick: function () {
                                        this.exportChartLocal();
                                    }
                                }]
                        }
                    }
                },
                series: data

            };
            return tempvline;
        }
//dealersnapshotmean
        function barplainbarsplitDelightedsnap(type, color, data, seriesname, margin, weight, spacing, pointdata, markersize, colorpoint, suffix, labelenable, xaxistitle) {
            console.log(suffix);
            var sdata = data.data[2].data;
            var sadata = [];
            var wdata = data.data[3].data;
            var scattername = [];
            var minAndMax = [];
            var series1 = data.data[0].data;
            var series2 = data.data[1].data;
            if (labelenable == undefined) {
                labelenable = true;
            }

            var bestname = data.data[2].name;
            var worstname = data.data[3].name;
            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    type: type,
                    marginLeft: margin,
                    marginRight: 25

                },
                colors: color,
                title: {
                    text: '',
                    style: {
                        display: 'none'
                    }
                },
                subtitle: {
                    text: 'Mean',
                    align: 'right',
                    x: -30
                },
                xAxis: {
//                    max:1000,
                    categories: data.categories,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0,
                    minPadding: 0.5,
                    maxPadding: 0.5,
                    title: {
                        text: xaxistitle
                    },
                    "labels": {
                        enabled: labelenable,
//                        "overflow": "justify",
                        style: {fontSize: "10px",
                            textOverflow: 'none'
//                            paddingBottom: "1px"
                        },
                        step: 1
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
                    gridLineWidth: 0,
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: null
                    }
                },
                plotOptions: {
                    series: {
                    },
                    scatter: {
                        dataLabels: {
                            enabled: false,
                            align: 'left',
                            x: 5,
                            y: 12,
                            marker: {
                                radius: 8,
                                fillColor: 'blue'
                            },
                            style: {
                                fontWeight: 'normal'
                            }
                        }, tooltip: {
                            pointFormat: '<b> </b><br> {point.y} ' + suffix
                        }
                    },
                    bar: {
                        stacking: 'percent',
                        allowOverlap: true,
                        tooltip: {
                            pointFormat: '<b> </b><br> {point.y} ' + suffix
                        },
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                return this.point.label

                            },
                            align: 'right',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            },
                        }
                    },
                    column: {
                        tooltip: {
                            pointFormat: '<b> </b><br> {point.y} ' + suffix
                        },
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
//                            y: 30,
                            align: 'center',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }
                        },
                        pointWidth: weight,
                        groupPadding: spacing,
                        depth: 25,
                        colorByPoint: colorpoint


                    }
                },
                tooltip: {
                    formatter: function () {
                        if (this.series.name == "FirstSeries") {
                            return false;
                        } else if (this.series.name == "SecondSeries") {
                            return this.point.bartitle;
                        } else
                        {
                            return this.series.name + "<br><b>" + this.point.label + "</b>" + "%";
                        }


                    },
                    crosshairs: false
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChart();
                                    }
                                }]
                        }
                    }
                },
                series: [
                    {
                        data: series1,
                        name: data.data[0].name

                    },
                    {
                        data: series2,
                        name: data.data[1].name

                    },
                    {
                        type: 'scatter',
                        color: '#689F38',
                        data: sdata,
                        name: bestname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    },
                    {
                        type: 'scatter',
                        color: '#800000',
                        data: wdata,
                        name: worstname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    }
                ]
            }
            ;
            console.log(JSON.stringify(tempbar));
            return tempbar;
        }
        function barplainbarsplitDelighted(type, color, data, seriesname, margin, weight, spacing, pointdata, markersize, colorpoint, suffix, labelenable, xaxistitle) {
            console.log(suffix);
            var sdata = data.data[2].data;
            var sadata = [];
            var wdata = data.data[3].data;
            var scattername = [];
            var minAndMax = [];
            var series1 = data.data[0].data;
            var series2 = data.data[1].data;
            if (labelenable == undefined) {
                labelenable = true;
            }

            var bestname = data.data[2].name;
            var worstname = data.data[3].name;
            var tempbar = {lang: {
                    downloadPNG: $rootScope.downloadpng,
                    downloadCSV: $rootScope.downloadcsv,
                    downloadXLS: $rootScope.downloadxls,
                    contextButtonTitle: $rootScope.downloadheader
                },
                chart: {
                    type: type,
                    marginLeft: margin,
                    marginRight: 25

                },
                colors: color,
                title: {
                    text: '',
                    style: {
                        display: 'none'
                    }
                },
                subtitle: {
                    text: 'Mean',
                    align: 'right',
                    x: -30
                },
                xAxis: {
//                    max:1000,
                    categories: data.categories,
                    lineWidth: 0,
                    minorGridLineWidth: 0,
                    lineColor: 'transparent',
                    minorTickLength: 0,
                    tickLength: 0,
                    minPadding: 0.5,
                    maxPadding: 0.5,
                    title: {
                        text: xaxistitle
                    },
                    "labels": {
                        enabled: labelenable,
//                        "overflow": "justify",
                        style: {fontSize: "10px",
                            textOverflow: 'none'
//                            paddingBottom: "1px"
                        },
                        step: 1
                    },
                    legend: {
                        enabled: false
                    }
                },
                yAxis: {
                    gridLineWidth: 0,
                    labels: {
                        enabled: false
                    },
                    title: {
                        text: null
                    }
                },
                plotOptions: {
                    series: {
                    },
                    scatter: {
                        dataLabels: {
                            enabled: false,
                            align: 'left',
                            x: 5,
                            y: 12,
                            marker: {
                                radius: 8,
                                fillColor: 'blue'
                            },
                            style: {
                                fontWeight: 'normal'
                            }
                        }, tooltip: {
                            pointFormat: '<b> </b><br> {point.y} ' + suffix
                        }
                    },
                    bar: {
                        stacking: 'percent',
                        allowOverlap: true,
                        tooltip: {
                            pointFormat: '<b> </b><br> {point.y} ' + suffix
                        },
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                return this.point.label

                            },
                            align: 'right',
                            style: {
                                color: '#000000',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            },
                        }
                    },
                    column: {
                        tooltip: {
                            pointFormat: '<b> </b><br> {point.y} ' + suffix
                        },
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#000000',
//                            y: 30,
                            align: 'center',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px'
                            }
                        },
                        pointWidth: weight,
                        groupPadding: spacing,
                        depth: 25,
                        colorByPoint: colorpoint


                    }
                },
                tooltip: {
                    formatter: function () {
                        if (this.series.name == "FirstSeries") {
                            return false;
                        } else if (this.series.name == "SecondSeries") {
                            return this.point.bartitle;
                        } else
                        {
                            return this.series.name + '<b> </b><br>' + this.y + "%";
                        }



                    },
                    crosshairs: false
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChart();
                                    }
                                }]
                        }
                    }
                },
                series: [
                    {
                        data: series1,
                        name: data.data[0].name

                    },
                    {
                        data: series2,
                        name: data.data[1].name

                    },
                    {
                        type: 'scatter',
                        color: '#689F38',
                        data: sdata,
                        name: bestname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    },
                    {
                        type: 'scatter',
                        color: '#800000',
                        data: wdata,
                        name: worstname,
                        marker: {
                            symbol: 'diamond',
                            radius: markersize
                        }
                    },
                ]
            }
            ;
            console.log(JSON.stringify(tempbar));
            return tempbar;
        }



//vertical line
        function horizontalline(response, color) {
            var categories = [];
            for (var i = 0; i < response[0].data.length; i++) {
                categories[i] = response[0].data[i].name;
            }

            var maxaray = [];
            var minarray = [];
            for (var i = 0; i < response.length; i++) {
                maxaray.push(Math.max.apply(Math, response[i].data.map(function (o) {
                    return o.y;
                })));
                minarray.push(Math.min.apply(Math, response[i].data.map(function (o) {
                    return o.y;
                })))

            }
            var min = Math.min.apply(Math, minarray);
            var max = Math.max.apply(Math, maxaray);
            var line =
                    {lang: {
                            downloadPNG: $rootScope.downloadpng,
                            downloadCSV: $rootScope.downloadcsv,
                            downloadXLS: $rootScope.downloadxls,
                            contextButtonTitle: $rootScope.downloadheader
                        },
                        chart: {
                            events: {
                                load: function () {
                                    $rootScope.chartloadcount = $rootScope.chartloadcount + 1;
                                    console.log($rootScope.chartloadcount)
                                    if ($rootScope.chartloadcount > 21) {

                                        $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                                    }

                                    if ($rootScope.chartloadcount > 23) {
                                        $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                                    }
                                }
                            },
                            type: 'spline',
//                animation: Highcharts.svg, // don't animate in old IE

                            marginRight: 25, backgroundColor: '#FFFFFF'
                        },
                        colors: ['#FFC000', '#808080', '#A9D08E', '#305496', '#ff4100', '#002060', '#92D050', '#000000'],
                        title: {
                            text: ''
                        },
                        xAxis: {
                            categories: categories,
                            gridLineWidth: 0,
//                    lineColor: 'transparent',
                            tickLength: 0,
                            minorGridLineWidth: 0,
                            labels: {
                                style: {fontSize: '2vmin',
                                    lineHeight: 25
                                }
                            }
                        },
                        yAxis: {
                            min: min,
                            max: max,
                            gridLineWidth: 1,
                            lineColor: '#eeeeee',
                            lineWidth: 1,
                            tickLength: 1,
                            minorGridLineWidth: 1,
                            title: {
                                text: ''
                            },
                            labels: {enabled: true,
                                overflow: 'justify',
                                format: '{value:.2f}'
                            }
                        },
                        tooltip: {
                            style: {fontSize: '2vmin'},
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                            crosshairs: false
                        },
                        legend: {
                            enabled: true,
                            itemStyle: {
                                fontSize: '12px'
                                        //color: '#A0A0A0'
                            },
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'

                        },
                        exporting: {
                            formAttributes: {
                                target: '_blank'
                            },
                            buttons: {
                                contextButton: {
                                    menuItems: [{
                                            text: $rootScope.downloadxls,
                                            onclick: function () {
                                                this.downloadXLS();
                                            }
                                        }, {
                                            text: $rootScope.downloadcsv,
                                            onclick: function () {
                                                this.downloadCSV();
                                            }
                                        }, {
                                            text: $rootScope.downloadpng,
                                            onclick: function () {
                                                this.exportChartLocal();
                                            }
                                        }]
                                }
                            }
                        },
                        series: response,
                        "credits": {
                            "enabled": false
                        }
                        ,
                        navigation: {
                            buttonOptions: {
                                verticalAlign: 'top',
                                align: 'right',
                                y: -15
                            }
                        }
                    };
            return line;
        }



        function dsmultigraph(response, colorbypoint, color) {

            var categories = [];
            console.log(response);
            console.log(response.name);
            console.log(response.data);
            console.log(response[0].name);
            console.log(response[0].data);
//           console.log(response.data);
//           console.log(response[0].name);
//           console.log(response[0].data);
            for (var i = 0; i < response[0].data.length; i++) {
                categories[i] = response[0].data[i].name;
            }

            var multi_return =
                    {lang: {
                            downloadPNG: $rootScope.downloadpng,
                            downloadCSV: $rootScope.downloadcsv,
                            downloadXLS: $rootScope.downloadxls,
                            contextButtonTitle: $rootScope.downloadheader
                        },
                        "chart": {
                            "type": "column"
                        },
                        colors: color,
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                            crosshairs: false
                        },
                        plotOptions: {
                            series: {
                                borderWidth: 0,
                                maxPointWidth: 40,
//                             cursor: 'pointer',
//                            point: {
//                                events: {
//                                    click: function () {
//                                        alert('Category: ' + this.category + ', value: ' + this.y);
//                                    }
//                                }
//                            },

                                dataLabels: {
                                    enabled: true,
                                    formatter: function () {
                                        var pcnt = this.point.options.bartitle;
                                        return pcnt;
                                    },
                                    color: '#ffffff',
                                    y: 30,
                                    align: 'right',
                                    style: {
                                        //color: '#FFFFFF',
                                        textShadow: 'none',
                                        fontWeight: 'light',
                                        fontSize: '11px'
                                    }
                                }
                            }
                        },
                        "series": response[0].data,
                        "title": {
                            "text": null
                        },
                        "credits": {
                            "enabled": false
                        },
                        "loading": false,
                        "size": {},
                        "xAxis": {
                            gridLineWidth: 0,
                            categories: categories,
                            currentMin: 0,
//                        lineColor: 'transparent',
//                        tickLength: 0,
                            minorGridLineWidth: 0,
                            labels: {
                                style: {
                                    fontSize: '10px'
                                }
                            }

                        },
                        "yAxis": {
                            title: {
                                text: null,
                                align: 'high'
                            },
                            labels: {
                                enabled: false
                            },
                            gridLineWidth: 0,
                            allowDecimals: true,
                            min: 0

                        },
                        exporting: {
                            formAttributes: {
                                target: '_blank'
                            },
                            buttons: {
                                contextButton: {
                                    menuItems: [{
                                            text: $rootScope.downloadxls,
                                            onclick: function () {
                                                this.downloadXLS();
                                            }
                                        }, {
                                            text: $rootScope.downloadcsv,
                                            onclick: function () {
                                                this.downloadCSV();
                                            }
                                        }, {
                                            text: $rootScope.downloadpng,
                                            onclick: function () {
                                                this.exportChartLocal();
                                            }
                                        }]
                                }
                            }
                        },
                        legend: {
                            enabled: true

                        }
                    };
            return multi_return;
        }

//" City Level retention for Total Sales", null, response, 250, 1150,false
//the multigraph
//function multigraph(response,colorpoint,color) {
        function multigraph(response, colorbypoint, color) {

            var categories = [];
            console.log(response);
//           console.log(response.data);
//           console.log(response[0].name);
//           console.log(response[0].data);
            for (var i = 0; i < response.data[0].data.length; i++) {
                categories[i] = response.data[0].data[i].name;
//
            }

            var multi_return =
                    {lang: {
                            downloadPNG: $rootScope.downloadpng,
                            downloadCSV: $rootScope.downloadcsv,
                            downloadXLS: $rootScope.downloadxls,
                            contextButtonTitle: $rootScope.downloadheader
                        },
                        "chart": {
                            "type": "column"
                        },
                        colors: color,
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                            crosshairs: false
                        },
                        plotOptions: {
                            series: {
                                borderWidth: 0,
                                maxPointWidth: 40,
//                             cursor: 'pointer',
//                            point: {
//                                events: {
//                                    click: function () {
//                                        alert('Category: ' + this.category + ', value: ' + this.y);
//                                    }
//                                }
//                            },

                                dataLabels: {
                                    enabled: true,
                                    formatter: function () {
                                        var pcnt = this.point.options.bartitle;
                                        return pcnt;
                                    },
                                    color: '#ffffff',
                                    y: 30,
                                    align: 'right',
                                    style: {
                                        //color: '#FFFFFF',
                                        textShadow: 'none',
                                        fontWeight: 'light',
                                        fontSize: '11px'
                                    }
                                }
                            }
                        },
                        "series": response.data,
                        "title": {
                            "text": null
                        },
                        "credits": {
                            "enabled": false
                        },
                        "loading": false,
                        "size": {},
                        "xAxis": {
                            gridLineWidth: 0,
                            categories: categories,
                            currentMin: 0,
//                        lineColor: 'transparent',
//                        tickLength: 0,
                            minorGridLineWidth: 0,
                            labels: {
                                style: {
                                    fontSize: '10px'
                                }
                            }

                        },
                        "yAxis": {
                            title: {
                                text: null,
                                align: 'high'
                            },
                            labels: {
                                enabled: false
                            },
                            gridLineWidth: 0,
                            allowDecimals: true,
                            min: 0

                        },
                        exporting: {
                            formAttributes: {
                                target: '_blank'
                            },
                            buttons: {
                                contextButton: {
                                    menuItems: [{
                                            text: $rootScope.downloadxls,
                                            onclick: function () {
                                                this.downloadXLS();
                                            }
                                        }, {
                                            text: $rootScope.downloadcsv,
                                            onclick: function () {
                                                this.downloadCSV();
                                            }
                                        }, {
                                            text: $rootScope.downloadpng,
                                            onclick: function () {
                                                this.exportChartLocal();
                                            }
                                        }]
                                }
                            }
                        },
                        legend: {
                            enabled: true

                        }
                    };
            return multi_return;
        }


        function stackmultigraph(response, color, pointwidth, fontsize, grouppadding, title, footnoter) {
            var category = [];
            for (var i = 0; i < response[0].data.length; i++) {
                console.log("inisde here is the fucniton");
                category[i] = response[0].data[i].name;
            }
            console.log("othersssssssss");
            console.log(response);
            var multi_return =
                    {lang: {
                            downloadPNG: $rootScope.downloadpng,
                            downloadCSV: $rootScope.downloadcsv,
                            downloadXLS: $rootScope.downloadxls,
                            contextButtonTitle: $rootScope.downloadheader
                        },
                        "chart": {
                            "type": "column"
                        },
                        title: {
                            text: title,
                            align: 'left',
                            style: {
                                textShadow: 'none',
//                            fontWeight: '400',
                                fontSize: '12px'

                            }
                        },
                        colors: color,
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                            crosshairs: false
                        },
                        plotOptions: {
                            series: {
                                pointWidth: pointwidth,
                                groupPadding: grouppadding,
                                minPointLength: 0,
                                dataLabels: {
                                    enabled: true,
                                    allowOverlap: true,
                                    formatter: function () {
                                        var pcnt = this.point.options.bartitle;
                                        return pcnt;
                                    },
                                    color: '#000000',
//                                    y: -2,
//                                    verticalAlign: 'top',
//                                            align: 'right',
                                    style: {
                                        textShadow: 'none',
                                        fontSize: fontsize
                                    }
                                }
                            }
                        },
                        "series": response,
                        "credits": {
                            "enabled": true,
                            text: footnoter,
                            style: {
                                color: '#000000',
                                textShadow: 'none',
                                fontSize: '10px',
                                width: 700
                            }

                        },
                        "loading": false,
                        "size": {},
                        "xAxis": {
                            gridLineWidth: 0,
                            categories: category,
                            currentMin: 0,
                            tickLength: 0,
                            lineColor: 'transparent',
                            minorGridLineWidth: 0,
                            labels: {
                                style: {
                                    //   font:'14px bold Vardana'
                                    color: '#556977',
                                    fontSize: '11px'
                                            // fontFamily: 'Roboto',
                                            //  fontWeight: 'bold'
                                }
                            }

                        },
                        "yAxis": {
                            title: {
                                text: null,
                                align: 'high'
                            },
                            labels: {
                                enabled: false
                            },
                            gridLineWidth: 0,
                            allowDecimals: true,
                            min: 0,
                            max: 108

                        },
                        exporting: {
                            formAttributes: {
                                target: '_blank'
                            },
                            buttons: {
                                contextButton: {
                                    menuItems: [{
                                            text: $rootScope.downloadxls,
                                            onclick: function () {
                                                this.downloadXLS();
                                            }
                                        }, {
                                            text: $rootScope.downloadcsv,
                                            onclick: function () {
                                                this.downloadCSV();
                                            }
                                        }, {
                                            text: $rootScope.downloadpng,
                                            onclick: function () {
                                                this.exportChartLocal();
                                            }
                                        }]
                                }
                            }
                        },
                        legend: {
                            enabled: true,
                            itemStyle: {
                                fontSize: '11px'
                                        //color: '#A0A0A0'
                            },
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'

                        }
                    };
            return multi_return;
        }

        function othersbarchart(type, color, data, margin, weight, spacing, seriesname, fontsize, stepsize, maxlength, title) {
            var category = [];
            for (var i = 0; i < data.length; i++) {
                console.log("inisde here is the fucniton");
                category[i] = data[i].name;
            }

            console.log(category);
            console.log(data);
            var tempbar = {
                chart: {
                    type: type,
                    marginLeft: margin,
//              marginTop: 20

                },
                title: {
                    text: title,
                    align: 'left',
                    style: {
                        textShadow: 'none',
//                            fontWeight: '400',
                        fontSize: '15px'

                    },
                },
                xAxis: {
                    categories: category,
                    title: {
                        text: null
                    },
                    "labels": {
                        "overflow": "justify",
                        style: {fontSize: fontsize},
                        step: stepsize
                    },
                    legend: {
                        enabled: false
                    },
                },
                yAxis: {
//                     max: maxlength,
                    min: 0,
                    title: {
                        text: null,
                    },
                    gridLineWidth: 0,
                    minorGridLineWidth: 0,
                    gridLineColor: 'transparent',
                    labels: {enabled: false,
                        overflow: 'justify'
                    }
                },
                plotOptions: {
                    series: {
//                        pointWidth: weight,
//                         groupPadding: spacing,
//                         cursor: 'pointer',
//                        point: {
//                            events: {
//                                click: function () {
//                                    alert('Category: ' + this.category + ', value: ' + this.y);
//                                }
//                            }
//                        }

                    },
                    bar: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            inside: true,
                            align: 'center',
                            verticalAlign: 'top',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px',
                            }

                        },
                    },
                    column: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                var pcnt = this.point.options.bartitle;
                                return pcnt;
                            },
                            color: '#ffffff',
                            y: 30,
                            align: 'center',
                            verticalAlign: 'top',
                            style: {
                                //color: '#FFFFFF',
                                textShadow: 'none',
                                fontWeight: 'light',
                                fontSize: '14px',
                            }


                        },
                        depth: 25
                    }
                },
                credits: {
                    enabled: false
                },
//         size: {height:height,
//                width:width},

                legend: {
                    enabled: false
                },
                tooltip: {
                    formatter: function () {
                        var s =
                                this.point.options.tooltip;
                        return s;
                    },
                    crosshairs: false
                },
                exporting: {
                    formAttributes: {
                        target: '_blank'
                    },
                    buttons: {
                        contextButton: {
                            menuItems: [{
                                    text: $rootScope.downloadxls,
                                    onclick: function () {
                                        this.downloadXLS();
                                    }
                                }, {
                                    text: $rootScope.downloadcsv,
                                    onclick: function () {
                                        this.downloadCSV();
                                    }
                                }, {
                                    text: $rootScope.downloadpng,
                                    onclick: function () {
                                        this.exportChartLocal();
                                    }
                                }]
                        }
                    }
                },
                series: [{
                        color: color,
                        data: data,
                        name: seriesname
                    }],
                navigation: {
                    buttonOptions: {
                        verticalAlign: 'top',
                        y: -10
                    }
                }
            }
            ;
            return tempbar;
        }


        function stackedgraph(data, color, marginBottom, title) {

            var category = [];
            for (var i = 0; i < data[0].data.length; i++) {

                category[i] = data[0].data[i].name;
            }
            console.log("stackdataaaaaaa");
            console.log(data);
            var stack =
                    {lang: {
                            downloadPNG: $rootScope.downloadpng,
                            downloadCSV: $rootScope.downloadcsv,
                            downloadXLS: $rootScope.downloadxls,
                            contextButtonTitle: $rootScope.downloadheader
                        },
                        chart: {events: {
                                load: function () {
                                    $rootScope.chartloadcount = $rootScope.chartloadcount + 1;
                                    console.log($rootScope.chartloadcount);
                                    if ($rootScope.chartloadcount > 21) {

                                        $rootScope.$broadcast("dealerenabledownload", $rootScope.chartloadcount);
                                    }

                                    if ($rootScope.chartloadcount > 23) {
                                        $rootScope.$broadcast("enabledownload", $rootScope.chartloadcount);
                                    }
                                }
                            },
                            type: 'column',
                            marginRight: 25,
                            marginBottom: marginBottom

                        },
                        title: {
                            text: title,
                            align: 'left',
                            style: {
                                textShadow: 'none',
//                            fontWeight: '400',
                                fontSize: '12px'

                            }
                        },
                        colors: color,
                        xAxis: {
                            categories: category,
                            gridLineWidth: 0,
                            lineColor: 'transparent',
                            tickLength: 0,
                            minorGridLineWidth: 0,
                            labels: {
                                "overflow": "justify",
                                style: {fontSize: "11px"},
                                step: 1
                            }
                        },
                        yAxis: {
                            min: 0,
                            max: 100,
                            title: {
                                text: null
                            },
                            gridLineWidth: 0,
                            minorGridLineWidth: 0,
                            gridLineColor: 'transparent',
                            labels: {enabled: false,
                                overflow: 'justify'
                            }
                        },
                        legend: {
                            enabled: true,
//            floating: true,
                            itemStyle: {
                                fontSize: '10px'

                            },
                            layout: 'horizontal',
                            verticalAlign: 'bottom',
                            align: 'center',
                            y: 10
                        },
                        plotOptions: {
                            series: {
                                dataLabels: {
                                    enabled: true,
                                    allowOverlap: true,
                                    formatter: function () {
                                        var pcnt = this.point.options.bartitle;
                                        return pcnt;
                                    },
                                    style: {
                                        textShadow: 'none',
                                        fontWeight: 'light',
                                        fontSize: '11px'
                                    },
                                    color: '#FFFFFF',
                                    inside: true

                                },
                                stacking: 'percent'
                            }

                        },
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                            crosshairs: false
                        },
                        exporting: {
                            formAttributes: {
                                target: '_blank'
                            },
                            buttons: {
                                contextButton: {
                                    menuItems: [{
                                            text: $rootScope.downloadxls,
                                            onclick: function () {
                                                this.downloadXLS();
                                            }
                                        }, {
                                            text: $rootScope.downloadcsv,
                                            onclick: function () {
                                                this.downloadCSV();
                                            }
                                        }, {
                                            text: $rootScope.downloadpng,
                                            onclick: function () {
                                                this.exportChartLocal();
                                            }
                                        }]
                                }
                            }
                        },
                        series: data,
                        "credits": {
                            "enabled": false
                        },
                        navigation: {
                            buttonOptions: {
                                verticalAlign: 'top',
                                align: 'right',
                                y: 0
                            }
                        }


                    };
            return stack;
        }




        function dlrstackedgraph(data, color, marginBottom) {

            var category = [];
            for (var i = 0; i < data[0].data[i].length; i++) {
                category[i] = data[0].data[i].name;
            }


            var stack =
                    {lang: {
                            downloadPNG: $rootScope.downloadpng,
                            downloadCSV: $rootScope.downloadcsv,
                            downloadXLS: $rootScope.downloadxls,
                            contextButtonTitle: $rootScope.downloadheader
                        },
                        chart: {
                            type: 'column',
                            marginRight: 25,
                            marginBottom: marginBottom

                        },
                        title: {
                            text: null
                        },
                        colors: color,
                        xAxis: {
                            categories: category,
                            gridLineWidth: 0,
                            lineColor: 'transparent',
                            tickLength: 0,
                            minorGridLineWidth: 0,
                            labels: {
                                "overflow": "justify",
                                style: {fontSize: "120px"},
                                step: 1
                            }
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: null
                            },
                            gridLineWidth: 0,
                            minorGridLineWidth: 0,
                            gridLineColor: 'transparent',
                            labels: {enabled: false,
                                overflow: 'justify'
                            }
                        },
                        legend: {
                            enabled: true,
//            floating: true,
                            itemStyle: {
                                fontSize: '10px'

                            },
                            layout: 'horizontal',
                            verticalAlign: 'bottom',
                            align: 'center',
                            y: 10
                        },
                        plotOptions: {
                            series: {
                                dataLabels: {
                                    enabled: true,
                                    allowOverlap: true,
                                    formatter: function () {
                                        var pcnt = this.point.options.bartitle;
                                        return pcnt;
                                    },
                                    style: {
                                        textShadow: 'none',
                                        fontWeight: 'light',
                                        fontSize: '11px'
                                    },
                                    color: '#FFFFFF',
                                    inside: true

                                },
                                stacking: 'percent'
                            }






                        },
                        tooltip: {
                            formatter: function () {
                                var s =
                                        this.point.options.tooltip;
                                return s;
                            },
                            crosshairs: false
                        },
                        exporting: {
                            formAttributes: {
                                target: '_blank'
                            },
                            buttons: {
                                contextButton: {
                                    menuItems: [{
                                            text: $rootScope.downloadxls,
                                            onclick: function () {
                                                this.downloadXLS();
                                            }
                                        }, {
                                            text: $rootScope.downloadcsv,
                                            onclick: function () {
                                                this.downloadCSV();
                                            }
                                        }, {
                                            text: $rootScope.downloadpng,
                                            onclick: function () {
                                                this.exportChartLocal();
                                            }
                                        }]
                                }
                            }
                        },
                        series: data,
                        "credits": {
                            "enabled": false
                        },
                        navigation: {
                            buttonOptions: {
                                verticalAlign: 'top',
                                align: 'right',
                                y: -15
                            }
                        }
                    };
            return stack;
        }
        function destroyAllCharts(array) {
            console.log(array);
            if (array) {
                for (var k = 0; k < array.length; k++) {
                    if (array[k] && $("#" + array[k]).highcharts()) {
                        $("#" + array[k]).highcharts().destroy();
                        console.log("chart destroyed-->>", array[k]);
                    }
                }
            }
        }


    }
})();




