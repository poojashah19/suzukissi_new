(function () {
    'use strict';

    angular
            .module('menu')
            .controller('MenuDynamicController', MenuDynamicController);

    /* @ngInject */
    function MenuDynamicController(dynamicMenuService, triMenu, $rootScope, $state,$filter, $scope) {


        var vm = this;
        // get dynamic menu service to store & keep track the state of the menu status
        vm.dynamicMenu = dynamicMenuService.dynamicMenu;
        // create toggle function
        
        $rootScope.language = 'EN';
         var $translate = $filter('translate');
        var summary = "";
        var regional = "";
        var trend = "";
        var priority = "";
        var kpiperf = "";
        var kpitrend = "";
        var others = "";
        var dscore = "";
        var dsnap = "";
        var model = "";
        var loyal = "";
        var red = "";
        var ofeed = "";
        var individ = "";
         
         
         $scope.menutitles = function () {
             
            
            summary = $translate('menusummary');
            regional = $translate('menureggional');
            trend = $translate('menutrend');
            priority = $translate('menupriority');
           kpiperf = $translate('menukpiperform');
            kpitrend = $translate('menukpitrend');
            others = $translate('menuothers');
            dscore = $translate('menudealeranking');
            dsnap = $translate('menudealersnapshot');
            model = $translate('menumodelanalysis');
            loyal = $translate('menuloyality');
            red = $translate('menuredalert');
            ofeed = $translate('menuotherfeedback');
            individ = $translate('menupdf');





         }
        
       
        
         vm.toggleExtraMenu = toggleExtraMenu;
        vm.tempfunction = tempfunction;

        tempfunction($rootScope.reporttype);


        $scope.$on('changeLanguage', function (event, data) {
            
            tempfunction($rootScope.reporttype);

        });



        function toggleExtraMenu(showMenu) {
            
         
            
                triMenu.removeMenu('triangular.csisummary');
                triMenu.removeMenu('triangular.csiregional');
                triMenu.removeMenu('triangular.csitrend');
                triMenu.removeMenu('triangular.csipriority');
              triMenu.removeMenu('triangular.csikpiperformance');
                triMenu.removeMenu('triangular.csikpitrend');
                triMenu.removeMenu('triangular.csiothers');
                triMenu.removeMenu('triangular.csidealerranking');
                triMenu.removeMenu('triangular.csidealersnapshot');
                triMenu.removeMenu('triangular.csiproduct');
                triMenu.removeMenu('triangular.csiloyality');
                triMenu.removeMenu('triangular.csipage12');
                triMenu.removeMenu('triangular.csipage13');
                triMenu.removeMenu('triangular.csipage14');
                triMenu.removeMenu('triangular.csipage15');
                 triMenu.removeMenu('triangular.dealerkpiperformance');
                 triMenu.removeMenu('triangular.dealerkpitrend');
              //  triMenu.removeMenu('triangular.dealercsipage10');
                triMenu.removeMenu('triangular.dealercsipage1');
                triMenu.removeMenu('triangular.dealercsipage2');
                triMenu.removeMenu('triangular.dealercsipage3');
                triMenu.removeMenu('triangular.dealercsipage4');
                triMenu.removeMenu('triangular.dealercsipage5');
                triMenu.removeMenu('triangular.dealercsipage7');
                triMenu.removeMenu('triangular.dealercsipage8');
              //  triMenu.removeMenu('triangular.dealercsipage9');
                triMenu.removeMenu('triangular.dealercsipage14');
                triMenu.removeMenu('triangular.dealercsipage15');
              //  triMenu.removeMenu('triangular.dealergroupcsipage10');
                triMenu.removeMenu('triangular.dealergroupcsipage1');
                triMenu.removeMenu('triangular.dealergroupcsipage2');
                triMenu.removeMenu('triangular.dealergroupcsipage3');
                triMenu.removeMenu('triangular.dealergroupcsipage4');
                triMenu.removeMenu('triangular.dealergroupcsipage5');
                triMenu.removeMenu('triangular.dealergroupcsipage7');
                triMenu.removeMenu('triangular.dealergroupcsipage8');
               // triMenu.removeMenu('triangular.dealergroupcsipage9');
                triMenu.removeMenu('triangular.dealergroupcsipage14');
                triMenu.removeMenu('triangular.dealergroupcsipage15');
                 triMenu.removeMenu('triangular.dgkpiperformance');
                  triMenu.removeMenu('triangular.dgkpitrend');
            if (showMenu == 'oem') {

                triMenu.addMenu({
                    name: summary,
                    icon: 'zmdi zmdi-file-text',
                    type: 'link',
                    priority: 1.0,
                    state: 'triangular.csisummary'
                });
                triMenu.addMenu({
                    name: regional,
                    icon: 'zmdi zmdi-map',
                    type: 'link',
                    priority: 1.1,
                    state: 'triangular.csiregional'
                });
                triMenu.addMenu({
                    name: trend,
                    icon: 'zmdi zmdi-menu',
                    type: 'link',
                    priority: 1.2,
                    state: 'triangular.csitrend'
                });
                triMenu.addMenu({
                    name: priority,
                    icon: 'zmdi zmdi-badge-check',
                    type: 'link',
                    priority: 1.3,
                    state: 'triangular.csipriority'
                });

              triMenu.addMenu({
                    name: kpiperf,
                    icon: 'zmdi zmdi-collection-text',
                    type: 'link',
                    priority: 1.4,
                    state: 'triangular.csikpiperformance'
                });
                triMenu.addMenu({
                    name: kpitrend,
                    icon: 'zmdi zmdi-hourglass',
                    type: 'link',
                    priority: 1.5,
                    state: 'triangular.csikpitrend'
                });
                triMenu.addMenu({
                    name: others,
                    icon: 'zmdi zmdi-assignment-o',
                    type: 'link',
                    priority: 1.6,
                    state: 'triangular.csiothers'
                });
                triMenu.addMenu({
                    name: dscore,
                    icon: 'zmdi zmdi-view-list-alt',
                    type: 'link',
                    priority: 1.7,
                    state: 'triangular.csidealerranking'
                });
                triMenu.addMenu({
                    name: dsnap,
                    icon: 'zmdi zmdi-picture-in-picture',
                    type: 'link',
                    priority: 1.8,
                    state: 'triangular.csidealersnapshot'
                });
                triMenu.addMenu({
                    name: model,
                    icon: 'zmdi zmdi-car-taxi',
                    type: 'link',
                    priority: 1.9,
                    state: 'triangular.csiproduct'
                });
                triMenu.addMenu({
                    name: loyal,
                    icon: 'zmdi zmdi-favorite',
                    type: 'link',
                    priority: 2.0,
                    state: 'triangular.csiloyality'
                });



            } else if (showMenu == 'dealer') {



               
                triMenu.addMenu({
                    name: summary,
                    icon: 'zmdi zmdi-file-text',
                    type: 'link',
                    priority: 1.0,
                    state: 'triangular.dealercsipage4'
                });

                triMenu.addMenu({
                    name: trend,
                    icon: 'zmdi zmdi-menu',
                    type: 'link',
                    priority: 1.2,
                    state: 'triangular.dealercsipage7'
                });
                triMenu.addMenu({
                    name: priority,
                    icon: 'zmdi zmdi-badge-check',
                    type: 'link',
                    priority: 1.3,
                    state: 'triangular.dealercsipage1'
                });

                triMenu.addMenu({
                    name: kpiperf,
                    icon: 'zmdi zmdi-collection-text',
                    type: 'link',
                    priority: 1.4,
                    state: 'triangular.dealerkpiperformance'
                });
                triMenu.addMenu({
                    name: kpitrend,
                    icon: 'zmdi zmdi-hourglass',
                    type: 'link',
                    priority: 1.5,
                    state: 'triangular.dealerkpitrend'
                });
                triMenu.addMenu({
                    name: others,
                    icon: 'zmdi zmdi-assignment-o',
                    type: 'link',
                    priority: 1.6,
                    state: 'triangular.dealercsipage5'
                });

                triMenu.addMenu({
                    name: loyal,
                    icon: 'zmdi zmdi-favorite',
                    type: 'link',
                    priority: 2.0,
                    state: 'triangular.dealercsipage2'
                });


                triMenu.addMenu({
                    name: dscore,
                    icon: 'zmdi zmdi-view-list-alt',
                    type: 'link',
                    priority: 1.7,
                    state: 'triangular.dealercsipage3'
                });
                triMenu.addMenu({
                    name: regional,
                    icon: 'zmdi zmdi-map',
                    type: 'link',
                    priority: 1.1,
                    state: 'triangular.dealercsipage8'
                });

            }
            
            else if (showMenu == 'dealergroup') {

                
                triMenu.addMenu({
                    name: summary,
                    icon: 'zmdi zmdi-file-text',
                    type: 'link',
                    priority: 1.0,
                    state: 'triangular.dealergroupcsipage4'
                });

                triMenu.addMenu({
                    name: trend,
                    icon: 'zmdi zmdi-menu',
                    type: 'link',
                    priority: 1.2,
                    state: 'triangular.dealergroupcsipage7'
                });
                triMenu.addMenu({
                    name: priority,
                    icon: 'zmdi zmdi-badge-check',
                    type: 'link',
                    priority: 1.3,
                    state: 'triangular.dealergroupcsipage1'
                });
                triMenu.addMenu({
                    name: kpiperf,
                    icon: 'zmdi zmdi-collection-text',
                    type: 'link',
                    priority: 1.4,
                    state: 'triangular.dgkpiperformance'
                });
                triMenu.addMenu({
                    name: kpitrend,
                    icon: 'zmdi zmdi-hourglass',
                    type: 'link',
                    priority: 1.5,
                    state: 'triangular.dgkpitrend'
                });
                triMenu.addMenu({
                    name: others,
                    icon: 'zmdi zmdi-assignment-o',
                    type: 'link',
                    priority: 1.6,
                    state: 'triangular.dealergroupcsipage5'
                });

                triMenu.addMenu({
                    name: loyal,
                    icon: 'zmdi zmdi-favorite',
                    type: 'link',
                    priority: 2.0,
                    state: 'triangular.dealergroupcsipage2'
                });


                triMenu.addMenu({
                    name: dscore,
                    icon: 'zmdi zmdi-view-list-alt',
                    type: 'link',
                    priority: 1.7,
                    state: 'triangular.dealergroupcsipage3'
                });
                triMenu.addMenu({
                    name: regional,
                    icon: 'zmdi zmdi-map',
                    type: 'link',
                    priority: 1.1,
                    state: 'triangular.dealergroupcsipage8'
                });

            }
            
            
        }

        function tempfunction(oemdealer) {

            $scope.menutitles();
            if (oemdealer) {
                toggleExtraMenu('oem');
                console.log('oem', oemdealer, $rootScope.reporttype);
            } else
            if ($rootScope.dealerandgroup)
            {
                toggleExtraMenu('dealergroup');
            } else

            {
                toggleExtraMenu('dealer');
            }

        }
    }
})();