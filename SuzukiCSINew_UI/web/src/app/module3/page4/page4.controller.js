(function () {
    'use strict';

    angular
            .module('app.module3.page4')
            .controller('m3page4Controller', m3page4Controller);

    /* @ngInject */
    function m3page4Controller($scope, service, $http,$filter, $rootScope, $timeout) {
        var vm = this;
        $rootScope.langswitch = true;
        $rootScope.lockLeft = true;
        var $translate = $filter('translate');
        var color = ["#002776", "#92D400", "#00A1DE", "#72C7E7", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.hideyear = false;
        $rootScope.hidebiannual = true;
        $rootScope.hidemonth = true;
        $rootScope.hidezone = true;
        $rootScope.hideregion = true;
        $rootScope.hidemodel = true;
        $rootScope.hidedgregion = true;
         $rootScope.hidedealergroup = true;
        $rootScope.snaphidezone = true;
        $rootScope.snaphideregion = true;
        $rootScope.hidefactor = false;
        $rootScope.loyaltydealerhide = true;
        $rootScope.dealerpriorityhide = true;
        $rootScope.mainselectionhide = false;
        $rootScope.filterSelShow = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.redalertdealerhide = true;
        $rootScope.redfeedback = "not";
        $rootScope.hideyear2 = true;
        $scope.yearset = false;
        $scope.biset = false;
        $scope.monthset = true;
        $scope.show3 = true;
        $scope.show4 = false;
        var languagepara = [];
        
        $scope.svgname = $scope.factor;
//        $scope.factorname=$rootScope.Filterarray.factor[0].value;
        $rootScope.dealersnaphide = true;
        $rootScope.dealerhide = true;
        $rootScope.filterchanged = "On Login";
        $rootScope.pagenumber = '1';
        $rootScope.starlabelhide = false;
        $rootScope.legendname = "Study Best";
        $rootScope.legendnamew = "Worst Score";
        $rootScope.priorityhide = true;
        $scope.dealer = $rootScope.dealer;
        $scope.langcode = $rootScope.language;
         $scope.region = $rootScope.region;
         $scope.dealergroup=$rootScope.dealergroup;
        $rootScope.modelsnap = "not";
        $scope.zone = "All";
       
        $rootScope.periodhide = true;
        $scope.dealername = $scope.nameit;
        $scope.regionname = $scope.region;
        $scope.avg = "Average";
        $scope.model = "All";
        $scope.year = $rootScope.year;
        $scope.biannual = "All";
        
        $scope.legendname = "Study Best";
        $scope.legendnamew = "Worst Score";
        $scope.bestword = "Best";
        $rootScope.periodhide = true;
        //        $rootScope.dtwave=$scope.waves;
        if ($rootScope.summaryoffset) {
            $rootScope.summaryoffset = false;
        } else {
            $scope.$on("qsuccess",function(event,data){
                 $rootScope.checkboxInitiliser();
            });
           
        }
        $scope.summaryscope = 'region';
        $scope.summarymeasure = 'best';
        $scope.summaryscopecaps = 'Region';
        $scope.summarymeasurecaps = 'Best';
         $scope.factor = $rootScope.factor;
        var    benchmarkparameters = [{"name": "benchmarkmeasure", "value": $scope.summarymeasure}, {"name": "langCode", "value": $scope.langcode}, {"name": "benchmarkscope", "value": $scope.summaryscope},{"name": "year", "value": encodeURIComponent($scope.year)},{"name": "region", "value": encodeURIComponent($scope.region)}];
         


$scope.getStyles = function () {

            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '52%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '50%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': '36px'
            };
        };
        if ($scope.langcode == 'EN') {

            $scope.nationaltitle = "Dealer csi Score";
//            $scope.regionaltitle = $rootScope.Titlearray[2].ContentEN;
//            $scope.factortitle = $rootScope.Titlearray[3].ContentEN;
//            $scope.top5title = $rootScope.Titlearray[73].ContentEN;
//            $scope.delightedtitle = $rootScope.Titlearray[10].ContentEN;
//            $scope.footnote = $rootScope.Footarray[4].ContentEN;
//            $scope.footnote += "<br>" + $rootScope.Footarray[2].ContentEN;
//            $scope.footnote =  $rootScope.Footarray[0].ContentEN;
            $scope.factorscore = "Factor Score";
            $scope.svgname = "CSI";
            $scope.avg = "Average";
            $scope.benchmarktitle = "csi Score - ";
            $scope.bestword = "Best";
            $scope.firstchart = "";

        } else {
//            $scope.nationaltitle = "คะแนน CSI ของศูนย์บริการนี้";
//            $scope.regionaltitle = $rootScope.Titlearray[2].ContentReganal;
//            $scope.factortitle = $rootScope.Titlearray[3].ContentReganal;
//            $scope.top5title = $rootScope.Titlearray[73].ContentReganal;
//            $scope.delightedtitle = $rootScope.Titlearray[10].ContentReganal;
//            $scope.footnote = $rootScope.Footarray[4].ContentReganal;
//            $scope.footnote += "<br>" + $rootScope.Footarray[2].ContentReganal;
//            $scope.footnote += "<br>" + $rootScope.Footarray[0].ContentRegana;
//            $scope.factorscore = "คะแนนปัจจัย";
//            $scope.svgname = "ที่ปรึกษางานบริการ";
//            $scope.benchmarktitle = "คะแนน csi - ";
//            $scope.bestword = "ดีที่สุด";
//            $scope.avg = "เฉลี่ย";

  $scope.factorscore = "Factor Score";
            $scope.svgname = "csi";
            $scope.avg = "Average";
            $scope.benchmarktitle = "csi Score - ";
            $scope.bestword = "Best";
            $scope.firstchart = "";
            var regionpara1 = [{"name": "english", "value": encodeURIComponent($scope.region)}, {"name": "moduleType", "value": "CSI"}];
            service.mastersrv("Systemuserview-getName", regionpara1).then(function (response) {
                $scope.regionname = response.data[0].ContentReganal;
//                $scope.firstchart = "หมายเหตุ: แสดงเฉพาะข้อมูลล่าสุดที่มี";
            });

        }




        $scope.$on('changesummaryscoreswitch', function (event, data) {
            $scope.summaryscope = data;
            $scope.benchmark();
        });

        $scope.$on('changesummarymeasureswitch', function (event, data) {
            $scope.summarymeasure = data;
            $scope.benchmark();
        });


        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        console.log(" height :", h);
        console.log(" width :", w);
        var margin = 300;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 250;
        } else if (w < 960 && w > 600) {
            margin = 200;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 150;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        console.log('margin', margin);
        //////////////////////////////////////////////////////////////

        $scope.$on('changeLanguage', function (event, data) {   $scope.filterchanged = "Language Changed";
            $scope.langcode = data;
            var dealerpara = [{"name": "english", "value": $scope.dealername}, {"name": "moduleType", "value": "CSI"}];
            var regionpara = [{"name": "english", "value": encodeURIComponent($scope.region)}, {"name": "moduleType", "value": "CSI"}];
            var languagepara = [{"name": "english", "value": $scope.factor}, {"name": "moduleType", "value": "CSI"}];
            service.mastersrv("Systemuserview-getName", dealerpara).then(function (response) {
                if ($scope.langcode == 'EN') {
                    $scope.dealername = response.data[0].ContentEN;
                } else {
                    $scope.dealername = response.data[0].ContentReganal;
                }
            });
            service.mastersrv("Systemuserview-getName", regionpara).then(function (response) {
                if ($scope.langcode == 'EN') {
                    $scope.regionname = response.data[0].ContentEN;
                } else {
                    $scope.regionname = response.data[0].ContentReganal;
                }
            });
            service.mastersrv("Systemuserview-getName", languagepara).then(function (response) {
                if ($scope.langcode == 'EN') {
                    $scope.svgname = response.data[0].ContentEN;
                } else {
                    $scope.svgname = response.data[0].ContentReganal;
                }
            });

            $scope.filterchange();
            $rootScope.languagechangernav();
        });


        $scope.$on('changeyear', function (event, data) {   $scope.filterchanged = "Year Changed";
            $scope.year = data;
            $scope.show4 = false;
            $scope.show3 = true;
            $scope.yearset = true;
            $scope.biset = false;
            $scope.monthset = false;
            $scope.biannual = "All";
            $scope.month = "All";
            $scope.filterchange();
            $scope.yearscore();
        });

        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual = data;
            if ($scope.biannual == "All" || $scope.biannual == "Study Total") {
                $scope.show4 = false;
                $scope.show3 = true;
                $scope.yearset = true;
                $scope.biset = false;
                $scope.monthset = false;
                $scope.yearscore();
            } else {
                $scope.show4 = false;
                $scope.show3 = true;
                $scope.yearset = false;
                $scope.biset = true;
                $scope.monthset = false;
                $scope.biannualscore();
            }

            $scope.month = "All";
            $scope.filterchange();
        });

        $scope.$on('changemonth', function (event, data) {
            $scope.month = data;
            if ($scope.month == "All") {
                $scope.show4 = false;
                $scope.show3 = true;
                $scope.yearset = true;
                $scope.biset = false;
                $scope.monthset = false;
                $scope.yearscore();
            } else {
                $scope.show4 = true;
                $scope.show3 = false;
                $scope.yearset = false;
                $scope.biset = false;
                $scope.monthset = true;
                $scope.monthscore();
            }
            $scope.filterchange();
        });

        $scope.$on('changezone', function (event, data) {
            $scope.zone = data;
            $scope.region = "All";
            $scope.filterchange();
        });

        $scope.$on('changeregion', function (event, data) {   $scope.filterchanged = "Region Changed";
            $scope.region = data;
            $scope.filterchange();
        });

        $scope.$on('changemodel', function (event, data) {   $scope.filterchanged = "Model Changed";
            $scope.model = data;
            $scope.filterchange();
        });



        $scope.$on('changefactor', function (event, data) {
            $scope.factor = data;
            languagepara = [{"name": "english", "value": $scope.factor}, {"name": "moduleType", "value": "CSI"}];
            service.mastersrv("Systemuserview-getName", languagepara).then(function (response) {
                console.log('response response[0]', response.data[0].ContentReganal);
                if ($scope.langcode == 'EN') {
                    $scope.svgname = response.data[0].ContentEN;
                } else {
                    $scope.svgname = response.data[0].ContentReganal;
                }

            });

            $scope.botchange();
        });

        //////////////////////////////////////////////////////////////



        $scope.benchmark = function () {
              benchmarkparameters = [{"name": "benchmarkmeasure", "value": $scope.summarymeasure}, {"name": "langCode", "value": $scope.langcode}, {"name": "benchmarkscope", "value": $scope.summaryscope},{"name": "year", "value": encodeURIComponent($scope.year)},{"name": "region", "value": encodeURIComponent($scope.region)}];
         

           service.mastersrv("SummaryDealer-getNationalScoresYear", benchmarkparameters).then(function (response) {
                console.log(response.data);
                $scope.benchmarkcurrmonth = response.data.data[0].Wave;
               $scope.benchmark1 = response.data.data[0].Score;
                $scope.benchmarkprevmonth = response.data.data[1].Wave;
                $scope.benchmark2 = response.data.data[1].Score;
                $scope.benchmarkprevquater = response.data.data[2].Wave;
                $scope.benchmark13= response.data.data[2].Score;
            })
        }



        var parameters = [];

//        $rootScope.dealerScoreDealer = $rootScope.Filterarray.dealer[0];
        $rootScope.mapState = 0;

        parameters = [{"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "dealer", "value": $scope.dealer}, {"name": "model", "value": $scope.model}, {"name": "factor", "value": $scope.factor}];
//service.csi_filterfunction();
        $scope.getStyle = function () {
            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '53%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': $scope.radius / 3.5 + 'px'
            };
        };
        $scope.getStyle1 = function () {
            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '53%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': $scope.radius / 3.5 + 'px'
            };
        };


        $scope.botgraph = function () {

          
            
            
             service.mastersrv("SummaryDealer-getRegionalAttributeAverage", parameters).then(function (response) {
                $timeout(function () {
                    $('#treecontainer').highcharts(service.barplainbar('bar', ['#2979FF'], response.data,  $scope.svgname+'-'+$translate('meanlabel'), 250, 35, 0, [0], 5, false, " "));
                    $('#treecontainer1').highcharts(service.barplainbar('bar', ['#2979FF'], response.data,  $scope.svgname+'-'+$translate('meanlabel'), 180, 35, 0, [0], 5, false, " "));
                }, 1000)
            });
        }
//            $scope.yearscore1 = function () {
//            
//            
//            //csiSummary-getNationalcsiScores
//            service.mastersrv("Summary-getNationalScoresYear", parameters).then(function (response) {
//
//                if (response.data.samplecount < 10) {
//                    $scope.samplespaceless = true;
//                    //                    console.log($scope.samplespaceless);
//                } else {
//                    $scope.samplespaceless = false;
//                    //                    console.log($scope.samplespaceless);
//                }
//
//                $scope.currmonth = response.data.data[0].Wave;
//                $scope.svg1 = response.data.data[0].Score;
//                $scope.prevmonth = response.data.data[1].Wave;
//                $scope.svg2 = response.data.data[1].Score;
//                
//             
//                $scope.prevquater = response.data.data[2].Wave;
//                $scope.svg3 = response.data.data[2].Score;
//                
////                
//////                 $scope.bgColor =   $scope.svg1>0?     '#eaeaea': '#FFFFFF';
////                 $scope.color1 =   $scope.svg1>0?'#45ccce': '#FFFFFF';
////                  $scope.color2 =   $scope.svg2>0?"#45ccce": '#FFFFFF';
////                  $scope.color3 =   $scope.svg3>0?"#45ccce": "#FFFFFF";
//
//
//            });
//        };

        $scope.yearscore = function () {
            //Summary-getNationalScores
            service.mastersrv("SummaryDealer-getNationalScoresYear", parameters).then(function (response) {

                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                } else {
                    $scope.samplespaceless = false;
                }
                $scope.currmonth = response.data.data[0].Wave;
                $scope.svg1 = response.data.data[0].Score;
                $scope.prevmonth = response.data.data[1].Wave;
                $scope.svg2 = response.data.data[1].Score;
                $scope.prevquater = response.data.data[2].Wave;
                $scope.svg3 = response.data.data[2].Score;


            });
//           parametersq = [{"name": "period", "value": $scope.period},{"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)},{"name": "factor", "value": $scope.factor},{"name": "dealer", "value": $scope.dealer},{"name": "regiona", "value": encodeURIComponent($scope.regiona)} ,{"name": "year", "value": $scope.year},{"name": "quarter", "value": $scope.quarter},{"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}];
//            service.mastersrv("SummaryDealer-getCityScore", parametersq).then(function (response) {
//                //console.log("URL-------->",$location.absUrl().split('#/')[0]);
//                $scope.city = response.data.data;
//                if($scope.city.length > 3){
//                    $scope.chartheight="70px";
//                }else{
//                 $scope.chartheight="10px";   
//                }
//            });

            benchmarkparameters = [{"name": "benchmarkmeasure", "value": $scope.summarymeasure}, {"name": "langCode", "value": $scope.langcode}, {"name": "benchmarkscope", "value": $scope.summaryscope},{"name": "year", "value": encodeURIComponent($scope.year)},{"name": "region", "value": encodeURIComponent($scope.region)}];
            service.mastersrv("SummaryDealer-getNationalScoresYear", benchmarkparameters).then(function (response) {
                console.log(response.data);
                
                
                 $scope.benchmarkcurrmonth = response.data.data[0].Wave;
               $scope.benchmark1 = response.data.data[0].Score;
                $scope.benchmarkprevmonth = response.data.data[1].Wave;
                $scope.benchmark2 = response.data.data[1].Score;
                $scope.benchmarkprevquater = response.data.data[2].Wave;
                $scope.benchmark13= response.data.data[2].Score;
            })
        };

        

        

        $scope.graphs = function () {

            if ($scope.langcode == 'EN') {
                $scope.nationaltitle = "Dealer csi Score";
//                $scope.regionaltitle = $rootScope.Titlearray[2].ContentEN;
//                $scope.factortitle = $rootScope.Titlearray[3].ContentEN;
//                $scope.top5title = $rootScope.Titlearray[73].ContentEN;
//                $scope.delightedtitle = $rootScope.Titlearray[10].ContentEN;
//                $scope.footnote = $rootScope.Footarray[4].ContentEN;
//                $scope.footnote += "<br> " + $rootScope.Footarray[2].ContentEN;
//                $scope.footnote =  $rootScope.Footarray[0].ContentEN;
                $rootScope.overallfootnote = angular.copy($scope.footnote);

//                $scope.footnotetitle = $rootScope.Footarray[4].ContentEN;
//                $scope.footnotetitle += "\n " + $rootScope.Footarray[2].ContentEN;
//                $scope.footnotetitle =  $rootScope.Footarray[0].ContentEN;
//                $rootScope.overallfootnotetitle = angular.copy($scope.footnotetitle);
                $scope.factorscore = "Factor Score";
                $scope.avg = "Average";
//                    $scope.svgname="csi";
                $scope.benchmarktitle = "csi Score - ";
                $scope.bestword = "Best";
                $scope.firstchart = "";

            } else {
//                $scope.nationaltitle = "คะแนน csi ของศูนย์บริการนี้";
//                $scope.regionaltitle = $rootScope.Titlearray[2].ContentReganal;
//                $scope.factortitle = $rootScope.Titlearray[3].ContentReganal;
//                $scope.top5title = $rootScope.Titlearray[73].ContentReganal;
//                $scope.delightedtitle = $rootScope.Titlearray[10].ContentReganal;
//                $scope.footnote = $rootScope.Footarray[4].ContentReganal;

//                $scope.footnote += "<br> " + $rootScope.Footarray[2].ContentReganal;
//                $scope.footnote += "<br> " + $rootScope.Footarray[0].ContentReganal;
//                $rootScope.overallfootnote = angular.copy($scope.footnote);
//
//                $scope.footnotetitle = $rootScope.Footarray[4].ContentReganal;
//                $scope.footnotetitle += "\n  " + $rootScope.Footarray[2].ContentReganal;
//                $scope.footnotetitle += "\n  " + $rootScope.Footarray[0].ContentReganal;
//                $rootScope.overallfootnotetitle = angular.copy($scope.footnotetitle);
                  $scope.factorscore = "Factor Score";
                $scope.avg = "Average";
//                    $scope.svgname="csi";
                $scope.benchmarktitle = "csi Score - ";
                $scope.bestword = "Best";
                $scope.firstchart = "";
            }
//        
//            $scope.benchmark();
//           service.mastersrv("csiSummaryDealer-getcsiBenchmark", benchmarkparameters).then(function (response) {
//                    $scope.benchmark1=response.data[0].Score;
//                    $scope.benchmarkcurrmonth = response.data[0].Wave;
//                    $scope.benchmark2=response.data[1].Score;
//                    $scope.benchmarkprevmonth = response.data[1].Wave;
//                    $scope.benchmark3=response.data[2].Score;
//                    $scope.benchmarkprevquater = response.data[2].Wave;
//            })




            service.mastersrv("SummaryDealer-getSummaryRegionFactorScores", parameters).then(function (response) {
                $timeout(function () {
                    $('#barlinecontainer').highcharts(service.creditsummaryplainbar($scope.firstchart, 'column', ["#4292FF"], response.data,  $translate('regionalfactorscores'),  0, 45, .3, [1, 1, 1, 1, 1, 1], 5, false));
                    $('#barlinecontainer1').highcharts(service.creditsummaryplainbar($scope.firstchart, 'column', ["#4292FF"], response.data,  $translate('regionalfactorscores'),  0, 45, .3, [1, 1, 1, 1, 1, 1], 5, false));
                }, 1000)
                if ($scope.langcode == 'EN') {
                    $rootScope.legendname = response.data[0].scatter;
                    $rootScope.legendnamew = response.data[0].scatter1;
                } else {
                    $rootScope.legendname = response.data[0].scatter;
                    $rootScope.legendnamew = response.data[0].scatter1;
                }
                $rootScope.legendforselectionb = response.data[0].scatter;
                $rootScope.legendforselectionw = response.data[0].scatter1;
            });


            //csiRegional-getcsiRegionalAttributeAverage
            service.mastersrv("SummaryDealer-getRegionalAttributeAverage", parameters).then(function (response) {
                $timeout(function () {
                    $('#treecontainer').highcharts(service.barplainbar('bar', ['#2979FF'], response.data,  $scope.svgname+'-'+$translate('meanlabel'), 250, 35, 0, [0], 5, false, " "));
                    $('#treecontainer1').highcharts(service.barplainbar('bar', ['#2979FF'], response.data,  $scope.svgname+'-'+$translate('meanlabel'), 180, 35, 0, [0], 5, false, " "));
                }, 1000)
            });
            
             service.mastersrv("DealerSnapshot-getDealerSOPTopBottom", parameters).then(function (response) {
                $('#top2').highcharts(service.multbarchartsplit(105, 'bar', response.data, 250, 15, 10, $scope.sopscore, 9, '6.2px', "%"));
                $('#top21').highcharts(service.multbarchartsplit(105, 'bar', response.data, 250, 15, 10, $scope.sopscore, 9, '6.2px', "%"));
            });
            

            //Summary-getTop5Bottom5SOP
            service.mastersrv("Summary-getTop5Bottom5AttributesMean", parameters).then(function (response) {
                $timeout(function () {
                    $('#bot1').highcharts(service.multbarchartsplit(12, 'bar', response.data, 260, 15, 0, $translate('delightlabel'), 12, '8.5px', ""));
                    $('#bot11').highcharts(service.multbarchartsplit(12, 'bar', response.data, 260, 15, 0, $translate('delightlabel'), 12, '8.5px', ""));
                }, 1000)
            });

           




        };



        $scope.botchange = function () {
            parameters = [{"name": "year", "value": encodeURIComponent($scope.year)}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "dealer", "value": $scope.dealer}, {"name": "factor", "value": $scope.factor},{"name":"dealergroup","value":$scope.dealergroup}];
            $scope.botgraph();
        }


        $scope.filterchange = function () {
            parameters = [{"name": "year", "value": encodeURIComponent($scope.year)},  {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "dealer", "value": $scope.dealer}, {"name": "factor", "value": $scope.factor},{"name":"dealergroup","value":$scope.dealergroup}];
             $scope.yearscore();
            $scope.graphs();
        };

    }
})();

