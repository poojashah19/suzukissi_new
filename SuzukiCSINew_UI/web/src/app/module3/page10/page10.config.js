(function () {
    'use strict';

    angular
            .module('app.module3.page10')
            .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
                .state('triangular.dealercsipage10', {
                    url: '/dealercsipage10',
                    views: {
                        '': {
                            templateUrl: 'app/module3/page10/page10.tmpl.html',
                            controller: 'm3page10Controller',
                            controllerAs: 'vm',

                        },
                        'belowContent': {
                            templateUrl: 'app/module3/page10/fab-button.tmpl.html',
                            controller: 'SalesFabController',
                            controllerAs: 'vm'
                        }

                    },

                    data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
                });

//         triMenuProvider.addMenu({
//                   name: 'SOP Performance',
//                    icon: 'zmdi zmdi-plus-square',
//                    type: 'link',
//                    priority: 1.4,
//                     state: 'triangular.dealercsipage10',
//                });
//             triMenuProvider.removeMenu('triangular.dealercsipage10');   

    }
})();