(function () {
    'use strict';

    angular
            .module('app.module3.dealerkpiperformance')
            .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
                .state('triangular.dealerkpiperformance', {
                    url: '/dealerkpiperformance',
                    views: {
                        '': {
                            templateUrl: 'app/module3/dealerkpiperformance/dealerkpiperformance.tmpl.html',
                            controller: 'dealerkpiperformanceController',
                            controllerAs: 'vm'

                        },
                        'belowContent': {
                            templateUrl: 'app/module3/dealerkpiperformance/fab-button.tmpl.html',
                            controller: 'SalesFabController',
                            controllerAs: 'vm'
                        }

                    },

                    data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
                });

    }
})();