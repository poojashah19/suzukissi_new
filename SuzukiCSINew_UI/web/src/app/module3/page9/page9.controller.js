(function () {
    'use strict';

    angular
            .module('app.module3.page9')
            .controller('m3page9Controller', m3page9Controller);

    /* @ngInject */
    function m3page9Controller($scope, $state, service, $http, $rootScope) {
        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776", "#92D400", "#00A1DE", "#72C7E7", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.hideyear = true;
        $rootScope.hidebiannual = true;
        $rootScope.hidemonth = true;
        $rootScope.hidezone = true;
        $rootScope.hideregion = true;
        $rootScope.hidedealer = true;
        $rootScope.dealerhide = true;
        $rootScope.snaphidezone = true;
        $rootScope.hidedgregion = true;
        $rootScope.snaphideregion = true;
        $rootScope.hidemodel = true;
        $rootScope.hidemodel = true;
        $rootScope.hidefactor = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.dealersnaphide = true;
        $rootScope.dealerhide = true;
        $rootScope.filterchanged = "On Login";
        $rootScope.pagenumber = '6';
        $rootScope.starlabelhide = true;
        $rootScope.priorityhide = true;
        $rootScope.summaryRefresh = 1;
        $scope.sopname = $rootScope.Filterarray.sop[0].value;
        
       
        $scope.langcode = $rootScope.language;
        $rootScope.redfeedback = "not";
        $rootScope.loyaltydealerhide = true;
        $rootScope.dealerpriorityhide = true;
        $rootScope.mainselectionhide = true;
        $rootScope.filterSelShow = false;
        $rootScope.periodhide = true;
         $rootScope.hideyear2 = true;
        $scope.period = "Year";
        $rootScope.checkboxInitiliser();
        $scope.dealer = $rootScope.dealer;

        if ($scope.langcode == 'EN') {
//            $scope.sottitle1 = $rootScope.Titlearray[54].ContentEN;
//            $scope.sottitle2 = $rootScope.Titlearray[56].ContentEN;
//            $scope.sottitle3 = $rootScope.Titlearray[57].ContentEN;
            $scope.opt1 = "Total";
            $scope.opt2 = "Zone";
            $scope.opt3 = "Region";
//            $scope.sotfooter1 = $rootScope.Titlearray[58].ContentEN;
//            $scope.sotfooter2 = $rootScope.Titlearray[59].ContentEN;
//            $scope.sotfooter3 = $rootScope.Titlearray[60].ContentEN;
//            $scope.footnote = $rootScope.Footarray[1].ContentEN;
            $scope.graphname = 'Selected Main Selection';
        } else {
//            $scope.sottitle1 = $rootScope.Titlearray[54].ContentReganal;
//            $scope.sottitle2 = $rootScope.Titlearray[56].ContentReganal;
//            $scope.sottitle3 = $rootScope.Titlearray[57].ContentReganal;
            $scope.opt1 = "全部";
            $scope.opt2 = "โซน";
            $scope.opt3 = "ภูมิภาค";
//            $scope.sotfooter1 = $rootScope.Titlearray[58].ContentReganal;
//            $scope.sotfooter2 = $rootScope.Titlearray[59].ContentReganal;
//            $scope.sotfooter3 = $rootScope.Titlearray[60].ContentReganal;
//            $scope.footnote = $rootScope.Footarray[1].ContentReganal;
            $scope.graphname = 'หัวข้อที่เลือก';
        }


        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        console.log(" height :", h);
        console.log(" width :", w);
        var margin = 300;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 250;
        } else if (w < 960 && w > 600) {
            margin = 200;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 150;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        console.log('margin', margin);

        $scope.$on('changeyear', function (event, data) {   $scope.filterchanged = "Year Changed";
            $scope.year = data;
            $scope.biannual = "All";
            $scope.month = "All";
            $scope.filterchange();
        });


        $scope.$on('changeLanguage', function (event, data) {   $scope.filterchanged = "Language Changed";
            $scope.langcode = data;
            languagepara = [{"name": "english", "value": encodeURIComponent($scope.sop)}, {"name": "moduleType", "value": "CSI"}];
            service.mastersrv("Systemuserview-getName", languagepara).then(function (response) {
                console.log('response response[0]', response.data[0].ContentReganal);
                if ($scope.langcode == 'EN') {
//                    $scope.sopname = response.data[0].ContentEN;
                    $scope.sopname = $scope.sop;

                } else {
                    $scope.sopname = $scope.sop;
//                    $scope.sopname = response.data[0].ContentReganal;
                }
            });
            $scope.filterchange();
        });

        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual = data;
            $scope.month = "All";
            $scope.filterchange();
        });

        $scope.$on('changemonth', function (event, data) {
            $scope.month = data;
            $scope.filterchange();
        });

        $scope.$on('changezone', function (event, data) {
            $scope.zone = data;
            $scope.region = "All";
            $scope.filterchange();
        });

        $scope.$on('changeregion', function (event, data) {   $scope.filterchanged = "Region Changed";
            $scope.region = data;
            $scope.filterchange();
        });

        $scope.$on('changedealer', function (event, data) {   $scope.filterchanged = "Dealer Changed";
            $scope.dealer = data;
            $scope.filterchange();
        });
        $scope.$on('changeperiod', function (event, data) {   $scope.filterchanged = "Period Changed";
            $scope.period = data;
            $scope.filterchange();
//            $scope.year = "All";
        });
        //////////////////////////////////////////////////////////////


        var languagepara = [];
        vm.sopwswitch = sopswitch;
        $scope.region = $rootScope.region;
        $scope.zone = "All";
        $scope.region = "All";
        $scope.dealer = $rootScope.dealer;
        $scope.year = $rootScope.Filterarray.year[0].name;
        $scope.biannual = "All";
        $scope.month = "All";//$rootScope.Filterarray.month[1].name;
//        $scope.factor = $rootScope.Filterarray.factor[0].name;
        $scope.sopswitchoutput = 'total';
        $scope.sopswitchoutputcaps = 'Total';
        $scope.sop = $rootScope.Filterarray.sop[0].name;

        var parameters = [{"name": "period", "value": $scope.period}, {"name": "compareto", "value": $scope.sopswitchoutput}, {"name": "attribute", "value": encodeURIComponent($scope.sop)}, {"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)},  {"name": "dealer", "value": $scope.dealer}];
        service.csi_filterfunction();
        $scope.group1 = "Total";

        function sopswitch(flag) {
            if (flag == 'Total') {
                $scope.sopswitchoutput = 'total';
                $scope.sopswitchoutputcaps = 'Total';
            } else if (flag == 'Zone') {
                $scope.sopswitchoutput = 'zone';
                $scope.sopswitchoutputcaps = 'Zone';
            } else if (flag == 'Region') {
                $scope.sopswitchoutput = 'region';
                $scope.sopswitchoutputcaps = 'Region';
            }

            $scope.filterchange();
        }


        $scope.graphs = function () {
            if ($scope.langcode == 'EN') {
//                $scope.sottitle1 = $rootScope.Titlearray[54].ContentEN;
//                $scope.sottitle2 = $rootScope.Titlearray[56].ContentEN;
//                $scope.sottitle3 = $rootScope.Titlearray[57].ContentEN;
                $scope.opt1 = "Total";
                $scope.opt2 = "Zone";
                $scope.opt3 = "Region";
//                $scope.sotfooter1 = $rootScope.Titlearray[58].ContentEN;
//                $scope.sotfooter2 = $rootScope.Titlearray[59].ContentEN;
//                $scope.sotfooter3 = $rootScope.Titlearray[60].ContentEN;
                $scope.sopfootnote = "+Based on those respondents who made an appointment for the service";
//                $scope.sopfootnote += "<br> " + $rootScope.Footarray[19].ContentEN;
//                $scope.sopfootnote += "<br>" + $rootScope.Footarray[3].ContentEN;
//                $scope.sopfootnote =  $rootScope.Footarray[0].ContentEN;
//                $rootScope.sopfootnote = angular.copy($scope.sopfootnote);

                $scope.sopfootnotetitle = "+Based on those respondents who made an appointment for the service";
//                $scope.sopfootnotetitle += "\n" + $rootScope.Footarray[19].ContentEN;
//                $scope.sopfootnotetitle += "\n" + $rootScope.Footarray[3].ContentEN;
//                $scope.sopfootnotetitle =  $rootScope.Footarray[0].ContentEN;
//                $rootScope.sopfootnotetitle = angular.copy($scope.sopfootnotetitle);
                $scope.graphname = 'Selected Main Selection';
            } else {
//                $scope.sottitle1 = $rootScope.Titlearray[54].ContentReganal;
//                $scope.sottitle2 = $rootScope.Titlearray[56].ContentReganal;
//                $scope.sottitle3 = $rootScope.Titlearray[57].ContentReganal;
                $scope.opt1 = "全部";
                $scope.opt2 = "โซน";
                $scope.opt3 = "ภูมิภาค";
//                $scope.sotfooter1 = $rootScope.Titlearray[58].ContentReganal;
//                $scope.sotfooter2 = $rootScope.Titlearray[59].ContentReganal;
//                $scope.sotfooter3 = $rootScope.Titlearray[60].ContentReganal;
                $scope.sopfootnote = "+ข้อมูลที่แสดงนี้มาจากกลุ่มลูกค้าที่ทำการนัดหมายล่วงหน้า เพื่อขอรับบริการ";
//                $scope.sopfootnote += "<br> " + $rootScope.Footarray[19].ContentReganal;
//                $scope.sopfootnote += "<br>" + $rootScope.Footarray[3].ContentReganal;
//                $scope.sopfootnote = $rootScope.Footarray[0].ContentReganal;
                $rootScope.sopfootnote = angular.copy($scope.sopfootnote);

                $scope.sopfootnotetitle = "+ข้อมูลที่แสดงนี้มาจากกลุ่มลูกค้าที่ทำการนัดหมายล่วงหน้า เพื่อขอรับบริการ";
//                $scope.sopfootnotetitle += "\n" + $rootScope.Footarray[19].ContentReganal;
//                $scope.sopfootnotetitle += "\n" + $rootScope.Footarray[3].ContentReganal;
//                $scope.sopfootnotetitle =  $rootScope.Footarray[0].ContentReganal;
                $rootScope.sopfootnotetitle = angular.copy($scope.sopfootnotetitle);
                $scope.graphname = 'หัวข้อที่เลือก';
            }


            service.mastersrv("SOPPerformance-getSOPTrend", parameters).then(function (response) {
//                 console.log('response.data',response.data);
                console.log('response.data.data', response.data.data);
                console.log('response.data.samplecount', response.data.samplecount);
                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    console.log($scope.samplespaceless);
                }
                if (response.data.data.length == 0) {
                    response.data.data = [{"bartitle": "100%"}, {"y": "0"}];
                }
                $('#mainchart').highcharts(service.barline(['#2196F3'], response.data.data, $scope.graphname, "%"));

            });

        };

        $scope.sopchanged = function (sop) {
            $scope.sop = sop;
//            alert($scope.sop + "in changed")
            $scope.sopname = $scope.sop;
            languagepara = [{"name": "english", "value": encodeURIComponent($scope.sop)}, {"name": "moduleType", "value": "CSI"}];
            service.mastersrv("Systemuserview-getName", languagepara).then(function (response) {
                console.log('response response[0]', response.data[0].ContentReganal);
                if ($scope.langcode == 'EN') {

//                    $scope.sopname = $scope.sop;
                    $scope.sopname = response.data[0].ContentEN;
                } else {
                    $scope.sopname = response.data[0].ContentReganal;
//                    $scope.sopname = $scope.sop;
                }
            });
            $scope.filterchange();
        };

        $scope.filterchange = function () {
//             $scope.switchdisable=false;
            parameters = [{"name": "period", "value": $scope.period}, {"name": "compareto", "value": $scope.sopswitchoutput}, {"name": "langCode", "value": $scope.langcode}, {"name": "attribute", "value": encodeURIComponent($scope.sop)},  {"name": "region", "value": encodeURIComponent($scope.region)},  {"name": "dealer", "value": $scope.dealer}];
            $scope.graphs();
        };
    }
})();