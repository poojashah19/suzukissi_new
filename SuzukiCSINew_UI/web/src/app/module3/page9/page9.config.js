(function () {
    'use strict';

    angular
            .module('app.module3.page9')
            .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
                .state('triangular.dealercsipage9', {
                    url: '/dealercsipage9',

                    views: {
                        '': {
                            templateUrl: 'app/module3/page9/page9.tmpl.html',
                            controller: 'm3page9Controller',
                            controllerAs: 'vm',

                        },
                        'belowContent': {
                            templateUrl: 'app/module3/page9/fab-button.tmpl.html',
                            controller: 'SalesFabController',
                            controllerAs: 'vm'
                        }

                    },
                    data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
                });
//        
//         triMenuProvider.addMenu({
//                   name: 'SOP Trend',
//                    icon: 'zmdi zmdi-plus-square',
//                    type: 'link',
//                    priority: 1.5,
//                     state: 'triangular.dealercsipage9',
//                });
//             triMenuProvider.removeMenu('triangular.dealercsipage9');   


    }
})();