(function () {
    'use strict';

    angular
            .module('app.module3.page5')
            .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
                .state('triangular.dealercsipage5', {
                    url: '/dealercsipage5',

                    views: {
                        '': {
                            templateUrl: 'app/module3/page5/page5.tmpl.html',
                            controller: 'm3page5Controller',
                            controllerAs: 'vm',

                        },
                        'belowContent': {
                            templateUrl: 'app/module3/page5/fab-button.tmpl.html',
                            controller: 'SalesFabController',
                            controllerAs: 'vm'
                        }

                    },
                    data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
                });

//         triMenuProvider.addMenu({
//                   name: 'Others Analysis',
//                    icon: 'zmdi zmdi-plus-square',
//                    type: 'link',
//                    priority: 1.6,
//                     state: 'triangular.dealercsipage5',
//                });
//             triMenuProvider.removeMenu('triangular.dealercsipage5');   


    }
})();