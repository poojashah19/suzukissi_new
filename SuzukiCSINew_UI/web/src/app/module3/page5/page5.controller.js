(function () {
    'use strict';

    angular
            .module('app.module3.page5')
            .controller('m3page5Controller', m3page5Controller);

    /* @ngInject */
    function m3page5Controller($scope, $state, service,$filter, $http, $rootScope) {
        var vm = this;
        
          service.csi_filterfunction();
         $scope.$on("qsuccess",function(event,data){
                 $rootScope.checkboxInitiliser();
            });
          var $translate = $filter('translate');
          $rootScope.lockLeft = true;
        var color = ["#002776", "#92D400", "#00A1DE", "#FFA500", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.hideyear = false;
        $rootScope.hidebiannual = true;
        $rootScope.hidedgregion = true;
        $rootScope.hidemonth = true;
        $rootScope.hidezone = true;
        $rootScope.hideregion = true;
        $rootScope.hidemodel = true;
        $rootScope.hidefactor = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.snaphidezone = true;
        $rootScope.snaphideregion = true;
        $rootScope.dealerhide = true;
        $rootScope.dealersnaphide = true;
        $rootScope.filterchanged = "On Login";
        $rootScope.pagenumber = '7';
        $rootScope.starlabelhide = true;
        $rootScope.priorityhide = true;
        $rootScope.loyaltydealerhide = true;
        $rootScope.summaryRefresh = 1;
        
        $scope.region = $rootScope.region;
        $scope.model ="All";
        $scope.year = $rootScope.year;
        $scope.dealergroup=$rootScope.dalergroup;
        $scope.biannual = "All";
        
       
        $scope.langcode = $rootScope.language;
        $rootScope.hideyear2 = true;
        $rootScope.dsnapshot = "not";
        $rootScope.redfeedback = "not";
    
        $rootScope.mainselectionhide = true;
        $rootScope.filterSelShow = true;
        $rootScope.periodhide = true;
        if ($scope.langcode == 'EN') {
//            $scope.othertitle1 = $rootScope.Titlearray[61].ContentEN;
            $scope.page1 = "Page 1";
            $scope.page2 = "Page 2";
//            $scope.Questions = $rootScope.Titlearray[62].ContentEN;
            $scope.footer1 = "(1) Base: By Appointment";
            $scope.footer2 = "(2) Base: Vehicle ready on the same day of service";
            $scope.footer3 = "(3) Base: Contacted After Service";
        } else {
//            $scope.othertitle1 = $rootScope.Titlearray[61].ContentReganal;
//            $scope.page1 = "หน้า 1";
//            $scope.page2 = "หน้า 2";
//            $scope.Questions = $rootScope.Titlearray[62].ContentReganal;
//            $scope.footer1 = "(1) หมายเหตุ: เฉพาะกลุ่มตัวอย่างที่ทำการนัดหมายล่วงหน้า";
//            $scope.footer2 = "(2) หมายเหตุ: เฉพาะกลุ่มตัวอย่างที่ได้รับบริการเสร็จสิ้นภายในวันเดียวกัน";
//            $scope.footer3 = "(3) หมายเหตุ: เฉพาะกลุ่มตัวอย่างที่ได้รับการติดต่อรับเข้ารับบริการ";
        }

        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        console.log(" height :", h);
        console.log(" width :", w);
        var margin = 300;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 250;
        } else if (w < 960 && w > 600) {
            margin = 200;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 150;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        console.log('margin', margin);



        $scope.$on('changeLanguage', function (event, data) {   $scope.filterchanged = "Language Changed";
            $scope.langcode = data;
            $scope.filterchange();
        });


        $scope.$on('changeyear', function (event, data) {   $scope.filterchanged = "Year Changed";
            $scope.year = data;
          
            $scope.filterchange();
        });

        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual = data;
            $scope.month = "All";
            $scope.filterchange();
        });

        $scope.$on('changemonth', function (event, data) {
            $scope.month = data;
            $scope.filterchange();
        });

        $scope.$on('changezone', function (event, data) {
            $scope.zone = data;
            $scope.region = "All";
            $scope.filterchange();
        });

        $scope.$on('changeregion', function (event, data) {   $scope.filterchanged = "Region Changed";
            $scope.region = data;
            $scope.filterchange();
        });

        $scope.$on('changemodel', function (event, data) {   $scope.filterchanged = "Model Changed";
            $scope.model = data;
            $scope.filterchange();
        });

        $scope.$on('changedealer', function (event, data) {   $scope.filterchanged = "Dealer Changed";
            $scope.dealer = data;
            $scope.filterchange();
        });
        //////////////////////////////////////////////////////////////

        $("#otherexportCS").click(function () {
            $("#table22").table2excel({
                // exclude CSS class
                exclude: ".noExl",
                filename: "otherTableDownload"
            });
        });

        var parameters = [];
//        $scope.year = $rootScope.Filterarray.year[$rootScope.Filterarray.year.length - 1];
        $scope.region = $rootScope.region;
        $scope.model = "All";

        parameters = [{"name": "langCode", "value": $scope.langcode}, {"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "dealer", "value": $scope.dealer}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "dealer", "value": $scope.dealer}, {"name": "dealergroup", "value": $scope.dealergroup}];
        //////////////////////////////////////////////////////////////
        var marginp1;
        var marginp2;
        var marginp3;
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        console.log(" width :", w);

        if (w < 1400)
        {
            marginp2 = 80;
        } else
        {
            marginp2 = 76;
        }

        if (w > 1600) {
            marginp3 = null;
        } else
        {
            marginp3 = 90;
        }

        if (w > 1680)
        {
            marginp1 = null;
        } else
        {
            marginp1 = 80;
        }
        console.log('marginp1', marginp1);
        console.log('marginp2', marginp2);
        console.log('marginp3', marginp3);
//////////////////////////////////////////////////////////////
        $scope.graphs = function () {
            $scope.hideloader = false;
            if ($scope.langcode == 'EN') {
//                $scope.othertitle1 = $rootScope.Titlearray[61].ContentEN;
                $scope.page1 = "Page 1";
                $scope.page2 = "Page 2";
                $scope.page3 = "Page 3";
                $scope.page4 = "Page 4";
                $scope.page5 = "Page 5";
                $scope.page6 = "Page 6";
//                $scope.Questions = $rootScope.Titlearray[62].ContentEN;
                $scope.footer1 = "(1) Base: Those who made an appointment";
                $scope.footer2 = "(2) Base: Those who saw cleaning checkcard placed in the car";
                $scope.footer3 = "(3) Base: Those who were contacted after the service";
                $scope.footer4 = "(4) Base: Those who reported problems when contacted";
                 $scope.foot="++ indicates Multiple Response Question, total might exceed 100%";
//                $scope.footnote = $rootScope.Footarray[4].ContentEN;
//                $scope.footnote =  $rootScope.Footarray[0].ContentEN;
//                $scope.footnotetitle = $rootScope.Footarray[4].ContentEN;
//                $scope.footnotetitle =  $rootScope.Footarray[0].ContentEN;
//                $scope.tableFootnote = $rootScope.Footarray[5].ContentEN;
//                $scope.tableFootnote += "\n" + $rootScope.Footarray[6].ContentEN;
//                $scope.tableFootnote += "\n" + $rootScope.Footarray[7].ContentEN;
//                $scope.tableFootnote += "\n" + $rootScope.Footarray[8].ContentEN;
//                $scope.tableFootnote += "\n" + $rootScope.Footarray[9].ContentEN;
//                $scope.chartsFootnote = $rootScope.Footarray[10].ContentEN;
//                $scope.chartsFootnote += "\n" + $rootScope.Footarray[11].ContentEN;
//                $scope.chartsFootnote += "\n" + $rootScope.Footarray[12].ContentEN;
//                $scope.chartsFootnote += "\n" + $rootScope.Footarray[13].ContentEN;
//                $scope.chartsFootnote += "\n" + $rootScope.Footarray[14].ContentEN;
//                $scope.chartsFootnote += "\n" + $rootScope.Footarray[15].ContentEN;
//                $scope.chartsFootnote += "\n" + $rootScope.Footarray[16].ContentEN;
//                $scope.chartsFootnote += "\n" + $rootScope.Footarray[17].ContentEN;
//                $scope.chartsFootnote += "\n" + $rootScope.Footarray[18].ContentEN;
            } else {
                
                 $scope.footer1 = "(1) Base: Those who made an appointment";
                $scope.footer2 = "(2) Base: Those who saw cleaning checkcard placed in the car";
                $scope.footer3 = "(3) Base: Those who were contacted after the service";
                $scope.footer4 = "(4) Base: Those who reported problems when contacted";
                $scope.foot="++ 複選題，總百分比可能會超過100%";
//                $scope.othertitle1 = $rootScope.Titlearray[61].ContentReganal;
//                $scope.page1 = "หน้า 1";
//                $scope.page2 = "หน้า 2";
//                $scope.page3 = "หน้า 3";
//                $scope.page4 = "หน้า 4";
//                $scope.page5 = "หน้า 5";
//                $scope.page6 = "หน้า 6";
//                $scope.Questions = $rootScope.Titlearray[62].ContentReganal;
//                $scope.footer1 = "(1) หมายเหตุ: เฉพาะกลุ่มตัวอย่างที่ทำการนัดหมายล่วงหน้า";
//                $scope.footer2 = "(2) หมายเหตุ: เฉพาะกลุ่มตัวอย่างที่ได้รับบริการเสร็จสิ้นภายในวันเดียวกัน";
//                $scope.footer3 = "(3) หมายเหตุ: เฉพาะกลุ่มตัวอย่างที่ได้รับการติดต่อรับเข้ารับบริการ";
//                $scope.footnote = $rootScope.Footarray[4].ContentReganal;
//                $scope.footnote += "<br>" + $rootScope.Footarray[0].ContentReganal;
//
//                $scope.footnotetitle = $rootScope.Footarray[4].ContentReganal;
//                $scope.footnotetitle += "\n" + $rootScope.Footarray[0].ContentReganal;
//                $scope.tableFootnote = $rootScope.Footarray[5].ContentReganal;
//                $scope.tableFootnote += "\n" + $rootScope.Footarray[6].ContentReganal;
//                $scope.tableFootnote += "\n" + $rootScope.Footarray[7].ContentReganal;
//                $scope.tableFootnote += "\n" + $rootScope.Footarray[8].ContentReganal;
//                $scope.tableFootnote += "\n" + $rootScope.Footarray[9].ContentReganal;
//                $scope.chartsFootnote = $rootScope.Footarray[10].ContentReganal;
//                $scope.chartsFootnote += "\n" + $rootScope.Footarray[11].ContentReganal;
//                $scope.chartsFootnote += "\n" + $rootScope.Footarray[12].ContentReganal;
//                $scope.chartsFootnote += "\n" + $rootScope.Footarray[13].ContentReganal;
//                $scope.chartsFootnote += "\n" + $rootScope.Footarray[14].ContentReganal;
//                $scope.chartsFootnote += "\n" + $rootScope.Footarray[15].ContentReganal;
//                $scope.chartsFootnote += "\n" + $rootScope.Footarray[16].ContentReganal;
//                $scope.chartsFootnote += "\n" + $rootScope.Footarray[17].ContentReganal;
//                $scope.chartsFootnote += "\n" + $rootScope.Footarray[18].ContentReganal;
            }

            //       Others-getOthersTable
            service.mastersrv("Others-getOthersTable", parameters).then(function (response) {
                $scope.testdata = response.data.data;
                if (response.data) {
                    $scope.hideloader = true;
                }
                console.log('response.data', response.data);
                console.log('response.data.data', response.data.data);
                console.log('response.data', response.data.samplecount);

                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    console.log($scope.samplespaceless);
                }

  $scope.sampleCount = response.data.samplecount;
                $scope.astersik = '';
                if ($scope.sampleCount < 10) {
                    $scope.astersik = '**';
                } else if ($scope.sampleCount >= 10 && $scope.sampleCount < 30) {
                    $scope.astersik = '*'
                } else {
                    $scope.astersik;
                }


                $scope.Heading1 = response.data.data[0].Heading1;
                $scope.Heading2 = response.data.data[0].Heading2;
                $scope.Heading3 = response.data.data[0].Heading4;
//                $scope.Heading4 = response.data.data[0].Heading4;

                var Value1 = response.data.data[0].Heading1;
                var Value2 = response.data.data[0].Heading2;
                var Value3 = response.data.data[0].Heading4;
                //             $scope.format1( $scope.testdata[i].Value1); $scope.format1( $scope.testdata[i].Value2); $scope.format1( $scope.testdata[i].Value3); $scope.format1( $scope.testdata[i].Value4);
            });

            //Others-getOthersCharts
            service.mastersrv("Others-getOthersCharts", parameters).then(function (response) {
                            var count=1;
                for (var i = 0; i < response.data.length; i++)
                {

                    if ((i == 0) || (i == 3) ||  (i == 5) || (i == 7))
                    {
 
                         if(i==7){
                           $('#paget' + count).highcharts(service.stackmultigraph(response.data[i].headingdata, color, 25, '11px', '.07', response.data[i].headingname,''));  
                        }
                        else{
                        $('#paget' + count).highcharts(service.stackmultigraph(response.data[i].headingdata, color, 25, '11px', '.07', response.data[i].headingname,$scope.foot));
                       }
                    } else
                    {
                      
                            $('#paget' + count).highcharts(service.stackedgraph(response.data[i].headingdata, color, null, response.data[i].headingname));
                        


                    }
                    count++;
                }

            });

        };

        $scope.filterchange = function () {
            parameters = [{"name": "langCode", "value": $scope.langcode}, {"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "dealergroup", "value": $scope.dealergroup}, {"name": "model", "value": $scope.model}, {"name": "dealer", "value": $scope.dealer}];
            $scope.graphs();
        };
    }
})();