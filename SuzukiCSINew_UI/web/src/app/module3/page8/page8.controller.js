(function () {
    'use strict';

    angular
            .module('app.module3.page8')
            .controller('m3page8Controller', m3page8Controller);

    /* @ngInject */
    function m3page8Controller($scope, $state,$filter, service, $http, $rootScope) {
        var vm = this;
        $rootScope.lockLeft = true;
         service.csi_filterfunction();
         $scope.$on("qsuccess",function(event,data){
                 $rootScope.checkboxInitiliser();
            });
        var $translate = $filter('translate');
        var color = ["#002776", "#92D400", "#00A1DE", "#72C7E7", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.hideyear = false;
        $rootScope.hidebiannual = true;
        $rootScope.hidemonth = true;
        $rootScope.hidedgregion = true;
        $rootScope.hidezone = true;
        $rootScope.hideregion = true;
        $rootScope.hidemodel = true;
        $rootScope.hidefactor = true;
        $rootScope.snaphidezone = true;
        $rootScope.snaphideregion = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.dealersnaphide = true
        $rootScope.dealerhide = true;
        $rootScope.filterchanged = "On Login";
        $rootScope.pagenumber = '2';
        $rootScope.legendname = "Study Best";
        $rootScope.legendnamew = "Worst Score";
       $rootScope.starlabelhide = false;
        $rootScope.priorityhide = true;
        $rootScope.loyaltydealerhide = true;
        $rootScope.dealerpriorityhide = true;
         $rootScope.hideyear2 = true;
//        $rootScope.mainselectionhide= true;
//         $rootScope.mainSelectionTooltip = "Filters";
//        $rootScope.filterSelShow = false;
        $rootScope.summaryRefresh = 1;
        $rootScope.modelsnap = "not";
        $scope.langcode = $rootScope.language;
        $scope.zone = "All";
        $scope.region = $rootScope.region;
        console.log('$scope.region', $scope.region);
        console.log('$scope.region', $scope.region);
        console.log('$scope.region', $scope.region);
        console.log('$scope.region', $scope.region);
        console.log('$scope.region', $scope.region);
        console.log('$scope.region', $scope.region);
        console.log('$scope.region', $scope.region);
        $scope.model ="All";
        $scope.year = $rootScope.year;
        $scope.biannual = "All";
        $scope.month = $rootScope.month;
        $rootScope.mainselectionhide = true;
        $rootScope.filterSelShow = true;
        $rootScope.periodhide = true;
        $scope.dealer = $rootScope.dealer;
        $scope.svgname = $rootScope.factor;
        $rootScope.redfeedback = "not";
  
        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        console.log(" height :", h);
        console.log(" width :", w);
        var margin = 300;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 250;
        } else if (w < 960 && w > 600) {
            margin = 200;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 150;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        console.log('margin', margin);


        if ($scope.langcode == 'EN') {
            $scope.factorl = "Factor";
            $scope.regfacttitle = "Factor Score";
//            $scope.byregiontitle = $rootScope.Titlearray[72].ContentEN;
//            $scope.meantitle = $rootScope.Titlearray[73].ContentEN;
//            $scope.percenttitle = $rootScope.Titlearray[74].ContentEN;
            $scope.factorscore = "Factor Score";
            $scope.attscore = 'Attribute Score';
//            $scope.footnote = $rootScope.Footarray[1].ContentEN;
        } else {
            
             $scope.factorl = "Factor";
            $scope.regfacttitle = "Factor Score";
            $scope.factorscore = "Factor Score";
            $scope.attscore = 'Attribute Score';
//            $scope.regfacttitle = "ปัจจัยต่างๆ รายภูมิภาค";
//            $scope.byregiontitle = $rootScope.Titlearray[72].ContentReganal;
//            $scope.meantitle = $rootScope.Titlearray[73].ContentReganal;
//            $scope.percenttitle = $rootScope.Titlearray[74].ContentReganal;
//            $scope.factorscore = "คะแนนปัจจัย";
//            $scope.attscore = 'คุณลักษณะ คะแนน';
//            $scope.factorl = 'ปัจจัยต่างๆ';
//            $scope.footnote = $rootScope.Footarray[1].ContentReganal;
        }


        $scope.state = "All";
        $scope.zone = "All";
//            $scope.region = "All";
        $scope.model = "All";
        $scope.legendname = "Study Best";
        $scope.legendnamew = "Worst Score";
        $scope.city = "All";

//  $scope.model = $rootScope.model;
        $scope.year = $rootScope.year;
        $scope.biannual = "All";
        $scope.month = $rootScope.month;
        $scope.dealer = $rootScope.dealer;
        $scope.factor = $rootScope.factor;
//        alert($scope.factor+"before parameters")
        console.log()
        var parameters = [{"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "dealer", "value": $scope.dealer}, {"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}];


        $scope.$on('changeLanguage', function (event, data) {   $scope.filterchanged = "Language Changed";
            $scope.langcode = data;
            $scope.filterchange();
        });

        $scope.$on('changeyear', function (event, data) {   $scope.filterchanged = "Year Changed";
            $scope.year = data;
            $scope.biannual = "All";
            $scope.month = "All";
            $scope.filterchange();
        });

        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual = data;
            $scope.month = "All";
            $scope.filterchange();
        });

        $scope.$on('changemonth', function (event, data) {
            $scope.month = data;
            $scope.filterchange();
        });
        $scope.$on('changefactor', function (event, data) {
            $scope.factor = data;
            $scope.filterchange();
        });

        $scope.factorchanged = function (factor) {
            $scope.factor = factor;
//            $scope.svgname = factor.value;
//            alert($scope.factor + "In factor changed")
            $scope.filterchange();
        };
        //////////////////////////////////////////////////////////////



        $scope.graphs = function () {

            if ($scope.langcode == 'EN') {
                $scope.regfacttitle = "Factor Score";
//                $scope.byregiontitle = $rootScope.Titlearray[72].ContentEN;
//                $scope.meantitle = $rootScope.Titlearray[73].ContentEN;
//                $scope.percenttitle = $rootScope.Titlearray[74].ContentEN;
                $scope.factorscore = "Factor Score";
                $scope.attscore = 'Attribute Score';
//                $scope.footnote = $rootScope.Footarray[4].ContentEN;
//                $scope.footnote += "<br> " + $rootScope.Footarray[2].ContentEN;
//                $scope.footnote =  $rootScope.Footarray[0].ContentEN;
//                $rootScope.overallfootnote = angular.copy($scope.footnote);

//                $scope.footnotetitle = $rootScope.Footarray[4].ContentEN;
//                $scope.footnotetitle += "\n " + $rootScope.Footarray[2].ContentEN;
//                $scope.footnotetitle =  $rootScope.Footarray[0].ContentEN;
                $rootScope.overallfootnotetitle = angular.copy($scope.footnotetitle);
            } else {
                
                 $scope.regfacttitle = "Factor Score";

                $scope.factorscore = "Factor Score";
                $scope.attscore = 'Attribute Score';
//                $scope.regfacttitle = "ปัจจัยต่างๆ รายภูมิภาค";
//                $scope.byregiontitle = $rootScope.Titlearray[72].ContentReganal;
//                $scope.meantitle = $rootScope.Titlearray[73].ContentReganal;
//                $scope.percenttitle = $rootScope.Titlearray[74].ContentReganal;
//                $scope.factorscore = "คะแนนปัจจัย";
//                $scope.attscore = 'คุณลักษณะ คะแนน';
//                $scope.footnote = $rootScope.Footarray[4].ContentReganal;
//                $scope.footnote += "<br> " + $rootScope.Footarray[2].ContentReganal;
//                $scope.footnote += "<br> " + $rootScope.Footarray[0].ContentReganal;
//                $rootScope.overallfootnote = angular.copy($scope.footnote);

//                $scope.footnotetitle = $rootScope.Footarray[4].ContentReganal;
//                $scope.footnotetitle += "\n  " + $rootScope.Footarray[2].ContentReganal;
//                $scope.footnotetitle += "\n  " + $rootScope.Footarray[0].ContentReganal;
//                $rootScope.overallfootnotetitle = angular.copy($scope.footnotetitle);
            }


            service.mastersrv("RegionalDealer-getFactorAverage", parameters).then(function (response) {
                console.log('response.data.data', response.data.data);
                console.log('response.data', response.data.samplecount);
                $scope.svgname = response.data.data[0].Factor;
                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    console.log($scope.samplespaceless);
                }

                if (response.data.data.length == 0) {
                    response.data.data = [{"bartitle": "100%"}, {"Score": "0"}];
                }
                $scope.svgvalue = response.data.data[0].Score;
            });


            service.mastersrv("RegionalDealer-getRegionalFactorAverage", parameters).then(function (response) {
                $('#barlinecontainer').highcharts(service.regplainbar('column', color, response.data, $scope.svgname+'-'+$translate('regionalfactorscores'), 0, 60, .3, [0], 8, true));
                $('#barlinecontainer1').highcharts(service.regplainbar('column', color, response.data, $scope.svgname+'-'+$translate('regionalfactorscores'), 0, 60, .3, [0], 8, true));


            });


            service.mastersrv("RegionalDealer-getRegionalAttributeAverage", parameters).then(function (response) {
                $('#regbot1').highcharts(service.barplainbar('bar', ['#2979FF'], response.data, $scope.svgname+'-'+$translate('meanlabel'), 260, 35, 0, [0], 5, false, " "));
                $('#regbot11').highcharts(service.barplainbar('bar', ['#2979FF'], response.data, $scope.svgname+'-'+$translate('meanlabel'), 150, 35, 0, [0], 5, false, " "));
                $rootScope.legendforselectionb = response.data[0].scatter;
                $rootScope.legendforselectionw = response.data[0].scatter1;
            });


            service.mastersrv("RegionalDealer-getDelighted", parameters).then(function (response) {
                $('#regbot2').highcharts(service.barplainbar('bar', ['#2979FF'], response.data,  $scope.svgname+'-'+$translate('delightlabel'), 260, 35, 0, [0], 5, false, "%"));
                $('#regbot21').highcharts(service.barplainbar('bar', ['#2979FF'], response.data, $scope.svgname+'-'+$translate('delightlabel'), 150, 35, 0, [0], 5, false, "%"));

            });
        };

        $scope.isSemi = false;
        $scope.getStyle = function () {

            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '50%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': '36px'
            };
        };

        $scope.getStyle1 = function () {

            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '50%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': '36px'
            };
        };



        $scope.filterchange = function () {
//            alert($scope.factor + "In Filter Change")
            parameters = [{"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "dealer", "value": $scope.dealer}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "model", "value": $scope.model}, {"name": "factor", "value": $scope.factor}];
            $scope.graphs();
        };



    }
})();