(function () {
    'use strict';

    angular
            .module('app.module3.page2_1')
            .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
                .state('triangular.dealercsipage2', {
                    url: '/dealercsipage2',
                    views: {
                        '': {
                            templateUrl: 'app/module3/page2_1/page2_1.tmpl.html',
                            controller: 'm3page2_1Controller',
                            controllerAs: 'vm',

                        },
                        'belowContent': {
                            templateUrl: 'app/module3/page2_1/fab-button.tmpl.html',
                            controller: 'SalesFabController',
                            controllerAs: 'vm'
                        }

                    },
                    data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }

                });


//         triMenuProvider.addMenu({
//                    name: 'Loyality and Advocacy',
//                    icon: 'zmdi zmdi-car',
//                    type: 'link',
//                    priority: 2.0,
//                     state: 'triangular.dealercsipage2_1',
//                });
//             triMenuProvider.removeMenu('triangular.dealercsipage2_1');   


    }
})();