
(function () {
    'use strict';

    angular
            .module('app.module3.page2_1')
            .controller('m3page2_1Controller', m3page2_1Controller);

    /* @ngInject */
    function m3page2_1Controller($scope, service,$filter, $state, $http, $rootScope) {

        var vm = this;
        service.csi_filterfunction();
         $scope.$on("qsuccess",function(event,data){
                 $rootScope.checkboxInitiliser();
            });
        $rootScope.lockLeft = true;
        var color = ["#002776", "#92D400", "#00A1DE", "#72C7E7", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.hideyear = false;
        $rootScope.hidedgregion = true;
        $rootScope.hidebiannual = true;
        $rootScope.regionofdealerhide=false;
         $rootScope.regionofdealergrouphide=true;
        $rootScope.hidemonth = true;
        $rootScope.hidezone = true;
        $rootScope.hideregion = true;
        $rootScope.hidemodel = true;
        $rootScope.snaphidezone = true;
        $rootScope.snaphideregion = true;
        $rootScope.hidedealer = true;
        $rootScope.dealerhide = true;
        $rootScope.hidefactor = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.dealersnaphide = true
        $rootScope.filterchanged = "On Login";
        $rootScope.pagenumber = '11';
        $rootScope.starlabelhide = true;
        $rootScope.priorityhide = true;
        $rootScope.summaryRefresh = 1;
        $rootScope.modelsnap = "not";
        $scope.langcode = $rootScope.language;
        $scope.dealer = "Study Total";
        $rootScope.redfeedback = "not";
      
        $scope.dealer = $rootScope.dealer;
         $rootScope.hideyear2 = true;
          $rootScope.loyaltydealertitlehide = false;
        var $translate = $filter('translate');
           if ($scope.langcode == 'EN') {
             $rootScope.dealernameonloyal = $rootScope.dealer;
        } else {
           var  languagepara = [{"name": "english", "value": $rootScope.dealer}, {"name": "moduleType", "value": "CSI"}];
            service.mastersrv("Systemuserview-getName", languagepara).then(function (response) {
                
                   $rootScope.dealernameonloyal =  response.data[0].ContentReganal;
            });
        }
        $scope.changeloyalSwitch = 'Study Total';
        $rootScope.loyaltydealerhide = false;
        $rootScope.dealerpriorityhide = true;
        $rootScope.mainselectionhide = false;
        $rootScope.filterSelShow = true;
        $rootScope.periodhide = true;
        
        $scope.mainSelName=$translate('rightsidenavstudy')
        if ($scope.langcode == 'EN') {
//            $scope.loyal1=$rootScope.Titlearray[134].ContentEN+ " - Study total";
//            $scope.loyal2=$rootScope.Titlearray[135].ContentEN+ " - Study total"; 
//            $scope.loyal1 = $rootScope.Titlearray[134].ContentEN;
//            $scope.loyal2 = $rootScope.Titlearray[135].ContentEN;
//            $scope.footnote = $rootScope.Footarray[1].ContentEN;
        } else {
//            $scope.loyal1=$rootScope.Titlearray[134].ContentReganal+ " - ทั่วประเทศ";
//            $scope.loyal2=$rootScope.Titlearray[135].ContentReganal + " - ทั่วประเทศ";
//            $scope.loyal1 = $rootScope.Titlearray[134].ContentReganal;
//            $scope.loyal2 = $rootScope.Titlearray[135].ContentReganal;
//            $scope.footnote = $rootScope.Footarray[1].ContentReganal;
        }


        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        console.log(" height :", h);
        console.log(" width :", w);
        var margin = 300;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 250;
        } else if (w < 960 && w > 600) {
            margin = 200;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 150;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        console.log('margin', margin);
        //////////////////////////////////////////////////////////////

        $scope.$on('changeLanguage', function (event, data) {   $scope.filterchanged = "Language Changed";
            $scope.langcode = data;
            
            
             if($scope.changeloyalSwitch =='Region')
            {
                $scope.mainSelNameFuntion( $scope.region );
            }
            else
                if($scope.changeloyalSwitch =='Dealer')
            {
                $scope.mainSelNameFuntion(  $scope.dealer );
            }
            else
            
            {
                $scope.mainSelNameFuntion('Study Total');
                 
            }
              if ($scope.langcode == 'EN') {
                $rootScope.dealernameonloyal = $rootScope.dealer;
               
            } else
            {

                var languagepara = [{"name": "english", "value": $rootScope.dealer}, {"name": "moduleType", "value": "CSI"}];
                service.mastersrv("Systemuserview-getName", languagepara).then(function (response) {
                   
                        $rootScope.dealernameonloyal = response.data[0].ContentReganal;
                  
                });

            }
            $scope.filterchange();
        });


        $scope.$on('changeyear', function (event, data) {   $scope.filterchanged = "Year Changed";
            $scope.year = data;
            $scope.biannual = "Study Total";
            $scope.month = "Study Total";
            $scope.filterchange();
        });


        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual = data;
            $scope.month = "Study Total";
            $scope.filterchange();
        });

        $scope.$on('changemonth', function (event, data) {
            $scope.month = data;
            $scope.filterchange();
        });


        $scope.$on('changezone', function (event, data) {
            $scope.zone = data;
            $scope.region = "Study Total";
            $scope.filterchange();
        });

        $scope.$on('changeregion', function (event, data) {   $scope.filterchanged = "Region Changed";
            $scope.region = data;
            $scope.filterchange();
             $scope.mainSelNameFuntion( $scope.region );
        });

        $scope.$on('changemodel', function (event, data) {   $scope.filterchanged = "Model Changed";
            $scope.model = data;
            $scope.filterchange();
        });

        $scope.$on('changedealer', function (event, data) {   $scope.filterchanged = "Dealer Changed";
            $scope.dealer = data;
            $scope.filterchange();
            
             $scope.mainSelNameFuntion($scope.dealer );
        });


        $scope.$on('changeloyalSwitch', function (event, data) {
            $scope.changeloyalSwitch = data;
            $scope.filterchange();
            
            if($scope.changeloyalSwitch =='Region')
            {
                $scope.mainSelNameFuntion( $scope.region );
            }
            else
                if($scope.changeloyalSwitch =='Dealer')
            {
                $scope.mainSelNameFuntion($scope.dealer );
            }
            else
            
            {
                $scope.mainSelNameFuntion('Study Total');
                 
            }
            
        });


        //////////////////////////////////////////////////////////////


        var parameters = [];
       
        $scope.region = $rootScope.region;
        $scope.model = "All";
         $scope.year = $rootScope.year;
       
      
        parameters = [{"name": "changeloyalSwitch", "value": $scope.changeloyalSwitch}, {"name": "langCode", "value": $scope.langcode}, {"name": "year", "value": $scope.year}, {"name": "dealer", "value": $scope.dealer}];



        $scope.graphs = function () {

            if ($scope.langcode == 'EN') {
                $scope.loyal1 = "Would Recommend This Service Center to a Friend/ Relative";//$rootScope.Titlearray[134].ContentEN;
                $scope.loyal2 = "Would Use This Service Center to Service the Machine Again";//$rootScope.Titlearray[135].ContentEN; 
//                $scope.footnote = $rootScope.Footarray[0].ContentEN;
//                $scope.footnotetitle = $rootScope.Footarray[0].ContentEN;
            } else {
//                $scope.loyal1 = "จะแนะนำศูนย์บริการนี้ให้กับเพื่อนหรือญาติ";//$rootScope.Titlearray[134].ContentReganal;
//                $scope.loyal2 = "จะกลับมาใช้บริการที่ศูนย์บริการนี้อีกครั้ง";//$rootScope.Titlearray[135].ContentReganal;
////                $scope.footnote = $rootScope.Footarray[0].ContentReganal;
//                $scope.footnotetitle = $rootScope.Footarray[0].ContentReganal;
            }



//            LoyaltyAndAdvocacy-getLoyaltyAndAdvocacy
            service.mastersrv("LoyaltyAndAdvocacyDealer-getLoyaltyAndAdvocacy", parameters).then(function (response) {

               
                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    console.log($scope.samplespaceless);
                }


                var sampleCount = response.data.samplecount;
                $scope.astersik = '';
                if (sampleCount < 10) {
                     $scope.astersik = '**';
                } else if (sampleCount >= 10 && sampleCount < 30) {
                     $scope.astersik = '*'
                }


                if (response.data.data.length == 0) {
                    response.data.data = [{"bartitle": "100%"}, {"y": "0"}];
                }

                $scope.heading1 = response.data.data[0].chartheading;
                $('#top1').highcharts(service.loyalitybar($scope.heading1 , response.data.data[0].chartdata, false, color));

                $scope.heading2 = response.data.data[1].chartheading;
                $('#top2').highcharts(service.loyalitybar($scope.heading2 , response.data.data[1].chartdata, false, color));

                $scope.heading3 = response.data.data[2].chartheading;
                $('#bot1').highcharts(service.loyalitybar($scope.heading3 , response.data.data[2].chartdata, false, color));

//                $scope.heading4 = response.data.data[3].chartheading;
//                $('#bot2').highcharts(service.loyalitybar($scope.heading4 , response.data.data[3].chartdata, false, color));
//
//                $scope.heading5 = response.data.data[4].chartheading;
//                $('#bot3').highcharts(service.loyalitybar($scope.heading5, response.data.data[4].chartdata, false, color));
//          
            });

        };

        $scope.filterchange = function () {
            parameters = [{"name": "changeloyalSwitch", "value": $scope.changeloyalSwitch}, {"name": "langCode", "value": $scope.langcode}, {"name": "year", "value": $scope.year}, {"name": "dealer", "value": $scope.dealer}];
            $scope.graphs();
        };





 $scope.mainSelNameFuntion = function (name) {
     var languagepara = [{"name": "english", "value": name}, {"name": "moduleType", "value": "CSI"}];
           $scope.factorname = $scope.factor;
            
            service.mastersrv("Systemuserview-getName", languagepara).then(function (response) {
                if ($scope.langcode == 'EN') {
                    $scope.mainSelName = response.data[0].ContentEN;
                } else {
                    $scope.mainSelName= response.data[0].ContentReganal;
                }
            });
          
        };

    }
})();