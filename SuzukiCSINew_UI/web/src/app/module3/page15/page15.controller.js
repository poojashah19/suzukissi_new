(function () {
    'use strict';

    angular
            .module('app.module3.page15')
            .controller('m3page15Controller', m3page15Controller);

    /* @ngInject */
    function m3page15Controller($scope, service, $state, $http, $rootScope) {

        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776", "#92D400", "#00A1DE", "#72C7E7", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.hideyear = false;
        $rootScope.hidebiannual = false;
        $rootScope.hidemonth = false;
        $rootScope.hideregion = true;
        $rootScope.hidearea = true;
        $rootScope.hidedgregion = true;
        $rootScope.hidezone = true;
        $rootScope.hideprovince = true;
        $rootScope.hidecity = true;
        $rootScope.hidemodel = true;
        $rootScope.hidefactor = true;
        $rootScope.loyaltydealerhide = true;
//        $rootScope.dealerpriorityhide = false;
        $rootScope.dealerhide = true;
        $rootScope.filterchanged = "On Login";
        $rootScope.pagenumber = '11';
        $rootScope.starlabelhide = true;
        $rootScope.priorityhide = true;
        $rootScope.summaryRefresh = 1;
        $rootScope.dsnapshot = "not";
        $scope.Report_Month1 = "Report<br>Month";
        $scope.Dealer1 = "Dealer";
        $scope.Model1 = "Model";
        $scope.Job_Detail1 = "Job Detail";
        $scope.Response1 = "Response";
        $scope.Other_Feedback1 = "Other<br>Feedback";
        $scope.Overall_Satisfaction1 = "OSAT";
        $scope.zone = "All";
        $scope.year = $rootScope.Filterarray.year[0].name;
        $scope.biannual = "All";
        
        $scope.region = $rootScope.region;
        $rootScope.mainselectionhide = true;
        $rootScope.filterSelShow = true;
        $rootScope.periodhide = true;
         $rootScope.hideyear2 = true;

        var langparameter = [{"name": "langCode", "value": $rootScope.language}];
        service.mastersrv("Filters-getYear", langparameter).then(function (result) {
            if ($rootScope.language == 'EN') {
                var all = {"name": "All", "value": "All"};
            } else if ($rootScope.language == 'TH') {
                all = {"name": "All", "value": "全部"};
            }
            $rootScope.Filterarray.year = result.data;
            $rootScope.Filterarray.year.splice(0, 0, all);
        });


        $scope.dealer = $rootScope.dealer;
        $scope.langcode = $rootScope.language;
        $rootScope.dealerScoreDealer = "null";
        $rootScope.redfeedback = "redfeedback";
        $rootScope.tablename = "redalert";
        $rootScope.checkboxInitiliser();


        if ($scope.langcode == 'EN') {
            $scope.export = $rootScope.Titlearray[79].ContentEN;
            $scope.footer = "indicates small sample and is not considered for color rankings";
        } else {
            $scope.export = $rootScope.Titlearray[79].ContentReganal;
            $scope.footer = "บ่งชี้ว่ากลุ่มตัวอย่างขนาดเล็กและไม่ได้รับการพิจารณาสำหรับการจัดอันดับของสี การส่งออกตาราง";
        }


        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        console.log(" height :", h);
        console.log(" width :", w);
        var margin = 300;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 250;
        } else if (w < 960 && w > 600) {
            margin = 200;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 150;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        console.log('margin', margin);
        //////////////////////////////////////////////////////////////

        $scope.$on('changezone', function (event, data) {
            $scope.zone = data;
            $scope.region = "All";
            $scope.filterchange();
        });

        $scope.$on('changeregion', function (event, data) {   $scope.filterchanged = "Region Changed";
            $scope.region = data;
            $scope.filterchange();
        });

        $scope.$on('changeyear', function (event, data) {   $scope.filterchanged = "Year Changed";
            $scope.year = data;
            $scope.biannual = "All";
            $scope.month = "All";
            $scope.filterchange();
        });

        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual = data;
            $scope.month = "All";
            $scope.filterchange();
        });

        $scope.$on('changemonth', function (event, data) {
            $scope.month = data;
            $scope.filterchange();
        });



        //////////////////////////////////////////////////////////////

        var parameters = [];
        parameters = [{"name": "dealer", "value": $scope.dealer}, {"name": "month", "value": $scope.month}, {"name": "langCode", "value": $scope.langcode}, {"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "region", "value": encodeURIComponent($scope.region)}];
        service.csi_filterfunction();
        $scope.alert_dealer = function (dealer) {
            dealer = dealer.replace(/[*]/g, "");
            $rootScope.dealerScoreDealer = dealer;
            $state.go('triangular.csipage11', {dealer: ''});
            console.log('state2 params:', dealer);
        };


//        $("#exportCS").click(function () {
//                        $("#table2").table2excel({
//                            // exclude CSS class
//                            exclude: ".noExl",
//                            filename: "DealerScoresDownload"
//                        });
//                });


        function createData() {
            if ($scope.langcode == 'EN') {
                $scope.export = $rootScope.Titlearray[79].ContentEN;
                $scope.footer = "indicates small sample and is not considered for color rankings";
                $scope.Report_Month1 = "Report<br>Month";
                $scope.Zone1 = "Zone";
                $scope.Region1 = "Region";
                $scope.Dealer1 = "Dealer";
                $scope.Model1 = "Model";
                $scope.Job_Detail1 = "Job Detail";
                $scope.Response1 = "Response";
                $scope.mproduct = "Product";
                $scope.Other_Feedback1 = "Other<br>Feedback";
                $scope.Overall_Satisfaction1 = "OSAT";
                $scope.footnote = $rootScope.Footarray[1].ContentEN;  //0
//                $scope.footnote += "<br>" + $rootScope.Footarray[1].ContentEN;
                $scope.redfootnotetitle = $rootScope.Footarray[1].ContentEN;
//                $scope.redfootnotetitle += "\n" + $rootScope.Footarray[1].ContentEN;
            } else {
                $scope.export = $rootScope.Titlearray[79].ContentReganal;
                $scope.footer = "บ่งชี้ว่ากลุ่มตัวอย่างขนาดเล็กและไม่ได้รับการพิจารณาสำหรับการจัดอันดับของสี การส่งออกตาราง";
                $scope.Report_Month1 = "รอบการ<br>สำรวจ";
                $scope.Zone1 = "โซน";
                $scope.Region1 = "ภูมิภาค";
                $scope.Dealer1 = "ศูนย์บริการ";
                $scope.mproduct = "ประเภทเครื่องจักรกล";
                $scope.Model1 = "รุ่น";
                $scope.Job_Detail1 = "รายละเอียดงาน";
                $scope.Response1 = "ความคิดเห็นของลูกค้า";
                $scope.Other_Feedback1 = "ข้อเสนอแนะอื่นๆ";
                $scope.Overall_Satisfaction1 = "คะแนน<br>โดยรวม";
                $scope.footnote = $rootScope.Footarray[1].ContentReganal;
//                $scope.footnote += "<br>" + $rootScope.Footarray[1].ContentReganal;

                $scope.redfootnotetitle = $rootScope.Footarray[1].ContentReganal;
//                $scope.redfootnotetitle += "\n" + $rootScope.Footarray[1].ContentReganal;
            }
            service.mastersrv("CSIDealerScoreTable-getCSIRedalert", parameters).then(function (response) {

                $scope.records = response.data[0].values;
                console.log('$scope.salesData', vm.salesData)
                $scope.redalerttable = {
                    id: "redalert",
                    view: "datatable",
//                          height:495,
                    fixedRowHeight: false,
                    rowLineHeight: 20, rowHeight: 20,
                    scroll: 'y',
                    position: "flex",
                    select: "row",
                    hover: "myhover",
                    css: "my_style",

                    tooltip: true,
                    columns: [
                        {id: "Report_Month", css: "columnstyle", header: {text: $scope.Report_Month1, css: 'multiline'}, minWidth: 80, adjust: 'header'},
                        {id: "Dealer", css: "columnstyle", header: {text: $scope.Dealer1}, minWidth: 70, adjust: 'true'},
                        {id: "product", header: [$scope.mproduct], adjust: 'data'},
                        {id: "Model", header: [$scope.Model1], adjust: 'data'},

                        {id: "Response", header: [$scope.Response1], sort: "string", fillspace: true, minWidth: 175},
                        {id: "Other_Feedback", header: {text: $scope.Other_Feedback1, css: 'multiline'}, width: 155},
                        {id: "Overall_Satisfaction", header: {text: $scope.Overall_Satisfaction1, css: 'multiline'}, width: 70}
                    ],
                    data: $scope.records,

                    resizeColumn: true,

                    on: {
                        onAfterLoad: function () {
                            webix.delay(function () {
                                this.adjustRowHeight("Response", true);
                                this.render();
                            }, this);
//                  webix.delay(function(){
//                  	this.adjustRowHeight("Other_Feedback", true); 
//                    this.render();
//                  }, this);
                        },
//                onColumnResize:function(){
//                  	this.adjustRowHeight("Other_Feedback", true);
//                    this.render();
//                }

                    },

                };


            });

        }
        createData();


        $scope.exportdata = function () {
            webix.toExcel($$("redalert"), {
                filterHTML: true
            });
        }
        $scope.$on('changeLanguage', function (event, data) {   $scope.filterchanged = "Language Changed";
            $scope.langcode = data;
            $scope.filterchange();
        });


        $scope.filterchange = function () {
            console.log("filter change of dscore called");
            parameters = [{"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "month", "value": $scope.month}, {"name": "dealer", "value": $scope.dealer}, {"name": "langCode", "value": $scope.langcode}, {"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "region", "value": encodeURIComponent($scope.region)}];
            createData();
        };
    }
})();