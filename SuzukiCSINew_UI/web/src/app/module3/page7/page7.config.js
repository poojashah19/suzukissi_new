(function () {
    'use strict';

    angular
            .module('app.module3.page7')
            .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
                .state('triangular.dealercsipage7', {
                    url: '/dealercsipage7',

                    views: {
                        '': {
                            templateUrl: 'app/module3/page7/page7.tmpl.html',
                            controller: 'm3page7Controller',
                            controllerAs: 'vm',

                        },
                        'belowContent': {
                            templateUrl: 'app/module3/page7/fab-button.tmpl.html',
                            controller: 'SalesFabController',
                            controllerAs: 'vm'
                        }

                    },
                    data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
                });

//         triMenuProvider.addMenu({
//                   name: 'Trend Analysis',
//                    icon: 'zmdi zmdi-plus-square',
//                    type: 'link',
//                    priority: 1.2,
//                     state: 'triangular.dealercsipage7',
//                });
//             triMenuProvider.removeMenu('triangular.dealercsipage7');   


    }
})();