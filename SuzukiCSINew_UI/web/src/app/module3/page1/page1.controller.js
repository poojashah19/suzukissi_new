
(function () {
    'use strict';

    angular
            .module('app.module3.page1')
            .controller('m3page1Controller', m3page1Controller);

    /* @ngInject */
    function m3page1Controller($scope, service, $state, $http,$filter, $rootScope) {

        var vm = this;
          service.csi_filterfunction();
         $scope.$on("qsuccess",function(event,data){
                 $rootScope.checkboxInitiliser();
            });
        $rootScope.lockLeft = true;
        var color = ["#002776", "#92D400", "#00A1DE", "#f8ae19", "#72C7E7", "#3C8A2E", "#335291", "#A8DD33", "#33B4E5"];
        var $translate=$filter('translate');
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.filterchanged = "On Login";
        $rootScope.pagenumber = '4';
        $rootScope.starlabelhide = true;
        $rootScope.dealersnaphide = true;
        $rootScope.dealerpriority = true;
        $rootScope.hidedgregion = true;
        $rootScope.loyaltydealerhide = false;
        $rootScope.priorityhide = false;
        $rootScope.dealerpriorityhide = false;
        $rootScope.snaphidezone = true;
        $rootScope.snaphideregion = true;
        $rootScope.summaryRefresh = 1;
        $rootScope.redfeedback = "not";
       
        $scope.langcode = $rootScope.language;
        $scope.zone = "All";
        $scope.region = $rootScope.region;
        $scope.model = "All";
        $scope.year = $rootScope.year;
        $scope.biannual = "All";
        
        $scope.dealer1 = $rootScope.dealer;
        
        $scope.new = "new 1";
        $rootScope.mainselectionhide = true;
        $rootScope.filterSelShow = true;
        $rootScope.hidefactor = true;
        $rootScope.hideyear = false;
        $rootScope.hideyear2 = true;
        $rootScope.hidebiannual = false;
        $rootScope.hidemonth = false;
        $rootScope.periodhide = true;
        
        $rootScope.loyaltydealerhide = true;
        console.log('$rootScope.dealer', $rootScope.dealer);
        console.log('$rootScope.dealer', $rootScope.dealer)
        console.log('$rootScope.dealer', $rootScope.dealer)
        console.log('$rootScope.dealer', $rootScope.dealer)
        console.log('$rootScope.dealer', $rootScope.dealer)
        console.log('$rootScope.dealer', $rootScope.dealer)
        console.log('$rootScope.dealer', $rootScope.dealer)

        if ($scope.langcode == 'EN') {
//            $rootScope.choosecomp = $rootScope.Titlearray[40].ContentEN;
//            $rootScope.regcomp = $rootScope.Titlearray[41].ContentEN;
//            $rootScope.dealercomp = $rootScope.Titlearray[42].ContentEN;
//            $rootScope.reg1 = $rootScope.Titlearray[43].ContentEN;
//            $rootScope.reg2 = $rootScope.Titlearray[42].ContentEN;
//            $rootScope.dealer1 = $rootScope.Titlearray[45].ContentEN;
//            $rootScope.dealer2 = $rootScope.Titlearray[45].ContentEN;
//            $scope.footnote = $rootScope.Footarray[4].ContentEN;
            $scope.plotlineName = "Study Total";
//            $scope.footnote += "<br>" + $rootScope.Footarray[2].ContentEN;
//            $scope.footnote =  $rootScope.Footarray[0].ContentEN;
        } else {
//            $rootScope.choosecomp = $rootScope.Titlearray[40].ContentReganal;
//            $rootScope.regcomp = $rootScope.Titlearray[41].ContentReganal;
//            $rootScope.dealercomp = $rootScope.Titlearray[42].ContentReganal;
//            $rootScope.reg1 = $rootScope.Titlearray[43].ContentReganal;
//            $rootScope.reg2 = $rootScope.Titlearray[42].ContentReganal;
//            $rootScope.dealer1 = $rootScope.Titlearray[45].ContentReganal;
//            $rootScope.dealer2 = $rootScope.Titlearray[45].ContentReganal;
//            $scope.footnote = $rootScope.Footarray[4].ContentEN;
//            $scope.footnote += "<br>" + $rootScope.Footarray[2].ContentEN;
//            $scope.footnote =  $rootScope.Footarray[0].ContentEN;
            $scope.plotlineName = "ทั่วประเทศ";
        }





        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        console.log(" height :", h);
        console.log(" width :", w);
        var margin = 300;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 250;
        } else if (w < 960 && w > 600) {
            margin = 200;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 150;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        console.log('margin', margin);
        //////////////////////////////////////////////////////////////

        $scope.$on('changeLanguage', function (event, data) {   $scope.filterchanged = "Language Changed";
            $scope.langcode = data;
            $scope.filterchange();
        });




        $scope.$on('changeRadioSwitch', function (event, data) {
            $scope.default = 'dealer';
            $scope.filterchange();
        });



        $scope.$on('changeyear', function (event, data) {   $scope.filterchanged = "Year Changed";
            $scope.year = data;
            $scope.biannual = "All";
            $scope.month = "All";
            $scope.filterchange();
        });


        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual = data;
            $scope.month = "All";
            $scope.filterchange();
        });


        $scope.$on('changemonth', function (event, data) {
            $scope.month = data;
            $scope.filterchange();
        });


        //////////////////////////////////////////////////////////////
        var vm = this;

        $scope.samplespaceless = false;

        $scope.radioSwitch = "Dealer Comparison";
        $scope.default = "dealer";


        var parameters = [{"name": "measure", "value": $scope.default}, {"name": "langCode", "value": $scope.langcode}, {"name": "dealer1", "value": $rootScope.dealer}, {"name": "biannual", "value": $scope.biannual}, {"name": "year", "value": $scope.year}, {"name": "month", "value": $scope.month}];
      





        $scope.graphs = function () {
            $scope.new1 = "new 111";
            if ($scope.langcode == 'EN') {
//                $rootScope.choosecomp = $rootScope.Titlearray[40].ContentEN;
//                $rootScope.regcomp = $rootScope.Titlearray[41].ContentEN;
//                $rootScope.dealercomp = $rootScope.Titlearray[42].ContentEN;
//                $rootScope.reg1 = $rootScope.Titlearray[43].ContentEN;
//                $rootScope.reg2 = $rootScope.Titlearray[42].ContentEN;
//                $rootScope.dealer1 = $rootScope.Titlearray[45].ContentEN;
//                $rootScope.dealer2 = $rootScope.Titlearray[45].ContentEN;
//                $scope.footnote = $rootScope.Footarray[4].ContentEN;
//                $scope.footnote += "<br> " + $rootScope.Footarray[2].ContentEN;
//                $scope.footnote =  $rootScope.Footarray[0].ContentEN;
//                $rootScope.overallfootnote = angular.copy($scope.footnote);
//
//                $scope.footnotetitle = $rootScope.Footarray[4].ContentEN;
//                $scope.footnotetitle += "\n " + $rootScope.Footarray[2].ContentEN;
//                $scope.footnotetitle =  $rootScope.Footarray[0].ContentEN;
//                $rootScope.overallfootnotetitle = angular.copy($scope.footnotetitle);
            } else {
//                $rootScope.choosecomp = $rootScope.Titlearray[40].ContentReganal;
//                $rootScope.regcomp = $rootScope.Titlearray[41].ContentReganal;
//                $rootScope.dealercomp = $rootScope.Titlearray[42].ContentReganal;
//                $rootScope.reg1 = $rootScope.Titlearray[43].ContentReganal;
//                $rootScope.reg2 = $rootScope.Titlearray[42].ContentReganal;
//                $rootScope.dealer1 = $rootScope.Titlearray[45].ContentReganal;
//                $rootScope.dealer2 = $rootScope.Titlearray[45].ContentReganal;
//                $scope.footnote = $rootScope.Footarray[4].ContentReganal;
//                $scope.footnote += "<br> " + $rootScope.Footarray[2].ContentReganal;
//                $scope.footnote += "<br> " + $rootScope.Footarray[0].ContentReganal;
//                $rootScope.overallfootnote = angular.copy($scope.footnote);
//
//                $scope.footnotetitle = $rootScope.Footarray[4].ContentReganal;
//                $scope.footnotetitle += "\n  " + $rootScope.Footarray[2].ContentReganal;
//                $scope.footnotetitle += "\n  " + $rootScope.Footarray[0].ContentReganal;
//                $rootScope.overallfootnotetitle = angular.copy($scope.footnotetitle);
            }

//           $scope.dataaa=[{"data":[{"name":"Ease of arranging service visit","tooltip":"Ease of arranging service visit:<br>-4.4079","y":-4.4079},{"name":"Flexibility to accommodate schedule","tooltip":"Flexibility to accommodate schedule:<br>-3.6386","y":-3.6386},{"name":"Timeliness of hand over process","tooltip":"Timeliness of hand over process:<br>-2.4440","y":-2.444},{"name":"Courtesy of Service Advisor","tooltip":"Courtesy of Service Advisor:<br>-2.3680","y":-2.368},{"name":"Responsiveness of Service Advisor","tooltip":"Responsiveness of Service Advisor:<br>-4.0104","y":-4.0104},{"name":"Thoroughness of Service Advisor explanations","tooltip":"Thoroughness of Service Advisor explanations:<br>-4.5301","y":-4.5301},{"name":"Ease of driving in/ out of facility","tooltip":"Ease of driving in/ out of facility:<br>-9.7150","y":-9.715},{"name":"Convenience of location","tooltip":"Convenience of location:<br>-6.7154","y":-6.7154},{"name":"Cleanliness of dealership","tooltip":"Cleanliness of dealership:<br>-1.4656","y":-1.4656},{"name":"Comfort of waiting area","tooltip":"Comfort of waiting area:<br>1.5707","y":1.5707},{"name":"Timeliness of the pick–up process","tooltip":"Timeliness of the pick–up process:<br>-3.6359","y":-3.6359},{"name":"Fairness of the charges","tooltip":"Fairness of the charges:<br>-2.9744","y":-2.9744},{"name":"Helpfulness of staff at pick–up","tooltip":"Helpfulness of staff at pick–up:<br>-3.5375","y":-3.5375},{"name":"Total time required to service your vehicle","tooltip":"Total time required to service your vehicle:<br>-4.4619","y":-4.4619},{"name":"Thoroughness of maintenance/ repair work performed","tooltip":"Thoroughness of maintenance/ repair work performed:<br>-4.5898","y":-4.5898},{"name":"Condition/ cleanliness of vehicle on return","tooltip":"Condition/ cleanliness of vehicle on return:<br>-4.3523","y":-4.3523}],"name":"Autowork"},{"data":[{"name":"Ease of arranging service visit","tooltip":"Ease of arranging service visit:<br>10.6235","y":10.6235},{"name":"Flexibility to accommodate schedule","tooltip":"Flexibility to accommodate schedule:<br>8.5607","y":8.5607},{"name":"Timeliness of hand over process","tooltip":"Timeliness of hand over process:<br>8.8435","y":8.8435},{"name":"Courtesy of Service Advisor","tooltip":"Courtesy of Service Advisor:<br>7.3242","y":7.3242},{"name":"Responsiveness of Service Advisor","tooltip":"Responsiveness of Service Advisor:<br>7.7319","y":7.7319},{"name":"Thoroughness of Service Advisor explanations","tooltip":"Thoroughness of Service Advisor explanations:<br>9.3525","y":9.3525},{"name":"Ease of driving in/ out of facility","tooltip":"Ease of driving in/ out of facility:<br>10.2095","y":10.2095},{"name":"Convenience of location","tooltip":"Convenience of location:<br>8.9693","y":8.9693},{"name":"Cleanliness of dealership","tooltip":"Cleanliness of dealership:<br>6.7323","y":6.7323},{"name":"Comfort of waiting area","tooltip":"Comfort of waiting area:<br>9.2495","y":9.2495},{"name":"Timeliness of the pick–up process","tooltip":"Timeliness of the pick–up process:<br>8.7888","y":8.7888},{"name":"Fairness of the charges","tooltip":"Fairness of the charges:<br>9.2815","y":9.2815},{"name":"Helpfulness of staff at pick–up","tooltip":"Helpfulness of staff at pick–up:<br>7.3956","y":7.3956},{"name":"Total time required to service your vehicle","tooltip":"Total time required to service your vehicle:<br>8.4695","y":8.4695},{"name":"Thoroughness of maintenance/ repair work performed","tooltip":"Thoroughness of maintenance/ repair work performed:<br>8.7535","y":8.7535},{"name":"Condition/ cleanliness of vehicle on return","tooltip":"Condition/ cleanliness of vehicle on return:<br>7.8493","y":7.8493}],"name":"Study Best"}]
//           console.log($scope.dataaa)  ;
//           $('#mainchart').highcharts(service.verticalline(color,true,$scope.dataaa,'left','top',true,'-50','50','2'));
//                       $('#mainchart1').highcharts(service.verticalline(color,true,$scope.dataaa,'left','top',true,'-50','50','2'));

            service.mastersrv("PriorityDealer-getPriority", parameters).then(function (response) {
                console.log("response", response);
                console.log("responsedata", response.data);
                if (response.data.length == 0) {
                    response.data = [{"data": [{}]}];
                    console.log("response", response);
                    console.log("responsedata", response.data);
                }



                var maxaray = [];
                var minarray = [];
                $scope.data = response.data;


                for (var i = 0; i < $scope.data.length; i++) {
                    maxaray.push(Math.max.apply(Math, $scope.data[i].data.map(function (o) {
                        return o.y;
                    })));
                    minarray.push(Math.min.apply(Math, $scope.data[i].data.map(function (o) {
                        return o.y;
                    })))

                }
                var min = Math.min.apply(Math, minarray);

                var max = Math.max.apply(Math, maxaray);

                var roundminvalue = Math.floor(min / 5) * 5;
                var roundmaxvalue = Math.ceil(max / 5) * 5;
                if (roundminvalue < 0) {
                    var roundminvalue = roundminvalue * (-1);
                } else
                if (roundmaxvalue < 0)
                {
                    var roundmaxvalue = roundmaxvalue * (-1);
                }
                var finallastindex = Math.max(roundminvalue, roundmaxvalue);
                var finalfirstindex = finallastindex * (-1);
                
                 var label=$translate('prioritylabelstudytotal')
                $('#mainchart').highcharts(service.verticalline(color, true, response.data, 'left', 'top', true, finalfirstindex, finallastindex, '2', label));
                $('#mainchart1').highcharts(service.verticalline(color, true, response.data, 'left', 'top', true, finalfirstindex, finallastindex, '2', label));

            });

            $scope.new2 = "new 1222";
        };


        $scope.filterchange = function () {
            parameters = [{"name": "measure", "value": $scope.default}, {"name": "langCode", "value": $scope.langcode}, {"name": "dealer1", "value": $scope.dealer1}, {"name": "biannual", "value": $scope.biannual}, {"name": "year", "value": $scope.year}, {"name": "month", "value": $scope.month}];
            $scope.graphs();
        };



    }
})();