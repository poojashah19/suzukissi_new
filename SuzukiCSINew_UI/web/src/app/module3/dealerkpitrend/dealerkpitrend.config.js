(function () {
    'use strict';

    angular
            .module('app.module3.dealerkpitrend')
            .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
                .state('triangular.dealerkpitrend', {
                    url: '/dealerkpitrend',

                    views: {
                        '': {
                            templateUrl: 'app/module3/dealerkpitrend/dealerkpitrend.tmpl.html',
                            controller: 'dealerkpitrendController',
                            controllerAs: 'vm',

                        },
                        'belowContent': {
                            templateUrl: 'app/module3/dealerkpitrend/fab-button.tmpl.html',
                            controller: 'SalesFabController',
                            controllerAs: 'vm'
                        }

                    },
                    data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
                });

//           triMenuProvider.removeMenu('triangular.dealercsipage39'); 
//         triMenuProvider.addMenu({
//                   name: 'SOP Trend',
//                    icon: 'zmdi zmdi-hourglass',
//                    type: 'link',
//                    priority: 1.5,
//                     state: 'triangular.csipage9',
//                });  



    }
})();