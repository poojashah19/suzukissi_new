(function () {
    'use strict';

    angular
            .module('app.module3', [
                'app.module3.page1',
                'app.module3.page3',
                'app.module3.page4',
                'app.module3.page5',
                'app.module3.page2_1',
                'app.module3.page7',
                'app.module3.page8',
                'app.module3.page9',
                'app.module3.page10',
                'app.module3.page14',
                'app.module3.page15',
                'app.module3.dealerkpiperformance',
                 'app.module3.dealerkpitrend'
            ]);
})();