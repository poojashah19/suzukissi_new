(function () {
    'use strict';

    angular
            .module('app.module5.page7')
            .controller('m5page7Controller', m5page7Controller);

    /* @ngInject */
    function m5page7Controller($scope, $state, service, $filter,$http, $rootScope) {
        var vm = this;
        
               service.csi_filterfunction();
            $scope.$on("qsuccess",function(event,data){
                $rootScope.checkboxInitiliser();
            });
            
        
        $rootScope.lockLeft = true;
        var $translate = $filter('translate');
        var color = ["#002776", "#92D400", "#00A1DE", "#72C7E7", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.hideyear = true;
        $rootScope.hidebiannual = true;
        $rootScope.hidemonth = true;
        $rootScope.hidedgregion = true;
        $rootScope.hidezone = true;
        $rootScope.hideregion = true;
        $rootScope.hidemodel = true;
        $rootScope.snaphidezone = true;
        $rootScope.snaphideregion = true;
        $rootScope.hidefactor = false;
        $rootScope.hidetrendfactor = true;
        $rootScope.loyaltydealerhide = true;
        $rootScope.dealerpriorityhide = true;
        $rootScope.mainselectionhide = true;
        $rootScope.filterSelShow = false;
        $rootScope.dealersnaphide = true
        $rootScope.dealerhide = false;
        $rootScope.filterchanged = "On Login";
        $rootScope.pagenumber = '3';
        $rootScope.starlabelhide = false;
        $rootScope.legendname = "Study Best";
        $rootScope.legendnamew = "Worst Score";
        $rootScope.priorityhide = true;
        $rootScope.summaryRefresh = 1;
         $rootScope.hideyear2 = true;
        $scope.langcode = $rootScope.language;
        $rootScope.redfeedback = "not";
      
        $scope.dealer = $rootScope.dealer;
        $rootScope.periodhide = false;
        $scope.period = "Year";
        var parameters = [];
        var botparameters = [{"name": "dealer", "value": $scope.dealer},{"name": "dealergroup", "value": encodeURIComponent($rootScope.dealergroup)}];
        var languagepara = [];
        $scope.legendname = "Study Best";
        $scope.legendnamew = "Worst Score";
        $scope.factor =  $rootScope.factor;
        $scope.factorname = $rootScope.factor;
        $scope.zone = "All";
        $scope.region = $rootScope.region;
        $scope.model = "All";
        $scope.year = $rootScope.year;
        $scope.biannual = "All";
        $scope.month = "All";
        $scope.dealer="All";

        if ($scope.langcode == 'EN') {
//            $scope.trend1 = $rootScope.Titlearray[27].ContentEN;
//            $scope.trend2 = $rootScope.Titlearray[75].ContentEN;
//            $scope.trend3 = $rootScope.Titlearray[76].ContentEN;
//            $scope.footnote = $rootScope.Footarray[1].ContentEN;
            $scope.factorscore = "Factor Score";
            $scope.csiscore = 'csi Score';
        } else {
            
             $scope.factorscore = "Factor Score";
            $scope.csiscore = 'csi Score';
//            $scope.trend1 = $rootScope.Titlearray[27].ContentReganal;
//            $scope.trend2 = $rootScope.Titlearray[75].ContentReganal;
//            $scope.trend3 = $rootScope.Titlearray[76].ContentReganal;
//            $scope.footnote = $rootScope.Footarray[1].ContentReganal;
//            $scope.factorscore = "คะแนนปัจจัย";
//            $scope.csiscore = 'csi คะแนน';
        }


        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        console.log(" height :", h);
        console.log(" width :", w);
        var margin = 300;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 250;
        } else if (w < 960 && w > 600) {
            margin = 200;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 150;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        console.log('margin', margin);


        $scope.$on('changeLanguage', function (event, data) {   $scope.filterchanged = "Language Changed";
            $scope.langcode = data;
            languagepara = [{"name": "english", "value": $scope.factor}, {"name": "moduleType", "value": "CSI"}];
            service.mastersrv("Systemuserview-getName", languagepara).then(function (response) {
                console.log('response response[0]', response.data[0].ContentReganal);
                if ($scope.langcode == 'EN') {
                    $scope.factorname = response.data[0].ContentEN;
                } else {
                    $scope.factorname = response.data[0].ContentReganal;
                }
            });
            $scope.filterchange();
        });

        $scope.$on('changeyear', function (event, data) {   $scope.filterchanged = "Year Changed";
            $scope.year = data;
            $scope.biannual = "All";
            $scope.month = "All";
            $scope.filterchange();
        });

        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual = data;
            $scope.month = "All";
            $scope.filterchange();
        });

        $scope.$on('changemonth', function (event, data) {
            $scope.month = data;
            $scope.filterchange();
        });

        $scope.$on('changezone', function (event, data) {
            $scope.zone = data;
            $scope.region = "All";
            $scope.filterchange();
        });

        $scope.$on('changeregion', function (event, data) {   $scope.filterchanged = "Region Changed";
            $scope.region = data;
            $scope.filterchange();
        });

        $scope.$on('changemodel', function (event, data) {   $scope.filterchanged = "Model Changed";
            $scope.model = data;
            $scope.filterchange();
        });
 $scope.$on('changedealer', function (event, data) {   $scope.filterchanged = "Dealer Changed";
            $scope.dealer = data;
            $scope.filterchange();
        });
        
        $scope.$on('changefactor', function (event, data) {
            $scope.factor = data;
            languagepara = [{"name": "english", "value": $scope.factor}, {"name": "moduleType", "value": "CSI"}];
            service.mastersrv("Systemuserview-getName", languagepara).then(function (response) {
                console.log('response response[0]', response.data[0].ContentReganal);
                if ($scope.langcode == 'EN') {
                    $scope.factorname = response.data[0].ContentEN;
                } else {
                    $scope.factorname = response.data[0].ContentReganal;
                }

            });

            $scope.botgraph();
        });
        $scope.$on('changeperiod', function (event, data) {   $scope.filterchanged = "Period Changed";
            $scope.period = data;
            $scope.filterchange();
            if (data == 'year') {
                $scope.year = "All";
            }

        });
        //////////////////////////////////////////////////////////////



        parameters = [{"name": "period", "value": $scope.period}, {"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode},{"name": "dealer", "value": $scope.dealer},{"name": "dealergroup", "value": encodeURIComponent($rootScope.dealergroup)}];
      
        $scope.graphs = function () {

            if ($scope.langcode == 'EN') {
//                $scope.trend1 = $rootScope.Titlearray[27].ContentEN;
//                $scope.trend2 = $rootScope.Titlearray[75].ContentEN;
//                $scope.trend3 = $rootScope.Titlearray[76].ContentEN;
//                $scope.footnote = $rootScope.Footarray[4].ContentEN;
//                $scope.footnote += "<br> " + $rootScope.Footarray[19].ContentEN;
//                $scope.footnote += "<br> " + $rootScope.Footarray[2].ContentEN;
//                $scope.footnote =  $rootScope.Footarray[0].ContentEN;
//                $rootScope.overallfootnote = angular.copy($scope.footnote);
//
//                $scope.footnotetitle = $rootScope.Footarray[4].ContentEN;
//                $scope.footnotetitle += "\n " + $rootScope.Footarray[19].ContentEN;
//                $scope.footnotetitle += "\n " + $rootScope.Footarray[2].ContentEN;
//                $scope.footnotetitle =  $rootScope.Footarray[0].ContentEN;
//                $rootScope.overallfootnotetitle = angular.copy($scope.footnotetitle);
                $scope.factorscore = "Factor Score";
                $scope.csiscore = 'csi Score';
            } else {
                   $scope.factorscore = "Factor Score";
                $scope.csiscore = 'csi Score';
//                $scope.trend1 = $rootScope.Titlearray[27].ContentReganal;
//                $scope.trend2 = $rootScope.Titlearray[75].ContentReganal;
//                $scope.trend3 = $rootScope.Titlearray[76].ContentReganal;
//                $scope.footnote = $rootScope.Footarray[4].ContentReganal;
//                $scope.footnote += "<br> " + $rootScope.Footarray[19].ContentReganal;
//                $scope.footnote += "<br> " + $rootScope.Footarray[2].ContentReganal;
//                $scope.footnote += "<br> " + $rootScope.Footarray[0].ContentReganal;
//                $rootScope.overallfootnote = angular.copy($scope.footnote);
//
//                $scope.footnotetitle = $rootScope.Footarray[4].ContentReganal;
//                $scope.footnotetitle += "\n  " + $rootScope.Footarray[19].ContentReganal;
//                $scope.footnotetitle += "\n  " + $rootScope.Footarray[2].ContentReganal;
//                $scope.footnotetitle += "\n  " + $rootScope.Footarray[0].ContentReganal;
//                $rootScope.overallfootnotetitle = angular.copy($scope.footnotetitle);
//                $scope.factorscore = "คะแนนปัจจัย";
//                $scope.csiscore = ' คะแนน';
            }

            //  CSITrendAnalysis-getCSITrendByWave 1
            service.mastersrv("TrendAnalysis-getTrendByWave", parameters).then(function (response) {
                console.log('response.data', response.data);
                console.log('response.data.data', response.data.data);
                console.log('response.data.samplecount', response.data.samplecount);
                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    console.log($scope.samplespaceless);
                }

                $('#top1').highcharts(service.summaryplainbar('column', ['#2979FF'], response.data.data,$translate('summarycsiscore'), 0, 25, .3, [1, 1, 1, 1, 1, 1], 5, false));
                $('#top11').highcharts(service.summaryplainbar('column', ['#2979FF'], response.data.data, $translate('summarycsiscore'), 0, 25, .3, [1, 1, 1, 1, 1, 1], 5, false));
                $rootScope.legendname = response.data[0].scatter;
                $rootScope.legendnamew = response.data[0].scatter1;
            });

            //  TrendAnalysis-getFactorTrendByWave 2
            service.mastersrv("TrendAnalysis-getFactorTrendByWave", parameters).then(function (response) {
                $('#top2').highcharts(service.summaryplainbar('column', ['#7E57C2'], response.data, $scope.factorname+'-'+$translate('regionalfactorscores'), 0, 25, .3, [1, 1, 1, 1, 1, 1], 5, false));
                $('#top21').highcharts(service.summaryplainbar('column', ['#7E57C2'], response.data, $scope.factorname+'-'+$translate('regionalfactorscores'), 0, 25, .3, [1, 1, 1, 1, 1, 1], 5, false));
                $rootScope.legendname = response.data[0].scatter;
                $rootScope.legendnamew = response.data[0].scatter1;
            });

            //  TrendAnalysis-getAttributeTrendByWave 3
            service.mastersrv("TrendAnalysis-getAttributeTrendByWave", parameters).then(function (response) {
                $('#bot').highcharts(service.horizontalline(response.data, color));
                $('#bot1').highcharts(service.horizontalline(response.data, color));
            });
        };


        $scope.botrender = function () {
            //  TrendAnalysis-getFactorTrendByWave 2
            service.mastersrv("TrendAnalysis-getFactorTrendByWave", botparameters).then(function (response) {
                $('#top2').highcharts(service.summaryplainbar('column', ['#7E57C2'], response.data, $scope.factorname+'-'+$translate('regionalfactorscores'), 0, 25, .3, [1, 1, 1, 1, 1, 1], 5, false));
                $scope.legendname = response.data[0].scatter;
                $rootScope.legendnamew = response.data[0].scatter1;
            });

            //  TrendAnalysis-getAttributeTrendByWave 3
            service.mastersrv("TrendAnalysis-getAttributeTrendByWave", botparameters).then(function (response) {
                $('#bot').highcharts(service.horizontalline(response.data, color));
                $('#bot1').highcharts(service.horizontalline(response.data, color));
            });

        };

        $scope.filterchange = function () {
            parameters = [{"name": "period", "value": $scope.period},  {"name": "langCode", "value": $scope.langcode}, {"name": "dealer", "value": $scope.dealer},{"name": "dealergroup", "value": encodeURIComponent($rootScope.dealergroup)},  {"name": "factor", "value": $scope.factor}];
            $scope.graphs();
        };


        $scope.botgraph = function () {
            botparameters = [{"name": "period", "value": $scope.period},  {"name": "dealer", "value": $scope.dealer},{"name": "dealergroup", "value": encodeURIComponent($rootScope.dealergroup)}, {"name": "langCode", "value": $scope.langcode}, {"name": "factor", "value": $scope.factor}];
            $scope.botrender();
        };
    }
})();