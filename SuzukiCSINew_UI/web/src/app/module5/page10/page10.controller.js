(function () {
    'use strict';

    angular
            .module('app.module5.page10')
            .controller('m5page10Controller', m5page10Controller);

    /* @ngInject */
    function m5page10Controller($scope, $state, service, $http, $rootScope) {
        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776", "#92D400", "#00A1DE", "#72C7E7", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.hideyear = false;
        $rootScope.hidebiannual = true;
        $rootScope.hidemonth = true;
        $rootScope.hidezone = true;
        $rootScope.hideregion = true;
        $rootScope.hidemodel = true;
        $rootScope.dealerhide = false;
        $rootScope.hidedgregion = true;
        $rootScope.hidedealer = false;
        $rootScope.snaphidezone = true;
        $rootScope.snaphideregion = true;
        $rootScope.loyaltydealerhide = true;
        $rootScope.dealerpriority = true;
        $rootScope.dealerpriorityhide = false;
        $rootScope.mainselectionhide = false;
        $rootScope.filterSelShow = true;
        $rootScope.hidefactor = true;
        $rootScope.periodhide = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.dealersnaphide = true;
        $rootScope.hidetrendfactor = true;
        
        $rootScope.filterchanged = "On Login";
        $rootScope.pagenumber = '5';
        $rootScope.starlabelhide = true;
        $rootScope.priorityhide = true;
        $rootScope.summaryRefresh = 1;
        $scope.langcode = $rootScope.language;
        $rootScope.dsnapshot = "not";
        $rootScope.redfeedback = "not";
        
        $rootScope.checkboxInitiliser();
       
        $scope.dealer = "All";
        $scope.factor = "CSI";
        $rootScope.hideyear2 = true;
        $rootScope.periodhide = true;
        if ($scope.langcode == 'EN') {
//            $scope.soptitle1 = $rootScope.Titlearray[46].ContentEN;
//            $scope.soptitle2 = $rootScope.Titlearray[47].ContentEN;
//            $scope.soptitle3 = $rootScope.Titlearray[48].ContentEN;
            $scope.opt1 = "Total";
            $scope.opt2 = "Zone";
            $scope.opt3 = "Region";
//            $scope.sopfooter1 = $rootScope.Titlearray[50].ContentEN;
//            $scope.sopfooter2 = $rootScope.Titlearray[51].ContentEN;
//            $scope.sopfooter3 = $rootScope.Titlearray[52].ContentEN;
//            $scope.sopfooter4 = $rootScope.Titlearray[49].ContentEN;
//            $scope.footnote = $rootScope.Footarray[1].ContentEN;
            $scope.attscore = 'Selected Main Selection';
        } else {
//            $scope.soptitle1 = $rootScope.Titlearray[46].ContentReganal;
//            $scope.soptitle2 = $rootScope.Titlearray[47].ContentReganal;
//            $scope.soptitle3 = $rootScope.Titlearray[48].ContentReganal;
            $scope.opt1 = "全部";
            $scope.opt2 = "โซน";
            $scope.opt3 = "ภูมิภาค";
//            $scope.sopfooter1 = $rootScope.Titlearray[50].ContentReganal;
//            $scope.sopfooter2 = $rootScope.Titlearray[51].ContentReganal;
//            $scope.sopfooter3 = $rootScope.Titlearray[52].ContentReganal;
//            $scope.sopfooter4 = $rootScope.Titlearray[49].ContentReganal;
//            $scope.footnote = $rootScope.Footarray[1].ContentReganal;
            $scope.attscore = 'หัวข้อที่เลือก';
        }




        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        console.log(" height :", h);
        console.log(" width :", w);
        var margin = 320;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 230;
        } else if (w < 960 && w > 600) {
            margin = 180;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 160;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        console.log('margin', margin);

        $scope.$on('changeLanguage', function (event, data) {   $scope.filterchanged = "Language Changed";
            $scope.langcode = data;
            $scope.filterchange();
        });


        $scope.$on('changeyear', function (event, data) {   $scope.filterchanged = "Year Changed";
            $scope.year = data;
            $scope.biannual = "All";
            $scope.month = "All";
            $scope.filterchange();
        });

        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual = data;
            ;
            $scope.month = "All";
            $scope.filterchange();
        });

        $scope.$on('changemonth', function (event, data) {
            $scope.month = data;
            $scope.filterchange();
        });

        $scope.$on('changezone', function (event, data) {
            $scope.zone = data;
            $scope.region = "All";
            $scope.filterchange();
        });

        $scope.$on('changeregion', function (event, data) {   $scope.filterchanged = "Region Changed";
            $scope.region = data;
            $scope.filterchange();
        });

        $scope.$on('changedealer', function (event, data) {   $scope.filterchanged = "Dealer Changed";
            $scope.dealer = data;
            $scope.filterchange();
        });
        $scope.$on('changefactor', function (event, data) {
            $scope.factor = data;
            $scope.filterchange();
        });
        
        
        
        
        //////////////////////////////////////////////////////////////


        vm.sopwswitch = sopswitch;
        $scope.zone = "All";
        $scope.region = "All";
        $scope.dealer = $rootScope.dealer;
//        alert($scope.dealer+"In Page10")
        $scope.year = $rootScope.year;
        $scope.biannual = "All";
        
        $scope.sopswitchoutput = 'total';
        $scope.sopswitchoutputcaps = 'Total';
        var parameters = [{"name": "compareto", "value": $scope.sopswitchoutput},  {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year},{"name": "dealer", "value": $scope.dealer},{"name": "dealergroup", "value": encodeURIComponent($rootScope.dealergroup)}, {"name": "factor", "value": $scope.factor}];
        service.csi_filterfunction();
        $scope.group1 = "Total";

        function sopswitch(flag) {
            if (flag == 'Total') {
                $scope.sopswitchoutput = 'total';
                $scope.sopswitchoutputcaps = 'Total';
                 $scope.region = "All";
            } else if (flag == 'Zone') {
                $scope.sopswitchoutput = 'zone';
                $scope.sopswitchoutputcaps = 'Zone';
              
            } else if (flag == 'Region') {
                $scope.sopswitchoutput = 'region';
                $scope.sopswitchoutputcaps = 'Region';
                  $scope.region =$rootScope.region;
            }

            $scope.filterchange();
        }




        $scope.graphs = function () {
            $scope.hideloader = false;
//            $rootScope.dealerhide=false;
//            alert("dg:::::::::::::"+$rootScope.dealerhide);


            if ($scope.langcode == 'EN') {
//                $scope.soptitle1 = $rootScope.Titlearray[46].ContentEN;
//                $scope.soptitle2 = $rootScope.Titlearray[47].ContentEN;
//                $scope.soptitle = $rootScope.Titlearray[46].ContentEN;
//                $scope.soptitle += "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + $rootScope.Titlearray[47].ContentEN;

//                $scope.soptitle3 = $rootScope.Titlearray[48].ContentEN;
                $scope.opt1 = "Total";
                $scope.opt2 = "Zone";
                $scope.opt3 = "Region";
//                $scope.sopfooter1 = $rootScope.Titlearray[50].ContentEN;
//                $scope.sopfooter2 = $rootScope.Titlearray[51].ContentEN;
//                $scope.sopfooter3 = $rootScope.Titlearray[52].ContentEN;
//                $scope.sopfooter4 = $rootScope.Titlearray[49].ContentEN;
                $scope.sopfootnote = "+Based on those respondents who made an appointment for the service";
//                $scope.sopfootnote += "<br>" + $rootScope.Footarray[3].ContentEN;
//                $scope.sopfootnote =  $rootScope.Footarray[0].ContentEN;
//                $rootScope.sopfootnote = angular.copy($scope.sopfootnote);

                $scope.sopfootnotetitle = "+Based on those respondents who made an appointment for the service";
//                $scope.sopfootnotetitle += "\n" + $rootScope.Footarray[3].ContentEN;
//                $scope.sopfootnotetitle =  $rootScope.Footarray[0].ContentEN;
//                $rootScope.sopfootnotetitle = angular.copy($scope.sopfootnotetitle);
                $scope.attscore = 'Selected Main Selection';
            } else {
//                $scope.soptitle1 = $rootScope.Titlearray[46].ContentReganal;
////                $scope.soptitle2 = $rootScope.Titlearray[47].ContentReganal;
//                $scope.soptitle = $rootScope.Titlearray[46].ContentReganal;
//                $scope.soptitle += "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + $rootScope.Titlearray[47].ContentReganal;
//
//                $scope.soptitle3 = $rootScope.Titlearray[48].ContentReganal;
//                $scope.opt1 = "全部";
//                $scope.opt2 = "โซน";
//                $scope.opt3 = "ภูมิภาค";
//                $scope.sopfooter1 = $rootScope.Titlearray[50].ContentReganal;
//                $scope.sopfooter2 = $rootScope.Titlearray[51].ContentReganal;
//                $scope.sopfooter3 = $rootScope.Titlearray[52].ContentReganal;
//                $scope.sopfooter4 = $rootScope.Titlearray[49].ContentReganal;
//                $scope.sopfootnote = "+ข้อมูลที่แสดงนี้มาจากกลุ่มลูกค้าที่ทำการนัดหมายล่วงหน้า เพื่อขอรับบริการ";
//                $scope.sopfootnote += "<br>" + $rootScope.Footarray[3].ContentReganal;
//                $scope.sopfootnote = $rootScope.Footarray[0].ContentReganal;
//                $rootScope.sopfootnote = angular.copy($scope.sopfootnote);
//
//                $scope.sopfootnotetitle = "+ข้อมูลที่แสดงนี้มาจากกลุ่มลูกค้าที่ทำการนัดหมายล่วงหน้า เพื่อขอรับบริการ";
//                $scope.sopfootnotetitle += "\n" + $rootScope.Footarray[3].ContentReganal;
//                $scope.sopfootnotetitle =  $rootScope.Footarray[0].ContentReganal;
//                $rootScope.sopfootnotetitle = angular.copy($scope.sopfootnotetitle);
            }

            //        SOPPerformance-getSOPPerformance&compareto=region
            service.mastersrv("SOPPerformance-getSOPPerformance", parameters).then(function (response) {
                if (response.data) {
                    $scope.hideloader = true;
                }
                console.log('response.data', response.data);
                console.log('response.data.data', response.data.data);
                console.log('response.data.samplecount', response.data.samplecount);
                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    console.log($scope.samplespaceless);
                }
                if (response.data.data.length == 0) {
                    response.data.data = [{"bartitle": "100%"}, {"y": "0"}];
                }
//                    
//                          $('#sop').highcharts(service.sopplainbar('bar','11px',color,response.data.data,'Attribute Score',margin,14,0,[0],5,false));
//                        $('#sop1').highcharts(service.sopplainbar('bar','9px',color,response.data.data,'Attribute Score',margin,25,0,[0],5,false));

                $('#sop').highcharts(service.sopplainbar('bar', '12px', color, response.data.data, $scope.attscore, margin, 14, 0, [0], 5, false, ""));
                $('#sop1').highcharts(service.sopplainbar('bar', '9px', color, response.data.data, $scope.attscore, margin, 25, 0, [0], 5, false, ""));

            });
        };

        $scope.filterchange = function () {
//             $scope.switchdisable=false;
            parameters = [{"name": "compareto", "value": $scope.sopswitchoutput}, {"name": "langCode", "value": $scope.langcode},  {"name": "year", "value": $scope.year}, {"name": "dealer", "value": $scope.dealer},{"name": "dealergroup", "value": encodeURIComponent($rootScope.dealergroup)}, {"name": "factor", "value": $scope.factor},{"name": "region", "value": $scope.region}];
            $scope.graphs();
        };
    }
})();