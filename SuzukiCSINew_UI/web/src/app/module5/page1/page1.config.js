(function () {
    'use strict';

    angular
            .module('app.module5.page1')
            .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
                .state('triangular.dealergroupcsipage1', {
                    url: '/dealergroupcsipage1',
                    views: {
                        '': {
                            templateUrl: 'app/module5/page1/page1.tmpl.html',
                            controller: 'm5page1Controller',
                            controllerAs: 'vm',

                        },
                        'belowContent': {
                            templateUrl: 'app/module5/page1/fab-button.tmpl.html',
                            controller: 'SalesFabController',
                            controllerAs: 'vm'
                        }

                    },
                    data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }

                });

    }
})();