(function () {
    'use strict';
    angular
        .module('app.module5.dgkpitrend')
        .controller('dgkpitrendController', dgkpitrendController);
    /* @ngInject */
    function dgkpitrendController($scope, $state, service, $http, $rootScope) {
        var vm = this;
//        alert(11111);
        $rootScope.lockLeft = true;
        var color = ["#002776", "#92D400", "#00A1DE", "#72C7E7", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.hideyear = true;
        $rootScope.hidebiannual = true;
        $rootScope.hidemonth =true; 
        $rootScope.hidecity=false;    
        $rootScope.hidedgregion = true;
        $rootScope.studyAvg =true; 
        $rootScope.hidezone = false;
        $rootScope.hidequarter = false;
        $rootScope.hideregion = true;
        $rootScope.hideregiona = true;
        $rootScope.periodhide = true ;
        $rootScope.hidedealergroup = true;
        $rootScope.hidezone = true;
        $rootScope.snaphidezone = true;
        $rootScope.snaphideregion = true;
        $rootScope.hidemodel = true;
        
        $rootScope.hidefactor = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.dealerhide = false;
        $rootScope.hidestate = false;
        $rootScope.dealersnaphide = true;
        $rootScope.dsnapshot = "not";
        $rootScope.pagenumber = '6';
        $rootScope.starlabelhide = true;
        $rootScope.starlabelhidesummry = true;
        $rootScope.priorityhide = true;
        $rootScope.loyaltydealerhide = true;
        $rootScope.summaryRefresh = 1;
        $rootScope.hideyear2 = true;
        $scope.dealergroup=$rootScope.dealergroup;
        $scope.regiona="All";
        $rootScope.redalertdealerhide = true;
        
        $rootScope.redfeedback = "not";
        service.csi_filterfunction();
        $scope.$on('qsuccess',function(event){
            $rootScope.checkboxInitiliser();
        });
        $rootScope.mainselectionhide = false;
        $rootScope.filterSelShow = true;
        $rootScope.periodhide = true;
      
       
           
        
        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
        var margin = 300;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 250;
        } else if (w < 960 && w > 600) {
            margin = 200;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 150;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        $scope.$on('changeyear', function (event, data) {
            $scope.year = data;
            $scope.quarter = "All";
            $scope.month="All";
            $scope.dealer="All";
            $scope.dealergroup="All";
            $scope.region="All";
            $scope.regiona="All";
            $scope.city = "All";
            $scope.filterchange();
        });
        
        
        
         $scope.$on('changeLanguage', function (event, data) {   
             $scope.filterchanged = "Language Changed";
            $scope.langcode = data;
            
             var param = [{"name": "langCode", "value": $rootScope.language}];
             service.mastersrv("Filters-getSOPAttributes", param).then(function (result) {
                    $rootScope.Filterarray.sop = result.data;
                    console.log("KPI Attributes ::::::::", $rootScope.Filterarray.sop);
                    });
                 languagepara = [{"name": "english", "value": encodeURIComponent($scope.sop)}, {"name": "moduleType", "value": "CSI"}];
            service.mastersrv("Systemuserview-getName", languagepara).then(function (response) {
                console.log('response response[0]', response.data[0].ContentReganal);
                if ($scope.langcode == 'EN') {
                    $scope.sopname = response.data[0].ContentEN;
                } else {
                    $scope.sopname = response.data[0].ContentReganal;
                }
            });
                
            $scope.filterchange();
            
            
        });
        
        
        
         $scope.$on('changecity', function (event, data) {
            $scope.city = data;
            $scope.dealer = "All";
            //$scope.dealergroup="All";
            $scope.filterchange();
        });
        
        $scope.$on('dealergroupchanged', function (event, data) {
            $scope.dealergroup = data;
            $scope.dealer = "All";
            $scope.city="All";
            $scope.filterchange();
        });
        
        $scope.$on('changestate', function (event, data) {
            $scope.state = data;
            $scope.dealer = "All";
            $scope.city = "All";
            $scope.model = "All";
            $scope.regiona = $rootScope.regiona;
            $scope.filterchange();
            
        });

        $scope.$on('changequarter', function (event, data) {
            $scope.quarter = data;
            $scope.regiona = "All";
            $scope.region = "All";
            $scope.city = "All";
            $scope.dealer = "All";
            $scope.dealergroup="All";
            $scope.filterchange();
        });
   
        $scope.$on('changeregion', function (event, data) {
            $scope.region = data;
            $scope.dealer = "All";
             $scope.state = "All";
            $scope.city = "All";
            $scope.dealergroup = "All";
            $scope.filterchange();
        }); 
        $scope.$on('changeregiona', function (event, data) {
            $scope.regiona = data;
            $scope.dealer = "All";
             $scope.region = "All";
             $scope.city = "All";
            $scope.dealergroup = "All";
            $scope.filterchange();
        });
        $scope.$on('changedealer', function (event, data) {
            $scope.dealer = data;
            $scope.filterchange();
        });
        $scope.$on('changemodel', function (event, data) {
            $scope.model = data;
            $scope.filterchange();
        });
        
        
//        $scope.$on('changeperiod', function (event, data) {
//            $scope.period = data;
//         $scope.filterchange();
//            $scope.year = "All";
//             
//        });
        //////////////////////////////////////////////////////////////
        var languagepara = [];
        var parameters=[];
        
       $scope.$on('qsuccess2',function(event){
           
           vm.sopwswitch = sopswitch;
            $scope.opt1 = "Total";
            $scope.opt2 = "Zone";
            $scope.opt3 = "Region";
            $scope.graphname = 'Selected Main Selection';
            $rootScope.dealerScoreDealer = $rootScope.Filterarray.dealer[1].name;
            $scope.sopname = $rootScope.Filterarray.sop[0].value;
            $scope.langcode = $rootScope.language;
            $scope.zone = "All";
            $scope.region = "All";
            $scope.dealer = "All";
            $scope.year = "All";
            $scope.biannual = "All";
            $scope.regiona = "All";
            $scope.month = "All";
            $scope.city = "All";
            $scope.state = "All";
            $scope.dealergroup=$rootScope.dealergroup;
            $scope.factor = $rootScope.Filterarray.factor[0].name;
            $scope.sopswitchoutput = 'Total';//total
            $scope.sopswitchoutputcaps = 'Total';
            $scope.sop = $rootScope.Filterarray.sop[0].name;
            $scope.period = "year";
            parameters = [
                {"name": "year", "value": $scope.year},
                {"name": "period", "value": $scope.period}, 
                {"name": "compareto", "value": $scope.sopswitchoutput},
                {"name": "model", "value": $scope.model}, 
                {"name": "attribute", "value": encodeURIComponent($scope.sop)},
                {"name": "langCode", "value": $scope.langcode}, 
                {"name": "state", "value": $scope.state},
                {"name": "region", "value": encodeURIComponent($scope.region)},  
                {"name": "regiona", "value": $scope.regiona},
                {"name": "city", "value": encodeURIComponent($scope.city)},  
                {"name": "dealer", "value": $scope.dealer},  
                {"name": "dealergroup", "value": $scope.dealergroup}];
            $scope.graphs();
        });
        $scope.group1 = "Total";
        function sopswitch(flag) {
            if (flag == 'Total') {
                $scope.sopswitchoutput = 'total';
                $scope.sopswitchoutputcaps = 'Total';
            } 
            else if (flag == 'Region') {
                $scope.sopswitchoutput = 'region';
                $scope.sopswitchoutputcaps = 'Region';
            }
//            else if (flag == 'Zone') {
//                $scope.sopswitchoutput = 'zone';
//                $scope.sopswitchoutputcaps = 'Zone';
//            } 
            $scope.filterchange();
        }
        $scope.graphs = function () {
           
                $scope.opt1 = "Total";
                $scope.opt2 = "Zone";
                $scope.opt3 = "Region";
                $scope.sopfootnote = "+Based on those respondents who made an appointment for the service";
                $rootScope.sopfootnote = angular.copy($scope.sopfootnote);
                $scope.sopfootnotetitle = "+Based on those respondents who made an appointment for the service";
                $rootScope.sopfootnotetitle = angular.copy($scope.sopfootnotetitle);
                $scope.graphname = 'Selected Main Selection';
           
            service.mastersrv("SOPPerformance-getSOPTrend", parameters).then(function (response) {
                
                console.log("kpi trend resopn ::", response);
                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                } else {
                    $scope.samplespaceless = false;
                }
                if (response.data.data.length == 0) {
                    response.data.data = [{"bartitle": "100%"}, {"y": "0"}];
                }
                $('#mainchart').highcharts(service.barline(['#2196F3'], response.data.data, $scope.graphname, "%"));
            });
        };
        $scope.sopchanged = function (sop) {
            $scope.sop = sop;
            languagepara = [{"name": "english", "value": encodeURIComponent($scope.sop)}, {"name": "moduleType", "value": "CSI"}];
//            $scope.sopname = sop;
            
            languagepara = [{"name": "english", "value": encodeURIComponent($scope.sop)}, {"name": "moduleType", "value": "CSI"}];
            service.mastersrv("Systemuserview-getName", languagepara).then(function (response) {
                console.log('response response[0]', response.data[0].ContentReganal);
                if ($scope.langcode == 'EN') {
                    $scope.sopname = response.data[0].ContentEN;
                } else {
                    $scope.sopname = response.data[0].ContentReganal;
                }
            });
            $scope.filterchange();
        };
        $scope.filterchange = function () {
            var  chartsIds = ["mainchart"];
            service.destroyAllCharts(chartsIds);
            parameters = [
                {"name": "year", "value": $scope.year},
                {"name": "model", "value": $scope.model},
                {"name": "regiona", "value": $scope.regiona},
                {"name": "city", "value": encodeURIComponent($scope.city)},
                {"name": "period", "value": $scope.period},
                {"name": "state", "value": $scope.state},
                {"name": "compareto", "value": $scope.sopswitchoutput}, 
                {"name": "langCode", "value": $scope.langcode}, 
                {"name": "attribute", "value": encodeURIComponent($scope.sop)}, 
                {"name": "region", "value": encodeURIComponent($scope.region)}, 
                {"name": "dealer", "value": $scope.dealer},  
                {"name": "dealergroup", "value": $scope.dealergroup}];
            $scope.graphs();
        };
    }
})();