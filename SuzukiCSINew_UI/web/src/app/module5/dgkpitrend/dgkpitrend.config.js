(function () {
    'use strict';

    angular
            .module('app.module5.dgkpitrend')
            .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
                .state('triangular.dgkpitrend', {
                    url: '/dgkpitrend',

                    views: {
                        '': {
                            templateUrl: 'app/module5/dgkpitrend/dgkpitrend.tmpl.html',
                            controller: 'dgkpitrendController',
                            controllerAs: 'vm',

                        },
                        'belowContent': {
                            templateUrl: 'app/module5/dgkpitrend/fab-button.tmpl.html',
                            controller: 'SalesFabController',
                            controllerAs: 'vm'
                        }

                    },
                    data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
                });

//           triMenuProvider.removeMenu('triangular.dealercsipage39'); 
//         triMenuProvider.addMenu({
//                   name: 'SOP Trend',
//                    icon: 'zmdi zmdi-hourglass',
//                    type: 'link',
//                    priority: 1.5,
//                     state: 'triangular.csipage9',
//                });  



    }
})();