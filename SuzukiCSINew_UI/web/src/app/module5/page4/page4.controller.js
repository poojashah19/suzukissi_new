(function () {
    'use strict';

    angular
            .module('app.module5.page4')
            .controller('m5page4Controller', m5page4Controller);

    /* @ngInject */
    function m5page4Controller($scope, service, $http, $filter,$rootScope, $timeout) {
        var vm = this;
           if ($rootScope.summaryoffset) {
            $rootScope.summaryoffset = false;
        } else {
               service.csi_filterfunction();
            $scope.$on("qsuccess",function(event,data){
                $rootScope.checkboxInitiliser();
            });
            
        }
        
       
        $rootScope.langswitch = true;
        $rootScope.lockLeft = true;
         var $translate = $filter('translate');
        var color = ["#002776", "#92D400", "#00A1DE", "#72C7E7", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.hideyear = false;
        $rootScope.hidebiannual = true;
        $rootScope.hidemonth = true;
        $rootScope.hidezone = true;
        $rootScope.hideregion = true;
        $rootScope.hidemodel = true;
         $rootScope.hidedealergroup = true;
         $rootScope.hidedgregion = true;
        $rootScope.snaphidezone = true;
        $rootScope.snaphideregion = true;
        $rootScope.hidefactor = true;
        $rootScope.loyaltydealerhide = true;
        $rootScope.dealerpriorityhide = true;
        $rootScope.mainselectionhide = false;
        $rootScope.filterSelShow = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.redalertdealerhide = true;
        $rootScope.redfeedback = "not";
        $rootScope.hideyear2 = true;
        $scope.yearset = false;
        $scope.biset = false;
        $scope.monthset = true;
        $scope.show3 = true;
        $scope.show4 = false;
        
        var languagepara = [];
        $scope.factor = $rootScope.factor;
        $scope.svgname = $scope.factor;
//        $scope.factorname=$rootScope.Filterarray.factor[0].value;
        $rootScope.dealersnaphide = true;
        $rootScope.dealerhide = false;
        $rootScope.filterchanged = "On Login";
        $rootScope.pagenumber = '1';
        $rootScope.starlabelhide = false;
        $rootScope.legendname = "Study Best";
        $rootScope.legendnamew = "Worst Score";
        $rootScope.priorityhide = true;
        $scope.dealergroup =  $rootScope.dealergroup;
         $scope.dealer =   "All";
        $scope.langcode = $rootScope.language;
        $rootScope.modelsnap = "not";
        $scope.zone = "All";
        $scope.region = $rootScope.region;
        $rootScope.periodhide = true;
        $scope.dealername ="All";
        $scope.regionname = $scope.region;
        $scope.avg = "Average";
        $scope.model = "All";
        $scope.year = $rootScope.year;
        $scope.biannual = "All";
        
        $scope.legendname = "Study Best";
        $scope.legendnamew = "Worst Score";
        $scope.bestword = "Best";
        $rootScope.periodhide = true;
        //        $rootScope.dtwave=$scope.waves;
      
        $scope.summaryscope = 'region';
        $scope.summarymeasure = 'best';
        $scope.summaryscopecaps = 'Region';
        $scope.summarymeasurecaps = 'Best';

        var benchmarkparameters = [{"name": "benchmarkmeasure", "value": $scope.summarymeasure}, {"name": "langCode", "value": $scope.langcode},  {"name": "benchmarkscope", "value": $scope.summaryscope},{"name":"year","value":$scope.year},{"name": "region", "value": encodeURIComponent($scope.region)}];
        var benchmarkparameters2 = [{"name": "benchmarkmeasure", "value": $scope.summarymeasure}, {"name": "langCode", "value": $scope.langcode}, {"name": "benchmarkscope", "value": $scope.summaryscope},{"name": "year", "value": encodeURIComponent($scope.year)},{"name": "dealergroup", "value": encodeURIComponent($rootScope.dealergroup)}];
        

$scope.getStyles = function () {

            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '52%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '50%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': '36px'
            };
        };


        if ($scope.langcode == 'EN') {

            $scope.nationaltitle = "Dealer csi Score";
//            $scope.regionaltitle = $rootScope.Titlearray[2].ContentEN;
//            $scope.factortitle = $rootScope.Titlearray[3].ContentEN;
//            $scope.top5title = $rootScope.Titlearray[73].ContentEN;
//            $scope.delightedtitle = $rootScope.Titlearray[10].ContentEN;
//            $scope.footnote = $rootScope.Footarray[4].ContentEN;
//            $scope.footnote += "<br>" + $rootScope.Footarray[2].ContentEN;
//            $scope.footnote =  $rootScope.Footarray[0].ContentEN;
            $scope.factorscore = "Factor Score";
            $scope.svgname = "csi";
            $scope.avg = "Average";
            $scope.benchmarktitle = "csi Score - ";
            $scope.bestword = "Best";
            $scope.firstchart = "";

        } else {
              $scope.factorscore = "Factor Score";
            $scope.svgname = "csi";
            $scope.avg = "Average";
            $scope.benchmarktitle = "csi Score - ";
            $scope.bestword = "Best";
            $scope.firstchart = "";
//            $scope.nationaltitle = "คะแนน csi ของศูนย์บริการนี้";
//            $scope.regionaltitle = $rootScope.Titlearray[2].ContentReganal;
//            $scope.factortitle = $rootScope.Titlearray[3].ContentReganal;
//            $scope.top5title = $rootScope.Titlearray[73].ContentReganal;
//            $scope.delightedtitle = $rootScope.Titlearray[10].ContentReganal;
//            $scope.footnote = $rootScope.Footarray[4].ContentReganal;
//            $scope.footnote += "<br>" + $rootScope.Footarray[2].ContentReganal;
//            $scope.footnote += "<br>" + $rootScope.Footarray[0].ContentRegana;
//            $scope.factorscore = "คะแนนปัจจัย";
//            $scope.svgname = "ที่ปรึกษางานบริการ";
//            $scope.benchmarktitle = "คะแนน csi - ";
//            $scope.bestword = "ดีที่สุด";
//            $scope.avg = "เฉลี่ย";
            var regionpara1 = [{"name": "english", "value": encodeURIComponent($scope.region)}, {"name": "moduleType", "value": "CSI"}];
            service.mastersrv("Systemuserview-getName", regionpara1).then(function (response) {
                $scope.regionname = response.data[0].ContentReganal;
                $scope.firstchart = "หมายเหตุ: แสดงเฉพาะข้อมูลล่าสุดที่มี";
            });

        }




        $scope.$on('changesummaryscoreswitch', function (event, data) {
            $scope.summaryscope = data;
            $scope.benchmark();
        });

        $scope.$on('changesummarymeasureswitch', function (event, data) {
            $scope.summarymeasure = data;
            $scope.benchmark();
        });


        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        console.log(" height :", h);
        console.log(" width :", w);
        var margin = 300;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 250;
        } else if (w < 960 && w > 600) {
            margin = 200;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 150;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        console.log('margin', margin);
        //////////////////////////////////////////////////////////////

        $scope.$on('changeLanguage', function (event, data) {   $scope.filterchanged = "Language Changed";
            $scope.langcode = data;
            var dealerpara = [{"name": "english", "value": $scope.dealername}, {"name": "moduleType", "value": "CSI"}];
            var regionpara = [{"name": "english", "value": encodeURIComponent($scope.region)}, {"name": "moduleType", "value": "CSI"}];
            var languagepara = [{"name": "english", "value": $scope.factor}, {"name": "moduleType", "value": "CSI"}];
            service.mastersrv("Systemuserview-getName", dealerpara).then(function (response) {
                if ($scope.langcode == 'EN') {
                    $scope.dealername = response.data[0].ContentEN;
                } else {
                    $scope.dealername = response.data[0].ContentReganal;
                }
            });
            service.mastersrv("Systemuserview-getName", regionpara).then(function (response) {
                if ($scope.langcode == 'EN') {
                    $scope.regionname = response.data[0].ContentEN;
                } else {
                    $scope.regionname = response.data[0].ContentReganal;
                }
            });
            service.mastersrv("Systemuserview-getName", languagepara).then(function (response) {
                if ($scope.langcode == 'EN') {
                    $scope.svgname = response.data[0].ContentEN;
                } else {
                    $scope.svgname = response.data[0].ContentReganal;
                }
            });
             
            $scope.filterchange();
            $rootScope.languagechangernav();
        });


        $scope.$on('changeyear', function (event, data) {  
            $scope.filterchanged = "Year Changed";
            $scope.year = data;
            $scope.show4 = false;
            $scope.show3 = true;
            $scope.yearset = true;
            $scope.biset = false;
            $scope.monthset = false;
            $scope.biannual = "All";
            $scope.dealer = "All";
            $scope.filterchange();
            $scope.yearscore();
        });

        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual = data;
            if ($scope.biannual == "All" || $scope.biannual == "Study Total") {
                $scope.show4 = false;
                $scope.show3 = true;
                $scope.yearset = true;
                $scope.biset = false;
                $scope.monthset = false;
                $scope.yearscore();
            } else {
                $scope.show4 = false;
                $scope.show3 = true;
                $scope.yearset = false;
                $scope.biset = true;
                $scope.monthset = false;
                $scope.biannualscore();
            }

            $scope.month = "All";
            $scope.filterchange();
        });

        $scope.$on('changemonth', function (event, data) {
            $scope.month = data;
            if ($scope.month == "All") {
                $scope.show4 = false;
                $scope.show3 = true;
                $scope.yearset = true;
                $scope.biset = false;
                $scope.monthset = false;
                $scope.yearscore();
            } else {
                $scope.show4 = true;
                $scope.show3 = false;
                $scope.yearset = false;
                $scope.biset = false;
                $scope.monthset = true;
                $scope.monthscore();
            }
            $scope.filterchange();
        });

        $scope.$on('changezone', function (event, data) {
            $scope.zone = data;
            $scope.region = "All";
            $scope.filterchange();
        });

        $scope.$on('changeregion', function (event, data) {   $scope.filterchanged = "Region Changed";
            $scope.region = data;
            $scope.filterchange();
        });

        $scope.$on('changemodel', function (event, data) {   $scope.filterchanged = "Model Changed";
            $scope.model = data;
            $scope.filterchange();
        });
        $scope.$on('changedealer', function (event, data) {   
            $scope.filterchanged = "Dealer Changed";
            $scope.dealer = data;
            $scope.botchange();
        });


        $scope.$on('changefactor', function (event, data) {
            $scope.factor = data;
            languagepara = [{"name": "english", "value": $scope.factor}, {"name": "moduleType", "value": "CSI"}];
            service.mastersrv("Systemuserview-getName", languagepara).then(function (response) {
                console.log('response response[0]', response.data[0].ContentReganal);
                if ($scope.langcode == 'EN') {
                    $scope.svgname = response.data[0].ContentEN;
                } else {
                    $scope.svgname = response.data[0].ContentReganal;
                }

            });

            $scope.botchange();
        });

        //////////////////////////////////////////////////////////////



        $scope.benchmark = function () {
            benchmarkparameters = [{"name": "benchmarkmeasure", "value": $scope.summarymeasure}, {"name": "langCode", "value": $scope.langcode},{"name": "year", "value": encodeURIComponent($scope.year)},{"name":"region","value":$scope.region}];
            service.mastersrv("Summary-getNationalScoresYear", benchmarkparameters).then(function (response) {
                
                console.log(response.data);
                  $scope.benchmarkcurrmonth = response.data.data[0].Wave;
                $scope.benchmark1 = response.data.data[0].Score;
                $scope.benchmarkprevmonth = response.data.data[1].Wave;
                $scope.benchmark2 = response.data.data[1].Score;
                $scope.prevquater = response.data.data[2].Wave;
                $scope.benchmarkprevquater = response.data.data[2].Score;e;
            })
        }



        var parameters = [];
        var parametersk =[];

//        $rootScope.dealerScoreDealer = $rootScope.Filterarray.dealer[0];
        $rootScope.mapState = 0;

        parameters = [{"name": "langCode", "value": $scope.langcode},  {"name": "year", "value": $scope.year}, {"name": "dealer", "value": $scope.dealer}, {"name": "model", "value": $scope.model}, {"name": "factor", "value": $scope.factor},{"name": "dealergroup", "value": encodeURIComponent($rootScope.dealergroup)}];
         parametersk = [{"name": "langCode", "value": $scope.langcode},  {"name": "year", "value": $scope.year}, {"name": "dealer", "value": $scope.dealer}, {"name": "model", "value": $scope.model}, {"name": "factor", "value": $scope.factor},{"name": "dealergroup", "value": encodeURIComponent($rootScope.dealergroup)}];

        
        //service.csi_filterfunction();
        $scope.getStyle = function () {
            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '53%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': $scope.radius / 3.5 + 'px'
            };
        };
        $scope.getStyle1 = function () {
            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '53%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': $scope.radius / 3.5 + 'px'
            };
        };


        $scope.botgraph = function () {
            
            
             service.mastersrv("DealerSnapshot-getDealerSOPTopBottom", parametersk).then(function (response) {
                $('#top2').highcharts(service.multbarchartsplit(105, 'bar', response.data, 250, 15, 10, $scope.sopscore, 9, '6.2px', "%"));
                $('#top21').highcharts(service.multbarchartsplit(105, 'bar', response.data, 250, 15, 10, $scope.sopscore, 9, '6.2px', "%"));
            });
            
            
             service.mastersrv("Summary-getTop5Bottom5AttributesMean", parameters).then(function (response) {
                $timeout(function () {
                    $('#bot1').highcharts(service.multbarchartsplit(11, 'bar', response.data, 250, 15, 0, $translate('delightlabel'), 12, '8.0px', ""));
                    $('#bot11').highcharts(service.multbarchartsplit(11, 'bar', response.data, 250, 15, 0, $translate('delightlabel'), 12, '8.0px', ""));
                }, 1000)
            });
                  service.mastersrv("SummaryDealer-getSummaryRegionFactorScores", parameters).then(function (response) {
                $timeout(function () {
                    $('#barlinecontainer').highcharts(service.creditsummaryplainbar($scope.firstchart, 'column', ["#4292FF"], response.data,  $translate('regionalfactorscores'),  0, 45, .3, [1, 1, 1, 1, 1, 1], 5, false));
                    $('#barlinecontainer1').highcharts(service.creditsummaryplainbar($scope.firstchart, 'column', ["#4292FF"], response.data,  $translate('regionalfactorscores'),  0, 45, .3, [1, 1, 1, 1, 1, 1], 5, false));
                }, 1000)
                if ($scope.langcode == 'EN') {
                    $rootScope.legendname = response.data[0].scatter;
                    $rootScope.legendnamew = response.data[0].scatter1;
                } else {
                    $rootScope.legendname = response.data[0].scatter;
                    $rootScope.legendnamew = response.data[0].scatter1;
                }
                $rootScope.legendforselectionb = response.data[0].scatter;
                $rootScope.legendforselectionw = response.data[0].scatter1;
            });
             benchmarkparameters2 = [{"name": "benchmarkmeasure", "value": $scope.summarymeasure}, {"name": "langCode", "value": $scope.langcode}, {"name": "benchmarkscope", "value": $scope.summaryscope},{"name": "year", "value": encodeURIComponent($scope.year)},{"name": "dealergroup", "value": encodeURIComponent($rootScope.dealergroup)}, {"name": "dealer", "value": $scope.dealer}];
        
            service.mastersrv("Summary-getNationalScoresYear", benchmarkparameters2).then(function (response) {
               
                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                } else {
                    $scope.samplespaceless = false;
                }
                $scope.currmonth = response.data.data[0].Wave;
                $scope.svg1 = response.data.data[0].Score;
                $scope.prevmonth = response.data.data[1].Wave;
                $scope.svg2 = response.data.data[1].Score;
                $scope.prevquater = response.data.data[2].Wave;
                $scope.svg3 = response.data.data[2].Score;


            });
            
        }





        $scope.yearscore = function () {
            //Summary-getNationalScores
              benchmarkparameters2 = [{"name": "benchmarkmeasure", "value": $scope.summarymeasure}, {"name": "langCode", "value": $scope.langcode}, {"name": "benchmarkscope", "value": $scope.summaryscope},{"name": "year", "value": encodeURIComponent($scope.year)},{"name": "dealergroup", "value": encodeURIComponent($rootScope.dealergroup)}, {"name": "dealer", "value": $scope.dealer}];
        
            service.mastersrv("Summary-getNationalScoresYear", benchmarkparameters2).then(function (response) {
               
                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                } else {
                    $scope.samplespaceless = false;
                }
                $scope.currmonth = response.data.data[0].Wave;
                $scope.svg1 = response.data.data[0].Score;
                $scope.prevmonth = response.data.data[1].Wave;
                $scope.svg2 = response.data.data[1].Score;
                $scope.prevquater = response.data.data[2].Wave;
                $scope.svg3 = response.data.data[2].Score;


            });
            benchmarkparameters = [
                {"name": "benchmarkmeasure", "value": $scope.summarymeasure}, 
                {"name": "langCode", "value": $scope.langcode}, 
                {"name": "benchmarkscope", "value": $scope.summaryscope},
                {"name": "year", "value": encodeURIComponent($scope.year)},
                {"name": "region", "value": encodeURIComponent($scope.region)},
                 {"name": "dealergroup", "value": $rootScope.dealergroup}
            ];
            service.mastersrv("Summary-getNationalScoresYearBM", benchmarkparameters).then(function (response) {
                
                console.log(response.data);
                
                 $scope.benchmarkcurrmonth = response.data.data[0].Wave;
                $scope.benchmark1 = response.data.data[0].Score;
                $scope.benchmarkprevmonth = response.data.data[1].Wave;
                $scope.benchmark2 = response.data.data[1].Score;
                $scope.prevquater = response.data.data[2].Wave;
                $scope.benchmarkprevquater = response.data.data[2].Score;
                
        
           
            })


        };

        $scope.biannualscore = function () {
            //Summary-getNationalScores
            service.mastersrv("SummaryDealer-getNationalScoresBiannual", parameters).then(function (response) {

                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                } else {
                    $scope.samplespaceless = false;
                }
                $scope.currmonth = response.data.data[0].Wave;
                $scope.svg1 = response.data.data[0].Score;
                if (response.data.data.length > 1) {
                    $scope.prevmonth = response.data.data[1].Wave;
                    $scope.svg2 = response.data.data[1].Score;
                } else
                {
                    $scope.prevmonth = null;
                    $scope.svg2 = null;
                }
                if (response.data.data.length > 2) {
                    $scope.prevquater = response.data.data[2].Wave;
                    $scope.svg3 = response.data.data[2].Score;
                } else
                {
                    $scope.prevquater = null
                    $scope.svg3 = null;
                }

            });


            benchmarkparameters = [{"name": "benchmarkmeasure", "value": $scope.summarymeasure}, {"name": "langCode", "value": $scope.langcode}, {"name": "biannual", "value": $scope.biannual},  {"name": "year", "value": $scope.year}, {"name": "benchmarkscope", "value": $scope.summaryscope}, {"name": "dealer", "value": $scope.dealer}, {"name": "region", "value": encodeURIComponent($scope.region)}];
            service.mastersrv("SummaryDealer-getBenchmarkbiannual", benchmarkparameters).then(function (response) {
                console.log(response.data);
                $scope.benchmark1 = response.data[0].Score;
                $scope.benchmarkcurrmonth = response.data[0].Wave;
                $scope.benchmark2 = response.data[1].Score;
                $scope.benchmarkprevmonth = response.data[1].Wave;
                $scope.benchmark3 = response.data[2].Score;
                $scope.benchmarkprevquater = response.data[2].Wave;
            })
        };

        $scope.monthscore = function () {
            //Summary-getNationalScores
            service.mastersrv("SummaryDealer-getNationalScoresMonth", parameters).then(function (response) {

                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                } else {
                    $scope.samplespaceless = false;
                }
                $scope.currmonth = response.data.data[0].Wave;
                $scope.svg1 = response.data.data[0].Score;
                $scope.prevmonth = response.data.data[1].Wave;
                $scope.svg2 = response.data.data[1].Score;
                $scope.currquater = response.data.data[2].Wave;
                $scope.svg3 = response.data.data[2].Score;
                $scope.prevquater = response.data.data[3].Wave;
                $scope.svg4 = response.data.data[3].Score;


            });


           
        };




        $scope.graphs = function () {

            if ($scope.langcode == 'EN') {
//                $scope.nationaltitle = "Dealer  Score";
//                $scope.regionaltitle = $rootScope.Titlearray[2].ContentEN;
//                $scope.factortitle = $rootScope.Titlearray[3].ContentEN;
//                $scope.top5title = $rootScope.Titlearray[73].ContentEN;
//                $scope.delightedtitle = $rootScope.Titlearray[10].ContentEN;
//                $scope.footnote = $rootScope.Footarray[4].ContentEN;
//                $scope.footnote += "<br> " + $rootScope.Footarray[2].ContentEN;
//                $scope.footnote =  $rootScope.Footarray[0].ContentEN;
//                $rootScope.overallfootnote = angular.copy($scope.footnote);
//
//                $scope.footnotetitle = $rootScope.Footarray[4].ContentEN;
//                $scope.footnotetitle += "\n " + $rootScope.Footarray[2].ContentEN;
//                $scope.footnotetitle =  $rootScope.Footarray[0].ContentEN;
//                $rootScope.overallfootnotetitle = angular.copy($scope.footnotetitle);
                $scope.factorscore = "Factor Score";
                $scope.avg = "Average";
//                    $scope.svgname="csi";
                $scope.benchmarktitle = "csi Score - ";
                $scope.bestword = "Best";
                $scope.firstchart = "";

            } else {
                
                 $scope.factorscore = "Factor Score";
                $scope.avg = "Average";
//                    $scope.svgname="csi";
                $scope.benchmarktitle = "csi Score - ";
                $scope.bestword = "Best";
                $scope.firstchart = "";
//                $scope.nationaltitle = "คะแนน csi ของศูนย์บริการนี้";
//                $scope.regionaltitle = $rootScope.Titlearray[2].ContentReganal;
//                $scope.factortitle = $rootScope.Titlearray[3].ContentReganal;
//                $scope.top5title = $rootScope.Titlearray[73].ContentReganal;
//                $scope.delightedtitle = $rootScope.Titlearray[10].ContentReganal;
//                $scope.footnote = $rootScope.Footarray[4].ContentReganal;
//
//                $scope.footnote += "<br> " + $rootScope.Footarray[2].ContentReganal;
//                $scope.footnote += "<br> " + $rootScope.Footarray[0].ContentReganal;
//                $rootScope.overallfootnote = angular.copy($scope.footnote);
//
//                $scope.footnotetitle = $rootScope.Footarray[4].ContentReganal;
//                $scope.footnotetitle += "\n  " + $rootScope.Footarray[2].ContentReganal;
//                $scope.footnotetitle += "\n  " + $rootScope.Footarray[0].ContentReganal;
//                $rootScope.overallfootnotetitle = angular.copy($scope.footnotetitle);
//                $scope.factorscore = "คะแนนปัจจัย";
//                $scope.avg = "เฉลี่ย";
////                    $scope.svgname="csi";
//                $scope.benchmarktitle = "คะแนน csi - ";
//                $scope.bestword = "ดีที่สุด";
//                $scope.firstchart = "หมายเหตุ: แสดงเฉพาะข้อมูลล่าสุดที่มี";
            }
//        
//            $scope.benchmark();
//           service.mastersrv("csiSummaryDealer-getcsiBenchmark", benchmarkparameters).then(function (response) {
//                    $scope.benchmark1=response.data[0].Score;
//                    $scope.benchmarkcurrmonth = response.data[0].Wave;
//                    $scope.benchmark2=response.data[1].Score;
//                    $scope.benchmarkprevmonth = response.data[1].Wave;
//                    $scope.benchmark3=response.data[2].Score;
//                    $scope.benchmarkprevquater = response.data[2].Wave;
//            })




            service.mastersrv("SummaryDealer-getSummaryRegionFactorScores", parameters).then(function (response) {
                $timeout(function () {
                    $('#barlinecontainer').highcharts(service.creditsummaryplainbar($scope.firstchart, 'column', ["#4292FF"], response.data,  $translate('regionalfactorscores'),  0, 45, .3, [1, 1, 1, 1, 1, 1], 5, false));
                    $('#barlinecontainer1').highcharts(service.creditsummaryplainbar($scope.firstchart, 'column', ["#4292FF"], response.data,  $translate('regionalfactorscores'),  0, 45, .3, [1, 1, 1, 1, 1, 1], 5, false));
                }, 1000)
                if ($scope.langcode == 'EN') {
                    $rootScope.legendname = response.data[0].scatter;
                    $rootScope.legendnamew = response.data[0].scatter1;
                } else {
                    $rootScope.legendname = response.data[0].scatter;
                    $rootScope.legendnamew = response.data[0].scatter1;
                }
                $rootScope.legendforselectionb = response.data[0].scatter;
                $rootScope.legendforselectionw = response.data[0].scatter1;
            });


        

            //Summary-getTop5Bottom5SOP
            service.mastersrv("Summary-getTop5Bottom5AttributesMean", parameters).then(function (response) {
                $timeout(function () {
                    $('#bot1').highcharts(service.multbarchartsplit(11, 'bar', response.data, 250, 15, 0, $translate('delightlabel'), 12, '8.0px', ""));
                    $('#bot11').highcharts(service.multbarchartsplit(11, 'bar', response.data, 250, 15, 0, $translate('delightlabel'), 12, '8.0px', ""));
                }, 1000)
            });


            service.mastersrv("DealerSnapshot-getDealerSOPTopBottom", parametersk).then(function (response) {
                $('#top2').highcharts(service.multbarchartsplit(105, 'bar', response.data, 250, 15, 10, $scope.sopscore, 9, '6.2px', "%"));
                $('#top21').highcharts(service.multbarchartsplit(105, 'bar', response.data, 250, 15, 10, $scope.sopscore, 9, '6.2px', "%"));
            });
            
            
            service.mastersrv("Summary-getTop5Bottom5", parameters).then(function (response) {
                $timeout(function () {
                    $('#T5B5sop').highcharts(service.multbarchart(1005, 'bar', response.data, 275, 15, 0, $translate('sopscorelabel'), 12, '8.5px', ""));
                    $('#T5B5sop').highcharts(service.multbarchart(1005, 'bar', response.data, 275, 15, 0, $translate('sopscorelabel'), 12, '8.5px', ""));
                }, 1000);
               

            });




        };



        $scope.botchange = function () {
            parameters = [{"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "langCode", "value": $scope.langcode}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "dealer", "value": $scope.dealer},  {"name": "model", "value": $scope.model},{"name": "dealergroup", "value": encodeURIComponent($rootScope.dealergroup)}];
//            if ($scope.yearset == true && $scope.biset == false && $scope.monthset == false) {
            parametersk = [{"name": "langCode", "value": $scope.langcode},  {"name": "year", "value": $scope.year}, {"name": "dealer", "value": $scope.dealer}, {"name": "model", "value": $scope.model}, {"name": "factor", "value": $scope.factor},{"name": "dealergroup", "value": encodeURIComponent($rootScope.dealergroup)}];

            $scope.botgraph();
             
        };


        $scope.filterchange = function () {
             parametersk = [{"name": "langCode", "value": $scope.langcode},  {"name": "year", "value": $scope.year}, {"name": "dealer", "value": $scope.dealer}, {"name": "model", "value": $scope.model}, {"name": "factor", "value": $scope.factor},{"name": "dealergroup", "value": encodeURIComponent($rootScope.dealergroup)}];

            parameters = [{"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "biannual", "value": $scope.biannual}, {"name": "dealer", "value": $scope.dealer},  {"name": "model", "value": $scope.model}, {"name": "factor", "value": $scope.factor},{"name": "dealergroup", "value": encodeURIComponent($rootScope.dealergroup)}];
//            if ($scope.yearset == true && $scope.biset == false && $scope.monthset == false) {
//
//                $scope.yearscore();
//            }
            $scope.yearscore();

            $scope.graphs();
        };

    }
})();

