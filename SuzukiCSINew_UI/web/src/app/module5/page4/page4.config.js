(function () {
    'use strict';

    angular
            .module('app.module5.page4')
            .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
                .state('triangular.dealergroupcsipage4', {
                    url: '/dealergroupcsipage4',
                    views: {
                        '': {
                            templateUrl: 'app/module5/page4/page4.tmpl.html',
                            controller: 'm5page4Controller',
                            controllerAs: 'vm',

                        },
                        'belowContent': {
                            templateUrl: 'app/module5/page4/fab-button.tmpl.html',
                            controller: 'SalesFabController',
                            controllerAs: 'vm'
                        }

                    },
                    data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }

                });

//         triMenuProvider.addMenu({
//                    name: 'Summary',
//                    icon: 'zmdi zmdi-file-text',
//                    type: 'link',
//                    priority: 1.0,
//                    state: 'triangular.dealercsipage4',
//                });
//             triMenuProvider.removeMenu('triangular.dealercsipage4');   


    }
})();