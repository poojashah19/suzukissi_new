(function () {
    'use strict';

    angular
            .module('app.module5.page3')
            .controller('m5page3Controller', m5page3Controller);

    /* @ngInject */
    function m5page3Controller($scope, service, $state, $http, $filter,$rootScope, $timeout) {

        var vm = this;
        service.csi_filterfunction();
         $scope.$on("qsuccess",function(event,data){
              $rootScope.checkboxInitiliser();
             
         });
         var $translate = $filter('translate');
        $rootScope.lockLeft = true;
        var color = ["#002776", "#92D400", "#00A1DE", "#72C7E7", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.hideyear = false;
        $rootScope.hidebiannual = true;
        $rootScope.hidemonth = true;
        $rootScope.hidezone = true;
        $rootScope.regionofdealerhide=true;
         $rootScope.regionofdealergrouphide=false;
        $rootScope.hideregion = true;
        $rootScope.hidedgregion = true;
        $rootScope.snaphidezone = true;
        $rootScope.snaphideregion = true;
        $rootScope.hidemodel = true;
        $rootScope.hidefactor = true;
        $rootScope.loyaltydealerhide = false;
        $rootScope.dealerpriorityhide = true;
        $rootScope.mainselectionhide = false;
        $rootScope.filterSelShow = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.hidefactor = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.dealerhide = true;
        $rootScope.hideyear2 = true;
        $rootScope.loyaltydealertitlehide = false;
        $scope.changeloyalSwitch = 'Study Total';
        $scope.dealer1 = $rootScope.dealer;
        $scope.region1 = $rootScope.region;
        $rootScope.dealersnaphide = true;
        $rootScope.filterchanged = "On Login";
        $rootScope.pagenumber = '8';
        $rootScope.starlabelhide = false;
        $rootScope.priorityhide = true;
        $rootScope.summaryRefresh = 1;
        $rootScope.dsnapshot = "not";
        
        $scope.langcode = $rootScope.language;
        $rootScope.redfeedback = "not";
      
        $scope.region = "All";
        $scope.area = "All";
        $scope.zone = "All";
        $scope.province = "All";
        $scope.zone = "All";
        $scope.dealer = "All";
        $scope.region = $rootScope.region;
        $scope.model = "All";
        $scope.year = $rootScope.year;
        $scope.biannual = "All";
        
        $rootScope.legendforselectionb = "Best Overall";
        $rootScope.legendforselectionw = "Worst Overall";
        $rootScope.periodhide = true;
        $rootScope.dealernameonloyal = $translate('rightsidenavdg')
        if ($scope.langcode == 'EN') {
            console.log($rootScope.Titlearray)
//            $scope.export = $rootScope.Titlearray[79].ContentEN;
//            $scope.table0 = $rootScope.Titlearray[80].ContentEN;
//            $scope.table1 = $rootScope.Titlearray[81].ContentEN;
//            $scope.table3 = $rootScope.Titlearray[149].ContentEN;
////            $scope.table4 = $rootScope.Titlearray[84].ContentEN;
//            $scope.table5 = $rootScope.Titlearray[150].ContentEN;
//            $scope.table6 = $rootScope.Titlearray[151].ContentEN;
//            $scope.table7 = $rootScope.Titlearray[152].ContentEN;
//            $scope.table5 = $rootScope.Titlearray[85].ContentEN;
//            $scope.table6 = $rootScope.Titlearray[86].ContentEN;
//            $scope.table7 = $rootScope.Titlearray[87].ContentEN;
//            $scope.table8 = "SOP Implem- ented";
//            $scope.footnote = $rootScope.Footarray[0].ContentEN;
            $scope.footer = "indicates small sample and is not considered for color rankings";
            $rootScope.legendforselectionb = "Best Overall";
            $rootScope.legendforselectionw = "Worst Overall";
        } else {
//            $scope.export = $rootScope.Titlearray[79].ContentReganal;
//            $scope.table0 = $rootScope.Titlearray[80].ContentReganal;
//            $scope.table1 = $rootScope.Titlearray[81].ContentReganal;
//            $scope.table2 = $rootScope.Titlearray[82].ContentReganal;
//            $scope.table3 = $rootScope.Titlearray[149].ContentReganal;
////                $scope.table4 = $rootScope.Titlearray[84].ContentEN;
//            $scope.table5 = $rootScope.Titlearray[150].ContentReganal;
//            $scope.table6 = $rootScope.Titlearray[151].ContentReganal;
//            $scope.table7 = $rootScope.Titlearray[152].ContentReganal;
////            $scope.table8 = $rootScope.Titlearray[88].ContentReganal;
//            $scope.footnote = $rootScope.Footarray[0].ContentReganal;
            $scope.footer = "บ่งชี้ว่ากลุ่มตัวอย่างขนาดเล็กและไม่ได้รับการพิจารณาสำหรับการจัดอันดับของสี การส่งออกตาราง";
            $rootScope.legendforselectionb = "ดีที่สุดจากทั่วประเทศ";
            $rootScope.legendforselectionw = "แย่ที่สุดจากทั่วประเทศ";
        }

        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        console.log(" height :", h);
        console.log(" width :", w);
        var margin = 300;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 250;
        } else if (w < 960 && w > 600) {
            margin = 200;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 150;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        console.log('margin', margin);
        //////////////////////////////////////////////////////////////

        $scope.$on('changeLanguage', function (event, data) {   $scope.filterchanged = "Language Changed";
            $scope.langcode = data;
              $rootScope.dealernameonloyal = $translate('rightsidenavdg')
            $scope.showLoader = true;
            if ($scope.langcode == 'EN') {
                $rootScope.legendforselectionb = "Best Overall";
                $rootScope.legendforselectionw = "Worst Overall";
            } else {
                
                  $rootScope.legendforselectionb = "Best Overall";
                $rootScope.legendforselectionw = "Worst Overall";
//                $rootScope.legendforselectionb = "ดีที่สุดจากทั่วประเทศ";
//                $rootScope.legendforselectionw = "แย่ที่สุดจากทั่วประเทศ";
            }
            $scope.filterchange();
        });

        $scope.$on('changeyear', function (event, data) {   $scope.filterchanged = "Year Changed";
            $scope.year = data;
            $scope.biannual = "All";
            $scope.month = "All";
            $scope.showLoader = true;
            $scope.filterchange();
        });

        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual = data;
            $scope.month = "All";
            $scope.showLoader = true;
            $scope.filterchange();
        });

        $scope.$on('changemonth', function (event, data) {
            $scope.month = data;
            $scope.showLoader = true;
            $scope.filterchange();
        });

        $scope.$on('changezone', function (event, data) {
            $scope.zone = data;
            $scope.region = "All";
            $scope.dealer = "All";
            $scope.showLoader = true;
            $scope.filterchange();
        });

        $scope.$on('changeregion', function (event, data) {   $scope.filterchanged = "Region Changed";
            $scope.region = data;
            $scope.dealer = "All";
            $scope.showLoader = true;
            $scope.filterchange();
        });

        $scope.$on('changemodel', function (event, data) {   $scope.filterchanged = "Model Changed";
            $scope.model = data;
            $scope.showLoader = true;
            $scope.filterchange();
        });


        $scope.$on('changeloyalSwitch', function (event, data) {
            $scope.changeloyalSwitch = data;
            $rootScope.dealernameonloyal = $translate('rightsidenavdg')
            
            
            $scope.filterchange();
        });
        
 $scope.$on('changedealer', function (event, data) {   $scope.filterchanged = "Dealer Changed";
            $scope.dealer = data;
            $scope.filterchange();
        });

        //////////////////////////////////////////////////////////////


        $scope.finished = function () {
            $timeout(function () {
                $(document).ready(function () {
                    $("#fixTable").tableHeadFixer();
                });
            }, 0);

        };
        $scope.dealer = $rootScope.dealer;
        var parameters = [{"name": "langCode", "value": $scope.langcode}, {"name": "year", "value": $scope.year}, {"name": "loyalSwitch", "value": $scope.changeloyalSwitch}];
        
        $scope.alert_dealer = function (dealer) {
            dealer = dealer.replace(/[*]/g, "");
            $rootScope.dealerScoreDealer = dealer;
            $state.go('triangular.csipage11', {dealer: ''});
            console.log('state2 params:', dealer);
        }


        $("#exportCS").click(function () {
            $("#table2").table2excel({
                // exclude CSS class
                exclude: ".noExl",
                filename: "DealerScoresDownload"
            });
        });






        function createData() {
            if ($scope.langcode == 'EN') {
                console.log($rootScope.Titlearray)
//                $scope.export = $rootScope.Titlearray[79].ContentEN;
//                $scope.table0 = $rootScope.Titlearray[80].ContentEN;
//                $scope.table1 = $rootScope.Titlearray[81].ContentEN;
//                $scope.table2 = $rootScope.Titlearray[82].ContentEN;
//                $scope.table3 = $rootScope.Titlearray[149].ContentEN;
////                $scope.table4 = $rootScope.Titlearray[84].ContentEN;
//                $scope.table5 = $rootScope.Titlearray[150].ContentEN;
//                $scope.table6 = $rootScope.Titlearray[151].ContentEN;
//                $scope.table7 = $rootScope.Titlearray[152].ContentEN;
//                $scope.table5 = $rootScope.Titlearray[85].ContentEN;
//                $scope.table6 = $rootScope.Titlearray[86].ContentEN;
//                $scope.table7 = $rootScope.Titlearray[87].ContentEN;
//                $scope.table8 = "SOP Implem- ented";
//                $scope.footnote = $rootScope.Footarray[0].ContentEN;
//                $scope.footnotetitle = $rootScope.Footarray[0].ContentEN;
                $scope.footer = "indicates small sample and is not considered for color rankings";
                $scope.tableregion = 'Region';
                $rootScope.legendforselectionb = "Best Overall";
                $rootScope.legendforselectionw = "Worst Overall";

            } else {
//                $scope.export = $rootScope.Titlearray[79].ContentReganal;
//                $scope.table0 = $rootScope.Titlearray[80].ContentReganal;
//                $scope.table1 = $rootScope.Titlearray[81].ContentReganal;
//                $scope.table2 = $rootScope.Titlearray[82].ContentReganal;
//                $scope.table3 = $rootScope.Titlearray[149].ContentReganal;
////                $scope.table4 = $rootScope.Titlearray[84].ContentEN;
//                $scope.table5 = $rootScope.Titlearray[150].ContentReganal;
//                $scope.table6 = $rootScope.Titlearray[151].ContentReganal;
//                $scope.table7 = $rootScope.Titlearray[152].ContentReganal;
//                $scope.footnotetitle = $rootScope.Footarray[0].ContentReganal;
//                $scope.table5 = $rootScope.Titlearray[85].ContentReganal;
//                $scope.table6 = $rootScope.Titlearray[86].ContentReganal;
//                $scope.table7 = $rootScope.Titlearray[87].ContentReganal;
//                $scope.table8 = $rootScope.Titlearray[88].ContentReganal;
//                $scope.footnote = $rootScope.Footarray[0].ContentReganal;
//                $scope.table2 = $rootScope.Titlearray[82].ContentReganal;
//                $scope.table3 = $rootScope.Titlearray[83].ContentReganal;
//                $scope.table4 = $rootScope.Titlearray[84].ContentReganal;
//                $scope.table5 = $rootScope.Titlearray[85].ContentReganal;
//                $scope.table6 = $rootScope.Titlearray[86].ContentReganal;
//                $scope.table7 = $rootScope.Titlearray[87].ContentReganal;
//                $scope.table8 = $rootScope.Titlearray[88].ContentReganal;
//                $scope.footnote = $rootScope.Footarray[1].ContentReganal;
//                $scope.tableregion = 'ภูมิภาค';
//                $scope.footer = "บ่งชี้ว่ากลุ่มตัวอย่างขนาดเล็กและไม่ได้รับการพิจารณาสำหรับการจัดอันดับของสี การส่งออกตาราง";
//                $rootScope.legendforselectionb = "ดีที่สุดจากทั่วประเทศ";
//                $rootScope.legendforselectionw = "แย่ที่สุดจากทั่วประเทศ";

  $scope.footer = "indicates small sample and is not considered for color rankings";
                $scope.tableregion = 'Region';
                $rootScope.legendforselectionb = "Best Overall";
                $rootScope.legendforselectionw = "Worst Overall";

            }
var dealercolumns= [
                {id: "rank", css: "columnstyle", header: {text: $translate('dealerrankcsirank'), css: 'multiline'}, width: 50},
                {id: "region", css: "columnstyle", header: {text: $translate('rightsidenavregion')}, adjust: 'true', minWidth: 105},
                {id: "regioneng", css: "columnstyle", header: {text: $scope.tableregion}, adjust: 'true'},
                {id: "dealereng", css: "columnstyle", header: {text: 'Dealer'}, adjust: 'true'},
                {id: "dealer", css: "columnstyle", header: {text: $translate('rightsidenavdealer')}, minWidth: 100, fillspace: true, adjust: 'true', template: dealerclick},
                {id: "CSI", header: {text: $translate('dealerrankcsi')}, width: 80, cssFormat: mark_csi},
                {id: "Service Initiation", header: {text: $translate('dealerrankmlserviceinit'), css: 'multiline'}, cssFormat: mark_serviceinitiation, adjust: 'true'},
                {id: "Service Advisor", header: {text: $translate('dealerrankmlserviceadvisor'), css: 'multiline'}, cssFormat: mark_serviceadvisor, adjust: 'true'},
                {id: "Service Facility", header: {text: $translate('dealerrankmlservicefacility'), css: 'multiline'}, cssFormat: mark_servicefacility, adjust: 'true'},
                {id: "Vehicle Pickup", header: {text: $translate('dealerrankmlvehiclepickup'), css: 'multiline'}, cssFormat: mark_vehiclepickup, adjust: 'true'},
                {id: "Service Quality", header: {text: $translate('dealerrankmlservicequality'), css: 'multiline'}, cssFormat: mark_servicequality, adjust: 'true'},
                      //  {id: "sop", header: {text:$translate('dealerranksop')}, width: 100, cssFormat: mark_sop}
        ];

            var dealerclick = " <span class='downloadtext'>#dealer#</span>";
            $scope.dalerscorettable = {
                id: "dealertable",
                view: "datatable",
                headerRowHeight: 75,
                fixedRowHeight: false,
                rowLineHeight: 30,
                rowHeight: 30,
                scroll: 'y',
                position: "flex",
                css: "my_style",
                columns:dealercolumns,
                ready: function () {
                        $$("dealertable").hideProgress();
                        $$("dealertable").hideColumn("regioneng");
                        $$("dealertable").hideColumn("dealereng");
                    }

            };


            service.mastersrv("DealerScoreTableDealer-getDealerScoreTableDG", parameters).then(function (response) {
//              vm.salesData=response.data;
                $scope.records = response.data;
                console.log('response.data[0].counterfreeze;', response.data[0].counterfreeze);
                console.log('response.data[0].counterfreeze;', response.data[0].counterfreeze);
                console.log('response.data[0].counterfreeze;', response.data[0].counterfreeze);
                console.log('response.data[0].counterfreeze;', response.data[0].counterfreeze);
                $scope.countfreeze = response.data[0].counterfreeze;
                console.log($scope.records);
                $scope.dalerscorettable = {
                    id: "dealertable",
                    view: "datatable",
                    fixedRowHeight: false,
                    headerRowHeight: 75,
                    rowLineHeight: 30,
                    rowHeight: 30,
                    scroll: 'y',
                    position: "flex",
                    topSplit: $scope.countfreeze,
                    select: "row",
                    css: "my_style",
                    tooltip: true,
                    columns: dealercolumns,
                    data: $scope.records,
                    resizeColumn: false,
                    ready: function () {
                        $$("dealertable").hideProgress();
                        $$("dealertable").hideColumn("regioneng");
                        $$("dealertable").hideColumn("dealereng");
                    },
//                        on: {
//                        'onClick': function (e, id) {
//                            var datatable = $$("dealertable");
//                             var selectedId = datatable.getSelectedId()
//                              var row = datatable.getItem(selectedId);
//                             $scope.alert_dealer(row.dealereng)
//                        }
                    onClick: {
                        "downloadtext": function (e, id) {

                            $scope.dealer = this.getItem(id).dealereng;
                            $scope.region = this.getItem(id).region;
                            $scope.alert_dealer(this.getItem(id).dealereng);

                        }}



                };



            })

//            dealertable

        }



        $scope.doSome = function () {
            webix.extend($$("dealertable"), webix.ProgressBar);
            $$("dealertable").showProgress({
                type: "icon",
            });
        }
        createData();
function mark_csi(value, config) {
            if(config["CSIcolor"]!="White")
                {console.log({"background-color": config["CSIcolor"],"color": '#FFFFFF !important'});
                    
                    return {"background-color": config["CSIcolor"],"color": '#FFFFFF !important'}; 
                }
                else
                {
                     return {"background-color": config["CSIcolor"]}; 
                }
                return value;
           
        }

       function mark_serviceinitiation(value, config) {
            if (config["Service Initiationcolor"] != "White") {
                return {"background-color": config["Service Initiationcolor"], "color": '#FFFFFF !important'};
            } else {
                return {"background-color": config["Service Initiationcolor"]};
            }
            return value;
        }
        function mark_serviceadvisor(value, config) {
            if (config["Service Advisorcolor"] != "White") {
                return {"background-color": config["Service Advisorcolor"], "color": '#FFFFFF !important'};
            } else {
                return {"background-color": config["Service Advisorcolor"]};
            }
            return value;
        }
        function mark_servicefacility(value, config) {
            if (config["Service Facilitycolor"] != "White") {
                return {"background-color": config["Service Facilitycolor"], "color": '#FFFFFF !important'};
            } else {
                return {"background-color": config["Service Facilitycolor"]};
            }
            return value;
        }
        function mark_vehiclepickup(value, config) {
            if (config["Vehicle Pickupcolor"] != "White") {
                return {"background-color": config["Vehicle Pickupcolor"], "color": '#FFFFFF !important'};
            } else {
                return {"background-color": config["Vehicle Pickupcolor"]};
            }
            return value;
        }
        function mark_servicequality(value, config) {
            if (config["Service Qualitycolor"] != "White") {
                return {"background-color": config["Service Qualitycolor"], "color": '#FFFFFF !important'};
            } else {
                return {"background-color": config["Service Qualitycolor"]};
            }
            return value;
        }
        
        $scope.exportdata = function () {
            webix.toExcel($$("dealertable"), {
                filterHTML: true
            });
            //  webix.toExcel($$("dealertable"));
        }
        $scope.filterchange = function () {
            
            if ($scope.changeloyalSwitch == 'Study Total') {

                parameters = [{"name": "langCode", "value": $scope.langcode},{"name": "year", "value": $scope.year}, {"name": "loyalSwitch", "value": $scope.changeloyalSwitch}];

            } else
            if ($scope.changeloyalSwitch == 'Region') {

                parameters = [{"name": "langCode", "value": $scope.langcode},{"name": "dealergroup", "value": encodeURIComponent($rootScope.dealergroup)},{"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year}, {"name": "loyalSwitch", "value": $scope.changeloyalSwitch}];

            } else
            if ($scope.changeloyalSwitch == 'Dealer') {

                parameters = [{"name": "langCode", "value": $scope.langcode},{"name": "dealergroup", "value": encodeURIComponent($rootScope.dealergroup)}, {"name": "year", "value": $scope.year}, {"name": "loyalSwitch", "value": $scope.changeloyalSwitch}];
            }
           
               
            createData();
        }
    }
})();