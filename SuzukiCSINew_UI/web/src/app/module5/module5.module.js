(function () {
    'use strict';

    angular
            .module('app.module5', [
                'app.module5.page1',
                'app.module5.page3',
                'app.module5.page4',
                'app.module5.page5',
                'app.module5.page2_1',
                'app.module5.page7',
                'app.module5.page8',
                'app.module5.page9',
                'app.module5.page10',
                'app.module5.page14',
                'app.module5.page15',
                'app.module5.dgkpiperformance',
                'app.module5.dgkpitrend'
            ]);
})();