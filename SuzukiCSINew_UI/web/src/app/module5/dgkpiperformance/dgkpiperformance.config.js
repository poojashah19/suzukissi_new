(function () {
    'use strict';

    angular
            .module('app.module5.dgkpiperformance')
            .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
                .state('triangular.dgkpiperformance', {
                    url: '/dgkpiperformance',
                    views: {
                        '': {
                            templateUrl: 'app/module5/dgkpiperformance/dgkpiperformance.tmpl.html',
                            controller: 'dgkpiperformanceController',
                            controllerAs: 'vm'

                        },
                        'belowContent': {
                            templateUrl: 'app/module5/dgkpiperformance/fab-button.tmpl.html',
                            controller: 'SalesFabController',
                            controllerAs: 'vm'
                        }

                    },

                    data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
                });

    }
})();