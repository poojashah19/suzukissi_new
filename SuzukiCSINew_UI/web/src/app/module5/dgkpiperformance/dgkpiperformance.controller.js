(function () {
    'use strict';
    angular
        .module('app.module5.dgkpiperformance')
        .controller('dgkpiperformanceController', dgkpiperformanceController);
    /* @ngInject */
    function dgkpiperformanceController($scope, $state, service, $http, $rootScope) {
        var vm = this;
      
        $rootScope.lockLeft = true;
        var color = ["#002776", "#92D400", "#00A1DE", "#72C7E7", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.hideyear = false;
        $rootScope.hidebiannual = true;
        $rootScope.hidemonth =true; 
        $rootScope.hidecity=true;    
        $rootScope.studyAvg =true; 
        $rootScope.hidedgregion = true;
        $rootScope.hidezone = true;
        $rootScope.hideregion = true;
        $rootScope.dealerhide = false;
        $rootScope.hidestate = false;
        $rootScope.hidezone = true;
        $rootScope.hidemodel = true;
        $rootScope.hideregiona = true;
        $rootScope.hidefactor = true;
        $rootScope.snaphidezone = true;
        $rootScope.snaphideregion = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.hidedealergroup=true;
        $rootScope.hidequarter=false;
        $rootScope.dealersnaphide = true;
        $rootScope.redalertdealerhide = true;
        $rootScope.pagenumber = '5';
        $rootScope.starlabelhide = true;
        $rootScope.starlabelhidesummry = true;
        $rootScope.priorityhide = true;
        $rootScope.summaryRefresh = 1;
        $scope.langcode = $rootScope.language;
        $rootScope.dsnapshot = "not";
        $scope.dealer = "All";
        $scope.dealergroup= $rootScope.dealergroup;
        $rootScope.redfeedback = "not";
        service.csi_filterfunction();
         $scope.$on('qsuccess',function(event){
            $rootScope.checkboxInitiliser();
        });
        
        $rootScope.mainselectionhide = false;
        $rootScope.filterSelShow = true;
        $rootScope.periodhide = true;
        $rootScope.loyaltydealerhide = true;
       
        $rootScope.hideyear2 = true;
        
        
        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
        var margin = 370;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 260;
        } else if (w < 960 && w > 600) {
            margin = 200;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 180;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
    
         $scope.$on('changeLanguage', function (event, data) {   
             $scope.filterchanged = "Language Changed";
            $scope.langcode = data;
            $scope.filterchange();
            
            
        });
        
        $scope.$on('changeyear', function (event, data) {
            $scope.year = data;
            $scope.quarter = "All";
            $scope.month="All";
            $scope.dealer="All";
            $scope.dealergroup="All";
            $scope.region="All";
            $scope.regiona="All";
            $scope.city = "All";
            $scope.filterchange();
        });
        $scope.$on('changestate', function (event, data) {
            $scope.state = data;
            $scope.dealer = "All";
            $scope.city = "All";
            $scope.model = "All";
            $scope.regiona = $rootScope.regiona;
            $scope.filterchange();
            
        });
   
        $scope.$on('changequarter', function (event, data) {
            $scope.quarter = data;
            $scope.month="All";
            $scope.dealer="All";
            $scope.dealergroup="All";
            $scope.region="All";
            $scope.regiona="All";
            $scope.city = "All";
            $scope.filterchange();
        });
        $scope.$on('changemonth', function (event, data) {
            $scope.month = data;
            $scope.dealer="All";
            $scope.dealergroup="All";
           $scope.city = "All";
            $scope.region="All";
            $scope.regiona="All";
            $scope.filterchange();
        });
     
        $scope.$on('changeregion', function (event, data) {
            $scope.region = data;
            $scope.dealer = "All";
            $scope.state = "All";
            $scope.dealergroup = "All";
            $scope.city = "All";
            $scope.filterchange();
        });
         $scope.$on('changecity', function (event, data) {
            $scope.city = data;
            $scope.dealer = "All";
              $scope.state = "All";
           // $scope.dealergroup="All";
            $scope.filterchange();
        });
        $scope.$on('changeregiona', function (event, data) {
            $scope.regiona = data;
            $scope.dealer = "All";
            $scope.region="All";
            $scope.dealergroup = "All";
            $scope.city = "All";
            $scope.filterchange();
        });
        $scope.$on('dealergroupchanged', function (event, data) {
            $scope.dealergroup = data;
            $scope.dealer = "All";
            $scope.city="All";
            $scope.filterchange();
        });
        $scope.$on('changedealer', function (event, data) {
            $scope.dealer = data;
            $scope.filterchange();
        });
        $scope.$on('changemodel', function (event, data) {
            $scope.model = data;
            $scope.filterchange();
        });
        //////////////////////////////////////////////////////////////
        vm.sopwswitch = sopswitch;
      
        var parameters=[];
        $scope.$on('qsuccess2',function(event){
            $scope.dealergroup = $rootScope.dealergroup;
            $scope.month = "All";
            $scope.opt1 = "Total";
            $scope.opt2 = "Zone";
            $scope.opt3 = "Region";
            $scope.attscore = 'Selected Main Selection';
            $scope.zone = "All";
            $scope.region = "All";
            $scope.state ="All";
            $scope.dealer = "All";
             $scope.langcode = $rootScope.language;
            $scope.year = $rootScope.year;
            $scope.biannual = "All";
            $scope.regiona = "All";
           
            $scope.sopswitchoutput = 'total';
            $scope.sopswitchoutputcaps = 'Total';
            $scope.city = "All";
           parameters = [
               {"name": "month", "value": $scope.month},
               {"name": "model", "value": $scope.model},
               {"name": "compareto", "value": $scope.sopswitchoutput},
               {"name": "regiona", "value": $scope.regiona},
                {"name": "state", "value": $scope.state},
               {"name": "city", "value": encodeURIComponent($scope.city)}, 
               {"name": "region", "value": encodeURIComponent($scope.region)}, 
               {"name": "year", "value": $scope.year},  
                {"name": "langCode", "value": $scope.langcode}, 
               {"name": "dealer", "value": $scope.dealer},  
               {"name": "dealergroup", "value": $scope.dealergroup}];
          
            $scope.graphs();
        });
        $scope.group1 = "Total";
        function sopswitch(flag) {
            if (flag == 'Total') {
                $scope.sopswitchoutput = 'total';
                $scope.sopswitchoutputcaps = 'Total';
            } else if (flag == 'Region') {
                $scope.sopswitchoutput = 'region';
                $scope.sopswitchoutputcaps = 'Region';
            }
            $scope.filterchange();
        }
        $scope.graphs = function () {
           
            $scope.hideloader = false;
            
                $scope.opt1 = "Total";
                //$scope.opt2 = "Zone";
                $scope.opt3 = "Region";
                $scope.sopfootnote = "+Based on those respondents who made an appointment for the service";
                $rootScope.sopfootnote = angular.copy($scope.sopfootnote);  
                $scope.sopfootnotetitle = "+Based on those respondents who made an appointment for the service";
                $rootScope.sopfootnotetitle = angular.copy($scope.sopfootnotetitle);
                $scope.attscore = 'Selected Main Selection';
            
            //dgkpiperformance-getdgkpiperformance&compareto=region
            service.mastersrv("SOPPerformance-getSOPPerformance", parameters).then(function (response) {
               
//               console.log("kpi perf resopn ::", response);
                if (response.data) {
                    $scope.hideloader = true;
                }
                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    console.log($scope.samplespaceless);
                }

               
                $('#sop').highcharts(service.kpiperforamnce('bar', ['#FFFFFF', '#2979FF'], response.data, $scope.attscore, margin, 35, 0, [0], 5, false, "%"," "));
                $('#sop1').highcharts(service.kpiplainbar('bar', '9px', color, response.data, $scope.attscore, margin, 25, 0, [0], 5, false, ""));
                
//                $('#sop').highcharts(service.sopplainbar('bar', '11px', color, response.data.data, $scope.attscore, margin, 14, 0, [0], 5, false, ""));
//                $('#sop1').highcharts(service.sopplainbar('bar', '9px', color, response.data.data, $scope.attscore, margin, 25, 0, [0], 5, false, ""));
                if (!response.data.samplecount || response.data.samplecount == "" ) {
                    var  chartsIds = ["sop","sop1"];
                    service.destroyAllCharts(chartsIds);
                }
              
            });
//             SOPPerformance-getSOPPerformanceImpact
//            service.mastersrv("SOPPerformance-getSOPPerformanceImpact", parameters).then(function (response) {
//                if (response.data) {
//                    $scope.hideloader = true;
//                }
//                console.log("sopimpact data:--------->",response.data.data);
//                $('#page1Sop').highcharts(service.sopnavigatebar('bar', '11px', color, response.data.data, margin, 14, 0, [0], 5, false, ""));
//                //$('#sop1').highcharts(service.sopplainbar('bar', '9px', color, response.data.data, $scope.attscore, margin, 25, 0, [0], 5, false, ""));
//            });
        };
           $scope.hidemodelfilter = function(){
            
            $rootScope.hidemodel=false;
            $rootScope.dealerhide = false;
            $rootScope.mainselectionhide = false;
            $rootScope.hideregion = true;
            $rootScope.hideregiona = false;
            $rootScope.hidecity=false;
            $rootScope.hidestate = false;
            
            $rootScope.hidedealergroup=true;
        };
        $scope.hidemodelfilter1 = function(){
            
            $rootScope.hideregion = true;
            $rootScope.hidemodel = true;
            $rootScope.hideregiona = true;
            $rootScope.dealerhide = false;
            $rootScope.mainselectionhide = true;
            $rootScope.hidecity=true;
            $rootScope.hidedealergroup=true;
            $rootScope.hidestate = false;
            
        };
        $scope.filterchange = function () {
            var  chartsIds = ["sop","sop1"];
            service.destroyAllCharts(chartsIds);
            parameters = [
                {"name": "quarter", "value": $scope.quarter},
                {"name": "month", "value": $scope.month},
                {"name": "compareto", "value": $scope.sopswitchoutput},
                {"name": "model", "value": $scope.model}, 
                {"name": "region", "value": encodeURIComponent($scope.region)},
                {"name": "year", "value": $scope.year},
                {"name": "state", "value": $scope.state},
                {"name": "regiona", "value": $scope.regiona},
                {"name": "langCode", "value": $scope.langcode}, 
                {"name": "city", "value": encodeURIComponent($scope.city)}, 
                {"name": "dealer", "value": $scope.dealer},  
                {"name": "dealergroup", "value": $scope.dealergroup}];
            $scope.graphs();
        };
    }
})();