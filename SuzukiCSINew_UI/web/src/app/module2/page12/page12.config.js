(function () {
    'use strict';

    angular
            .module('app.module2.page12')
            .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
                .state('triangular.csipage12', {
                    url: '/csipage12',
                    views: {
                        '': {
                            templateUrl: 'app/module2/page12/page12.tmpl.html',
                            controller: 'm2page12Controller',
                            controllerAs: 'vm',

                        },
                        'belowContent': {
                            templateUrl: 'app/module2/page12/fab-button.tmpl.html',
                            controller: 'SalesFabController',
                            controllerAs: 'vm'
                        }

                    },
                    data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }

                });

//         triMenuProvider.addMenu({
//                      name: 'Topline',
//                    icon: 'zmdi zmdi-view-list-alt',
//                    type: 'link',
//                    priority: 2.5,
//                     state: 'triangular.csipage12',
//                });
//             triMenuProvider.removeMenu('triangular.page12');   


    }
})();