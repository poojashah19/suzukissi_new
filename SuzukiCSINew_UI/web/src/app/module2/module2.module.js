(function () {
    'use strict';

    angular
            .module('app.module2', [
                'app.module2.csipriority',
                'app.module2.csidealerranking',
                'app.module2.csisummary',
                'app.module2.csiothers',
                'app.module2.csiloyality',
                'app.module2.csiproduct',
                'app.module2.csitrend',
                'app.module2.csisoptrend',
                'app.module2.csiregional',
                'app.module2.csisopperformance',
                'app.module2.csidealersnapshot',
//                'app.module2.page12',
//                'app.module2.page13',
                'app.module2.page14',
                'app.module2.page15',
                 'app.module2.csikpiperformance',
                'app.module2.csikpitrend'


            ]);
})();