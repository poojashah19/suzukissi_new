(function () {
    'use strict';

    angular
            .module('app.module2.csikpitrend')
            .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
                .state('triangular.csikpitrend', {
                    url: '/csikpitrend',

                    views: {
                        '': {
                            templateUrl: 'app/module2/csikpitrend/csikpitrend.tmpl.html',
                            controller: 'csikpitrendController',
                            controllerAs: 'vm',

                        },
                        'belowContent': {
                            templateUrl: 'app/module2/csikpitrend/fab-button.tmpl.html',
                            controller: 'SalesFabController',
                            controllerAs: 'vm'
                        }

                    },
                    data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
                });

//           triMenuProvider.removeMenu('triangular.dealercsipage39'); 
//         triMenuProvider.addMenu({
//                   name: 'SOP Trend',
//                    icon: 'zmdi zmdi-hourglass',
//                    type: 'link',
//                    priority: 1.5,
//                     state: 'triangular.csipage9',
//                });  



    }
})();