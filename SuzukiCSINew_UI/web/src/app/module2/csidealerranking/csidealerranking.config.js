(function () {
    'use strict';

    angular
            .module('app.module2.csidealerranking')
            .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
                .state('triangular.csidealerranking', {
                    url: '/csidealerranking',
                    views: {
                        '': {
                            templateUrl: 'app/module2/csidealerranking/csidealerranking.tmpl.html',
                            controller: 'csidealerrankingController',
                            controllerAs: 'vm',

                        },
                        'belowContent': {
                            templateUrl: 'app/module2/csidealerranking/fab-button.tmpl.html',
                            controller: 'SalesFabController',
                            controllerAs: 'vm'
                        }

                    },
                    data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }

                });



    }
})();