(function () {
    'use strict';

    angular
            .module('app.module2.csiothers')
            .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
                .state('triangular.csiothers', {
                    url: '/csipage5',

                    views: {
                        '': {
                            templateUrl: 'app/module2/csiothers/csiothers.tmpl.html',
                            controller: 'csiothersController',
                            controllerAs: 'vm',

                        },
                        'belowContent': {
                            templateUrl: 'app/module2/csiothers/fab-button.tmpl.html',
                            controller: 'SalesFabController',
                            controllerAs: 'vm'
                        }

                    },
                    data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
                });


    }
})();