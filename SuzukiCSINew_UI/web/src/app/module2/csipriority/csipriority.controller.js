
(function () {
    'use strict';

    angular
            .module('app.module2.csipriority')
            .controller('csipriorityController', csipriorityController);

    /* @ngInject */
    function csipriorityController($scope, service, $state,$filter, $http, $rootScope) {
 var $translate = $filter('translate');
        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776", "#92D400", "#00A1DE", "#f8ae19", "#72C7E7", "#3C8A2E", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.filterchanged = "On Login";
        $rootScope.pagenumber = '4';
        $rootScope.starlabelhide = true;
        $rootScope.dealersnaphide = true
        $rootScope.priorityhide = false;
        $rootScope.dealerpriority = false;
        $rootScope.dealerpriorityhide = false;//only bi-annual
        $rootScope.snaphidezone = true;
        $rootScope.snaphideregion = true;
        $rootScope.summaryRefresh = 1;
        $rootScope.redfeedback = "not";
        $rootScope.mainselectionhide = false;
        $rootScope.filterSelShow = true;
        $rootScope.hidedgregion = true;
        $rootScope.hideyear = true;
        $rootScope.hidebiannual = true;
        $rootScope.hidemonth = true;
        $rootScope.redalertdealerhide = true;
        $rootScope.checkboxInitiliser();
        $scope.langcode = $rootScope.language;
        $scope.zone = "All";
        $scope.region = "All";
        $scope.model = "Study Total";
        $scope.year = $rootScope.year;
        $scope.biannual = "All";
        $scope.month = "Study Total";
 $scope.dealergroup="All";
        if ($scope.langcode == 'EN') {
//            $rootScope.choosecomp = $rootScope.Titlearray[40].ContentEN;
//            $rootScope.regcomp = $rootScope.Titlearray[41].ContentEN;
//            $rootScope.dealercomp = $rootScope.Titlearray[42].ContentEN;
//            $rootScope.reg1 = $rootScope.Titlearray[43].ContentEN;
//            $rootScope.reg2 = $rootScope.Titlearray[42].ContentEN;
//            $rootScope.dealer1 = $rootScope.Titlearray[45].ContentEN;
//            $rootScope.dealer2 = $rootScope.Titlearray[45].ContentEN;
//            $scope.footnote = $rootScope.Footarray[1].ContentEN;
        } else {
//            $rootScope.choosecomp = $rootScope.Titlearray[40].ContentReganal;
//            $rootScope.regcomp = $rootScope.Titlearray[41].ContentReganal;
//            $rootScope.dealercomp = $rootScope.Titlearray[42].ContentReganal;
//            $rootScope.reg1 = $rootScope.Titlearray[43].ContentReganal;
//            $rootScope.reg2 = $rootScope.Titlearray[42].ContentReganal;
//            $rootScope.dealer1 = $rootScope.Titlearray[45].ContentReganal;
//            $rootScope.dealer2 = $rootScope.Titlearray[45].ContentReganal;
//            $scope.footnote = $rootScope.Footarray[1].ContentReganal;
        }

        console.log('debug' + $rootScope.Filterarray.regions);



        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        console.log(" height :", h);
        console.log(" width :", w);
        var margin = 300;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 250;
        } else if (w < 960 && w > 600) {
            margin = 200;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 150;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        console.log('margin', margin);
        //////////////////////////////////////////////////////////////

        $scope.$on('changeLanguage', function (event, data) {   $scope.filterchanged = "Language Changed";



            $scope.langcode = data;
            if ($scope.default == 'dealer') {
                $scope.dealerfilterchange();
            } else if ($scope.default == 'region') {
               $scope.regionfilterchange();
            }
            
             else
             if ($scope.default == 'dealer group') 
            {
                
                $scope.dealergroupfilterchange();
            }
        });




        $scope.$on('changeRadioSwitch', function (event, data) {
          

            if (data == 'dealer') {
                
                
              
        $rootScope.dealer1 = $rootScope.Filterarray.dealerx[0].name;//"$rootScope.Filterarray.dealer[1].name;";
        $rootScope.dealer2 = $rootScope.Filterarray.dealery[2].name;//"Ariyakij";
        

                
                $scope.default = 'dealer';      
                $scope.dealerfilterchange();
            } else
            if(data == 'region')
            {
                
                 $rootScope.region1 = $rootScope.Filterarray.regionx[0].name;//"Bangkok & Greater";
        $rootScope.region2 = $rootScope.Filterarray.regiony[2].name;//"Central and East";
        
                
                
                $scope.default = 'region';
                $scope.regionfilterchange();
            }
            else
            {
                
                $rootScope.dealergroup1 = $rootScope.Filterarray.dealergroupx[0].name;//"$rootScope.Filterarray.dealer[1].name;";
                $rootScope.dealergroup2 = $rootScope.Filterarray.dealergroupy[2].name;
                
                 $scope.default = 'dealer group';
                $scope.dealergroupfilterchange();
            }
        });
//        $rootScope.Filterarray.regions = angular.copy($rootScope.Filterarray.region);

        $scope.$on('changeregion1', function (event, data) {
             $rootScope.region1=data;
                   { $scope.regionfilterchange();}
          });
          
          
          $scope.$on('changeregion2', function (event, data) {
              $rootScope.region2=data;
                   { $scope.regionfilterchange();}
          });

        $scope.$on('changedealer1', function (event, data) {
            $rootScope.dealer1 = data;
            {
                $scope.dealerfilterchange();
            }
        });
        $scope.$on('changedealer2', function (event, data) {
            $rootScope.dealer2 = data;
            {
                $scope.dealerfilterchange();
            }
        });
        
        
        $scope.$on('changedealergroup1', function (event, data) {
            $rootScope.dealergroup1 = data;
            {
                $scope.dealergroupfilterchange();
            }
        });
        $scope.$on('changedealergroup2', function (event, data) {
            $rootScope.dealergroup2 = data;
            {
                $scope.dealergroupfilterchange();
            }
        });

        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual = data;
            if ($scope.default == 'dealer') {
                $scope.default = 'dealer';
                $scope.dealerfilterchange();
            } else if ($scope.default == 'region') {
                $scope.default = 'region';
                $scope.zonefilterchange();
            }
//            $scope.filterchange();
        });



 $scope.$on('changeyear', function (event, data) {   $scope.filterchanged = "Year Changed";
            $scope.year=data;
            $scope.biannual="Study Total";
            $scope.month="Study Total";
             if($scope.default=='dealer'){
                    $scope.default='dealer';
                    $scope.dealerfilterchange();}
                else if($scope.default =='region'){
                    $scope.default='region';
                    $scope.regionfilterchange();
                } 
          });


 
        //////////////////////////////////////////////////////////////
        var vm = this;
        $scope.radioSwitch = "Region Comparison";
        $scope.default = "region";

        $scope.samplespaceless = false;
        $rootScope.region1 = $rootScope.Filterarray.regionx[0].name;//"Bangkok & Greater";
        $rootScope.region2 = $rootScope.Filterarray.regiony[2].name;//"Central and East";
      
        var parameters = [{"name": "measure", "value": $scope.default}, {"name": "langCode", "value": $scope.langcode}, {"name": "region1", "value": encodeURIComponent($rootScope.region1)}, {"name": "region2", "value": encodeURIComponent($rootScope.region2)}];
        service.csi_filterfunction();

//        $scope.onchangeRadioSwitch = function (radio) {
//            if (radio == 'Dealer Comparison') {
//                $scope.priorityRegionHide = true;
//                $scope.default = 'dealer';
//                $scope.dealerfilterchange();
//            } else {
//                $scope.priorityRegionHide = false;
//                $scope.default = 'region';
//                $scope.regionfilterchange();
//            }
//        };

        $scope.graphs = function () {
            
            
             
            if ($scope.langcode == 'EN') {
//                $rootScope.choosecomp = $rootScope.Titlearray[40].ContentEN;
//                $rootScope.regcomp = $rootScope.Titlearray[41].ContentEN;
//                $rootScope.dealercomp = $rootScope.Titlearray[42].ContentEN;
//                $rootScope.reg1 = $rootScope.Titlearray[43].ContentEN;
//                $rootScope.reg2 = $rootScope.Titlearray[42].ContentEN;
//                $rootScope.dealer1 = $rootScope.Titlearray[45].ContentEN;
//                $rootScope.dealer2 = $rootScope.Titlearray[45].ContentEN;
//                $scope.footnote = $rootScope.Footarray[4].ContentEN;
//                $scope.footnote += "<br> " + $rootScope.Footarray[2].ContentEN;
//                $scope.footnote =  $rootScope.Footarray[0].ContentEN;

                if ($scope.default == 'dealer') {
//                    alert("hai dealer en")
//                    $scope.footnote += "<br> " + $rootScope.Footarray[20].ContentEN;
                }
//                $rootScope.overallfootnote = angular.copy($scope.footnote);
//
//                $scope.footnotetitle = $rootScope.Footarray[4].ContentEN;
//                $scope.footnotetitle += "\n " + $rootScope.Footarray[2].ContentEN;
//                $scope.footnotetitle =  $rootScope.Footarray[0].ContentEN;
                if ($scope.default == 'dealer') {
//                    $scope.footnotetitle += "\n " + $rootScope.Footarray[20].ContentEN;
                }

//                $rootScope.overallfootnotetitle = angular.copy($scope.footnotetitle);
                $scope.plotlineName = "Study Total";
            } else {
//                $rootScope.choosecomp = $rootScope.Titlearray[40].ContentReganal;
//                $rootScope.regcomp = $rootScope.Titlearray[41].ContentReganal;
//                $rootScope.dealercomp = $rootScope.Titlearray[42].ContentReganal;
//                $rootScope.reg1 = $rootScope.Titlearray[43].ContentReganal;
//
//                $rootScope.reg2 = $rootScope.Titlearray[42].ContentReganal;
//                $rootScope.dealer1 = $rootScope.Titlearray[45].ContentReganal;
//                $rootScope.dealer2 = $rootScope.Titlearray[45].ContentReganal;
//                $scope.footnote = $rootScope.Footarray[4].ContentReganal;
//                $scope.footnote += "<br> " + $rootScope.Footarray[2].ContentReganal;
//                $scope.footnote += "<br> " + $rootScope.Footarray[0].ContentReganal;
                if ($scope.default == 'dealer') {
//                    alert("hai dealer th")
//                    $scope.footnote += "<br> " + $rootScope.Footarray[20].ContentReganal;
                }
//                $rootScope.overallfootnote = angular.copy($scope.footnote);
//
//                $scope.footnotetitle = $rootScope.Footarray[4].ContentReganal;
//                $scope.footnotetitle += "\n  " + $rootScope.Footarray[2].ContentReganal;
//                $scope.footnotetitle += "\n  " + $rootScope.Footarray[0].ContentReganal;
                if ($scope.default == 'dealer') {
//                    $scope.footnotetitle += "\n  " + $rootScope.Footarray[20].ContentReganal;
                }
//                $rootScope.overallfootnotetitle = angular.copy($scope.footnotetitle);
                $scope.plotlineName = "Study Total";
            }


            service.mastersrv("Priority-getPriority", parameters).then(function (response) {

                var maxaray = [];
                var minarray = [];
                $scope.data = response.data;


                for (var i = 0; i < $scope.data.length; i++) {
                    maxaray.push(Math.max.apply(Math, $scope.data[i].data.map(function (o) {
                        return o.y;
                    })));
                    minarray.push(Math.min.apply(Math, $scope.data[i].data.map(function (o) {
                        return o.y;
                    })))

                }
                var min = Math.min.apply(Math, minarray);

                var max = Math.max.apply(Math, maxaray);

                var roundminvalue = Math.floor(min / 5) * 5;
                var roundmaxvalue = Math.ceil(max / 5) * 5;
                if (roundminvalue < 0) {
                    var roundminvalue = roundminvalue * (-1);
                } else
                if (roundmaxvalue < 0)
                {
                    var roundmaxvalue = roundmaxvalue * (-1);
                }
                var finallastindex = Math.max(roundminvalue, roundmaxvalue);
                var finalfirstindex = finallastindex * (-1);
               var label= $translate('prioritylabelstudytotal');

                $('#mainchart').highcharts(service.verticalline(color, true, response.data, 'left', 'top', true, finalfirstindex, finallastindex, '2',label));
                $('#mainchart1').highcharts(service.verticalline(color, true, response.data, 'left', 'top', true, finalfirstindex, finallastindex, '2', label));

            });
        };


        $scope.dealerfilterchange = function () {



            parameters = [{"name": "measure", "value": $scope.default}, {"name": "langCode", "value": $scope.langcode}, {"name": "dealer1", "value": $rootScope.dealer1}, {"name": "dealer2", "value": $rootScope.dealer2}, {"name": "biannual", "value": $scope.biannual}];
            $scope.graphs();
        };

        $scope.regionfilterchange = function () {

            parameters = [{"name": "measure", "value": $scope.default}, {"name": "langCode", "value": $scope.langcode}, {"name": "region1", "value": encodeURIComponent($rootScope.region1)}, {"name": "region2", "value": encodeURIComponent($rootScope.region2)}];
            $scope.graphs();
        };
        
         $scope.dealergroupfilterchange = function () {

            parameters = [{"name": "measure", "value": $scope.default}, {"name": "langCode", "value": $scope.langcode}, {"name": "dealergroup1", "value": encodeURIComponent($rootScope.dealergroup1)}, {"name": "dealergroup2", "value": encodeURIComponent($rootScope.dealergroup2)}];
            $scope.graphs();
        };



    }
})();