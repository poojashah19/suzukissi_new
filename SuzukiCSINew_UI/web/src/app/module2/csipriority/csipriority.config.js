(function () {
    'use strict';

    angular
            .module('app.module2.csipriority')
            .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
                .state('triangular.csipriority', {
                    url: '/csipriority',
                    views: {
                        '': {
                            templateUrl: 'app/module2/csipriority/csipriority.tmpl.html',
                            controller: 'csipriorityController',
                            controllerAs: 'vm'

                        },
                        'belowContent': {
                            templateUrl: 'app/module2/csipriority/fab-button.tmpl.html',
                            controller: 'SalesFabController',
                            controllerAs: 'vm'
                        }

                    },
                    data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }

                });

    }
})();