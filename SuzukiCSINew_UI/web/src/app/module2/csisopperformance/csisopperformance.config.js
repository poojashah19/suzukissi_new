(function () {
    'use strict';

    angular
            .module('app.module2.csisopperformance')
            .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
                .state('triangular.csisopperformance', {
                    url: '/csisopperformance',
                    views: {
                        '': {
                            templateUrl: 'app/module2/csisopperformance/csisopperformance.tmpl.html',
                            controller: 'csisopperformanceController',
                            controllerAs: 'vm'

                        },
                        'belowContent': {
                            templateUrl: 'app/module2/csisopperformance/fab-button.tmpl.html',
                            controller: 'SalesFabController',
                            controllerAs: 'vm'
                        }

                    },

                    data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
                });

    }
})();