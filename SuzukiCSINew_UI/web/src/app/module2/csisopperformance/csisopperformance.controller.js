(function () {
    'use strict';

    angular
            .module('app.module2.csisopperformance')
            .controller('csisopperformanceController', csisopperformanceController);

    /* @ngInject */
    function csisopperformanceController($scope, $state, service, $http, $rootScope) {
        var vm = this;
        $rootScope.lockLeft = true;
        var color = ["#002776", "#92D400", "#00A1DE", "#72C7E7", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.hideyear = false;
        $rootScope.hidebiannual = true;
        $rootScope.hidemonth = true;
        $rootScope.hidezone = true;
        $rootScope.hideregion = false;
        $rootScope.hidemodel = true;
        $rootScope.hidefactor = true;
        $rootScope.snaphidezone = true;
        $rootScope.snaphideregion = true;
        $rootScope.hidedgregion = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.dealerhide = false;
        $rootScope.dealersnaphide = true;
        $rootScope.redalertdealerhide = true;
        $rootScope.filterchanged = "On Login";
        $rootScope.pagenumber = '5';
        $rootScope.starlabelhide = true;
        $rootScope.priorityhide = true;
        $rootScope.summaryRefresh = 1;
        $scope.langcode = $rootScope.language;
        $rootScope.dsnapshot = "not";
        $scope.dealer = "All";
        $rootScope.redfeedback = "not";
        $rootScope.checkboxInitiliser();
        $rootScope.mainselectionhide = false;
        $rootScope.filterSelShow = true;
        $rootScope.periodhide = true;
        $rootScope.loyaltydealerhide = true;
         $rootScope.hidedealergroup=false;
         
          $rootScope.hideyear2 = true;
         $scope.dealergroup = "All";
//        service.mastersrv("Filters-getDealers").then(function (result) {
//            var study = {"name": "Study Total", "value": "Study Total"};
//            if ($rootScope.language == 'EN') {
//                study = {"name": "Study Total", "value": "Study Total"};
//            } else if ($rootScope.language == 'TH') {
//                study = {"name": "Study Total", "value": "ทั่วประเทศ"};
//            }
//            $rootScope.Filterarray.dealer = result.data;
//            $rootScope.Filterarray.dealer.splice(0, 0, study);
//        });
        if ($scope.langcode == 'EN') {
//            $scope.soptitle1 = $rootScope.Titlearray[46].ContentEN;
//            $scope.soptitle2 = $rootScope.Titlearray[47].ContentEN;
//            $scope.soptitle3 = $rootScope.Titlearray[48].ContentEN;
            $scope.opt1 = "Total";
            $scope.opt2 = "Zone";
            $scope.opt3 = "Region";
//            $scope.sopfooter1 = $rootScope.Titlearray[50].ContentEN;
//            $scope.sopfooter2 = $rootScope.Titlearray[51].ContentEN;
//            $scope.sopfooter3 = $rootScope.Titlearray[52].ContentEN;
//            $scope.sopfooter4 = $rootScope.Titlearray[49].ContentEN;
//            $scope.footnote = $rootScope.Footarray[1].ContentEN;
            $scope.attscore = 'Selected Main Selection';
        } else {
//            $scope.soptitle1 = $rootScope.Titlearray[46].ContentReganal;
//            $scope.soptitle2 = $rootScope.Titlearray[47].ContentReganal;
//            $scope.soptitle3 = $rootScope.Titlearray[48].ContentReganal;
            $scope.opt1 = "全部";
            $scope.opt2 = "โซน";
            $scope.opt3 = "ภูมิภาค";
//            $scope.sopfooter1 = $rootScope.Titlearray[50].ContentReganal;
//            $scope.sopfooter2 = $rootScope.Titlearray[51].ContentReganal;
//            $scope.sopfooter3 = $rootScope.Titlearray[52].ContentReganal;
//            $scope.sopfooter4 = $rootScope.Titlearray[49].ContentReganal;
//            $scope.footnote = $rootScope.Footarray[1].ContentReganal;
            $scope.attscore = 'หัวข้อที่เลือก';
        }




        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        console.log(" height :", h);
        console.log(" width :", w);
        var margin = 320;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 230;
        } else if (w < 960 && w > 600) {
            margin = 180;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 160;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        console.log('margin', margin);

        $scope.$on('changeLanguage', function (event, data) {   $scope.filterchanged = "Language Changed";
            $scope.langcode = data;
            $scope.filterchange();
        });


        $scope.$on('changeyear', function (event, data) {   $scope.filterchanged = "Year Changed";
            $scope.year = data;
            $scope.biannual = "All";
           
            $scope.filterchange();
        });

        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual = data;
           
            $scope.filterchange();
        });

        $scope.$on('changemonth', function (event, data) {
            $scope.month = data;
            $scope.filterchange();
        });

        $scope.$on('changezone', function (event, data) {
            $scope.zone = data;
            $scope.region = "All";
            $scope.dealer = "All";
            $scope.filterchange();
        });

        $scope.$on('changeregion', function (event, data) {   $scope.filterchanged = "Region Changed";
           
            $scope.region = data;
            $scope.dealer = "All";
             $scope.dealergroup = "All";
            $scope.filterchange();
        });
        
        
         $scope.$on('dealergroupchanged', function (event, data) {   $scope.filterchanged = "Dealer Group Changed";
            $scope.dealergroup = data;
            $scope.dealer = "All";
            $scope.filterchange();
        });

        $scope.$on('changedealer', function (event, data) {   $scope.filterchanged = "Dealer Changed";
            $scope.dealer = data;
            $scope.filterchange();
        });
        $scope.$on('changemodel', function (event, data) {   $scope.filterchanged = "Model Changed";
            $scope.model = data;
            $scope.filterchange();
        });
        
        
       
        //////////////////////////////////////////////////////////////


        vm.sopwswitch = sopswitch;
        $scope.zone = "All";
        $scope.region = "All";
        $scope.dealer = $rootScope.Filterarray.dealer[0].name;

        $scope.year = $rootScope.year;
        $scope.biannual = "All";
       
        $scope.sopswitchoutput = 'total';
        $scope.sopswitchoutputcaps = 'Total';
        var parameters = [{"name": "dealergroup", "value": $scope.dealergroup},{"name": "compareto", "value": $scope.sopswitchoutput},  {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year},  {"name": "dealer", "value": $scope.dealer}];
        service.csi_filterfunction();
        $scope.group1 = "Total";

        function sopswitch(flag) {
            if (flag == 'Total') {
                $scope.sopswitchoutput = 'total';
                $scope.sopswitchoutputcaps = 'Total';
                  $rootScope.$broadcast("sopbenchswitch", false);
            } else if (flag == 'Zone') {
                $scope.sopswitchoutput = 'zone';
                $scope.sopswitchoutputcaps = 'Zone';
            } else if (flag == 'Region') {
                $scope.sopswitchoutput = 'region';
                $scope.sopswitchoutputcaps = 'Region';
                
                 $scope.dealergroup='All'
                $rootScope.$broadcast("sopbenchswitch", true);
            }

            $scope.filterchange();
        }




        $scope.graphs = function () {

            $scope.hideloader = false;
            if ($scope.langcode == 'EN') {
//                $scope.soptitle1 = $rootScope.Titlearray[46].ContentEN;
//                $scope.soptitle2 = $rootScope.Titlearray[47].ContentEN;
//                $scope.soptitle = $rootScope.Titlearray[46].ContentEN;
//                $scope.soptitle += "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + $rootScope.Titlearray[47].ContentEN;
//                $scope.soptitle3 = $rootScope.Titlearray[48].ContentEN;
                $scope.opt1 = "Total";
                $scope.opt2 = "Zone";
                $scope.opt3 = "Region";
//                $scope.sopfooter1 = $rootScope.Titlearray[50].ContentEN;
//                $scope.sopfooter2 = $rootScope.Titlearray[51].ContentEN;
//                $scope.sopfooter3 = $rootScope.Titlearray[52].ContentEN;
//                $scope.sopfooter4 = $rootScope.Titlearray[49].ContentEN;

                $scope.sopfootnote = "+Based on those respondents who made an appointment for the service";
//                $scope.sopfootnote += "<br>" + $rootScope.Footarray[3].ContentEN;
//                $scope.sopfootnote =  $rootScope.Footarray[0].ContentEN;
                $rootScope.sopfootnote = angular.copy($scope.sopfootnote);

                $scope.sopfootnotetitle = "+Based on those respondents who made an appointment for the service";
//                $scope.sopfootnotetitle += "\n" + $rootScope.Footarray[3].ContentEN;
//                $scope.sopfootnotetitle =  $rootScope.Footarray[0].ContentEN;
                $rootScope.sopfootnotetitle = angular.copy($scope.sopfootnotetitle);

                $scope.attscore = 'Selected Main Selection';
            } else {
//                $scope.soptitle1 = $rootScope.Titlearray[46].ContentReganal;
//                $scope.soptitle2 = $rootScope.Titlearray[47].ContentReganal;
//                $scope.soptitle = $rootScope.Titlearray[46].ContentReganal;
//                $scope.soptitle += "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + $rootScope.Titlearray[47].ContentReganal;
//                $scope.soptitle3 = $rootScope.Titlearray[48].ContentReganal;
                $scope.opt1 = "全部";
                $scope.opt2 = "โซน";
                $scope.opt3 = "ภูมิภาค";
//                $scope.sopfooter1 = $rootScope.Titlearray[50].ContentReganal;
//                $scope.sopfooter2 = $rootScope.Titlearray[51].ContentReganal;
//                $scope.sopfooter3 = $rootScope.Titlearray[52].ContentReganal;
//                $scope.sopfooter4 = $rootScope.Titlearray[49].ContentReganal;
//                $scope.sopfootnote = "+ข้อมูลที่แสดงนี้มาจากกลุ่มลูกค้าที่ทำการนัดหมายล่วงหน้า เพื่อขอรับบริการ";
//                $scope.sopfootnote += "<br>" + $rootScope.Footarray[3].ContentReganal;
//                $scope.sopfootnote = $rootScope.Footarray[0].ContentReganal;
                $rootScope.sopfootnote = angular.copy($scope.sopfootnote);

//                $scope.sopfootnotetitle = "+ข้อมูลที่แสดงนี้มาจากกลุ่มลูกค้าที่ทำการนัดหมายล่วงหน้า เพื่อขอรับบริการ";
//                $scope.sopfootnotetitle += "\n" + $rootScope.Footarray[3].ContentReganal;
//                $scope.sopfootnotetitle =  $rootScope.Footarray[0].ContentReganal;
//                $rootScope.sopfootnotetitle = angular.copy($scope.sopfootnotetitle);
//                $scope.attscore = 'หัวข้อที่เลือก';
            }

            //        CSISOPPerformance-getCSISOPPerformance&compareto=region

//                $('#sop').highcharts.showLoading();
            service.mastersrv("SOPPerformance-getSOPPerformance", parameters).then(function (response) {
//                   $('#sop').highcharts.hideLoading ();
                if (response.data) {
                    $scope.hideloader = true;
                }
                console.log('response.data', response.data);
                console.log('response.data.data', response.data.data);
                console.log('response.data.samplecount', response.data.samplecount);
                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    console.log($scope.samplespaceless);
                }
                if (response.data.data.length == 0) {
                    response.data.data = [{"bartitle": "100%"}, {"y": "0"}];
                }
                $('#sop').highcharts(service.sopplainbar('bar', '12px', color, response.data.data, $scope.attscore, margin, 14, 0, [0], 5, false, ""));
                $('#sop1').highcharts(service.sopplainbar('bar', '10px', color, response.data.data, $scope.attscore, margin, 25, 0, [0], 5, false, ""));

            });
        };

        $scope.filterchange = function () {
//             $scope.switchdisable=false;
            parameters = [{"name": "dealergroup", "value": $scope.dealergroup},{"name": "compareto", "value": $scope.sopswitchoutput}, {"name": "zone", "value": encodeURIComponent($scope.zone)}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year},  {"name": "dealer", "value": $scope.dealer}];
            $scope.graphs();
        };
    }
})();