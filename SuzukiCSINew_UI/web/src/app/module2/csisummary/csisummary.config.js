(function () {
    'use strict';

    angular
            .module('app.module2.csisummary')
            .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {


 
//        $stateProvider
//                .state('triangular.csisummary', {
//                    abstract: true,
//                    views: {
//                        'sidebarRight@triangular': {
//                            templateUrl: 'app/layouts/rightsidenav/rightsidenav.tmpl.html',
//                            controller: 'RightSidenavController',
//                            controllerAs: 'vm'
//                        }
//                    }
//
//                });
//                
                
                
        $stateProvider
                .state('triangular.csisummary', {
                    url: '/csisummary',
                    views: {
                        '': {
                            templateUrl: 'app/module2/csisummary/csisummary.tmpl.html',
                            controller: 'csisummaryController',
                            controllerAs: 'vm'

                        },
                        'belowContent': {
                            templateUrl: 'app/module2/csisummary/fab-button.tmpl.html',
                            controller: 'SalesFabController',
                            controllerAs: 'vm'
                        }
                        
//                        'sidebarRight@triangular': {
//                            templateUrl: 'app/layouts/rightsidenav/rightsidenav.tmpl.html',
//                            controller: 'RightSidenavController',
//                            controllerAs: 'vm'
//                        }

                    },
                    data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }

                });




    }
})();