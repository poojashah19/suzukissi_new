(function () {
    'use strict';

    angular
            .module('app.module2.csisummary')
            .controller('csisummaryController', csisummaryController);

    /* @ngInject */
    function csisummaryController($scope, service, $q, $filter, $http, $rootScope, $timeout) {
        var vm = this;


        var $translate = $filter('translate');

        $rootScope.langswitch = true;
        $rootScope.lockLeft = true;


        var color = ["#002776", "#92D400", "#00A1DE", "#72C7E7", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.hidedealergroup = true;
        $rootScope.hideyear = false;
        $rootScope.hidebiannual = false;
        $rootScope.hidemonth = false;
        $rootScope.hidezone = false;
        $rootScope.hideregion = false;
        $rootScope.hidedgregion = true;
        $rootScope.hidemodel = true;
        $rootScope.hidefactor = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.snaphidezone = true;
        $rootScope.snaphideregion = true;
        $rootScope.redalertdealerhide = true;
        $scope.show4 = false;
        $scope.show3 = true;
        $rootScope.redfeedback = "not";
        $scope.month = "All";
        $scope.yearset = false;
        $scope.biset = false;
        $scope.monthset = true;
        $rootScope.dealersnaphide = true;
        $rootScope.dealerhide = true;
        $rootScope.filterchanged = "On Login";
        $rootScope.pagenumber = '1';
        $rootScope.starlabelhide = false;
        $rootScope.legendname = "Study Best";
        $rootScope.legendnamew = "Worst Score";
        $rootScope.priorityhide = true;
        $rootScope.mainselectionhide = false;
        $rootScope.filterSelShow = true;
        $rootScope.periodhide = true;
        $rootScope.loyaltydealerhide = true;
        $scope.langcode = $rootScope.language;
        $rootScope.modelsnap = "not";
        $scope.zone = "All";
        $scope.region = "All";
        $scope.model = "All";
        $scope.year = $rootScope.year;

        $scope.biannual = "All";

        $scope.legendname = "Study Best";
        $scope.legendnamew = "Worst Score";
        $rootScope.hideyear2 = true;
//        $rootScope.dtwave=$scope.waves;
        $rootScope.dealerScoreDealer = $rootScope.Filterarray.dealer[1].name;

        if ($rootScope.summaryoffset) {
            $rootScope.summaryoffset = false;
        } else {
            $rootScope.checkboxInitiliser();
        }



        $scope.getStyles = function () {

            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '52%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '50%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': '36px'
            };
        };



        if ($scope.langcode == 'EN') {
            $scope.factorscore = "Factor Score";
            $scope.firstchart = "";

        } else {
            $scope.factorscore = "Factor Score";
        }



        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        console.log(" height :", h);
        console.log(" width :", w);
        var margin = 300;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 250;
        } else if (w < 960 && w > 600) {
            margin = 200;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 150;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        console.log('margin', margin);
        //////////////////////////////////////////////////////////////

        $scope.$on('changeLanguage', function (event, data) {
            $scope.filterchanged = "Language Changed";
            $scope.langcode = data;
            $scope.filterchange();
            $rootScope.languagechangernav();//langCode
        });


        $scope.$on('changeyear', function (event, data) {
            $scope.filterchanged = "Year Changed";
            $scope.year = data;
            $scope.yearset = true;
            $scope.biset = false;
            $scope.monthset = false;
            $scope.show4 = false;
            $scope.show3 = true;
            $scope.biannual = "All";
            $scope.month = "All";
            $scope.region = "All";
            $scope.zone = "All";
            $scope.filterchange();
        });

        $scope.$on('changebiannual', function (event, data) {
            $scope.filterchanged = "Biannual changed";
            $scope.biannual = data;
            $scope.month = "All";
            $scope.region = "All";
            $scope.zone = "All";
            $scope.filterchange();
        });

        $scope.$on('changemonth', function (event, data) {
            $scope.month = data;
            $scope.zone = "All";
            $scope.region = "All";
            $scope.filterchange();
        });

        $scope.$on('changeregion', function (event, data) {
            $scope.filterchanged = "Region Changed";
            $scope.region = data;
            $scope.filterchange();
        });

        $scope.$on('changezone', function (event, data) {
            $scope.zone = data;
            $scope.region = "All";
            $scope.filterchange();
        });
        //////////////////////////////////////////////////////////////


        var parameters = [];
        var yearparameters = [{"name": "langCode", "value": $scope.langcode}];
        $rootScope.mapState = 0;
//        
        parameters = [{"name": "langCode", "value": $scope.langcode}, {"name": "year", "value": $scope.year}, {"name":"biannual", "value":$scope.biannual}, {"name":"month", "value":$scope.month}, {"name":"zone", "value":$scope.zone}, {"name":"region", "value":$scope.region}];

        $scope.getStyle = function () {
            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '53%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': $scope.radius / 3.5 + 'px'
            };
        };
        $scope.getStyle1 = function () {
            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '53%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': $scope.radius / 3.5 + 'px'
            };
        };

        $scope.yearscore = function () {
            yearparameters = [
                {"name": "langCode", "value": $scope.langcode}
            ];

            //csiSummary-getNationalcsiScores
            service.mastersrv("Summary-getNationalScoresYear", parameters).then(function (response) {

                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                } else {
                    $scope.samplespaceless = false;
                }

                $scope.currmonth = response.data.data[0].Wave;
                $scope.svg1 = response.data.data[0].Score;
                $scope.prevmonth = response.data.data[1].Wave;
                $scope.svg2 = response.data.data[1].Score;
                $scope.prevquater = response.data.data[2].Wave;
                $scope.svg3 = response.data.data[2].Score;


            });
        };

        $scope.graphs = function () {

            $scope.hideloader = false;
            if ($scope.langcode == 'EN') {
                $rootScope.overallfootnotetitle = angular.copy($scope.footnotetitle);
                $scope.factorscore = "Factor Score";
                $scope.firstchart = ""; //"";

            } else {
                $scope.factorscore = "Factor Score";
            }
            //Summary-getSummaryRegionFactorScores


            service.mastersrv("Summary-getSummaryRegionFactorScores", parameters).then(function (response) {

                $timeout(function () {
                    $('#barlinecontainer').highcharts(service.creditsummaryplainbar($scope.firstchart, 'column', ["#4292FF"], response.data, $translate('regionalfactorscores'), 0, 45, .3, [1, 1, 1, 1, 1, 1], 5, false));
                    $('#barlinecontainer1').highcharts(service.creditsummaryplainbar($scope.firstchart, 'column', ["#4292FF"], response.data, $translate('regionalfactorscores'), 0, 45, .3, [1, 1, 1, 1, 1, 1], 5, false));
                }, 1000);
                if ($scope.langcode == 'EN') {
                    $rootScope.legendforselectionb = response.data[0].scatter;
                    $rootScope.legendforselectionw = response.data[0].scatter1;
                } else {
                    $rootScope.legendforselectionb = response.data[0].scatter;
                    $rootScope.legendforselectionw = response.data[0].scatter1;
                }
                $rootScope.legendforselectionb = response.data[0].scatter;
                $rootScope.legendforselectionw = response.data[0].scatter1;
            });


            //Summary-getTop5Bottom5
            service.mastersrv("Summary-getTop5Bottom5", parameters).then(function (response) {
                $timeout(function () {
                    $('#treecontainer').highcharts(service.multbarchart(1005, 'bar', response.data, 275, 15, 0, $translate('sopscorelabel'), 11, '8.5px', ""));
                    $('#treecontainer1').highcharts(service.multbarchart(100, 'bar', response.data, 275, 15, 0, $translate('sopscorelabel'), 11, '8.5px', ""));
                }, 1000);
                if (response.data.length == 0) {
                    $scope.samplespaceless = true;
                } else {
                    $scope.samplespaceless = false;
                }

            });

            service.mastersrv("Summary-getTop5Bottom5AttributesMean", parameters).then(function (response) {
                $timeout(function () {
                    $('#bot1').highcharts(service.multbarchartsplit(11, 'bar', response.data, 250, 15, 0, $translate('delightlabel'), 12, '8.5px', ''));
                    $('#bot11').highcharts(service.multbarchartsplit(11, 'bar', response.data, 250, 15, 0, $translate('delightlabel'), 12, '8.5px', ''));
                }, 1000)
            });

            //Summary-RegionWise
            service.mastersrv("Summary-RegionWise", parameters).
                    then(function (response) {
                        $scope.mapData = response.data;
                        $scope.maprender();
                    });
        };

        $scope.printdata = function () {

            $scope.mapdownloadarray = [$translate('dealerrankregion'), $translate('summaryregcsi'), $translate('summarymaplabel')];
            var myData = [];
            $scope.mapData.forEach(function (obj2) {
                myData.push({region: obj2.regioncnvt, score: obj2.value, repondents: obj2.repondents});
            });
            console.log("mapdata", myData);
            return myData;
        };

        $scope.maprender = function () {
            var latlong = {};



            latlong["North 1"] = {"latitude": 24.915576, "longitude": 122.3489, "color": '#002776'};
            latlong["North 2"] = {"latitude": 24.6158, "longitude": 122.7120, "color": '#92D400'};
            latlong["South"] = {"latitude": 21.777073, "longitude": 120.5714, "color": '#00A1DE'};
            latlong["Central"] = {"latitude": 23.653452, "longitude": 121.388033, "color": '#FFBE19'};





            var mapcolor = ["#92D400", "#33B4E5",
                "#4200d4",
                "#c3d327", "#3C8A2E",
                "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];

            var map;
            var minBulletSize = 25;
            var maxBulletSize = 50;
            var min = Infinity;
            var max = -Infinity;

            for (var i = 0; i < $scope.mapData.length; i++) {
                var value = $scope.mapData[i].value;
                if (value < min) {
                    min = value;
                }
                if (value > max) {
                    max = value;
                }


            }

            if (min == max) {
                min = 0;
                max = value;
            }


            AmCharts.ready = function () {
                map = new AmCharts.AmMap();
                map.pathToImages = "http://www.amcharts.com/lib/3/images/";
                map.panEventsEnabled = false;
                map.dragMap = true;
                map.areasSettings = {
                    selectable: true,
                    unlistedAreasColor: "#EFEFEF",
                    unlistedAreasOutlineAlpha: .9,
                    unlistedAreasOutlineColor: "#7F7F7F"
                };
                map.imagesSettings = {
                    balloonText: "<span style='font-size:14px;' align='left'>[[title]]</span>",
                    centered: true
                };
                map.developerMode = true;


                map.addListener("clickMapObject", function (event) {
                    console.log(event.mapObject.code);
                    console.log(event.mapObject.value);
                    if ($rootScope.mapState == 0) {
                        $scope.region = event.mapObject.code;
                        $rootScope.$broadcast("Summaryregionclick", $scope.region);
                        $rootScope.mapState = 1;
                    } else {
                        $scope.region = "All";
                        $rootScope.$broadcast("Summaryregionclick", $scope.region);
                        $rootScope.mapState = 0;
                    }


                });


                var dataProvider = {
                    mapVar: AmCharts.maps.taiwanHigh,
                    zoomLevel: 1.25, // insert the values...
                    zoomLongitude: 121.503197, // from the alert box...
                    zoomLatitude: 22.915636, // here
                    //                       
                    images: []
                };

                map.zoomControl = {
                    panControlEnabled: false,
                    zoomControlEnabled: false,
                    homeButtonEnabled: false
                };



                // it's better to use circle square to show difference between values, not a radius
                var maxSquare = maxBulletSize * maxBulletSize * 2 * Math.PI;
                var minSquare = minBulletSize * minBulletSize * 2 * Math.PI;


                // create circle for each country

                console.log("tsestsyts");
                console.log($scope.mapData);
                for (var i = 0; i < $scope.mapData.length; i++) {
                    var dataItem = $scope.mapData[i];
                    var value = dataItem.value;
                    var datacolor = mapcolor[i];

                    // calculate size of a bubble   

                    var square = ((value - min) / (max - min) * (maxSquare - minSquare)) + minSquare;
                    if (square < minSquare) {
                        square = minSquare;
                    }
                    var size = Math.sqrt(square / (Math.PI * 2));
                    var id = dataItem.code;
                    console.log(id)
                    console.log(latlong[id].color)
                    dataProvider.images.push({
                        "map": "taiwanHigh",
                        type: "bubble",
                        width: size,
                        height: size,
                        color: latlong[id].color,
                        longitude: latlong[id].longitude,
                        latitude: latlong[id].latitude,
                        label: (value),
                        alpha: .30,
                        code: dataItem.code,
                        labelPosition: "inside",
                        title: dataItem.name,
                        value: value,
                        rollOverScale: "1.3",
//                            outlineColor: "#104E8B",
                        outlineAlpha: ".5",
                        outlineThickness: "1",
                        selectable: true,
                        bringForwardOnHover: false,
                        selectedOutlineThickness: "5"

                    });
                }

                map.dataProvider = dataProvider;
                map.write("map");
            };



            AmCharts.ready();

        };

        $scope.filterchange = function () {

            parameters = [{"name": "langCode", "value": $scope.langcode}, {"name": "year", "value": $scope.year}, {"name":"biannual", "value":$scope.biannual}, {"name":"month", "value":$scope.month}, {"name":"zone", "value":$scope.zone}, {"name":"region", "value":$scope.region}];
            $scope.yearscore();
            $scope.graphs();
        };

    }
})();

