(function () {
    'use strict';

    angular
            .module('app.module2.csidealersnapshot')
            .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
                .state('triangular.csidealersnapshot', {
                    url: '/csidealersnapshot',
                    views: {
                        '': {
                            templateUrl: 'app/module2/csidealersnapshot/csidealersnapshot.tmpl.html',
                            controller: 'csidealersnapshotController',
                            controllerAs: 'vm'

                        },
                        'belowContent': {
                            templateUrl: 'app/module2/csidealersnapshot/fab-button.tmpl.html',
                            controller: 'SalesFabController',
                            controllerAs: 'vm'
                        }

                    },

                    data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
                });



    }
})();