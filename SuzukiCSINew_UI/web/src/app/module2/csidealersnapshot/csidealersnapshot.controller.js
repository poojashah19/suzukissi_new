(function () {
    'use strict';

    angular
            .module('app.module2.csidealersnapshot')
            .controller('csidealersnapshotController', csidealersnapshotController);

    /* @ngInject */
    function csidealersnapshotController($scope, $state,$filter, service, $http, $rootScope) {
        var vm = this;
         service.csi_filterfunction();
         $scope.$on("qsuccess",function(event,data){
              $rootScope.checkboxInitiliser();
             
         });
            
        $rootScope.lockLeft = true;
         var $translate = $filter('translate');

        var color = ["#002776", "#92D400", "#00A1DE", "#72C7E7", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.hideyear = false;
        $rootScope.hidebiannual = true;
        $rootScope.hidedgregion = true;
        $rootScope.hidemonth = true;
        $rootScope.hidedealergroup = false;
        $rootScope.hidezone = true;
        $rootScope.hideregion = true;
        $rootScope.snaphidezone = true;
        $rootScope.snaphideregion = false;
        $rootScope.hidemodel = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.hidefactor = false;
        $rootScope.dealerhide = true;
        $rootScope.dealersnaphide = false;
        $rootScope.pagenumber = '9';
        $rootScope.starlabelhide = false;
        $rootScope.priorityhide = true;
        $rootScope.loyaltydealerhide = true;
        $rootScope.hideyear2 = true;
        $rootScope.redalertdealerhide = true;
        $rootScope.summaryRefresh = 1;
        $rootScope.dsnapshot = "dsnap";

        $scope.yearset = true;
        $scope.biset = false;
        $scope.monthset = false;
        $scope.factor =$rootScope.factor;
        $scope.factorname=$rootScope.factor;
        $scope.dealergroup="All";
        var parameters = [];
        $scope.langcode = $rootScope.language;
        $rootScope.redfeedback = "not";
   
        var languagepara = [];
        $scope.zone = "All";
        $scope.region = "All";
        $scope.model = "Study Total";
        $scope.year = $rootScope.year;
        $scope.biannual = "All";
     
        
        $scope.getStyles = function () {

            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '52%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '50%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': '36px'
            };
        };

        var modelpara = [];
        var languagepara = [];
        $scope.show4 = false;
        $scope.show3 = true;
        $rootScope.mainselectionhide = false;
        $rootScope.filterSelShow = true;
        $rootScope.periodhide = false;
        $scope.period = "Year";
        if ($scope.langcode == 'EN') {
//            $scope.dsnap1 = $rootScope.Titlearray[92].ContentEN;
//            $scope.dsnap2 = $rootScope.Titlearray[96].ContentEN;
//            $scope.dsnap3 = $rootScope.Titlearray[137].ContentEN;//deligh
//            $scope.dsnap4 = $rootScope.Titlearray[138].ContentEN;//trent
//            $scope.dsnap5 = $rootScope.Titlearray[110].ContentEN;
//            $scope.dsnapf6 = $rootScope.Titlearray[113].ContentEN;
//            $scope.dsnapf7 = $rootScope.Titlearray[114].ContentEN;
//            $scope.dsnapf8 = $rootScope.Titlearray[115].ContentEN;
//            $scope.footnote = $rootScope.Footarray[0].ContentEN;
            $scope.factorscore = "Factor Score";
            $scope.sopscore = 'csi Score';
            $scope.attscore = 'Attribute Score';
            $scope.factorname = "CSI";
            $scope.firstchart = "";
//            $scope.trendFootnotetitle = $rootScope.Footarray[19].ContentEN;

        } else {
//            $scope.trendFootnotetitle = $rootScope.Footarray[19].ContentReganal;
//            $scope.dsnap1 = $rootScope.Titlearray[92].ContentReganal;
//            $scope.dsnap2 = $rootScope.Titlearray[96].ContentReganal;
//            $scope.dsnap3 = $rootScope.Titlearray[137].ContentReganal;//deligh
//            $scope.dsnap4 = $rootScope.Titlearray[138].ContentReganal;//trent
//            $scope.dsnap5 = $rootScope.Titlearray[110].ContentReganal;
//            $scope.dsnapf6 = $rootScope.Titlearray[113].ContentReganal;
//            $scope.dsnapf7 = $rootScope.Titlearray[114].ContentReganal;
//            $scope.dsnapf8 = $rootScope.Titlearray[115].ContentReganal;
//            $scope.footnote = $rootScope.Footarray[0].ContentReganal;
//            $scope.firstchart = "หมายเหตุ: แสดงเฉพาะข้อมูลล่าสุดที่มี";
//            $scope.factorscore = "คะแนนปัจจัย";
//            $scope.sopscore = 'SOP คะแนน';
//            $scope.attscore = 'คุณลักษณะ คะแนน';

            modelpara = [{"name": "english", "value": $scope.dealername}, {"name": "moduleType", "value": "CSI"}];
            service.mastersrv("Systemuserview-getName", modelpara).then(function (response) {
                $scope.dealername = response.data[0].ContentReganal;
                

            });
            service.mastersrv("Systemuserview-getName", languagepara).then(function (response) {
                $scope.factorname = response.data[0].ContentReganal;
            });


        }
        $scope.$on("snapchange",function(event,data){   
            
            if($rootScope.flagdealerScoreDealer){ 
                $scope.dealer = data;
                $scope.dealername =data;
            }else{ 
                $scope.dealer =  $rootScope.dealerScoreDealer;
                $scope.dealername = $rootScope.dealerScoreDealer; 
            }    
         
         $scope.factor = $rootScope.factor;
        parameters = [{"name": "period", "value": $scope.period}, {"name": "factor", "value": $scope.factor}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "dealer", "value": $scope.dealer},  {"name": "year", "value": $scope.year}, {"name": "langCode", "value": $scope.langcode},{"name": "dealergroup", "value": $scope.dealergroup}];
        var regionobj = [{"name": "state", "value": "All"}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": "All"}];
      $scope.yearscore();
            $scope.graphs();
        })

//        $scope.dealer = $rootScope.dealerScoreDealer;
        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        console.log(" height :", h);
        console.log(" width :", w);
        var margin = 300;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 250;
        } else if (w < 960 && w > 600) {
            margin = 200;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 150;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        console.log('margin', margin);

        $scope.$on('changeLanguage', function (event, data) {   $scope.filterchanged = "Language Changed";
            $scope.langcode = data;

            modelpara = [{"name": "english", "value": $scope.dealer}, {"name": "moduleType", "value": "CSI"}];
            languagepara = [{"name": "english", "value": $scope.factor}, {"name": "moduleType", "value": "CSI"}];
            service.mastersrv("Systemuserview-getName", languagepara).then(function (response) {
                console.log('response response[0]', response.data[0].ContentReganal);
                if ($scope.langcode == 'EN') {
                    $scope.factorname = response.data[0].ContentEN;
                } else {
                    $scope.factorname = response.data[0].ContentReganal;
                }
            });
            service.mastersrv("Systemuserview-getName", modelpara).then(function (response) {
                console.log('response response[0]', response.data[0].ContentReganal);
                if ($scope.langcode == 'EN') {
                    $scope.dealername = response.data[0].ContentEN;
                } else {
                    $scope.dealername = response.data[0].ContentReganal;
                }
            });
            $scope.filterchange();
        });



        $scope.$on('changeyear', function (event, data) {   $scope.filterchanged = "Year Changed";
            $scope.year = data;
            $scope.show4 = false;
            $scope.show3 = true;
            $scope.yearset = true;
            $scope.biset = false;
            $scope.monthset = false;
            $scope.biannual = "All";
            $scope.region="All";
             $scope.dealergroup="All";
            $scope.filterchange();
//            $scope.yearscore();
        });

        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual = data;
            if ($scope.biannual == "All") {
                $scope.show4 = false;
                $scope.show3 = true;
                $scope.yearset = true;
                $scope.biset = false;
                $scope.monthset = false;
                $scope.yearscore();
            } else {
                $scope.show4 = false;
                $scope.show3 = true;
                $scope.yearset = false;
                $scope.biset = true;
                $scope.monthset = false;
               
                $scope.filterchange();
            }

            $scope.month = "Study Total";
        });
         $scope.$on('dealergroupchanged', function (event, data) {    
             $scope.filterchanged = "Dealer Group Changed";
            $rootScope.dealergroup = data;
    
            $scope.filterchange();
        });
        $scope.$on('changemonth', function (event, data) {
            $scope.month = data;
            if ($scope.month == "All") {
                $scope.show4 = false;
                $scope.show3 = true;
                $scope.yearset = true;
                $scope.biset = false;
                $scope.monthset = false;
                $scope.yearscore();
            } else {
                $scope.show4 = true;
                $scope.show3 = false;
                $scope.yearset = false;
                $scope.biset = false;
                $scope.monthset = true;
                $scope.monthscore();
            }

            $scope.filterchange();
        });

        $scope.$on('changefactor', function (event, data) {
            $scope.factor = data;
            languagepara = [{"name": "english", "value": $scope.factor}, {"name": "moduleType", "value": "CSI"}];
            service.mastersrv("Systemuserview-getName", languagepara).then(function (response) {
                console.log('response response[0]', response.data[0].ContentReganal);
                if ($scope.langcode == 'EN') {
                    $scope.factorname = response.data[0].ContentEN;
                } else {
                    $scope.factorname = response.data[0].ContentReganal;
                }

            });
            $scope.filterchange();
        });

        $scope.$on('changefactordsnap', function (event, data) {
            $scope.factor = data;
            languagepara = [{"name": "english", "value": $scope.factor}, {"name": "moduleType", "value": "CSI"}];
            service.mastersrv("Systemuserview-getName", languagepara).then(function (response) {
                console.log('response response[0]', response.data[0].ContentReganal);
                if ($scope.langcode == 'EN') {
                    $scope.factorname = response.data[0].ContentEN;
                } else {
                    $scope.factorname = response.data[0].ContentReganal;
                }

            });
            $scope.loadgraphs();
        });

        $scope.$on('factorClick', function (event, data) {
            $scope.factor = data;
            console.log('state2 params:', $rootScope.factorClick);
            $scope.filterchange();
        });


        

        $scope.$on('snapregionchanged', function (event, data) {
             $scope.filterchanged = "Region Changed";
            $scope.region = data;
            
//            $scope.dealergroup = data;
            if ($rootScope.language == 'EN') {
                var study = {"name": "Study Total", "value": "Study Total"};
                var all = {"name": "All", "value": "All"};
            } else if ($rootScope.language == 'TH') {
                study = {"name": "Study Total", "value": "ทั่วประเทศ"};
                all = {"name": "All", "value": "全部"};
            }
            
            
            
//            var regionobj = [{"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}];
//            service.mastersrv("CSIFilters-getCSIDealergroup", regionobj).then(function (result) {
//                $rootScope.Filterarray.dealergroup = result.data;
//                  
//            });
            
            
            var regionobj = [{"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)}];
            service.mastersrv("Filters-getDealers", regionobj).then(function (result) {
                $rootScope.Filterarray.dealerp = result.data;
                $scope.dealer = $rootScope.Filterarray.dealerp[0].name;
                $scope.dealername = $rootScope.Filterarray.dealerp[0].value;
                
               
               
                $scope.filterchange();
            });
            
        });

        $scope.$on('changedealer', function (event, data) {   $scope.filterchanged = "Dealer Changed";
            $scope.dealer = data;


            $scope.filterchange();
        });
        $scope.$on('dealersnapchanged', function (event, data) {
            $scope.filterchanged = "Dealer Changed";
            $scope.dealer = data;
            
            $scope.dealername = $scope.dealer;
            languagepara = [{"name": "english", "value": $scope.dealer}, {"name": "moduleType", "value": "CSI"}];
            service.mastersrv("Systemuserview-getName", languagepara).then(function (response) {
                console.log('response response[0]', response.data[0].ContentReganal);

                if ($scope.langcode == 'EN') {
                    $scope.dealername = response.data[0].ContentEN;
                    console.log('response $scope.dealername[0]', $scope.dealername);
                } else {
                    $scope.dealername = response.data[0].ContentReganal;
                    console.log('response $scope.dealername[0]', $scope.dealername);
                }
            });
            $scope.filterchange();
        });
        $scope.$on('changeperiod', function (event, data) {
            $scope.period = data;
            $scope.filterchange();
     
        });
        //////////////////////////////////////////////////////////////


        $scope.factor = $rootScope.factor;
        parameters = [{"name": "period", "value": $scope.period}, {"name": "factor", "value": $scope.factor}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "dealer", "value": $scope.dealer},  {"name": "year", "value": $scope.year}, {"name": "langCode", "value": $scope.langcode},{"name": "dealergroup", "value": $scope.dealergroup}];
        var regionobj = [{"name": "state", "value": "All"}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": "All"}];
      
        service.mastersrv("Filters-getDealers", regionobj).then(function (result) {
            $rootScope.Filterarray.dealer = result.data;
        });




        $scope.yearscore = function () {
            //DealerSnapshot-getDealerScores
            service.mastersrv("DealerSnapshot-getDealerScoresYear", parameters).then(function (response) {
                console.log('response.data', response.data);
                console.log('response.data.data', response.data.data);
                console.log('response.data', response.data.samplecount);
                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    console.log($scope.samplespaceless);
                }
                $scope.currmonth = "";
                $scope.prevmonth = "";
                $scope.prevquater = "";

                $scope.svg1 = "";
                $scope.svg2 = "";
                $scope.svg3 = "";


                $scope.currmonth = response.data.data[0].Wave;
                $scope.svg1 = response.data.data[0].Score;
                $scope.prevmonth = response.data.data[1].Wave;
                $scope.svg2 = response.data.data[1].Score;
//                if (!response.data.data[1].Wave) {
//                    $scope.prevmonth = 0;
//                    $scope.svg2 = 0;
//
//                }
                $scope.prevquater = response.data.data[2].Wave;
                $scope.svg3 = response.data.data[2].Score;
            });
        };

        






        $scope.graphs = function () {

            if ($scope.langcode == 'EN') {
//                $scope.dsnap1 = $rootScope.Titlearray[92].ContentEN;
//                $scope.dsnap2 = $rootScope.Titlearray[96].ContentEN;
//                $scope.dsnap3 = $rootScope.Titlearray[137].ContentEN;//deligh
//                $scope.dsnap4 = $rootScope.Titlearray[138].ContentEN;//trent
//                $scope.dsnap5 = $rootScope.Titlearray[110].ContentEN;
//                $scope.dsnapf6 = $rootScope.Titlearray[113].ContentEN;
//                $scope.dsnapf7 = $rootScope.Titlearray[114].ContentEN;
//                $scope.dsnapf8 = $rootScope.Titlearray[115].ContentEN;
//                $scope.footnote = $rootScope.Footarray[4].ContentEN;
//                $scope.footnote += "<br>" + $rootScope.Footarray[2].ContentEN;
//                $scope.footnote += "<br>" + $rootScope.Footarray[3].ContentEN;
//                $scope.footnote =  $rootScope.Footarray[0].ContentEN;

//                $scope.footnotetitle = $rootScope.Footarray[4].ContentEN;
//                $scope.footnotetitle += "\n" + $rootScope.Footarray[2].ContentEN;
//                $scope.footnotetitle += "\n" + $rootScope.Footarray[3].ContentEN;
//                $scope.footnotetitle =  $rootScope.Footarray[0].ContentEN;
                $scope.factorscore = "Factor Score";
                $scope.sopscore = 'SOP Score';
                $scope.attscore = 'Attribute Score';
                $scope.firstchart = "";
//                $scope.trendFootnotetitle = $rootScope.Footarray[19].ContentEN;
            } else {
//                $scope.trendFootnotetitle = $rootScope.Footarray[19].ContentReganal;
//                $scope.dsnap1 = $rootScope.Titlearray[92].ContentReganal;
//                $scope.dsnap2 = $rootScope.Titlearray[96].ContentReganal;
//                $scope.dsnap3 = $rootScope.Titlearray[137].ContentReganal;//deligh
//                $scope.dsnap4 = $rootScope.Titlearray[138].ContentReganal;//trent
//                $scope.dsnap5 = $rootScope.Titlearray[110].ContentReganal;
//                $scope.dsnapf6 = $rootScope.Titlearray[113].ContentReganal;
//                $scope.dsnapf7 = $rootScope.Titlearray[114].ContentReganal;
//                $scope.dsnapf8 = $rootScope.Titlearray[115].ContentReganal;
//                $scope.footnote = $rootScope.Footarray[4].ContentReganal;
//                $scope.footnote += "<br>" + $rootScope.Footarray[2].ContentReganal;
//                $scope.footnote += "<br>" + $rootScope.Footarray[3].ContentReganal;
//                $scope.footnote += "<br>" + $rootScope.Footarray[0].ContentReganal;
//
//
//                $scope.footnotetitle = $rootScope.Footarray[4].ContentReganal;
//                $scope.footnotetitle += "\n" + $rootScope.Footarray[2].ContentReganal;
//                $scope.footnotetitle += "\n" + $rootScope.Footarray[3].ContentReganal;
//                $scope.footnotetitle += "\n" + $rootScope.Footarray[0].ContentReganal;
//                $scope.factorscore = "คะแนนปัจจัย";
//                $scope.sopscore = 'SOP คะแนน';
//                $scope.attscore = 'คุณลักษณะ คะแนน';
//                $scope.firstchart = "หมายเหตุ: แสดงเฉพาะข้อมูลล่าสุดที่มี";

            }



            console.log(parameters);


//                DealerSnapshot-getDealerFactorScores
            service.mastersrv("DealerSnapshot-getDealerFactorScores", parameters).then(function (response) {
               
                  $scope.sampleCount = response.data.samplecount;
                 
                 console.log()
                 $scope.graphdata = response.data.data;
                $scope.astersik = '';
                if ($scope.sampleCount < 10) {
                    $scope.astersik = '**';
                } else if ($scope.sampleCount >= 10 && $scope.sampleCount < 30) {
                    $scope.astersik = '*'
                } else {
                    $scope.astersik;
                }
                
                $('#top1').highcharts(service.clicksummaryplainbar($scope.firstchart, 'column', ['#2979FF'], $scope.graphdata, $translate('regionalfactorscores'), 0, 35, .2, [0], 5, false));
                $('#top11').highcharts(service.clicksummaryplainbar($scope.firstchart, 'column', ['#2979FF'], $scope.graphdata, $translate('regionalfactorscores'), 0, 35, .2, [0], 5, false));
//                        $scope.legendname=response.data[0].scatter;
//                        $scope.legendnamew=response.data[0].scatter1;
                $rootScope.legendforselectionb = response.data[0].scatter;
                $rootScope.legendforselectionw = response.data[0].scatter1;
            });
            //  DealerSnapshot-getDealerSOPTopBottom
//            service.mastersrv("DealerSnapshot-getDealerSOPTopBottom", parameters).then(function (response) {
//                $('#top2').highcharts(service.multbarchartsplit(105, 'bar', response.data, 250, 10, 10, $translate('sopscorelabel'), 9, '6.2px', "%"));
//                $('#top21').highcharts(service.multbarchartsplit(105, 'bar', response.data, 250, 10, 10, $translate('sopscorelabel'), 9, '6.2px', "%"));
//            });

            service.mastersrv("DealerSnapshot-getDealerSOPTopBottom", parameters).then(function (response) {
                $('#top2').highcharts(service.multbarchartsplit(105, 'bar', response.data, 250, 10, 10, $scope.sopscore, 9, '6.2px', "%"));
                $('#top21').highcharts(service.multbarchartsplit(105, 'bar', response.data, 250, 10, 10, $scope.sopscore, 9, '6.2px', "%"));
            });

            //line graph
            service.mastersrv("DealerSnapshot-getAttributeTrendByWave", parameters).then(function (response) {
                $('#dsnapbot1').highcharts(service.horizontalline(response.data, color));
                $('#dsnapbot11').highcharts(service.horizontalline(response.data, color));
            });

//delighted
            service.mastersrv("DealerSnapshot-getDelighted", parameters).then(function (response) {
                $('#bot2').highcharts(service.barplainbarsplitDelightedsnap('bar', ['#FFFFFF','#2979FF'], response.data, $scope.attscore, 210, 35, 0, [0], 5, false, "%"));
               
               // $('#bot2').highcharts(service.barplainbarsplitDelighted('bar', ['#2979FF'], response.data, $scope.factorname+'-'+$translate('delightlabel'), 250, 35, 0, [0], 5, false, "%"));
                $('#bot21').highcharts(service.barplainbarsplitDelightedsnap('bar', ['#2979FF'], response.data, $scope.factorname+'-'+$translate('delightlabel'), 250, 35, 0, [0], 5, false, "%"));
            });


        };




        $scope.loadgraphs = function () {

            if ($scope.langcode == 'EN') {
//                $scope.dsnap1 = $rootScope.Titlearray[92].ContentEN;
//                $scope.dsnap2 = $rootScope.Titlearray[96].ContentEN;
//                $scope.dsnap3 = $rootScope.Titlearray[139].ContentEN;//deligh
//                $scope.dsnap4 = $rootScope.Titlearray[138].ContentEN;//trent
//                $scope.dsnap5 = $rootScope.Titlearray[110].ContentEN;
//                $scope.dsnapf6 = $rootScope.Titlearray[113].ContentEN;
//                $scope.dsnapf7 = $rootScope.Titlearray[114].ContentEN;
//                $scope.dsnapf8 = $rootScope.Titlearray[115].ContentEN;
//                $scope.footnote = $rootScope.Footarray[4].ContentEN;
//                $scope.footnote += "<br>" + $rootScope.Footarray[2].ContentEN;
//                $scope.footnote += "<br>" + $rootScope.Footarray[3].ContentEN;
//                $scope.footnote =  $rootScope.Footarray[0].ContentEN;
//
//                $scope.footnotetitle = $rootScope.Footarray[4].ContentEN;
//                $scope.footnotetitle += "\n" + $rootScope.Footarray[2].ContentEN;
//                $scope.footnotetitle += "\n" + $rootScope.Footarray[3].ContentEN;
//                $scope.footnotetitle =  $rootScope.Footarray[0].ContentEN;
//
//                $scope.trendFootnotetitle = $rootScope.Footarray[19].ContentEN;

                $scope.factorscore = "Factor Score";
                $scope.sopscore = 'SOP Score';
                $scope.attscore = 'Attribute Score';
                $scope.firstchart = "";

            } else {
//                $scope.trendFootnotetitle = $rootScope.Footarray[19].ContentReganal;
//                $scope.dsnap1 = $rootScope.Titlearray[92].ContentReganal;
//                $scope.dsnap2 = $rootScope.Titlearray[96].ContentReganal;
//                $scope.dsnap3 = $rootScope.Titlearray[139].ContentReganal;//deligh
//                $scope.dsnap4 = $rootScope.Titlearray[138].ContentReganal;//trent
//                $scope.dsnap5 = $rootScope.Titlearray[110].ContentReganal;
//                $scope.dsnapf6 = $rootScope.Titlearray[113].ContentReganal;
//                $scope.dsnapf7 = $rootScope.Titlearray[114].ContentReganal;
//                $scope.dsnapf8 = $rootScope.Titlearray[115].ContentReganal;
//                $scope.footnote = $rootScope.Footarray[4].ContentReganal;
//                $scope.footnote += "<br>" + $rootScope.Footarray[2].ContentReganal;
//                $scope.footnote += "<br>" + $rootScope.Footarray[3].ContentReganal;
//                $scope.footnote += "<br>" + $rootScope.Footarray[0].ContentReganal;
//
//
//                $scope.footnotetitle = $rootScope.Footarray[4].ContentReganal;
//                $scope.footnotetitle += "\n" + $rootScope.Footarray[2].ContentReganal;
//                $scope.footnotetitle += "\n" + $rootScope.Footarray[3].ContentReganal;
//                $scope.footnotetitle += "\n" + $rootScope.Footarray[0].ContentReganal;

 
//                $scope.factorscore = "คะแนนปัจจัย";
//                $scope.sopscore = 'SOP คะแนน';
//                $scope.attscore = 'คุณลักษณะ คะแนน';
//                $scope.firstchart = "หมายเหตุ: แสดงเฉพาะข้อมูลล่าสุดที่มี";

            }



            console.log(parameters);


//            //  DealerSnapshot-getDealerSOPTopBottom
//            service.mastersrv("DealerSnapshot-getDealerSOPTopBottom", parameters).then(function (response) {
//                $('#top2').highcharts(service.multbarchartsplit(105, 'bar', response.data, 120, 10, 10, $translate('sopscorelabel'), 9, '6.2px', "%"));
//                $('#top21').highcharts(service.multbarchartsplit(105, 'bar', response.data, 120, 10, 10, $translate('sopscorelabel'), '6.2px', "%"));
//            });

            //line graph
            service.mastersrv("DealerSnapshot-getAttributeTrendByWave", parameters).then(function (response) {
                $('#dsnapbot1').highcharts(service.horizontalline(response.data, color));
                $('#dsnapbot11').highcharts(service.horizontalline(response.data, color));
            });

//delighted
            service.mastersrv("DealerSnapshot-getDelighted", parameters).then(function (response) {
                $('#bot2').highcharts(service.barplainbarsplitDelightedsnap('bar', ['#FFFFFF','#2979FF'], response.data, $scope.attscore, 210, 35, 0, [0], 5, false, "%"));
               
                //$('#bot2').highcharts(service.barplainbarsplitDelighted('bar', ['#2979FF'], response.data, $scope.factorname+'-'+$translate('delightlabel'), 250, 35, 0, [0], 5, false, "%"));
                $('#bot21').highcharts(service.barplainbarsplitDelightedsnap('bar', ['#2979FF'], response.data, $scope.factorname+'-'+$translate('delightlabel'), 250, 35, 0, [0], 5, false, "%"));
            });


        };



        $scope.getStyle = function () {
            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '50%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': $scope.radius / 3.5 + 'px'
            };
        };

        $scope.getStyle1 = function () {
            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '50%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': $scope.radius / 3.5 + 'px'
            };
        };


        $scope.getStyle2 = function () {
            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '50%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': $scope.radius / 4.5 + 'px'
            };
        };

        $scope.filterchange = function () {
            parameters = [{"name": "period", "value": $scope.period}, {"name": "year", "value": $scope.year}, {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "langCode", "value": $scope.langcode}, {"name": "dealer", "value": $scope.dealer}, {"name": "factor", "value": $scope.factor},{"name": "dealergroup", "value": $scope.dealergroup}];
            console.log("dealer" + $scope.dealer); 
            if ($scope.yearset == true && $scope.biset == false && $scope.monthset == false) {

                $scope.yearscore();
            }
            
            $scope.graphs();
        };
        $scope.navTrendAnalysis = function () {

            service.mastersrv("DealerSnapshot-getAttributeTrendByWave", parameters).then(function (response) {
                $('#dsnapbot1').highcharts(service.horizontalline(response.data, color));
                $('#dsnapbot11').highcharts(service.horizontalline(response.data, color));
            });
//            $state.go('triangular.csipage7', {});
        }
    }
})();