(function () {
    'use strict';

    angular
            .module('app.module2.csiproduct')
            .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
                .state('triangular.csiproduct', {
                    url: '/csiproduct',

                    views: {
                        '': {
                            templateUrl: 'app/module2/csiproduct/csiproduct.tmpl.html',
                            controller: 'csiproductController',
                            controllerAs: 'vm',

                        },
                        'belowContent': {
                            templateUrl: 'app/module2/csiproduct/fab-button.tmpl.html',
                            controller: 'SalesFabController',
                            controllerAs: 'vm'
                        }

                    },
                    data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
                });



    }
})();