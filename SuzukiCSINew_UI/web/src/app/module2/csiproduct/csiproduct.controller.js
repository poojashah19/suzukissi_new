(function () {
    'use strict';

    angular
            .module('app.module2.csiproduct')
            .controller('csiproductController', csiproductController);

    /* @ngInject */
    function csiproductController($scope, $state, service,$filter, $http, $rootScope) {
        var vm = this;
        $rootScope.lockLeft = true;
        
        var $translate = $filter('translate');
        var color = ["#002776", "#92D400", "#00A1DE", "#72C7E7", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.hideyear = false;
        $rootScope.hidebiannual = true;
        $rootScope.hidemonth = true;
        $rootScope.hidezone = true;
        $rootScope.hideregion = true;
        $rootScope.hidemodel = false;
        $rootScope.snaphidezone = true;
        $rootScope.snaphideregion = true;
        $rootScope.hidefactor = false;
        $rootScope.hidedgregion = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.dealerhide = true;
        $rootScope.dealersnaphide = true;
        $rootScope.filterchanged = "On Login";
        $rootScope.pagenumber = '10';
        $rootScope.starlabelhide = false;
         $rootScope.hidedealergroup=true;
        $rootScope.legendforselectionb = "Best Overall";
        $rootScope.legendforselectionw = "Worst Overall";
        $rootScope.priorityhide = true;
        $rootScope.loyaltydealerhide = true;
        $rootScope.redfeedback = "not";
        $rootScope.mainselectionhide = false;
        $rootScope.filterSelShow = true;
        
         $rootScope.hideyear2 = true;
 
        $rootScope.summaryRefresh = 1;
        $rootScope.dsnapshot = "not";
        $scope.zone = "All";
        $scope.region = "All";
        $rootScope.redalertdealerhide = true;

        $scope.model = "All";
        $scope.year = $rootScope.year;
        $scope.biannual = "All";
       
        $rootScope.periodhide = false;
        $scope.period = "Year";

        $rootScope.modelsnap = "modelsnap";
        $scope.langcode = $rootScope.language;
        $rootScope.checkboxInitiliser();
        var languagepara = [];
        var modelpara = [];
        var parameters = [];
        $scope.area = "All";
        $scope.zone = "All";
        $scope.province = "All";
        $scope.city = "All";
        $scope.factor = $rootScope.factor;
        $scope.factorname = $rootScope.factor;
        $scope.model = "All";
//        $scope.modelname = $rootScope.Filterarray.model[0].value + " - ";
        if ($scope.model == "All") {
                $scope.modelname = $translate('filterlabelall');

            }

        if ($scope.langcode == 'EN') {
            $scope.model1 = 'SSI by Product'
//            $scope.model2 = $rootScope.Titlearray[117].ContentEN;
//            $scope.mtable1 = $rootScope.Titlearray[149].ContentEN;
////                $scope.mtable2 = $rootScope.Titlearray[145].ContentEN;
//            $scope.mtable3 = $rootScope.Titlearray[150].ContentEN;
//            $scope.mtable4 = $rootScope.Titlearray[151].ContentEN;
//            $scope.mtable5 = $rootScope.Titlearray[152].ContentEN;

            $rootScope.legendforselectionb = "Best Overall";
            $rootScope.legendforselectionw = "Worst Overall";
            $scope.mtable0 = "Model";
            $scope.model = "All";
            $scope.factorscore = "Factor Score";
//            $scope.model3 = $rootScope.Titlearray[129].ContentEN;
//            $scope.footnote = $rootScope.Footarray[0].ContentEN;
        } else {
            
            
            $scope.model1 = 'SSI by Product'
            $rootScope.legendforselectionb = "Best Overall";
            $rootScope.legendforselectionw = "Worst Overall";
            $scope.mtable0 = "Model";
            $scope.model = "All";
            $scope.factorscore = "Factor Score";
            
            
//            $scope.model1 = $rootScope.Titlearray[116].ContentReganal;
//            $scope.model2 = $rootScope.Titlearray[117].ContentReganal;
//            $scope.mtable1 = $rootScope.Titlearray[83].ContentReganal;
//            $rootScope.legendforselectionb = "ดีที่สุดจากทั่วประเทศ";
//            $rootScope.legendforselectionw = "แย่ที่สุดจากทั่วประเทศ";
//            $scope.mtable2 = $rootScope.Titlearray[149].ContentReganal;
//            $scope.mtable3 = $rootScope.Titlearray[150].ContentReganal;
//            $scope.mtable4 = $rootScope.Titlearray[151].ContentReganal;
//            $scope.mtable5 = $rootScope.Titlearray[152].ContentReganal;
//            $scope.mtable2 = $rootScope.Titlearray[84].ContentReganal;
//            $scope.mtable3 = $rootScope.Titlearray[85].ContentReganal;
//            $scope.mtable4 = $rootScope.Titlearray[86].ContentReganal;
//            $scope.mtable5 = $rootScope.Titlearray[87].ContentReganal;
//            $scope.mtable0 = "เครื่องจักรกล";
            $scope.model = "All";
//            $scope.factorscore = "คะแนนปัจจัย";
//            $scope.model3 = $rootScope.Titlearray[129].ContentReganal;
//            $scope.footnote = $rootScope.Footarray[0].ContentReganal;
        }

        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        console.log(" height :", h);
        console.log(" width :", w);
        var margin = 300;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 250;
        } else if (w < 960 && w > 600) {
            margin = 200;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 150;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        console.log('margin', margin);


        $scope.$on('changeLanguage', function (event, data) {   $scope.filterchanged = "Language Changed";
            $scope.langcode = data;
            languagepara = [{"name": "english", "value": $scope.factor}, {"name": "moduleType", "value": "CSI"}];
            modelpara = [{"name": "english", "value": $scope.model}, {"name": "moduleType", "value": "CSI"}];

            service.mastersrv("Systemuserview-getName", languagepara).then(function (response) {
                if ($scope.langcode == 'EN') {
                    $scope.factorname = response.data[0].ContentEN;
                } else {
                    $scope.factorname = response.data[0].ContentReganal;
                }
            });
            service.mastersrv("Systemuserview-getName", modelpara).then(function (response) {
                if ($scope.langcode == 'EN') {
                    $scope.modelname = response.data[0].ContentEN + " - ";
                } else {
                    $scope.modelname = response.data[0].ContentReganal + " - ";
                }
  if ($scope.model == "All") {
                $scope.modelname = $translate('filterlabelall');

            }

            });
            $scope.filterchange();
            $scope.botgraph();
        });

        $scope.$on('changeyear', function (event, data) {   $scope.filterchanged = "Year Changed";
            $scope.year = data;
            $scope.biannual = "All";
            $scope.month = "All";
             $scope.dealer = "All";
             $scope.factor=$rootScope.factor;
            $scope.filterchange();
            $scope.botgraph();
        });

        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual = data;
            $scope.month = "All";
            $scope.filterchange();
            $scope.botgraph();
        });

        $scope.$on('changemonth', function (event, data) {
            $scope.month = data;
            $scope.filterchange();
        });

        $scope.$on('changezone', function (event, data) {
            $scope.zone = data;
            $scope.region = "All";
            $scope.dealer = "All";
            $scope.filterchange();
            $scope.botgraph();
        });

        $scope.$on('changeregion', function (event, data) {   $scope.filterchanged = "Region Changed";
            $scope.region = data;
            $scope.dealer = "All";
            $scope.filterchange();
            $scope.botgraph();
        });

        $scope.$on('changemodel', function (event, data) {   $scope.filterchanged = "Model Changed";
            $scope.model = data;
            
              if ($scope.model == "All") {
                $scope.modelname = $translate('filterlabelall');

            }
            else{
                $scope.modelname = $scope.model;
                }
                
                
            modelpara = [{"name": "english", "value": $scope.model}];
            $scope.botgraph();
        });

        $scope.$on('changemodelgraph', function (event, data) {
            $scope.model = data;
            

            if ($scope.model == "All") {
                $scope.modelname = $translate('filterlabelall');

            }
            else{
                $scope.modelname = $scope.model;
                }
            modelpara = [{"name": "english", "value": $scope.model}];
            $scope.botgraph();
        });

        $scope.$on('changefactor', function (event, data) {
            $scope.factor = data;
            languagepara = [{"name": "english", "value": $scope.factor}, {"name": "moduleType", "value": "CSI"}];
           $scope.factorname = $scope.factor;
            
            service.mastersrv("Systemuserview-getName", languagepara).then(function (response) {
                if ($scope.langcode == 'EN') {
                    $scope.factorname = response.data[0].ContentEN;
                } else {
                    $scope.factorname = response.data[0].ContentReganal;
                }
            });
            $scope.botgraph();
        });
        $scope.$on('changeperiod', function (event, data) {   $scope.filterchanged = "Period Changed";
            $scope.period = data;
            $scope.botgraph();
            $scope.year = "All";
        });
        //////////////////////////////////////////////////////////////

       $scope.exportdata = function () {
            webix.toExcel($$("modeltable"), {
                filterHTML: true
            });
//  webix.toExcel($$("dealertable"));
        }


        var botparameters = [];

        botparameters = [{"name": "period", "value": $scope.period},  {"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)},  {"name": "factor", "value": $scope.factor},  {"name": "model", "value": $scope.model}];
        service.csi_filterfunction();

        parameters = [
            {"name": "langCode", "value": $scope.langcode},       
            {"name": "factor", "value": $scope.factor},       
            {"name": "year", "value": $scope.year}            
        ];

        $scope.graphs = function () {

            if ($scope.langcode == 'EN') {
                
                $scope.model1 = "SSI by Model"
                $scope.model2 = "Factor Scores by Model"
                $rootScope.legendforselectionb = "Best Overall";
                $rootScope.legendforselectionw = "Worst Overall";
                $scope.mtable1 = "Service<br>Initiation<br>(26%)"

                $scope.mtable2 = "Service<br>Advisor<br>(15%)"
                $scope.mtable3 = "Service<br>Facility<br>(12%)"
                $scope.mtable4 = "Vehicle<br>Pickup<br>(18%)"
                $scope.mtable5 = "Service<br>Quality<br>(29%)"
                $scope.mtable0 = "Model";
//                $scope.model3 = $rootScope.Titlearray[129].ContentEN;
//                $scope.mtable1 = $rootScope.Titlearray[149].ContentEN;
//                $scope.mtable3 = $rootScope.Titlearray[150].ContentEN;
//                $scope.mtable4 = $rootScope.Titlearray[151].ContentEN;
//                $scope.mtable5 = $rootScope.Titlearray[152].ContentEN;
//                $scope.mtable0 = "Product";
//                $scope.model3 = $rootScope.Titlearray[129].ContentEN;
//                $scope.footnote = $rootScope.Footarray[0].ContentEN;
//                $scope.footnotetitle = $rootScope.Footarray[0].ContentEN;
                $scope.trendFootnotetitle = ""
//                        $rootScope.Footarray[19].ContentEN;
            } else {
//                $scope.model1 = $rootScope.Titlearray[116].ContentReganal;
//                $scope.model2 = $rootScope.Titlearray[117].ContentReganal;
//                $rootScope.legendforselectionb = "ดีที่สุดจากทั่วประเทศ";
//                $rootScope.legendforselectionw = "แย่ที่สุดจากทั่วประเทศ";
//                    $scope.mtable1=$rootScope.Titlearray[120].ContentReganal+'(26%)';
//                    $scope.mtable2=$rootScope.Titlearray[121].ContentReganal+'(15%)';
//                    $scope.mtable3=$rootScope.Titlearray[122].ContentReganal+'(12%)';
//                    $scope.mtable4=$rootScope.Titlearray[123].ContentReganal+'(18%)';
//                    $scope.mtable5=$rootScope.Titlearray[124].ContentReganal+'(29%)';
//                $scope.mtable2 = $rootScope.Titlearray[83].ContentReganal;
//                $scope.mtable1 = $rootScope.Titlearray[149].ContentReganal;
//                $scope.mtable3 = $rootScope.Titlearray[150].ContentReganal;
//                $scope.mtable4 = $rootScope.Titlearray[151].ContentReganal;
//                $scope.mtable5 = $rootScope.Titlearray[152].ContentReganal;
//                $scope.mtable1 = "การเริ่มต้น ให้บริการ<br>(25%)";//$rootScope.Titlearray[149].ContentReganal;
//                $scope.mtable3 = "ิศวกร/ช่าง ที่ให้บริการ<br>(25%)";//$rootScope.Titlearray[150].ContentReganal;
//                $scope.mtable4 = "ขั้นตอน หลังบริการ<br>(25%)";//$rootScope.Titlearray[151].ContentReganal;
//                $scope.mtable5 = "คุณภาพ งานบริการ<br>(25%)";//$rootScope.Titlearray[152].ContentReganal;
//                $scope.mtable0 = "เครื่องจักรกล";
//                $scope.model3 = $rootScope.Titlearray[129].ContentReganal;
//                $scope.footnote = $rootScope.Footarray[0].ContentReganal;
//                $scope.footnotetitle = $rootScope.Footarray[0].ContentReganal;
//                $scope.trendFootnotetitle = $rootScope.Footarray[19].ContentReganal;
            }



            //        ModelAnalysis-getModelScores 1
            service.mastersrv("ModelAnalysis-getModelScores", parameters).then(function (response) {
                $('#leftgraph').highcharts(service.modelbarchart('bar', '#2979FF ', response.data, 120, 25, 0, $translate('csiavglabel')));
            });
            service.mastersrv("ModelAnalysis-getModelScores", parameters).then(function (response) {
                $('#leftgraph1').highcharts(service.modelbarchart('bar', '#2979FF ', response.data, 120, 20, 0, $translate('csiavglabel')));
            });

            //        ModelAnalysis-getModelFactorScores  2
            service.mastersrv("ModelAnalysis-getModelFactorScores", parameters).then(function (response) {


                $scope.FSmodeltable = {
                    id: "modeltable",
                    view: "datatable",
                    height: 220,

                    //                    headerRowHeight: 60,
                    fixedRowHeight: false,

                    scroll: 'y',
                    position: "flex",

                    select: "row",
                    hover: "myhover",
                    css: "my_style",

                    tooltip: true,
                    columns: [

                        {id: "model", css: "columnstyle", header: {text: $translate('rightsidenavmodel'), css: 'multiline'}, minWidth: 80, adjust: 'true', fillspace: true},
                        {id: "Service Initiation", header: {text: $translate('dealerrankmlserviceinit'), css: 'multiline'}, cssFormat: mark_serviceinitiation, adjust: 'true'},
                        {id: "Service Advisor", header: {text: $translate('dealerrankmlserviceadvisor'), css: 'multiline'}, cssFormat: mark_serviceadvisor, adjust: 'true'},
                        {id: "Service Facility", header: {text: $translate('dealerrankmlservicefacility'), css: 'multiline'}, cssFormat: mark_servicefacility, adjust: 'true'},
                        {id: "Vehicle Pickup", header: {text: $translate('dealerrankmlvehiclepickup'), css: 'multiline'}, cssFormat: mark_vehiclepickup, adjust: 'true'},
                        {id: "Service Quality", header: {text: $translate('dealerrankmlservicequality'), css: 'multiline'}, cssFormat: mark_servicequality, adjust: 'true'},
                    ],
                    data: response.data[1],
                    resizeColumn: true,

                };
                 $rootScope.legendforselectionb = response.data[0].best;
                 $rootScope.legendforselectionw = response.data[0].worst;
            });


           
//         $scope.botrender();
//            
        };





  function mark_serviceinitiation(value, config) {
            if (config["Service Initiationcolor"] != "White") {
                return {"background-color": config["Service Initiationcolor"], "color": '#FFFFFF !important'};
            } else {
                return {"background-color": config["Service Initiationcolor"]};
            }
            return value;
        }
        function mark_serviceadvisor(value, config) {
            if (config["Service Advisorcolor"] != "White") {
                return {"background-color": config["Service Advisorcolor"], "color": '#FFFFFF !important'};
            } else {
                return {"background-color": config["Service Advisorcolor"]};
            }
            return value;
        }
        function mark_servicefacility(value, config) {
            if (config["Service Facilitycolor"] != "White") {
                return {"background-color": config["Service Facilitycolor"], "color": '#FFFFFF !important'};
            } else {
                return {"background-color": config["Service Facilitycolor"]};
            }
            return value;
        }
        function mark_vehiclepickup(value, config) {
            if (config["Vehicle Pickupcolor"] != "White") {
                return {"background-color": config["Vehicle Pickupcolor"], "color": '#FFFFFF !important'};
            } else {
                return {"background-color": config["Vehicle Pickupcolor"]};
            }
            return value;
        }
        function mark_servicequality(value, config) {
            if (config["Service Qualitycolor"] != "White") {
                return {"background-color": config["Service Qualitycolor"], "color": '#FFFFFF !important'};
            } else {
                return {"background-color": config["Service Qualitycolor"]};
            }
            return value;
        }
        $scope.botrender = function () {

            //        ModelAnalysis-getModelFactorTrend 3
            service.mastersrv("ModelAnalysis-getModelFactorTrend", botparameters).then(function (response) {

                $('#botgraph').highcharts(service.barchartmiddle('column', '#92D400 ', response.data.data, 0, 40, 0, 'Model Score', 12, 1, 1000));
                $('#botgraph1').highcharts(service.barchartmiddle('column', '#92D400 ', response.data.data, 0, 40, 0, 'Model Score', 12, 1, 1000));

                modelpara = [{"name": "english", "value": $scope.model}, {"name": "moduleType", "value": "CSI"}];
                
//                $scope.modelname=$scope.model;
            

            });
        };

//{"name": "langCode", "value": $scope.langcode},
        $scope.filterchange = function () {
            parameters = [{"name": "period", "value": $scope.period},  {"name": "langCode", "value": $scope.langcode},  {"name": "region", "value": encodeURIComponent($scope.region)}, {"name": "year", "value": $scope.year},  {"name": "model", "value": $scope.model}];
            $scope.graphs();
        };

        $scope.botgraph = function () {
            botparameters = [{"name": "period", "value": $scope.period}, {"name": "langCode", "value": $scope.langcode}, {"name": "region", "value": encodeURIComponent($scope.region)},  {"name": "model", "value": $scope.model}, {"name": "factor", "value": $scope.factor}];
            $scope.botrender();
        };




    }
})();