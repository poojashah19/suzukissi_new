(function () {
    'use strict';

    angular
            .module('app.module2.csitrend')
            .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
                .state('triangular.csitrend', {
                    url: '/csitrend',

                    views: {
                        '': {
                            templateUrl: 'app/module2/csitrend/csitrend.tmpl.html',
                            controller: 'csitrendController',
                            controllerAs: 'vm',

                        },
                        'belowContent': {
                            templateUrl: 'app/module2/csitrend/fab-button.tmpl.html',
                            controller: 'SalesFabController',
                            controllerAs: 'vm'
                        }

                    },
                    data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
                });


    }
})();