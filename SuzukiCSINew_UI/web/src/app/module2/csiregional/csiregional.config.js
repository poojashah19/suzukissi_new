(function () {
    'use strict';

    angular
            .module('app.module2.csiregional')
            .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
                .state('triangular.csiregional', {
                    url: '/csiregional',

                    views: {
                        '': {
                            templateUrl: 'app/module2/csiregional/csiregional.tmpl.html',
                            controller: 'csiregionalController',
                            controllerAs: 'vm',

                        },
                        'belowContent': {
                            templateUrl: 'app/module2/csiregional/fab-button.tmpl.html',
                            controller: 'SalesFabController',
                            controllerAs: 'vm'
                        }
                        
//                        
//                         'sidebarRight@triangular': {
//                            templateUrl: 'app/layouts/rightsidenav/rightsidenav.tmpl.html',
//                            controller: 'RightSidenavController',
//                            controllerAs: 'vm'
//                        }

                    },
                    data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
                });
    }
})();