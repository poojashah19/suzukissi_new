(function () {
    'use strict';

    angular
            .module('app.module2.csiregional')
            .controller('csiregionalController', csiregionalController);

    /* @ngInject */
    function csiregionalController($scope, $state, $filter, service, $http, $rootScope) {
        var vm = this;
        service.csi_filterfunction();
        $scope.$on("qsuccess", function (event, data) {
            $rootScope.checkboxInitiliser();
        });
        $rootScope.lockLeft = true;
        var color = ["#002776", "#92D400", "#00A1DE", "#72C7E7", "#3C8A2E", "#C9DD03", "#335291", "#A8DD33", "#33B4E5"];
        $rootScope.subpage = "";
        $rootScope.subpagexs = "";
        $rootScope.subpage += " ";
        $rootScope.hideyear = false;
        $rootScope.hidebiannual = false;
        $rootScope.hidemonth = false;
        $rootScope.hidezone = false;
        $rootScope.mainselectionhidetrue = true;
        $rootScope.hideregion = true;
        $rootScope.hidemodel = true;
        $rootScope.snaphidezone = true;
        $rootScope.hidedgregion = true;
        $rootScope.snaphideregion = true;
        $rootScope.hidefactor = true;
        $rootScope.hidetrendfactor = true;
        $rootScope.dealersnaphide = true;
        $rootScope.redalertdealerhide = true;
        $rootScope.dealerhide = true;
        $rootScope.filterchanged = "On Login";
        $rootScope.pagenumber = '2';
        $rootScope.dsnapshot = "not";
        $rootScope.legendname = "Study Best";
        $rootScope.legendnamew = "Worst Score";
        $rootScope.starlabelhide = false;
        $rootScope.priorityhide = true;
        $rootScope.loyaltydealerhide = true;
        $rootScope.summaryRefresh = 1;
        $rootScope.modelsnap = "not";
        $scope.langcode = $rootScope.language;
        $rootScope.hideyear2 = true;
        $rootScope.hidedealergroup = true;
        $scope.zone = "All";
        $scope.region = "All";
        $scope.dealergroup = "All";
        $scope.model = "All";
        $scope.year = $rootScope.year;
        $scope.biannual = "All";
        $scope.factor = $rootScope.factor;

        $scope.svgname = $rootScope.factor;
        $rootScope.redfeedback = "not";
        $rootScope.mainselectionhide = false;
        $rootScope.filterSelShow = true;
        $rootScope.periodhide = true;

        //////////////////////////////////////////////////////////////
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        console.log(" height :", h);
        console.log(" width :", w);
        var margin = 300;
        var lineheight = 25;
        $scope.xstrue = false;
        var weight = 50;
        if (w < 1280 && w > 960) {
            margin = 250;
        } else if (w < 960 && w > 600) {
            margin = 200;
            lineheight = 12;
            weight = 40;
        } else if (w < 600) {
            margin = 150;
            lineheight = 12;
            weight = 30;
            $scope.xstrue = true;
        }
        console.log('margin', margin);


        if ($scope.langcode == 'EN') {
            $scope.factorl = "Factor";
            $scope.regfacttitle = "Factor Score";
            $scope.factorscore = "Factor Score";
            $scope.attscore = 'Attribute Score';
        } else {
        }


        var $translate = $filter('translate');
        $scope.regionname = $translate('filterlabelall');
        $scope.year = $rootScope.year;
        $scope.biannual = "All";
        $scope.month = "All";
        $scope.zone = "All";
        $scope.legendname = "Study Best";
        $scope.legendnamew = "Worst Score";
        $scope.factor = $rootScope.factor;
        var parameters = [{"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode}, {"name": "year", "value": $scope.year},
            {"name":"biannual", "value":$scope.biannual}, {"name":"month", "value":$scope.month}, {"name":"zone", "value":$scope.zone}];



        $scope.$on('changeLanguage', function (event, data) {
            $scope.filterchanged = "Language Changed";
            $scope.langcode = data;

            var regionparam = [{"name": "english", "value": $scope.region}, {"name": "moduleType", "value": "CSI"}];
            service.mastersrv("Systemuserview-getName", regionparam).then(function (response) {
                if ($scope.langcode == 'EN') {
                    $scope.regionname = response.data[0].ContentEN;
                } else {
                    $scope.regionname = response.data[0].ContentReganal;
                }
            });
            $scope.filterchange();
        });

        $scope.$on('changeyear', function (event, data) {
            $scope.filterchanged = "Year Changed";
            $scope.year = data;
            $scope.biannual = "All";
            $scope.month = "All";
            $scope.zone = "All";
            $scope.filterchange();
        });

        $scope.$on('changebiannual', function (event, data) {
            $scope.biannual = data;
            $scope.month = "All";
            $scope.zone = "All";
            $scope.filterchange();
        });

        $scope.$on('changemonth', function (event, data) {
            $scope.month = data;
            $scope.zone = "All";
            $scope.filterchange();
        });

        $scope.$on('changezone', function (event, data) {
            $scope.zone = data;
            $scope.filterchange();
        });

        $scope.factorchanged = function (factor) {
            $scope.factor = factor;
            $scope.filterchange();
        };
        //////////////////////////////////////////////////////////////



        $scope.graphs = function () {

            if ($scope.langcode == 'EN') {
                $scope.regfacttitle = "Factor Score";
                $scope.factorscore = "Factor Score";
                $scope.attscore = 'Attribute Score';
            } else {

                $scope.regfacttitle = "Factor Score";
                $scope.factorscore = "Factor Score";
                $scope.attscore = 'Attribute Score';
            }


            //Regional-getFactorAverage
            service.mastersrv("Regional-getFactorAverage", parameters).then(function (response) {
                console.log('response.data.data', response.data.data);
                console.log('response.data', response.data.samplecount);
                $scope.svgname = response.data.data[0].Factor;
                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    console.log($scope.samplespaceless);
                }

                if (response.data.data.length == 0) {
                    response.data.data = [{"bartitle": "100%"}, {"Score": "0"}];
                }
                $scope.svgvalue = response.data.data[0].Score;
//                            $scope.svgname=response.data.data[0].Factor;
            });


            //Regional-getRegionalFactorAverage
            service.mastersrv("Regional-getRegionalFactorAverage", parameters).then(function (response) {
                $('#barlinecontainer').highcharts(service.regplainbar('column', color, response.data, $scope.svgname + '-' + $translate('regionalfactorscores'), 0, 40, .3, [0], 8, true));
                $('#barlinecontainer1').highcharts(service.regplainbar('column', color, response.data, $scope.svgname + '-' + $translate('regionalfactorscores'), 0, 40, .3, [0], 8, true));


            });


            //Regional-getRegionalAttributeAverage
            service.mastersrv("Regional-getRegionalAttributeAverage", parameters).then(function (response) {
                $('#regbot1').highcharts(service.barplainbar('bar', ['#2979FF'], response.data, $scope.svgname + '-' + $translate('meanlabel'), 280, 35, 0, [0], 5, false, " ", null, null, 6, 8));
                $('#regbot11').highcharts(service.barplainbar('bar', ['#2979FF'], response.data, $scope.svgname + '-' + $translate('meanlabel'), 150, 35, 0, [0], 5, false, " ", null, null, 6, 8));
                $rootScope.legendforselectionb = response.data[0].scatter;
                $rootScope.legendforselectionw = response.data[0].scatter1;
            });


            //Regional-getDissatisfied
            service.mastersrv("Regional-getDelighted", parameters).then(function (response) {
                $('#regbot2').highcharts(service.barplainbar('bar', ['#2979FF'], response.data, $scope.svgname + '-' + $translate('delightlabel'), 280, 35, 0, [0], 5, false, "%"));
                $('#regbot21').highcharts(service.barplainbar('bar', ['#2979FF'], response.data, $scope.svgname + '-' + $translate('delightlabel'), 150, 35, 0, [0], 5, false, "%"));
                $rootScope.legendforselectionb = response.data[0].scatter;
                $rootScope.legendforselectionw = response.data[0].scatter1;
            });
        };

        $scope.graphs1 = function () {
            $scope.regfacttitle = "Factor Score by Region";
            $scope.factorscore = "Factor Score";
            $scope.attscore = 'Attribute Score';


            //Regional-getFactorAverage
            service.mastersrv("Regional-getFactorAverage", parameters).then(function (response) {
                console.log('response.data.data', response.data.data);
                console.log('response.data', response.data.samplecount);
                $scope.svgname = response.data.data[0].Factor;
                if (response.data.samplecount < 10) {
                    $scope.samplespaceless = true;
                    console.log($scope.samplespaceless);
                } else {
                    $scope.samplespaceless = false;
                    console.log($scope.samplespaceless);
                }

                if (response.data.data.length == 0) {
                    response.data.data = [{"bartitle": "100%"}, {"Score": "0"}];
                }
                $scope.svgvalue = response.data.data[0].Score;
//                            $scope.svgname=response.data.data[0].Factor;
            });




            //Regional-getRegionalAttributeAverage
            service.mastersrv("Regional-getRegionalAttributeAverage", parameters).then(function (response) {
                $('#regbot1').highcharts(service.barplainbar('bar', ['#2979FF'], response.data, $scope.svgname + '-' + $translate('meanlabel'), 280, 35, 0, [0], 5, false, " "));
                $('#regbot11').highcharts(service.barplainbar('bar', ['#2979FF'], response.data, $scope.svgname + '-' + $translate('meanlabel'), 150, 35, 0, [0], 5, false, " "));
                $rootScope.legendforselectionb = response.data[0].scatter;
                $rootScope.legendforselectionw = response.data[0].scatter1;
            });


            //Regional-getDissatisfied
            service.mastersrv("Regional-getDelighted", parameters).then(function (response) {
                $('#regbot2').highcharts(service.barplainbar('bar', ['#2979FF'], response.data, $scope.svgname + '-' + $translate('delightlabel'), 280, 35, 0, [0], 5, false, "%"));
                $('#regbot21').highcharts(service.barplainbar('bar', ['#2979FF'], response.data, $scope.svgname + '-' + $translate('delightlabel'), 150, 35, 0, [0], 5, false, "%"));
                $rootScope.legendforselectionb = response.data[0].scatter;
                $rootScope.legendforselectionw = response.data[0].scatter1;
            });


        };

        $scope.isSemi = false;
        $scope.getStyle = function () {

            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '50%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': '36px'
            };
        };

        $scope.getStyle1 = function () {

            var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

            return {
                'top': $scope.isSemi ? 'auto' : '50%',
                'bottom': $scope.isSemi ? '5%' : 'auto',
                'left': '50%',
                'transform': transform,
                '-moz-transform': transform,
                '-webkit-transform': transform,
                'font-size': '36px'
            };
        };




        $scope.filterchange = function () {
            parameters = [{"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode}, {"name": "year", "value": $scope.year},
            {"name":"biannual", "value":$scope.biannual}, {"name":"month", "value":$scope.month}, {"name":"zone", "value":$scope.zone}];
            $scope.graphs();
        };


        $scope.filterchange1 = function () {
            parameters = [{"name": "factor", "value": $scope.factor}, {"name": "langCode", "value": $scope.langcode}, {"name": "year", "value": $scope.year},
            {"name":"biannual", "value":$scope.biannual}, {"name":"month", "value":$scope.month}, {"name":"zone", "value":$scope.zone}];
            $scope.graphs1();
        };


    }
})();