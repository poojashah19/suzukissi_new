(function () {
    'use strict';

    angular
            .module('app.module2.csiloyality')
            .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
                .state('triangular.csiloyality', {
                    url: '/csiloyality',
                    views: {
                        '': {
                            templateUrl: 'app/module2/csiloyality/csiloyality.tmpl.html',
                            controller: 'csiloyalityController',
                            controllerAs: 'vm',

                        },
                        'belowContent': {
                            templateUrl: 'app/module2/csiloyality/fab-button.tmpl.html',
                            controller: 'SalesFabController',
                            controllerAs: 'vm'
                        }

                    },
                    data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    },
                    permicsions: {
                        only: ['viewGitHub']
                    }

                });

    }
})();