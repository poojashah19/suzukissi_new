(function () {
    'use strict';

    angular
            .module('app.module2.csikpiperformance')
            .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
                .state('triangular.csikpiperformance', {
                    url: '/csikpiperformance',
                    views: {
                        '': {
                            templateUrl: 'app/module2/csikpiperformance/csikpiperformance.tmpl.html',
                            controller: 'csikpiperformanceController',
                            controllerAs: 'vm'

                        },
                        'belowContent': {
                            templateUrl: 'app/module2/csikpiperformance/fab-button.tmpl.html',
                            controller: 'SalesFabController',
                            controllerAs: 'vm'
                        }

                    },

                    data: {
                        layout: {
                            contentClass: 'layout-column'
                        }
                    }
                });

    }
})();