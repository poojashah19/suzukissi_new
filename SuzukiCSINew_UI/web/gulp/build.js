'use strict';

var gulp = require('gulp');
var path = require('path');
var rev=require('gulp-rev');
var revReplace=require('gulp-rev-replace');
var paths = gulp.paths;
var revManifest = require('gulp-revmanifest');
var fs = require('fs');
var gulp_sync_task = require('gulp-sync-task');
var gulpSequence = require('gulp-sequence');
var replace = require('gulp-replace');
var $ = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'main-bower-files', 'uglify-save-license', 'del']
});

gulp.task('partials', function () {
  return gulp.src([
    paths.src + '/app/**/*.html',
    paths.tmp + '/app/**/*.html'
  ])
    .pipe($.if(function(file) {
        return $.match(file, ['!**/examples/*.html']);
      },
      $.minifyHtml({
        empty: true,
        spare: true,
        quotes: true
      }))
    )
    .pipe($.angularTemplatecache('templateCacheHtml.js', {
      module: 'app',
      root: 'app'
    }))
    .pipe(gulp.dest(paths.tmp + '/partials/'));
});

gulp.task('html', ['inject', 'partials'], function () {
  var partialsInjectFile = gulp.src(paths.tmp + '/partials/templateCacheHtml.js', { read: false });
  var partialsInjectOptions = {
    starttag: '<!-- inject:partials -->',
    ignorePath: paths.tmp + '/partials',
    addRootSlash: false
  };

  var htmlFilter = $.filter(['*.html', '!/src/app/elements/examples/*.html'], {restore: true});
  var jsFilter = $.filter('**/*.js', {restore: true});
  var cssFilter = $.filter('**/*.css', {restore: true});

  return gulp.src(paths.tmp + '/serve/*.html')
    .pipe($.inject(partialsInjectFile, partialsInjectOptions))
    .pipe($.useref())
    .pipe(jsFilter)
    .pipe($.ngAnnotate())
    .pipe($.uglify({preserveComments: $.uglifySaveLicense}))
    .pipe(rev())
    .pipe(revReplace())
    .pipe(jsFilter.restore)
    .pipe(cssFilter)
    .pipe($.csso())
     .pipe(rev())
    .pipe(revReplace())
    .pipe(cssFilter.restore)
    .pipe($.replace('../bower_components/material-design-iconic-font/dist/fonts', '../fonts'))
    .pipe($.replace('../font/weathericons-regular', '../fonts/weathericons-regular'))
    .pipe($.revReplace())
    .pipe(htmlFilter)
    .pipe($.minifyHtml({
      empty: true,
      spare: true,
      quotes: true
    }))
    .pipe(htmlFilter.restore)
    .pipe(gulp.dest(paths.dist + '/'))
    .pipe($.size({ title: paths.dist + '/', showFiles: true }));
});

gulp.task('images', function () {
  return gulp.src(paths.src + '/assets/images/**/*')
    .pipe(gulp.dest(paths.dist + '/assets/images/'));
});

gulp.task('fonts', function () {
  return gulp.src($.mainBowerFiles())
    .pipe($.filter('**/*.{eot,otf,svg,ttf,woff,woff2}'))
    .pipe($.flatten())
    .pipe(gulp.dest(paths.dist + '/fonts/'));
});

gulp.task('translations', gulpSequence('translation','html','changeprefferedlanguage'))
gulp.task('translation', function () {
  return gulp.src('src/**/i18n/*.json')
    .pipe(rev())
     .pipe(revReplace())
    .pipe(gulp.dest(paths.dist + '/'))
    .pipe(revManifest())
    .pipe(gulp.dest(paths.dist + '/'))
    .pipe($.size());
});

gulp.task('changeprefferedlanguage', function () {
    var name = JSON.parse(fs.readFileSync('dist/rev-manifest.json', 'utf8'));
    var namesOfJsFiles = fs.readdirSync(paths.dist + "/scripts/");
    var valueOfKey = name['app/i18n/eng.json'];
    var nameOfFile = valueOfKey.split("/")[2].split(".")[0];
   
    
    gulp.src(paths.dist + '/scripts/' + namesOfJsFiles[0])
            .pipe(replace('e.preferredLanguage("eng")', 'e.preferredLanguage("' + nameOfFile + '")'))
            .pipe(gulp.dest(paths.dist + '/scripts'));

});

gulp.task('data', function () {
  return gulp.src('src/**/data/*.json')
    .pipe(gulp.dest(paths.dist + '/'))
    .pipe($.size());
});

gulp.task('examplejs', function () {
  return gulp.src('src/**/examples/*.{js,scss}')
    .pipe(gulp.dest(paths.dist + '/'))
    .pipe($.size());
});

gulp.task('misc', function () {
  return gulp.src(paths.src + '/favicon.png')
    .pipe(gulp.dest(paths.dist + '/'));
});

gulp.task('clean', function () {
  return $.del([path.join(paths.dist, '/'), path.join(paths.tmp, '/')]);
});

gulp.task('buildapp', ['html', 'images', 'fonts', 'translations', 'misc', 'data', 'examplejs']);
