'use strict';

var gulp = require('gulp');

var paths = gulp.paths;

var replace = require('gulp-replace');

var util = require('util');

var browserSync = require('browser-sync');

var middleware = require('./proxy');

function browserSyncInit(baseDir, files, browser) {
  browser = browser === undefined ? 'default' : browser;

  var routes = null;
  if(baseDir === paths.src || (util.isArray(baseDir) && baseDir.indexOf(paths.src) !== -1)) {
    routes = {
      '/bower_components': 'bower_components'
    };
  }

  browserSync.instance = browserSync.init(files, {
    startPath: '/',
    server: {
      baseDir: baseDir,
      middleware: middleware,
      routes: routes
    },
    port: 3000,
    browser: browser
  });
}

gulp.task('serve', ['backtolocalurl','watch'], function () {
  browserSyncInit([
    paths.tmp + '/serve',
    paths.src
  ], [
    paths.tmp + '/serve/app/**/*.css',
    paths.src + '/app/**/*.js',
    paths.src + 'src/assets/images/**/*',
    paths.tmp + '/serve/*.html',
    paths.tmp + '/serve/app/**/*.html',
    paths.src + '/app/**/*.html'
  ]);
});
gulp.task('backtolocalurl', function () {

gulp.src(['src/app/app.module.js'])

.pipe(replace('https://webapps1.wheelmonk.com/', 'http://localhost:8084/'))
.pipe(gulp.dest('src/app/'));
});
//gulp.task('backtolocallogin', function () {
//gulp.src(['src/app/authentication/authentication.config.js'])
//.pipe(replace('loader.tmpl.html', 'login.tmpl.html'))
//.pipe(gulp.dest('src/app/authentication/'));
//});
gulp.task('serve:dist', ['buildapp'], function () {
  browserSyncInit(paths.dist);
});

gulp.task('serve:e2e', ['inject'], function () {
  browserSyncInit([paths.tmp + '/serve', paths.src], null, []);
});

gulp.task('serve:e2e-dist', ['buildapp'], function () {
  browserSyncInit(paths.dist, null, []);
});
